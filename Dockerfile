FROM openjdk:8-jre-alpine

VOLUME /tmp
VOLUME /data
ARG JAR_FILE
ADD ${JAR_FILE} app.war
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","./app.war"]
