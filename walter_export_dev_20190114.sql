--
-- PostgreSQL database dump
--

-- Dumped from database version 11.0
-- Dumped by pg_dump version 11.0

-- Started on 2019-01-14 21:52:42

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 18330)
-- Name: tenant1; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tenant1;


ALTER SCHEMA tenant1 OWNER TO postgres;

--
-- TOC entry 9 (class 2615 OID 17359)
-- Name: tenant2; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tenant2;


ALTER SCHEMA tenant2 OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 226 (class 1259 OID 16555)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    id bigint NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL
);


ALTER TABLE public.address OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16553)
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO postgres;

--
-- TOC entry 4483 (class 0 OID 0)
-- Dependencies: 225
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;


--
-- TOC entry 264 (class 1259 OID 16737)
-- Name: appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    begin_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    official_appointment_id bigint,
    type_id bigint
);


ALTER TABLE public.appointment OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 16735)
-- Name: appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4484 (class 0 OID 0)
-- Dependencies: 263
-- Name: appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointment_id_seq OWNED BY public.appointment.id;


--
-- TOC entry 265 (class 1259 OID 16748)
-- Name: appointment_persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment_persons (
    persons_id bigint NOT NULL,
    appointments_id bigint NOT NULL
);


ALTER TABLE public.appointment_persons OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 16755)
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    is_official boolean NOT NULL,
    person_group_id bigint
);


ALTER TABLE public.appointment_type OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 16753)
-- Name: appointment_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointment_type_id_seq OWNER TO postgres;

--
-- TOC entry 4485 (class 0 OID 0)
-- Dependencies: 266
-- Name: appointment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointment_type_id_seq OWNED BY public.appointment_type.id;


--
-- TOC entry 281 (class 1259 OID 16819)
-- Name: arranger; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arranger (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.arranger OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 16825)
-- Name: arranger_compositions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arranger_compositions (
    compositions_id bigint NOT NULL,
    arrangers_id bigint NOT NULL
);


ALTER TABLE public.arranger_compositions OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 16817)
-- Name: arranger_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arranger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arranger_id_seq OWNER TO postgres;

--
-- TOC entry 4486 (class 0 OID 0)
-- Dependencies: 280
-- Name: arranger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arranger_id_seq OWNED BY public.arranger.id;


--
-- TOC entry 244 (class 1259 OID 16637)
-- Name: award; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.award (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    descripton character varying(255)
);


ALTER TABLE public.award OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 16635)
-- Name: award_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.award_id_seq OWNER TO postgres;

--
-- TOC entry 4487 (class 0 OID 0)
-- Dependencies: 243
-- Name: award_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.award_id_seq OWNED BY public.award.id;


--
-- TOC entry 290 (class 1259 OID 16862)
-- Name: bank_import_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bank_import_data (
    id bigint NOT NULL,
    entry_date date NOT NULL,
    entry_value numeric(10,2) NOT NULL,
    entry_text character varying(255),
    partner_name character varying(255),
    entry_reference character varying(255) NOT NULL,
    bank_importer_type character varying(255) NOT NULL,
    cashbook_entry_id bigint,
    cashbook_category_id bigint
);


ALTER TABLE public.bank_import_data OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 16860)
-- Name: bank_import_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_import_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_import_data_id_seq OWNER TO postgres;

--
-- TOC entry 4488 (class 0 OID 0)
-- Dependencies: 289
-- Name: bank_import_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bank_import_data_id_seq OWNED BY public.bank_import_data.id;


--
-- TOC entry 252 (class 1259 OID 16672)
-- Name: cashbook; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.cashbook OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 16937)
-- Name: cashbook_account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_account (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    bank_account boolean,
    bank_importer_type character varying(255),
    receipt_code character varying(255) NOT NULL,
    current_number integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.cashbook_account OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 16935)
-- Name: cashbook_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_account_id_seq OWNER TO postgres;

--
-- TOC entry 4489 (class 0 OID 0)
-- Dependencies: 303
-- Name: cashbook_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_account_id_seq OWNED BY public.cashbook_account.id;


--
-- TOC entry 253 (class 1259 OID 16681)
-- Name: cashbook_cashbook_entries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_cashbook_entries (
    cashbook_entries_id bigint NOT NULL,
    cashbooks_id bigint NOT NULL
);


ALTER TABLE public.cashbook_cashbook_entries OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 16699)
-- Name: cashbook_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_category (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    color character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE public.cashbook_category OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 16697)
-- Name: cashbook_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_category_id_seq OWNER TO postgres;

--
-- TOC entry 4490 (class 0 OID 0)
-- Dependencies: 256
-- Name: cashbook_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_category_id_seq OWNED BY public.cashbook_category.id;


--
-- TOC entry 255 (class 1259 OID 16688)
-- Name: cashbook_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_entry (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    date date NOT NULL,
    amount numeric(10,2) NOT NULL,
    appointment_id bigint,
    cashbook_category_id bigint,
    receipt_id bigint,
    cashbook_account_id bigint
);


ALTER TABLE public.cashbook_entry OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 16686)
-- Name: cashbook_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_entry_id_seq OWNER TO postgres;

--
-- TOC entry 4491 (class 0 OID 0)
-- Dependencies: 254
-- Name: cashbook_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_entry_id_seq OWNED BY public.cashbook_entry.id;


--
-- TOC entry 251 (class 1259 OID 16670)
-- Name: cashbook_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_id_seq OWNER TO postgres;

--
-- TOC entry 4492 (class 0 OID 0)
-- Dependencies: 251
-- Name: cashbook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_id_seq OWNED BY public.cashbook.id;


--
-- TOC entry 238 (class 1259 OID 16610)
-- Name: clothing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing (
    id bigint NOT NULL,
    size character varying(255),
    purchase_date date,
    not_available boolean,
    type_id bigint
);


ALTER TABLE public.clothing OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 16854)
-- Name: clothing_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing_group (
    id bigint NOT NULL,
    size character varying(255),
    quantity integer,
    quantity_available integer,
    type_id bigint
);


ALTER TABLE public.clothing_group OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 16852)
-- Name: clothing_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_group_id_seq OWNER TO postgres;

--
-- TOC entry 4493 (class 0 OID 0)
-- Dependencies: 287
-- Name: clothing_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_group_id_seq OWNED BY public.clothing_group.id;


--
-- TOC entry 237 (class 1259 OID 16608)
-- Name: clothing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4494 (class 0 OID 0)
-- Dependencies: 237
-- Name: clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_id_seq OWNED BY public.clothing.id;


--
-- TOC entry 240 (class 1259 OID 16618)
-- Name: clothing_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.clothing_type OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 16616)
-- Name: clothing_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_type_id_seq OWNER TO postgres;

--
-- TOC entry 4495 (class 0 OID 0)
-- Dependencies: 239
-- Name: clothing_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_type_id_seq OWNED BY public.clothing_type.id;


--
-- TOC entry 272 (class 1259 OID 16779)
-- Name: color_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.color_code (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.color_code OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 16777)
-- Name: color_code_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.color_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.color_code_id_seq OWNER TO postgres;

--
-- TOC entry 4496 (class 0 OID 0)
-- Dependencies: 271
-- Name: color_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.color_code_id_seq OWNED BY public.color_code.id;


--
-- TOC entry 311 (class 1259 OID 17330)
-- Name: column_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.column_config (
    id bigint NOT NULL,
    entity character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    default_visible boolean NOT NULL,
    default_position integer NOT NULL,
    column_display_name character varying(255) NOT NULL,
    sort_by_name character varying(255) NOT NULL
);


ALTER TABLE public.column_config OWNER TO postgres;

--
-- TOC entry 310 (class 1259 OID 17328)
-- Name: column_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4497 (class 0 OID 0)
-- Dependencies: 310
-- Name: column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.column_config_id_seq OWNED BY public.column_config.id;


--
-- TOC entry 261 (class 1259 OID 16721)
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    recipient character varying(255)
);


ALTER TABLE public.company OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 16719)
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO postgres;

--
-- TOC entry 4498 (class 0 OID 0)
-- Dependencies: 260
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;


--
-- TOC entry 262 (class 1259 OID 16730)
-- Name: company_person_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company_person_groups (
    person_groups_id bigint NOT NULL,
    companies_id bigint NOT NULL
);


ALTER TABLE public.company_person_groups OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 16806)
-- Name: composer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composer (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.composer OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 16812)
-- Name: composer_compositions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composer_compositions (
    compositions_id bigint NOT NULL,
    composers_id bigint NOT NULL
);


ALTER TABLE public.composer_compositions OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 16804)
-- Name: composer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.composer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.composer_id_seq OWNER TO postgres;

--
-- TOC entry 4499 (class 0 OID 0)
-- Dependencies: 277
-- Name: composer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.composer_id_seq OWNED BY public.composer.id;


--
-- TOC entry 269 (class 1259 OID 16763)
-- Name: composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composition (
    id bigint NOT NULL,
    nr integer NOT NULL,
    title character varying(255) NOT NULL,
    note character varying(255),
    akm_composition_nr integer NOT NULL,
    cashbook_entry_id bigint,
    ordering_company_id bigint,
    publisher_id bigint,
    genre_id bigint,
    music_book_id bigint,
    color_code_id bigint NOT NULL
);


ALTER TABLE public.composition OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 16772)
-- Name: composition_appointments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composition_appointments (
    appointments_id bigint NOT NULL,
    compositions_id bigint NOT NULL
);


ALTER TABLE public.composition_appointments OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 16761)
-- Name: composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.composition_id_seq OWNER TO postgres;

--
-- TOC entry 4500 (class 0 OID 0)
-- Dependencies: 268
-- Name: composition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.composition_id_seq OWNED BY public.composition.id;


--
-- TOC entry 302 (class 1259 OID 16926)
-- Name: customization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customization (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    data character varying(255) NOT NULL,
    text character varying(255)
);


ALTER TABLE public.customization OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 16924)
-- Name: customization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customization_id_seq OWNER TO postgres;

--
-- TOC entry 4501 (class 0 OID 0)
-- Dependencies: 301
-- Name: customization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customization_id_seq OWNED BY public.customization.id;


--
-- TOC entry 199 (class 1259 OID 16392)
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16387)
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 17305)
-- Name: email_verification_link; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_verification_link (
    id bigint NOT NULL,
    secret_key character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    validation_date timestamp without time zone,
    invalid boolean NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE public.email_verification_link OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 17303)
-- Name: email_verification_link_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_verification_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_verification_link_id_seq OWNER TO postgres;

--
-- TOC entry 4502 (class 0 OID 0)
-- Dependencies: 306
-- Name: email_verification_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_verification_link_id_seq OWNED BY public.email_verification_link.id;


--
-- TOC entry 309 (class 1259 OID 17319)
-- Name: filter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filter (
    id bigint NOT NULL,
    list_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    filter character varying(255) NOT NULL
);


ALTER TABLE public.filter OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 17317)
-- Name: filter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filter_id_seq OWNER TO postgres;

--
-- TOC entry 4503 (class 0 OID 0)
-- Dependencies: 308
-- Name: filter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filter_id_seq OWNED BY public.filter.id;


--
-- TOC entry 276 (class 1259 OID 16798)
-- Name: genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genre (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.genre OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 16796)
-- Name: genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_id_seq OWNER TO postgres;

--
-- TOC entry 4504 (class 0 OID 0)
-- Dependencies: 275
-- Name: genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genre_id_seq OWNED BY public.genre.id;


--
-- TOC entry 200 (class 1259 OID 16398)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 16626)
-- Name: honor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.honor (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.honor OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 16624)
-- Name: honor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.honor_id_seq OWNER TO postgres;

--
-- TOC entry 4505 (class 0 OID 0)
-- Dependencies: 241
-- Name: honor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.honor_id_seq OWNED BY public.honor.id;


--
-- TOC entry 230 (class 1259 OID 16574)
-- Name: instrument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    purchase_date date,
    private_instrument boolean,
    price numeric(10,2),
    type_id bigint NOT NULL,
    cashbook_entry_id bigint
);


ALTER TABLE public.instrument OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16572)
-- Name: instrument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4506 (class 0 OID 0)
-- Dependencies: 229
-- Name: instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_id_seq OWNED BY public.instrument.id;


--
-- TOC entry 234 (class 1259 OID 16592)
-- Name: instrument_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument_service (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    cashbook_entry_id bigint,
    instrument_id bigint NOT NULL
);


ALTER TABLE public.instrument_service OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16590)
-- Name: instrument_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_service_id_seq OWNER TO postgres;

--
-- TOC entry 4507 (class 0 OID 0)
-- Dependencies: 233
-- Name: instrument_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_service_id_seq OWNED BY public.instrument_service.id;


--
-- TOC entry 232 (class 1259 OID 16584)
-- Name: instrument_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.instrument_type OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16582)
-- Name: instrument_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_type_id_seq OWNER TO postgres;

--
-- TOC entry 4508 (class 0 OID 0)
-- Dependencies: 231
-- Name: instrument_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_type_id_seq OWNED BY public.instrument_type.id;


--
-- TOC entry 203 (class 1259 OID 16415)
-- Name: jhi_authority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_authority (
    name character varying(50) NOT NULL,
    role_id bigint NOT NULL,
    id bigint NOT NULL,
    write boolean
);


ALTER TABLE public.jhi_authority OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 17271)
-- Name: jhi_authority_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_authority_id_seq OWNER TO postgres;

--
-- TOC entry 4509 (class 0 OID 0)
-- Dependencies: 305
-- Name: jhi_authority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_authority_id_seq OWNED BY public.jhi_authority.id;


--
-- TOC entry 206 (class 1259 OID 16437)
-- Name: jhi_persistent_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_persistent_audit_event (
    event_id bigint NOT NULL,
    principal character varying(50) NOT NULL,
    event_date timestamp without time zone,
    event_type character varying(255)
);


ALTER TABLE public.jhi_persistent_audit_event OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16435)
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_persistent_audit_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_persistent_audit_event_event_id_seq OWNER TO postgres;

--
-- TOC entry 4510 (class 0 OID 0)
-- Dependencies: 205
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_persistent_audit_event_event_id_seq OWNED BY public.jhi_persistent_audit_event.event_id;


--
-- TOC entry 207 (class 1259 OID 16443)
-- Name: jhi_persistent_audit_evt_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_persistent_audit_evt_data (
    event_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.jhi_persistent_audit_evt_data OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16402)
-- Name: jhi_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_user (
    id bigint NOT NULL,
    login character varying(50) NOT NULL,
    password_hash character varying(60),
    first_name character varying(50),
    last_name character varying(50),
    email character varying(100),
    activated boolean NOT NULL,
    lang_key character varying(5),
    activation_key character varying(20),
    reset_key character varying(20),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    reset_date timestamp without time zone,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone
);


ALTER TABLE public.jhi_user OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16420)
-- Name: jhi_user_authority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_user_authority (
    user_id bigint NOT NULL,
    role_id bigint DEFAULT '2'::bigint NOT NULL
);


ALTER TABLE public.jhi_user_authority OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16400)
-- Name: jhi_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_user_id_seq OWNER TO postgres;

--
-- TOC entry 4511 (class 0 OID 0)
-- Dependencies: 201
-- Name: jhi_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_user_id_seq OWNED BY public.jhi_user.id;


--
-- TOC entry 294 (class 1259 OID 16886)
-- Name: letter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.letter (
    id bigint NOT NULL,
    title character varying(255),
    text text,
    payment boolean,
    current_period boolean,
    previous_period boolean,
    template_id bigint
);


ALTER TABLE public.letter OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 16884)
-- Name: letter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.letter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.letter_id_seq OWNER TO postgres;

--
-- TOC entry 4512 (class 0 OID 0)
-- Dependencies: 293
-- Name: letter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.letter_id_seq OWNED BY public.letter.id;


--
-- TOC entry 295 (class 1259 OID 16895)
-- Name: letter_person_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.letter_person_group (
    person_groups_id bigint NOT NULL,
    letters_id bigint NOT NULL
);


ALTER TABLE public.letter_person_group OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16515)
-- Name: membership; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    person_id bigint,
    membership_fee_id bigint
);


ALTER TABLE public.membership OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16523)
-- Name: membership_fee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    period character varying(255) NOT NULL,
    period_time_fraction integer
);


ALTER TABLE public.membership_fee OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 16534)
-- Name: membership_fee_amount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee_amount (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    amount numeric(10,2) NOT NULL,
    membership_fee_id bigint
);


ALTER TABLE public.membership_fee_amount OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16532)
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_amount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_amount_id_seq OWNER TO postgres;

--
-- TOC entry 4513 (class 0 OID 0)
-- Dependencies: 221
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_amount_id_seq OWNED BY public.membership_fee_amount.id;


--
-- TOC entry 224 (class 1259 OID 16542)
-- Name: membership_fee_for_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee_for_period (
    id bigint NOT NULL,
    nr integer NOT NULL,
    jhi_year integer NOT NULL,
    reference_code character varying(255),
    paid boolean,
    note character varying(255),
    cashbook_entry_id bigint,
    membership_id bigint
);


ALTER TABLE public.membership_fee_for_period OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16540)
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_for_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_for_period_id_seq OWNER TO postgres;

--
-- TOC entry 4514 (class 0 OID 0)
-- Dependencies: 223
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_for_period_id_seq OWNED BY public.membership_fee_for_period.id;


--
-- TOC entry 219 (class 1259 OID 16521)
-- Name: membership_fee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_id_seq OWNER TO postgres;

--
-- TOC entry 4515 (class 0 OID 0)
-- Dependencies: 219
-- Name: membership_fee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_id_seq OWNED BY public.membership_fee.id;


--
-- TOC entry 217 (class 1259 OID 16513)
-- Name: membership_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_id_seq OWNER TO postgres;

--
-- TOC entry 4516 (class 0 OID 0)
-- Dependencies: 217
-- Name: membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_id_seq OWNED BY public.membership.id;


--
-- TOC entry 274 (class 1259 OID 16787)
-- Name: music_book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.music_book (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.music_book OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 16785)
-- Name: music_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.music_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.music_book_id_seq OWNER TO postgres;

--
-- TOC entry 4517 (class 0 OID 0)
-- Dependencies: 273
-- Name: music_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.music_book_id_seq OWNED BY public.music_book.id;


--
-- TOC entry 300 (class 1259 OID 16918)
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification (
    id bigint NOT NULL,
    entity_id bigint,
    created_at timestamp without time zone NOT NULL,
    read_by_user_id bigint,
    notification_rule_id bigint
);


ALTER TABLE public.notification OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 16916)
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_id_seq OWNER TO postgres;

--
-- TOC entry 4518 (class 0 OID 0)
-- Dependencies: 299
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- TOC entry 297 (class 1259 OID 16902)
-- Name: notification_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_rule (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    entity character varying(255) NOT NULL,
    conditions text NOT NULL,
    active boolean NOT NULL,
    message_group character varying(255) NOT NULL,
    message_single character varying(255) NOT NULL
);


ALTER TABLE public.notification_rule OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 16900)
-- Name: notification_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_rule_id_seq OWNER TO postgres;

--
-- TOC entry 4519 (class 0 OID 0)
-- Dependencies: 296
-- Name: notification_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_rule_id_seq OWNED BY public.notification_rule.id;


--
-- TOC entry 298 (class 1259 OID 16911)
-- Name: notification_rule_target_audiences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_rule_target_audiences (
    notification_rules_id bigint NOT NULL,
    target_role_id bigint NOT NULL
);


ALTER TABLE public.notification_rule_target_audiences OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 16843)
-- Name: official_appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.official_appointment (
    id bigint NOT NULL,
    organizer_name character varying(255) NOT NULL,
    organizer_address character varying(255) NOT NULL,
    is_head_quota boolean NOT NULL,
    report_id bigint
);


ALTER TABLE public.official_appointment OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 16841)
-- Name: official_appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.official_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.official_appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4520 (class 0 OID 0)
-- Dependencies: 285
-- Name: official_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.official_appointment_id_seq OWNED BY public.official_appointment.id;


--
-- TOC entry 209 (class 1259 OID 16457)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    birth_date date,
    email character varying(255),
    telephone_number character varying(255),
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    gender character varying(255),
    email_type character varying(255) DEFAULT 'MANUALLY'::character varying NOT NULL
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 16664)
-- Name: person_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_address (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date NOT NULL,
    address_id bigint,
    person_id bigint
);


ALTER TABLE public.person_address OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 16662)
-- Name: person_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_address_id_seq OWNER TO postgres;

--
-- TOC entry 4521 (class 0 OID 0)
-- Dependencies: 249
-- Name: person_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_address_id_seq OWNED BY public.person_address.id;


--
-- TOC entry 248 (class 1259 OID 16656)
-- Name: person_award; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_award (
    id bigint NOT NULL,
    date date NOT NULL,
    award_id bigint,
    person_id bigint
);


ALTER TABLE public.person_award OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16654)
-- Name: person_award_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_award_id_seq OWNER TO postgres;

--
-- TOC entry 4522 (class 0 OID 0)
-- Dependencies: 247
-- Name: person_award_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_award_id_seq OWNED BY public.person_award.id;


--
-- TOC entry 236 (class 1259 OID 16602)
-- Name: person_clothing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_clothing (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    clothing_id bigint,
    person_id bigint
);


ALTER TABLE public.person_clothing OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16600)
-- Name: person_clothing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4523 (class 0 OID 0)
-- Dependencies: 235
-- Name: person_clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_clothing_id_seq OWNED BY public.person_clothing.id;


--
-- TOC entry 212 (class 1259 OID 16473)
-- Name: person_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_group (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE public.person_group OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16471)
-- Name: person_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_group_id_seq OWNER TO postgres;

--
-- TOC entry 4524 (class 0 OID 0)
-- Dependencies: 211
-- Name: person_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_group_id_seq OWNED BY public.person_group.id;


--
-- TOC entry 246 (class 1259 OID 16648)
-- Name: person_honor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_honor (
    id bigint NOT NULL,
    date date NOT NULL,
    honor_id bigint,
    person_id bigint
);


ALTER TABLE public.person_honor OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 16646)
-- Name: person_honor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_honor_id_seq OWNER TO postgres;

--
-- TOC entry 4525 (class 0 OID 0)
-- Dependencies: 245
-- Name: person_honor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_honor_id_seq OWNED BY public.person_honor.id;


--
-- TOC entry 208 (class 1259 OID 16455)
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- TOC entry 4526 (class 0 OID 0)
-- Dependencies: 208
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- TOC entry 216 (class 1259 OID 16507)
-- Name: person_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_info (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.person_info OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16505)
-- Name: person_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_info_id_seq OWNER TO postgres;

--
-- TOC entry 4527 (class 0 OID 0)
-- Dependencies: 215
-- Name: person_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_info_id_seq OWNED BY public.person_info.id;


--
-- TOC entry 228 (class 1259 OID 16566)
-- Name: person_instrument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_instrument (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    instrument_id bigint,
    person_id bigint
);


ALTER TABLE public.person_instrument OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16564)
-- Name: person_instrument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4528 (class 0 OID 0)
-- Dependencies: 227
-- Name: person_instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_instrument_id_seq OWNED BY public.person_instrument.id;


--
-- TOC entry 210 (class 1259 OID 16466)
-- Name: person_person_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_person_groups (
    person_groups_id bigint NOT NULL,
    people_id bigint NOT NULL
);


ALTER TABLE public.person_person_groups OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 16710)
-- Name: receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.receipt (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    title character varying(255) NOT NULL,
    file character varying(255),
    identifier character varying(255),
    company_id bigint,
    overwrite_identifier boolean DEFAULT false NOT NULL,
    initial_cashbook_entry_id bigint NOT NULL
);


ALTER TABLE public.receipt OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 16708)
-- Name: receipt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.receipt_id_seq OWNER TO postgres;

--
-- TOC entry 4529 (class 0 OID 0)
-- Dependencies: 258
-- Name: receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.receipt_id_seq OWNED BY public.receipt.id;


--
-- TOC entry 284 (class 1259 OID 16832)
-- Name: report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.report (
    id bigint NOT NULL,
    generation_date timestamp without time zone NOT NULL,
    association_id character varying(255) NOT NULL,
    association_name character varying(255) NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL
);


ALTER TABLE public.report OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 16830)
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_id_seq OWNER TO postgres;

--
-- TOC entry 4530 (class 0 OID 0)
-- Dependencies: 283
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- TOC entry 214 (class 1259 OID 16494)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16492)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 4531 (class 0 OID 0)
-- Dependencies: 213
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- TOC entry 292 (class 1259 OID 16875)
-- Name: template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.template (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(255),
    margin_left real,
    margin_right real,
    margin_top real,
    margin_bottom real
);


ALTER TABLE public.template OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 16873)
-- Name: template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_id_seq OWNER TO postgres;

--
-- TOC entry 4532 (class 0 OID 0)
-- Dependencies: 291
-- Name: template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.template_id_seq OWNED BY public.template.id;


--
-- TOC entry 313 (class 1259 OID 17341)
-- Name: user_column_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_column_config (
    id bigint NOT NULL,
    visible boolean NOT NULL,
    "position" integer NOT NULL,
    column_config_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.user_column_config OWNER TO postgres;

--
-- TOC entry 312 (class 1259 OID 17339)
-- Name: user_column_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4533 (class 0 OID 0)
-- Dependencies: 312
-- Name: user_column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_column_config_id_seq OWNED BY public.user_column_config.id;


--
-- TOC entry 458 (class 1259 OID 18499)
-- Name: address; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.address (
    id bigint NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL
);


ALTER TABLE tenant1.address OWNER TO postgres;

--
-- TOC entry 457 (class 1259 OID 18497)
-- Name: address_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.address_id_seq OWNER TO postgres;

--
-- TOC entry 4534 (class 0 OID 0)
-- Dependencies: 457
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.address_id_seq OWNED BY tenant1.address.id;


--
-- TOC entry 496 (class 1259 OID 18681)
-- Name: appointment; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.appointment (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    begin_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    official_appointment_id bigint,
    type_id bigint
);


ALTER TABLE tenant1.appointment OWNER TO postgres;

--
-- TOC entry 495 (class 1259 OID 18679)
-- Name: appointment_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4535 (class 0 OID 0)
-- Dependencies: 495
-- Name: appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.appointment_id_seq OWNED BY tenant1.appointment.id;


--
-- TOC entry 497 (class 1259 OID 18692)
-- Name: appointment_persons; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.appointment_persons (
    persons_id bigint NOT NULL,
    appointments_id bigint NOT NULL
);


ALTER TABLE tenant1.appointment_persons OWNER TO postgres;

--
-- TOC entry 499 (class 1259 OID 18699)
-- Name: appointment_type; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.appointment_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    is_official boolean NOT NULL,
    person_group_id bigint
);


ALTER TABLE tenant1.appointment_type OWNER TO postgres;

--
-- TOC entry 498 (class 1259 OID 18697)
-- Name: appointment_type_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.appointment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.appointment_type_id_seq OWNER TO postgres;

--
-- TOC entry 4536 (class 0 OID 0)
-- Dependencies: 498
-- Name: appointment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.appointment_type_id_seq OWNED BY tenant1.appointment_type.id;


--
-- TOC entry 513 (class 1259 OID 18763)
-- Name: arranger; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.arranger (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.arranger OWNER TO postgres;

--
-- TOC entry 514 (class 1259 OID 18769)
-- Name: arranger_compositions; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.arranger_compositions (
    compositions_id bigint NOT NULL,
    arrangers_id bigint NOT NULL
);


ALTER TABLE tenant1.arranger_compositions OWNER TO postgres;

--
-- TOC entry 512 (class 1259 OID 18761)
-- Name: arranger_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.arranger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.arranger_id_seq OWNER TO postgres;

--
-- TOC entry 4537 (class 0 OID 0)
-- Dependencies: 512
-- Name: arranger_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.arranger_id_seq OWNED BY tenant1.arranger.id;


--
-- TOC entry 476 (class 1259 OID 18581)
-- Name: award; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.award (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    descripton character varying(255)
);


ALTER TABLE tenant1.award OWNER TO postgres;

--
-- TOC entry 475 (class 1259 OID 18579)
-- Name: award_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.award_id_seq OWNER TO postgres;

--
-- TOC entry 4538 (class 0 OID 0)
-- Dependencies: 475
-- Name: award_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.award_id_seq OWNED BY tenant1.award.id;


--
-- TOC entry 522 (class 1259 OID 18806)
-- Name: bank_import_data; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.bank_import_data (
    id bigint NOT NULL,
    entry_date date NOT NULL,
    entry_value numeric(10,2) NOT NULL,
    entry_text character varying(255),
    partner_name character varying(255),
    entry_reference character varying(255) NOT NULL,
    bank_importer_type character varying(255) NOT NULL,
    cashbook_entry_id bigint,
    cashbook_category_id bigint
);


ALTER TABLE tenant1.bank_import_data OWNER TO postgres;

--
-- TOC entry 521 (class 1259 OID 18804)
-- Name: bank_import_data_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.bank_import_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.bank_import_data_id_seq OWNER TO postgres;

--
-- TOC entry 4539 (class 0 OID 0)
-- Dependencies: 521
-- Name: bank_import_data_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.bank_import_data_id_seq OWNED BY tenant1.bank_import_data.id;


--
-- TOC entry 484 (class 1259 OID 18616)
-- Name: cashbook; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.cashbook (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant1.cashbook OWNER TO postgres;

--
-- TOC entry 536 (class 1259 OID 18881)
-- Name: cashbook_account; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.cashbook_account (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    bank_account boolean,
    bank_importer_type character varying(255),
    receipt_code character varying(255) NOT NULL,
    current_number integer DEFAULT 0 NOT NULL
);


ALTER TABLE tenant1.cashbook_account OWNER TO postgres;

--
-- TOC entry 535 (class 1259 OID 18879)
-- Name: cashbook_account_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.cashbook_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.cashbook_account_id_seq OWNER TO postgres;

--
-- TOC entry 4540 (class 0 OID 0)
-- Dependencies: 535
-- Name: cashbook_account_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.cashbook_account_id_seq OWNED BY tenant1.cashbook_account.id;


--
-- TOC entry 485 (class 1259 OID 18625)
-- Name: cashbook_cashbook_entries; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.cashbook_cashbook_entries (
    cashbook_entries_id bigint NOT NULL,
    cashbooks_id bigint NOT NULL
);


ALTER TABLE tenant1.cashbook_cashbook_entries OWNER TO postgres;

--
-- TOC entry 489 (class 1259 OID 18643)
-- Name: cashbook_category; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.cashbook_category (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    color character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE tenant1.cashbook_category OWNER TO postgres;

--
-- TOC entry 488 (class 1259 OID 18641)
-- Name: cashbook_category_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.cashbook_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.cashbook_category_id_seq OWNER TO postgres;

--
-- TOC entry 4541 (class 0 OID 0)
-- Dependencies: 488
-- Name: cashbook_category_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.cashbook_category_id_seq OWNED BY tenant1.cashbook_category.id;


--
-- TOC entry 487 (class 1259 OID 18632)
-- Name: cashbook_entry; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.cashbook_entry (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    date date NOT NULL,
    amount numeric(10,2) NOT NULL,
    appointment_id bigint,
    cashbook_category_id bigint,
    receipt_id bigint,
    cashbook_account_id bigint
);


ALTER TABLE tenant1.cashbook_entry OWNER TO postgres;

--
-- TOC entry 486 (class 1259 OID 18630)
-- Name: cashbook_entry_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.cashbook_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.cashbook_entry_id_seq OWNER TO postgres;

--
-- TOC entry 4542 (class 0 OID 0)
-- Dependencies: 486
-- Name: cashbook_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.cashbook_entry_id_seq OWNED BY tenant1.cashbook_entry.id;


--
-- TOC entry 483 (class 1259 OID 18614)
-- Name: cashbook_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.cashbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.cashbook_id_seq OWNER TO postgres;

--
-- TOC entry 4543 (class 0 OID 0)
-- Dependencies: 483
-- Name: cashbook_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.cashbook_id_seq OWNED BY tenant1.cashbook.id;


--
-- TOC entry 470 (class 1259 OID 18554)
-- Name: clothing; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.clothing (
    id bigint NOT NULL,
    size character varying(255),
    purchase_date date,
    not_available boolean,
    type_id bigint
);


ALTER TABLE tenant1.clothing OWNER TO postgres;

--
-- TOC entry 520 (class 1259 OID 18798)
-- Name: clothing_group; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.clothing_group (
    id bigint NOT NULL,
    size character varying(255),
    quantity integer,
    quantity_available integer,
    type_id bigint
);


ALTER TABLE tenant1.clothing_group OWNER TO postgres;

--
-- TOC entry 519 (class 1259 OID 18796)
-- Name: clothing_group_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.clothing_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.clothing_group_id_seq OWNER TO postgres;

--
-- TOC entry 4544 (class 0 OID 0)
-- Dependencies: 519
-- Name: clothing_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.clothing_group_id_seq OWNED BY tenant1.clothing_group.id;


--
-- TOC entry 469 (class 1259 OID 18552)
-- Name: clothing_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4545 (class 0 OID 0)
-- Dependencies: 469
-- Name: clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.clothing_id_seq OWNED BY tenant1.clothing.id;


--
-- TOC entry 472 (class 1259 OID 18562)
-- Name: clothing_type; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.clothing_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.clothing_type OWNER TO postgres;

--
-- TOC entry 471 (class 1259 OID 18560)
-- Name: clothing_type_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.clothing_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.clothing_type_id_seq OWNER TO postgres;

--
-- TOC entry 4546 (class 0 OID 0)
-- Dependencies: 471
-- Name: clothing_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.clothing_type_id_seq OWNED BY tenant1.clothing_type.id;


--
-- TOC entry 504 (class 1259 OID 18723)
-- Name: color_code; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.color_code (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.color_code OWNER TO postgres;

--
-- TOC entry 503 (class 1259 OID 18721)
-- Name: color_code_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.color_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.color_code_id_seq OWNER TO postgres;

--
-- TOC entry 4547 (class 0 OID 0)
-- Dependencies: 503
-- Name: color_code_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.color_code_id_seq OWNED BY tenant1.color_code.id;


--
-- TOC entry 543 (class 1259 OID 19274)
-- Name: column_config; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.column_config (
    id bigint NOT NULL,
    entity character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    default_visible boolean NOT NULL,
    default_position integer NOT NULL,
    column_display_name character varying(255) NOT NULL,
    sort_by_name character varying(255) NOT NULL
);


ALTER TABLE tenant1.column_config OWNER TO postgres;

--
-- TOC entry 542 (class 1259 OID 19272)
-- Name: column_config_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4548 (class 0 OID 0)
-- Dependencies: 542
-- Name: column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.column_config_id_seq OWNED BY tenant1.column_config.id;


--
-- TOC entry 493 (class 1259 OID 18665)
-- Name: company; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.company (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    recipient character varying(255)
);


ALTER TABLE tenant1.company OWNER TO postgres;

--
-- TOC entry 492 (class 1259 OID 18663)
-- Name: company_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.company_id_seq OWNER TO postgres;

--
-- TOC entry 4549 (class 0 OID 0)
-- Dependencies: 492
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.company_id_seq OWNED BY tenant1.company.id;


--
-- TOC entry 494 (class 1259 OID 18674)
-- Name: company_person_groups; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.company_person_groups (
    person_groups_id bigint NOT NULL,
    companies_id bigint NOT NULL
);


ALTER TABLE tenant1.company_person_groups OWNER TO postgres;

--
-- TOC entry 510 (class 1259 OID 18750)
-- Name: composer; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.composer (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.composer OWNER TO postgres;

--
-- TOC entry 511 (class 1259 OID 18756)
-- Name: composer_compositions; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.composer_compositions (
    compositions_id bigint NOT NULL,
    composers_id bigint NOT NULL
);


ALTER TABLE tenant1.composer_compositions OWNER TO postgres;

--
-- TOC entry 509 (class 1259 OID 18748)
-- Name: composer_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.composer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.composer_id_seq OWNER TO postgres;

--
-- TOC entry 4550 (class 0 OID 0)
-- Dependencies: 509
-- Name: composer_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.composer_id_seq OWNED BY tenant1.composer.id;


--
-- TOC entry 501 (class 1259 OID 18707)
-- Name: composition; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.composition (
    id bigint NOT NULL,
    nr integer NOT NULL,
    title character varying(255) NOT NULL,
    note character varying(255),
    akm_composition_nr integer NOT NULL,
    cashbook_entry_id bigint,
    ordering_company_id bigint,
    publisher_id bigint,
    genre_id bigint,
    music_book_id bigint,
    color_code_id bigint NOT NULL
);


ALTER TABLE tenant1.composition OWNER TO postgres;

--
-- TOC entry 502 (class 1259 OID 18716)
-- Name: composition_appointments; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.composition_appointments (
    appointments_id bigint NOT NULL,
    compositions_id bigint NOT NULL
);


ALTER TABLE tenant1.composition_appointments OWNER TO postgres;

--
-- TOC entry 500 (class 1259 OID 18705)
-- Name: composition_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.composition_id_seq OWNER TO postgres;

--
-- TOC entry 4551 (class 0 OID 0)
-- Dependencies: 500
-- Name: composition_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.composition_id_seq OWNED BY tenant1.composition.id;


--
-- TOC entry 534 (class 1259 OID 18870)
-- Name: customization; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.customization (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    data character varying(255) NOT NULL,
    text character varying(255)
);


ALTER TABLE tenant1.customization OWNER TO postgres;

--
-- TOC entry 533 (class 1259 OID 18868)
-- Name: customization_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.customization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.customization_id_seq OWNER TO postgres;

--
-- TOC entry 4552 (class 0 OID 0)
-- Dependencies: 533
-- Name: customization_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.customization_id_seq OWNED BY tenant1.customization.id;


--
-- TOC entry 431 (class 1259 OID 18336)
-- Name: databasechangelog; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255)
);


ALTER TABLE tenant1.databasechangelog OWNER TO postgres;

--
-- TOC entry 430 (class 1259 OID 18331)
-- Name: databasechangeloglock; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE tenant1.databasechangeloglock OWNER TO postgres;

--
-- TOC entry 539 (class 1259 OID 19249)
-- Name: email_verification_link; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.email_verification_link (
    id bigint NOT NULL,
    secret_key character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    validation_date timestamp without time zone,
    invalid boolean NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE tenant1.email_verification_link OWNER TO postgres;

--
-- TOC entry 538 (class 1259 OID 19247)
-- Name: email_verification_link_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.email_verification_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.email_verification_link_id_seq OWNER TO postgres;

--
-- TOC entry 4553 (class 0 OID 0)
-- Dependencies: 538
-- Name: email_verification_link_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.email_verification_link_id_seq OWNED BY tenant1.email_verification_link.id;


--
-- TOC entry 541 (class 1259 OID 19263)
-- Name: filter; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.filter (
    id bigint NOT NULL,
    list_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    filter character varying(255) NOT NULL
);


ALTER TABLE tenant1.filter OWNER TO postgres;

--
-- TOC entry 540 (class 1259 OID 19261)
-- Name: filter_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.filter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.filter_id_seq OWNER TO postgres;

--
-- TOC entry 4554 (class 0 OID 0)
-- Dependencies: 540
-- Name: filter_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.filter_id_seq OWNED BY tenant1.filter.id;


--
-- TOC entry 508 (class 1259 OID 18742)
-- Name: genre; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.genre (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.genre OWNER TO postgres;

--
-- TOC entry 507 (class 1259 OID 18740)
-- Name: genre_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.genre_id_seq OWNER TO postgres;

--
-- TOC entry 4555 (class 0 OID 0)
-- Dependencies: 507
-- Name: genre_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.genre_id_seq OWNED BY tenant1.genre.id;


--
-- TOC entry 432 (class 1259 OID 18342)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.hibernate_sequence
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 474 (class 1259 OID 18570)
-- Name: honor; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.honor (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant1.honor OWNER TO postgres;

--
-- TOC entry 473 (class 1259 OID 18568)
-- Name: honor_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.honor_id_seq OWNER TO postgres;

--
-- TOC entry 4556 (class 0 OID 0)
-- Dependencies: 473
-- Name: honor_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.honor_id_seq OWNED BY tenant1.honor.id;


--
-- TOC entry 462 (class 1259 OID 18518)
-- Name: instrument; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.instrument (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    purchase_date date,
    private_instrument boolean,
    price numeric(10,2),
    type_id bigint NOT NULL,
    cashbook_entry_id bigint
);


ALTER TABLE tenant1.instrument OWNER TO postgres;

--
-- TOC entry 461 (class 1259 OID 18516)
-- Name: instrument_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4557 (class 0 OID 0)
-- Dependencies: 461
-- Name: instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.instrument_id_seq OWNED BY tenant1.instrument.id;


--
-- TOC entry 466 (class 1259 OID 18536)
-- Name: instrument_service; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.instrument_service (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    cashbook_entry_id bigint,
    instrument_id bigint NOT NULL
);


ALTER TABLE tenant1.instrument_service OWNER TO postgres;

--
-- TOC entry 465 (class 1259 OID 18534)
-- Name: instrument_service_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.instrument_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.instrument_service_id_seq OWNER TO postgres;

--
-- TOC entry 4558 (class 0 OID 0)
-- Dependencies: 465
-- Name: instrument_service_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.instrument_service_id_seq OWNED BY tenant1.instrument_service.id;


--
-- TOC entry 464 (class 1259 OID 18528)
-- Name: instrument_type; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.instrument_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.instrument_type OWNER TO postgres;

--
-- TOC entry 463 (class 1259 OID 18526)
-- Name: instrument_type_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.instrument_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.instrument_type_id_seq OWNER TO postgres;

--
-- TOC entry 4559 (class 0 OID 0)
-- Dependencies: 463
-- Name: instrument_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.instrument_type_id_seq OWNED BY tenant1.instrument_type.id;


--
-- TOC entry 435 (class 1259 OID 18359)
-- Name: jhi_authority; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.jhi_authority (
    name character varying(50) NOT NULL,
    role_id bigint NOT NULL,
    id bigint NOT NULL,
    write boolean
);


ALTER TABLE tenant1.jhi_authority OWNER TO postgres;

--
-- TOC entry 537 (class 1259 OID 19215)
-- Name: jhi_authority_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.jhi_authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.jhi_authority_id_seq OWNER TO postgres;

--
-- TOC entry 4560 (class 0 OID 0)
-- Dependencies: 537
-- Name: jhi_authority_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.jhi_authority_id_seq OWNED BY tenant1.jhi_authority.id;


--
-- TOC entry 438 (class 1259 OID 18381)
-- Name: jhi_persistent_audit_event; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.jhi_persistent_audit_event (
    event_id bigint NOT NULL,
    principal character varying(50) NOT NULL,
    event_date timestamp without time zone,
    event_type character varying(255)
);


ALTER TABLE tenant1.jhi_persistent_audit_event OWNER TO postgres;

--
-- TOC entry 437 (class 1259 OID 18379)
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.jhi_persistent_audit_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.jhi_persistent_audit_event_event_id_seq OWNER TO postgres;

--
-- TOC entry 4561 (class 0 OID 0)
-- Dependencies: 437
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.jhi_persistent_audit_event_event_id_seq OWNED BY tenant1.jhi_persistent_audit_event.event_id;


--
-- TOC entry 439 (class 1259 OID 18387)
-- Name: jhi_persistent_audit_evt_data; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.jhi_persistent_audit_evt_data (
    event_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    value character varying(255)
);


ALTER TABLE tenant1.jhi_persistent_audit_evt_data OWNER TO postgres;

--
-- TOC entry 434 (class 1259 OID 18346)
-- Name: jhi_user; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.jhi_user (
    id bigint NOT NULL,
    login character varying(50) NOT NULL,
    password_hash character varying(60),
    first_name character varying(50),
    last_name character varying(50),
    email character varying(100),
    activated boolean NOT NULL,
    lang_key character varying(5),
    activation_key character varying(20),
    reset_key character varying(20),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    reset_date timestamp without time zone,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone
);


ALTER TABLE tenant1.jhi_user OWNER TO postgres;

--
-- TOC entry 436 (class 1259 OID 18364)
-- Name: jhi_user_authority; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.jhi_user_authority (
    user_id bigint NOT NULL,
    role_id bigint DEFAULT '2'::bigint NOT NULL
);


ALTER TABLE tenant1.jhi_user_authority OWNER TO postgres;

--
-- TOC entry 433 (class 1259 OID 18344)
-- Name: jhi_user_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.jhi_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.jhi_user_id_seq OWNER TO postgres;

--
-- TOC entry 4562 (class 0 OID 0)
-- Dependencies: 433
-- Name: jhi_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.jhi_user_id_seq OWNED BY tenant1.jhi_user.id;


--
-- TOC entry 526 (class 1259 OID 18830)
-- Name: letter; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.letter (
    id bigint NOT NULL,
    title character varying(255),
    text text,
    payment boolean,
    current_period boolean,
    previous_period boolean,
    template_id bigint
);


ALTER TABLE tenant1.letter OWNER TO postgres;

--
-- TOC entry 525 (class 1259 OID 18828)
-- Name: letter_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.letter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.letter_id_seq OWNER TO postgres;

--
-- TOC entry 4563 (class 0 OID 0)
-- Dependencies: 525
-- Name: letter_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.letter_id_seq OWNED BY tenant1.letter.id;


--
-- TOC entry 527 (class 1259 OID 18839)
-- Name: letter_person_group; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.letter_person_group (
    person_groups_id bigint NOT NULL,
    letters_id bigint NOT NULL
);


ALTER TABLE tenant1.letter_person_group OWNER TO postgres;

--
-- TOC entry 450 (class 1259 OID 18459)
-- Name: membership; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.membership (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    person_id bigint,
    membership_fee_id bigint
);


ALTER TABLE tenant1.membership OWNER TO postgres;

--
-- TOC entry 452 (class 1259 OID 18467)
-- Name: membership_fee; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.membership_fee (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    period character varying(255) NOT NULL,
    period_time_fraction integer
);


ALTER TABLE tenant1.membership_fee OWNER TO postgres;

--
-- TOC entry 454 (class 1259 OID 18478)
-- Name: membership_fee_amount; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.membership_fee_amount (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    amount numeric(10,2) NOT NULL,
    membership_fee_id bigint
);


ALTER TABLE tenant1.membership_fee_amount OWNER TO postgres;

--
-- TOC entry 453 (class 1259 OID 18476)
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.membership_fee_amount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.membership_fee_amount_id_seq OWNER TO postgres;

--
-- TOC entry 4564 (class 0 OID 0)
-- Dependencies: 453
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.membership_fee_amount_id_seq OWNED BY tenant1.membership_fee_amount.id;


--
-- TOC entry 456 (class 1259 OID 18486)
-- Name: membership_fee_for_period; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.membership_fee_for_period (
    id bigint NOT NULL,
    nr integer NOT NULL,
    jhi_year integer NOT NULL,
    reference_code character varying(255),
    paid boolean,
    note character varying(255),
    cashbook_entry_id bigint,
    membership_id bigint
);


ALTER TABLE tenant1.membership_fee_for_period OWNER TO postgres;

--
-- TOC entry 455 (class 1259 OID 18484)
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.membership_fee_for_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.membership_fee_for_period_id_seq OWNER TO postgres;

--
-- TOC entry 4565 (class 0 OID 0)
-- Dependencies: 455
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.membership_fee_for_period_id_seq OWNED BY tenant1.membership_fee_for_period.id;


--
-- TOC entry 451 (class 1259 OID 18465)
-- Name: membership_fee_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.membership_fee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.membership_fee_id_seq OWNER TO postgres;

--
-- TOC entry 4566 (class 0 OID 0)
-- Dependencies: 451
-- Name: membership_fee_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.membership_fee_id_seq OWNED BY tenant1.membership_fee.id;


--
-- TOC entry 449 (class 1259 OID 18457)
-- Name: membership_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.membership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.membership_id_seq OWNER TO postgres;

--
-- TOC entry 4567 (class 0 OID 0)
-- Dependencies: 449
-- Name: membership_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.membership_id_seq OWNED BY tenant1.membership.id;


--
-- TOC entry 506 (class 1259 OID 18731)
-- Name: music_book; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.music_book (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant1.music_book OWNER TO postgres;

--
-- TOC entry 505 (class 1259 OID 18729)
-- Name: music_book_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.music_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.music_book_id_seq OWNER TO postgres;

--
-- TOC entry 4568 (class 0 OID 0)
-- Dependencies: 505
-- Name: music_book_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.music_book_id_seq OWNED BY tenant1.music_book.id;


--
-- TOC entry 532 (class 1259 OID 18862)
-- Name: notification; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.notification (
    id bigint NOT NULL,
    entity_id bigint,
    created_at timestamp without time zone NOT NULL,
    read_by_user_id bigint,
    notification_rule_id bigint
);


ALTER TABLE tenant1.notification OWNER TO postgres;

--
-- TOC entry 531 (class 1259 OID 18860)
-- Name: notification_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.notification_id_seq OWNER TO postgres;

--
-- TOC entry 4569 (class 0 OID 0)
-- Dependencies: 531
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.notification_id_seq OWNED BY tenant1.notification.id;


--
-- TOC entry 529 (class 1259 OID 18846)
-- Name: notification_rule; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.notification_rule (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    entity character varying(255) NOT NULL,
    conditions text NOT NULL,
    active boolean NOT NULL,
    message_group character varying(255) NOT NULL,
    message_single character varying(255) NOT NULL
);


ALTER TABLE tenant1.notification_rule OWNER TO postgres;

--
-- TOC entry 528 (class 1259 OID 18844)
-- Name: notification_rule_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.notification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.notification_rule_id_seq OWNER TO postgres;

--
-- TOC entry 4570 (class 0 OID 0)
-- Dependencies: 528
-- Name: notification_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.notification_rule_id_seq OWNED BY tenant1.notification_rule.id;


--
-- TOC entry 530 (class 1259 OID 18855)
-- Name: notification_rule_target_audiences; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.notification_rule_target_audiences (
    notification_rules_id bigint NOT NULL,
    target_role_id bigint NOT NULL
);


ALTER TABLE tenant1.notification_rule_target_audiences OWNER TO postgres;

--
-- TOC entry 518 (class 1259 OID 18787)
-- Name: official_appointment; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.official_appointment (
    id bigint NOT NULL,
    organizer_name character varying(255) NOT NULL,
    organizer_address character varying(255) NOT NULL,
    is_head_quota boolean NOT NULL,
    report_id bigint
);


ALTER TABLE tenant1.official_appointment OWNER TO postgres;

--
-- TOC entry 517 (class 1259 OID 18785)
-- Name: official_appointment_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.official_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.official_appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4571 (class 0 OID 0)
-- Dependencies: 517
-- Name: official_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.official_appointment_id_seq OWNED BY tenant1.official_appointment.id;


--
-- TOC entry 441 (class 1259 OID 18401)
-- Name: person; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    birth_date date,
    email character varying(255),
    telephone_number character varying(255),
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    gender character varying(255),
    email_type character varying(255) DEFAULT 'MANUALLY'::character varying NOT NULL
);


ALTER TABLE tenant1.person OWNER TO postgres;

--
-- TOC entry 482 (class 1259 OID 18608)
-- Name: person_address; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_address (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date NOT NULL,
    address_id bigint,
    person_id bigint
);


ALTER TABLE tenant1.person_address OWNER TO postgres;

--
-- TOC entry 481 (class 1259 OID 18606)
-- Name: person_address_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_address_id_seq OWNER TO postgres;

--
-- TOC entry 4572 (class 0 OID 0)
-- Dependencies: 481
-- Name: person_address_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_address_id_seq OWNED BY tenant1.person_address.id;


--
-- TOC entry 480 (class 1259 OID 18600)
-- Name: person_award; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_award (
    id bigint NOT NULL,
    date date NOT NULL,
    award_id bigint,
    person_id bigint
);


ALTER TABLE tenant1.person_award OWNER TO postgres;

--
-- TOC entry 479 (class 1259 OID 18598)
-- Name: person_award_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_award_id_seq OWNER TO postgres;

--
-- TOC entry 4573 (class 0 OID 0)
-- Dependencies: 479
-- Name: person_award_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_award_id_seq OWNED BY tenant1.person_award.id;


--
-- TOC entry 468 (class 1259 OID 18546)
-- Name: person_clothing; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_clothing (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    clothing_id bigint,
    person_id bigint
);


ALTER TABLE tenant1.person_clothing OWNER TO postgres;

--
-- TOC entry 467 (class 1259 OID 18544)
-- Name: person_clothing_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4574 (class 0 OID 0)
-- Dependencies: 467
-- Name: person_clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_clothing_id_seq OWNED BY tenant1.person_clothing.id;


--
-- TOC entry 444 (class 1259 OID 18417)
-- Name: person_group; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_group (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE tenant1.person_group OWNER TO postgres;

--
-- TOC entry 443 (class 1259 OID 18415)
-- Name: person_group_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_group_id_seq OWNER TO postgres;

--
-- TOC entry 4575 (class 0 OID 0)
-- Dependencies: 443
-- Name: person_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_group_id_seq OWNED BY tenant1.person_group.id;


--
-- TOC entry 478 (class 1259 OID 18592)
-- Name: person_honor; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_honor (
    id bigint NOT NULL,
    date date NOT NULL,
    honor_id bigint,
    person_id bigint
);


ALTER TABLE tenant1.person_honor OWNER TO postgres;

--
-- TOC entry 477 (class 1259 OID 18590)
-- Name: person_honor_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_honor_id_seq OWNER TO postgres;

--
-- TOC entry 4576 (class 0 OID 0)
-- Dependencies: 477
-- Name: person_honor_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_honor_id_seq OWNED BY tenant1.person_honor.id;


--
-- TOC entry 440 (class 1259 OID 18399)
-- Name: person_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_id_seq OWNER TO postgres;

--
-- TOC entry 4577 (class 0 OID 0)
-- Dependencies: 440
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_id_seq OWNED BY tenant1.person.id;


--
-- TOC entry 448 (class 1259 OID 18451)
-- Name: person_info; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_info (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.person_info OWNER TO postgres;

--
-- TOC entry 447 (class 1259 OID 18449)
-- Name: person_info_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_info_id_seq OWNER TO postgres;

--
-- TOC entry 4578 (class 0 OID 0)
-- Dependencies: 447
-- Name: person_info_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_info_id_seq OWNED BY tenant1.person_info.id;


--
-- TOC entry 460 (class 1259 OID 18510)
-- Name: person_instrument; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_instrument (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    instrument_id bigint,
    person_id bigint
);


ALTER TABLE tenant1.person_instrument OWNER TO postgres;

--
-- TOC entry 459 (class 1259 OID 18508)
-- Name: person_instrument_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.person_instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.person_instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4579 (class 0 OID 0)
-- Dependencies: 459
-- Name: person_instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.person_instrument_id_seq OWNED BY tenant1.person_instrument.id;


--
-- TOC entry 442 (class 1259 OID 18410)
-- Name: person_person_groups; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.person_person_groups (
    person_groups_id bigint NOT NULL,
    people_id bigint NOT NULL
);


ALTER TABLE tenant1.person_person_groups OWNER TO postgres;

--
-- TOC entry 491 (class 1259 OID 18654)
-- Name: receipt; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.receipt (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    title character varying(255) NOT NULL,
    file character varying(255),
    identifier character varying(255),
    company_id bigint,
    overwrite_identifier boolean DEFAULT false NOT NULL,
    initial_cashbook_entry_id bigint NOT NULL
);


ALTER TABLE tenant1.receipt OWNER TO postgres;

--
-- TOC entry 490 (class 1259 OID 18652)
-- Name: receipt_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.receipt_id_seq OWNER TO postgres;

--
-- TOC entry 4580 (class 0 OID 0)
-- Dependencies: 490
-- Name: receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.receipt_id_seq OWNED BY tenant1.receipt.id;


--
-- TOC entry 516 (class 1259 OID 18776)
-- Name: report; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.report (
    id bigint NOT NULL,
    generation_date timestamp without time zone NOT NULL,
    association_id character varying(255) NOT NULL,
    association_name character varying(255) NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL
);


ALTER TABLE tenant1.report OWNER TO postgres;

--
-- TOC entry 515 (class 1259 OID 18774)
-- Name: report_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.report_id_seq OWNER TO postgres;

--
-- TOC entry 4581 (class 0 OID 0)
-- Dependencies: 515
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.report_id_seq OWNED BY tenant1.report.id;


--
-- TOC entry 446 (class 1259 OID 18438)
-- Name: role; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.role (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant1.role OWNER TO postgres;

--
-- TOC entry 445 (class 1259 OID 18436)
-- Name: role_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.role_id_seq OWNER TO postgres;

--
-- TOC entry 4582 (class 0 OID 0)
-- Dependencies: 445
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.role_id_seq OWNED BY tenant1.role.id;


--
-- TOC entry 524 (class 1259 OID 18819)
-- Name: template; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.template (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(255),
    margin_left real,
    margin_right real,
    margin_top real,
    margin_bottom real
);


ALTER TABLE tenant1.template OWNER TO postgres;

--
-- TOC entry 523 (class 1259 OID 18817)
-- Name: template_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.template_id_seq OWNER TO postgres;

--
-- TOC entry 4583 (class 0 OID 0)
-- Dependencies: 523
-- Name: template_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.template_id_seq OWNED BY tenant1.template.id;


--
-- TOC entry 545 (class 1259 OID 19285)
-- Name: user_column_config; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.user_column_config (
    id bigint NOT NULL,
    visible boolean NOT NULL,
    "position" integer NOT NULL,
    column_config_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE tenant1.user_column_config OWNER TO postgres;

--
-- TOC entry 544 (class 1259 OID 19283)
-- Name: user_column_config_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.user_column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.user_column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4584 (class 0 OID 0)
-- Dependencies: 544
-- Name: user_column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.user_column_config_id_seq OWNED BY tenant1.user_column_config.id;


--
-- TOC entry 342 (class 1259 OID 17528)
-- Name: address; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.address (
    id bigint NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL
);


ALTER TABLE tenant2.address OWNER TO postgres;

--
-- TOC entry 341 (class 1259 OID 17526)
-- Name: address_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.address_id_seq OWNER TO postgres;

--
-- TOC entry 4585 (class 0 OID 0)
-- Dependencies: 341
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.address_id_seq OWNED BY tenant2.address.id;


--
-- TOC entry 380 (class 1259 OID 17710)
-- Name: appointment; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.appointment (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    begin_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    official_appointment_id bigint,
    type_id bigint
);


ALTER TABLE tenant2.appointment OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 17708)
-- Name: appointment_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4586 (class 0 OID 0)
-- Dependencies: 379
-- Name: appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.appointment_id_seq OWNED BY tenant2.appointment.id;


--
-- TOC entry 381 (class 1259 OID 17721)
-- Name: appointment_persons; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.appointment_persons (
    persons_id bigint NOT NULL,
    appointments_id bigint NOT NULL
);


ALTER TABLE tenant2.appointment_persons OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 17728)
-- Name: appointment_type; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.appointment_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    is_official boolean NOT NULL,
    person_group_id bigint
);


ALTER TABLE tenant2.appointment_type OWNER TO postgres;

--
-- TOC entry 382 (class 1259 OID 17726)
-- Name: appointment_type_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.appointment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.appointment_type_id_seq OWNER TO postgres;

--
-- TOC entry 4587 (class 0 OID 0)
-- Dependencies: 382
-- Name: appointment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.appointment_type_id_seq OWNED BY tenant2.appointment_type.id;


--
-- TOC entry 397 (class 1259 OID 17792)
-- Name: arranger; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.arranger (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.arranger OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 17798)
-- Name: arranger_compositions; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.arranger_compositions (
    compositions_id bigint NOT NULL,
    arrangers_id bigint NOT NULL
);


ALTER TABLE tenant2.arranger_compositions OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 17790)
-- Name: arranger_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.arranger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.arranger_id_seq OWNER TO postgres;

--
-- TOC entry 4588 (class 0 OID 0)
-- Dependencies: 396
-- Name: arranger_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.arranger_id_seq OWNED BY tenant2.arranger.id;


--
-- TOC entry 360 (class 1259 OID 17610)
-- Name: award; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.award (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    descripton character varying(255)
);


ALTER TABLE tenant2.award OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 17608)
-- Name: award_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.award_id_seq OWNER TO postgres;

--
-- TOC entry 4589 (class 0 OID 0)
-- Dependencies: 359
-- Name: award_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.award_id_seq OWNED BY tenant2.award.id;


--
-- TOC entry 406 (class 1259 OID 17835)
-- Name: bank_import_data; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.bank_import_data (
    id bigint NOT NULL,
    entry_date date NOT NULL,
    entry_value numeric(10,2) NOT NULL,
    entry_text character varying(255),
    partner_name character varying(255),
    entry_reference character varying(255) NOT NULL,
    bank_importer_type character varying(255) NOT NULL,
    cashbook_entry_id bigint,
    cashbook_category_id bigint
);


ALTER TABLE tenant2.bank_import_data OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 17833)
-- Name: bank_import_data_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.bank_import_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.bank_import_data_id_seq OWNER TO postgres;

--
-- TOC entry 4590 (class 0 OID 0)
-- Dependencies: 405
-- Name: bank_import_data_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.bank_import_data_id_seq OWNED BY tenant2.bank_import_data.id;


--
-- TOC entry 368 (class 1259 OID 17645)
-- Name: cashbook; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.cashbook (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant2.cashbook OWNER TO postgres;

--
-- TOC entry 420 (class 1259 OID 17910)
-- Name: cashbook_account; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.cashbook_account (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    bank_account boolean,
    bank_importer_type character varying(255),
    receipt_code character varying(255) NOT NULL,
    current_number integer DEFAULT 0 NOT NULL
);


ALTER TABLE tenant2.cashbook_account OWNER TO postgres;

--
-- TOC entry 419 (class 1259 OID 17908)
-- Name: cashbook_account_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.cashbook_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.cashbook_account_id_seq OWNER TO postgres;

--
-- TOC entry 4591 (class 0 OID 0)
-- Dependencies: 419
-- Name: cashbook_account_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.cashbook_account_id_seq OWNED BY tenant2.cashbook_account.id;


--
-- TOC entry 369 (class 1259 OID 17654)
-- Name: cashbook_cashbook_entries; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.cashbook_cashbook_entries (
    cashbook_entries_id bigint NOT NULL,
    cashbooks_id bigint NOT NULL
);


ALTER TABLE tenant2.cashbook_cashbook_entries OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 17672)
-- Name: cashbook_category; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.cashbook_category (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    color character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE tenant2.cashbook_category OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 17670)
-- Name: cashbook_category_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.cashbook_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.cashbook_category_id_seq OWNER TO postgres;

--
-- TOC entry 4592 (class 0 OID 0)
-- Dependencies: 372
-- Name: cashbook_category_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.cashbook_category_id_seq OWNED BY tenant2.cashbook_category.id;


--
-- TOC entry 371 (class 1259 OID 17661)
-- Name: cashbook_entry; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.cashbook_entry (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    date date NOT NULL,
    amount numeric(10,2) NOT NULL,
    appointment_id bigint,
    cashbook_category_id bigint,
    receipt_id bigint,
    cashbook_account_id bigint
);


ALTER TABLE tenant2.cashbook_entry OWNER TO postgres;

--
-- TOC entry 370 (class 1259 OID 17659)
-- Name: cashbook_entry_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.cashbook_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.cashbook_entry_id_seq OWNER TO postgres;

--
-- TOC entry 4593 (class 0 OID 0)
-- Dependencies: 370
-- Name: cashbook_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.cashbook_entry_id_seq OWNED BY tenant2.cashbook_entry.id;


--
-- TOC entry 367 (class 1259 OID 17643)
-- Name: cashbook_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.cashbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.cashbook_id_seq OWNER TO postgres;

--
-- TOC entry 4594 (class 0 OID 0)
-- Dependencies: 367
-- Name: cashbook_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.cashbook_id_seq OWNED BY tenant2.cashbook.id;


--
-- TOC entry 354 (class 1259 OID 17583)
-- Name: clothing; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.clothing (
    id bigint NOT NULL,
    size character varying(255),
    purchase_date date,
    not_available boolean,
    type_id bigint
);


ALTER TABLE tenant2.clothing OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 17827)
-- Name: clothing_group; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.clothing_group (
    id bigint NOT NULL,
    size character varying(255),
    quantity integer,
    quantity_available integer,
    type_id bigint
);


ALTER TABLE tenant2.clothing_group OWNER TO postgres;

--
-- TOC entry 403 (class 1259 OID 17825)
-- Name: clothing_group_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.clothing_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.clothing_group_id_seq OWNER TO postgres;

--
-- TOC entry 4595 (class 0 OID 0)
-- Dependencies: 403
-- Name: clothing_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.clothing_group_id_seq OWNED BY tenant2.clothing_group.id;


--
-- TOC entry 353 (class 1259 OID 17581)
-- Name: clothing_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4596 (class 0 OID 0)
-- Dependencies: 353
-- Name: clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.clothing_id_seq OWNED BY tenant2.clothing.id;


--
-- TOC entry 356 (class 1259 OID 17591)
-- Name: clothing_type; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.clothing_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.clothing_type OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 17589)
-- Name: clothing_type_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.clothing_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.clothing_type_id_seq OWNER TO postgres;

--
-- TOC entry 4597 (class 0 OID 0)
-- Dependencies: 355
-- Name: clothing_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.clothing_type_id_seq OWNED BY tenant2.clothing_type.id;


--
-- TOC entry 388 (class 1259 OID 17752)
-- Name: color_code; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.color_code (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.color_code OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 17750)
-- Name: color_code_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.color_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.color_code_id_seq OWNER TO postgres;

--
-- TOC entry 4598 (class 0 OID 0)
-- Dependencies: 387
-- Name: color_code_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.color_code_id_seq OWNED BY tenant2.color_code.id;


--
-- TOC entry 427 (class 1259 OID 18303)
-- Name: column_config; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.column_config (
    id bigint NOT NULL,
    entity character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    default_visible boolean NOT NULL,
    default_position integer NOT NULL,
    column_display_name character varying(255) NOT NULL,
    sort_by_name character varying(255) NOT NULL
);


ALTER TABLE tenant2.column_config OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 18301)
-- Name: column_config_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4599 (class 0 OID 0)
-- Dependencies: 426
-- Name: column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.column_config_id_seq OWNED BY tenant2.column_config.id;


--
-- TOC entry 377 (class 1259 OID 17694)
-- Name: company; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.company (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    recipient character varying(255)
);


ALTER TABLE tenant2.company OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 17692)
-- Name: company_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.company_id_seq OWNER TO postgres;

--
-- TOC entry 4600 (class 0 OID 0)
-- Dependencies: 376
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.company_id_seq OWNED BY tenant2.company.id;


--
-- TOC entry 378 (class 1259 OID 17703)
-- Name: company_person_groups; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.company_person_groups (
    person_groups_id bigint NOT NULL,
    companies_id bigint NOT NULL
);


ALTER TABLE tenant2.company_person_groups OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 17779)
-- Name: composer; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.composer (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.composer OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 17785)
-- Name: composer_compositions; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.composer_compositions (
    compositions_id bigint NOT NULL,
    composers_id bigint NOT NULL
);


ALTER TABLE tenant2.composer_compositions OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 17777)
-- Name: composer_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.composer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.composer_id_seq OWNER TO postgres;

--
-- TOC entry 4601 (class 0 OID 0)
-- Dependencies: 393
-- Name: composer_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.composer_id_seq OWNED BY tenant2.composer.id;


--
-- TOC entry 385 (class 1259 OID 17736)
-- Name: composition; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.composition (
    id bigint NOT NULL,
    nr integer NOT NULL,
    title character varying(255) NOT NULL,
    note character varying(255),
    akm_composition_nr integer NOT NULL,
    cashbook_entry_id bigint,
    ordering_company_id bigint,
    publisher_id bigint,
    genre_id bigint,
    music_book_id bigint,
    color_code_id bigint NOT NULL
);


ALTER TABLE tenant2.composition OWNER TO postgres;

--
-- TOC entry 386 (class 1259 OID 17745)
-- Name: composition_appointments; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.composition_appointments (
    appointments_id bigint NOT NULL,
    compositions_id bigint NOT NULL
);


ALTER TABLE tenant2.composition_appointments OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 17734)
-- Name: composition_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.composition_id_seq OWNER TO postgres;

--
-- TOC entry 4602 (class 0 OID 0)
-- Dependencies: 384
-- Name: composition_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.composition_id_seq OWNED BY tenant2.composition.id;


--
-- TOC entry 418 (class 1259 OID 17899)
-- Name: customization; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.customization (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    data character varying(255) NOT NULL,
    text character varying(255)
);


ALTER TABLE tenant2.customization OWNER TO postgres;

--
-- TOC entry 417 (class 1259 OID 17897)
-- Name: customization_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.customization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.customization_id_seq OWNER TO postgres;

--
-- TOC entry 4603 (class 0 OID 0)
-- Dependencies: 417
-- Name: customization_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.customization_id_seq OWNED BY tenant2.customization.id;


--
-- TOC entry 315 (class 1259 OID 17365)
-- Name: databasechangelog; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255)
);


ALTER TABLE tenant2.databasechangelog OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 17360)
-- Name: databasechangeloglock; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE tenant2.databasechangeloglock OWNER TO postgres;

--
-- TOC entry 423 (class 1259 OID 18278)
-- Name: email_verification_link; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.email_verification_link (
    id bigint NOT NULL,
    secret_key character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    validation_date timestamp without time zone,
    invalid boolean NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE tenant2.email_verification_link OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 18276)
-- Name: email_verification_link_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.email_verification_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.email_verification_link_id_seq OWNER TO postgres;

--
-- TOC entry 4604 (class 0 OID 0)
-- Dependencies: 422
-- Name: email_verification_link_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.email_verification_link_id_seq OWNED BY tenant2.email_verification_link.id;


--
-- TOC entry 425 (class 1259 OID 18292)
-- Name: filter; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.filter (
    id bigint NOT NULL,
    list_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    filter character varying(255) NOT NULL
);


ALTER TABLE tenant2.filter OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 18290)
-- Name: filter_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.filter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.filter_id_seq OWNER TO postgres;

--
-- TOC entry 4605 (class 0 OID 0)
-- Dependencies: 424
-- Name: filter_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.filter_id_seq OWNED BY tenant2.filter.id;


--
-- TOC entry 392 (class 1259 OID 17771)
-- Name: genre; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.genre (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.genre OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 17769)
-- Name: genre_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.genre_id_seq OWNER TO postgres;

--
-- TOC entry 4606 (class 0 OID 0)
-- Dependencies: 391
-- Name: genre_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.genre_id_seq OWNED BY tenant2.genre.id;


--
-- TOC entry 316 (class 1259 OID 17371)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.hibernate_sequence
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 358 (class 1259 OID 17599)
-- Name: honor; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.honor (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant2.honor OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 17597)
-- Name: honor_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.honor_id_seq OWNER TO postgres;

--
-- TOC entry 4607 (class 0 OID 0)
-- Dependencies: 357
-- Name: honor_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.honor_id_seq OWNED BY tenant2.honor.id;


--
-- TOC entry 346 (class 1259 OID 17547)
-- Name: instrument; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.instrument (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    purchase_date date,
    private_instrument boolean,
    price numeric(10,2),
    type_id bigint NOT NULL,
    cashbook_entry_id bigint
);


ALTER TABLE tenant2.instrument OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 17545)
-- Name: instrument_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4608 (class 0 OID 0)
-- Dependencies: 345
-- Name: instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.instrument_id_seq OWNED BY tenant2.instrument.id;


--
-- TOC entry 350 (class 1259 OID 17565)
-- Name: instrument_service; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.instrument_service (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    cashbook_entry_id bigint,
    instrument_id bigint NOT NULL
);


ALTER TABLE tenant2.instrument_service OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 17563)
-- Name: instrument_service_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.instrument_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.instrument_service_id_seq OWNER TO postgres;

--
-- TOC entry 4609 (class 0 OID 0)
-- Dependencies: 349
-- Name: instrument_service_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.instrument_service_id_seq OWNED BY tenant2.instrument_service.id;


--
-- TOC entry 348 (class 1259 OID 17557)
-- Name: instrument_type; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.instrument_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.instrument_type OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 17555)
-- Name: instrument_type_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.instrument_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.instrument_type_id_seq OWNER TO postgres;

--
-- TOC entry 4610 (class 0 OID 0)
-- Dependencies: 347
-- Name: instrument_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.instrument_type_id_seq OWNED BY tenant2.instrument_type.id;


--
-- TOC entry 319 (class 1259 OID 17388)
-- Name: jhi_authority; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.jhi_authority (
    name character varying(50) NOT NULL,
    role_id bigint NOT NULL,
    id bigint NOT NULL,
    write boolean
);


ALTER TABLE tenant2.jhi_authority OWNER TO postgres;

--
-- TOC entry 421 (class 1259 OID 18244)
-- Name: jhi_authority_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.jhi_authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.jhi_authority_id_seq OWNER TO postgres;

--
-- TOC entry 4611 (class 0 OID 0)
-- Dependencies: 421
-- Name: jhi_authority_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.jhi_authority_id_seq OWNED BY tenant2.jhi_authority.id;


--
-- TOC entry 322 (class 1259 OID 17410)
-- Name: jhi_persistent_audit_event; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.jhi_persistent_audit_event (
    event_id bigint NOT NULL,
    principal character varying(50) NOT NULL,
    event_date timestamp without time zone,
    event_type character varying(255)
);


ALTER TABLE tenant2.jhi_persistent_audit_event OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 17408)
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.jhi_persistent_audit_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.jhi_persistent_audit_event_event_id_seq OWNER TO postgres;

--
-- TOC entry 4612 (class 0 OID 0)
-- Dependencies: 321
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.jhi_persistent_audit_event_event_id_seq OWNED BY tenant2.jhi_persistent_audit_event.event_id;


--
-- TOC entry 323 (class 1259 OID 17416)
-- Name: jhi_persistent_audit_evt_data; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.jhi_persistent_audit_evt_data (
    event_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    value character varying(255)
);


ALTER TABLE tenant2.jhi_persistent_audit_evt_data OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 17375)
-- Name: jhi_user; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.jhi_user (
    id bigint NOT NULL,
    login character varying(50) NOT NULL,
    password_hash character varying(60),
    first_name character varying(50),
    last_name character varying(50),
    email character varying(100),
    activated boolean NOT NULL,
    lang_key character varying(5),
    activation_key character varying(20),
    reset_key character varying(20),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    reset_date timestamp without time zone,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone
);


ALTER TABLE tenant2.jhi_user OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 17393)
-- Name: jhi_user_authority; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.jhi_user_authority (
    user_id bigint NOT NULL,
    role_id bigint DEFAULT '2'::bigint NOT NULL
);


ALTER TABLE tenant2.jhi_user_authority OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 17373)
-- Name: jhi_user_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.jhi_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.jhi_user_id_seq OWNER TO postgres;

--
-- TOC entry 4613 (class 0 OID 0)
-- Dependencies: 317
-- Name: jhi_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.jhi_user_id_seq OWNED BY tenant2.jhi_user.id;


--
-- TOC entry 410 (class 1259 OID 17859)
-- Name: letter; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.letter (
    id bigint NOT NULL,
    title character varying(255),
    text text,
    payment boolean,
    current_period boolean,
    previous_period boolean,
    template_id bigint
);


ALTER TABLE tenant2.letter OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 17857)
-- Name: letter_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.letter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.letter_id_seq OWNER TO postgres;

--
-- TOC entry 4614 (class 0 OID 0)
-- Dependencies: 409
-- Name: letter_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.letter_id_seq OWNED BY tenant2.letter.id;


--
-- TOC entry 411 (class 1259 OID 17868)
-- Name: letter_person_group; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.letter_person_group (
    person_groups_id bigint NOT NULL,
    letters_id bigint NOT NULL
);


ALTER TABLE tenant2.letter_person_group OWNER TO postgres;

--
-- TOC entry 334 (class 1259 OID 17488)
-- Name: membership; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.membership (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    person_id bigint,
    membership_fee_id bigint
);


ALTER TABLE tenant2.membership OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 17496)
-- Name: membership_fee; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.membership_fee (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    period character varying(255) NOT NULL,
    period_time_fraction integer
);


ALTER TABLE tenant2.membership_fee OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 17507)
-- Name: membership_fee_amount; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.membership_fee_amount (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    amount numeric(10,2) NOT NULL,
    membership_fee_id bigint
);


ALTER TABLE tenant2.membership_fee_amount OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 17505)
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.membership_fee_amount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.membership_fee_amount_id_seq OWNER TO postgres;

--
-- TOC entry 4615 (class 0 OID 0)
-- Dependencies: 337
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.membership_fee_amount_id_seq OWNED BY tenant2.membership_fee_amount.id;


--
-- TOC entry 340 (class 1259 OID 17515)
-- Name: membership_fee_for_period; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.membership_fee_for_period (
    id bigint NOT NULL,
    nr integer NOT NULL,
    jhi_year integer NOT NULL,
    reference_code character varying(255),
    paid boolean,
    note character varying(255),
    cashbook_entry_id bigint,
    membership_id bigint
);


ALTER TABLE tenant2.membership_fee_for_period OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 17513)
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.membership_fee_for_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.membership_fee_for_period_id_seq OWNER TO postgres;

--
-- TOC entry 4616 (class 0 OID 0)
-- Dependencies: 339
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.membership_fee_for_period_id_seq OWNED BY tenant2.membership_fee_for_period.id;


--
-- TOC entry 335 (class 1259 OID 17494)
-- Name: membership_fee_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.membership_fee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.membership_fee_id_seq OWNER TO postgres;

--
-- TOC entry 4617 (class 0 OID 0)
-- Dependencies: 335
-- Name: membership_fee_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.membership_fee_id_seq OWNED BY tenant2.membership_fee.id;


--
-- TOC entry 333 (class 1259 OID 17486)
-- Name: membership_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.membership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.membership_id_seq OWNER TO postgres;

--
-- TOC entry 4618 (class 0 OID 0)
-- Dependencies: 333
-- Name: membership_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.membership_id_seq OWNED BY tenant2.membership.id;


--
-- TOC entry 390 (class 1259 OID 17760)
-- Name: music_book; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.music_book (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE tenant2.music_book OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 17758)
-- Name: music_book_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.music_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.music_book_id_seq OWNER TO postgres;

--
-- TOC entry 4619 (class 0 OID 0)
-- Dependencies: 389
-- Name: music_book_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.music_book_id_seq OWNED BY tenant2.music_book.id;


--
-- TOC entry 416 (class 1259 OID 17891)
-- Name: notification; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.notification (
    id bigint NOT NULL,
    entity_id bigint,
    created_at timestamp without time zone NOT NULL,
    read_by_user_id bigint,
    notification_rule_id bigint
);


ALTER TABLE tenant2.notification OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 17889)
-- Name: notification_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.notification_id_seq OWNER TO postgres;

--
-- TOC entry 4620 (class 0 OID 0)
-- Dependencies: 415
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.notification_id_seq OWNED BY tenant2.notification.id;


--
-- TOC entry 413 (class 1259 OID 17875)
-- Name: notification_rule; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.notification_rule (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    entity character varying(255) NOT NULL,
    conditions text NOT NULL,
    active boolean NOT NULL,
    message_group character varying(255) NOT NULL,
    message_single character varying(255) NOT NULL
);


ALTER TABLE tenant2.notification_rule OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 17873)
-- Name: notification_rule_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.notification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.notification_rule_id_seq OWNER TO postgres;

--
-- TOC entry 4621 (class 0 OID 0)
-- Dependencies: 412
-- Name: notification_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.notification_rule_id_seq OWNED BY tenant2.notification_rule.id;


--
-- TOC entry 414 (class 1259 OID 17884)
-- Name: notification_rule_target_audiences; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.notification_rule_target_audiences (
    notification_rules_id bigint NOT NULL,
    target_role_id bigint NOT NULL
);


ALTER TABLE tenant2.notification_rule_target_audiences OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 17816)
-- Name: official_appointment; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.official_appointment (
    id bigint NOT NULL,
    organizer_name character varying(255) NOT NULL,
    organizer_address character varying(255) NOT NULL,
    is_head_quota boolean NOT NULL,
    report_id bigint
);


ALTER TABLE tenant2.official_appointment OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 17814)
-- Name: official_appointment_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.official_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.official_appointment_id_seq OWNER TO postgres;

--
-- TOC entry 4622 (class 0 OID 0)
-- Dependencies: 401
-- Name: official_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.official_appointment_id_seq OWNED BY tenant2.official_appointment.id;


--
-- TOC entry 325 (class 1259 OID 17430)
-- Name: person; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    birth_date date,
    email character varying(255),
    telephone_number character varying(255),
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    gender character varying(255),
    email_type character varying(255) DEFAULT 'MANUALLY'::character varying NOT NULL
);


ALTER TABLE tenant2.person OWNER TO postgres;

--
-- TOC entry 366 (class 1259 OID 17637)
-- Name: person_address; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_address (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date NOT NULL,
    address_id bigint,
    person_id bigint
);


ALTER TABLE tenant2.person_address OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 17635)
-- Name: person_address_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_address_id_seq OWNER TO postgres;

--
-- TOC entry 4623 (class 0 OID 0)
-- Dependencies: 365
-- Name: person_address_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_address_id_seq OWNED BY tenant2.person_address.id;


--
-- TOC entry 364 (class 1259 OID 17629)
-- Name: person_award; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_award (
    id bigint NOT NULL,
    date date NOT NULL,
    award_id bigint,
    person_id bigint
);


ALTER TABLE tenant2.person_award OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 17627)
-- Name: person_award_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_award_id_seq OWNER TO postgres;

--
-- TOC entry 4624 (class 0 OID 0)
-- Dependencies: 363
-- Name: person_award_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_award_id_seq OWNED BY tenant2.person_award.id;


--
-- TOC entry 352 (class 1259 OID 17575)
-- Name: person_clothing; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_clothing (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    clothing_id bigint,
    person_id bigint
);


ALTER TABLE tenant2.person_clothing OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 17573)
-- Name: person_clothing_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_clothing_id_seq OWNER TO postgres;

--
-- TOC entry 4625 (class 0 OID 0)
-- Dependencies: 351
-- Name: person_clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_clothing_id_seq OWNED BY tenant2.person_clothing.id;


--
-- TOC entry 328 (class 1259 OID 17446)
-- Name: person_group; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_group (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE tenant2.person_group OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 17444)
-- Name: person_group_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_group_id_seq OWNER TO postgres;

--
-- TOC entry 4626 (class 0 OID 0)
-- Dependencies: 327
-- Name: person_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_group_id_seq OWNED BY tenant2.person_group.id;


--
-- TOC entry 362 (class 1259 OID 17621)
-- Name: person_honor; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_honor (
    id bigint NOT NULL,
    date date NOT NULL,
    honor_id bigint,
    person_id bigint
);


ALTER TABLE tenant2.person_honor OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 17619)
-- Name: person_honor_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_honor_id_seq OWNER TO postgres;

--
-- TOC entry 4627 (class 0 OID 0)
-- Dependencies: 361
-- Name: person_honor_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_honor_id_seq OWNED BY tenant2.person_honor.id;


--
-- TOC entry 324 (class 1259 OID 17428)
-- Name: person_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_id_seq OWNER TO postgres;

--
-- TOC entry 4628 (class 0 OID 0)
-- Dependencies: 324
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_id_seq OWNED BY tenant2.person.id;


--
-- TOC entry 332 (class 1259 OID 17480)
-- Name: person_info; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_info (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.person_info OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 17478)
-- Name: person_info_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_info_id_seq OWNER TO postgres;

--
-- TOC entry 4629 (class 0 OID 0)
-- Dependencies: 331
-- Name: person_info_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_info_id_seq OWNED BY tenant2.person_info.id;


--
-- TOC entry 344 (class 1259 OID 17539)
-- Name: person_instrument; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_instrument (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    instrument_id bigint,
    person_id bigint
);


ALTER TABLE tenant2.person_instrument OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 17537)
-- Name: person_instrument_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.person_instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.person_instrument_id_seq OWNER TO postgres;

--
-- TOC entry 4630 (class 0 OID 0)
-- Dependencies: 343
-- Name: person_instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.person_instrument_id_seq OWNED BY tenant2.person_instrument.id;


--
-- TOC entry 326 (class 1259 OID 17439)
-- Name: person_person_groups; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.person_person_groups (
    person_groups_id bigint NOT NULL,
    people_id bigint NOT NULL
);


ALTER TABLE tenant2.person_person_groups OWNER TO postgres;

--
-- TOC entry 375 (class 1259 OID 17683)
-- Name: receipt; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.receipt (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    title character varying(255) NOT NULL,
    file character varying(255),
    identifier character varying(255),
    company_id bigint,
    overwrite_identifier boolean DEFAULT false NOT NULL,
    initial_cashbook_entry_id bigint NOT NULL
);


ALTER TABLE tenant2.receipt OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 17681)
-- Name: receipt_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.receipt_id_seq OWNER TO postgres;

--
-- TOC entry 4631 (class 0 OID 0)
-- Dependencies: 374
-- Name: receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.receipt_id_seq OWNED BY tenant2.receipt.id;


--
-- TOC entry 400 (class 1259 OID 17805)
-- Name: report; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.report (
    id bigint NOT NULL,
    generation_date timestamp without time zone NOT NULL,
    association_id character varying(255) NOT NULL,
    association_name character varying(255) NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL
);


ALTER TABLE tenant2.report OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 17803)
-- Name: report_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.report_id_seq OWNER TO postgres;

--
-- TOC entry 4632 (class 0 OID 0)
-- Dependencies: 399
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.report_id_seq OWNED BY tenant2.report.id;


--
-- TOC entry 330 (class 1259 OID 17467)
-- Name: role; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.role (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE tenant2.role OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 17465)
-- Name: role_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.role_id_seq OWNER TO postgres;

--
-- TOC entry 4633 (class 0 OID 0)
-- Dependencies: 329
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.role_id_seq OWNED BY tenant2.role.id;


--
-- TOC entry 408 (class 1259 OID 17848)
-- Name: template; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.template (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(255),
    margin_left real,
    margin_right real,
    margin_top real,
    margin_bottom real
);


ALTER TABLE tenant2.template OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 17846)
-- Name: template_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.template_id_seq OWNER TO postgres;

--
-- TOC entry 4634 (class 0 OID 0)
-- Dependencies: 407
-- Name: template_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.template_id_seq OWNED BY tenant2.template.id;


--
-- TOC entry 429 (class 1259 OID 18314)
-- Name: user_column_config; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.user_column_config (
    id bigint NOT NULL,
    visible boolean NOT NULL,
    "position" integer NOT NULL,
    column_config_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE tenant2.user_column_config OWNER TO postgres;

--
-- TOC entry 428 (class 1259 OID 18312)
-- Name: user_column_config_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.user_column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.user_column_config_id_seq OWNER TO postgres;

--
-- TOC entry 4635 (class 0 OID 0)
-- Dependencies: 428
-- Name: user_column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.user_column_config_id_seq OWNED BY tenant2.user_column_config.id;


--
-- TOC entry 3203 (class 2604 OID 16558)
-- Name: address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);


--
-- TOC entry 3222 (class 2604 OID 16740)
-- Name: appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment ALTER COLUMN id SET DEFAULT nextval('public.appointment_id_seq'::regclass);


--
-- TOC entry 3223 (class 2604 OID 16758)
-- Name: appointment_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type ALTER COLUMN id SET DEFAULT nextval('public.appointment_type_id_seq'::regclass);


--
-- TOC entry 3229 (class 2604 OID 16822)
-- Name: arranger id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger ALTER COLUMN id SET DEFAULT nextval('public.arranger_id_seq'::regclass);


--
-- TOC entry 3212 (class 2604 OID 16640)
-- Name: award id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.award ALTER COLUMN id SET DEFAULT nextval('public.award_id_seq'::regclass);


--
-- TOC entry 3233 (class 2604 OID 16865)
-- Name: bank_import_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data ALTER COLUMN id SET DEFAULT nextval('public.bank_import_data_id_seq'::regclass);


--
-- TOC entry 3216 (class 2604 OID 16675)
-- Name: cashbook id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook ALTER COLUMN id SET DEFAULT nextval('public.cashbook_id_seq'::regclass);


--
-- TOC entry 3239 (class 2604 OID 16940)
-- Name: cashbook_account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account ALTER COLUMN id SET DEFAULT nextval('public.cashbook_account_id_seq'::regclass);


--
-- TOC entry 3218 (class 2604 OID 16702)
-- Name: cashbook_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category ALTER COLUMN id SET DEFAULT nextval('public.cashbook_category_id_seq'::regclass);


--
-- TOC entry 3217 (class 2604 OID 16691)
-- Name: cashbook_entry id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry ALTER COLUMN id SET DEFAULT nextval('public.cashbook_entry_id_seq'::regclass);


--
-- TOC entry 3209 (class 2604 OID 16613)
-- Name: clothing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing ALTER COLUMN id SET DEFAULT nextval('public.clothing_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 16857)
-- Name: clothing_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group ALTER COLUMN id SET DEFAULT nextval('public.clothing_group_id_seq'::regclass);


--
-- TOC entry 3210 (class 2604 OID 16621)
-- Name: clothing_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_type ALTER COLUMN id SET DEFAULT nextval('public.clothing_type_id_seq'::regclass);


--
-- TOC entry 3225 (class 2604 OID 16782)
-- Name: color_code id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code ALTER COLUMN id SET DEFAULT nextval('public.color_code_id_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 17333)
-- Name: column_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.column_config ALTER COLUMN id SET DEFAULT nextval('public.column_config_id_seq'::regclass);


--
-- TOC entry 3221 (class 2604 OID 16724)
-- Name: company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);


--
-- TOC entry 3228 (class 2604 OID 16809)
-- Name: composer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer ALTER COLUMN id SET DEFAULT nextval('public.composer_id_seq'::regclass);


--
-- TOC entry 3224 (class 2604 OID 16766)
-- Name: composition id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition ALTER COLUMN id SET DEFAULT nextval('public.composition_id_seq'::regclass);


--
-- TOC entry 3238 (class 2604 OID 16929)
-- Name: customization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization ALTER COLUMN id SET DEFAULT nextval('public.customization_id_seq'::regclass);


--
-- TOC entry 3241 (class 2604 OID 17308)
-- Name: email_verification_link id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link ALTER COLUMN id SET DEFAULT nextval('public.email_verification_link_id_seq'::regclass);


--
-- TOC entry 3242 (class 2604 OID 17322)
-- Name: filter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter ALTER COLUMN id SET DEFAULT nextval('public.filter_id_seq'::regclass);


--
-- TOC entry 3227 (class 2604 OID 16801)
-- Name: genre id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre ALTER COLUMN id SET DEFAULT nextval('public.genre_id_seq'::regclass);


--
-- TOC entry 3211 (class 2604 OID 16629)
-- Name: honor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.honor ALTER COLUMN id SET DEFAULT nextval('public.honor_id_seq'::regclass);


--
-- TOC entry 3205 (class 2604 OID 16577)
-- Name: instrument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument ALTER COLUMN id SET DEFAULT nextval('public.instrument_id_seq'::regclass);


--
-- TOC entry 3207 (class 2604 OID 16595)
-- Name: instrument_service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service ALTER COLUMN id SET DEFAULT nextval('public.instrument_service_id_seq'::regclass);


--
-- TOC entry 3206 (class 2604 OID 16587)
-- Name: instrument_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_type ALTER COLUMN id SET DEFAULT nextval('public.instrument_type_id_seq'::regclass);


--
-- TOC entry 3191 (class 2604 OID 17273)
-- Name: jhi_authority id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_authority ALTER COLUMN id SET DEFAULT nextval('public.jhi_authority_id_seq'::regclass);


--
-- TOC entry 3193 (class 2604 OID 16440)
-- Name: jhi_persistent_audit_event event_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_event ALTER COLUMN event_id SET DEFAULT nextval('public.jhi_persistent_audit_event_event_id_seq'::regclass);


--
-- TOC entry 3190 (class 2604 OID 16405)
-- Name: jhi_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user ALTER COLUMN id SET DEFAULT nextval('public.jhi_user_id_seq'::regclass);


--
-- TOC entry 3235 (class 2604 OID 16889)
-- Name: letter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter ALTER COLUMN id SET DEFAULT nextval('public.letter_id_seq'::regclass);


--
-- TOC entry 3199 (class 2604 OID 16518)
-- Name: membership id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership ALTER COLUMN id SET DEFAULT nextval('public.membership_id_seq'::regclass);


--
-- TOC entry 3200 (class 2604 OID 16526)
-- Name: membership_fee id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_id_seq'::regclass);


--
-- TOC entry 3201 (class 2604 OID 16537)
-- Name: membership_fee_amount id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_amount_id_seq'::regclass);


--
-- TOC entry 3202 (class 2604 OID 16545)
-- Name: membership_fee_for_period id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_for_period_id_seq'::regclass);


--
-- TOC entry 3226 (class 2604 OID 16790)
-- Name: music_book id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.music_book ALTER COLUMN id SET DEFAULT nextval('public.music_book_id_seq'::regclass);


--
-- TOC entry 3237 (class 2604 OID 16921)
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- TOC entry 3236 (class 2604 OID 16905)
-- Name: notification_rule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule ALTER COLUMN id SET DEFAULT nextval('public.notification_rule_id_seq'::regclass);


--
-- TOC entry 3231 (class 2604 OID 16846)
-- Name: official_appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment ALTER COLUMN id SET DEFAULT nextval('public.official_appointment_id_seq'::regclass);


--
-- TOC entry 3194 (class 2604 OID 16460)
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- TOC entry 3215 (class 2604 OID 16667)
-- Name: person_address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address ALTER COLUMN id SET DEFAULT nextval('public.person_address_id_seq'::regclass);


--
-- TOC entry 3214 (class 2604 OID 16659)
-- Name: person_award id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award ALTER COLUMN id SET DEFAULT nextval('public.person_award_id_seq'::regclass);


--
-- TOC entry 3208 (class 2604 OID 16605)
-- Name: person_clothing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing ALTER COLUMN id SET DEFAULT nextval('public.person_clothing_id_seq'::regclass);


--
-- TOC entry 3196 (class 2604 OID 16476)
-- Name: person_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group ALTER COLUMN id SET DEFAULT nextval('public.person_group_id_seq'::regclass);


--
-- TOC entry 3213 (class 2604 OID 16651)
-- Name: person_honor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor ALTER COLUMN id SET DEFAULT nextval('public.person_honor_id_seq'::regclass);


--
-- TOC entry 3198 (class 2604 OID 16510)
-- Name: person_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_info ALTER COLUMN id SET DEFAULT nextval('public.person_info_id_seq'::regclass);


--
-- TOC entry 3204 (class 2604 OID 16569)
-- Name: person_instrument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument ALTER COLUMN id SET DEFAULT nextval('public.person_instrument_id_seq'::regclass);


--
-- TOC entry 3219 (class 2604 OID 16713)
-- Name: receipt id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt ALTER COLUMN id SET DEFAULT nextval('public.receipt_id_seq'::regclass);


--
-- TOC entry 3230 (class 2604 OID 16835)
-- Name: report id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- TOC entry 3197 (class 2604 OID 16497)
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- TOC entry 3234 (class 2604 OID 16878)
-- Name: template id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.template ALTER COLUMN id SET DEFAULT nextval('public.template_id_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 17344)
-- Name: user_column_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config ALTER COLUMN id SET DEFAULT nextval('public.user_column_config_id_seq'::regclass);


--
-- TOC entry 3313 (class 2604 OID 18502)
-- Name: address id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.address ALTER COLUMN id SET DEFAULT nextval('tenant1.address_id_seq'::regclass);


--
-- TOC entry 3332 (class 2604 OID 18684)
-- Name: appointment id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment ALTER COLUMN id SET DEFAULT nextval('tenant1.appointment_id_seq'::regclass);


--
-- TOC entry 3333 (class 2604 OID 18702)
-- Name: appointment_type id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_type ALTER COLUMN id SET DEFAULT nextval('tenant1.appointment_type_id_seq'::regclass);


--
-- TOC entry 3339 (class 2604 OID 18766)
-- Name: arranger id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.arranger ALTER COLUMN id SET DEFAULT nextval('tenant1.arranger_id_seq'::regclass);


--
-- TOC entry 3322 (class 2604 OID 18584)
-- Name: award id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.award ALTER COLUMN id SET DEFAULT nextval('tenant1.award_id_seq'::regclass);


--
-- TOC entry 3343 (class 2604 OID 18809)
-- Name: bank_import_data id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.bank_import_data ALTER COLUMN id SET DEFAULT nextval('tenant1.bank_import_data_id_seq'::regclass);


--
-- TOC entry 3326 (class 2604 OID 18619)
-- Name: cashbook id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook ALTER COLUMN id SET DEFAULT nextval('tenant1.cashbook_id_seq'::regclass);


--
-- TOC entry 3349 (class 2604 OID 18884)
-- Name: cashbook_account id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_account ALTER COLUMN id SET DEFAULT nextval('tenant1.cashbook_account_id_seq'::regclass);


--
-- TOC entry 3328 (class 2604 OID 18646)
-- Name: cashbook_category id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_category ALTER COLUMN id SET DEFAULT nextval('tenant1.cashbook_category_id_seq'::regclass);


--
-- TOC entry 3327 (class 2604 OID 18635)
-- Name: cashbook_entry id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry ALTER COLUMN id SET DEFAULT nextval('tenant1.cashbook_entry_id_seq'::regclass);


--
-- TOC entry 3319 (class 2604 OID 18557)
-- Name: clothing id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing ALTER COLUMN id SET DEFAULT nextval('tenant1.clothing_id_seq'::regclass);


--
-- TOC entry 3342 (class 2604 OID 18801)
-- Name: clothing_group id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing_group ALTER COLUMN id SET DEFAULT nextval('tenant1.clothing_group_id_seq'::regclass);


--
-- TOC entry 3320 (class 2604 OID 18565)
-- Name: clothing_type id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing_type ALTER COLUMN id SET DEFAULT nextval('tenant1.clothing_type_id_seq'::regclass);


--
-- TOC entry 3335 (class 2604 OID 18726)
-- Name: color_code id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.color_code ALTER COLUMN id SET DEFAULT nextval('tenant1.color_code_id_seq'::regclass);


--
-- TOC entry 3353 (class 2604 OID 19277)
-- Name: column_config id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.column_config ALTER COLUMN id SET DEFAULT nextval('tenant1.column_config_id_seq'::regclass);


--
-- TOC entry 3331 (class 2604 OID 18668)
-- Name: company id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.company ALTER COLUMN id SET DEFAULT nextval('tenant1.company_id_seq'::regclass);


--
-- TOC entry 3338 (class 2604 OID 18753)
-- Name: composer id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composer ALTER COLUMN id SET DEFAULT nextval('tenant1.composer_id_seq'::regclass);


--
-- TOC entry 3334 (class 2604 OID 18710)
-- Name: composition id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition ALTER COLUMN id SET DEFAULT nextval('tenant1.composition_id_seq'::regclass);


--
-- TOC entry 3348 (class 2604 OID 18873)
-- Name: customization id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.customization ALTER COLUMN id SET DEFAULT nextval('tenant1.customization_id_seq'::regclass);


--
-- TOC entry 3351 (class 2604 OID 19252)
-- Name: email_verification_link id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.email_verification_link ALTER COLUMN id SET DEFAULT nextval('tenant1.email_verification_link_id_seq'::regclass);


--
-- TOC entry 3352 (class 2604 OID 19266)
-- Name: filter id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.filter ALTER COLUMN id SET DEFAULT nextval('tenant1.filter_id_seq'::regclass);


--
-- TOC entry 3337 (class 2604 OID 18745)
-- Name: genre id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.genre ALTER COLUMN id SET DEFAULT nextval('tenant1.genre_id_seq'::regclass);


--
-- TOC entry 3321 (class 2604 OID 18573)
-- Name: honor id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.honor ALTER COLUMN id SET DEFAULT nextval('tenant1.honor_id_seq'::regclass);


--
-- TOC entry 3315 (class 2604 OID 18521)
-- Name: instrument id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument ALTER COLUMN id SET DEFAULT nextval('tenant1.instrument_id_seq'::regclass);


--
-- TOC entry 3317 (class 2604 OID 18539)
-- Name: instrument_service id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_service ALTER COLUMN id SET DEFAULT nextval('tenant1.instrument_service_id_seq'::regclass);


--
-- TOC entry 3316 (class 2604 OID 18531)
-- Name: instrument_type id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_type ALTER COLUMN id SET DEFAULT nextval('tenant1.instrument_type_id_seq'::regclass);


--
-- TOC entry 3301 (class 2604 OID 19217)
-- Name: jhi_authority id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_authority ALTER COLUMN id SET DEFAULT nextval('tenant1.jhi_authority_id_seq'::regclass);


--
-- TOC entry 3303 (class 2604 OID 18384)
-- Name: jhi_persistent_audit_event event_id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_persistent_audit_event ALTER COLUMN event_id SET DEFAULT nextval('tenant1.jhi_persistent_audit_event_event_id_seq'::regclass);


--
-- TOC entry 3300 (class 2604 OID 18349)
-- Name: jhi_user id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user ALTER COLUMN id SET DEFAULT nextval('tenant1.jhi_user_id_seq'::regclass);


--
-- TOC entry 3345 (class 2604 OID 18833)
-- Name: letter id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter ALTER COLUMN id SET DEFAULT nextval('tenant1.letter_id_seq'::regclass);


--
-- TOC entry 3309 (class 2604 OID 18462)
-- Name: membership id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership ALTER COLUMN id SET DEFAULT nextval('tenant1.membership_id_seq'::regclass);


--
-- TOC entry 3310 (class 2604 OID 18470)
-- Name: membership_fee id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee ALTER COLUMN id SET DEFAULT nextval('tenant1.membership_fee_id_seq'::regclass);


--
-- TOC entry 3311 (class 2604 OID 18481)
-- Name: membership_fee_amount id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_amount ALTER COLUMN id SET DEFAULT nextval('tenant1.membership_fee_amount_id_seq'::regclass);


--
-- TOC entry 3312 (class 2604 OID 18489)
-- Name: membership_fee_for_period id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_for_period ALTER COLUMN id SET DEFAULT nextval('tenant1.membership_fee_for_period_id_seq'::regclass);


--
-- TOC entry 3336 (class 2604 OID 18734)
-- Name: music_book id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.music_book ALTER COLUMN id SET DEFAULT nextval('tenant1.music_book_id_seq'::regclass);


--
-- TOC entry 3347 (class 2604 OID 18865)
-- Name: notification id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification ALTER COLUMN id SET DEFAULT nextval('tenant1.notification_id_seq'::regclass);


--
-- TOC entry 3346 (class 2604 OID 18849)
-- Name: notification_rule id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification_rule ALTER COLUMN id SET DEFAULT nextval('tenant1.notification_rule_id_seq'::regclass);


--
-- TOC entry 3341 (class 2604 OID 18790)
-- Name: official_appointment id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.official_appointment ALTER COLUMN id SET DEFAULT nextval('tenant1.official_appointment_id_seq'::regclass);


--
-- TOC entry 3304 (class 2604 OID 18404)
-- Name: person id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person ALTER COLUMN id SET DEFAULT nextval('tenant1.person_id_seq'::regclass);


--
-- TOC entry 3325 (class 2604 OID 18611)
-- Name: person_address id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_address ALTER COLUMN id SET DEFAULT nextval('tenant1.person_address_id_seq'::regclass);


--
-- TOC entry 3324 (class 2604 OID 18603)
-- Name: person_award id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_award ALTER COLUMN id SET DEFAULT nextval('tenant1.person_award_id_seq'::regclass);


--
-- TOC entry 3318 (class 2604 OID 18549)
-- Name: person_clothing id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_clothing ALTER COLUMN id SET DEFAULT nextval('tenant1.person_clothing_id_seq'::regclass);


--
-- TOC entry 3306 (class 2604 OID 18420)
-- Name: person_group id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_group ALTER COLUMN id SET DEFAULT nextval('tenant1.person_group_id_seq'::regclass);


--
-- TOC entry 3323 (class 2604 OID 18595)
-- Name: person_honor id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_honor ALTER COLUMN id SET DEFAULT nextval('tenant1.person_honor_id_seq'::regclass);


--
-- TOC entry 3308 (class 2604 OID 18454)
-- Name: person_info id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_info ALTER COLUMN id SET DEFAULT nextval('tenant1.person_info_id_seq'::regclass);


--
-- TOC entry 3314 (class 2604 OID 18513)
-- Name: person_instrument id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_instrument ALTER COLUMN id SET DEFAULT nextval('tenant1.person_instrument_id_seq'::regclass);


--
-- TOC entry 3329 (class 2604 OID 18657)
-- Name: receipt id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.receipt ALTER COLUMN id SET DEFAULT nextval('tenant1.receipt_id_seq'::regclass);


--
-- TOC entry 3340 (class 2604 OID 18779)
-- Name: report id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.report ALTER COLUMN id SET DEFAULT nextval('tenant1.report_id_seq'::regclass);


--
-- TOC entry 3307 (class 2604 OID 18441)
-- Name: role id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.role ALTER COLUMN id SET DEFAULT nextval('tenant1.role_id_seq'::regclass);


--
-- TOC entry 3344 (class 2604 OID 18822)
-- Name: template id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.template ALTER COLUMN id SET DEFAULT nextval('tenant1.template_id_seq'::regclass);


--
-- TOC entry 3354 (class 2604 OID 19288)
-- Name: user_column_config id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.user_column_config ALTER COLUMN id SET DEFAULT nextval('tenant1.user_column_config_id_seq'::regclass);


--
-- TOC entry 3258 (class 2604 OID 17531)
-- Name: address id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.address ALTER COLUMN id SET DEFAULT nextval('tenant2.address_id_seq'::regclass);


--
-- TOC entry 3277 (class 2604 OID 17713)
-- Name: appointment id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment ALTER COLUMN id SET DEFAULT nextval('tenant2.appointment_id_seq'::regclass);


--
-- TOC entry 3278 (class 2604 OID 17731)
-- Name: appointment_type id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_type ALTER COLUMN id SET DEFAULT nextval('tenant2.appointment_type_id_seq'::regclass);


--
-- TOC entry 3284 (class 2604 OID 17795)
-- Name: arranger id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.arranger ALTER COLUMN id SET DEFAULT nextval('tenant2.arranger_id_seq'::regclass);


--
-- TOC entry 3267 (class 2604 OID 17613)
-- Name: award id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.award ALTER COLUMN id SET DEFAULT nextval('tenant2.award_id_seq'::regclass);


--
-- TOC entry 3288 (class 2604 OID 17838)
-- Name: bank_import_data id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.bank_import_data ALTER COLUMN id SET DEFAULT nextval('tenant2.bank_import_data_id_seq'::regclass);


--
-- TOC entry 3271 (class 2604 OID 17648)
-- Name: cashbook id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook ALTER COLUMN id SET DEFAULT nextval('tenant2.cashbook_id_seq'::regclass);


--
-- TOC entry 3295 (class 2604 OID 17913)
-- Name: cashbook_account id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_account ALTER COLUMN id SET DEFAULT nextval('tenant2.cashbook_account_id_seq'::regclass);


--
-- TOC entry 3273 (class 2604 OID 17675)
-- Name: cashbook_category id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_category ALTER COLUMN id SET DEFAULT nextval('tenant2.cashbook_category_id_seq'::regclass);


--
-- TOC entry 3272 (class 2604 OID 17664)
-- Name: cashbook_entry id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry ALTER COLUMN id SET DEFAULT nextval('tenant2.cashbook_entry_id_seq'::regclass);


--
-- TOC entry 3264 (class 2604 OID 17586)
-- Name: clothing id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing ALTER COLUMN id SET DEFAULT nextval('tenant2.clothing_id_seq'::regclass);


--
-- TOC entry 3287 (class 2604 OID 17830)
-- Name: clothing_group id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing_group ALTER COLUMN id SET DEFAULT nextval('tenant2.clothing_group_id_seq'::regclass);


--
-- TOC entry 3265 (class 2604 OID 17594)
-- Name: clothing_type id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing_type ALTER COLUMN id SET DEFAULT nextval('tenant2.clothing_type_id_seq'::regclass);


--
-- TOC entry 3280 (class 2604 OID 17755)
-- Name: color_code id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.color_code ALTER COLUMN id SET DEFAULT nextval('tenant2.color_code_id_seq'::regclass);


--
-- TOC entry 3298 (class 2604 OID 18306)
-- Name: column_config id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.column_config ALTER COLUMN id SET DEFAULT nextval('tenant2.column_config_id_seq'::regclass);


--
-- TOC entry 3276 (class 2604 OID 17697)
-- Name: company id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.company ALTER COLUMN id SET DEFAULT nextval('tenant2.company_id_seq'::regclass);


--
-- TOC entry 3283 (class 2604 OID 17782)
-- Name: composer id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composer ALTER COLUMN id SET DEFAULT nextval('tenant2.composer_id_seq'::regclass);


--
-- TOC entry 3279 (class 2604 OID 17739)
-- Name: composition id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition ALTER COLUMN id SET DEFAULT nextval('tenant2.composition_id_seq'::regclass);


--
-- TOC entry 3293 (class 2604 OID 17902)
-- Name: customization id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.customization ALTER COLUMN id SET DEFAULT nextval('tenant2.customization_id_seq'::regclass);


--
-- TOC entry 3296 (class 2604 OID 18281)
-- Name: email_verification_link id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.email_verification_link ALTER COLUMN id SET DEFAULT nextval('tenant2.email_verification_link_id_seq'::regclass);


--
-- TOC entry 3297 (class 2604 OID 18295)
-- Name: filter id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.filter ALTER COLUMN id SET DEFAULT nextval('tenant2.filter_id_seq'::regclass);


--
-- TOC entry 3282 (class 2604 OID 17774)
-- Name: genre id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.genre ALTER COLUMN id SET DEFAULT nextval('tenant2.genre_id_seq'::regclass);


--
-- TOC entry 3266 (class 2604 OID 17602)
-- Name: honor id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.honor ALTER COLUMN id SET DEFAULT nextval('tenant2.honor_id_seq'::regclass);


--
-- TOC entry 3260 (class 2604 OID 17550)
-- Name: instrument id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument ALTER COLUMN id SET DEFAULT nextval('tenant2.instrument_id_seq'::regclass);


--
-- TOC entry 3262 (class 2604 OID 17568)
-- Name: instrument_service id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_service ALTER COLUMN id SET DEFAULT nextval('tenant2.instrument_service_id_seq'::regclass);


--
-- TOC entry 3261 (class 2604 OID 17560)
-- Name: instrument_type id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_type ALTER COLUMN id SET DEFAULT nextval('tenant2.instrument_type_id_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 18246)
-- Name: jhi_authority id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_authority ALTER COLUMN id SET DEFAULT nextval('tenant2.jhi_authority_id_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 17413)
-- Name: jhi_persistent_audit_event event_id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_persistent_audit_event ALTER COLUMN event_id SET DEFAULT nextval('tenant2.jhi_persistent_audit_event_event_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 17378)
-- Name: jhi_user id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user ALTER COLUMN id SET DEFAULT nextval('tenant2.jhi_user_id_seq'::regclass);


--
-- TOC entry 3290 (class 2604 OID 17862)
-- Name: letter id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter ALTER COLUMN id SET DEFAULT nextval('tenant2.letter_id_seq'::regclass);


--
-- TOC entry 3254 (class 2604 OID 17491)
-- Name: membership id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership ALTER COLUMN id SET DEFAULT nextval('tenant2.membership_id_seq'::regclass);


--
-- TOC entry 3255 (class 2604 OID 17499)
-- Name: membership_fee id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee ALTER COLUMN id SET DEFAULT nextval('tenant2.membership_fee_id_seq'::regclass);


--
-- TOC entry 3256 (class 2604 OID 17510)
-- Name: membership_fee_amount id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_amount ALTER COLUMN id SET DEFAULT nextval('tenant2.membership_fee_amount_id_seq'::regclass);


--
-- TOC entry 3257 (class 2604 OID 17518)
-- Name: membership_fee_for_period id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_for_period ALTER COLUMN id SET DEFAULT nextval('tenant2.membership_fee_for_period_id_seq'::regclass);


--
-- TOC entry 3281 (class 2604 OID 17763)
-- Name: music_book id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.music_book ALTER COLUMN id SET DEFAULT nextval('tenant2.music_book_id_seq'::regclass);


--
-- TOC entry 3292 (class 2604 OID 17894)
-- Name: notification id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification ALTER COLUMN id SET DEFAULT nextval('tenant2.notification_id_seq'::regclass);


--
-- TOC entry 3291 (class 2604 OID 17878)
-- Name: notification_rule id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification_rule ALTER COLUMN id SET DEFAULT nextval('tenant2.notification_rule_id_seq'::regclass);


--
-- TOC entry 3286 (class 2604 OID 17819)
-- Name: official_appointment id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.official_appointment ALTER COLUMN id SET DEFAULT nextval('tenant2.official_appointment_id_seq'::regclass);


--
-- TOC entry 3249 (class 2604 OID 17433)
-- Name: person id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person ALTER COLUMN id SET DEFAULT nextval('tenant2.person_id_seq'::regclass);


--
-- TOC entry 3270 (class 2604 OID 17640)
-- Name: person_address id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_address ALTER COLUMN id SET DEFAULT nextval('tenant2.person_address_id_seq'::regclass);


--
-- TOC entry 3269 (class 2604 OID 17632)
-- Name: person_award id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_award ALTER COLUMN id SET DEFAULT nextval('tenant2.person_award_id_seq'::regclass);


--
-- TOC entry 3263 (class 2604 OID 17578)
-- Name: person_clothing id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_clothing ALTER COLUMN id SET DEFAULT nextval('tenant2.person_clothing_id_seq'::regclass);


--
-- TOC entry 3251 (class 2604 OID 17449)
-- Name: person_group id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_group ALTER COLUMN id SET DEFAULT nextval('tenant2.person_group_id_seq'::regclass);


--
-- TOC entry 3268 (class 2604 OID 17624)
-- Name: person_honor id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_honor ALTER COLUMN id SET DEFAULT nextval('tenant2.person_honor_id_seq'::regclass);


--
-- TOC entry 3253 (class 2604 OID 17483)
-- Name: person_info id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_info ALTER COLUMN id SET DEFAULT nextval('tenant2.person_info_id_seq'::regclass);


--
-- TOC entry 3259 (class 2604 OID 17542)
-- Name: person_instrument id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_instrument ALTER COLUMN id SET DEFAULT nextval('tenant2.person_instrument_id_seq'::regclass);


--
-- TOC entry 3274 (class 2604 OID 17686)
-- Name: receipt id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.receipt ALTER COLUMN id SET DEFAULT nextval('tenant2.receipt_id_seq'::regclass);


--
-- TOC entry 3285 (class 2604 OID 17808)
-- Name: report id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.report ALTER COLUMN id SET DEFAULT nextval('tenant2.report_id_seq'::regclass);


--
-- TOC entry 3252 (class 2604 OID 17470)
-- Name: role id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.role ALTER COLUMN id SET DEFAULT nextval('tenant2.role_id_seq'::regclass);


--
-- TOC entry 3289 (class 2604 OID 17851)
-- Name: template id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.template ALTER COLUMN id SET DEFAULT nextval('tenant2.template_id_seq'::regclass);


--
-- TOC entry 3299 (class 2604 OID 18317)
-- Name: user_column_config id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.user_column_config ALTER COLUMN id SET DEFAULT nextval('tenant2.user_column_config_id_seq'::regclass);


--
-- TOC entry 4158 (class 0 OID 16555)
-- Dependencies: 226
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.address (id, street_address, postal_code, city, state) FROM stdin;
\.


--
-- TOC entry 4196 (class 0 OID 16737)
-- Dependencies: 264
-- Data for Name: appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment (id, name, description, street_address, postal_code, city, country, begin_date, end_date, official_appointment_id, type_id) FROM stdin;
\.


--
-- TOC entry 4197 (class 0 OID 16748)
-- Dependencies: 265
-- Data for Name: appointment_persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment_persons (persons_id, appointments_id) FROM stdin;
\.


--
-- TOC entry 4199 (class 0 OID 16755)
-- Dependencies: 267
-- Data for Name: appointment_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment_type (id, name, is_official, person_group_id) FROM stdin;
\.


--
-- TOC entry 4213 (class 0 OID 16819)
-- Dependencies: 281
-- Data for Name: arranger; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arranger (id, name) FROM stdin;
\.


--
-- TOC entry 4214 (class 0 OID 16825)
-- Dependencies: 282
-- Data for Name: arranger_compositions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arranger_compositions (compositions_id, arrangers_id) FROM stdin;
\.


--
-- TOC entry 4176 (class 0 OID 16637)
-- Dependencies: 244
-- Data for Name: award; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.award (id, name, descripton) FROM stdin;
\.


--
-- TOC entry 4222 (class 0 OID 16862)
-- Dependencies: 290
-- Data for Name: bank_import_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bank_import_data (id, entry_date, entry_value, entry_text, partner_name, entry_reference, bank_importer_type, cashbook_entry_id, cashbook_category_id) FROM stdin;
\.


--
-- TOC entry 4184 (class 0 OID 16672)
-- Dependencies: 252
-- Data for Name: cashbook; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook (id, name, description) FROM stdin;
\.


--
-- TOC entry 4236 (class 0 OID 16937)
-- Dependencies: 304
-- Data for Name: cashbook_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_account (id, name, bank_account, bank_importer_type, receipt_code, current_number) FROM stdin;
\.


--
-- TOC entry 4185 (class 0 OID 16681)
-- Dependencies: 253
-- Data for Name: cashbook_cashbook_entries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_cashbook_entries (cashbook_entries_id, cashbooks_id) FROM stdin;
\.


--
-- TOC entry 4189 (class 0 OID 16699)
-- Dependencies: 257
-- Data for Name: cashbook_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_category (id, name, description, color, parent_id) FROM stdin;
\.


--
-- TOC entry 4187 (class 0 OID 16688)
-- Dependencies: 255
-- Data for Name: cashbook_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_entry (id, title, type, date, amount, appointment_id, cashbook_category_id, receipt_id, cashbook_account_id) FROM stdin;
\.


--
-- TOC entry 4170 (class 0 OID 16610)
-- Dependencies: 238
-- Data for Name: clothing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing (id, size, purchase_date, not_available, type_id) FROM stdin;
\.


--
-- TOC entry 4220 (class 0 OID 16854)
-- Dependencies: 288
-- Data for Name: clothing_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing_group (id, size, quantity, quantity_available, type_id) FROM stdin;
\.


--
-- TOC entry 4172 (class 0 OID 16618)
-- Dependencies: 240
-- Data for Name: clothing_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing_type (id, name) FROM stdin;
\.


--
-- TOC entry 4204 (class 0 OID 16779)
-- Dependencies: 272
-- Data for Name: color_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.color_code (id, name) FROM stdin;
\.


--
-- TOC entry 4243 (class 0 OID 17330)
-- Dependencies: 311
-- Data for Name: column_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.column_config (id, entity, column_name, default_visible, default_position, column_display_name, sort_by_name) FROM stdin;
1	COMPOSITION	colorCodeName	t	1	walterApp.composition.colorCode	colorCode.name
2	COMPOSITION	nr	t	2	walterApp.composition.nr	nr
3	COMPOSITION	title	t	3	walterApp.composition.title	title
4	COMPOSITION	genreName	t	4	walterApp.composition.genre	genre.name
5	COMPOSITION	musicBookName	t	5	walterApp.composition.musicBook	musicBook.name
6	COMPOSITION	orderingCompanyName	f	1	walterApp.composition.orderingCompany	orderingCompany.name
7	COMPOSITION	publisherName	f	2	walterApp.composition.publisher	publisher.name
8	PERSON	firstName	t	1	walterApp.person.firstName	firstName
9	PERSON	lastName	t	2	walterApp.person.lastName	lastName
10	PERSON	birthDate	t	3	walterApp.person.birthDate	birthDate
11	PERSON	gender	f	1	walterApp.person.gender	gender
12	PERSON	email	f	2	walterApp.person.email	email
13	INSTRUMENT	name	t	1	walterApp.instrument.name	name
14	INSTRUMENT	price	t	2	walterApp.instrument.price	price
15	INSTRUMENT	purchaseDate	t	3	walterApp.instrument.purchaseDate	purchaseDate
16	INSTRUMENT	privateInstrument	t	4	walterApp.instrument.privateInstrument	privateInstrument
17	INSTRUMENT	typeName	t	5	walterApp.instrument.type	typeName
18	CASHBOOK-ENTRY	date	t	1	walterApp.cashbookEntry.date	date
19	CASHBOOK-ENTRY	title	t	2	walterApp.cashbookEntry.title	title
20	CASHBOOK-ENTRY	receiptIdentifier	t	3	walterApp.cashbookEntry.receipt	receipt.identifier
21	CASHBOOK-ENTRY	cashbookCategoryName	t	4	walterApp.cashbookEntry.cashbookCategory	cashbookCategory.name
22	CASHBOOK-ENTRY	amount	t	5	walterApp.CashbookEntryType.INCOME	
23	CASHBOOK-ENTRY	amount	t	6	walterApp.CashbookEntryType.SPENDING	
24	CASHBOOK-ENTRY	cashbookAccountName	f	1	walterApp.cashbookEntry.cashbookAccount	cashbookAccount.name
25	APPOINTMENT	beginDate	t	1	walterApp.appointment.beginDate	beginDate
26	APPOINTMENT	endDate	t	2	walterApp.appointment.endDate	endDate
27	APPOINTMENT	name	t	3	walterApp.appointment.name	name
28	APPOINTMENT	typeName	t	4	walterApp.appointment.type	appointmentType.name
29	APPOINTMENT	description	f	1	walterApp.appointment.description	description
30	APPOINTMENT	streetAddress	f	2	walterApp.appointment.streetAddress	streetAddress
31	APPOINTMENT	postalCode	f	3	walterApp.appointment.postalCode	postalCode
32	APPOINTMENT	city	f	4	walterApp.appointment.city	city
33	APPOINTMENT	country	f	5	walterApp.appointment.country	country
\.


--
-- TOC entry 4193 (class 0 OID 16721)
-- Dependencies: 261
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company (id, name, street_address, postal_code, city, country, recipient) FROM stdin;
\.


--
-- TOC entry 4194 (class 0 OID 16730)
-- Dependencies: 262
-- Data for Name: company_person_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company_person_groups (person_groups_id, companies_id) FROM stdin;
\.


--
-- TOC entry 4210 (class 0 OID 16806)
-- Dependencies: 278
-- Data for Name: composer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composer (id, name) FROM stdin;
\.


--
-- TOC entry 4211 (class 0 OID 16812)
-- Dependencies: 279
-- Data for Name: composer_compositions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composer_compositions (compositions_id, composers_id) FROM stdin;
\.


--
-- TOC entry 4201 (class 0 OID 16763)
-- Dependencies: 269
-- Data for Name: composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composition (id, nr, title, note, akm_composition_nr, cashbook_entry_id, ordering_company_id, publisher_id, genre_id, music_book_id, color_code_id) FROM stdin;
\.


--
-- TOC entry 4202 (class 0 OID 16772)
-- Dependencies: 270
-- Data for Name: composition_appointments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composition_appointments (appointments_id, compositions_id) FROM stdin;
\.


--
-- TOC entry 4234 (class 0 OID 16926)
-- Dependencies: 302
-- Data for Name: customization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customization (id, name, data, text) FROM stdin;
2	MEMBER_PERSONGROUP	0	
3	DEACTIVATED_FEATUREGROUPS	[]	
4	AKM_DATA	{}	
5	HOURS_TO_INVALIDATE_VERIFICATION	48	
\.


--
-- TOC entry 4131 (class 0 OID 16392)
-- Dependencies: 199
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels) FROM stdin;
00000000000000	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:17:26.289612	1	EXECUTED	7:eda8cd7fd15284e6128be97bd8edea82	createSequence		\N	3.4.2	\N	\N
00000000000001	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:17:26.487704	2	EXECUTED	7:b9be43015ddcfe6cc0173fe720f5a3ec	createTable, createIndex (x2), createTable (x2), addPrimaryKey, addForeignKeyConstraint (x2), loadData, dropDefaultValue, loadData (x2), createTable (x2), addPrimaryKey, createIndex (x2), addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104914-1	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_Person.xml	2019-01-14 19:17:26.534725	3	EXECUTED	7:f9a5278625d3f0c2031c364518a6f220	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104915-1	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_PersonGroup.xml	2019-01-14 19:17:26.550767	4	EXECUTED	7:e17bca58c36e5e044deab7534eb463ff	createTable		\N	3.4.2	\N	\N
20161102104916-1	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_WalterUser.xml	2019-01-14 19:17:26.578783	5	EXECUTED	7:05aafd4a1058b1bd1fa2e91c57c3b38d	createTable		\N	3.4.2	\N	\N
20161102104917-1	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_Role.xml	2019-01-14 19:17:26.606796	6	EXECUTED	7:b0fb658f2c19f8e68374c33846e78b24	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104918-1	jhipster	classpath:config/liquibase/changelog/20161102104918_added_entity_PersonInfo.xml	2019-01-14 19:17:26.624802	7	EXECUTED	7:37371b6f98f2929dbd78eab5351c882e	createTable		\N	3.4.2	\N	\N
20161102104919-1	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_Membership.xml	2019-01-14 19:17:26.63881	8	EXECUTED	7:7022f3fb55947358348baa4efd892655	createTable		\N	3.4.2	\N	\N
20161102104920-1	jhipster	classpath:config/liquibase/changelog/20161102104920_added_entity_MembershipFee.xml	2019-01-14 19:17:26.661818	9	EXECUTED	7:22e533231a2ba6fcb4ed987fc8957432	createTable		\N	3.4.2	\N	\N
20161102104921-1	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_MembershipFeeAmount.xml	2019-01-14 19:17:26.681831	10	EXECUTED	7:6bb3458e477095b053e7ae921e26de4d	createTable		\N	3.4.2	\N	\N
20161102104922-1	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_MembershipFeeForPeriod.xml	2019-01-14 19:17:26.706843	11	EXECUTED	7:e1591f04339e27e9b024c598c5633905	createTable		\N	3.4.2	\N	\N
20161102104923-1	jhipster	classpath:config/liquibase/changelog/20161102104923_added_entity_Address.xml	2019-01-14 19:17:26.729854	12	EXECUTED	7:901335603ea0d0197968a7fcfc2d36a1	createTable		\N	3.4.2	\N	\N
20161102104924-1	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_PersonInstrument.xml	2019-01-14 19:17:26.74483	13	EXECUTED	7:dfb8c080f3bcdd58cd6d369297feccdf	createTable		\N	3.4.2	\N	\N
20161102104925-1	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_Instrument.xml	2019-01-14 19:17:26.76387	14	EXECUTED	7:d404e23102c8268532fd0a1fcda881ba	createTable		\N	3.4.2	\N	\N
20161102104926-1	jhipster	classpath:config/liquibase/changelog/20161102104926_added_entity_InstrumentType.xml	2019-01-14 19:17:26.779877	15	EXECUTED	7:5ca4785573c595749d48676b920e2b35	createTable		\N	3.4.2	\N	\N
20161102104927-1	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_InstrumentService.xml	2019-01-14 19:17:26.801888	16	EXECUTED	7:08ed231981c6842b34551314095aa782	createTable		\N	3.4.2	\N	\N
20161102104928-1	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_PersonClothing.xml	2019-01-14 19:17:26.820873	17	EXECUTED	7:fa3796f1649cc12f659ed9f61b1b9bdf	createTable		\N	3.4.2	\N	\N
20161102104929-1	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_Clothing.xml	2019-01-14 19:17:26.838907	18	EXECUTED	7:54aa0caf46fa900362aded1b3f2d371a	createTable		\N	3.4.2	\N	\N
20161102104930-1	jhipster	classpath:config/liquibase/changelog/20161102104930_added_entity_ClothingType.xml	2019-01-14 19:17:26.857893	19	EXECUTED	7:5dada8ca39d16a26bafaf105ca46db8e	createTable		\N	3.4.2	\N	\N
20161102104931-1	jhipster	classpath:config/liquibase/changelog/20161102104931_added_entity_Honor.xml	2019-01-14 19:17:26.87993	20	EXECUTED	7:52f16f4cbf9599f8a2f206c40890cf12	createTable		\N	3.4.2	\N	\N
20161102104932-1	jhipster	classpath:config/liquibase/changelog/20161102104932_added_entity_Award.xml	2019-01-14 19:17:26.900935	21	EXECUTED	7:2b50b6a3b36da0e2890627fde009b833	createTable		\N	3.4.2	\N	\N
20161102104933-1	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_PersonHonor.xml	2019-01-14 19:17:26.914943	22	EXECUTED	7:988f3ea4fbb6eff4d455512cd105e8b2	createTable		\N	3.4.2	\N	\N
20161102104934-1	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_PersonAward.xml	2019-01-14 19:17:26.934921	23	EXECUTED	7:6854b28e0d1e31b2fa8f50a3fca05321	createTable		\N	3.4.2	\N	\N
20161102104935-1	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_PersonAddress.xml	2019-01-14 19:17:26.950962	24	EXECUTED	7:a2321f6ae1eb12f070558f851866800f	createTable		\N	3.4.2	\N	\N
20161102104936-1	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_Cashbook.xml	2019-01-14 19:17:26.978974	25	EXECUTED	7:0192ec399e1521a58cd2b85d4e0cef41	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104937-1	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_CashbookEntry.xml	2019-01-14 19:17:27.002953	26	EXECUTED	7:29dd4da295fce1393dccd54b90e015ed	createTable		\N	3.4.2	\N	\N
20161102104938-1	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_CashbookCategory.xml	2019-01-14 19:17:27.024966	27	EXECUTED	7:d2608f5e4b02701e2d9d6ea3194c7634	createTable		\N	3.4.2	\N	\N
20161102104939-1	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_Receipt.xml	2019-01-14 19:17:27.045012	28	EXECUTED	7:d8186d6ed7d45b615d7c556aa5158825	createTable		\N	3.4.2	\N	\N
20161102104940-1	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_Company.xml	2019-01-14 19:17:27.076994	29	EXECUTED	7:6b77cafdde0392639a51d6893de98e7d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104941-1	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_Appointment.xml	2019-01-14 19:17:27.117042	30	EXECUTED	7:077a2164c35698bc753460fd27eb1980	createTable, dropDefaultValue (x2), createTable, addPrimaryKey		\N	3.4.2	\N	\N
20161102104942-1	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_AppointmentType.xml	2019-01-14 19:17:27.13903	31	EXECUTED	7:7f25c9f4fd7051bc46d3871488a6eace	createTable		\N	3.4.2	\N	\N
20161102104943-1	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_Composition.xml	2019-01-14 19:17:27.17704	32	EXECUTED	7:269d44883ecb2c77afa1d8e758c46ecf	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104944-1	jhipster	classpath:config/liquibase/changelog/20161102104944_added_entity_ColorCode.xml	2019-01-14 19:17:27.19605	33	EXECUTED	7:5f9754f91d90ece349915983e984085c	createTable		\N	3.4.2	\N	\N
20161102104945-1	jhipster	classpath:config/liquibase/changelog/20161102104945_added_entity_MusicBook.xml	2019-01-14 19:17:27.223063	34	EXECUTED	7:3ed82be4541ab8c3fd45d07930b7395d	createTable		\N	3.4.2	\N	\N
20161102104946-1	jhipster	classpath:config/liquibase/changelog/20161102104946_added_entity_Genre.xml	2019-01-14 19:17:27.241071	35	EXECUTED	7:a0c6c19aa664b40a5af2a57db0ce57b2	createTable		\N	3.4.2	\N	\N
20161102104947-1	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_Composer.xml	2019-01-14 19:17:27.276087	36	EXECUTED	7:31e2a0aeb10819ff47c990c2a6508cc9	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104948-1	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_Arranger.xml	2019-01-14 19:17:27.314104	37	EXECUTED	7:fa875605d756b8c4ad811d6e3aead79d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104949-1	jhipster	classpath:config/liquibase/changelog/20161102104949_added_entity_Report.xml	2019-01-14 19:17:27.345121	38	EXECUTED	7:3678897ecea8ea9f19faa890389fb785	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20161113155740-1	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_OfficialAppointment.xml	2019-01-14 19:17:27.377174	39	EXECUTED	7:6821b236618c98ce49c5c77cab7796d7	createTable		\N	3.4.2	\N	\N
20161214053409-1	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_ClothingGroup.xml	2019-01-14 19:17:27.395145	40	EXECUTED	7:f6c8a7bdd4e1ea562d5a3337885ababc	createTable		\N	3.4.2	\N	\N
20170104210724-1	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_BankImportData.xml	2019-01-14 19:17:27.430193	41	EXECUTED	7:71b0a5b780302dadfd482b433724b74c	createTable		\N	3.4.2	\N	\N
20170104190142-1	jhipster	classpath:config/liquibase/changelog/20170104190142_added_entity_Template.xml	2019-01-14 19:17:27.454173	42	EXECUTED	7:26b54acd02768919aafc83838dd85a55	createTable		\N	3.4.2	\N	\N
20170104190709-1	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_Letter.xml	2019-01-14 19:17:27.484192	43	EXECUTED	7:3ed461b478f749dd060394fca492e250	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122114257-1	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_NotificationRule.xml	2019-01-14 19:17:27.532257	44	EXECUTED	7:b5e036d48a87e243f25312fea04e341f	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122115258-1	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_Notification.xml	2019-01-14 19:17:27.548224	45	EXECUTED	7:7faa054b9dc60d5ab4ddb11f0926891c	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20170930130601-1	jhipster	classpath:config/liquibase/changelog/20170930130601_added_entity_Customization.xml	2019-01-14 19:17:27.570261	46	EXECUTED	7:769cc51ccd6e1c24ac182213fc19c2c9	createTable		\N	3.4.2	\N	\N
20171030171006-1	jhipster	classpath:config/liquibase/changelog/20171030171006_added_entity_CashbookAccount.xml	2019-01-14 19:17:27.591239	47	EXECUTED	7:700413e98436a448c213c16e33ad285f	createTable		\N	3.4.2	\N	\N
20161102104914-2	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_constraints_Person.xml	2019-01-14 19:17:27.607279	48	EXECUTED	7:88759aeb6a26c2d102adca65c5c0a01a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104915-2	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_constraints_PersonGroup.xml	2019-01-14 19:17:27.615254	49	EXECUTED	7:21b983b33da2aa2ffc57ee6555ca5696	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104916-2	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_constraints_WalterUser.xml	2019-01-14 19:17:27.629261	50	EXECUTED	7:8276b656b3639d4bf6e7aff7ba06dbfc	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104917-2	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_constraints_Role.xml	2019-01-14 19:17:27.646277	51	EXECUTED	7:405ae59b31b851353ce6135e9e49032a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104919-2	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_constraints_Membership.xml	2019-01-14 19:17:27.665278	52	EXECUTED	7:7f9df05df792e84d585e3a87791200fb	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104921-2	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_constraints_MembershipFeeAmount.xml	2019-01-14 19:17:27.673315	53	EXECUTED	7:87d4d44d7570eeb06613cd9ff7980a38	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104922-2	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_constraints_MembershipFeeForPeriod.xml	2019-01-14 19:17:27.693316	54	EXECUTED	7:64c63ba937f2e90697f666693f8d97af	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104924-2	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_constraints_PersonInstrument.xml	2019-01-14 19:17:27.712329	55	EXECUTED	7:9896da86e9715a890aa1000012c3bb06	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104925-2	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_constraints_Instrument.xml	2019-01-14 19:17:27.724306	56	EXECUTED	7:35f47a04edd6853298522c7565ab3bd7	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104927-2	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_constraints_InstrumentService.xml	2019-01-14 19:17:27.738316	57	EXECUTED	7:86e3a9f61ffc8d438597e086d9db6fc2	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104928-2	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_constraints_PersonClothing.xml	2019-01-14 19:17:27.752318	58	EXECUTED	7:20e0c2c6ed2ee7e95745bfd00d4ac6a1	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104929-2	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_constraints_Clothing.xml	2019-01-14 19:17:27.764403	59	EXECUTED	7:a0aad378f7097d9619b68d6246d971c5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104933-2	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_constraints_PersonHonor.xml	2019-01-14 19:17:27.779365	60	EXECUTED	7:0dc636021a5fa29f7ffc57df0f0f1804	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104934-2	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_constraints_PersonAward.xml	2019-01-14 19:17:27.794337	61	EXECUTED	7:5c4037e9e9eb6843362f3636cee3365b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104935-2	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_constraints_PersonAddress.xml	2019-01-14 19:17:27.812381	62	EXECUTED	7:f852f2459b9b5b6fcf3ecc5bdf33ee13	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104936-2	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_constraints_Cashbook.xml	2019-01-14 19:17:27.829356	63	EXECUTED	7:d3ccfe4859720998a3563b705d361d90	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104937-2	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_constraints_CashbookEntry.xml	2019-01-14 19:17:27.857399	64	EXECUTED	7:89905ed52236d1b70d976c728388902a	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104938-2	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_constraints_CashbookCategory.xml	2019-01-14 19:17:27.865375	65	EXECUTED	7:d2bcaebedffa74fd5dcc7cdb4b2074b5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104939-2	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_constraints_Receipt.xml	2019-01-14 19:17:27.873477	66	EXECUTED	7:9ed379388af0889d05d4d9c44d8d5523	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104940-2	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_constraints_Company.xml	2019-01-14 19:17:27.885386	67	EXECUTED	7:38fadcf668fde47fd50887e3061270e4	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104941-2	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_constraints_Appointment.xml	2019-01-14 19:17:27.910425	68	EXECUTED	7:7788ef135fd9f5af1ce4792ef95fff69	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104942-2	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_constraints_AppointmentType.xml	2019-01-14 19:17:27.92043	69	EXECUTED	7:7eef97fb0ecb478a954685a99a37305c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104943-2	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_constraints_Composition.xml	2019-01-14 19:17:27.963424	70	EXECUTED	7:da9379c2b1e784bc095b0004949cc923	addForeignKeyConstraint (x8)		\N	3.4.2	\N	\N
20161102104947-2	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_constraints_Composer.xml	2019-01-14 19:17:27.981429	71	EXECUTED	7:6fe0805e298261fd2d92060ae88af2a5	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104948-2	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_constraints_Arranger.xml	2019-01-14 19:17:27.994439	72	EXECUTED	7:f89f75ef2ffe3d7e2f0c9a8059b0e267	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161113155740-2	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_constraints_OfficialAppointment.xml	2019-01-14 19:17:28.003443	73	EXECUTED	7:6f06af06f73b7e1c13230f87db52be0c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161214053409-2	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_constraints_ClothingGroup.xml	2019-01-14 19:17:28.011476	74	EXECUTED	7:5e6bb7daa6a9f43e0e42a56df4e7e53e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170104210724-2	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_constraints_BankImportData.xml	2019-01-14 19:17:28.022451	75	EXECUTED	7:bdb156dd17d8e9c58a28cecc3df1221e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170122114257-2	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_constraints_NotificationRule.xml	2019-01-14 19:17:28.041488	76	EXECUTED	7:e67d6025922b570718da7976ce33b22b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170122115258-2	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_constraints_Notification.xml	2019-01-14 19:17:28.064471	77	EXECUTED	7:4ccc834b3f211eaa544371ed31013737	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170104190709-2	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_constraints_Letter.xml	2019-01-14 19:17:28.083512	78	EXECUTED	7:cd53991884ecf839288a05c7e51fdca6	addForeignKeyConstraint (x3)		\N	3.4.2	\N	\N
20171001073000-1	fonkwill	classpath:config/liquibase/changelog/20171001073000_added_entity_constraints_Customization.xml	2019-01-14 19:17:28.098489	79	EXECUTED	7:f7567b71347f418f89c20f8ffcae1e80	addUniqueConstraint		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Customization.xml	2019-01-14 19:17:28.110498	80	EXECUTED	7:a2924d761292b8776126b9fae0fed6ac	loadData		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Notification_Rule.xml	2019-01-14 19:17:28.118531	81	EXECUTED	7:2694452973080e16bcd9c41dcc3b8500	loadData		\N	3.4.2	\N	\N
201809222213	fonkwill	classpath:config/liquibase/changelog/201809222213_added_database_tag_v10.xml	2019-01-14 19:17:28.121497	82	EXECUTED	7:c0a27752fc13556fc6746108e7b21be9	tagDatabase		v1.0	3.4.2	\N	\N
201809280957	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:17:28.152545	83	EXECUTED	7:0aea42cc1bc0e506264e6740187aced2	dropForeignKeyConstraint (x3), dropTable (x2)		\N	3.4.2	\N	\N
201809280957-2	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:17:28.252594	84	EXECUTED	7:bf918496ecdd20e49ba05c8936668a63	delete, loadData, addColumn, update (x2), dropPrimaryKey, addPrimaryKey, addForeignKeyConstraint, dropColumn		\N	3.4.2	\N	\N
201809280957-3	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:17:28.34961	85	EXECUTED	7:f0cb4324e76f152007ddf4ac5c66d9c4	delete, addColumn (x3), addUniqueConstraint, dropForeignKeyConstraint, dropPrimaryKey, dropColumn, addColumn, addNotNullConstraint, addPrimaryKey, addForeignKeyConstraint, dropPrimaryKey, addPrimaryKey, loadData		\N	3.4.2	\N	\N
20181030171007	jhipster	classpath:config/liquibase/changelog/20181030171007_added_fields_entity_CashbookAccount.xml	2019-01-14 19:17:28.378622	86	EXECUTED	7:36f0be8f5225fbb0432ab67a2385b563	addColumn, sql, addUniqueConstraint, addNotNullConstraint, addColumn, addNotNullConstraint		\N	3.4.2	\N	\N
20181030171008	jhipster	classpath:config/liquibase/changelog/20181030171008_added_fields_entity_BankImportData.xml	2019-01-14 19:17:28.389629	87	EXECUTED	7:c437869a48070aa46b9ba00e83962521	addColumn, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181030171009	jhipster	classpath:config/liquibase/changelog/20181030171009_added_fields_entity_Receipt.xml	2019-01-14 19:17:28.420672	88	EXECUTED	7:c0b6024f7b5d77f7a807906386f9e21c	addColumn (x2), sql, addNotNullConstraint, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181116150506-1	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_EmailVerificationLink.xml	2019-01-14 19:17:28.436684	89	EXECUTED	7:8b5960e5d3de2a3721bac81fe2301885	createTable, dropDefaultValue (x2)		\N	3.4.2	\N	\N
20181116150506-2	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_constraints_EmailVerificationLink.xml	2019-01-14 19:17:28.447693	90	EXECUTED	7:2df6a8baf651650337e85e89c0fc7f0b	addForeignKeyConstraint		\N	3.4.2	\N	\N
20181201171009	jhipster	classpath:config/liquibase/changelog/20181201171009_added_fields_entity_Person.xml	2019-01-14 19:17:28.453657	91	EXECUTED	7:283254850972190046fc015604c56808	addColumn		\N	3.4.2	\N	\N
20181209160512-1	jhipster	classpath:config/liquibase/changelog/20181209160512_added_entity_Filter.xml	2019-01-14 19:17:28.476672	92	EXECUTED	7:a47059f41a8b45f602684fca638eb32c	createTable		\N	3.4.2	\N	\N
20181214184940-1	jhipster	classpath:config/liquibase/changelog/20181214184940_added_entity_ColumnConfig.xml	2019-01-14 19:17:28.498684	93	EXECUTED	7:f742d2b11b2e2cb3b75a413177f86481	createTable		\N	3.4.2	\N	\N
20181214184959	jhipster	classpath:config/liquibase/changelog/20181214184959_added_ColumnConfig_Compositions.xml	2019-01-14 19:17:28.510718	94	EXECUTED	7:a60a23361bcd5e5b40cff16faef538c1	insert (x7)		\N	3.4.2	\N	\N
20181214185854-1	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_UserColumnConfig.xml	2019-01-14 19:17:28.5287	95	EXECUTED	7:123261b8b8410ab942c2832d60496343	createTable		\N	3.4.2	\N	\N
20181214185854-2	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_constraints_UserColumnConfig.xml	2019-01-14 19:17:28.544677	96	EXECUTED	7:a4d8cb578d5753329dc0afd908936e08	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20190501140300-0	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:17:28.55074	97	EXECUTED	7:304971ccf8e6b35a933685d572ec99d9	delete		\N	3.4.2	\N	\N
20190501140300-1	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:17:28.556712	98	EXECUTED	7:2cb713628de375c86d9f32b66fbf220c	loadData		\N	3.4.2	\N	\N
20190109193459	jhipster	classpath:config/liquibase/changelog/20190109193459_added_ColumnConfig_Person.xml	2019-01-14 19:17:28.565714	99	EXECUTED	7:547b36312f7cc468f198695b357ff71b	insert (x5)		\N	3.4.2	\N	\N
20190111103059	jhipster	classpath:config/liquibase/changelog/20190111103059_added_ColumnConfig_Instrument.xml	2019-01-14 19:17:28.573691	100	EXECUTED	7:7d876ee688d09650d4fc5190aaa0f511	insert (x5)		\N	3.4.2	\N	\N
20190113105859	jhipster	classpath:config/liquibase/changelog/20190113105859_added_ColumnConfig_CashbookEntry.xml	2019-01-14 19:17:28.584793	101	EXECUTED	7:b553c8721b4cbdc0d1ddcdc7b50dfabd	insert (x7)		\N	3.4.2	\N	\N
20190113132859	jhipster	classpath:config/liquibase/changelog/20190113132859_added_ColumnConfig_Appointment.xml	2019-01-14 19:17:28.595732	102	EXECUTED	7:f98b2a57358b4d92f1c57e3f085a57bc	insert (x9)		\N	3.4.2	\N	\N
\.


--
-- TOC entry 4130 (class 0 OID 16387)
-- Dependencies: 198
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- TOC entry 4239 (class 0 OID 17305)
-- Dependencies: 307
-- Data for Name: email_verification_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.email_verification_link (id, secret_key, creation_date, validation_date, invalid, person_id) FROM stdin;
\.


--
-- TOC entry 4241 (class 0 OID 17319)
-- Dependencies: 309
-- Data for Name: filter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filter (id, list_name, name, filter) FROM stdin;
\.


--
-- TOC entry 4208 (class 0 OID 16798)
-- Dependencies: 276
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.genre (id, name) FROM stdin;
\.


--
-- TOC entry 4174 (class 0 OID 16626)
-- Dependencies: 242
-- Data for Name: honor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.honor (id, name, description) FROM stdin;
\.


--
-- TOC entry 4162 (class 0 OID 16574)
-- Dependencies: 230
-- Data for Name: instrument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument (id, name, purchase_date, private_instrument, price, type_id, cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4166 (class 0 OID 16592)
-- Dependencies: 234
-- Data for Name: instrument_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument_service (id, date, note, cashbook_entry_id, instrument_id) FROM stdin;
\.


--
-- TOC entry 4164 (class 0 OID 16584)
-- Dependencies: 232
-- Data for Name: instrument_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument_type (id, name) FROM stdin;
\.


--
-- TOC entry 4135 (class 0 OID 16415)
-- Dependencies: 203
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_authority (name, role_id, id, write) FROM stdin;
BASE	1	1	t
MANAGEMENT	1	2	t
USER	1	3	t
ROLES	1	4	t
TENANT_CONFIGURATION	1	5	t
BASE	2	6	t
\.


--
-- TOC entry 4138 (class 0 OID 16437)
-- Dependencies: 206
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_persistent_audit_event (event_id, principal, event_date, event_type) FROM stdin;
\.


--
-- TOC entry 4139 (class 0 OID 16443)
-- Dependencies: 207
-- Data for Name: jhi_persistent_audit_evt_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_persistent_audit_evt_data (event_id, name, value) FROM stdin;
\.


--
-- TOC entry 4134 (class 0 OID 16402)
-- Dependencies: 202
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_user (id, login, password_hash, first_name, last_name, email, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date) FROM stdin;
1	system	$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG	System	System	system@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
2	anonymoususer	$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO	Anonymous	User	anonymous@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
3	admin	$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC	Administrator	Administrator	admin@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
4	user	$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K	User	User	user@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
5	vorstand	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Vorstand	Verein	vorstand@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
6	mitglieder-admin	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Mitglieder-Verwalter	Verein	vorstand1@localhost	t	de	\N	\N	system	2019-01-14 19:17:26.301648	\N	system	\N
\.


--
-- TOC entry 4136 (class 0 OID 16420)
-- Dependencies: 204
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_user_authority (user_id, role_id) FROM stdin;
1	1
3	1
1	2
3	2
4	2
6	2
5	2
\.


--
-- TOC entry 4226 (class 0 OID 16886)
-- Dependencies: 294
-- Data for Name: letter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.letter (id, title, text, payment, current_period, previous_period, template_id) FROM stdin;
\.


--
-- TOC entry 4227 (class 0 OID 16895)
-- Dependencies: 295
-- Data for Name: letter_person_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.letter_person_group (person_groups_id, letters_id) FROM stdin;
\.


--
-- TOC entry 4150 (class 0 OID 16515)
-- Dependencies: 218
-- Data for Name: membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership (id, begin_date, end_date, person_id, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4152 (class 0 OID 16523)
-- Dependencies: 220
-- Data for Name: membership_fee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee (id, description, period, period_time_fraction) FROM stdin;
\.


--
-- TOC entry 4154 (class 0 OID 16534)
-- Dependencies: 222
-- Data for Name: membership_fee_amount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee_amount (id, begin_date, end_date, amount, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4156 (class 0 OID 16542)
-- Dependencies: 224
-- Data for Name: membership_fee_for_period; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee_for_period (id, nr, jhi_year, reference_code, paid, note, cashbook_entry_id, membership_id) FROM stdin;
\.


--
-- TOC entry 4206 (class 0 OID 16787)
-- Dependencies: 274
-- Data for Name: music_book; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.music_book (id, name, description) FROM stdin;
\.


--
-- TOC entry 4232 (class 0 OID 16918)
-- Dependencies: 300
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification (id, entity_id, created_at, read_by_user_id, notification_rule_id) FROM stdin;
\.


--
-- TOC entry 4229 (class 0 OID 16902)
-- Dependencies: 297
-- Data for Name: notification_rule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification_rule (id, title, entity, conditions, active, message_group, message_single) FROM stdin;
1	Geburtstage im nächsten Monat	Person	SELECT p FROM Person p WHERE MONTH(p.birthDate) = ( MONTH(CURRENT_DATE) + 1 )	t	Im nächsten Monat haben {{count}} Personen Geburtstag.	Im nächsten Monat hat {{entity.firstName}} {{entity.lastName}} Geburtstag (geboren am {{entity.birthDate}})
\.


--
-- TOC entry 4230 (class 0 OID 16911)
-- Dependencies: 298
-- Data for Name: notification_rule_target_audiences; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification_rule_target_audiences (notification_rules_id, target_role_id) FROM stdin;
\.


--
-- TOC entry 4218 (class 0 OID 16843)
-- Dependencies: 286
-- Data for Name: official_appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.official_appointment (id, organizer_name, organizer_address, is_head_quota, report_id) FROM stdin;
\.


--
-- TOC entry 4141 (class 0 OID 16457)
-- Dependencies: 209
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, first_name, last_name, birth_date, email, telephone_number, street_address, postal_code, city, country, gender, email_type) FROM stdin;
\.


--
-- TOC entry 4182 (class 0 OID 16664)
-- Dependencies: 250
-- Data for Name: person_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_address (id, begin_date, end_date, address_id, person_id) FROM stdin;
\.


--
-- TOC entry 4180 (class 0 OID 16656)
-- Dependencies: 248
-- Data for Name: person_award; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_award (id, date, award_id, person_id) FROM stdin;
\.


--
-- TOC entry 4168 (class 0 OID 16602)
-- Dependencies: 236
-- Data for Name: person_clothing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_clothing (id, begin_date, end_date, clothing_id, person_id) FROM stdin;
\.


--
-- TOC entry 4144 (class 0 OID 16473)
-- Dependencies: 212
-- Data for Name: person_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_group (id, name, parent_id) FROM stdin;
\.


--
-- TOC entry 4178 (class 0 OID 16648)
-- Dependencies: 246
-- Data for Name: person_honor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_honor (id, date, honor_id, person_id) FROM stdin;
\.


--
-- TOC entry 4148 (class 0 OID 16507)
-- Dependencies: 216
-- Data for Name: person_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_info (id, name) FROM stdin;
\.


--
-- TOC entry 4160 (class 0 OID 16566)
-- Dependencies: 228
-- Data for Name: person_instrument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_instrument (id, begin_date, end_date, instrument_id, person_id) FROM stdin;
\.


--
-- TOC entry 4142 (class 0 OID 16466)
-- Dependencies: 210
-- Data for Name: person_person_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_person_groups (person_groups_id, people_id) FROM stdin;
\.


--
-- TOC entry 4191 (class 0 OID 16710)
-- Dependencies: 259
-- Data for Name: receipt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.receipt (id, date, note, title, file, identifier, company_id, overwrite_identifier, initial_cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4216 (class 0 OID 16832)
-- Dependencies: 284
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.report (id, generation_date, association_id, association_name, street_address, postal_code, city) FROM stdin;
\.


--
-- TOC entry 4146 (class 0 OID 16494)
-- Dependencies: 214
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, name) FROM stdin;
1	ROLE_ADMIN
2	ROLE_USER
3	ROLE_BOARD_MEMBER
4	ROLE_MEMBER_ADMIN
5	ROLE_ACCOUNTING_ADMIN
6	ROLE_INVENTORY_ADMIN
7	ROLE_BASE_ADMIN
\.


--
-- TOC entry 4224 (class 0 OID 16875)
-- Dependencies: 292
-- Data for Name: template; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.template (id, title, file, margin_left, margin_right, margin_top, margin_bottom) FROM stdin;
\.


--
-- TOC entry 4245 (class 0 OID 17341)
-- Dependencies: 313
-- Data for Name: user_column_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_column_config (id, visible, "position", column_config_id, user_id) FROM stdin;
1	t	1	1	1
2	t	2	2	1
3	t	3	3	1
4	t	4	4	1
5	t	5	5	1
6	f	1	6	1
7	f	2	7	1
8	t	1	8	1
9	t	2	9	1
10	t	3	10	1
11	f	1	11	1
12	f	2	12	1
13	t	1	13	1
14	t	2	14	1
15	t	3	15	1
16	t	4	16	1
17	t	5	17	1
18	t	1	18	1
19	t	2	19	1
20	t	3	20	1
21	t	4	21	1
22	t	5	22	1
23	t	6	23	1
24	f	1	24	1
25	t	1	25	1
26	t	2	26	1
27	t	3	27	1
28	t	4	28	1
29	f	1	29	1
30	f	2	30	1
31	f	3	31	1
32	f	4	32	1
33	f	5	33	1
34	t	1	1	2
35	t	2	2	2
36	t	3	3	2
37	t	4	4	2
38	t	5	5	2
39	f	1	6	2
40	f	2	7	2
41	t	1	8	2
42	t	2	9	2
43	t	3	10	2
44	f	1	11	2
45	f	2	12	2
46	t	1	13	2
47	t	2	14	2
48	t	3	15	2
49	t	4	16	2
50	t	5	17	2
51	t	1	18	2
52	t	2	19	2
53	t	3	20	2
54	t	4	21	2
55	t	5	22	2
56	t	6	23	2
57	f	1	24	2
58	t	1	25	2
59	t	2	26	2
60	t	3	27	2
61	t	4	28	2
62	f	1	29	2
63	f	2	30	2
64	f	3	31	2
65	f	4	32	2
66	f	5	33	2
67	t	1	1	3
68	t	2	2	3
69	t	3	3	3
70	t	4	4	3
71	t	5	5	3
72	f	1	6	3
73	f	2	7	3
74	t	1	8	3
75	t	2	9	3
76	t	3	10	3
77	f	1	11	3
78	f	2	12	3
79	t	1	13	3
80	t	2	14	3
81	t	3	15	3
82	t	4	16	3
83	t	5	17	3
84	t	1	18	3
85	t	2	19	3
86	t	3	20	3
87	t	4	21	3
88	t	5	22	3
89	t	6	23	3
90	f	1	24	3
91	t	1	25	3
92	t	2	26	3
93	t	3	27	3
94	t	4	28	3
95	f	1	29	3
96	f	2	30	3
97	f	3	31	3
98	f	4	32	3
99	f	5	33	3
100	t	1	1	4
101	t	2	2	4
102	t	3	3	4
103	t	4	4	4
104	t	5	5	4
105	f	1	6	4
106	f	2	7	4
107	t	1	8	4
108	t	2	9	4
109	t	3	10	4
110	f	1	11	4
111	f	2	12	4
112	t	1	13	4
113	t	2	14	4
114	t	3	15	4
115	t	4	16	4
116	t	5	17	4
117	t	1	18	4
118	t	2	19	4
119	t	3	20	4
120	t	4	21	4
121	t	5	22	4
122	t	6	23	4
123	f	1	24	4
124	t	1	25	4
125	t	2	26	4
126	t	3	27	4
127	t	4	28	4
128	f	1	29	4
129	f	2	30	4
130	f	3	31	4
131	f	4	32	4
132	f	5	33	4
133	t	1	1	5
134	t	2	2	5
135	t	3	3	5
136	t	4	4	5
137	t	5	5	5
138	f	1	6	5
139	f	2	7	5
140	t	1	8	5
141	t	2	9	5
142	t	3	10	5
143	f	1	11	5
144	f	2	12	5
145	t	1	13	5
146	t	2	14	5
147	t	3	15	5
148	t	4	16	5
149	t	5	17	5
150	t	1	18	5
151	t	2	19	5
152	t	3	20	5
153	t	4	21	5
154	t	5	22	5
155	t	6	23	5
156	f	1	24	5
157	t	1	25	5
158	t	2	26	5
159	t	3	27	5
160	t	4	28	5
161	f	1	29	5
162	f	2	30	5
163	f	3	31	5
164	f	4	32	5
165	f	5	33	5
166	t	1	1	6
167	t	2	2	6
168	t	3	3	6
169	t	4	4	6
170	t	5	5	6
171	f	1	6	6
172	f	2	7	6
173	t	1	8	6
174	t	2	9	6
175	t	3	10	6
176	f	1	11	6
177	f	2	12	6
178	t	1	13	6
179	t	2	14	6
180	t	3	15	6
181	t	4	16	6
182	t	5	17	6
183	t	1	18	6
184	t	2	19	6
185	t	3	20	6
186	t	4	21	6
187	t	5	22	6
188	t	6	23	6
189	f	1	24	6
190	t	1	25	6
191	t	2	26	6
192	t	3	27	6
193	t	4	28	6
194	f	1	29	6
195	f	2	30	6
196	f	3	31	6
197	f	4	32	6
198	f	5	33	6
\.


--
-- TOC entry 4390 (class 0 OID 18499)
-- Dependencies: 458
-- Data for Name: address; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.address (id, street_address, postal_code, city, state) FROM stdin;
\.


--
-- TOC entry 4428 (class 0 OID 18681)
-- Dependencies: 496
-- Data for Name: appointment; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.appointment (id, name, description, street_address, postal_code, city, country, begin_date, end_date, official_appointment_id, type_id) FROM stdin;
1	Maibaumstellen	\N	\N	\N	\N	\N	2019-04-30 19:00:00	2019-04-30 22:00:00	1	1
2	Konzert	\N	\N	\N	\N	\N	2019-05-04 18:00:00	2019-05-04 23:00:00	\N	2
\.


--
-- TOC entry 4429 (class 0 OID 18692)
-- Dependencies: 497
-- Data for Name: appointment_persons; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.appointment_persons (persons_id, appointments_id) FROM stdin;
\.


--
-- TOC entry 4431 (class 0 OID 18699)
-- Dependencies: 499
-- Data for Name: appointment_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.appointment_type (id, name, is_official, person_group_id) FROM stdin;
1	Auftritt	t	\N
2	Konzert	f	\N
\.


--
-- TOC entry 4445 (class 0 OID 18763)
-- Dependencies: 513
-- Data for Name: arranger; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.arranger (id, name) FROM stdin;
\.


--
-- TOC entry 4446 (class 0 OID 18769)
-- Dependencies: 514
-- Data for Name: arranger_compositions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.arranger_compositions (compositions_id, arrangers_id) FROM stdin;
\.


--
-- TOC entry 4408 (class 0 OID 18581)
-- Dependencies: 476
-- Data for Name: award; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.award (id, name, descripton) FROM stdin;
\.


--
-- TOC entry 4454 (class 0 OID 18806)
-- Dependencies: 522
-- Data for Name: bank_import_data; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.bank_import_data (id, entry_date, entry_value, entry_text, partner_name, entry_reference, bank_importer_type, cashbook_entry_id, cashbook_category_id) FROM stdin;
\.


--
-- TOC entry 4416 (class 0 OID 18616)
-- Dependencies: 484
-- Data for Name: cashbook; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.cashbook (id, name, description) FROM stdin;
\.


--
-- TOC entry 4468 (class 0 OID 18881)
-- Dependencies: 536
-- Data for Name: cashbook_account; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.cashbook_account (id, name, bank_account, bank_importer_type, receipt_code, current_number) FROM stdin;
2	George	t	GEORGE_IMPORTER	GEO	0
1	Kassabuch	f	\N	KA	2
\.


--
-- TOC entry 4417 (class 0 OID 18625)
-- Dependencies: 485
-- Data for Name: cashbook_cashbook_entries; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.cashbook_cashbook_entries (cashbook_entries_id, cashbooks_id) FROM stdin;
\.


--
-- TOC entry 4421 (class 0 OID 18643)
-- Dependencies: 489
-- Data for Name: cashbook_category; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.cashbook_category (id, name, description, color, parent_id) FROM stdin;
1	Getränke	Einkäufe Getränke für das Probenlokal	#008000	\N
2	Büro	Ausgaben Büro	#ff8040	\N
\.


--
-- TOC entry 4419 (class 0 OID 18632)
-- Dependencies: 487
-- Data for Name: cashbook_entry; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.cashbook_entry (id, title, type, date, amount, appointment_id, cashbook_category_id, receipt_id, cashbook_account_id) FROM stdin;
2	Notenankauf: Lindenbaum	SPENDING	2019-01-14	15.00	\N	\N	\N	\N
3	Einkauf Papiermaterial	SPENDING	2019-01-11	11.55	\N	2	\N	1
1	Einkauf Bier Probenlokal2	SPENDING	2019-01-14	23.99	\N	1	\N	1
\.


--
-- TOC entry 4402 (class 0 OID 18554)
-- Dependencies: 470
-- Data for Name: clothing; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.clothing (id, size, purchase_date, not_available, type_id) FROM stdin;
\.


--
-- TOC entry 4452 (class 0 OID 18798)
-- Dependencies: 520
-- Data for Name: clothing_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.clothing_group (id, size, quantity, quantity_available, type_id) FROM stdin;
\.


--
-- TOC entry 4404 (class 0 OID 18562)
-- Dependencies: 472
-- Data for Name: clothing_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.clothing_type (id, name) FROM stdin;
\.


--
-- TOC entry 4436 (class 0 OID 18723)
-- Dependencies: 504
-- Data for Name: color_code; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.color_code (id, name) FROM stdin;
1	Grün
2	Blau
\.


--
-- TOC entry 4475 (class 0 OID 19274)
-- Dependencies: 543
-- Data for Name: column_config; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.column_config (id, entity, column_name, default_visible, default_position, column_display_name, sort_by_name) FROM stdin;
1	COMPOSITION	colorCodeName	t	1	walterApp.composition.colorCode	colorCode.name
2	COMPOSITION	nr	t	2	walterApp.composition.nr	nr
3	COMPOSITION	title	t	3	walterApp.composition.title	title
4	COMPOSITION	genreName	t	4	walterApp.composition.genre	genre.name
5	COMPOSITION	musicBookName	t	5	walterApp.composition.musicBook	musicBook.name
6	COMPOSITION	orderingCompanyName	f	1	walterApp.composition.orderingCompany	orderingCompany.name
7	COMPOSITION	publisherName	f	2	walterApp.composition.publisher	publisher.name
8	PERSON	firstName	t	1	walterApp.person.firstName	firstName
9	PERSON	lastName	t	2	walterApp.person.lastName	lastName
10	PERSON	birthDate	t	3	walterApp.person.birthDate	birthDate
11	PERSON	gender	f	1	walterApp.person.gender	gender
12	PERSON	email	f	2	walterApp.person.email	email
13	INSTRUMENT	name	t	1	walterApp.instrument.name	name
14	INSTRUMENT	price	t	2	walterApp.instrument.price	price
15	INSTRUMENT	purchaseDate	t	3	walterApp.instrument.purchaseDate	purchaseDate
16	INSTRUMENT	privateInstrument	t	4	walterApp.instrument.privateInstrument	privateInstrument
17	INSTRUMENT	typeName	t	5	walterApp.instrument.type	typeName
18	CASHBOOK-ENTRY	date	t	1	walterApp.cashbookEntry.date	date
19	CASHBOOK-ENTRY	title	t	2	walterApp.cashbookEntry.title	title
20	CASHBOOK-ENTRY	receiptIdentifier	t	3	walterApp.cashbookEntry.receipt	receipt.identifier
21	CASHBOOK-ENTRY	cashbookCategoryName	t	4	walterApp.cashbookEntry.cashbookCategory	cashbookCategory.name
22	CASHBOOK-ENTRY	amount	t	5	walterApp.CashbookEntryType.INCOME	
23	CASHBOOK-ENTRY	amount	t	6	walterApp.CashbookEntryType.SPENDING	
24	CASHBOOK-ENTRY	cashbookAccountName	f	1	walterApp.cashbookEntry.cashbookAccount	cashbookAccount.name
25	APPOINTMENT	beginDate	t	1	walterApp.appointment.beginDate	beginDate
26	APPOINTMENT	endDate	t	2	walterApp.appointment.endDate	endDate
27	APPOINTMENT	name	t	3	walterApp.appointment.name	name
28	APPOINTMENT	typeName	t	4	walterApp.appointment.type	appointmentType.name
29	APPOINTMENT	description	f	1	walterApp.appointment.description	description
30	APPOINTMENT	streetAddress	f	2	walterApp.appointment.streetAddress	streetAddress
31	APPOINTMENT	postalCode	f	3	walterApp.appointment.postalCode	postalCode
32	APPOINTMENT	city	f	4	walterApp.appointment.city	city
33	APPOINTMENT	country	f	5	walterApp.appointment.country	country
\.


--
-- TOC entry 4425 (class 0 OID 18665)
-- Dependencies: 493
-- Data for Name: company; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.company (id, name, street_address, postal_code, city, country, recipient) FROM stdin;
\.


--
-- TOC entry 4426 (class 0 OID 18674)
-- Dependencies: 494
-- Data for Name: company_person_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.company_person_groups (person_groups_id, companies_id) FROM stdin;
\.


--
-- TOC entry 4442 (class 0 OID 18750)
-- Dependencies: 510
-- Data for Name: composer; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.composer (id, name) FROM stdin;
\.


--
-- TOC entry 4443 (class 0 OID 18756)
-- Dependencies: 511
-- Data for Name: composer_compositions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.composer_compositions (compositions_id, composers_id) FROM stdin;
\.


--
-- TOC entry 4433 (class 0 OID 18707)
-- Dependencies: 501
-- Data for Name: composition; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.composition (id, nr, title, note, akm_composition_nr, cashbook_entry_id, ordering_company_id, publisher_id, genre_id, music_book_id, color_code_id) FROM stdin;
1	1	Lindenbaum	\N	1	2	\N	\N	\N	\N	1
\.


--
-- TOC entry 4434 (class 0 OID 18716)
-- Dependencies: 502
-- Data for Name: composition_appointments; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.composition_appointments (appointments_id, compositions_id) FROM stdin;
\.


--
-- TOC entry 4466 (class 0 OID 18870)
-- Dependencies: 534
-- Data for Name: customization; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.customization (id, name, data, text) FROM stdin;
2	MEMBER_PERSONGROUP	0	
4	AKM_DATA	{}	
5	HOURS_TO_INVALIDATE_VERIFICATION	48	
3	DEACTIVATED_FEATUREGROUPS	["INSTRUMENT"]	
\.


--
-- TOC entry 4363 (class 0 OID 18336)
-- Dependencies: 431
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels) FROM stdin;
00000000000000	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:19:28.521483	1	EXECUTED	7:eda8cd7fd15284e6128be97bd8edea82	createSequence		\N	3.4.2	\N	\N
00000000000001	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:19:28.699565	2	EXECUTED	7:b9be43015ddcfe6cc0173fe720f5a3ec	createTable, createIndex (x2), createTable (x2), addPrimaryKey, addForeignKeyConstraint (x2), loadData, dropDefaultValue, loadData (x2), createTable (x2), addPrimaryKey, createIndex (x2), addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104914-1	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_Person.xml	2019-01-14 19:19:28.743584	3	EXECUTED	7:f9a5278625d3f0c2031c364518a6f220	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104915-1	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_PersonGroup.xml	2019-01-14 19:19:28.766598	4	EXECUTED	7:e17bca58c36e5e044deab7534eb463ff	createTable		\N	3.4.2	\N	\N
20161102104916-1	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_WalterUser.xml	2019-01-14 19:19:28.792607	5	EXECUTED	7:05aafd4a1058b1bd1fa2e91c57c3b38d	createTable		\N	3.4.2	\N	\N
20161102104917-1	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_Role.xml	2019-01-14 19:19:28.820624	6	EXECUTED	7:b0fb658f2c19f8e68374c33846e78b24	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104918-1	jhipster	classpath:config/liquibase/changelog/20161102104918_added_entity_PersonInfo.xml	2019-01-14 19:19:28.838631	7	EXECUTED	7:37371b6f98f2929dbd78eab5351c882e	createTable		\N	3.4.2	\N	\N
20161102104919-1	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_Membership.xml	2019-01-14 19:19:28.85467	8	EXECUTED	7:7022f3fb55947358348baa4efd892655	createTable		\N	3.4.2	\N	\N
20161102104920-1	jhipster	classpath:config/liquibase/changelog/20161102104920_added_entity_MembershipFee.xml	2019-01-14 19:19:28.879652	9	EXECUTED	7:22e533231a2ba6fcb4ed987fc8957432	createTable		\N	3.4.2	\N	\N
20161102104921-1	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_MembershipFeeAmount.xml	2019-01-14 19:19:28.901662	10	EXECUTED	7:6bb3458e477095b053e7ae921e26de4d	createTable		\N	3.4.2	\N	\N
20161102104922-1	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_MembershipFeeForPeriod.xml	2019-01-14 19:19:28.931675	11	EXECUTED	7:e1591f04339e27e9b024c598c5633905	createTable		\N	3.4.2	\N	\N
20161102104923-1	jhipster	classpath:config/liquibase/changelog/20161102104923_added_entity_Address.xml	2019-01-14 19:19:28.952702	12	EXECUTED	7:901335603ea0d0197968a7fcfc2d36a1	createTable		\N	3.4.2	\N	\N
20161102104924-1	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_PersonInstrument.xml	2019-01-14 19:19:28.980698	13	EXECUTED	7:dfb8c080f3bcdd58cd6d369297feccdf	createTable		\N	3.4.2	\N	\N
20161102104925-1	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_Instrument.xml	2019-01-14 19:19:29.003714	14	EXECUTED	7:d404e23102c8268532fd0a1fcda881ba	createTable		\N	3.4.2	\N	\N
20161102104926-1	jhipster	classpath:config/liquibase/changelog/20161102104926_added_entity_InstrumentType.xml	2019-01-14 19:19:29.023734	15	EXECUTED	7:5ca4785573c595749d48676b920e2b35	createTable		\N	3.4.2	\N	\N
20161102104927-1	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_InstrumentService.xml	2019-01-14 19:19:29.044733	16	EXECUTED	7:08ed231981c6842b34551314095aa782	createTable		\N	3.4.2	\N	\N
20161102104928-1	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_PersonClothing.xml	2019-01-14 19:19:29.060739	17	EXECUTED	7:fa3796f1649cc12f659ed9f61b1b9bdf	createTable		\N	3.4.2	\N	\N
20161102104929-1	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_Clothing.xml	2019-01-14 19:19:29.075758	18	EXECUTED	7:54aa0caf46fa900362aded1b3f2d371a	createTable		\N	3.4.2	\N	\N
20161102104930-1	jhipster	classpath:config/liquibase/changelog/20161102104930_added_entity_ClothingType.xml	2019-01-14 19:19:29.095765	19	EXECUTED	7:5dada8ca39d16a26bafaf105ca46db8e	createTable		\N	3.4.2	\N	\N
20161102104931-1	jhipster	classpath:config/liquibase/changelog/20161102104931_added_entity_Honor.xml	2019-01-14 19:19:29.119799	20	EXECUTED	7:52f16f4cbf9599f8a2f206c40890cf12	createTable		\N	3.4.2	\N	\N
20161102104932-1	jhipster	classpath:config/liquibase/changelog/20161102104932_added_entity_Award.xml	2019-01-14 19:19:29.138776	21	EXECUTED	7:2b50b6a3b36da0e2890627fde009b833	createTable		\N	3.4.2	\N	\N
20161102104933-1	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_PersonHonor.xml	2019-01-14 19:19:29.153788	22	EXECUTED	7:988f3ea4fbb6eff4d455512cd105e8b2	createTable		\N	3.4.2	\N	\N
20161102104934-1	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_PersonAward.xml	2019-01-14 19:19:29.16979	23	EXECUTED	7:6854b28e0d1e31b2fa8f50a3fca05321	createTable		\N	3.4.2	\N	\N
20161102104935-1	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_PersonAddress.xml	2019-01-14 19:19:29.184797	24	EXECUTED	7:a2321f6ae1eb12f070558f851866800f	createTable		\N	3.4.2	\N	\N
20161102104936-1	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_Cashbook.xml	2019-01-14 19:19:29.212813	25	EXECUTED	7:0192ec399e1521a58cd2b85d4e0cef41	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104937-1	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_CashbookEntry.xml	2019-01-14 19:19:29.235836	26	EXECUTED	7:29dd4da295fce1393dccd54b90e015ed	createTable		\N	3.4.2	\N	\N
20161102104938-1	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_CashbookCategory.xml	2019-01-14 19:19:29.259834	27	EXECUTED	7:d2608f5e4b02701e2d9d6ea3194c7634	createTable		\N	3.4.2	\N	\N
20161102104939-1	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_Receipt.xml	2019-01-14 19:19:29.280844	28	EXECUTED	7:d8186d6ed7d45b615d7c556aa5158825	createTable		\N	3.4.2	\N	\N
20161102104940-1	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_Company.xml	2019-01-14 19:19:29.313861	29	EXECUTED	7:6b77cafdde0392639a51d6893de98e7d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104941-1	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_Appointment.xml	2019-01-14 19:19:29.349878	30	EXECUTED	7:077a2164c35698bc753460fd27eb1980	createTable, dropDefaultValue (x2), createTable, addPrimaryKey		\N	3.4.2	\N	\N
20161102104942-1	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_AppointmentType.xml	2019-01-14 19:19:29.367889	31	EXECUTED	7:7f25c9f4fd7051bc46d3871488a6eace	createTable		\N	3.4.2	\N	\N
20161102104943-1	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_Composition.xml	2019-01-14 19:19:29.402907	32	EXECUTED	7:269d44883ecb2c77afa1d8e758c46ecf	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104944-1	jhipster	classpath:config/liquibase/changelog/20161102104944_added_entity_ColorCode.xml	2019-01-14 19:19:29.423917	33	EXECUTED	7:5f9754f91d90ece349915983e984085c	createTable		\N	3.4.2	\N	\N
20161102104945-1	jhipster	classpath:config/liquibase/changelog/20161102104945_added_entity_MusicBook.xml	2019-01-14 19:19:29.447925	34	EXECUTED	7:3ed82be4541ab8c3fd45d07930b7395d	createTable		\N	3.4.2	\N	\N
20161102104946-1	jhipster	classpath:config/liquibase/changelog/20161102104946_added_entity_Genre.xml	2019-01-14 19:19:29.460935	35	EXECUTED	7:a0c6c19aa664b40a5af2a57db0ce57b2	createTable		\N	3.4.2	\N	\N
20161102104947-1	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_Composer.xml	2019-01-14 19:19:29.486956	36	EXECUTED	7:31e2a0aeb10819ff47c990c2a6508cc9	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104948-1	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_Arranger.xml	2019-01-14 19:19:29.516969	37	EXECUTED	7:fa875605d756b8c4ad811d6e3aead79d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104949-1	jhipster	classpath:config/liquibase/changelog/20161102104949_added_entity_Report.xml	2019-01-14 19:19:29.547976	38	EXECUTED	7:3678897ecea8ea9f19faa890389fb785	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20161113155740-1	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_OfficialAppointment.xml	2019-01-14 19:19:29.586002	39	EXECUTED	7:6821b236618c98ce49c5c77cab7796d7	createTable		\N	3.4.2	\N	\N
20161214053409-1	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_ClothingGroup.xml	2019-01-14 19:19:29.607008	40	EXECUTED	7:f6c8a7bdd4e1ea562d5a3337885ababc	createTable		\N	3.4.2	\N	\N
20170104210724-1	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_BankImportData.xml	2019-01-14 19:19:29.639024	41	EXECUTED	7:71b0a5b780302dadfd482b433724b74c	createTable		\N	3.4.2	\N	\N
20170104190142-1	jhipster	classpath:config/liquibase/changelog/20170104190142_added_entity_Template.xml	2019-01-14 19:19:29.667005	42	EXECUTED	7:26b54acd02768919aafc83838dd85a55	createTable		\N	3.4.2	\N	\N
20170104190709-1	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_Letter.xml	2019-01-14 19:19:29.709054	43	EXECUTED	7:3ed461b478f749dd060394fca492e250	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122114257-1	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_NotificationRule.xml	2019-01-14 19:19:29.755079	44	EXECUTED	7:b5e036d48a87e243f25312fea04e341f	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122115258-1	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_Notification.xml	2019-01-14 19:19:29.784094	45	EXECUTED	7:7faa054b9dc60d5ab4ddb11f0926891c	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20170930130601-1	jhipster	classpath:config/liquibase/changelog/20170930130601_added_entity_Customization.xml	2019-01-14 19:19:29.815108	46	EXECUTED	7:769cc51ccd6e1c24ac182213fc19c2c9	createTable		\N	3.4.2	\N	\N
20171030171006-1	jhipster	classpath:config/liquibase/changelog/20171030171006_added_entity_CashbookAccount.xml	2019-01-14 19:19:29.840147	47	EXECUTED	7:700413e98436a448c213c16e33ad285f	createTable		\N	3.4.2	\N	\N
20161102104914-2	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_constraints_Person.xml	2019-01-14 19:19:29.858125	48	EXECUTED	7:88759aeb6a26c2d102adca65c5c0a01a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104915-2	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_constraints_PersonGroup.xml	2019-01-14 19:19:29.866129	49	EXECUTED	7:21b983b33da2aa2ffc57ee6555ca5696	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104916-2	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_constraints_WalterUser.xml	2019-01-14 19:19:29.875135	50	EXECUTED	7:8276b656b3639d4bf6e7aff7ba06dbfc	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104917-2	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_constraints_Role.xml	2019-01-14 19:19:29.892141	51	EXECUTED	7:405ae59b31b851353ce6135e9e49032a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104919-2	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_constraints_Membership.xml	2019-01-14 19:19:29.909153	52	EXECUTED	7:7f9df05df792e84d585e3a87791200fb	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104921-2	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_constraints_MembershipFeeAmount.xml	2019-01-14 19:19:29.919157	53	EXECUTED	7:87d4d44d7570eeb06613cd9ff7980a38	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104922-2	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_constraints_MembershipFeeForPeriod.xml	2019-01-14 19:19:29.934162	54	EXECUTED	7:64c63ba937f2e90697f666693f8d97af	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104924-2	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_constraints_PersonInstrument.xml	2019-01-14 19:19:29.952171	55	EXECUTED	7:9896da86e9715a890aa1000012c3bb06	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104925-2	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_constraints_Instrument.xml	2019-01-14 19:19:29.96518	56	EXECUTED	7:35f47a04edd6853298522c7565ab3bd7	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104927-2	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_constraints_InstrumentService.xml	2019-01-14 19:19:29.981187	57	EXECUTED	7:86e3a9f61ffc8d438597e086d9db6fc2	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104928-2	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_constraints_PersonClothing.xml	2019-01-14 19:19:29.996192	58	EXECUTED	7:20e0c2c6ed2ee7e95745bfd00d4ac6a1	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104929-2	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_constraints_Clothing.xml	2019-01-14 19:19:30.005198	59	EXECUTED	7:a0aad378f7097d9619b68d6246d971c5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104933-2	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_constraints_PersonHonor.xml	2019-01-14 19:19:30.02221	60	EXECUTED	7:0dc636021a5fa29f7ffc57df0f0f1804	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104934-2	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_constraints_PersonAward.xml	2019-01-14 19:19:30.038214	61	EXECUTED	7:5c4037e9e9eb6843362f3636cee3365b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104935-2	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_constraints_PersonAddress.xml	2019-01-14 19:19:30.052219	62	EXECUTED	7:f852f2459b9b5b6fcf3ecc5bdf33ee13	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104936-2	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_constraints_Cashbook.xml	2019-01-14 19:19:30.067197	63	EXECUTED	7:d3ccfe4859720998a3563b705d361d90	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104937-2	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_constraints_CashbookEntry.xml	2019-01-14 19:19:30.098245	64	EXECUTED	7:89905ed52236d1b70d976c728388902a	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104938-2	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_constraints_CashbookCategory.xml	2019-01-14 19:19:30.107218	65	EXECUTED	7:d2bcaebedffa74fd5dcc7cdb4b2074b5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104939-2	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_constraints_Receipt.xml	2019-01-14 19:19:30.116252	66	EXECUTED	7:9ed379388af0889d05d4d9c44d8d5523	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104940-2	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_constraints_Company.xml	2019-01-14 19:19:30.126264	67	EXECUTED	7:38fadcf668fde47fd50887e3061270e4	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104941-2	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_constraints_Appointment.xml	2019-01-14 19:19:30.150266	68	EXECUTED	7:7788ef135fd9f5af1ce4792ef95fff69	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104942-2	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_constraints_AppointmentType.xml	2019-01-14 19:19:30.159342	69	EXECUTED	7:7eef97fb0ecb478a954685a99a37305c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104943-2	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_constraints_Composition.xml	2019-01-14 19:19:30.198293	70	EXECUTED	7:da9379c2b1e784bc095b0004949cc923	addForeignKeyConstraint (x8)		\N	3.4.2	\N	\N
20161102104947-2	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_constraints_Composer.xml	2019-01-14 19:19:30.215298	71	EXECUTED	7:6fe0805e298261fd2d92060ae88af2a5	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104948-2	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_constraints_Arranger.xml	2019-01-14 19:19:30.234311	72	EXECUTED	7:f89f75ef2ffe3d7e2f0c9a8059b0e267	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161113155740-2	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_constraints_OfficialAppointment.xml	2019-01-14 19:19:30.244313	73	EXECUTED	7:6f06af06f73b7e1c13230f87db52be0c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161214053409-2	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_constraints_ClothingGroup.xml	2019-01-14 19:19:30.253317	74	EXECUTED	7:5e6bb7daa6a9f43e0e42a56df4e7e53e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170104210724-2	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_constraints_BankImportData.xml	2019-01-14 19:19:30.263323	75	EXECUTED	7:bdb156dd17d8e9c58a28cecc3df1221e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170122114257-2	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_constraints_NotificationRule.xml	2019-01-14 19:19:30.283332	76	EXECUTED	7:e67d6025922b570718da7976ce33b22b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170122115258-2	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_constraints_Notification.xml	2019-01-14 19:19:30.309345	77	EXECUTED	7:4ccc834b3f211eaa544371ed31013737	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170104190709-2	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_constraints_Letter.xml	2019-01-14 19:19:30.327325	78	EXECUTED	7:cd53991884ecf839288a05c7e51fdca6	addForeignKeyConstraint (x3)		\N	3.4.2	\N	\N
20171001073000-1	fonkwill	classpath:config/liquibase/changelog/20171001073000_added_entity_constraints_Customization.xml	2019-01-14 19:19:30.339362	79	EXECUTED	7:f7567b71347f418f89c20f8ffcae1e80	addUniqueConstraint		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Customization.xml	2019-01-14 19:19:30.351367	80	EXECUTED	7:a2924d761292b8776126b9fae0fed6ac	loadData		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Notification_Rule.xml	2019-01-14 19:19:30.360372	81	EXECUTED	7:2694452973080e16bcd9c41dcc3b8500	loadData		\N	3.4.2	\N	\N
201809222213	fonkwill	classpath:config/liquibase/changelog/201809222213_added_database_tag_v10.xml	2019-01-14 19:19:30.363372	82	EXECUTED	7:c0a27752fc13556fc6746108e7b21be9	tagDatabase		v1.0	3.4.2	\N	\N
201809280957	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:30.388385	83	EXECUTED	7:0aea42cc1bc0e506264e6740187aced2	dropForeignKeyConstraint (x3), dropTable (x2)		\N	3.4.2	\N	\N
201809280957-2	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:30.436384	84	EXECUTED	7:bf918496ecdd20e49ba05c8936668a63	delete, loadData, addColumn, update (x2), dropPrimaryKey, addPrimaryKey, addForeignKeyConstraint, dropColumn		\N	3.4.2	\N	\N
201809280957-3	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:30.531454	85	EXECUTED	7:f0cb4324e76f152007ddf4ac5c66d9c4	delete, addColumn (x3), addUniqueConstraint, dropForeignKeyConstraint, dropPrimaryKey, dropColumn, addColumn, addNotNullConstraint, addPrimaryKey, addForeignKeyConstraint, dropPrimaryKey, addPrimaryKey, loadData		\N	3.4.2	\N	\N
20181030171007	jhipster	classpath:config/liquibase/changelog/20181030171007_added_fields_entity_CashbookAccount.xml	2019-01-14 19:19:30.56047	86	EXECUTED	7:36f0be8f5225fbb0432ab67a2385b563	addColumn, sql, addUniqueConstraint, addNotNullConstraint, addColumn, addNotNullConstraint		\N	3.4.2	\N	\N
20181030171008	jhipster	classpath:config/liquibase/changelog/20181030171008_added_fields_entity_BankImportData.xml	2019-01-14 19:19:30.571477	87	EXECUTED	7:c437869a48070aa46b9ba00e83962521	addColumn, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181030171009	jhipster	classpath:config/liquibase/changelog/20181030171009_added_fields_entity_Receipt.xml	2019-01-14 19:19:30.590456	88	EXECUTED	7:c0b6024f7b5d77f7a807906386f9e21c	addColumn (x2), sql, addNotNullConstraint, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181116150506-1	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_EmailVerificationLink.xml	2019-01-14 19:19:30.612493	89	EXECUTED	7:8b5960e5d3de2a3721bac81fe2301885	createTable, dropDefaultValue (x2)		\N	3.4.2	\N	\N
20181116150506-2	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_constraints_EmailVerificationLink.xml	2019-01-14 19:19:30.623507	90	EXECUTED	7:2df6a8baf651650337e85e89c0fc7f0b	addForeignKeyConstraint		\N	3.4.2	\N	\N
20181201171009	jhipster	classpath:config/liquibase/changelog/20181201171009_added_fields_entity_Person.xml	2019-01-14 19:19:30.629473	91	EXECUTED	7:283254850972190046fc015604c56808	addColumn		\N	3.4.2	\N	\N
20181209160512-1	jhipster	classpath:config/liquibase/changelog/20181209160512_added_entity_Filter.xml	2019-01-14 19:19:30.654512	92	EXECUTED	7:a47059f41a8b45f602684fca638eb32c	createTable		\N	3.4.2	\N	\N
20181214184940-1	jhipster	classpath:config/liquibase/changelog/20181214184940_added_entity_ColumnConfig.xml	2019-01-14 19:19:30.680527	93	EXECUTED	7:f742d2b11b2e2cb3b75a413177f86481	createTable		\N	3.4.2	\N	\N
20181214184959	jhipster	classpath:config/liquibase/changelog/20181214184959_added_ColumnConfig_Compositions.xml	2019-01-14 19:19:30.694503	94	EXECUTED	7:a60a23361bcd5e5b40cff16faef538c1	insert (x7)		\N	3.4.2	\N	\N
20181214185854-1	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_UserColumnConfig.xml	2019-01-14 19:19:30.712541	95	EXECUTED	7:123261b8b8410ab942c2832d60496343	createTable		\N	3.4.2	\N	\N
20181214185854-2	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_constraints_UserColumnConfig.xml	2019-01-14 19:19:30.727548	96	EXECUTED	7:a4d8cb578d5753329dc0afd908936e08	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20190501140300-0	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:19:30.733554	97	EXECUTED	7:304971ccf8e6b35a933685d572ec99d9	delete		\N	3.4.2	\N	\N
20190501140300-1	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:19:30.73856	98	EXECUTED	7:2cb713628de375c86d9f32b66fbf220c	loadData		\N	3.4.2	\N	\N
20190109193459	jhipster	classpath:config/liquibase/changelog/20190109193459_added_ColumnConfig_Person.xml	2019-01-14 19:19:30.74656	99	EXECUTED	7:547b36312f7cc468f198695b357ff71b	insert (x5)		\N	3.4.2	\N	\N
20190111103059	jhipster	classpath:config/liquibase/changelog/20190111103059_added_ColumnConfig_Instrument.xml	2019-01-14 19:19:30.754532	100	EXECUTED	7:7d876ee688d09650d4fc5190aaa0f511	insert (x5)		\N	3.4.2	\N	\N
20190113105859	jhipster	classpath:config/liquibase/changelog/20190113105859_added_ColumnConfig_CashbookEntry.xml	2019-01-14 19:19:30.76557	101	EXECUTED	7:b553c8721b4cbdc0d1ddcdc7b50dfabd	insert (x7)		\N	3.4.2	\N	\N
20190113132859	jhipster	classpath:config/liquibase/changelog/20190113132859_added_ColumnConfig_Appointment.xml	2019-01-14 19:19:30.777569	102	EXECUTED	7:f98b2a57358b4d92f1c57e3f085a57bc	insert (x9)		\N	3.4.2	\N	\N
\.


--
-- TOC entry 4362 (class 0 OID 18331)
-- Dependencies: 430
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- TOC entry 4471 (class 0 OID 19249)
-- Dependencies: 539
-- Data for Name: email_verification_link; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.email_verification_link (id, secret_key, creation_date, validation_date, invalid, person_id) FROM stdin;
1	shtTofT7	2019-01-14 20:00:31.559	\N	t	1
2	3MgxNOpp	2019-01-14 20:22:25.21	2019-01-14 20:37:53.715	f	1
3	UVCDjTWU	2019-01-14 20:38:46.246	\N	f	2
\.


--
-- TOC entry 4473 (class 0 OID 19263)
-- Dependencies: 541
-- Data for Name: filter; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.filter (id, list_name, name, filter) FROM stdin;
1	APPOINTMENT	Auftritte	{"appointmentTypeId":1,"nameOrDescription":null,"earliestBegin":null,"latestEnd":null,"location":null,"search":true}
\.


--
-- TOC entry 4440 (class 0 OID 18742)
-- Dependencies: 508
-- Data for Name: genre; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.genre (id, name) FROM stdin;
\.


--
-- TOC entry 4406 (class 0 OID 18570)
-- Dependencies: 474
-- Data for Name: honor; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.honor (id, name, description) FROM stdin;
\.


--
-- TOC entry 4394 (class 0 OID 18518)
-- Dependencies: 462
-- Data for Name: instrument; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.instrument (id, name, purchase_date, private_instrument, price, type_id, cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4398 (class 0 OID 18536)
-- Dependencies: 466
-- Data for Name: instrument_service; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.instrument_service (id, date, note, cashbook_entry_id, instrument_id) FROM stdin;
\.


--
-- TOC entry 4396 (class 0 OID 18528)
-- Dependencies: 464
-- Data for Name: instrument_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.instrument_type (id, name) FROM stdin;
\.


--
-- TOC entry 4367 (class 0 OID 18359)
-- Dependencies: 435
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.jhi_authority (name, role_id, id, write) FROM stdin;
BASE	1	1	t
MANAGEMENT	1	2	t
USER	1	3	t
ROLES	1	4	t
TENANT_CONFIGURATION	1	5	t
BASE	2	6	t
AWARD	3	7	t
MUSIC_BOOK	3	9	t
CASHBOOK	3	11	t
CLOTHING_TYPE	3	12	t
PERSON_INSTRUMENT	3	13	t
INSTRUMENT_SERVICE	3	14	t
PERSON_AWARD	3	15	t
COMPOSER	3	16	t
CLOTHING	3	17	t
CUSTOMIZING	3	19	t
MEMBERSHIP	3	20	t
HONOR	3	21	t
APPOINTMENT_TYPE	3	22	t
APPOINTMENT	3	23	t
ARRANGER	3	24	t
COMPOSITION	3	25	t
PERSON_GROUP	3	26	t
APPOINTMENT_PEOPLE	3	27	t
PERSON_HONOR	3	28	t
MEMBERSHIP_CATEGORY	3	29	t
NOTIFICATION_RULE	3	30	t
INSTRUMENT_TYPE	3	31	t
AKM	3	32	t
PERSON	3	33	t
ICAL	3	34	t
CASHBOOK-IMPORT	3	35	t
PERSON_CLOTHING	3	36	t
GENRE	3	37	t
USER	3	38	t
COLOR_CODE	3	39	t
COMPANY	3	40	t
INSTRUMENT	3	41	t
NOTIFICATION	3	42	t
APPOINTMENT_COMPOSITION	3	43	t
ROLES	3	44	t
\.


--
-- TOC entry 4370 (class 0 OID 18381)
-- Dependencies: 438
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.jhi_persistent_audit_event (event_id, principal, event_date, event_type) FROM stdin;
\.


--
-- TOC entry 4371 (class 0 OID 18387)
-- Dependencies: 439
-- Data for Name: jhi_persistent_audit_evt_data; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.jhi_persistent_audit_evt_data (event_id, name, value) FROM stdin;
\.


--
-- TOC entry 4366 (class 0 OID 18346)
-- Dependencies: 434
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.jhi_user (id, login, password_hash, first_name, last_name, email, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date) FROM stdin;
1	system	$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG	System	System	system@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
2	anonymoususer	$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO	Anonymous	User	anonymous@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
3	admin	$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC	Administrator	Administrator	admin@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
4	user	$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K	User	User	user@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
5	vorstand	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Vorstand	Verein	vorstand@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
6	mitglieder-admin	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Mitglieder-Verwalter	Verein	vorstand1@localhost	t	de	\N	\N	system	2019-01-14 19:19:28.528481	\N	system	\N
\.


--
-- TOC entry 4368 (class 0 OID 18364)
-- Dependencies: 436
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.jhi_user_authority (user_id, role_id) FROM stdin;
1	1
3	1
1	2
3	2
4	2
6	2
5	2
3	3
\.


--
-- TOC entry 4458 (class 0 OID 18830)
-- Dependencies: 526
-- Data for Name: letter; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.letter (id, title, text, payment, current_period, previous_period, template_id) FROM stdin;
\.


--
-- TOC entry 4459 (class 0 OID 18839)
-- Dependencies: 527
-- Data for Name: letter_person_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.letter_person_group (person_groups_id, letters_id) FROM stdin;
\.


--
-- TOC entry 4382 (class 0 OID 18459)
-- Dependencies: 450
-- Data for Name: membership; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.membership (id, begin_date, end_date, person_id, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4384 (class 0 OID 18467)
-- Dependencies: 452
-- Data for Name: membership_fee; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.membership_fee (id, description, period, period_time_fraction) FROM stdin;
\.


--
-- TOC entry 4386 (class 0 OID 18478)
-- Dependencies: 454
-- Data for Name: membership_fee_amount; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.membership_fee_amount (id, begin_date, end_date, amount, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4388 (class 0 OID 18486)
-- Dependencies: 456
-- Data for Name: membership_fee_for_period; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.membership_fee_for_period (id, nr, jhi_year, reference_code, paid, note, cashbook_entry_id, membership_id) FROM stdin;
\.


--
-- TOC entry 4438 (class 0 OID 18731)
-- Dependencies: 506
-- Data for Name: music_book; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.music_book (id, name, description) FROM stdin;
\.


--
-- TOC entry 4464 (class 0 OID 18862)
-- Dependencies: 532
-- Data for Name: notification; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.notification (id, entity_id, created_at, read_by_user_id, notification_rule_id) FROM stdin;
\.


--
-- TOC entry 4461 (class 0 OID 18846)
-- Dependencies: 529
-- Data for Name: notification_rule; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.notification_rule (id, title, entity, conditions, active, message_group, message_single) FROM stdin;
1	Geburtstage im nächsten Monat	Person	SELECT p FROM Person p WHERE MONTH(p.birthDate) = ( MONTH(CURRENT_DATE) + 1 )	t	Im nächsten Monat haben {{count}} Personen Geburtstag.	Im nächsten Monat hat {{entity.firstName}} {{entity.lastName}} Geburtstag (geboren am {{entity.birthDate}})
\.


--
-- TOC entry 4462 (class 0 OID 18855)
-- Dependencies: 530
-- Data for Name: notification_rule_target_audiences; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.notification_rule_target_audiences (notification_rules_id, target_role_id) FROM stdin;
\.


--
-- TOC entry 4450 (class 0 OID 18787)
-- Dependencies: 518
-- Data for Name: official_appointment; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.official_appointment (id, organizer_name, organizer_address, is_head_quota, report_id) FROM stdin;
1	Jugend Wolfsthal	Dorfplatz 1	f	\N
\.


--
-- TOC entry 4373 (class 0 OID 18401)
-- Dependencies: 441
-- Data for Name: person; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person (id, first_name, last_name, birth_date, email, telephone_number, street_address, postal_code, city, country, gender, email_type) FROM stdin;
1	Patrick	Fleck	1991-01-27	patrickflck0@gmail.com	\N	Windmühlstraße 24	2405	Bad Deutsch Altenburg	Österreich	MALE	VERIFIED
2	Theres	Gamsjäger	1989-03-13	\N	\N	Windmühlstraße 34	2405	Bad Deutsch Altenburg	Österreich	FEMALE	VERIFIED
\.


--
-- TOC entry 4414 (class 0 OID 18608)
-- Dependencies: 482
-- Data for Name: person_address; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_address (id, begin_date, end_date, address_id, person_id) FROM stdin;
\.


--
-- TOC entry 4412 (class 0 OID 18600)
-- Dependencies: 480
-- Data for Name: person_award; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_award (id, date, award_id, person_id) FROM stdin;
\.


--
-- TOC entry 4400 (class 0 OID 18546)
-- Dependencies: 468
-- Data for Name: person_clothing; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_clothing (id, begin_date, end_date, clothing_id, person_id) FROM stdin;
\.


--
-- TOC entry 4376 (class 0 OID 18417)
-- Dependencies: 444
-- Data for Name: person_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_group (id, name, parent_id) FROM stdin;
1	Musiker	\N
\.


--
-- TOC entry 4410 (class 0 OID 18592)
-- Dependencies: 478
-- Data for Name: person_honor; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_honor (id, date, honor_id, person_id) FROM stdin;
\.


--
-- TOC entry 4380 (class 0 OID 18451)
-- Dependencies: 448
-- Data for Name: person_info; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_info (id, name) FROM stdin;
\.


--
-- TOC entry 4392 (class 0 OID 18510)
-- Dependencies: 460
-- Data for Name: person_instrument; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_instrument (id, begin_date, end_date, instrument_id, person_id) FROM stdin;
\.


--
-- TOC entry 4374 (class 0 OID 18410)
-- Dependencies: 442
-- Data for Name: person_person_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.person_person_groups (person_groups_id, people_id) FROM stdin;
\.


--
-- TOC entry 4423 (class 0 OID 18654)
-- Dependencies: 491
-- Data for Name: receipt; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.receipt (id, date, note, title, file, identifier, company_id, overwrite_identifier, initial_cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4448 (class 0 OID 18776)
-- Dependencies: 516
-- Data for Name: report; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.report (id, generation_date, association_id, association_name, street_address, postal_code, city) FROM stdin;
\.


--
-- TOC entry 4378 (class 0 OID 18438)
-- Dependencies: 446
-- Data for Name: role; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.role (id, name) FROM stdin;
1	ROLE_ADMIN
2	ROLE_USER
3	ROLE_BOARD_MEMBER
4	ROLE_MEMBER_ADMIN
5	ROLE_ACCOUNTING_ADMIN
6	ROLE_INVENTORY_ADMIN
7	ROLE_BASE_ADMIN
\.


--
-- TOC entry 4456 (class 0 OID 18819)
-- Dependencies: 524
-- Data for Name: template; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.template (id, title, file, margin_left, margin_right, margin_top, margin_bottom) FROM stdin;
\.


--
-- TOC entry 4477 (class 0 OID 19285)
-- Dependencies: 545
-- Data for Name: user_column_config; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.user_column_config (id, visible, "position", column_config_id, user_id) FROM stdin;
1	t	1	1	1
2	t	2	2	1
3	t	3	3	1
4	t	4	4	1
5	t	5	5	1
6	f	1	6	1
7	f	2	7	1
8	t	1	8	1
9	t	2	9	1
10	t	3	10	1
11	f	1	11	1
12	f	2	12	1
13	t	1	13	1
14	t	2	14	1
15	t	3	15	1
16	t	4	16	1
17	t	5	17	1
18	t	1	18	1
19	t	2	19	1
20	t	3	20	1
21	t	4	21	1
22	t	5	22	1
23	t	6	23	1
24	f	1	24	1
25	t	1	25	1
26	t	2	26	1
27	t	3	27	1
28	t	4	28	1
29	f	1	29	1
30	f	2	30	1
31	f	3	31	1
32	f	4	32	1
33	f	5	33	1
34	t	1	1	2
35	t	2	2	2
36	t	3	3	2
37	t	4	4	2
38	t	5	5	2
39	f	1	6	2
40	f	2	7	2
41	t	1	8	2
42	t	2	9	2
43	t	3	10	2
44	f	1	11	2
45	f	2	12	2
46	t	1	13	2
47	t	2	14	2
48	t	3	15	2
49	t	4	16	2
50	t	5	17	2
51	t	1	18	2
52	t	2	19	2
53	t	3	20	2
54	t	4	21	2
55	t	5	22	2
56	t	6	23	2
57	f	1	24	2
58	t	1	25	2
59	t	2	26	2
60	t	3	27	2
61	t	4	28	2
62	f	1	29	2
63	f	2	30	2
64	f	3	31	2
65	f	4	32	2
66	f	5	33	2
67	t	1	1	3
68	t	2	2	3
69	t	3	3	3
70	t	4	4	3
71	t	5	5	3
72	f	1	6	3
73	f	2	7	3
74	t	1	8	3
75	t	2	9	3
76	t	3	10	3
77	f	1	11	3
79	t	1	13	3
80	t	2	14	3
81	t	3	15	3
82	t	4	16	3
83	t	5	17	3
84	t	1	18	3
85	t	2	19	3
86	t	3	20	3
87	t	4	21	3
88	t	5	22	3
89	t	6	23	3
90	f	1	24	3
91	t	1	25	3
92	t	2	26	3
93	t	3	27	3
94	t	4	28	3
100	t	1	1	4
101	t	2	2	4
102	t	3	3	4
103	t	4	4	4
104	t	5	5	4
105	f	1	6	4
106	f	2	7	4
107	t	1	8	4
108	t	2	9	4
109	t	3	10	4
110	f	1	11	4
111	f	2	12	4
112	t	1	13	4
113	t	2	14	4
114	t	3	15	4
115	t	4	16	4
116	t	5	17	4
117	t	1	18	4
118	t	2	19	4
119	t	3	20	4
120	t	4	21	4
121	t	5	22	4
122	t	6	23	4
123	f	1	24	4
124	t	1	25	4
125	t	2	26	4
126	t	3	27	4
127	t	4	28	4
128	f	1	29	4
129	f	2	30	4
130	f	3	31	4
131	f	4	32	4
132	f	5	33	4
133	t	1	1	5
134	t	2	2	5
135	t	3	3	5
136	t	4	4	5
78	t	4	12	3
137	t	5	5	5
138	f	1	6	5
139	f	2	7	5
140	t	1	8	5
141	t	2	9	5
142	t	3	10	5
143	f	1	11	5
144	f	2	12	5
145	t	1	13	5
146	t	2	14	5
147	t	3	15	5
148	t	4	16	5
149	t	5	17	5
150	t	1	18	5
151	t	2	19	5
152	t	3	20	5
153	t	4	21	5
154	t	5	22	5
155	t	6	23	5
156	f	1	24	5
157	t	1	25	5
158	t	2	26	5
159	t	3	27	5
160	t	4	28	5
161	f	1	29	5
162	f	2	30	5
163	f	3	31	5
164	f	4	32	5
165	f	5	33	5
166	t	1	1	6
167	t	2	2	6
168	t	3	3	6
169	t	4	4	6
170	t	5	5	6
171	f	1	6	6
172	f	2	7	6
173	t	1	8	6
174	t	2	9	6
175	t	3	10	6
176	f	1	11	6
177	f	2	12	6
178	t	1	13	6
179	t	2	14	6
180	t	3	15	6
181	t	4	16	6
182	t	5	17	6
183	t	1	18	6
184	t	2	19	6
185	t	3	20	6
186	t	4	21	6
187	t	5	22	6
188	t	6	23	6
189	f	1	24	6
190	t	1	25	6
191	t	2	26	6
192	t	3	27	6
193	t	4	28	6
194	f	1	29	6
195	f	2	30	6
196	f	3	31	6
197	f	4	32	6
198	f	5	33	6
96	f	1	30	3
97	f	2	31	3
98	f	3	32	3
99	f	4	33	3
95	f	5	29	3
\.


--
-- TOC entry 4274 (class 0 OID 17528)
-- Dependencies: 342
-- Data for Name: address; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.address (id, street_address, postal_code, city, state) FROM stdin;
\.


--
-- TOC entry 4312 (class 0 OID 17710)
-- Dependencies: 380
-- Data for Name: appointment; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.appointment (id, name, description, street_address, postal_code, city, country, begin_date, end_date, official_appointment_id, type_id) FROM stdin;
\.


--
-- TOC entry 4313 (class 0 OID 17721)
-- Dependencies: 381
-- Data for Name: appointment_persons; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.appointment_persons (persons_id, appointments_id) FROM stdin;
\.


--
-- TOC entry 4315 (class 0 OID 17728)
-- Dependencies: 383
-- Data for Name: appointment_type; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.appointment_type (id, name, is_official, person_group_id) FROM stdin;
\.


--
-- TOC entry 4329 (class 0 OID 17792)
-- Dependencies: 397
-- Data for Name: arranger; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.arranger (id, name) FROM stdin;
\.


--
-- TOC entry 4330 (class 0 OID 17798)
-- Dependencies: 398
-- Data for Name: arranger_compositions; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.arranger_compositions (compositions_id, arrangers_id) FROM stdin;
\.


--
-- TOC entry 4292 (class 0 OID 17610)
-- Dependencies: 360
-- Data for Name: award; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.award (id, name, descripton) FROM stdin;
\.


--
-- TOC entry 4338 (class 0 OID 17835)
-- Dependencies: 406
-- Data for Name: bank_import_data; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.bank_import_data (id, entry_date, entry_value, entry_text, partner_name, entry_reference, bank_importer_type, cashbook_entry_id, cashbook_category_id) FROM stdin;
\.


--
-- TOC entry 4300 (class 0 OID 17645)
-- Dependencies: 368
-- Data for Name: cashbook; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.cashbook (id, name, description) FROM stdin;
\.


--
-- TOC entry 4352 (class 0 OID 17910)
-- Dependencies: 420
-- Data for Name: cashbook_account; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.cashbook_account (id, name, bank_account, bank_importer_type, receipt_code, current_number) FROM stdin;
\.


--
-- TOC entry 4301 (class 0 OID 17654)
-- Dependencies: 369
-- Data for Name: cashbook_cashbook_entries; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.cashbook_cashbook_entries (cashbook_entries_id, cashbooks_id) FROM stdin;
\.


--
-- TOC entry 4305 (class 0 OID 17672)
-- Dependencies: 373
-- Data for Name: cashbook_category; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.cashbook_category (id, name, description, color, parent_id) FROM stdin;
\.


--
-- TOC entry 4303 (class 0 OID 17661)
-- Dependencies: 371
-- Data for Name: cashbook_entry; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.cashbook_entry (id, title, type, date, amount, appointment_id, cashbook_category_id, receipt_id, cashbook_account_id) FROM stdin;
\.


--
-- TOC entry 4286 (class 0 OID 17583)
-- Dependencies: 354
-- Data for Name: clothing; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.clothing (id, size, purchase_date, not_available, type_id) FROM stdin;
\.


--
-- TOC entry 4336 (class 0 OID 17827)
-- Dependencies: 404
-- Data for Name: clothing_group; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.clothing_group (id, size, quantity, quantity_available, type_id) FROM stdin;
\.


--
-- TOC entry 4288 (class 0 OID 17591)
-- Dependencies: 356
-- Data for Name: clothing_type; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.clothing_type (id, name) FROM stdin;
\.


--
-- TOC entry 4320 (class 0 OID 17752)
-- Dependencies: 388
-- Data for Name: color_code; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.color_code (id, name) FROM stdin;
\.


--
-- TOC entry 4359 (class 0 OID 18303)
-- Dependencies: 427
-- Data for Name: column_config; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.column_config (id, entity, column_name, default_visible, default_position, column_display_name, sort_by_name) FROM stdin;
1	COMPOSITION	colorCodeName	t	1	walterApp.composition.colorCode	colorCode.name
2	COMPOSITION	nr	t	2	walterApp.composition.nr	nr
3	COMPOSITION	title	t	3	walterApp.composition.title	title
4	COMPOSITION	genreName	t	4	walterApp.composition.genre	genre.name
5	COMPOSITION	musicBookName	t	5	walterApp.composition.musicBook	musicBook.name
6	COMPOSITION	orderingCompanyName	f	1	walterApp.composition.orderingCompany	orderingCompany.name
7	COMPOSITION	publisherName	f	2	walterApp.composition.publisher	publisher.name
8	PERSON	firstName	t	1	walterApp.person.firstName	firstName
9	PERSON	lastName	t	2	walterApp.person.lastName	lastName
10	PERSON	birthDate	t	3	walterApp.person.birthDate	birthDate
11	PERSON	gender	f	1	walterApp.person.gender	gender
12	PERSON	email	f	2	walterApp.person.email	email
13	INSTRUMENT	name	t	1	walterApp.instrument.name	name
14	INSTRUMENT	price	t	2	walterApp.instrument.price	price
15	INSTRUMENT	purchaseDate	t	3	walterApp.instrument.purchaseDate	purchaseDate
16	INSTRUMENT	privateInstrument	t	4	walterApp.instrument.privateInstrument	privateInstrument
17	INSTRUMENT	typeName	t	5	walterApp.instrument.type	typeName
18	CASHBOOK-ENTRY	date	t	1	walterApp.cashbookEntry.date	date
19	CASHBOOK-ENTRY	title	t	2	walterApp.cashbookEntry.title	title
20	CASHBOOK-ENTRY	receiptIdentifier	t	3	walterApp.cashbookEntry.receipt	receipt.identifier
21	CASHBOOK-ENTRY	cashbookCategoryName	t	4	walterApp.cashbookEntry.cashbookCategory	cashbookCategory.name
22	CASHBOOK-ENTRY	amount	t	5	walterApp.CashbookEntryType.INCOME	
23	CASHBOOK-ENTRY	amount	t	6	walterApp.CashbookEntryType.SPENDING	
24	CASHBOOK-ENTRY	cashbookAccountName	f	1	walterApp.cashbookEntry.cashbookAccount	cashbookAccount.name
25	APPOINTMENT	beginDate	t	1	walterApp.appointment.beginDate	beginDate
26	APPOINTMENT	endDate	t	2	walterApp.appointment.endDate	endDate
27	APPOINTMENT	name	t	3	walterApp.appointment.name	name
28	APPOINTMENT	typeName	t	4	walterApp.appointment.type	appointmentType.name
29	APPOINTMENT	description	f	1	walterApp.appointment.description	description
30	APPOINTMENT	streetAddress	f	2	walterApp.appointment.streetAddress	streetAddress
31	APPOINTMENT	postalCode	f	3	walterApp.appointment.postalCode	postalCode
32	APPOINTMENT	city	f	4	walterApp.appointment.city	city
33	APPOINTMENT	country	f	5	walterApp.appointment.country	country
\.


--
-- TOC entry 4309 (class 0 OID 17694)
-- Dependencies: 377
-- Data for Name: company; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.company (id, name, street_address, postal_code, city, country, recipient) FROM stdin;
\.


--
-- TOC entry 4310 (class 0 OID 17703)
-- Dependencies: 378
-- Data for Name: company_person_groups; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.company_person_groups (person_groups_id, companies_id) FROM stdin;
\.


--
-- TOC entry 4326 (class 0 OID 17779)
-- Dependencies: 394
-- Data for Name: composer; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.composer (id, name) FROM stdin;
\.


--
-- TOC entry 4327 (class 0 OID 17785)
-- Dependencies: 395
-- Data for Name: composer_compositions; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.composer_compositions (compositions_id, composers_id) FROM stdin;
\.


--
-- TOC entry 4317 (class 0 OID 17736)
-- Dependencies: 385
-- Data for Name: composition; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.composition (id, nr, title, note, akm_composition_nr, cashbook_entry_id, ordering_company_id, publisher_id, genre_id, music_book_id, color_code_id) FROM stdin;
\.


--
-- TOC entry 4318 (class 0 OID 17745)
-- Dependencies: 386
-- Data for Name: composition_appointments; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.composition_appointments (appointments_id, compositions_id) FROM stdin;
\.


--
-- TOC entry 4350 (class 0 OID 17899)
-- Dependencies: 418
-- Data for Name: customization; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.customization (id, name, data, text) FROM stdin;
2	MEMBER_PERSONGROUP	0	
3	DEACTIVATED_FEATUREGROUPS	[]	
4	AKM_DATA	{}	
5	HOURS_TO_INVALIDATE_VERIFICATION	48	
\.


--
-- TOC entry 4247 (class 0 OID 17365)
-- Dependencies: 315
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels) FROM stdin;
00000000000000	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:19:13.538402	1	EXECUTED	7:eda8cd7fd15284e6128be97bd8edea82	createSequence		\N	3.4.2	\N	\N
00000000000001	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-01-14 19:19:13.710488	2	EXECUTED	7:b9be43015ddcfe6cc0173fe720f5a3ec	createTable, createIndex (x2), createTable (x2), addPrimaryKey, addForeignKeyConstraint (x2), loadData, dropDefaultValue, loadData (x2), createTable (x2), addPrimaryKey, createIndex (x2), addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104914-1	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_Person.xml	2019-01-14 19:19:13.772519	3	EXECUTED	7:f9a5278625d3f0c2031c364518a6f220	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104915-1	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_PersonGroup.xml	2019-01-14 19:19:13.794527	4	EXECUTED	7:e17bca58c36e5e044deab7534eb463ff	createTable		\N	3.4.2	\N	\N
20161102104916-1	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_WalterUser.xml	2019-01-14 19:19:13.838547	5	EXECUTED	7:05aafd4a1058b1bd1fa2e91c57c3b38d	createTable		\N	3.4.2	\N	\N
20161102104917-1	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_Role.xml	2019-01-14 19:19:13.868564	6	EXECUTED	7:b0fb658f2c19f8e68374c33846e78b24	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104918-1	jhipster	classpath:config/liquibase/changelog/20161102104918_added_entity_PersonInfo.xml	2019-01-14 19:19:13.891574	7	EXECUTED	7:37371b6f98f2929dbd78eab5351c882e	createTable		\N	3.4.2	\N	\N
20161102104919-1	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_Membership.xml	2019-01-14 19:19:13.911581	8	EXECUTED	7:7022f3fb55947358348baa4efd892655	createTable		\N	3.4.2	\N	\N
20161102104920-1	jhipster	classpath:config/liquibase/changelog/20161102104920_added_entity_MembershipFee.xml	2019-01-14 19:19:13.935596	9	EXECUTED	7:22e533231a2ba6fcb4ed987fc8957432	createTable		\N	3.4.2	\N	\N
20161102104921-1	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_MembershipFeeAmount.xml	2019-01-14 19:19:13.961609	10	EXECUTED	7:6bb3458e477095b053e7ae921e26de4d	createTable		\N	3.4.2	\N	\N
20161102104922-1	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_MembershipFeeForPeriod.xml	2019-01-14 19:19:13.993623	11	EXECUTED	7:e1591f04339e27e9b024c598c5633905	createTable		\N	3.4.2	\N	\N
20161102104923-1	jhipster	classpath:config/liquibase/changelog/20161102104923_added_entity_Address.xml	2019-01-14 19:19:14.019636	12	EXECUTED	7:901335603ea0d0197968a7fcfc2d36a1	createTable		\N	3.4.2	\N	\N
20161102104924-1	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_PersonInstrument.xml	2019-01-14 19:19:14.038646	13	EXECUTED	7:dfb8c080f3bcdd58cd6d369297feccdf	createTable		\N	3.4.2	\N	\N
20161102104925-1	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_Instrument.xml	2019-01-14 19:19:14.065657	14	EXECUTED	7:d404e23102c8268532fd0a1fcda881ba	createTable		\N	3.4.2	\N	\N
20161102104926-1	jhipster	classpath:config/liquibase/changelog/20161102104926_added_entity_InstrumentType.xml	2019-01-14 19:19:14.084665	15	EXECUTED	7:5ca4785573c595749d48676b920e2b35	createTable		\N	3.4.2	\N	\N
20161102104927-1	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_InstrumentService.xml	2019-01-14 19:19:14.11168	16	EXECUTED	7:08ed231981c6842b34551314095aa782	createTable		\N	3.4.2	\N	\N
20161102104928-1	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_PersonClothing.xml	2019-01-14 19:19:14.127692	17	EXECUTED	7:fa3796f1649cc12f659ed9f61b1b9bdf	createTable		\N	3.4.2	\N	\N
20161102104929-1	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_Clothing.xml	2019-01-14 19:19:14.148699	18	EXECUTED	7:54aa0caf46fa900362aded1b3f2d371a	createTable		\N	3.4.2	\N	\N
20161102104930-1	jhipster	classpath:config/liquibase/changelog/20161102104930_added_entity_ClothingType.xml	2019-01-14 19:19:14.167779	19	EXECUTED	7:5dada8ca39d16a26bafaf105ca46db8e	createTable		\N	3.4.2	\N	\N
20161102104931-1	jhipster	classpath:config/liquibase/changelog/20161102104931_added_entity_Honor.xml	2019-01-14 19:19:14.19272	20	EXECUTED	7:52f16f4cbf9599f8a2f206c40890cf12	createTable		\N	3.4.2	\N	\N
20161102104932-1	jhipster	classpath:config/liquibase/changelog/20161102104932_added_entity_Award.xml	2019-01-14 19:19:14.220734	21	EXECUTED	7:2b50b6a3b36da0e2890627fde009b833	createTable		\N	3.4.2	\N	\N
20161102104933-1	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_PersonHonor.xml	2019-01-14 19:19:14.235744	22	EXECUTED	7:988f3ea4fbb6eff4d455512cd105e8b2	createTable		\N	3.4.2	\N	\N
20161102104934-1	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_PersonAward.xml	2019-01-14 19:19:14.252749	23	EXECUTED	7:6854b28e0d1e31b2fa8f50a3fca05321	createTable		\N	3.4.2	\N	\N
20161102104935-1	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_PersonAddress.xml	2019-01-14 19:19:14.270758	24	EXECUTED	7:a2321f6ae1eb12f070558f851866800f	createTable		\N	3.4.2	\N	\N
20161102104936-1	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_Cashbook.xml	2019-01-14 19:19:14.307778	25	EXECUTED	7:0192ec399e1521a58cd2b85d4e0cef41	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104937-1	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_CashbookEntry.xml	2019-01-14 19:19:14.337793	26	EXECUTED	7:29dd4da295fce1393dccd54b90e015ed	createTable		\N	3.4.2	\N	\N
20161102104938-1	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_CashbookCategory.xml	2019-01-14 19:19:14.369811	27	EXECUTED	7:d2608f5e4b02701e2d9d6ea3194c7634	createTable		\N	3.4.2	\N	\N
20161102104939-1	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_Receipt.xml	2019-01-14 19:19:14.395821	28	EXECUTED	7:d8186d6ed7d45b615d7c556aa5158825	createTable		\N	3.4.2	\N	\N
20161102104940-1	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_Company.xml	2019-01-14 19:19:14.454848	29	EXECUTED	7:6b77cafdde0392639a51d6893de98e7d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104941-1	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_Appointment.xml	2019-01-14 19:19:14.509876	30	EXECUTED	7:077a2164c35698bc753460fd27eb1980	createTable, dropDefaultValue (x2), createTable, addPrimaryKey		\N	3.4.2	\N	\N
20161102104942-1	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_AppointmentType.xml	2019-01-14 19:19:14.532886	31	EXECUTED	7:7f25c9f4fd7051bc46d3871488a6eace	createTable		\N	3.4.2	\N	\N
20161102104943-1	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_Composition.xml	2019-01-14 19:19:14.578911	32	EXECUTED	7:269d44883ecb2c77afa1d8e758c46ecf	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104944-1	jhipster	classpath:config/liquibase/changelog/20161102104944_added_entity_ColorCode.xml	2019-01-14 19:19:14.598919	33	EXECUTED	7:5f9754f91d90ece349915983e984085c	createTable		\N	3.4.2	\N	\N
20161102104945-1	jhipster	classpath:config/liquibase/changelog/20161102104945_added_entity_MusicBook.xml	2019-01-14 19:19:14.627943	34	EXECUTED	7:3ed82be4541ab8c3fd45d07930b7395d	createTable		\N	3.4.2	\N	\N
20161102104946-1	jhipster	classpath:config/liquibase/changelog/20161102104946_added_entity_Genre.xml	2019-01-14 19:19:14.644939	35	EXECUTED	7:a0c6c19aa664b40a5af2a57db0ce57b2	createTable		\N	3.4.2	\N	\N
20161102104947-1	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_Composer.xml	2019-01-14 19:19:14.669954	36	EXECUTED	7:31e2a0aeb10819ff47c990c2a6508cc9	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104948-1	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_Arranger.xml	2019-01-14 19:19:14.702968	37	EXECUTED	7:fa875605d756b8c4ad811d6e3aead79d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104949-1	jhipster	classpath:config/liquibase/changelog/20161102104949_added_entity_Report.xml	2019-01-14 19:19:14.726982	38	EXECUTED	7:3678897ecea8ea9f19faa890389fb785	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20161113155740-1	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_OfficialAppointment.xml	2019-01-14 19:19:14.751991	39	EXECUTED	7:6821b236618c98ce49c5c77cab7796d7	createTable		\N	3.4.2	\N	\N
20161214053409-1	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_ClothingGroup.xml	2019-01-14 19:19:14.773002	40	EXECUTED	7:f6c8a7bdd4e1ea562d5a3337885ababc	createTable		\N	3.4.2	\N	\N
20170104210724-1	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_BankImportData.xml	2019-01-14 19:19:14.807018	41	EXECUTED	7:71b0a5b780302dadfd482b433724b74c	createTable		\N	3.4.2	\N	\N
20170104190142-1	jhipster	classpath:config/liquibase/changelog/20170104190142_added_entity_Template.xml	2019-01-14 19:19:14.834033	42	EXECUTED	7:26b54acd02768919aafc83838dd85a55	createTable		\N	3.4.2	\N	\N
20170104190709-1	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_Letter.xml	2019-01-14 19:19:14.866144	43	EXECUTED	7:3ed461b478f749dd060394fca492e250	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122114257-1	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_NotificationRule.xml	2019-01-14 19:19:14.905068	44	EXECUTED	7:b5e036d48a87e243f25312fea04e341f	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122115258-1	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_Notification.xml	2019-01-14 19:19:14.922073	45	EXECUTED	7:7faa054b9dc60d5ab4ddb11f0926891c	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20170930130601-1	jhipster	classpath:config/liquibase/changelog/20170930130601_added_entity_Customization.xml	2019-01-14 19:19:14.945089	46	EXECUTED	7:769cc51ccd6e1c24ac182213fc19c2c9	createTable		\N	3.4.2	\N	\N
20171030171006-1	jhipster	classpath:config/liquibase/changelog/20171030171006_added_entity_CashbookAccount.xml	2019-01-14 19:19:14.968101	47	EXECUTED	7:700413e98436a448c213c16e33ad285f	createTable		\N	3.4.2	\N	\N
20161102104914-2	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_constraints_Person.xml	2019-01-14 19:19:14.987108	48	EXECUTED	7:88759aeb6a26c2d102adca65c5c0a01a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104915-2	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_constraints_PersonGroup.xml	2019-01-14 19:19:14.995109	49	EXECUTED	7:21b983b33da2aa2ffc57ee6555ca5696	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104916-2	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_constraints_WalterUser.xml	2019-01-14 19:19:15.006115	50	EXECUTED	7:8276b656b3639d4bf6e7aff7ba06dbfc	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104917-2	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_constraints_Role.xml	2019-01-14 19:19:15.027126	51	EXECUTED	7:405ae59b31b851353ce6135e9e49032a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104919-2	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_constraints_Membership.xml	2019-01-14 19:19:15.041133	52	EXECUTED	7:7f9df05df792e84d585e3a87791200fb	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104921-2	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_constraints_MembershipFeeAmount.xml	2019-01-14 19:19:15.052137	53	EXECUTED	7:87d4d44d7570eeb06613cd9ff7980a38	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104922-2	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_constraints_MembershipFeeForPeriod.xml	2019-01-14 19:19:15.066144	54	EXECUTED	7:64c63ba937f2e90697f666693f8d97af	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104924-2	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_constraints_PersonInstrument.xml	2019-01-14 19:19:15.086155	55	EXECUTED	7:9896da86e9715a890aa1000012c3bb06	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104925-2	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_constraints_Instrument.xml	2019-01-14 19:19:15.100163	56	EXECUTED	7:35f47a04edd6853298522c7565ab3bd7	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104927-2	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_constraints_InstrumentService.xml	2019-01-14 19:19:15.115166	57	EXECUTED	7:86e3a9f61ffc8d438597e086d9db6fc2	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104928-2	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_constraints_PersonClothing.xml	2019-01-14 19:19:15.130176	58	EXECUTED	7:20e0c2c6ed2ee7e95745bfd00d4ac6a1	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104929-2	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_constraints_Clothing.xml	2019-01-14 19:19:15.141179	59	EXECUTED	7:a0aad378f7097d9619b68d6246d971c5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104933-2	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_constraints_PersonHonor.xml	2019-01-14 19:19:15.1582	60	EXECUTED	7:0dc636021a5fa29f7ffc57df0f0f1804	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104934-2	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_constraints_PersonAward.xml	2019-01-14 19:19:15.173196	61	EXECUTED	7:5c4037e9e9eb6843362f3636cee3365b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104935-2	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_constraints_PersonAddress.xml	2019-01-14 19:19:15.189205	62	EXECUTED	7:f852f2459b9b5b6fcf3ecc5bdf33ee13	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104936-2	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_constraints_Cashbook.xml	2019-01-14 19:19:15.203219	63	EXECUTED	7:d3ccfe4859720998a3563b705d361d90	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104937-2	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_constraints_CashbookEntry.xml	2019-01-14 19:19:15.235225	64	EXECUTED	7:89905ed52236d1b70d976c728388902a	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104938-2	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_constraints_CashbookCategory.xml	2019-01-14 19:19:15.244229	65	EXECUTED	7:d2bcaebedffa74fd5dcc7cdb4b2074b5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104939-2	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_constraints_Receipt.xml	2019-01-14 19:19:15.253235	66	EXECUTED	7:9ed379388af0889d05d4d9c44d8d5523	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104940-2	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_constraints_Company.xml	2019-01-14 19:19:15.26524	67	EXECUTED	7:38fadcf668fde47fd50887e3061270e4	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104941-2	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_constraints_Appointment.xml	2019-01-14 19:19:15.293253	68	EXECUTED	7:7788ef135fd9f5af1ce4792ef95fff69	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104942-2	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_constraints_AppointmentType.xml	2019-01-14 19:19:15.302326	69	EXECUTED	7:7eef97fb0ecb478a954685a99a37305c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104943-2	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_constraints_Composition.xml	2019-01-14 19:19:15.343279	70	EXECUTED	7:da9379c2b1e784bc095b0004949cc923	addForeignKeyConstraint (x8)		\N	3.4.2	\N	\N
20161102104947-2	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_constraints_Composer.xml	2019-01-14 19:19:15.372292	71	EXECUTED	7:6fe0805e298261fd2d92060ae88af2a5	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104948-2	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_constraints_Arranger.xml	2019-01-14 19:19:15.389302	72	EXECUTED	7:f89f75ef2ffe3d7e2f0c9a8059b0e267	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161113155740-2	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_constraints_OfficialAppointment.xml	2019-01-14 19:19:15.398308	73	EXECUTED	7:6f06af06f73b7e1c13230f87db52be0c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161214053409-2	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_constraints_ClothingGroup.xml	2019-01-14 19:19:15.482347	74	EXECUTED	7:5e6bb7daa6a9f43e0e42a56df4e7e53e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170104210724-2	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_constraints_BankImportData.xml	2019-01-14 19:19:15.495353	75	EXECUTED	7:bdb156dd17d8e9c58a28cecc3df1221e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170122114257-2	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_constraints_NotificationRule.xml	2019-01-14 19:19:15.514367	76	EXECUTED	7:e67d6025922b570718da7976ce33b22b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170122115258-2	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_constraints_Notification.xml	2019-01-14 19:19:15.540375	77	EXECUTED	7:4ccc834b3f211eaa544371ed31013737	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170104190709-2	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_constraints_Letter.xml	2019-01-14 19:19:15.563386	78	EXECUTED	7:cd53991884ecf839288a05c7e51fdca6	addForeignKeyConstraint (x3)		\N	3.4.2	\N	\N
20171001073000-1	fonkwill	classpath:config/liquibase/changelog/20171001073000_added_entity_constraints_Customization.xml	2019-01-14 19:19:15.574395	79	EXECUTED	7:f7567b71347f418f89c20f8ffcae1e80	addUniqueConstraint		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Customization.xml	2019-01-14 19:19:15.584396	80	EXECUTED	7:a2924d761292b8776126b9fae0fed6ac	loadData		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Notification_Rule.xml	2019-01-14 19:19:15.592402	81	EXECUTED	7:2694452973080e16bcd9c41dcc3b8500	loadData		\N	3.4.2	\N	\N
201809222213	fonkwill	classpath:config/liquibase/changelog/201809222213_added_database_tag_v10.xml	2019-01-14 19:19:15.597413	82	EXECUTED	7:c0a27752fc13556fc6746108e7b21be9	tagDatabase		v1.0	3.4.2	\N	\N
201809280957	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:15.622414	83	EXECUTED	7:0aea42cc1bc0e506264e6740187aced2	dropForeignKeyConstraint (x3), dropTable (x2)		\N	3.4.2	\N	\N
201809280957-2	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:15.668448	84	EXECUTED	7:bf918496ecdd20e49ba05c8936668a63	delete, loadData, addColumn, update (x2), dropPrimaryKey, addPrimaryKey, addForeignKeyConstraint, dropColumn		\N	3.4.2	\N	\N
201809280957-3	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-01-14 19:19:15.750478	85	EXECUTED	7:f0cb4324e76f152007ddf4ac5c66d9c4	delete, addColumn (x3), addUniqueConstraint, dropForeignKeyConstraint, dropPrimaryKey, dropColumn, addColumn, addNotNullConstraint, addPrimaryKey, addForeignKeyConstraint, dropPrimaryKey, addPrimaryKey, loadData		\N	3.4.2	\N	\N
20181030171007	jhipster	classpath:config/liquibase/changelog/20181030171007_added_fields_entity_CashbookAccount.xml	2019-01-14 19:19:15.773488	86	EXECUTED	7:36f0be8f5225fbb0432ab67a2385b563	addColumn, sql, addUniqueConstraint, addNotNullConstraint, addColumn, addNotNullConstraint		\N	3.4.2	\N	\N
20181030171008	jhipster	classpath:config/liquibase/changelog/20181030171008_added_fields_entity_BankImportData.xml	2019-01-14 19:19:15.780489	87	EXECUTED	7:c437869a48070aa46b9ba00e83962521	addColumn, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181030171009	jhipster	classpath:config/liquibase/changelog/20181030171009_added_fields_entity_Receipt.xml	2019-01-14 19:19:15.798513	88	EXECUTED	7:c0b6024f7b5d77f7a807906386f9e21c	addColumn (x2), sql, addNotNullConstraint, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181116150506-1	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_EmailVerificationLink.xml	2019-01-14 19:19:15.818514	89	EXECUTED	7:8b5960e5d3de2a3721bac81fe2301885	createTable, dropDefaultValue (x2)		\N	3.4.2	\N	\N
20181116150506-2	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_constraints_EmailVerificationLink.xml	2019-01-14 19:19:15.829523	90	EXECUTED	7:2df6a8baf651650337e85e89c0fc7f0b	addForeignKeyConstraint		\N	3.4.2	\N	\N
20181201171009	jhipster	classpath:config/liquibase/changelog/20181201171009_added_fields_entity_Person.xml	2019-01-14 19:19:15.835519	91	EXECUTED	7:283254850972190046fc015604c56808	addColumn		\N	3.4.2	\N	\N
20181209160512-1	jhipster	classpath:config/liquibase/changelog/20181209160512_added_entity_Filter.xml	2019-01-14 19:19:15.86053	92	EXECUTED	7:a47059f41a8b45f602684fca638eb32c	createTable		\N	3.4.2	\N	\N
20181214184940-1	jhipster	classpath:config/liquibase/changelog/20181214184940_added_entity_ColumnConfig.xml	2019-01-14 19:19:15.886572	93	EXECUTED	7:f742d2b11b2e2cb3b75a413177f86481	createTable		\N	3.4.2	\N	\N
20181214184959	jhipster	classpath:config/liquibase/changelog/20181214184959_added_ColumnConfig_Compositions.xml	2019-01-14 19:19:15.904558	94	EXECUTED	7:a60a23361bcd5e5b40cff16faef538c1	insert (x7)		\N	3.4.2	\N	\N
20181214185854-1	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_UserColumnConfig.xml	2019-01-14 19:19:15.921565	95	EXECUTED	7:123261b8b8410ab942c2832d60496343	createTable		\N	3.4.2	\N	\N
20181214185854-2	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_constraints_UserColumnConfig.xml	2019-01-14 19:19:15.936571	96	EXECUTED	7:a4d8cb578d5753329dc0afd908936e08	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20190501140300-0	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:19:15.940568	97	EXECUTED	7:304971ccf8e6b35a933685d572ec99d9	delete		\N	3.4.2	\N	\N
20190501140300-1	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-01-14 19:19:15.947576	98	EXECUTED	7:2cb713628de375c86d9f32b66fbf220c	loadData		\N	3.4.2	\N	\N
20190109193459	jhipster	classpath:config/liquibase/changelog/20190109193459_added_ColumnConfig_Person.xml	2019-01-14 19:19:15.960583	99	EXECUTED	7:547b36312f7cc468f198695b357ff71b	insert (x5)		\N	3.4.2	\N	\N
20190111103059	jhipster	classpath:config/liquibase/changelog/20190111103059_added_ColumnConfig_Instrument.xml	2019-01-14 19:19:15.974592	100	EXECUTED	7:7d876ee688d09650d4fc5190aaa0f511	insert (x5)		\N	3.4.2	\N	\N
20190113105859	jhipster	classpath:config/liquibase/changelog/20190113105859_added_ColumnConfig_CashbookEntry.xml	2019-01-14 19:19:15.9906	101	EXECUTED	7:b553c8721b4cbdc0d1ddcdc7b50dfabd	insert (x7)		\N	3.4.2	\N	\N
20190113132859	jhipster	classpath:config/liquibase/changelog/20190113132859_added_ColumnConfig_Appointment.xml	2019-01-14 19:19:16.008611	102	EXECUTED	7:f98b2a57358b4d92f1c57e3f085a57bc	insert (x9)		\N	3.4.2	\N	\N
\.


--
-- TOC entry 4246 (class 0 OID 17360)
-- Dependencies: 314
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- TOC entry 4355 (class 0 OID 18278)
-- Dependencies: 423
-- Data for Name: email_verification_link; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.email_verification_link (id, secret_key, creation_date, validation_date, invalid, person_id) FROM stdin;
\.


--
-- TOC entry 4357 (class 0 OID 18292)
-- Dependencies: 425
-- Data for Name: filter; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.filter (id, list_name, name, filter) FROM stdin;
\.


--
-- TOC entry 4324 (class 0 OID 17771)
-- Dependencies: 392
-- Data for Name: genre; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.genre (id, name) FROM stdin;
\.


--
-- TOC entry 4290 (class 0 OID 17599)
-- Dependencies: 358
-- Data for Name: honor; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.honor (id, name, description) FROM stdin;
\.


--
-- TOC entry 4278 (class 0 OID 17547)
-- Dependencies: 346
-- Data for Name: instrument; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.instrument (id, name, purchase_date, private_instrument, price, type_id, cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4282 (class 0 OID 17565)
-- Dependencies: 350
-- Data for Name: instrument_service; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.instrument_service (id, date, note, cashbook_entry_id, instrument_id) FROM stdin;
\.


--
-- TOC entry 4280 (class 0 OID 17557)
-- Dependencies: 348
-- Data for Name: instrument_type; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.instrument_type (id, name) FROM stdin;
\.


--
-- TOC entry 4251 (class 0 OID 17388)
-- Dependencies: 319
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.jhi_authority (name, role_id, id, write) FROM stdin;
BASE	1	1	t
MANAGEMENT	1	2	t
USER	1	3	t
ROLES	1	4	t
TENANT_CONFIGURATION	1	5	t
BASE	2	6	t
\.


--
-- TOC entry 4254 (class 0 OID 17410)
-- Dependencies: 322
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.jhi_persistent_audit_event (event_id, principal, event_date, event_type) FROM stdin;
\.


--
-- TOC entry 4255 (class 0 OID 17416)
-- Dependencies: 323
-- Data for Name: jhi_persistent_audit_evt_data; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.jhi_persistent_audit_evt_data (event_id, name, value) FROM stdin;
\.


--
-- TOC entry 4250 (class 0 OID 17375)
-- Dependencies: 318
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.jhi_user (id, login, password_hash, first_name, last_name, email, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date) FROM stdin;
1	system	$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG	System	System	system@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
2	anonymoususer	$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO	Anonymous	User	anonymous@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
3	admin	$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC	Administrator	Administrator	admin@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
4	user	$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K	User	User	user@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
5	vorstand	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Vorstand	Verein	vorstand@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
6	mitglieder-admin	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Mitglieder-Verwalter	Verein	vorstand1@localhost	t	de	\N	\N	system	2019-01-14 19:19:13.545412	\N	system	\N
\.


--
-- TOC entry 4252 (class 0 OID 17393)
-- Dependencies: 320
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.jhi_user_authority (user_id, role_id) FROM stdin;
1	1
3	1
1	2
3	2
4	2
6	2
5	2
\.


--
-- TOC entry 4342 (class 0 OID 17859)
-- Dependencies: 410
-- Data for Name: letter; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.letter (id, title, text, payment, current_period, previous_period, template_id) FROM stdin;
\.


--
-- TOC entry 4343 (class 0 OID 17868)
-- Dependencies: 411
-- Data for Name: letter_person_group; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.letter_person_group (person_groups_id, letters_id) FROM stdin;
\.


--
-- TOC entry 4266 (class 0 OID 17488)
-- Dependencies: 334
-- Data for Name: membership; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.membership (id, begin_date, end_date, person_id, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4268 (class 0 OID 17496)
-- Dependencies: 336
-- Data for Name: membership_fee; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.membership_fee (id, description, period, period_time_fraction) FROM stdin;
\.


--
-- TOC entry 4270 (class 0 OID 17507)
-- Dependencies: 338
-- Data for Name: membership_fee_amount; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.membership_fee_amount (id, begin_date, end_date, amount, membership_fee_id) FROM stdin;
\.


--
-- TOC entry 4272 (class 0 OID 17515)
-- Dependencies: 340
-- Data for Name: membership_fee_for_period; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.membership_fee_for_period (id, nr, jhi_year, reference_code, paid, note, cashbook_entry_id, membership_id) FROM stdin;
\.


--
-- TOC entry 4322 (class 0 OID 17760)
-- Dependencies: 390
-- Data for Name: music_book; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.music_book (id, name, description) FROM stdin;
\.


--
-- TOC entry 4348 (class 0 OID 17891)
-- Dependencies: 416
-- Data for Name: notification; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.notification (id, entity_id, created_at, read_by_user_id, notification_rule_id) FROM stdin;
\.


--
-- TOC entry 4345 (class 0 OID 17875)
-- Dependencies: 413
-- Data for Name: notification_rule; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.notification_rule (id, title, entity, conditions, active, message_group, message_single) FROM stdin;
1	Geburtstage im nächsten Monat	Person	SELECT p FROM Person p WHERE MONTH(p.birthDate) = ( MONTH(CURRENT_DATE) + 1 )	t	Im nächsten Monat haben {{count}} Personen Geburtstag.	Im nächsten Monat hat {{entity.firstName}} {{entity.lastName}} Geburtstag (geboren am {{entity.birthDate}})
\.


--
-- TOC entry 4346 (class 0 OID 17884)
-- Dependencies: 414
-- Data for Name: notification_rule_target_audiences; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.notification_rule_target_audiences (notification_rules_id, target_role_id) FROM stdin;
\.


--
-- TOC entry 4334 (class 0 OID 17816)
-- Dependencies: 402
-- Data for Name: official_appointment; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.official_appointment (id, organizer_name, organizer_address, is_head_quota, report_id) FROM stdin;
\.


--
-- TOC entry 4257 (class 0 OID 17430)
-- Dependencies: 325
-- Data for Name: person; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person (id, first_name, last_name, birth_date, email, telephone_number, street_address, postal_code, city, country, gender, email_type) FROM stdin;
\.


--
-- TOC entry 4298 (class 0 OID 17637)
-- Dependencies: 366
-- Data for Name: person_address; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_address (id, begin_date, end_date, address_id, person_id) FROM stdin;
\.


--
-- TOC entry 4296 (class 0 OID 17629)
-- Dependencies: 364
-- Data for Name: person_award; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_award (id, date, award_id, person_id) FROM stdin;
\.


--
-- TOC entry 4284 (class 0 OID 17575)
-- Dependencies: 352
-- Data for Name: person_clothing; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_clothing (id, begin_date, end_date, clothing_id, person_id) FROM stdin;
\.


--
-- TOC entry 4260 (class 0 OID 17446)
-- Dependencies: 328
-- Data for Name: person_group; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_group (id, name, parent_id) FROM stdin;
\.


--
-- TOC entry 4294 (class 0 OID 17621)
-- Dependencies: 362
-- Data for Name: person_honor; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_honor (id, date, honor_id, person_id) FROM stdin;
\.


--
-- TOC entry 4264 (class 0 OID 17480)
-- Dependencies: 332
-- Data for Name: person_info; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_info (id, name) FROM stdin;
\.


--
-- TOC entry 4276 (class 0 OID 17539)
-- Dependencies: 344
-- Data for Name: person_instrument; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_instrument (id, begin_date, end_date, instrument_id, person_id) FROM stdin;
\.


--
-- TOC entry 4258 (class 0 OID 17439)
-- Dependencies: 326
-- Data for Name: person_person_groups; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.person_person_groups (person_groups_id, people_id) FROM stdin;
\.


--
-- TOC entry 4307 (class 0 OID 17683)
-- Dependencies: 375
-- Data for Name: receipt; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.receipt (id, date, note, title, file, identifier, company_id, overwrite_identifier, initial_cashbook_entry_id) FROM stdin;
\.


--
-- TOC entry 4332 (class 0 OID 17805)
-- Dependencies: 400
-- Data for Name: report; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.report (id, generation_date, association_id, association_name, street_address, postal_code, city) FROM stdin;
\.


--
-- TOC entry 4262 (class 0 OID 17467)
-- Dependencies: 330
-- Data for Name: role; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.role (id, name) FROM stdin;
1	ROLE_ADMIN
2	ROLE_USER
3	ROLE_BOARD_MEMBER
4	ROLE_MEMBER_ADMIN
5	ROLE_ACCOUNTING_ADMIN
6	ROLE_INVENTORY_ADMIN
7	ROLE_BASE_ADMIN
\.


--
-- TOC entry 4340 (class 0 OID 17848)
-- Dependencies: 408
-- Data for Name: template; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.template (id, title, file, margin_left, margin_right, margin_top, margin_bottom) FROM stdin;
\.


--
-- TOC entry 4361 (class 0 OID 18314)
-- Dependencies: 429
-- Data for Name: user_column_config; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.user_column_config (id, visible, "position", column_config_id, user_id) FROM stdin;
1	t	1	1	1
2	t	2	2	1
3	t	3	3	1
4	t	4	4	1
5	t	5	5	1
6	f	1	6	1
7	f	2	7	1
8	t	1	8	1
9	t	2	9	1
10	t	3	10	1
11	f	1	11	1
12	f	2	12	1
13	t	1	13	1
14	t	2	14	1
15	t	3	15	1
16	t	4	16	1
17	t	5	17	1
18	t	1	18	1
19	t	2	19	1
20	t	3	20	1
21	t	4	21	1
22	t	5	22	1
23	t	6	23	1
24	f	1	24	1
25	t	1	25	1
26	t	2	26	1
27	t	3	27	1
28	t	4	28	1
29	f	1	29	1
30	f	2	30	1
31	f	3	31	1
32	f	4	32	1
33	f	5	33	1
34	t	1	1	2
35	t	2	2	2
36	t	3	3	2
37	t	4	4	2
38	t	5	5	2
39	f	1	6	2
40	f	2	7	2
41	t	1	8	2
42	t	2	9	2
43	t	3	10	2
44	f	1	11	2
45	f	2	12	2
46	t	1	13	2
47	t	2	14	2
48	t	3	15	2
49	t	4	16	2
50	t	5	17	2
51	t	1	18	2
52	t	2	19	2
53	t	3	20	2
54	t	4	21	2
55	t	5	22	2
56	t	6	23	2
57	f	1	24	2
58	t	1	25	2
59	t	2	26	2
60	t	3	27	2
61	t	4	28	2
62	f	1	29	2
63	f	2	30	2
64	f	3	31	2
65	f	4	32	2
66	f	5	33	2
67	t	1	1	3
68	t	2	2	3
69	t	3	3	3
70	t	4	4	3
71	t	5	5	3
72	f	1	6	3
73	f	2	7	3
74	t	1	8	3
75	t	2	9	3
76	t	3	10	3
77	f	1	11	3
78	f	2	12	3
79	t	1	13	3
80	t	2	14	3
81	t	3	15	3
82	t	4	16	3
83	t	5	17	3
84	t	1	18	3
85	t	2	19	3
86	t	3	20	3
87	t	4	21	3
88	t	5	22	3
89	t	6	23	3
90	f	1	24	3
91	t	1	25	3
92	t	2	26	3
93	t	3	27	3
94	t	4	28	3
95	f	1	29	3
96	f	2	30	3
97	f	3	31	3
98	f	4	32	3
99	f	5	33	3
100	t	1	1	4
101	t	2	2	4
102	t	3	3	4
103	t	4	4	4
104	t	5	5	4
105	f	1	6	4
106	f	2	7	4
107	t	1	8	4
108	t	2	9	4
109	t	3	10	4
110	f	1	11	4
111	f	2	12	4
112	t	1	13	4
113	t	2	14	4
114	t	3	15	4
115	t	4	16	4
116	t	5	17	4
117	t	1	18	4
118	t	2	19	4
119	t	3	20	4
120	t	4	21	4
121	t	5	22	4
122	t	6	23	4
123	f	1	24	4
124	t	1	25	4
125	t	2	26	4
126	t	3	27	4
127	t	4	28	4
128	f	1	29	4
129	f	2	30	4
130	f	3	31	4
131	f	4	32	4
132	f	5	33	4
133	t	1	1	5
134	t	2	2	5
135	t	3	3	5
136	t	4	4	5
137	t	5	5	5
138	f	1	6	5
139	f	2	7	5
140	t	1	8	5
141	t	2	9	5
142	t	3	10	5
143	f	1	11	5
144	f	2	12	5
145	t	1	13	5
146	t	2	14	5
147	t	3	15	5
148	t	4	16	5
149	t	5	17	5
150	t	1	18	5
151	t	2	19	5
152	t	3	20	5
153	t	4	21	5
154	t	5	22	5
155	t	6	23	5
156	f	1	24	5
157	t	1	25	5
158	t	2	26	5
159	t	3	27	5
160	t	4	28	5
161	f	1	29	5
162	f	2	30	5
163	f	3	31	5
164	f	4	32	5
165	f	5	33	5
166	t	1	1	6
167	t	2	2	6
168	t	3	3	6
169	t	4	4	6
170	t	5	5	6
171	f	1	6	6
172	f	2	7	6
173	t	1	8	6
174	t	2	9	6
175	t	3	10	6
176	f	1	11	6
177	f	2	12	6
178	t	1	13	6
179	t	2	14	6
180	t	3	15	6
181	t	4	16	6
182	t	5	17	6
183	t	1	18	6
184	t	2	19	6
185	t	3	20	6
186	t	4	21	6
187	t	5	22	6
188	t	6	23	6
189	f	1	24	6
190	t	1	25	6
191	t	2	26	6
192	t	3	27	6
193	t	4	28	6
194	f	1	29	6
195	f	2	30	6
196	f	3	31	6
197	f	4	32	6
198	f	5	33	6
\.


--
-- TOC entry 4636 (class 0 OID 0)
-- Dependencies: 225
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_id_seq', 1, false);


--
-- TOC entry 4637 (class 0 OID 0)
-- Dependencies: 263
-- Name: appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_id_seq', 1, false);


--
-- TOC entry 4638 (class 0 OID 0)
-- Dependencies: 266
-- Name: appointment_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_type_id_seq', 1, false);


--
-- TOC entry 4639 (class 0 OID 0)
-- Dependencies: 280
-- Name: arranger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arranger_id_seq', 1, false);


--
-- TOC entry 4640 (class 0 OID 0)
-- Dependencies: 243
-- Name: award_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.award_id_seq', 1, false);


--
-- TOC entry 4641 (class 0 OID 0)
-- Dependencies: 289
-- Name: bank_import_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_import_data_id_seq', 1, false);


--
-- TOC entry 4642 (class 0 OID 0)
-- Dependencies: 303
-- Name: cashbook_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_account_id_seq', 1, false);


--
-- TOC entry 4643 (class 0 OID 0)
-- Dependencies: 256
-- Name: cashbook_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_category_id_seq', 1, false);


--
-- TOC entry 4644 (class 0 OID 0)
-- Dependencies: 254
-- Name: cashbook_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_entry_id_seq', 1, false);


--
-- TOC entry 4645 (class 0 OID 0)
-- Dependencies: 251
-- Name: cashbook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_id_seq', 1, false);


--
-- TOC entry 4646 (class 0 OID 0)
-- Dependencies: 287
-- Name: clothing_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_group_id_seq', 1, false);


--
-- TOC entry 4647 (class 0 OID 0)
-- Dependencies: 237
-- Name: clothing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_id_seq', 1, false);


--
-- TOC entry 4648 (class 0 OID 0)
-- Dependencies: 239
-- Name: clothing_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_type_id_seq', 1, false);


--
-- TOC entry 4649 (class 0 OID 0)
-- Dependencies: 271
-- Name: color_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.color_code_id_seq', 1, false);


--
-- TOC entry 4650 (class 0 OID 0)
-- Dependencies: 310
-- Name: column_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.column_config_id_seq', 33, true);


--
-- TOC entry 4651 (class 0 OID 0)
-- Dependencies: 260
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.company_id_seq', 1, false);


--
-- TOC entry 4652 (class 0 OID 0)
-- Dependencies: 277
-- Name: composer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.composer_id_seq', 1, false);


--
-- TOC entry 4653 (class 0 OID 0)
-- Dependencies: 268
-- Name: composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.composition_id_seq', 1, false);


--
-- TOC entry 4654 (class 0 OID 0)
-- Dependencies: 301
-- Name: customization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customization_id_seq', 1, false);


--
-- TOC entry 4655 (class 0 OID 0)
-- Dependencies: 306
-- Name: email_verification_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.email_verification_link_id_seq', 1, false);


--
-- TOC entry 4656 (class 0 OID 0)
-- Dependencies: 308
-- Name: filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filter_id_seq', 1, false);


--
-- TOC entry 4657 (class 0 OID 0)
-- Dependencies: 275
-- Name: genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.genre_id_seq', 1, false);


--
-- TOC entry 4658 (class 0 OID 0)
-- Dependencies: 200
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1000, false);


--
-- TOC entry 4659 (class 0 OID 0)
-- Dependencies: 241
-- Name: honor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.honor_id_seq', 1, false);


--
-- TOC entry 4660 (class 0 OID 0)
-- Dependencies: 229
-- Name: instrument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_id_seq', 1, false);


--
-- TOC entry 4661 (class 0 OID 0)
-- Dependencies: 233
-- Name: instrument_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_service_id_seq', 1, false);


--
-- TOC entry 4662 (class 0 OID 0)
-- Dependencies: 231
-- Name: instrument_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_type_id_seq', 1, false);


--
-- TOC entry 4663 (class 0 OID 0)
-- Dependencies: 305
-- Name: jhi_authority_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_authority_id_seq', 6, true);


--
-- TOC entry 4664 (class 0 OID 0)
-- Dependencies: 205
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_persistent_audit_event_event_id_seq', 1, false);


--
-- TOC entry 4665 (class 0 OID 0)
-- Dependencies: 201
-- Name: jhi_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_user_id_seq', 1, false);


--
-- TOC entry 4666 (class 0 OID 0)
-- Dependencies: 293
-- Name: letter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.letter_id_seq', 1, false);


--
-- TOC entry 4667 (class 0 OID 0)
-- Dependencies: 221
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_amount_id_seq', 1, false);


--
-- TOC entry 4668 (class 0 OID 0)
-- Dependencies: 223
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_for_period_id_seq', 1, false);


--
-- TOC entry 4669 (class 0 OID 0)
-- Dependencies: 219
-- Name: membership_fee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_id_seq', 1, false);


--
-- TOC entry 4670 (class 0 OID 0)
-- Dependencies: 217
-- Name: membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_id_seq', 1, false);


--
-- TOC entry 4671 (class 0 OID 0)
-- Dependencies: 273
-- Name: music_book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.music_book_id_seq', 1, false);


--
-- TOC entry 4672 (class 0 OID 0)
-- Dependencies: 299
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_id_seq', 1, false);


--
-- TOC entry 4673 (class 0 OID 0)
-- Dependencies: 296
-- Name: notification_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_rule_id_seq', 1, false);


--
-- TOC entry 4674 (class 0 OID 0)
-- Dependencies: 285
-- Name: official_appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.official_appointment_id_seq', 1, false);


--
-- TOC entry 4675 (class 0 OID 0)
-- Dependencies: 249
-- Name: person_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_address_id_seq', 1, false);


--
-- TOC entry 4676 (class 0 OID 0)
-- Dependencies: 247
-- Name: person_award_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_award_id_seq', 1, false);


--
-- TOC entry 4677 (class 0 OID 0)
-- Dependencies: 235
-- Name: person_clothing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_clothing_id_seq', 1, false);


--
-- TOC entry 4678 (class 0 OID 0)
-- Dependencies: 211
-- Name: person_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_group_id_seq', 1, false);


--
-- TOC entry 4679 (class 0 OID 0)
-- Dependencies: 245
-- Name: person_honor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_honor_id_seq', 1, false);


--
-- TOC entry 4680 (class 0 OID 0)
-- Dependencies: 208
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 1, false);


--
-- TOC entry 4681 (class 0 OID 0)
-- Dependencies: 215
-- Name: person_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_info_id_seq', 1, false);


--
-- TOC entry 4682 (class 0 OID 0)
-- Dependencies: 227
-- Name: person_instrument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_instrument_id_seq', 1, false);


--
-- TOC entry 4683 (class 0 OID 0)
-- Dependencies: 258
-- Name: receipt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.receipt_id_seq', 1, false);


--
-- TOC entry 4684 (class 0 OID 0)
-- Dependencies: 283
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.report_id_seq', 1, false);


--
-- TOC entry 4685 (class 0 OID 0)
-- Dependencies: 213
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 7, true);


--
-- TOC entry 4686 (class 0 OID 0)
-- Dependencies: 291
-- Name: template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.template_id_seq', 1, false);


--
-- TOC entry 4687 (class 0 OID 0)
-- Dependencies: 312
-- Name: user_column_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_column_config_id_seq', 198, true);


--
-- TOC entry 4688 (class 0 OID 0)
-- Dependencies: 457
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.address_id_seq', 1, false);


--
-- TOC entry 4689 (class 0 OID 0)
-- Dependencies: 495
-- Name: appointment_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.appointment_id_seq', 2, true);


--
-- TOC entry 4690 (class 0 OID 0)
-- Dependencies: 498
-- Name: appointment_type_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.appointment_type_id_seq', 2, true);


--
-- TOC entry 4691 (class 0 OID 0)
-- Dependencies: 512
-- Name: arranger_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.arranger_id_seq', 1, false);


--
-- TOC entry 4692 (class 0 OID 0)
-- Dependencies: 475
-- Name: award_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.award_id_seq', 1, false);


--
-- TOC entry 4693 (class 0 OID 0)
-- Dependencies: 521
-- Name: bank_import_data_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.bank_import_data_id_seq', 1, false);


--
-- TOC entry 4694 (class 0 OID 0)
-- Dependencies: 535
-- Name: cashbook_account_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.cashbook_account_id_seq', 2, true);


--
-- TOC entry 4695 (class 0 OID 0)
-- Dependencies: 488
-- Name: cashbook_category_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.cashbook_category_id_seq', 2, true);


--
-- TOC entry 4696 (class 0 OID 0)
-- Dependencies: 486
-- Name: cashbook_entry_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.cashbook_entry_id_seq', 3, true);


--
-- TOC entry 4697 (class 0 OID 0)
-- Dependencies: 483
-- Name: cashbook_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.cashbook_id_seq', 1, false);


--
-- TOC entry 4698 (class 0 OID 0)
-- Dependencies: 519
-- Name: clothing_group_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.clothing_group_id_seq', 1, false);


--
-- TOC entry 4699 (class 0 OID 0)
-- Dependencies: 469
-- Name: clothing_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.clothing_id_seq', 1, false);


--
-- TOC entry 4700 (class 0 OID 0)
-- Dependencies: 471
-- Name: clothing_type_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.clothing_type_id_seq', 1, false);


--
-- TOC entry 4701 (class 0 OID 0)
-- Dependencies: 503
-- Name: color_code_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.color_code_id_seq', 2, true);


--
-- TOC entry 4702 (class 0 OID 0)
-- Dependencies: 542
-- Name: column_config_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.column_config_id_seq', 33, true);


--
-- TOC entry 4703 (class 0 OID 0)
-- Dependencies: 492
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.company_id_seq', 1, false);


--
-- TOC entry 4704 (class 0 OID 0)
-- Dependencies: 509
-- Name: composer_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.composer_id_seq', 1, false);


--
-- TOC entry 4705 (class 0 OID 0)
-- Dependencies: 500
-- Name: composition_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.composition_id_seq', 1, true);


--
-- TOC entry 4706 (class 0 OID 0)
-- Dependencies: 533
-- Name: customization_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.customization_id_seq', 1, false);


--
-- TOC entry 4707 (class 0 OID 0)
-- Dependencies: 538
-- Name: email_verification_link_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.email_verification_link_id_seq', 3, true);


--
-- TOC entry 4708 (class 0 OID 0)
-- Dependencies: 540
-- Name: filter_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.filter_id_seq', 1, true);


--
-- TOC entry 4709 (class 0 OID 0)
-- Dependencies: 507
-- Name: genre_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.genre_id_seq', 1, false);


--
-- TOC entry 4710 (class 0 OID 0)
-- Dependencies: 432
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.hibernate_sequence', 1000, false);


--
-- TOC entry 4711 (class 0 OID 0)
-- Dependencies: 473
-- Name: honor_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.honor_id_seq', 1, false);


--
-- TOC entry 4712 (class 0 OID 0)
-- Dependencies: 461
-- Name: instrument_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.instrument_id_seq', 1, false);


--
-- TOC entry 4713 (class 0 OID 0)
-- Dependencies: 465
-- Name: instrument_service_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.instrument_service_id_seq', 1, false);


--
-- TOC entry 4714 (class 0 OID 0)
-- Dependencies: 463
-- Name: instrument_type_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.instrument_type_id_seq', 1, false);


--
-- TOC entry 4715 (class 0 OID 0)
-- Dependencies: 537
-- Name: jhi_authority_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.jhi_authority_id_seq', 44, true);


--
-- TOC entry 4716 (class 0 OID 0)
-- Dependencies: 437
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.jhi_persistent_audit_event_event_id_seq', 1, false);


--
-- TOC entry 4717 (class 0 OID 0)
-- Dependencies: 433
-- Name: jhi_user_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.jhi_user_id_seq', 1, false);


--
-- TOC entry 4718 (class 0 OID 0)
-- Dependencies: 525
-- Name: letter_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.letter_id_seq', 1, false);


--
-- TOC entry 4719 (class 0 OID 0)
-- Dependencies: 453
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.membership_fee_amount_id_seq', 1, false);


--
-- TOC entry 4720 (class 0 OID 0)
-- Dependencies: 455
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.membership_fee_for_period_id_seq', 1, false);


--
-- TOC entry 4721 (class 0 OID 0)
-- Dependencies: 451
-- Name: membership_fee_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.membership_fee_id_seq', 1, false);


--
-- TOC entry 4722 (class 0 OID 0)
-- Dependencies: 449
-- Name: membership_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.membership_id_seq', 1, false);


--
-- TOC entry 4723 (class 0 OID 0)
-- Dependencies: 505
-- Name: music_book_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.music_book_id_seq', 1, false);


--
-- TOC entry 4724 (class 0 OID 0)
-- Dependencies: 531
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.notification_id_seq', 1, false);


--
-- TOC entry 4725 (class 0 OID 0)
-- Dependencies: 528
-- Name: notification_rule_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.notification_rule_id_seq', 1, false);


--
-- TOC entry 4726 (class 0 OID 0)
-- Dependencies: 517
-- Name: official_appointment_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.official_appointment_id_seq', 1, true);


--
-- TOC entry 4727 (class 0 OID 0)
-- Dependencies: 481
-- Name: person_address_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_address_id_seq', 1, false);


--
-- TOC entry 4728 (class 0 OID 0)
-- Dependencies: 479
-- Name: person_award_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_award_id_seq', 1, false);


--
-- TOC entry 4729 (class 0 OID 0)
-- Dependencies: 467
-- Name: person_clothing_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_clothing_id_seq', 1, false);


--
-- TOC entry 4730 (class 0 OID 0)
-- Dependencies: 443
-- Name: person_group_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_group_id_seq', 1, true);


--
-- TOC entry 4731 (class 0 OID 0)
-- Dependencies: 477
-- Name: person_honor_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_honor_id_seq', 1, false);


--
-- TOC entry 4732 (class 0 OID 0)
-- Dependencies: 440
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_id_seq', 2, true);


--
-- TOC entry 4733 (class 0 OID 0)
-- Dependencies: 447
-- Name: person_info_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_info_id_seq', 1, false);


--
-- TOC entry 4734 (class 0 OID 0)
-- Dependencies: 459
-- Name: person_instrument_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.person_instrument_id_seq', 1, false);


--
-- TOC entry 4735 (class 0 OID 0)
-- Dependencies: 490
-- Name: receipt_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.receipt_id_seq', 2, true);


--
-- TOC entry 4736 (class 0 OID 0)
-- Dependencies: 515
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.report_id_seq', 1, false);


--
-- TOC entry 4737 (class 0 OID 0)
-- Dependencies: 445
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.role_id_seq', 7, true);


--
-- TOC entry 4738 (class 0 OID 0)
-- Dependencies: 523
-- Name: template_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.template_id_seq', 1, false);


--
-- TOC entry 4739 (class 0 OID 0)
-- Dependencies: 544
-- Name: user_column_config_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.user_column_config_id_seq', 198, true);


--
-- TOC entry 4740 (class 0 OID 0)
-- Dependencies: 341
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.address_id_seq', 1, false);


--
-- TOC entry 4741 (class 0 OID 0)
-- Dependencies: 379
-- Name: appointment_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.appointment_id_seq', 1, false);


--
-- TOC entry 4742 (class 0 OID 0)
-- Dependencies: 382
-- Name: appointment_type_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.appointment_type_id_seq', 1, false);


--
-- TOC entry 4743 (class 0 OID 0)
-- Dependencies: 396
-- Name: arranger_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.arranger_id_seq', 1, false);


--
-- TOC entry 4744 (class 0 OID 0)
-- Dependencies: 359
-- Name: award_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.award_id_seq', 1, false);


--
-- TOC entry 4745 (class 0 OID 0)
-- Dependencies: 405
-- Name: bank_import_data_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.bank_import_data_id_seq', 1, false);


--
-- TOC entry 4746 (class 0 OID 0)
-- Dependencies: 419
-- Name: cashbook_account_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.cashbook_account_id_seq', 1, false);


--
-- TOC entry 4747 (class 0 OID 0)
-- Dependencies: 372
-- Name: cashbook_category_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.cashbook_category_id_seq', 1, false);


--
-- TOC entry 4748 (class 0 OID 0)
-- Dependencies: 370
-- Name: cashbook_entry_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.cashbook_entry_id_seq', 1, false);


--
-- TOC entry 4749 (class 0 OID 0)
-- Dependencies: 367
-- Name: cashbook_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.cashbook_id_seq', 1, false);


--
-- TOC entry 4750 (class 0 OID 0)
-- Dependencies: 403
-- Name: clothing_group_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.clothing_group_id_seq', 1, false);


--
-- TOC entry 4751 (class 0 OID 0)
-- Dependencies: 353
-- Name: clothing_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.clothing_id_seq', 1, false);


--
-- TOC entry 4752 (class 0 OID 0)
-- Dependencies: 355
-- Name: clothing_type_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.clothing_type_id_seq', 1, false);


--
-- TOC entry 4753 (class 0 OID 0)
-- Dependencies: 387
-- Name: color_code_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.color_code_id_seq', 1, false);


--
-- TOC entry 4754 (class 0 OID 0)
-- Dependencies: 426
-- Name: column_config_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.column_config_id_seq', 33, true);


--
-- TOC entry 4755 (class 0 OID 0)
-- Dependencies: 376
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.company_id_seq', 1, false);


--
-- TOC entry 4756 (class 0 OID 0)
-- Dependencies: 393
-- Name: composer_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.composer_id_seq', 1, false);


--
-- TOC entry 4757 (class 0 OID 0)
-- Dependencies: 384
-- Name: composition_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.composition_id_seq', 1, false);


--
-- TOC entry 4758 (class 0 OID 0)
-- Dependencies: 417
-- Name: customization_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.customization_id_seq', 1, false);


--
-- TOC entry 4759 (class 0 OID 0)
-- Dependencies: 422
-- Name: email_verification_link_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.email_verification_link_id_seq', 1, false);


--
-- TOC entry 4760 (class 0 OID 0)
-- Dependencies: 424
-- Name: filter_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.filter_id_seq', 1, false);


--
-- TOC entry 4761 (class 0 OID 0)
-- Dependencies: 391
-- Name: genre_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.genre_id_seq', 1, false);


--
-- TOC entry 4762 (class 0 OID 0)
-- Dependencies: 316
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.hibernate_sequence', 1000, false);


--
-- TOC entry 4763 (class 0 OID 0)
-- Dependencies: 357
-- Name: honor_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.honor_id_seq', 1, false);


--
-- TOC entry 4764 (class 0 OID 0)
-- Dependencies: 345
-- Name: instrument_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.instrument_id_seq', 1, false);


--
-- TOC entry 4765 (class 0 OID 0)
-- Dependencies: 349
-- Name: instrument_service_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.instrument_service_id_seq', 1, false);


--
-- TOC entry 4766 (class 0 OID 0)
-- Dependencies: 347
-- Name: instrument_type_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.instrument_type_id_seq', 1, false);


--
-- TOC entry 4767 (class 0 OID 0)
-- Dependencies: 421
-- Name: jhi_authority_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.jhi_authority_id_seq', 6, true);


--
-- TOC entry 4768 (class 0 OID 0)
-- Dependencies: 321
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.jhi_persistent_audit_event_event_id_seq', 1, false);


--
-- TOC entry 4769 (class 0 OID 0)
-- Dependencies: 317
-- Name: jhi_user_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.jhi_user_id_seq', 1, false);


--
-- TOC entry 4770 (class 0 OID 0)
-- Dependencies: 409
-- Name: letter_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.letter_id_seq', 1, false);


--
-- TOC entry 4771 (class 0 OID 0)
-- Dependencies: 337
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.membership_fee_amount_id_seq', 1, false);


--
-- TOC entry 4772 (class 0 OID 0)
-- Dependencies: 339
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.membership_fee_for_period_id_seq', 1, false);


--
-- TOC entry 4773 (class 0 OID 0)
-- Dependencies: 335
-- Name: membership_fee_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.membership_fee_id_seq', 1, false);


--
-- TOC entry 4774 (class 0 OID 0)
-- Dependencies: 333
-- Name: membership_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.membership_id_seq', 1, false);


--
-- TOC entry 4775 (class 0 OID 0)
-- Dependencies: 389
-- Name: music_book_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.music_book_id_seq', 1, false);


--
-- TOC entry 4776 (class 0 OID 0)
-- Dependencies: 415
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.notification_id_seq', 1, false);


--
-- TOC entry 4777 (class 0 OID 0)
-- Dependencies: 412
-- Name: notification_rule_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.notification_rule_id_seq', 1, false);


--
-- TOC entry 4778 (class 0 OID 0)
-- Dependencies: 401
-- Name: official_appointment_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.official_appointment_id_seq', 1, false);


--
-- TOC entry 4779 (class 0 OID 0)
-- Dependencies: 365
-- Name: person_address_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_address_id_seq', 1, false);


--
-- TOC entry 4780 (class 0 OID 0)
-- Dependencies: 363
-- Name: person_award_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_award_id_seq', 1, false);


--
-- TOC entry 4781 (class 0 OID 0)
-- Dependencies: 351
-- Name: person_clothing_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_clothing_id_seq', 1, false);


--
-- TOC entry 4782 (class 0 OID 0)
-- Dependencies: 327
-- Name: person_group_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_group_id_seq', 1, false);


--
-- TOC entry 4783 (class 0 OID 0)
-- Dependencies: 361
-- Name: person_honor_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_honor_id_seq', 1, false);


--
-- TOC entry 4784 (class 0 OID 0)
-- Dependencies: 324
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_id_seq', 1, false);


--
-- TOC entry 4785 (class 0 OID 0)
-- Dependencies: 331
-- Name: person_info_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_info_id_seq', 1, false);


--
-- TOC entry 4786 (class 0 OID 0)
-- Dependencies: 343
-- Name: person_instrument_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.person_instrument_id_seq', 1, false);


--
-- TOC entry 4787 (class 0 OID 0)
-- Dependencies: 374
-- Name: receipt_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.receipt_id_seq', 1, false);


--
-- TOC entry 4788 (class 0 OID 0)
-- Dependencies: 399
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.report_id_seq', 1, false);


--
-- TOC entry 4789 (class 0 OID 0)
-- Dependencies: 329
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.role_id_seq', 7, true);


--
-- TOC entry 4790 (class 0 OID 0)
-- Dependencies: 407
-- Name: template_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.template_id_seq', 1, false);


--
-- TOC entry 4791 (class 0 OID 0)
-- Dependencies: 428
-- Name: user_column_config_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.user_column_config_id_seq', 198, true);


--
-- TOC entry 3442 (class 2606 OID 16747)
-- Name: appointment appointment_official_appointment_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT appointment_official_appointment_id_key UNIQUE (official_appointment_id);


--
-- TOC entry 3446 (class 2606 OID 16752)
-- Name: appointment_persons appointment_persons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT appointment_persons_pkey PRIMARY KEY (appointments_id, persons_id);


--
-- TOC entry 3466 (class 2606 OID 16829)
-- Name: arranger_compositions arranger_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT arranger_compositions_pkey PRIMARY KEY (arrangers_id, compositions_id);


--
-- TOC entry 3474 (class 2606 OID 16872)
-- Name: bank_import_data bank_import_data_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT bank_import_data_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3430 (class 2606 OID 16685)
-- Name: cashbook_cashbook_entries cashbook_cashbook_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT cashbook_cashbook_entries_pkey PRIMARY KEY (cashbooks_id, cashbook_entries_id);


--
-- TOC entry 3440 (class 2606 OID 16734)
-- Name: company_person_groups company_person_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT company_person_groups_pkey PRIMARY KEY (companies_id, person_groups_id);


--
-- TOC entry 3462 (class 2606 OID 16816)
-- Name: composer_compositions composer_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT composer_compositions_pkey PRIMARY KEY (composers_id, compositions_id);


--
-- TOC entry 3452 (class 2606 OID 16776)
-- Name: composition_appointments composition_appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT composition_appointments_pkey PRIMARY KEY (compositions_id, appointments_id);


--
-- TOC entry 3490 (class 2606 OID 17262)
-- Name: customization customization_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization
    ADD CONSTRAINT customization_name_key UNIQUE (name);


--
-- TOC entry 3402 (class 2606 OID 16581)
-- Name: instrument instrument_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT instrument_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3408 (class 2606 OID 16599)
-- Name: instrument_service instrument_service_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT instrument_service_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3366 (class 2606 OID 17288)
-- Name: jhi_authority jhi_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_authority
    ADD CONSTRAINT jhi_authority_pkey PRIMARY KEY (id);


--
-- TOC entry 3374 (class 2606 OID 16447)
-- Name: jhi_persistent_audit_evt_data jhi_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT jhi_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- TOC entry 3368 (class 2606 OID 17265)
-- Name: jhi_user_authority jhi_user_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT jhi_user_authority_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 3360 (class 2606 OID 16412)
-- Name: jhi_user jhi_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT jhi_user_email_key UNIQUE (email);


--
-- TOC entry 3362 (class 2606 OID 16410)
-- Name: jhi_user jhi_user_login_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT jhi_user_login_key UNIQUE (login);


--
-- TOC entry 3482 (class 2606 OID 16899)
-- Name: letter_person_group letter_person_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT letter_person_group_pkey PRIMARY KEY (letters_id, person_groups_id);


--
-- TOC entry 3394 (class 2606 OID 16552)
-- Name: membership_fee_for_period membership_fee_for_period_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT membership_fee_for_period_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3486 (class 2606 OID 17281)
-- Name: notification_rule_target_audiences notification_rule_target_audiences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT notification_rule_target_audiences_pkey PRIMARY KEY (notification_rules_id, target_role_id);


--
-- TOC entry 3378 (class 2606 OID 16470)
-- Name: person_person_groups person_person_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT person_person_groups_pkey PRIMARY KEY (people_id, person_groups_id);


--
-- TOC entry 3398 (class 2606 OID 16563)
-- Name: address pk_address; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT pk_address PRIMARY KEY (id);


--
-- TOC entry 3444 (class 2606 OID 16745)
-- Name: appointment pk_appointment; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT pk_appointment PRIMARY KEY (id);


--
-- TOC entry 3448 (class 2606 OID 16760)
-- Name: appointment_type pk_appointment_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type
    ADD CONSTRAINT pk_appointment_type PRIMARY KEY (id);


--
-- TOC entry 3464 (class 2606 OID 16824)
-- Name: arranger pk_arranger; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger
    ADD CONSTRAINT pk_arranger PRIMARY KEY (id);


--
-- TOC entry 3420 (class 2606 OID 16645)
-- Name: award pk_award; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.award
    ADD CONSTRAINT pk_award PRIMARY KEY (id);


--
-- TOC entry 3476 (class 2606 OID 16870)
-- Name: bank_import_data pk_bank_import_data; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT pk_bank_import_data PRIMARY KEY (id);


--
-- TOC entry 3428 (class 2606 OID 16680)
-- Name: cashbook pk_cashbook; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook
    ADD CONSTRAINT pk_cashbook PRIMARY KEY (id);


--
-- TOC entry 3494 (class 2606 OID 16945)
-- Name: cashbook_account pk_cashbook_account; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account
    ADD CONSTRAINT pk_cashbook_account PRIMARY KEY (id);


--
-- TOC entry 3434 (class 2606 OID 16707)
-- Name: cashbook_category pk_cashbook_category; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category
    ADD CONSTRAINT pk_cashbook_category PRIMARY KEY (id);


--
-- TOC entry 3432 (class 2606 OID 16696)
-- Name: cashbook_entry pk_cashbook_entry; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT pk_cashbook_entry PRIMARY KEY (id);


--
-- TOC entry 3414 (class 2606 OID 16615)
-- Name: clothing pk_clothing; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing
    ADD CONSTRAINT pk_clothing PRIMARY KEY (id);


--
-- TOC entry 3472 (class 2606 OID 16859)
-- Name: clothing_group pk_clothing_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group
    ADD CONSTRAINT pk_clothing_group PRIMARY KEY (id);


--
-- TOC entry 3416 (class 2606 OID 16623)
-- Name: clothing_type pk_clothing_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_type
    ADD CONSTRAINT pk_clothing_type PRIMARY KEY (id);


--
-- TOC entry 3454 (class 2606 OID 16784)
-- Name: color_code pk_color_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code
    ADD CONSTRAINT pk_color_code PRIMARY KEY (id);


--
-- TOC entry 3502 (class 2606 OID 17338)
-- Name: column_config pk_column_config; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.column_config
    ADD CONSTRAINT pk_column_config PRIMARY KEY (id);


--
-- TOC entry 3438 (class 2606 OID 16729)
-- Name: company pk_company; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT pk_company PRIMARY KEY (id);


--
-- TOC entry 3460 (class 2606 OID 16811)
-- Name: composer pk_composer; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer
    ADD CONSTRAINT pk_composer PRIMARY KEY (id);


--
-- TOC entry 3450 (class 2606 OID 16771)
-- Name: composition pk_composition; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT pk_composition PRIMARY KEY (id);


--
-- TOC entry 3492 (class 2606 OID 16934)
-- Name: customization pk_customization; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization
    ADD CONSTRAINT pk_customization PRIMARY KEY (id);


--
-- TOC entry 3356 (class 2606 OID 16391)
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- TOC entry 3498 (class 2606 OID 17310)
-- Name: email_verification_link pk_email_verification_link; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link
    ADD CONSTRAINT pk_email_verification_link PRIMARY KEY (id);


--
-- TOC entry 3500 (class 2606 OID 17327)
-- Name: filter pk_filter; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter
    ADD CONSTRAINT pk_filter PRIMARY KEY (id);


--
-- TOC entry 3458 (class 2606 OID 16803)
-- Name: genre pk_genre; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT pk_genre PRIMARY KEY (id);


--
-- TOC entry 3418 (class 2606 OID 16634)
-- Name: honor pk_honor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.honor
    ADD CONSTRAINT pk_honor PRIMARY KEY (id);


--
-- TOC entry 3404 (class 2606 OID 16579)
-- Name: instrument pk_instrument; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT pk_instrument PRIMARY KEY (id);


--
-- TOC entry 3410 (class 2606 OID 16597)
-- Name: instrument_service pk_instrument_service; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT pk_instrument_service PRIMARY KEY (id);


--
-- TOC entry 3406 (class 2606 OID 16589)
-- Name: instrument_type pk_instrument_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_type
    ADD CONSTRAINT pk_instrument_type PRIMARY KEY (id);


--
-- TOC entry 3371 (class 2606 OID 16442)
-- Name: jhi_persistent_audit_event pk_jhi_persistent_audit_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_event
    ADD CONSTRAINT pk_jhi_persistent_audit_event PRIMARY KEY (event_id);


--
-- TOC entry 3364 (class 2606 OID 16408)
-- Name: jhi_user pk_jhi_user; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT pk_jhi_user PRIMARY KEY (id);


--
-- TOC entry 3480 (class 2606 OID 16894)
-- Name: letter pk_letter; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter
    ADD CONSTRAINT pk_letter PRIMARY KEY (id);


--
-- TOC entry 3388 (class 2606 OID 16520)
-- Name: membership pk_membership; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT pk_membership PRIMARY KEY (id);


--
-- TOC entry 3390 (class 2606 OID 16531)
-- Name: membership_fee pk_membership_fee; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee
    ADD CONSTRAINT pk_membership_fee PRIMARY KEY (id);


--
-- TOC entry 3392 (class 2606 OID 16539)
-- Name: membership_fee_amount pk_membership_fee_amount; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount
    ADD CONSTRAINT pk_membership_fee_amount PRIMARY KEY (id);


--
-- TOC entry 3396 (class 2606 OID 16550)
-- Name: membership_fee_for_period pk_membership_fee_for_period; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT pk_membership_fee_for_period PRIMARY KEY (id);


--
-- TOC entry 3456 (class 2606 OID 16795)
-- Name: music_book pk_music_book; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.music_book
    ADD CONSTRAINT pk_music_book PRIMARY KEY (id);


--
-- TOC entry 3488 (class 2606 OID 16923)
-- Name: notification pk_notification; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT pk_notification PRIMARY KEY (id);


--
-- TOC entry 3484 (class 2606 OID 16910)
-- Name: notification_rule pk_notification_rule; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule
    ADD CONSTRAINT pk_notification_rule PRIMARY KEY (id);


--
-- TOC entry 3470 (class 2606 OID 16851)
-- Name: official_appointment pk_official_appointment; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment
    ADD CONSTRAINT pk_official_appointment PRIMARY KEY (id);


--
-- TOC entry 3376 (class 2606 OID 16465)
-- Name: person pk_person; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT pk_person PRIMARY KEY (id);


--
-- TOC entry 3426 (class 2606 OID 16669)
-- Name: person_address pk_person_address; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT pk_person_address PRIMARY KEY (id);


--
-- TOC entry 3424 (class 2606 OID 16661)
-- Name: person_award pk_person_award; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT pk_person_award PRIMARY KEY (id);


--
-- TOC entry 3412 (class 2606 OID 16607)
-- Name: person_clothing pk_person_clothing; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT pk_person_clothing PRIMARY KEY (id);


--
-- TOC entry 3380 (class 2606 OID 16478)
-- Name: person_group pk_person_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group
    ADD CONSTRAINT pk_person_group PRIMARY KEY (id);


--
-- TOC entry 3422 (class 2606 OID 16653)
-- Name: person_honor pk_person_honor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT pk_person_honor PRIMARY KEY (id);


--
-- TOC entry 3386 (class 2606 OID 16512)
-- Name: person_info pk_person_info; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_info
    ADD CONSTRAINT pk_person_info PRIMARY KEY (id);


--
-- TOC entry 3400 (class 2606 OID 16571)
-- Name: person_instrument pk_person_instrument; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT pk_person_instrument PRIMARY KEY (id);


--
-- TOC entry 3436 (class 2606 OID 16718)
-- Name: receipt pk_receipt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT pk_receipt PRIMARY KEY (id);


--
-- TOC entry 3468 (class 2606 OID 16840)
-- Name: report pk_report; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT pk_report PRIMARY KEY (id);


--
-- TOC entry 3382 (class 2606 OID 16499)
-- Name: role pk_role; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- TOC entry 3478 (class 2606 OID 16883)
-- Name: template pk_template; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.template
    ADD CONSTRAINT pk_template PRIMARY KEY (id);


--
-- TOC entry 3504 (class 2606 OID 17346)
-- Name: user_column_config pk_user_column_config; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT pk_user_column_config PRIMARY KEY (id);


--
-- TOC entry 3384 (class 2606 OID 17279)
-- Name: role role_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- TOC entry 3496 (class 2606 OID 17290)
-- Name: cashbook_account ux_cashbook_account_receipt_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account
    ADD CONSTRAINT ux_cashbook_account_receipt_code UNIQUE (receipt_code);


--
-- TOC entry 3742 (class 2606 OID 18691)
-- Name: appointment appointment_official_appointment_id_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment
    ADD CONSTRAINT appointment_official_appointment_id_key UNIQUE (official_appointment_id);


--
-- TOC entry 3746 (class 2606 OID 18696)
-- Name: appointment_persons appointment_persons_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_persons
    ADD CONSTRAINT appointment_persons_pkey PRIMARY KEY (appointments_id, persons_id);


--
-- TOC entry 3766 (class 2606 OID 18773)
-- Name: arranger_compositions arranger_compositions_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.arranger_compositions
    ADD CONSTRAINT arranger_compositions_pkey PRIMARY KEY (arrangers_id, compositions_id);


--
-- TOC entry 3774 (class 2606 OID 18816)
-- Name: bank_import_data bank_import_data_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.bank_import_data
    ADD CONSTRAINT bank_import_data_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3730 (class 2606 OID 18629)
-- Name: cashbook_cashbook_entries cashbook_cashbook_entries_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_cashbook_entries
    ADD CONSTRAINT cashbook_cashbook_entries_pkey PRIMARY KEY (cashbooks_id, cashbook_entries_id);


--
-- TOC entry 3740 (class 2606 OID 18678)
-- Name: company_person_groups company_person_groups_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.company_person_groups
    ADD CONSTRAINT company_person_groups_pkey PRIMARY KEY (companies_id, person_groups_id);


--
-- TOC entry 3762 (class 2606 OID 18760)
-- Name: composer_compositions composer_compositions_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composer_compositions
    ADD CONSTRAINT composer_compositions_pkey PRIMARY KEY (composers_id, compositions_id);


--
-- TOC entry 3752 (class 2606 OID 18720)
-- Name: composition_appointments composition_appointments_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition_appointments
    ADD CONSTRAINT composition_appointments_pkey PRIMARY KEY (compositions_id, appointments_id);


--
-- TOC entry 3790 (class 2606 OID 19206)
-- Name: customization customization_name_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.customization
    ADD CONSTRAINT customization_name_key UNIQUE (name);


--
-- TOC entry 3702 (class 2606 OID 18525)
-- Name: instrument instrument_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument
    ADD CONSTRAINT instrument_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3708 (class 2606 OID 18543)
-- Name: instrument_service instrument_service_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_service
    ADD CONSTRAINT instrument_service_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3666 (class 2606 OID 19232)
-- Name: jhi_authority jhi_authority_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_authority
    ADD CONSTRAINT jhi_authority_pkey PRIMARY KEY (id);


--
-- TOC entry 3674 (class 2606 OID 18391)
-- Name: jhi_persistent_audit_evt_data jhi_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_persistent_audit_evt_data
    ADD CONSTRAINT jhi_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- TOC entry 3668 (class 2606 OID 19209)
-- Name: jhi_user_authority jhi_user_authority_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user_authority
    ADD CONSTRAINT jhi_user_authority_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 3660 (class 2606 OID 18354)
-- Name: jhi_user jhi_user_email_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user
    ADD CONSTRAINT jhi_user_email_key UNIQUE (email);


--
-- TOC entry 3662 (class 2606 OID 18356)
-- Name: jhi_user jhi_user_login_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user
    ADD CONSTRAINT jhi_user_login_key UNIQUE (login);


--
-- TOC entry 3782 (class 2606 OID 18843)
-- Name: letter_person_group letter_person_group_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter_person_group
    ADD CONSTRAINT letter_person_group_pkey PRIMARY KEY (letters_id, person_groups_id);


--
-- TOC entry 3694 (class 2606 OID 18496)
-- Name: membership_fee_for_period membership_fee_for_period_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_for_period
    ADD CONSTRAINT membership_fee_for_period_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3786 (class 2606 OID 19225)
-- Name: notification_rule_target_audiences notification_rule_target_audiences_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification_rule_target_audiences
    ADD CONSTRAINT notification_rule_target_audiences_pkey PRIMARY KEY (notification_rules_id, target_role_id);


--
-- TOC entry 3678 (class 2606 OID 18414)
-- Name: person_person_groups person_person_groups_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_person_groups
    ADD CONSTRAINT person_person_groups_pkey PRIMARY KEY (people_id, person_groups_id);


--
-- TOC entry 3698 (class 2606 OID 18507)
-- Name: address pk_address; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.address
    ADD CONSTRAINT pk_address PRIMARY KEY (id);


--
-- TOC entry 3744 (class 2606 OID 18689)
-- Name: appointment pk_appointment; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment
    ADD CONSTRAINT pk_appointment PRIMARY KEY (id);


--
-- TOC entry 3748 (class 2606 OID 18704)
-- Name: appointment_type pk_appointment_type; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_type
    ADD CONSTRAINT pk_appointment_type PRIMARY KEY (id);


--
-- TOC entry 3764 (class 2606 OID 18768)
-- Name: arranger pk_arranger; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.arranger
    ADD CONSTRAINT pk_arranger PRIMARY KEY (id);


--
-- TOC entry 3720 (class 2606 OID 18589)
-- Name: award pk_award; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.award
    ADD CONSTRAINT pk_award PRIMARY KEY (id);


--
-- TOC entry 3776 (class 2606 OID 18814)
-- Name: bank_import_data pk_bank_import_data; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.bank_import_data
    ADD CONSTRAINT pk_bank_import_data PRIMARY KEY (id);


--
-- TOC entry 3728 (class 2606 OID 18624)
-- Name: cashbook pk_cashbook; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook
    ADD CONSTRAINT pk_cashbook PRIMARY KEY (id);


--
-- TOC entry 3794 (class 2606 OID 18889)
-- Name: cashbook_account pk_cashbook_account; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_account
    ADD CONSTRAINT pk_cashbook_account PRIMARY KEY (id);


--
-- TOC entry 3734 (class 2606 OID 18651)
-- Name: cashbook_category pk_cashbook_category; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_category
    ADD CONSTRAINT pk_cashbook_category PRIMARY KEY (id);


--
-- TOC entry 3732 (class 2606 OID 18640)
-- Name: cashbook_entry pk_cashbook_entry; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry
    ADD CONSTRAINT pk_cashbook_entry PRIMARY KEY (id);


--
-- TOC entry 3714 (class 2606 OID 18559)
-- Name: clothing pk_clothing; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing
    ADD CONSTRAINT pk_clothing PRIMARY KEY (id);


--
-- TOC entry 3772 (class 2606 OID 18803)
-- Name: clothing_group pk_clothing_group; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing_group
    ADD CONSTRAINT pk_clothing_group PRIMARY KEY (id);


--
-- TOC entry 3716 (class 2606 OID 18567)
-- Name: clothing_type pk_clothing_type; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing_type
    ADD CONSTRAINT pk_clothing_type PRIMARY KEY (id);


--
-- TOC entry 3754 (class 2606 OID 18728)
-- Name: color_code pk_color_code; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.color_code
    ADD CONSTRAINT pk_color_code PRIMARY KEY (id);


--
-- TOC entry 3802 (class 2606 OID 19282)
-- Name: column_config pk_column_config; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.column_config
    ADD CONSTRAINT pk_column_config PRIMARY KEY (id);


--
-- TOC entry 3738 (class 2606 OID 18673)
-- Name: company pk_company; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.company
    ADD CONSTRAINT pk_company PRIMARY KEY (id);


--
-- TOC entry 3760 (class 2606 OID 18755)
-- Name: composer pk_composer; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composer
    ADD CONSTRAINT pk_composer PRIMARY KEY (id);


--
-- TOC entry 3750 (class 2606 OID 18715)
-- Name: composition pk_composition; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT pk_composition PRIMARY KEY (id);


--
-- TOC entry 3792 (class 2606 OID 18878)
-- Name: customization pk_customization; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.customization
    ADD CONSTRAINT pk_customization PRIMARY KEY (id);


--
-- TOC entry 3656 (class 2606 OID 18335)
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- TOC entry 3798 (class 2606 OID 19254)
-- Name: email_verification_link pk_email_verification_link; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.email_verification_link
    ADD CONSTRAINT pk_email_verification_link PRIMARY KEY (id);


--
-- TOC entry 3800 (class 2606 OID 19271)
-- Name: filter pk_filter; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.filter
    ADD CONSTRAINT pk_filter PRIMARY KEY (id);


--
-- TOC entry 3758 (class 2606 OID 18747)
-- Name: genre pk_genre; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.genre
    ADD CONSTRAINT pk_genre PRIMARY KEY (id);


--
-- TOC entry 3718 (class 2606 OID 18578)
-- Name: honor pk_honor; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.honor
    ADD CONSTRAINT pk_honor PRIMARY KEY (id);


--
-- TOC entry 3704 (class 2606 OID 18523)
-- Name: instrument pk_instrument; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument
    ADD CONSTRAINT pk_instrument PRIMARY KEY (id);


--
-- TOC entry 3710 (class 2606 OID 18541)
-- Name: instrument_service pk_instrument_service; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_service
    ADD CONSTRAINT pk_instrument_service PRIMARY KEY (id);


--
-- TOC entry 3706 (class 2606 OID 18533)
-- Name: instrument_type pk_instrument_type; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_type
    ADD CONSTRAINT pk_instrument_type PRIMARY KEY (id);


--
-- TOC entry 3671 (class 2606 OID 18386)
-- Name: jhi_persistent_audit_event pk_jhi_persistent_audit_event; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_persistent_audit_event
    ADD CONSTRAINT pk_jhi_persistent_audit_event PRIMARY KEY (event_id);


--
-- TOC entry 3664 (class 2606 OID 18352)
-- Name: jhi_user pk_jhi_user; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user
    ADD CONSTRAINT pk_jhi_user PRIMARY KEY (id);


--
-- TOC entry 3780 (class 2606 OID 18838)
-- Name: letter pk_letter; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter
    ADD CONSTRAINT pk_letter PRIMARY KEY (id);


--
-- TOC entry 3688 (class 2606 OID 18464)
-- Name: membership pk_membership; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership
    ADD CONSTRAINT pk_membership PRIMARY KEY (id);


--
-- TOC entry 3690 (class 2606 OID 18475)
-- Name: membership_fee pk_membership_fee; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee
    ADD CONSTRAINT pk_membership_fee PRIMARY KEY (id);


--
-- TOC entry 3692 (class 2606 OID 18483)
-- Name: membership_fee_amount pk_membership_fee_amount; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_amount
    ADD CONSTRAINT pk_membership_fee_amount PRIMARY KEY (id);


--
-- TOC entry 3696 (class 2606 OID 18494)
-- Name: membership_fee_for_period pk_membership_fee_for_period; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_for_period
    ADD CONSTRAINT pk_membership_fee_for_period PRIMARY KEY (id);


--
-- TOC entry 3756 (class 2606 OID 18739)
-- Name: music_book pk_music_book; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.music_book
    ADD CONSTRAINT pk_music_book PRIMARY KEY (id);


--
-- TOC entry 3788 (class 2606 OID 18867)
-- Name: notification pk_notification; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification
    ADD CONSTRAINT pk_notification PRIMARY KEY (id);


--
-- TOC entry 3784 (class 2606 OID 18854)
-- Name: notification_rule pk_notification_rule; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification_rule
    ADD CONSTRAINT pk_notification_rule PRIMARY KEY (id);


--
-- TOC entry 3770 (class 2606 OID 18795)
-- Name: official_appointment pk_official_appointment; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.official_appointment
    ADD CONSTRAINT pk_official_appointment PRIMARY KEY (id);


--
-- TOC entry 3676 (class 2606 OID 18409)
-- Name: person pk_person; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person
    ADD CONSTRAINT pk_person PRIMARY KEY (id);


--
-- TOC entry 3726 (class 2606 OID 18613)
-- Name: person_address pk_person_address; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_address
    ADD CONSTRAINT pk_person_address PRIMARY KEY (id);


--
-- TOC entry 3724 (class 2606 OID 18605)
-- Name: person_award pk_person_award; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_award
    ADD CONSTRAINT pk_person_award PRIMARY KEY (id);


--
-- TOC entry 3712 (class 2606 OID 18551)
-- Name: person_clothing pk_person_clothing; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_clothing
    ADD CONSTRAINT pk_person_clothing PRIMARY KEY (id);


--
-- TOC entry 3680 (class 2606 OID 18422)
-- Name: person_group pk_person_group; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_group
    ADD CONSTRAINT pk_person_group PRIMARY KEY (id);


--
-- TOC entry 3722 (class 2606 OID 18597)
-- Name: person_honor pk_person_honor; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_honor
    ADD CONSTRAINT pk_person_honor PRIMARY KEY (id);


--
-- TOC entry 3686 (class 2606 OID 18456)
-- Name: person_info pk_person_info; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_info
    ADD CONSTRAINT pk_person_info PRIMARY KEY (id);


--
-- TOC entry 3700 (class 2606 OID 18515)
-- Name: person_instrument pk_person_instrument; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_instrument
    ADD CONSTRAINT pk_person_instrument PRIMARY KEY (id);


--
-- TOC entry 3736 (class 2606 OID 18662)
-- Name: receipt pk_receipt; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.receipt
    ADD CONSTRAINT pk_receipt PRIMARY KEY (id);


--
-- TOC entry 3768 (class 2606 OID 18784)
-- Name: report pk_report; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.report
    ADD CONSTRAINT pk_report PRIMARY KEY (id);


--
-- TOC entry 3682 (class 2606 OID 18443)
-- Name: role pk_role; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- TOC entry 3778 (class 2606 OID 18827)
-- Name: template pk_template; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.template
    ADD CONSTRAINT pk_template PRIMARY KEY (id);


--
-- TOC entry 3804 (class 2606 OID 19290)
-- Name: user_column_config pk_user_column_config; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.user_column_config
    ADD CONSTRAINT pk_user_column_config PRIMARY KEY (id);


--
-- TOC entry 3684 (class 2606 OID 19223)
-- Name: role role_name_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- TOC entry 3796 (class 2606 OID 19234)
-- Name: cashbook_account ux_cashbook_account_receipt_code; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_account
    ADD CONSTRAINT ux_cashbook_account_receipt_code UNIQUE (receipt_code);


--
-- TOC entry 3592 (class 2606 OID 17720)
-- Name: appointment appointment_official_appointment_id_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment
    ADD CONSTRAINT appointment_official_appointment_id_key UNIQUE (official_appointment_id);


--
-- TOC entry 3596 (class 2606 OID 17725)
-- Name: appointment_persons appointment_persons_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_persons
    ADD CONSTRAINT appointment_persons_pkey PRIMARY KEY (appointments_id, persons_id);


--
-- TOC entry 3616 (class 2606 OID 17802)
-- Name: arranger_compositions arranger_compositions_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.arranger_compositions
    ADD CONSTRAINT arranger_compositions_pkey PRIMARY KEY (arrangers_id, compositions_id);


--
-- TOC entry 3624 (class 2606 OID 17845)
-- Name: bank_import_data bank_import_data_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.bank_import_data
    ADD CONSTRAINT bank_import_data_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3580 (class 2606 OID 17658)
-- Name: cashbook_cashbook_entries cashbook_cashbook_entries_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_cashbook_entries
    ADD CONSTRAINT cashbook_cashbook_entries_pkey PRIMARY KEY (cashbooks_id, cashbook_entries_id);


--
-- TOC entry 3590 (class 2606 OID 17707)
-- Name: company_person_groups company_person_groups_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.company_person_groups
    ADD CONSTRAINT company_person_groups_pkey PRIMARY KEY (companies_id, person_groups_id);


--
-- TOC entry 3612 (class 2606 OID 17789)
-- Name: composer_compositions composer_compositions_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composer_compositions
    ADD CONSTRAINT composer_compositions_pkey PRIMARY KEY (composers_id, compositions_id);


--
-- TOC entry 3602 (class 2606 OID 17749)
-- Name: composition_appointments composition_appointments_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition_appointments
    ADD CONSTRAINT composition_appointments_pkey PRIMARY KEY (compositions_id, appointments_id);


--
-- TOC entry 3640 (class 2606 OID 18235)
-- Name: customization customization_name_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.customization
    ADD CONSTRAINT customization_name_key UNIQUE (name);


--
-- TOC entry 3552 (class 2606 OID 17554)
-- Name: instrument instrument_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument
    ADD CONSTRAINT instrument_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3558 (class 2606 OID 17572)
-- Name: instrument_service instrument_service_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_service
    ADD CONSTRAINT instrument_service_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3516 (class 2606 OID 18261)
-- Name: jhi_authority jhi_authority_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_authority
    ADD CONSTRAINT jhi_authority_pkey PRIMARY KEY (id);


--
-- TOC entry 3524 (class 2606 OID 17420)
-- Name: jhi_persistent_audit_evt_data jhi_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_persistent_audit_evt_data
    ADD CONSTRAINT jhi_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- TOC entry 3518 (class 2606 OID 18238)
-- Name: jhi_user_authority jhi_user_authority_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user_authority
    ADD CONSTRAINT jhi_user_authority_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 3510 (class 2606 OID 17383)
-- Name: jhi_user jhi_user_email_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user
    ADD CONSTRAINT jhi_user_email_key UNIQUE (email);


--
-- TOC entry 3512 (class 2606 OID 17385)
-- Name: jhi_user jhi_user_login_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user
    ADD CONSTRAINT jhi_user_login_key UNIQUE (login);


--
-- TOC entry 3632 (class 2606 OID 17872)
-- Name: letter_person_group letter_person_group_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter_person_group
    ADD CONSTRAINT letter_person_group_pkey PRIMARY KEY (letters_id, person_groups_id);


--
-- TOC entry 3544 (class 2606 OID 17525)
-- Name: membership_fee_for_period membership_fee_for_period_cashbook_entry_id_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_for_period
    ADD CONSTRAINT membership_fee_for_period_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- TOC entry 3636 (class 2606 OID 18254)
-- Name: notification_rule_target_audiences notification_rule_target_audiences_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification_rule_target_audiences
    ADD CONSTRAINT notification_rule_target_audiences_pkey PRIMARY KEY (notification_rules_id, target_role_id);


--
-- TOC entry 3528 (class 2606 OID 17443)
-- Name: person_person_groups person_person_groups_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_person_groups
    ADD CONSTRAINT person_person_groups_pkey PRIMARY KEY (people_id, person_groups_id);


--
-- TOC entry 3548 (class 2606 OID 17536)
-- Name: address pk_address; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.address
    ADD CONSTRAINT pk_address PRIMARY KEY (id);


--
-- TOC entry 3594 (class 2606 OID 17718)
-- Name: appointment pk_appointment; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment
    ADD CONSTRAINT pk_appointment PRIMARY KEY (id);


--
-- TOC entry 3598 (class 2606 OID 17733)
-- Name: appointment_type pk_appointment_type; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_type
    ADD CONSTRAINT pk_appointment_type PRIMARY KEY (id);


--
-- TOC entry 3614 (class 2606 OID 17797)
-- Name: arranger pk_arranger; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.arranger
    ADD CONSTRAINT pk_arranger PRIMARY KEY (id);


--
-- TOC entry 3570 (class 2606 OID 17618)
-- Name: award pk_award; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.award
    ADD CONSTRAINT pk_award PRIMARY KEY (id);


--
-- TOC entry 3626 (class 2606 OID 17843)
-- Name: bank_import_data pk_bank_import_data; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.bank_import_data
    ADD CONSTRAINT pk_bank_import_data PRIMARY KEY (id);


--
-- TOC entry 3578 (class 2606 OID 17653)
-- Name: cashbook pk_cashbook; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook
    ADD CONSTRAINT pk_cashbook PRIMARY KEY (id);


--
-- TOC entry 3644 (class 2606 OID 17918)
-- Name: cashbook_account pk_cashbook_account; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_account
    ADD CONSTRAINT pk_cashbook_account PRIMARY KEY (id);


--
-- TOC entry 3584 (class 2606 OID 17680)
-- Name: cashbook_category pk_cashbook_category; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_category
    ADD CONSTRAINT pk_cashbook_category PRIMARY KEY (id);


--
-- TOC entry 3582 (class 2606 OID 17669)
-- Name: cashbook_entry pk_cashbook_entry; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry
    ADD CONSTRAINT pk_cashbook_entry PRIMARY KEY (id);


--
-- TOC entry 3564 (class 2606 OID 17588)
-- Name: clothing pk_clothing; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing
    ADD CONSTRAINT pk_clothing PRIMARY KEY (id);


--
-- TOC entry 3622 (class 2606 OID 17832)
-- Name: clothing_group pk_clothing_group; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing_group
    ADD CONSTRAINT pk_clothing_group PRIMARY KEY (id);


--
-- TOC entry 3566 (class 2606 OID 17596)
-- Name: clothing_type pk_clothing_type; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing_type
    ADD CONSTRAINT pk_clothing_type PRIMARY KEY (id);


--
-- TOC entry 3604 (class 2606 OID 17757)
-- Name: color_code pk_color_code; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.color_code
    ADD CONSTRAINT pk_color_code PRIMARY KEY (id);


--
-- TOC entry 3652 (class 2606 OID 18311)
-- Name: column_config pk_column_config; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.column_config
    ADD CONSTRAINT pk_column_config PRIMARY KEY (id);


--
-- TOC entry 3588 (class 2606 OID 17702)
-- Name: company pk_company; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.company
    ADD CONSTRAINT pk_company PRIMARY KEY (id);


--
-- TOC entry 3610 (class 2606 OID 17784)
-- Name: composer pk_composer; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composer
    ADD CONSTRAINT pk_composer PRIMARY KEY (id);


--
-- TOC entry 3600 (class 2606 OID 17744)
-- Name: composition pk_composition; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT pk_composition PRIMARY KEY (id);


--
-- TOC entry 3642 (class 2606 OID 17907)
-- Name: customization pk_customization; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.customization
    ADD CONSTRAINT pk_customization PRIMARY KEY (id);


--
-- TOC entry 3506 (class 2606 OID 17364)
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- TOC entry 3648 (class 2606 OID 18283)
-- Name: email_verification_link pk_email_verification_link; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.email_verification_link
    ADD CONSTRAINT pk_email_verification_link PRIMARY KEY (id);


--
-- TOC entry 3650 (class 2606 OID 18300)
-- Name: filter pk_filter; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.filter
    ADD CONSTRAINT pk_filter PRIMARY KEY (id);


--
-- TOC entry 3608 (class 2606 OID 17776)
-- Name: genre pk_genre; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.genre
    ADD CONSTRAINT pk_genre PRIMARY KEY (id);


--
-- TOC entry 3568 (class 2606 OID 17607)
-- Name: honor pk_honor; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.honor
    ADD CONSTRAINT pk_honor PRIMARY KEY (id);


--
-- TOC entry 3554 (class 2606 OID 17552)
-- Name: instrument pk_instrument; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument
    ADD CONSTRAINT pk_instrument PRIMARY KEY (id);


--
-- TOC entry 3560 (class 2606 OID 17570)
-- Name: instrument_service pk_instrument_service; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_service
    ADD CONSTRAINT pk_instrument_service PRIMARY KEY (id);


--
-- TOC entry 3556 (class 2606 OID 17562)
-- Name: instrument_type pk_instrument_type; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_type
    ADD CONSTRAINT pk_instrument_type PRIMARY KEY (id);


--
-- TOC entry 3521 (class 2606 OID 17415)
-- Name: jhi_persistent_audit_event pk_jhi_persistent_audit_event; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_persistent_audit_event
    ADD CONSTRAINT pk_jhi_persistent_audit_event PRIMARY KEY (event_id);


--
-- TOC entry 3514 (class 2606 OID 17381)
-- Name: jhi_user pk_jhi_user; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user
    ADD CONSTRAINT pk_jhi_user PRIMARY KEY (id);


--
-- TOC entry 3630 (class 2606 OID 17867)
-- Name: letter pk_letter; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter
    ADD CONSTRAINT pk_letter PRIMARY KEY (id);


--
-- TOC entry 3538 (class 2606 OID 17493)
-- Name: membership pk_membership; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership
    ADD CONSTRAINT pk_membership PRIMARY KEY (id);


--
-- TOC entry 3540 (class 2606 OID 17504)
-- Name: membership_fee pk_membership_fee; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee
    ADD CONSTRAINT pk_membership_fee PRIMARY KEY (id);


--
-- TOC entry 3542 (class 2606 OID 17512)
-- Name: membership_fee_amount pk_membership_fee_amount; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_amount
    ADD CONSTRAINT pk_membership_fee_amount PRIMARY KEY (id);


--
-- TOC entry 3546 (class 2606 OID 17523)
-- Name: membership_fee_for_period pk_membership_fee_for_period; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_for_period
    ADD CONSTRAINT pk_membership_fee_for_period PRIMARY KEY (id);


--
-- TOC entry 3606 (class 2606 OID 17768)
-- Name: music_book pk_music_book; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.music_book
    ADD CONSTRAINT pk_music_book PRIMARY KEY (id);


--
-- TOC entry 3638 (class 2606 OID 17896)
-- Name: notification pk_notification; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification
    ADD CONSTRAINT pk_notification PRIMARY KEY (id);


--
-- TOC entry 3634 (class 2606 OID 17883)
-- Name: notification_rule pk_notification_rule; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification_rule
    ADD CONSTRAINT pk_notification_rule PRIMARY KEY (id);


--
-- TOC entry 3620 (class 2606 OID 17824)
-- Name: official_appointment pk_official_appointment; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.official_appointment
    ADD CONSTRAINT pk_official_appointment PRIMARY KEY (id);


--
-- TOC entry 3526 (class 2606 OID 17438)
-- Name: person pk_person; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person
    ADD CONSTRAINT pk_person PRIMARY KEY (id);


--
-- TOC entry 3576 (class 2606 OID 17642)
-- Name: person_address pk_person_address; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_address
    ADD CONSTRAINT pk_person_address PRIMARY KEY (id);


--
-- TOC entry 3574 (class 2606 OID 17634)
-- Name: person_award pk_person_award; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_award
    ADD CONSTRAINT pk_person_award PRIMARY KEY (id);


--
-- TOC entry 3562 (class 2606 OID 17580)
-- Name: person_clothing pk_person_clothing; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_clothing
    ADD CONSTRAINT pk_person_clothing PRIMARY KEY (id);


--
-- TOC entry 3530 (class 2606 OID 17451)
-- Name: person_group pk_person_group; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_group
    ADD CONSTRAINT pk_person_group PRIMARY KEY (id);


--
-- TOC entry 3572 (class 2606 OID 17626)
-- Name: person_honor pk_person_honor; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_honor
    ADD CONSTRAINT pk_person_honor PRIMARY KEY (id);


--
-- TOC entry 3536 (class 2606 OID 17485)
-- Name: person_info pk_person_info; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_info
    ADD CONSTRAINT pk_person_info PRIMARY KEY (id);


--
-- TOC entry 3550 (class 2606 OID 17544)
-- Name: person_instrument pk_person_instrument; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_instrument
    ADD CONSTRAINT pk_person_instrument PRIMARY KEY (id);


--
-- TOC entry 3586 (class 2606 OID 17691)
-- Name: receipt pk_receipt; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.receipt
    ADD CONSTRAINT pk_receipt PRIMARY KEY (id);


--
-- TOC entry 3618 (class 2606 OID 17813)
-- Name: report pk_report; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.report
    ADD CONSTRAINT pk_report PRIMARY KEY (id);


--
-- TOC entry 3532 (class 2606 OID 17472)
-- Name: role pk_role; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- TOC entry 3628 (class 2606 OID 17856)
-- Name: template pk_template; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.template
    ADD CONSTRAINT pk_template PRIMARY KEY (id);


--
-- TOC entry 3654 (class 2606 OID 18319)
-- Name: user_column_config pk_user_column_config; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.user_column_config
    ADD CONSTRAINT pk_user_column_config PRIMARY KEY (id);


--
-- TOC entry 3534 (class 2606 OID 18252)
-- Name: role role_name_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- TOC entry 3646 (class 2606 OID 18263)
-- Name: cashbook_account ux_cashbook_account_receipt_code; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_account
    ADD CONSTRAINT ux_cashbook_account_receipt_code UNIQUE (receipt_code);


--
-- TOC entry 3369 (class 1259 OID 16448)
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_persistent_audit_event ON public.jhi_persistent_audit_event USING btree (principal, event_date);


--
-- TOC entry 3372 (class 1259 OID 16449)
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_persistent_audit_evt_data ON public.jhi_persistent_audit_evt_data USING btree (event_id);


--
-- TOC entry 3357 (class 1259 OID 16414)
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_email ON public.jhi_user USING btree (email);


--
-- TOC entry 3358 (class 1259 OID 16413)
-- Name: idx_user_login; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_login ON public.jhi_user USING btree (login);


--
-- TOC entry 3669 (class 1259 OID 18392)
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX idx_persistent_audit_event ON tenant1.jhi_persistent_audit_event USING btree (principal, event_date);


--
-- TOC entry 3672 (class 1259 OID 18393)
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX idx_persistent_audit_evt_data ON tenant1.jhi_persistent_audit_evt_data USING btree (event_id);


--
-- TOC entry 3657 (class 1259 OID 18358)
-- Name: idx_user_email; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_email ON tenant1.jhi_user USING btree (email);


--
-- TOC entry 3658 (class 1259 OID 18357)
-- Name: idx_user_login; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_login ON tenant1.jhi_user USING btree (login);


--
-- TOC entry 3519 (class 1259 OID 17421)
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX idx_persistent_audit_event ON tenant2.jhi_persistent_audit_event USING btree (principal, event_date);


--
-- TOC entry 3522 (class 1259 OID 17422)
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX idx_persistent_audit_evt_data ON tenant2.jhi_persistent_audit_evt_data USING btree (event_id);


--
-- TOC entry 3507 (class 1259 OID 17387)
-- Name: idx_user_email; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_email ON tenant2.jhi_user USING btree (email);


--
-- TOC entry 3508 (class 1259 OID 17386)
-- Name: idx_user_login; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_login ON tenant2.jhi_user USING btree (login);


--
-- TOC entry 3842 (class 2606 OID 17126)
-- Name: appointment fk_appointment_official_appointment_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT fk_appointment_official_appointment_id FOREIGN KEY (official_appointment_id) REFERENCES public.official_appointment(id);


--
-- TOC entry 3844 (class 2606 OID 17131)
-- Name: appointment_persons fk_appointment_persons_appointments_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_appointments_id FOREIGN KEY (appointments_id) REFERENCES public.appointment(id);


--
-- TOC entry 3845 (class 2606 OID 17136)
-- Name: appointment_persons fk_appointment_persons_persons_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_persons_id FOREIGN KEY (persons_id) REFERENCES public.person(id);


--
-- TOC entry 3843 (class 2606 OID 17141)
-- Name: appointment fk_appointment_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT fk_appointment_type_id FOREIGN KEY (type_id) REFERENCES public.appointment_type(id);


--
-- TOC entry 3846 (class 2606 OID 17146)
-- Name: appointment_type fk_appointment_type_person_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type
    ADD CONSTRAINT fk_appointment_type_person_group_id FOREIGN KEY (person_group_id) REFERENCES public.person_group(id);


--
-- TOC entry 3857 (class 2606 OID 17201)
-- Name: arranger_compositions fk_arranger_compositions_arrangers_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_arrangers_id FOREIGN KEY (arrangers_id) REFERENCES public.arranger(id);


--
-- TOC entry 3858 (class 2606 OID 17206)
-- Name: arranger_compositions fk_arranger_compositions_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- TOC entry 3862 (class 2606 OID 17292)
-- Name: bank_import_data fk_bank_import_data_cashbook_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES public.cashbook_category(id);


--
-- TOC entry 3861 (class 2606 OID 17221)
-- Name: bank_import_data fk_bank_import_data_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3832 (class 2606 OID 17081)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbook_entries_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbook_entries_id FOREIGN KEY (cashbook_entries_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3831 (class 2606 OID 17076)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbooks_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbooks_id FOREIGN KEY (cashbooks_id) REFERENCES public.cashbook(id);


--
-- TOC entry 3837 (class 2606 OID 17106)
-- Name: cashbook_category fk_cashbook_category_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category
    ADD CONSTRAINT fk_cashbook_category_parent_id FOREIGN KEY (parent_id) REFERENCES public.cashbook_category(id);


--
-- TOC entry 3833 (class 2606 OID 17086)
-- Name: cashbook_entry fk_cashbook_entry_appointment_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_appointment_id FOREIGN KEY (appointment_id) REFERENCES public.appointment(id);


--
-- TOC entry 3836 (class 2606 OID 17101)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_account_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_account_id FOREIGN KEY (cashbook_account_id) REFERENCES public.cashbook_account(id);


--
-- TOC entry 3834 (class 2606 OID 17091)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES public.cashbook_category(id);


--
-- TOC entry 3835 (class 2606 OID 17096)
-- Name: cashbook_entry fk_cashbook_entry_receipt_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_receipt_id FOREIGN KEY (receipt_id) REFERENCES public.receipt(id);


--
-- TOC entry 3860 (class 2606 OID 17216)
-- Name: clothing_group fk_clothing_group_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group
    ADD CONSTRAINT fk_clothing_group_type_id FOREIGN KEY (type_id) REFERENCES public.clothing_type(id);


--
-- TOC entry 3824 (class 2606 OID 17041)
-- Name: clothing fk_clothing_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing
    ADD CONSTRAINT fk_clothing_type_id FOREIGN KEY (type_id) REFERENCES public.clothing_type(id);


--
-- TOC entry 3840 (class 2606 OID 17116)
-- Name: company_person_groups fk_company_person_groups_companies_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_companies_id FOREIGN KEY (companies_id) REFERENCES public.company(id);


--
-- TOC entry 3841 (class 2606 OID 17121)
-- Name: company_person_groups fk_company_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- TOC entry 3855 (class 2606 OID 17191)
-- Name: composer_compositions fk_composer_compositions_composers_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_composers_id FOREIGN KEY (composers_id) REFERENCES public.composer(id);


--
-- TOC entry 3856 (class 2606 OID 17196)
-- Name: composer_compositions fk_composer_compositions_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- TOC entry 3854 (class 2606 OID 17156)
-- Name: composition_appointments fk_composition_appointments_appointments_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_appointments_id FOREIGN KEY (appointments_id) REFERENCES public.official_appointment(id);


--
-- TOC entry 3853 (class 2606 OID 17151)
-- Name: composition_appointments fk_composition_appointments_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- TOC entry 3847 (class 2606 OID 17161)
-- Name: composition fk_composition_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3852 (class 2606 OID 17186)
-- Name: composition fk_composition_color_code_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_color_code_id FOREIGN KEY (color_code_id) REFERENCES public.color_code(id);


--
-- TOC entry 3850 (class 2606 OID 17176)
-- Name: composition fk_composition_genre_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_genre_id FOREIGN KEY (genre_id) REFERENCES public.genre(id);


--
-- TOC entry 3851 (class 2606 OID 17181)
-- Name: composition fk_composition_music_book_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_music_book_id FOREIGN KEY (music_book_id) REFERENCES public.music_book(id);


--
-- TOC entry 3848 (class 2606 OID 17166)
-- Name: composition fk_composition_ordering_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_ordering_company_id FOREIGN KEY (ordering_company_id) REFERENCES public.company(id);


--
-- TOC entry 3849 (class 2606 OID 17171)
-- Name: composition fk_composition_publisher_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_publisher_id FOREIGN KEY (publisher_id) REFERENCES public.company(id);


--
-- TOC entry 3870 (class 2606 OID 17311)
-- Name: email_verification_link fk_email_verification_link_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link
    ADD CONSTRAINT fk_email_verification_link_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3807 (class 2606 OID 16450)
-- Name: jhi_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES public.jhi_persistent_audit_event(event_id);


--
-- TOC entry 3819 (class 2606 OID 17016)
-- Name: instrument fk_instrument_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT fk_instrument_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3820 (class 2606 OID 17021)
-- Name: instrument_service fk_instrument_service_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT fk_instrument_service_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3821 (class 2606 OID 17026)
-- Name: instrument_service fk_instrument_service_instrument_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT fk_instrument_service_instrument_id FOREIGN KEY (instrument_id) REFERENCES public.instrument(id);


--
-- TOC entry 3818 (class 2606 OID 17011)
-- Name: instrument fk_instrument_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT fk_instrument_type_id FOREIGN KEY (type_id) REFERENCES public.instrument_type(id);


--
-- TOC entry 3864 (class 2606 OID 17251)
-- Name: letter_person_group fk_letter_person_group_letters_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_letters_id FOREIGN KEY (letters_id) REFERENCES public.letter(id);


--
-- TOC entry 3865 (class 2606 OID 17256)
-- Name: letter_person_group fk_letter_person_group_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- TOC entry 3863 (class 2606 OID 17246)
-- Name: letter fk_letter_template_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter
    ADD CONSTRAINT fk_letter_template_id FOREIGN KEY (template_id) REFERENCES public.template(id);


--
-- TOC entry 3813 (class 2606 OID 16986)
-- Name: membership_fee_amount fk_membership_fee_amount_membership_fee_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount
    ADD CONSTRAINT fk_membership_fee_amount_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES public.membership_fee(id);


--
-- TOC entry 3814 (class 2606 OID 16991)
-- Name: membership_fee_for_period fk_membership_fee_for_period_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3815 (class 2606 OID 16996)
-- Name: membership_fee_for_period fk_membership_fee_for_period_membership_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_membership_id FOREIGN KEY (membership_id) REFERENCES public.membership(id);


--
-- TOC entry 3812 (class 2606 OID 16981)
-- Name: membership fk_membership_membership_fee_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT fk_membership_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES public.membership_fee(id);


--
-- TOC entry 3811 (class 2606 OID 16976)
-- Name: membership fk_membership_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT fk_membership_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3869 (class 2606 OID 17241)
-- Name: notification fk_notification_notification_rule_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_notification_rule_id FOREIGN KEY (notification_rule_id) REFERENCES public.notification_rule(id);


--
-- TOC entry 3868 (class 2606 OID 17236)
-- Name: notification fk_notification_read_by_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_read_by_user_id FOREIGN KEY (read_by_user_id) REFERENCES public.jhi_user(id);


--
-- TOC entry 3866 (class 2606 OID 17226)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_notification_rules_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_notification_rules_id FOREIGN KEY (notification_rules_id) REFERENCES public.notification_rule(id);


--
-- TOC entry 3867 (class 2606 OID 17282)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_target_audiences_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_target_audiences_name FOREIGN KEY (target_role_id) REFERENCES public.role(id);


--
-- TOC entry 3859 (class 2606 OID 17211)
-- Name: official_appointment fk_official_appointment_report_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment
    ADD CONSTRAINT fk_official_appointment_report_id FOREIGN KEY (report_id) REFERENCES public.report(id);


--
-- TOC entry 3829 (class 2606 OID 17066)
-- Name: person_address fk_person_address_address_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT fk_person_address_address_id FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- TOC entry 3830 (class 2606 OID 17071)
-- Name: person_address fk_person_address_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT fk_person_address_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3827 (class 2606 OID 17056)
-- Name: person_award fk_person_award_award_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT fk_person_award_award_id FOREIGN KEY (award_id) REFERENCES public.award(id);


--
-- TOC entry 3828 (class 2606 OID 17061)
-- Name: person_award fk_person_award_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT fk_person_award_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3822 (class 2606 OID 17031)
-- Name: person_clothing fk_person_clothing_clothing_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT fk_person_clothing_clothing_id FOREIGN KEY (clothing_id) REFERENCES public.clothing(id);


--
-- TOC entry 3823 (class 2606 OID 17036)
-- Name: person_clothing fk_person_clothing_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT fk_person_clothing_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3810 (class 2606 OID 16956)
-- Name: person_group fk_person_group_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group
    ADD CONSTRAINT fk_person_group_parent_id FOREIGN KEY (parent_id) REFERENCES public.person_group(id);


--
-- TOC entry 3825 (class 2606 OID 17046)
-- Name: person_honor fk_person_honor_honor_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT fk_person_honor_honor_id FOREIGN KEY (honor_id) REFERENCES public.honor(id);


--
-- TOC entry 3826 (class 2606 OID 17051)
-- Name: person_honor fk_person_honor_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT fk_person_honor_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3816 (class 2606 OID 17001)
-- Name: person_instrument fk_person_instrument_instrument_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT fk_person_instrument_instrument_id FOREIGN KEY (instrument_id) REFERENCES public.instrument(id);


--
-- TOC entry 3817 (class 2606 OID 17006)
-- Name: person_instrument fk_person_instrument_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT fk_person_instrument_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- TOC entry 3808 (class 2606 OID 16946)
-- Name: person_person_groups fk_person_person_groups_people_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_people_id FOREIGN KEY (people_id) REFERENCES public.person(id);


--
-- TOC entry 3809 (class 2606 OID 16951)
-- Name: person_person_groups fk_person_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- TOC entry 3838 (class 2606 OID 17111)
-- Name: receipt fk_receipt_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT fk_receipt_company_id FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- TOC entry 3839 (class 2606 OID 17298)
-- Name: receipt fk_receipt_initial_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT fk_receipt_initial_cashbook_entry_id FOREIGN KEY (initial_cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- TOC entry 3806 (class 2606 OID 17266)
-- Name: jhi_user_authority fk_role_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- TOC entry 3871 (class 2606 OID 17347)
-- Name: user_column_config fk_user_column_config_column_config_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT fk_user_column_config_column_config_id FOREIGN KEY (column_config_id) REFERENCES public.column_config(id);


--
-- TOC entry 3872 (class 2606 OID 17352)
-- Name: user_column_config fk_user_column_config_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT fk_user_column_config_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user(id);


--
-- TOC entry 3805 (class 2606 OID 16430)
-- Name: jhi_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user(id);


--
-- TOC entry 3978 (class 2606 OID 19070)
-- Name: appointment fk_appointment_official_appointment_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment
    ADD CONSTRAINT fk_appointment_official_appointment_id FOREIGN KEY (official_appointment_id) REFERENCES tenant1.official_appointment(id);


--
-- TOC entry 3980 (class 2606 OID 19075)
-- Name: appointment_persons fk_appointment_persons_appointments_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_appointments_id FOREIGN KEY (appointments_id) REFERENCES tenant1.appointment(id);


--
-- TOC entry 3981 (class 2606 OID 19080)
-- Name: appointment_persons fk_appointment_persons_persons_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_persons_id FOREIGN KEY (persons_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3979 (class 2606 OID 19085)
-- Name: appointment fk_appointment_type_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment
    ADD CONSTRAINT fk_appointment_type_id FOREIGN KEY (type_id) REFERENCES tenant1.appointment_type(id);


--
-- TOC entry 3982 (class 2606 OID 19090)
-- Name: appointment_type fk_appointment_type_person_group_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.appointment_type
    ADD CONSTRAINT fk_appointment_type_person_group_id FOREIGN KEY (person_group_id) REFERENCES tenant1.person_group(id);


--
-- TOC entry 3993 (class 2606 OID 19145)
-- Name: arranger_compositions fk_arranger_compositions_arrangers_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_arrangers_id FOREIGN KEY (arrangers_id) REFERENCES tenant1.arranger(id);


--
-- TOC entry 3994 (class 2606 OID 19150)
-- Name: arranger_compositions fk_arranger_compositions_compositions_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant1.composition(id);


--
-- TOC entry 3998 (class 2606 OID 19236)
-- Name: bank_import_data fk_bank_import_data_cashbook_category_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES tenant1.cashbook_category(id);


--
-- TOC entry 3997 (class 2606 OID 19165)
-- Name: bank_import_data fk_bank_import_data_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3968 (class 2606 OID 19025)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbook_entries_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbook_entries_id FOREIGN KEY (cashbook_entries_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3967 (class 2606 OID 19020)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbooks_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbooks_id FOREIGN KEY (cashbooks_id) REFERENCES tenant1.cashbook(id);


--
-- TOC entry 3973 (class 2606 OID 19050)
-- Name: cashbook_category fk_cashbook_category_parent_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_category
    ADD CONSTRAINT fk_cashbook_category_parent_id FOREIGN KEY (parent_id) REFERENCES tenant1.cashbook_category(id);


--
-- TOC entry 3969 (class 2606 OID 19030)
-- Name: cashbook_entry fk_cashbook_entry_appointment_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_appointment_id FOREIGN KEY (appointment_id) REFERENCES tenant1.appointment(id);


--
-- TOC entry 3972 (class 2606 OID 19045)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_account_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_account_id FOREIGN KEY (cashbook_account_id) REFERENCES tenant1.cashbook_account(id);


--
-- TOC entry 3970 (class 2606 OID 19035)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_category_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES tenant1.cashbook_category(id);


--
-- TOC entry 3971 (class 2606 OID 19040)
-- Name: cashbook_entry fk_cashbook_entry_receipt_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_receipt_id FOREIGN KEY (receipt_id) REFERENCES tenant1.receipt(id);


--
-- TOC entry 3996 (class 2606 OID 19160)
-- Name: clothing_group fk_clothing_group_type_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing_group
    ADD CONSTRAINT fk_clothing_group_type_id FOREIGN KEY (type_id) REFERENCES tenant1.clothing_type(id);


--
-- TOC entry 3960 (class 2606 OID 18985)
-- Name: clothing fk_clothing_type_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.clothing
    ADD CONSTRAINT fk_clothing_type_id FOREIGN KEY (type_id) REFERENCES tenant1.clothing_type(id);


--
-- TOC entry 3976 (class 2606 OID 19060)
-- Name: company_person_groups fk_company_person_groups_companies_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_companies_id FOREIGN KEY (companies_id) REFERENCES tenant1.company(id);


--
-- TOC entry 3977 (class 2606 OID 19065)
-- Name: company_person_groups fk_company_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant1.person_group(id);


--
-- TOC entry 3991 (class 2606 OID 19135)
-- Name: composer_compositions fk_composer_compositions_composers_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_composers_id FOREIGN KEY (composers_id) REFERENCES tenant1.composer(id);


--
-- TOC entry 3992 (class 2606 OID 19140)
-- Name: composer_compositions fk_composer_compositions_compositions_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant1.composition(id);


--
-- TOC entry 3990 (class 2606 OID 19100)
-- Name: composition_appointments fk_composition_appointments_appointments_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_appointments_id FOREIGN KEY (appointments_id) REFERENCES tenant1.official_appointment(id);


--
-- TOC entry 3989 (class 2606 OID 19095)
-- Name: composition_appointments fk_composition_appointments_compositions_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant1.composition(id);


--
-- TOC entry 3983 (class 2606 OID 19105)
-- Name: composition fk_composition_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3988 (class 2606 OID 19130)
-- Name: composition fk_composition_color_code_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_color_code_id FOREIGN KEY (color_code_id) REFERENCES tenant1.color_code(id);


--
-- TOC entry 3986 (class 2606 OID 19120)
-- Name: composition fk_composition_genre_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_genre_id FOREIGN KEY (genre_id) REFERENCES tenant1.genre(id);


--
-- TOC entry 3987 (class 2606 OID 19125)
-- Name: composition fk_composition_music_book_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_music_book_id FOREIGN KEY (music_book_id) REFERENCES tenant1.music_book(id);


--
-- TOC entry 3984 (class 2606 OID 19110)
-- Name: composition fk_composition_ordering_company_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_ordering_company_id FOREIGN KEY (ordering_company_id) REFERENCES tenant1.company(id);


--
-- TOC entry 3985 (class 2606 OID 19115)
-- Name: composition fk_composition_publisher_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.composition
    ADD CONSTRAINT fk_composition_publisher_id FOREIGN KEY (publisher_id) REFERENCES tenant1.company(id);


--
-- TOC entry 4006 (class 2606 OID 19255)
-- Name: email_verification_link fk_email_verification_link_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.email_verification_link
    ADD CONSTRAINT fk_email_verification_link_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3943 (class 2606 OID 18394)
-- Name: jhi_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES tenant1.jhi_persistent_audit_event(event_id);


--
-- TOC entry 3955 (class 2606 OID 18960)
-- Name: instrument fk_instrument_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument
    ADD CONSTRAINT fk_instrument_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3956 (class 2606 OID 18965)
-- Name: instrument_service fk_instrument_service_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_service
    ADD CONSTRAINT fk_instrument_service_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3957 (class 2606 OID 18970)
-- Name: instrument_service fk_instrument_service_instrument_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument_service
    ADD CONSTRAINT fk_instrument_service_instrument_id FOREIGN KEY (instrument_id) REFERENCES tenant1.instrument(id);


--
-- TOC entry 3954 (class 2606 OID 18955)
-- Name: instrument fk_instrument_type_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.instrument
    ADD CONSTRAINT fk_instrument_type_id FOREIGN KEY (type_id) REFERENCES tenant1.instrument_type(id);


--
-- TOC entry 4000 (class 2606 OID 19195)
-- Name: letter_person_group fk_letter_person_group_letters_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_letters_id FOREIGN KEY (letters_id) REFERENCES tenant1.letter(id);


--
-- TOC entry 4001 (class 2606 OID 19200)
-- Name: letter_person_group fk_letter_person_group_person_groups_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant1.person_group(id);


--
-- TOC entry 3999 (class 2606 OID 19190)
-- Name: letter fk_letter_template_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.letter
    ADD CONSTRAINT fk_letter_template_id FOREIGN KEY (template_id) REFERENCES tenant1.template(id);


--
-- TOC entry 3949 (class 2606 OID 18930)
-- Name: membership_fee_amount fk_membership_fee_amount_membership_fee_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_amount
    ADD CONSTRAINT fk_membership_fee_amount_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES tenant1.membership_fee(id);


--
-- TOC entry 3950 (class 2606 OID 18935)
-- Name: membership_fee_for_period fk_membership_fee_for_period_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3951 (class 2606 OID 18940)
-- Name: membership_fee_for_period fk_membership_fee_for_period_membership_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_membership_id FOREIGN KEY (membership_id) REFERENCES tenant1.membership(id);


--
-- TOC entry 3948 (class 2606 OID 18925)
-- Name: membership fk_membership_membership_fee_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership
    ADD CONSTRAINT fk_membership_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES tenant1.membership_fee(id);


--
-- TOC entry 3947 (class 2606 OID 18920)
-- Name: membership fk_membership_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.membership
    ADD CONSTRAINT fk_membership_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 4005 (class 2606 OID 19185)
-- Name: notification fk_notification_notification_rule_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification
    ADD CONSTRAINT fk_notification_notification_rule_id FOREIGN KEY (notification_rule_id) REFERENCES tenant1.notification_rule(id);


--
-- TOC entry 4004 (class 2606 OID 19180)
-- Name: notification fk_notification_read_by_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification
    ADD CONSTRAINT fk_notification_read_by_user_id FOREIGN KEY (read_by_user_id) REFERENCES tenant1.jhi_user(id);


--
-- TOC entry 4002 (class 2606 OID 19170)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_notification_rules_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_notification_rules_id FOREIGN KEY (notification_rules_id) REFERENCES tenant1.notification_rule(id);


--
-- TOC entry 4003 (class 2606 OID 19226)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_target_audiences_name; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_target_audiences_name FOREIGN KEY (target_role_id) REFERENCES tenant1.role(id);


--
-- TOC entry 3995 (class 2606 OID 19155)
-- Name: official_appointment fk_official_appointment_report_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.official_appointment
    ADD CONSTRAINT fk_official_appointment_report_id FOREIGN KEY (report_id) REFERENCES tenant1.report(id);


--
-- TOC entry 3965 (class 2606 OID 19010)
-- Name: person_address fk_person_address_address_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_address
    ADD CONSTRAINT fk_person_address_address_id FOREIGN KEY (address_id) REFERENCES tenant1.address(id);


--
-- TOC entry 3966 (class 2606 OID 19015)
-- Name: person_address fk_person_address_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_address
    ADD CONSTRAINT fk_person_address_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3963 (class 2606 OID 19000)
-- Name: person_award fk_person_award_award_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_award
    ADD CONSTRAINT fk_person_award_award_id FOREIGN KEY (award_id) REFERENCES tenant1.award(id);


--
-- TOC entry 3964 (class 2606 OID 19005)
-- Name: person_award fk_person_award_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_award
    ADD CONSTRAINT fk_person_award_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3958 (class 2606 OID 18975)
-- Name: person_clothing fk_person_clothing_clothing_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_clothing
    ADD CONSTRAINT fk_person_clothing_clothing_id FOREIGN KEY (clothing_id) REFERENCES tenant1.clothing(id);


--
-- TOC entry 3959 (class 2606 OID 18980)
-- Name: person_clothing fk_person_clothing_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_clothing
    ADD CONSTRAINT fk_person_clothing_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3946 (class 2606 OID 18900)
-- Name: person_group fk_person_group_parent_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_group
    ADD CONSTRAINT fk_person_group_parent_id FOREIGN KEY (parent_id) REFERENCES tenant1.person_group(id);


--
-- TOC entry 3961 (class 2606 OID 18990)
-- Name: person_honor fk_person_honor_honor_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_honor
    ADD CONSTRAINT fk_person_honor_honor_id FOREIGN KEY (honor_id) REFERENCES tenant1.honor(id);


--
-- TOC entry 3962 (class 2606 OID 18995)
-- Name: person_honor fk_person_honor_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_honor
    ADD CONSTRAINT fk_person_honor_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3952 (class 2606 OID 18945)
-- Name: person_instrument fk_person_instrument_instrument_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_instrument
    ADD CONSTRAINT fk_person_instrument_instrument_id FOREIGN KEY (instrument_id) REFERENCES tenant1.instrument(id);


--
-- TOC entry 3953 (class 2606 OID 18950)
-- Name: person_instrument fk_person_instrument_person_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_instrument
    ADD CONSTRAINT fk_person_instrument_person_id FOREIGN KEY (person_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3944 (class 2606 OID 18890)
-- Name: person_person_groups fk_person_person_groups_people_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_people_id FOREIGN KEY (people_id) REFERENCES tenant1.person(id);


--
-- TOC entry 3945 (class 2606 OID 18895)
-- Name: person_person_groups fk_person_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant1.person_group(id);


--
-- TOC entry 3974 (class 2606 OID 19055)
-- Name: receipt fk_receipt_company_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.receipt
    ADD CONSTRAINT fk_receipt_company_id FOREIGN KEY (company_id) REFERENCES tenant1.company(id);


--
-- TOC entry 3975 (class 2606 OID 19242)
-- Name: receipt fk_receipt_initial_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.receipt
    ADD CONSTRAINT fk_receipt_initial_cashbook_entry_id FOREIGN KEY (initial_cashbook_entry_id) REFERENCES tenant1.cashbook_entry(id);


--
-- TOC entry 3942 (class 2606 OID 19210)
-- Name: jhi_user_authority fk_role_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user_authority
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES tenant1.role(id);


--
-- TOC entry 4007 (class 2606 OID 19291)
-- Name: user_column_config fk_user_column_config_column_config_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.user_column_config
    ADD CONSTRAINT fk_user_column_config_column_config_id FOREIGN KEY (column_config_id) REFERENCES tenant1.column_config(id);


--
-- TOC entry 4008 (class 2606 OID 19296)
-- Name: user_column_config fk_user_column_config_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.user_column_config
    ADD CONSTRAINT fk_user_column_config_user_id FOREIGN KEY (user_id) REFERENCES tenant1.jhi_user(id);


--
-- TOC entry 3941 (class 2606 OID 18374)
-- Name: jhi_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.jhi_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES tenant1.jhi_user(id);


--
-- TOC entry 3910 (class 2606 OID 18099)
-- Name: appointment fk_appointment_official_appointment_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment
    ADD CONSTRAINT fk_appointment_official_appointment_id FOREIGN KEY (official_appointment_id) REFERENCES tenant2.official_appointment(id);


--
-- TOC entry 3912 (class 2606 OID 18104)
-- Name: appointment_persons fk_appointment_persons_appointments_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_appointments_id FOREIGN KEY (appointments_id) REFERENCES tenant2.appointment(id);


--
-- TOC entry 3913 (class 2606 OID 18109)
-- Name: appointment_persons fk_appointment_persons_persons_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_persons_id FOREIGN KEY (persons_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3911 (class 2606 OID 18114)
-- Name: appointment fk_appointment_type_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment
    ADD CONSTRAINT fk_appointment_type_id FOREIGN KEY (type_id) REFERENCES tenant2.appointment_type(id);


--
-- TOC entry 3914 (class 2606 OID 18119)
-- Name: appointment_type fk_appointment_type_person_group_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.appointment_type
    ADD CONSTRAINT fk_appointment_type_person_group_id FOREIGN KEY (person_group_id) REFERENCES tenant2.person_group(id);


--
-- TOC entry 3925 (class 2606 OID 18174)
-- Name: arranger_compositions fk_arranger_compositions_arrangers_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_arrangers_id FOREIGN KEY (arrangers_id) REFERENCES tenant2.arranger(id);


--
-- TOC entry 3926 (class 2606 OID 18179)
-- Name: arranger_compositions fk_arranger_compositions_compositions_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant2.composition(id);


--
-- TOC entry 3930 (class 2606 OID 18265)
-- Name: bank_import_data fk_bank_import_data_cashbook_category_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES tenant2.cashbook_category(id);


--
-- TOC entry 3929 (class 2606 OID 18194)
-- Name: bank_import_data fk_bank_import_data_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3900 (class 2606 OID 18054)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbook_entries_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbook_entries_id FOREIGN KEY (cashbook_entries_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3899 (class 2606 OID 18049)
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbooks_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbooks_id FOREIGN KEY (cashbooks_id) REFERENCES tenant2.cashbook(id);


--
-- TOC entry 3905 (class 2606 OID 18079)
-- Name: cashbook_category fk_cashbook_category_parent_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_category
    ADD CONSTRAINT fk_cashbook_category_parent_id FOREIGN KEY (parent_id) REFERENCES tenant2.cashbook_category(id);


--
-- TOC entry 3901 (class 2606 OID 18059)
-- Name: cashbook_entry fk_cashbook_entry_appointment_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_appointment_id FOREIGN KEY (appointment_id) REFERENCES tenant2.appointment(id);


--
-- TOC entry 3904 (class 2606 OID 18074)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_account_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_account_id FOREIGN KEY (cashbook_account_id) REFERENCES tenant2.cashbook_account(id);


--
-- TOC entry 3902 (class 2606 OID 18064)
-- Name: cashbook_entry fk_cashbook_entry_cashbook_category_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES tenant2.cashbook_category(id);


--
-- TOC entry 3903 (class 2606 OID 18069)
-- Name: cashbook_entry fk_cashbook_entry_receipt_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_receipt_id FOREIGN KEY (receipt_id) REFERENCES tenant2.receipt(id);


--
-- TOC entry 3928 (class 2606 OID 18189)
-- Name: clothing_group fk_clothing_group_type_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing_group
    ADD CONSTRAINT fk_clothing_group_type_id FOREIGN KEY (type_id) REFERENCES tenant2.clothing_type(id);


--
-- TOC entry 3892 (class 2606 OID 18014)
-- Name: clothing fk_clothing_type_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.clothing
    ADD CONSTRAINT fk_clothing_type_id FOREIGN KEY (type_id) REFERENCES tenant2.clothing_type(id);


--
-- TOC entry 3908 (class 2606 OID 18089)
-- Name: company_person_groups fk_company_person_groups_companies_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_companies_id FOREIGN KEY (companies_id) REFERENCES tenant2.company(id);


--
-- TOC entry 3909 (class 2606 OID 18094)
-- Name: company_person_groups fk_company_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant2.person_group(id);


--
-- TOC entry 3923 (class 2606 OID 18164)
-- Name: composer_compositions fk_composer_compositions_composers_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_composers_id FOREIGN KEY (composers_id) REFERENCES tenant2.composer(id);


--
-- TOC entry 3924 (class 2606 OID 18169)
-- Name: composer_compositions fk_composer_compositions_compositions_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant2.composition(id);


--
-- TOC entry 3922 (class 2606 OID 18129)
-- Name: composition_appointments fk_composition_appointments_appointments_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_appointments_id FOREIGN KEY (appointments_id) REFERENCES tenant2.official_appointment(id);


--
-- TOC entry 3921 (class 2606 OID 18124)
-- Name: composition_appointments fk_composition_appointments_compositions_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_compositions_id FOREIGN KEY (compositions_id) REFERENCES tenant2.composition(id);


--
-- TOC entry 3915 (class 2606 OID 18134)
-- Name: composition fk_composition_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3920 (class 2606 OID 18159)
-- Name: composition fk_composition_color_code_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_color_code_id FOREIGN KEY (color_code_id) REFERENCES tenant2.color_code(id);


--
-- TOC entry 3918 (class 2606 OID 18149)
-- Name: composition fk_composition_genre_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_genre_id FOREIGN KEY (genre_id) REFERENCES tenant2.genre(id);


--
-- TOC entry 3919 (class 2606 OID 18154)
-- Name: composition fk_composition_music_book_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_music_book_id FOREIGN KEY (music_book_id) REFERENCES tenant2.music_book(id);


--
-- TOC entry 3916 (class 2606 OID 18139)
-- Name: composition fk_composition_ordering_company_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_ordering_company_id FOREIGN KEY (ordering_company_id) REFERENCES tenant2.company(id);


--
-- TOC entry 3917 (class 2606 OID 18144)
-- Name: composition fk_composition_publisher_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.composition
    ADD CONSTRAINT fk_composition_publisher_id FOREIGN KEY (publisher_id) REFERENCES tenant2.company(id);


--
-- TOC entry 3938 (class 2606 OID 18284)
-- Name: email_verification_link fk_email_verification_link_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.email_verification_link
    ADD CONSTRAINT fk_email_verification_link_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3875 (class 2606 OID 17423)
-- Name: jhi_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES tenant2.jhi_persistent_audit_event(event_id);


--
-- TOC entry 3887 (class 2606 OID 17989)
-- Name: instrument fk_instrument_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument
    ADD CONSTRAINT fk_instrument_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3888 (class 2606 OID 17994)
-- Name: instrument_service fk_instrument_service_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_service
    ADD CONSTRAINT fk_instrument_service_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3889 (class 2606 OID 17999)
-- Name: instrument_service fk_instrument_service_instrument_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument_service
    ADD CONSTRAINT fk_instrument_service_instrument_id FOREIGN KEY (instrument_id) REFERENCES tenant2.instrument(id);


--
-- TOC entry 3886 (class 2606 OID 17984)
-- Name: instrument fk_instrument_type_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.instrument
    ADD CONSTRAINT fk_instrument_type_id FOREIGN KEY (type_id) REFERENCES tenant2.instrument_type(id);


--
-- TOC entry 3932 (class 2606 OID 18224)
-- Name: letter_person_group fk_letter_person_group_letters_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_letters_id FOREIGN KEY (letters_id) REFERENCES tenant2.letter(id);


--
-- TOC entry 3933 (class 2606 OID 18229)
-- Name: letter_person_group fk_letter_person_group_person_groups_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant2.person_group(id);


--
-- TOC entry 3931 (class 2606 OID 18219)
-- Name: letter fk_letter_template_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.letter
    ADD CONSTRAINT fk_letter_template_id FOREIGN KEY (template_id) REFERENCES tenant2.template(id);


--
-- TOC entry 3881 (class 2606 OID 17959)
-- Name: membership_fee_amount fk_membership_fee_amount_membership_fee_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_amount
    ADD CONSTRAINT fk_membership_fee_amount_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES tenant2.membership_fee(id);


--
-- TOC entry 3882 (class 2606 OID 17964)
-- Name: membership_fee_for_period fk_membership_fee_for_period_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3883 (class 2606 OID 17969)
-- Name: membership_fee_for_period fk_membership_fee_for_period_membership_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_membership_id FOREIGN KEY (membership_id) REFERENCES tenant2.membership(id);


--
-- TOC entry 3880 (class 2606 OID 17954)
-- Name: membership fk_membership_membership_fee_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership
    ADD CONSTRAINT fk_membership_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES tenant2.membership_fee(id);


--
-- TOC entry 3879 (class 2606 OID 17949)
-- Name: membership fk_membership_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.membership
    ADD CONSTRAINT fk_membership_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3937 (class 2606 OID 18214)
-- Name: notification fk_notification_notification_rule_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification
    ADD CONSTRAINT fk_notification_notification_rule_id FOREIGN KEY (notification_rule_id) REFERENCES tenant2.notification_rule(id);


--
-- TOC entry 3936 (class 2606 OID 18209)
-- Name: notification fk_notification_read_by_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification
    ADD CONSTRAINT fk_notification_read_by_user_id FOREIGN KEY (read_by_user_id) REFERENCES tenant2.jhi_user(id);


--
-- TOC entry 3934 (class 2606 OID 18199)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_notification_rules_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_notification_rules_id FOREIGN KEY (notification_rules_id) REFERENCES tenant2.notification_rule(id);


--
-- TOC entry 3935 (class 2606 OID 18255)
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_target_audiences_name; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_target_audiences_name FOREIGN KEY (target_role_id) REFERENCES tenant2.role(id);


--
-- TOC entry 3927 (class 2606 OID 18184)
-- Name: official_appointment fk_official_appointment_report_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.official_appointment
    ADD CONSTRAINT fk_official_appointment_report_id FOREIGN KEY (report_id) REFERENCES tenant2.report(id);


--
-- TOC entry 3897 (class 2606 OID 18039)
-- Name: person_address fk_person_address_address_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_address
    ADD CONSTRAINT fk_person_address_address_id FOREIGN KEY (address_id) REFERENCES tenant2.address(id);


--
-- TOC entry 3898 (class 2606 OID 18044)
-- Name: person_address fk_person_address_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_address
    ADD CONSTRAINT fk_person_address_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3895 (class 2606 OID 18029)
-- Name: person_award fk_person_award_award_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_award
    ADD CONSTRAINT fk_person_award_award_id FOREIGN KEY (award_id) REFERENCES tenant2.award(id);


--
-- TOC entry 3896 (class 2606 OID 18034)
-- Name: person_award fk_person_award_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_award
    ADD CONSTRAINT fk_person_award_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3890 (class 2606 OID 18004)
-- Name: person_clothing fk_person_clothing_clothing_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_clothing
    ADD CONSTRAINT fk_person_clothing_clothing_id FOREIGN KEY (clothing_id) REFERENCES tenant2.clothing(id);


--
-- TOC entry 3891 (class 2606 OID 18009)
-- Name: person_clothing fk_person_clothing_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_clothing
    ADD CONSTRAINT fk_person_clothing_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3878 (class 2606 OID 17929)
-- Name: person_group fk_person_group_parent_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_group
    ADD CONSTRAINT fk_person_group_parent_id FOREIGN KEY (parent_id) REFERENCES tenant2.person_group(id);


--
-- TOC entry 3893 (class 2606 OID 18019)
-- Name: person_honor fk_person_honor_honor_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_honor
    ADD CONSTRAINT fk_person_honor_honor_id FOREIGN KEY (honor_id) REFERENCES tenant2.honor(id);


--
-- TOC entry 3894 (class 2606 OID 18024)
-- Name: person_honor fk_person_honor_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_honor
    ADD CONSTRAINT fk_person_honor_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3884 (class 2606 OID 17974)
-- Name: person_instrument fk_person_instrument_instrument_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_instrument
    ADD CONSTRAINT fk_person_instrument_instrument_id FOREIGN KEY (instrument_id) REFERENCES tenant2.instrument(id);


--
-- TOC entry 3885 (class 2606 OID 17979)
-- Name: person_instrument fk_person_instrument_person_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_instrument
    ADD CONSTRAINT fk_person_instrument_person_id FOREIGN KEY (person_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3876 (class 2606 OID 17919)
-- Name: person_person_groups fk_person_person_groups_people_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_people_id FOREIGN KEY (people_id) REFERENCES tenant2.person(id);


--
-- TOC entry 3877 (class 2606 OID 17924)
-- Name: person_person_groups fk_person_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES tenant2.person_group(id);


--
-- TOC entry 3906 (class 2606 OID 18084)
-- Name: receipt fk_receipt_company_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.receipt
    ADD CONSTRAINT fk_receipt_company_id FOREIGN KEY (company_id) REFERENCES tenant2.company(id);


--
-- TOC entry 3907 (class 2606 OID 18271)
-- Name: receipt fk_receipt_initial_cashbook_entry_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.receipt
    ADD CONSTRAINT fk_receipt_initial_cashbook_entry_id FOREIGN KEY (initial_cashbook_entry_id) REFERENCES tenant2.cashbook_entry(id);


--
-- TOC entry 3874 (class 2606 OID 18239)
-- Name: jhi_user_authority fk_role_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user_authority
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES tenant2.role(id);


--
-- TOC entry 3939 (class 2606 OID 18320)
-- Name: user_column_config fk_user_column_config_column_config_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.user_column_config
    ADD CONSTRAINT fk_user_column_config_column_config_id FOREIGN KEY (column_config_id) REFERENCES tenant2.column_config(id);


--
-- TOC entry 3940 (class 2606 OID 18325)
-- Name: user_column_config fk_user_column_config_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.user_column_config
    ADD CONSTRAINT fk_user_column_config_user_id FOREIGN KEY (user_id) REFERENCES tenant2.jhi_user(id);


--
-- TOC entry 3873 (class 2606 OID 17403)
-- Name: jhi_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.jhi_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES tenant2.jhi_user(id);


-- Completed on 2019-01-14 21:52:54

--
-- PostgreSQL database dump complete
--

