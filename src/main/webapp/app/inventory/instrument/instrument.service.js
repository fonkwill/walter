(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Instrument', Instrument);

    Instrument.$inject = ['$resource', 'DateUtils'];

    function Instrument ($resource, DateUtils) {
        var resourceUrl =  'api/instruments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.purchaseDate = DateUtils.convertLocalDateFromServer(data.purchaseDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.purchaseDate = DateUtils.convertLocalDateToServer(copy.purchaseDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.purchaseDate = DateUtils.convertLocalDateToServer(copy.purchaseDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('ServicesForInstrument', ServicesForInstrument);

    ServicesForInstrument.$inject = ['$resource'];

    function ServicesForInstrument ($resource) {
        var resourceUrl =  'api/instruments/:id/instrument-services';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('PersonsForInstrument', PersonsForInstrument);

    PersonsForInstrument.$inject = ['$resource'];

    function PersonsForInstrument ($resource) {
        var resourceUrl =  'api/instruments/:id/personInstruments';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('InstrumentInstrumentType', InstrumentInstrumentType);

    InstrumentInstrumentType.$inject = ['$resource'];

    function InstrumentInstrumentType ($resource) {
        var resourceUrl =  'api/instruments/instrument-types';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();

(function() {
    'use strict';
    angular
    .module('walterApp')
    .factory('InstrumentRelatedEntities', InstrumentRelatedEntities);

    InstrumentRelatedEntities.$inject = ['$resource'];

    function InstrumentRelatedEntities ($resource) {
        var me = this;

        var instrumentTypes;

        this.getInstrumentTypes = getInstrumentTypes;
        this.invalidate = invalidate;

        function getInstrumentTypes() {
            if (!instrumentTypes) {
                instrumentTypes = InstrumentTypeResource().query();
            }
            return instrumentTypes;
        }

        function invalidate() {
            instrumentTypes = null
        }

        function InstrumentTypeResource() {
            var resourceUrl =  'api/instruments/instrument-types';

            return $resource(resourceUrl, {}, {
                'query': { method: 'GET', isArray: true}
            });
        }

        return this;
    }
})();
