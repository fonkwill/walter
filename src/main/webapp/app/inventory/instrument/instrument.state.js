(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('instrument-overview', {
            parent: 'inventory',
            type: 'overview',
            abstract: true,
            views: {
                'filterbar@': {
                    templateUrl: 'app/layouts/navbar/filterbar.html',
                    controller: 'FilterbarController',
                    controllerAs: 'vm'

                }
            },
            onExit: ['InstrumentRelatedEntities', function(InstrumentRelatedEntities) {
                InstrumentRelatedEntities.invalidate();
            }]
        })
        .state('instrument', {
            parent: 'instrument-overview',
            url: '/instrument?page&sort&search',
            type: 'overview',
            listName: 'INSTRUMENT',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.instrument.home.title'
            },
            views: {
                'content-overview@': {
                    templateUrl: 'app/inventory/instrument/instruments.html',
                    controller: 'InstrumentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: {
                    value: null,
                    dynamic: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrument');
                    $translatePartialLoader.addPart('filter');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('columnConfig');
                    return $translate.refresh();
                }]
            }
        })
        .state('instrument.column-config-edit', {
            type: 'overview',
            url: '/column-config/{entity}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/column-config/column-config-dialog.html',
                    controller: 'ColumnConfigController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        columnConfig: ['ColumnConfig', function(ColumnConfig) {
                            return ColumnConfig.query({entity : $stateParams.entity}).$promise;
                        }],
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('columnConfig');
                            $translatePartialLoader.addPart('global');
                            $translatePartialLoader.addPart('instrument');
                            return $translate.refresh();
                        }]
                    }
                })
                .result.then(function() {
                    $state.go('instrument', $stateParams, { reload: 'instrument' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('instrument.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.instrument.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/instrument/instrument-edit.html',
                    controller: 'InstrumentEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Instrument', function($stateParams, Instrument) {
                        return {
                            name: null,
                            price: null,
                            purchaseDate: null,
                            privateInstrument: false,
                            instrumentServices: null,
                            personInstruments: null,
                            type: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-detail', {
            parent: 'instrument',
            url: '/instrument/{id}/detail',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.instrument.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/instrument/instrument-detail.html',
                    controller: 'InstrumentDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrument');
                    $translatePartialLoader.addPart('instrumentService');
                    $translatePartialLoader.addPart('personInstrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Instrument', function($stateParams, Instrument) {
                    return Instrument.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-detail.edit', {
            url: '/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/instrument/instrument-edit.html',
                    controller: 'CompositionEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Composition', function($stateParams, Composition) {
                    return Composition.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument-detail',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-detail.delete', {
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/inventory/instrument/instrument-delete-dialog.html',
                    controller: 'InstrumentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Instrument', function(Instrument) {
                            return Instrument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('instrument', $stateParams, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('instrument-edit', {
            parent: 'instrument',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.instrument.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/instrument/instrument-edit.html',
                    controller: 'InstrumentEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Instrument', function($stateParams, Instrument) {
                    return Instrument.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-edit.delete', {
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/inventory/instrument/instrument-delete-dialog.html',
                    controller: 'InstrumentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Instrument', function(Instrument) {
                            return Instrument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('instrument', $stateParams, { reload: 'instrument' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
