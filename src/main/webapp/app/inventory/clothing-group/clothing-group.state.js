(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('clothing-group', {
            parent: 'inventory',
            url: '/clothing-group?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.clothingGroup.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/clothing-group/clothing-groups.html',
                    controller: 'ClothingGroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingGroup');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('clothing-group.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.clothing-group.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/clothing-group/clothing-group-edit.html',
                    controller: 'ClothingGroupEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingGroup');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClothingGroup', function($stateParams, ClothingGroup) {
                        return {
                            csize: null,
                            typeId: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'clothing-group',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('clothing-group.edit', {
            url: '/{csize}/{typeId}/edit/{mode}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.clothing-group.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/inventory/clothing-group/clothing-group-edit.html',
                    controller: 'ClothingGroupEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingGroup');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClothingGroup', function($stateParams, ClothingGroup) {
                    return ClothingGroup.get({csize : $stateParams.csize, typeId : $stateParams.typeId}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'clothing-group',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('clothing-group.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/inventory/clothing-group/clothing-group-delete-dialog.html',
                    controller: 'ClothingGroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClothingGroup', function(ClothingGroup) {
                            return ClothingGroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('clothing-group', null, { reload: 'clothing-group' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
