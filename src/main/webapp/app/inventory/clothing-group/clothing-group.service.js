(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('ClothingGroup', ClothingGroup);

    ClothingGroup.$inject = ['$resource'];

    function ClothingGroup ($resource) {
        var resourceUrl =  'api/clothing-groups/:csize/:typeId';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
