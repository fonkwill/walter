(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ClothingGroupDeleteController',ClothingGroupDeleteController);

    ClothingGroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClothingGroup'];

    function ClothingGroupDeleteController($uibModalInstance, entity, ClothingGroup) {
        var vm = this;

        vm.clothingGroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClothingGroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
