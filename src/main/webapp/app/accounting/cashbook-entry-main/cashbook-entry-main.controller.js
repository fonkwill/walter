(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookEntryMainController', CashbookEntryMainController);

    CashbookEntryMainController.$inject = ['$scope', '$state', '$stateParams', '$filter', 'DateUtils', 'CashbookEntry', 'Receipt', 'CashbookRelatedEntities', 'ParseLinks', 'AlertService', 'CashbookEntryExcelExportService'];

    function CashbookEntryMainController ($scope, $state, $stateParams, $filter, DateUtils, CashbookEntry, Receipt, CashbookRelatedEntities, ParseLinks, AlertService, CashbookEntryExcelExportService) {
        var vm = this;
        vm.cashbookAccounts;
        vm.cashbookAccountName;
        vm.cashbookEntries;
        vm.summe;
        vm.summeEinnahmen;
        vm.summeAusgaben;

        vm.loadData = loadData;
        vm.transition = transition;
        vm.changeMonth = changeMonth;
        vm.downloadReceipt = downloadReceipt;
        vm.exportExcel = exportExcel;

        vm.month = $stateParams.month;
        vm.year = $stateParams.year;
        vm.cashbookAccountId = $stateParams.cashbookAccountId;
        vm.years = computeYears();

        loadData();

        function loadData() {
            CashbookRelatedEntities.getCashbookAccounts().$promise.then(function(data) {
                vm.cashbookAccounts = data;

                if (vm.month == null) {
                    vm.month = new Date().getMonth();
                }

                if (vm.year == null) {
                    vm.year = new Date().getFullYear();
                }

                if (vm.cashbookAccountId == null) {
                    chooseCurrentCashbookAccount(vm.cashbookAccounts);
                }

                vm.cashbookAccountName = $filter('filter')(vm.cashbookAccounts, function(account) {
                   if (account.id == vm.cashbookAccountId) {
                       return account.name;
                   }
                })[0].name;

                loadEntries();
            });

        }

        function loadEntries() {
            var dateFromUnformatted = new Date(vm.year, vm.month, 1);
            var dateToUnformatted = new Date(dateFromUnformatted.getFullYear(), dateFromUnformatted.getMonth() + 1, 0);
            var dateToLastMonthUnformatted = new Date(dateFromUnformatted.getFullYear(), dateFromUnformatted.getMonth(), 0);

            var dateFrom = DateUtils.convertLocalDateToServer(dateFromUnformatted);
            var dateTo = DateUtils.convertLocalDateToServer(dateToUnformatted);
            var dateToLastMonth = DateUtils.convertLocalDateToServer(dateToLastMonthUnformatted);

            CashbookEntry.query({
                sort: 'date,asc',
                accountId: vm.cashbookAccountId,
                dateFrom: dateFrom,
                dateTo: dateTo,
            }, onSuccess, onError);
            function onSuccess(data, headers) {
                vm.cashbookEntries = data;

                //Der Übertrag muss noch berechnet werden
                CashbookEntry.query({
                    sort: 'date,asc',
                    accountId: vm.cashbookAccountId,
                    dateTo: dateToLastMonth
                }, function(data) {

                    var sum = sumUpCashbookEntries(data);
                    var uebertrag = createUebertragFromSum(sum);

                    vm.cashbookEntries.push(uebertrag);

                    vm.cashbookEntries = $filter('orderBy')(vm.cashbookEntries, ['date', 'id']);

                    vm.cashbookEntriesWithoutBestand = vm.cashbookEntries;
                    vm.cashbookEntries = computeBestand(vm.cashbookEntries);

                    var einnahmen = $filter('filter')(vm.cashbookEntriesWithoutBestand, function(entry) {
                        if (entry.type == 'INCOME') {
                            return entry;
                        }
                    });

                    vm.summeEinnahmen = sumUpCashbookEntries(einnahmen);

                    var ausgaben = $filter('filter')(vm.cashbookEntriesWithoutBestand, function (entry) {
                        if (entry.type == 'SPENDING') {
                            return entry;
                        }
                    });

                    vm.summeAusgaben = Math.abs(sumUpCashbookEntries(ausgaben));

                    vm.summe = vm.summeEinnahmen - vm.summeAusgaben;

                }, onError);

                transition();
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function transition () {
            $state.transitionTo($state.$current, {
                month: vm.month,
                year: vm.year,
                cashbookAccountId: vm.cashbookAccountId
            });
        }

        function sumUpCashbookEntries (data) {
            var sum = 0;

            angular.forEach(data, function(value, key) {
                if (value.type == 'SPENDING') {
                    sum -= value.amount;
                } else if (value.type == 'INCOME') {
                    sum += value.amount;
                }
            });

            return sum;
        }

        function createUebertragFromSum(sum) {
            var uebertrag = {};
            uebertrag.title = "Übertrag aus Vormonat";

            if (sum < 0) {
                uebertrag.type = 'SPENDING';
            } else {
                uebertrag.type = 'INCOME';
            }

            uebertrag.amount = Math.abs(sum);

            var dateUnformatted = new Date(vm.year, vm.month, 1);
            var date = DateUtils.convertLocalDateToServer(dateUnformatted);

            uebertrag.date = date;
            uebertrag.id = -1;

            return uebertrag;

        }

        function chooseCurrentCashbookAccount(cashbookAccounts) {
            var defaultCashbookAccount = $filter('filter')(cashbookAccounts, function(account) {
                if (account.defaultForCashbook) {
                    return account;
                }
            });

            if (defaultCashbookAccount[0]) {
                vm.cashbookAccountId = defaultCashbookAccount[0].id;
            } else {
                vm.cashbookAccountId = cashbookAccounts[0].id;
            }

        }

        function computeYears() {
            var currentYear = new Date().getFullYear();
            var years = {};

            years[0] = currentYear;

            for (var i = 1; i < 5; i++) {
                years[i] = currentYear - i;
            }

            return years;
        }

        function changeMonth(month) {
            vm.month = month;
            transition();
        }

        function downloadReceipt(receiptFile) {
            Receipt.download({
                uuid: receiptFile
            }, onSuccess, onError);

            function onSuccess(data, headers) {
                var blob = new Blob([data.data], { type: headers('content-type') });
                saveAs(blob, data.filename);
            };

            function onError(error) {
                AlertService.error(error.data.message);
            };
        }

        function computeBestand(cashbookEntries) {
            var returnValues = {};

            for (var i = 0; i < cashbookEntries.length; i++) {
                returnValues[i] = cashbookEntries[i];
                if (i == 0) {
                    returnValues[i].holding = getBetragForBestandsBerechnung(cashbookEntries[i]);
                } else {
                    returnValues[i].holding = cashbookEntries[i-1].holding + getBetragForBestandsBerechnung(cashbookEntries[i]);
                }
            }

            return returnValues;
        }

        function getBetragForBestandsBerechnung(cashbookEntry) {
            if (cashbookEntry.type == 'SPENDING') {
                return cashbookEntry.amount * -1;
            } else {
                return cashbookEntry.amount;
            }
        }

        function exportExcel() {
            var realMonth = parseInt(vm.month) + 1;
            var filename = 'Export_' + vm.cashbookAccountName + '_' + realMonth + ".xlsx";
            CashbookEntryExcelExportService.exportForCashbookEntries(
                filename,
                vm.cashbookAccountName,
                vm.summeAusgaben,
                vm.summeEinnahmen,
                vm.summe,
                vm.year,
                vm.month,
                vm.cashbookEntries);
        }
    }
})();
