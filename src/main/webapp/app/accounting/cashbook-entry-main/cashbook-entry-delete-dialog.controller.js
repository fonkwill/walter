(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookEntryDeleteController',CashbookEntryDeleteController);

    CashbookEntryDeleteController.$inject = ['$uibModalInstance', 'entity', 'CashbookEntry'];

    function CashbookEntryDeleteController($uibModalInstance, entity, CashbookEntry) {
        var vm = this;

        vm.cashbookEntry = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CashbookEntry.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
