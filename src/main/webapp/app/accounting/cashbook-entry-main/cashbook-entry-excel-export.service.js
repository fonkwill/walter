(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookEntryExcelExportService', CashbookEntryExcelExportService);

    function CashbookEntryExcelExportService () {
        const dateFormat = "dd.mm.yyyy";
        const numFormatEuro = "#,##0.00 €";
        const months = [
            "Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"
        ];

        this.exportForCashbookEntries = exportForCashbookEntries;

        function exportForCashbookEntries(filename,
                                          cashbookAccountName,
                                          summeAusgaben,
                                          summeEinnahmen,
                                          summe,
                                          jahr,
                                          monat,
                                          cashbookEntries) {
            XlsxPopulate.fromBlankAsync().then(function (workbook) {
                printHeaders(workbook, cashbookAccountName, summe, summeEinnahmen, summeAusgaben, jahr, monat);
                printRows(workbook, cashbookEntries);
                return workbook.outputAsync();
            }).then(function (blob) {
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(blob, filename);
                } else {
                    var url = window.URL.createObjectURL(blob);
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.href = url;
                    a.download = filename;
                    a.click();
                    window.URL.revokeObjectURL(url);
                    document.body.removeChild(a);
                }
            })
            .catch(function (err) {
                alert(err.message || err);
                throw err;
            });
        }

        function printHeaders(workbook, cashbookAccountName, summe, summeEinnahmen, summeAusgaben, jahr, monat) {
            var sheet = workbook.sheet(0).name(months[monat]);
            writeCell(sheet, "B2", cashbookAccountName).style("fontSize", 14).style("bold", true);
            writeCell(sheet, "H2", "Stand:").style("horizontalAlignment", "right").style("bold", true);
            writeCell(sheet, "B4", "Monat: " + months[monat] + " " + jahr).style("bold", true);
            writeDateCell(sheet, "I2", new Date()).style("bold", true);
            writeCell(sheet, "H4", "Aktueller Kassabestand:").style("horizontalAlignment", "right").style("bold", true);
            writeBetragCell(sheet, "I4", summe).style("bold", true);
            writeCell(sheet, "F8", "Summen:").style("horizontalAlignment", "right").style("bold", true);
            writeBetragCell(sheet, "G8", summeEinnahmen).style("bold", true);
            writeBetragCell(sheet, "H8", summeAusgaben).style("bold", true);
            writeBetragCell(sheet, "I8", summe).style("bold", true);
            writeCell(sheet, "B9", "Nr.").style("bold", true);
            writeCell(sheet, "C9", "Datum").style("bold", true);
            writeCell(sheet, "D9", "Titel").style("bold", true);
            writeCell(sheet, "E9", "Kategorie").style("bold", true);
            writeCell(sheet, "F9", "Beleg").style("bold", true);
            writeCell(sheet, "G9", "Einnahme").style("bold", true);
            writeCell(sheet, "H9", "Ausgabe").style("bold", true);
            writeCell(sheet, "I9", "Bestand").style("bold", true);

            //Spaltenbreiten
            sheet.column("A").width(1.5);
            sheet.column("B").width(4);
            sheet.column("C").width(10);
            sheet.column("D").width(70);
            sheet.column("E").width(25);
            sheet.column("F").width(10);
            sheet.column("G").width(12);
            sheet.column("H").width(12);
            sheet.column("I").width(12);

            //Zeilenhöhen
            sheet.row("1").height(7);
            sheet.row("7").height(7);

            //Einfärbungen
            sheet.range("B2:I2").style("fill", "DDEBF7");
            sheet.range("B4:I6").style("fill", "FCE4D6");
            sheet.range("B8:I8").style("fill", "FCE4D6");
            sheet.range("B9:I9").style("fill", "FFE699");

            //Rahmen
            sheet.range("B2:I2").style("topBorder", "thin");
            sheet.range("B2:I2").style("bottomBorder", "thin");
            sheet.cell("B2").style("leftBorder", "thin");
            sheet.cell("I2").style("rightBorder", "thin");
            sheet.range("B4:I4").style("topBorder", "thin");
            sheet.range("B6:I6").style("bottomBorder", "thin");
            sheet.range("B4:B6").style("leftBorder", "thin");
            sheet.range("I4:I6").style("rightBorder", "thin");
            sheet.range("B8:I8").style("topBorder", "thin").style("bottomBorder", "thin");
            sheet.cell("B8").style("leftBorder", "thin");
            sheet.cell("I8").style("rightBorder", "thin");
            sheet.cell("B9").style("leftBorder", "thin");
            sheet.cell("I9").style("rightBorder", "thin");
            sheet.range("B9:I9").style("bottomBorder", "dotted");
            sheet.range("B9:H9").style("rightBorder", "dotted");
        }

        function printRows(workbook, cashbookEntries) {
            var sheet = workbook.sheet(0);
            var start = 10;
            var counter = 0;

            angular.forEach(cashbookEntries, function(value, key) {
               var currentRow = start + counter;
               sheet.cell("B" + currentRow).style("fill", "FFE699");

               if (counter != 0) {
                   writeCell(sheet, "B" + currentRow, counter);
                   sheet.range("B" + currentRow + ":I" + currentRow).style("bottomBorder", "dotted");
               } else {
                   sheet.range("B" + currentRow + ":I" + currentRow).style("fill", "FFE699");
                   sheet.range("B" + currentRow + ":I" + currentRow).style("bottomBorder", "thin");
               }

               writeDateCell(sheet, "C" + currentRow, new Date(value.date));
               writeCell(sheet, "D" + currentRow, value.title);
               writeCell(sheet, "E" + currentRow, value.cashbookCategoryName);
               if (value.cashbookCategoryColor !== undefined && value.cashbookCategoryColor != null) {
                   sheet.cell("E" + currentRow).style("fill", value.cashbookCategoryColor.replace('#', ''));
               }
               writeCell(sheet, "F" + currentRow, value.receiptIdentifier);
               if (value.type == 'INCOME') {
                   writeBetragCell(sheet, "G" + currentRow, value.amount);
               } else if (value.type == 'SPENDING') {
                   writeBetragCell(sheet, "H" + currentRow, value.amount);
               }
               writeBetragCell(sheet, "I" + currentRow, value.holding);
               sheet.cell("I" + currentRow).style("fill", "FFE699");
               sheet.cell("B" + currentRow).style("leftBorder", "thin");
               sheet.cell("I" + currentRow).style("rightBorder", "thin");
               sheet.range("B" + currentRow + ":H" + currentRow).style("rightBorder", "dotted");
               counter++;
            });

            var lastRow = start + counter - 1;

            sheet.range("B" + lastRow + ":I" + lastRow).style("bottomBorder", "thin");
        }

        function writeCell(sheet, cell, value) {
            return sheet.cell(cell).value(value);
        }

        function writeDateCell(sheet, cell, value) {
            return writeCell(sheet,cell,value).style("numberFormat", dateFormat);
        }

        function writeBetragCell(sheet, cell, value) {
            return writeCell(sheet,cell,value).style("numberFormat", numFormatEuro)
        }

        return this;
    }
})();
