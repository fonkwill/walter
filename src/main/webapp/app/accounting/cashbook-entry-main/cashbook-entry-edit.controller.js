(function() {
	'use strict';

	angular
		.module('walterApp')
		.controller('CashbookEntryEditController', CashbookEntryEditController);

	CashbookEntryEditController.$inject = ['$timeout', '$state', '$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CashbookEntry', 'Composition', 'InstrumentService', 'MembershipFeeForPeriod', 'Appointment', 'CashbookCategory', 'Receipt', 'CashbookAccount', '$filter', '$uibModal'];

	function CashbookEntryEditController($timeout, $state, $scope, $rootScope, $stateParams, previousState, entity, CashbookEntry, Composition, InstrumentService, MembershipFeeForPeriod, Appointment, CashbookCategory, Receipt, CashbookAccount, $filter, $uibModal) {
		var vm = this;

        vm.previousState = previousState;
        vm.year = vm.previousState.params.year;
        vm.month = vm.previousState.params.month;
        vm.cashbookEntry = entity;
        if (vm.cashbookEntry.cashbookAccountId == null) {
            vm.cashbookEntry.cashbookAccountId = vm.previousState.params.cashbookAccountId;
        }

        vm.type = $filter('translate')('walterApp.CashbookEntryType.' + vm.cashbookEntry.type);

        vm.cashbookcategories = CashbookCategory.query();
        vm.receipts = Receipt.query();

        CashbookAccount.get({
            id: vm.previousState.params.cashbookAccountId
        }, function(data) {
            vm.cashbookAccountName = data.name;
        })

        var dateFrom = new Date(vm.year, vm.month, 1);
        var dateTo = new Date(dateFrom.getFullYear(), dateFrom.getMonth() + 1, 0);
        vm.datepickerOptions = {
            minDate: dateFrom,
            maxDate: dateTo
        };

		vm.clear = clear;
		vm.datePickerOpenStatus = {};
		vm.openCalendar = openCalendar;
		vm.save = save;
		vm.goBack = goBack;

		vm.isCreatedByForeignRelation = isCreatedByForeignRelation;
		function isCreatedByForeignRelation() {
			var compositions = vm.cashbookEntry.compositions && vm.cashbookEntry.compositions.length === 0;
			var instrumentService = vm.cashbookEntry.instrumentServiceId === null;
			var instrument = vm.cashbookEntry.instrumentId === null;
			var membershipFeeForPeriod = vm.cashbookEntry.membershipFeeForPeriodId === null;

			return vm.cashbookEntry.id !== null && !(compositions && instrumentService && instrument && membershipFeeForPeriod);
		}

		$timeout(function () {
			angular.element('.form-group:eq(1)>input').focus();
		});

		function clear () {
			$uibModalInstance.dismiss('cancel');
		}

		function save () {
			vm.isSaving = true;
			if (vm.cashbookEntry.id !== null) {
				CashbookEntry.update(vm.cashbookEntry, onSaveSuccess, onSaveError);
			} else {
				CashbookEntry.save(vm.cashbookEntry, onSaveSuccess, onSaveError);
			}
		}

		function onSaveSuccess (result) {
			$scope.$emit('walterApp:cashbookEntryUpdate', result);
			vm.isSaving = false;
            $state.go('cashbook-entry-main', previousState.params, {reload : true});
		}

		function onSaveError () {
			vm.isSaving = false;
		}

        function goBack() {
            $state.go(previousState.name,  previousState.params);
        }

		vm.datePickerOpenStatus.date = false;

		function openCalendar (date) {
			vm.datePickerOpenStatus[date] = true;
		}

        vm.clearAppointmentSelection = clearAppointmentSelection;
        function clearAppointmentSelection() {
            vm.appointment = null;
            vm.cashbookEntry.appointmentId = null;
        }

        vm.openChooseAppointmentDialog = openChooseAppointmentDialog;
        function openChooseAppointmentDialog() {
            $uibModal.open({
                templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-choose-appointment.html',
                controller: 'CashbookEntryChooseAppointmentController',
                controllerAs: 'vm',
                size: 'md',
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            page: PaginationUtil.parsePage($stateParams.page),
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort),
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('appointment');
                        $translatePartialLoader.addPart('cashbookEntry');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            }).result.then(function(appointment) {
                vm.cashbookEntry.appointmentId = appointment.id;
                vm.appointment = appointment;
            });
        }

    }
})();
