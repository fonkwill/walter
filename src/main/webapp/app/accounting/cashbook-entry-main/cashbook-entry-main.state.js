(function() {
        'use strict';

        angular
            .module('walterApp')
            .config(stateConfig);

        stateConfig.$inject = ['$stateProvider'];

        function stateConfig($stateProvider) {
            $stateProvider
                .state('cashbook-entry-main', {
                    parent: 'accounting',
                    url: '/cashbook-entry-main?cashbookAccountId&month&year',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'walterApp.cashbookEntryMain.home.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-main.html',
                            controller: 'CashbookEntryMainController',
                            controllerAs: 'vm'
                        }
                    },
                    params: {
                        page: {
                            value: '1',
                            squash: true
                        },
                        sort: {
                            value: 'date,desc',
                            squash: true
                        },
                        cashbookAccountId: {
                            value: null,
                            squash: true
                        },
                        month: {
                            value: null,
                            squash: true
                        },
                        year: {
                            value: null,
                            squash: true
                        }
                    },
                    resolve: {
                        pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                            return {
                                page: PaginationUtil.parsePage($stateParams.page),
                                sort: $stateParams.sort,
                                predicate: PaginationUtil.parsePredicate($stateParams.sort),
                                ascending: PaginationUtil.parseAscending($stateParams.sort),
                                cashbookAccountId: $stateParams.cashbookAccountId,
                                month: $stateParams.month,
                                year: $stateParams.year
                            };
                        }],
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntryMain');
                            $translatePartialLoader.addPart('cashbookEntryType');
                            $translatePartialLoader.addPart('global');
                            return $translate.refresh();
                        }]
                    }
                })
                .state('cashbook-entry-main.newIncome', {
                    url: '/new',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'walterApp.cashbookEntry.detail.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-edit.html',
                            controller: 'CashbookEntryEditController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntry');
                            $translatePartialLoader.addPart('cashbookEntryType');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'CashbookEntry', function($stateParams, CashbookEntry) {
                            return {
                                title: null,
                                type: 'INCOME',
                                date: new Date($stateParams.year, $stateParams.month, 1),
                                amount: null,
                                id: null
                            };
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'cashbook-entry',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                })
                .state('cashbook-entry-main.newSpending', {
                    url: '/new',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'walterApp.cashbookEntry.detail.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-edit.html',
                            controller: 'CashbookEntryEditController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntry');
                            $translatePartialLoader.addPart('cashbookEntryType');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'CashbookEntry', function($stateParams, CashbookEntry) {
                            return {
                                title: null,
                                type: 'SPENDING',
                                date: new Date($stateParams.year, $stateParams.month, 1),
                                amount: null,
                                id: null
                            };
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'cashbook-entry',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                })
                .state('cashbook-entry-main-detail', {
                    parent: 'cashbook-entry-main',
                    url: '/{id}/detail',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'walterApp.cashbookEntry.detail.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-detail.html',
                            controller: 'CashbookEntryDetailController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntry');
                            $translatePartialLoader.addPart('cashbookEntryType');
                            $translatePartialLoader.addPart('receipt');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'CashbookEntry', function($stateParams, CashbookEntry) {
                            return CashbookEntry.get({ id : $stateParams.id }).$promise;
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'cashbook-entry-main',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                })
                .state('cashbook-entry-main-edit', {
                    parent: 'cashbook-entry-main',
                    url: '/{id}/edit',
                    data: {
                        authorities: ['ROLE_USER'],
                        pageTitle: 'walterApp.cashbookEntry.detail.title'
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-edit.html',
                            controller: 'CashbookEntryEditController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntry');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'CashbookEntry', function($stateParams, CashbookEntry) {
                            return CashbookEntry.get({id : $stateParams.id}).$promise;
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'cashbook-entry-main',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                })
                .state('cashbook-entry-main-detail.edit', {
                    url: '/edit',
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    views: {
                        'content@': {
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-edit.html',
                            controller: 'CashbookEntryEditController',
                            controllerAs: 'vm'
                        }
                    },
                    resolve: {
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('cashbookEntry');
                            return $translate.refresh();
                        }],
                        entity: ['$stateParams', 'CashbookEntry', function($stateParams, CashbookEntry) {
                            return CashbookEntry.get({id : $stateParams.id}).$promise;
                        }],
                        previousState: ["$state", function ($state) {
                            var currentStateData = {
                                name: $state.current.name || 'cashbook-entry-main-detail',
                                params: $state.params,
                                url: $state.href($state.current.name, $state.params)
                            };
                            return currentStateData;
                        }]
                    }
                })
                .state('cashbook-entry-main-detail.delete', {
                    url: '/delete',
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-delete-dialog.html',
                            controller: 'CashbookEntryDeleteController',
                            controllerAs: 'vm',
                            size: 'md',
                            resolve: {
                                entity: ['CashbookEntry', function(CashbookEntry) {
                                    return CashbookEntry.get({id : $stateParams.id}).$promise;
                                }]
                            }
                        }).result.then(function() {
                            $state.go('cashbook-entry-main', $stateParams, { reload: true });
                        }, function() {
                            $state.go('^');
                        });
                    }]
                })
                .state('cashbook-entry-main-edit.delete', {
                    url: '/delete',
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'app/accounting/cashbook-entry-main/cashbook-entry-delete-dialog.html',
                            controller: 'CashbookEntryDeleteController',
                            controllerAs: 'vm',
                            size: 'md',
                            resolve: {
                                entity: ['CashbookEntry', function(CashbookEntry) {
                                    return CashbookEntry.get({id : $stateParams.id}).$promise;
                                }]
                            }
                        }).result.then(function() {
                            $state.go('cashbook-entry-main', $stateParams, { reload: 'cashbook-entry-main' });
                        }, function() {
                            $state.go('^');
                        });
                    }]
                })
                .state('cashbook-entry-main-detail.choose-receipt', {
                    url: '/choose?page&sort&search',
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    params: {
                        page: {
                            value: '1',
                            squash: true
                        },
                        sort: {
                            value: 'date,desc',
                            squash: true
                        },
                        search: null
                    },
                    onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'app/accounting/receipt/receipt-choose-dialog.html',
                            controller: 'ReceiptChooseController',
                            controllerAs: 'vm',
                            size: 'md',
                            resolve: {
                                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                                    return {
                                        page: PaginationUtil.parsePage($stateParams.page),
                                        sort: $stateParams.sort,
                                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                                        cashbookAccountId: $stateParams.cashbookAccountId,
                                        month: $stateParams.month,
                                        year: $stateParams.year
                                    };
                                }],
                                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                                    $translatePartialLoader.addPart('receipt');
                                    $translatePartialLoader.addPart('global');
                                    return $translate.refresh();
                                }]
                            }
                        }).result.then(function() {
                            $state.go('cashbook-entry-main-detail', { id: $stateParams.id }, { reload: true });
                        }, function() {
                            $state.go('^');
                        });
                    }]
                })
                .state('cashbook-entry-main-detail.delete-receipt', {
                    url: '/delete-receipt/{receiptId}',
                    params: {
                        'receiptId': null
                    },
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'app/accounting/receipt/receipt-delete-dialog.html',
                            controller: 'ReceiptDeleteController',
                            controllerAs: 'vm',
                            size: 'md',
                            resolve: {
                                entity: ['Receipt', function(Receipt) {
                                    return Receipt.get({ id : $stateParams.receiptId }).$promise;
                                }]
                            }
                        }).result.then(function() {
                            $state.go('cashbook-entry-main-detail', { id: $stateParams.id }, { reload: true });
                        }, function() {
                            $state.go('^');
                        });
                    }]
                })
                .state('cashbook-entry-main-detail.unlink-receipt', {
                    url: '/unlink-receipt/{receiptId}',
                    params: {
                        'receiptId': null
                    },
                    data: {
                        authorities: ['ROLE_USER']
                    },
                    onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                        $uibModal.open({
                            templateUrl: 'app/accounting/receipt/receipt-unlink-dialog.html',
                            controller: 'ReceiptUnlinkController',
                            controllerAs: 'vm',
                            size: 'md',
                            resolve: {
                                entity: ['Receipt', function(Receipt) {
                                    return Receipt.get({ id : $stateParams.receiptId }).$promise;
                                }],
                                cashbookEntryId: function() { return $stateParams.id; }
                            }
                        }).result.then(function() {
                            $state.go('cashbook-entry-main-detail', { id: $stateParams.id }, { reload: true });
                        }, function() {
                            $state.go('^');
                        });
                    }]
                });
        }

})();
