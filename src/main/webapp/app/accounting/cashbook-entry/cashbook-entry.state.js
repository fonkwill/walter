(function() {
	'use strict';

	angular
		.module('walterApp')
		.config(stateConfig);

	stateConfig.$inject = ['$stateProvider'];

	function stateConfig($stateProvider) {
		$stateProvider
        .state('cashbook-entry-overview', {
            parent: 'accounting',
            abstract: true,
            views: {
                'filterbar@': {
                    templateUrl: 'app/layouts/navbar/filterbar.html',
                    controller: 'FilterbarController',
                    controllerAs: 'vm'

                }
            },
            onExit: ['CashbookRelatedEntities', function(CashbookRelatedEntities) {
                CashbookRelatedEntities.invalidate();
            }]
        })
		.state('cashbook-entry', {
			parent: 'cashbook-entry-overview',
			url: '/cashbook-entry?page&sort&search',
            listName: 'CASHBOOK-ENTRY',
            type: 'overview',
			data: {
				authorities: ['ROLE_USER'],
				pageTitle: 'walterApp.cashbookEntry.home.title'
			},
			views: {
				'content-overview@': {
					templateUrl: 'app/accounting/cashbook-entry/cashbook-entries.html',
					controller: 'CashbookEntryController',
					controllerAs: 'vm'
				}
			},
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'date,desc',
                    squash: true
                },
                search: {
                    value: null,
                    dynamic: true
                }
            },
			resolve: {
				pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
					return {
						page: PaginationUtil.parsePage($stateParams.page),
						sort: $stateParams.sort,
						predicate: PaginationUtil.parsePredicate($stateParams.sort),
						ascending: PaginationUtil.parseAscending($stateParams.sort),
						search: $stateParams.search
					};
				}],
				translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
					$translatePartialLoader.addPart('cashbookEntry');
					$translatePartialLoader.addPart('cashbookEntryType');
                    $translatePartialLoader.addPart('filter');
					$translatePartialLoader.addPart('global');
					$translatePartialLoader.addPart('columnConfig');
					return $translate.refresh();
				}]
			}
		})
        .state('cashbook-entry.column-config-edit', {
            type: 'overview',
            url: '/column-config/{entity}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/column-config/column-config-dialog.html',
                    controller: 'ColumnConfigController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        columnConfig: ['ColumnConfig', function(ColumnConfig) {
                            return ColumnConfig.query({entity : $stateParams.entity}).$promise;
                        }],
                        translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                            $translatePartialLoader.addPart('columnConfig');
                            $translatePartialLoader.addPart('global');
                            $translatePartialLoader.addPart('cashbookEntry');
                            return $translate.refresh();
                        }]
                    }
                })
                .result.then(function() {
                    $state.go('cashbook-entry', $stateParams, { reload: 'cashbook-entry' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
	}

})();
