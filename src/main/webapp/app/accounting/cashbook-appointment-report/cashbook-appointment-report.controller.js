(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookAppointmentReportController', CashbookAppointmentReportController);

    CashbookAppointmentReportController.$inject = ['$scope', '$state', 'CashbookAppointmentReport', 'CashbookAppointmentReportNeededEntities', 'DateUtils'];

    function CashbookAppointmentReportController ($scope, $state, CashbookAppointmentReport, CashbookAppointmentReportNeededEntities, DateUtils) {
        var vm = this;
        vm.getPdf = getPdf;

        var currentYear = (new Date()).getFullYear();
        vm.dateFrom = new Date(currentYear, 0, 1);
        vm.dateTo = new Date(currentYear + 1, 0, 0);
        vm.appointmentTypeId;
        vm.datesValid = datesValid;

        vm.appointmentTypes = CashbookAppointmentReportNeededEntities.appointmentTypes();
        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.dateTo = false;
        vm.datePickerOpenStatus.dateFrom = false;
        vm.openCalendar = openCalendar;

        function getPdf() {
            var dateFrom = vm.dateFrom ? DateUtils.convertLocalDateToServer(vm.dateFrom) : null;
            var dateTo = vm.dateTo ? DateUtils.convertLocalDateToServer(vm.dateTo) : null;

            CashbookAppointmentReport.getPdf(
                {
                    dateFrom: dateFrom,
                    dateTo: dateTo,
                    appointmentTypeId: vm.appointmentTypeId
                }
                );
        }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }

        function datesValid() {
            return vm.dateTo > vm.dateFrom;
        }

    }
})();
