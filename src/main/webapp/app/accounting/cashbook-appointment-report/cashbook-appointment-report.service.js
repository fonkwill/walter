(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookAppointmentReport', CashbookAppointmentReport);

    CashbookAppointmentReport.$inject = ['$resource', 'DateUtils', 'FileSaver'];

    function CashbookAppointmentReport ($resource, DateUtils, FileSaver) {
        var resourceUrl =  'api/cashbook-appointment-report/pdf';

        return $resource(resourceUrl, {}, {
            'getPdf': {
                responseType: 'arraybuffer',
                method: 'GET',
                isArray: true,
                transformResponse: function (data, headers, status) {
                    if (status === 200) {
                        if (data) {
                            var filename = "file.json";
                            if (headers) {
                                //get filename from header (partly from http://stackoverflow.com/questions/33046930/how-to-get-the-name-of-a-file-downloaded-with-angular-http
                                var contentDisposition = headers('Content-Disposition');
                                filename = contentDisposition.split(';')[1].trim().split('=')[1];
                                filename = filename.replace(/"/g, '');
                            }
                            var blob = new Blob([data]),
                                url = window.URL.createObjectURL(blob);
                            FileSaver.saveAs(blob, filename);
                            return angular.toString(url);
                        }
                    } else {
                        //because we have an arraybuffer we have to extract the needed data
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));

                        var obj = JSON.parse(decodedString);
                        var message = obj.message;
                        return message;
                    }
                }
            }

        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookAppointmentReportNeededEntities', CashbookAppointmentReportNeededEntities);

    CashbookAppointmentReportNeededEntities.$inject = ['NeededEntitiesCache'];

    function CashbookAppointmentReportNeededEntities (NeededEntitiesCache) {

        this.appointmentTypes = appointmentTypes;

        function appointmentTypes() {
            return NeededEntitiesCache.getCache('api/appointment-types/').query();
        }

        return this;

    }
})();

