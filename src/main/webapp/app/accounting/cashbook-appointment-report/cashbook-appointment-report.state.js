(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('cashbook-appointment-report', {
                parent: 'accounting',
                url: '/cashbook-appointment-report',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'walterApp.cashbookAppointmentReport.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/accounting/cashbook-appointment-report/cashbook-appointment-report.html',
                        controller: 'CashbookAppointmentReportController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cashbookAppointmentReport');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
    }

})();
