(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('cashbook-category', {
            parent: 'accounting',
            url: '/cashbook-category?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.cashbookCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/accounting/cashbook-category/cashbook-categories.html',
                    controller: 'CashbookCategoryController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cashbookCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('cashbook-category.new', {

            url: '/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.cashbookCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/accounting/cashbook-category/cashbook-category-detail.html',
                    controller: 'CashbookCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cashbookCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CashbookCategory', function($stateParams, CashbookCategory) {
                        return {
                            name: null,
                            description: null,
                            color: '#ffffff',
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'cashbook-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('cashbook-category.edit', {

            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.cashbookCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/accounting/cashbook-category/cashbook-category-detail.html',
                    controller: 'CashbookCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cashbookCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CashbookCategory', function($stateParams, CashbookCategory) {
                    return CashbookCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'cashbook-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('cashbook-category.delete', {
            //parent: 'cashbook-category.edit',
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/accounting/cashbook-category/cashbook-category-delete-dialog.html',
                    controller: 'CashbookCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CashbookCategory', function(CashbookCategory) {
                            return CashbookCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('cashbook-category', null, { reload: 'cashbook-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
