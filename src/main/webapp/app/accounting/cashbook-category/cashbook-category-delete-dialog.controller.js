(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookCategoryDeleteController',CashbookCategoryDeleteController);

    CashbookCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'CashbookCategory'];

    function CashbookCategoryDeleteController($uibModalInstance, entity, CashbookCategory) {
        var vm = this;

        vm.cashbookCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CashbookCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
