(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookOverallReport', CashbookOverallReport);

    CashbookOverallReport.$inject = ['$resource', 'DateUtils', 'FileSaver'];

    function CashbookOverallReport ($resource, DateUtils, FileSaver) {
        var resourceUrl =  'api/cashbook-overall-report/pdf';

        return $resource(resourceUrl, {}, {
            'getPdf': {
                responseType: 'arraybuffer',
                method: 'GET',
                isArray: true,
                transformResponse: function (data, headers, status) {
                    if (status === 200) {

                        if (data) {
                            var filename = "file.json";
                            if (headers) {
                                //get filename from header (partly from http://stackoverflow.com/questions/33046930/how-to-get-the-name-of-a-file-downloaded-with-angular-http
                                var contentDisposition = headers('Content-Disposition');
                                filename = contentDisposition.split(';')[1].trim().split('=')[1];
                                filename = filename.replace(/"/g, '');
                            }
                            var blob = new Blob([data]),
                                url = window.URL.createObjectURL(blob);
                            FileSaver.saveAs(blob, filename);
                            return angular.toString(url);
                        }
                    } else {
                        //because we have an arraybuffer we have to extract the needed data
                        var decodedString = String.fromCharCode.apply(null, new Uint8Array(data));

                        var obj = JSON.parse(decodedString);
                        var message = obj.message;
                        return message;
                    }
                }
            }

        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookOverallReportNeededEntities', CashbookOverallReportNeededEntities);

    CashbookOverallReportNeededEntities.$inject = ['NeededEntitiesCache'];

    function CashbookOverallReportNeededEntities (NeededEntitiesCache) {

        this.cashbookAccounts = cashbookAccounts;
        this.cashbookCategories = cashbookCategories;

        function cashbookAccounts() {
            return NeededEntitiesCache.getCache('api/cashbook-accounts/').query();
        }

        function cashbookCategories() {
            return NeededEntitiesCache.getCache('api/cashbook-categories/').query();
        }

        return this;

    }
})();

