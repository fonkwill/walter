(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookOverallReportController', CashbookOverallReportController);

    CashbookOverallReportController.$inject = ['$scope', '$state', 'CashbookOverallReport', 'CashbookOverallReportNeededEntities'];

    function CashbookOverallReportController ($scope, $state, CashbookOverallReport, CashbookOverallReportNeededEntities) {
        var vm = this;
        vm.getPdf = getPdf;

        vm.cashbookAccounts = CashbookOverallReportNeededEntities.cashbookAccounts();
        vm.cashbookCategories = CashbookOverallReportNeededEntities.cashbookCategories();
        vm.years = computeYears();
        vm.cashbookAccountIds;
        vm.cashbookCategoryIds;

        function getPdf() {
            CashbookOverallReport.getPdf(
                {
                    year : vm.year,
                    cashbookAccountIds : vm.cashbookAccountIds,
                    cashbookCategoryIds : vm.cashbookCategoryIds
                }
                );
        }

        function computeYears() {
            var currentYear = new Date().getFullYear();
            var years = {};

            years[0] = currentYear;

            for (var i = 1; i < 5; i++) {
                years[i] = currentYear - i;
            }

            return years;
        }
    }
})();
