(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('cashbook-overall-report', {
                parent: 'accounting',
                url: '/cashbook-overall-report',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'walterApp.cashbookOverallReport.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/accounting/cashbook-overall-report/cashbook-overall-report.html',
                        controller: 'CashbookOverallReportController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('cashbookOverallReport');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            });
    }

})();
