(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('CashbookAccount', CashbookAccount);

    CashbookAccount.$inject = ['$resource'];

    function CashbookAccount ($resource) {
        var resourceUrl =  'api/cashbook-accounts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
