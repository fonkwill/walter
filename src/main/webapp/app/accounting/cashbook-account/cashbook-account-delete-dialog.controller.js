(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookAccountDeleteController',CashbookAccountDeleteController);

    CashbookAccountDeleteController.$inject = ['$uibModalInstance', 'entity', 'CashbookAccount'];

    function CashbookAccountDeleteController($uibModalInstance, entity, CashbookAccount) {
        var vm = this;

        vm.cashbookAccount = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CashbookAccount.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
