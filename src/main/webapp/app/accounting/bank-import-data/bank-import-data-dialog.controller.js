(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ImportDialogController',ImportDialogController);

    ImportDialogController.$inject = ['$uibModalInstance', '$rootScope', '$stateParams', 'BankImportData', 'selectedBankImportData'];

    function ImportDialogController($uibModalInstance, $rootScope, $stateParams, BankImportData, selectedBankImportData) {
        var vm = this;

        vm.clear = clear;
        vm.confirm = confirm;
        vm.createReceipts = true

        function clear () {
            $uibModalInstance.dismiss();
        }

        function confirm () {
        	BankImportData.save({createReceipts: vm.createReceipts}, selectedBankImportData);
            $rootScope.$emit('walterApp:cashbookEntryUpdate', null);
            $uibModalInstance.close();
        }
    }
})();
