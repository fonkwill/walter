(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookAccountTransferController',CashbookAccountTransferController);

    CashbookAccountTransferController.$inject = ['$state', '$scope', '$rootScope', '$stateParams', 'CashbookAccountTransfer', 'CashbookAccountTransferRelatedEntities'];

    function CashbookAccountTransferController($state, $scope, $rootScope, $stateParams, CashbookAccountTransfer, CashbookAccountTransferRelatedEntities) {
        var vm = this;

        vm.save = save;
        vm.cashbookAccountFromId;
        vm.cashbookAccountToId;
        vm.amount = 0.0;
        vm.cashbookAccounts = CashbookAccountTransferRelatedEntities.getCashbookAccounts();

        function save () {
            vm.isSaving = true;
            CashbookAccountTransfer.save({cashbookAccountFromId: vm.cashbookAccountFromId, cashbookAccountToId: vm.cashbookAccountToId, amount: vm.amount},
                onSaveSuccess, onSaveError);
        }

        function onSaveSuccess (result) {
            $scope.$emit('walterApp:cashbookAccountTransferUpdate', result);
            vm.isSaving = false;
            $state.go('cashbook-entry-main', { cashbookAccountId: vm.cashbookAccountToId }, {reload : true});
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
