(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider

        .state('letter', {
            parent: 'app',
            url: '/letter',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.letter.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/letters/serial-letters/letter-edit.html',
                    controller: 'LetterEditController',
                    controllerAs: 'vm'
                }
            },
            params: {
                entity: {
                    value: null
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('letter');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Letter', function($stateParams, Letter) {
                    return {
                        title: null,
                        text: null,
                        personGroup: null,
                        letterType: "SERIAL_LETTER"
                    };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'letter',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('letter.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/letters/serial-letters/letter-delete-dialog.html',
                    controller: 'LetterDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Letter', function(Letter) {
                            return Letter.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('letter', null, { reload: 'letter' });
                }, function() {
                    $state.go('^');
                });
            }]
        }).state('letter.overwrite', {
            url: '/{id}/overwrite',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/letters/serial-letters/letter-overwrite-dialog.html',
                    controller: 'LetterEditController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Letter', function(Letter) {
                            return Letter.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('letter', null, { reload: 'letter' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
