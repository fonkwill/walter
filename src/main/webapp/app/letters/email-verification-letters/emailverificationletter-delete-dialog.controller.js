(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('EmailVerificationLetterDeleteController',EmailVerificationLetterDeleteController);

    EmailVerificationLetterDeleteController.$inject = ['$uibModalInstance', 'entity', 'EmailVerificationLetter'];

    function EmailVerificationLetterDeleteController($uibModalInstance, entity, EmailVerificationLetter) {
        var vm = this;

        vm.emailVerificationLetter = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            EmailVerificationLetter.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
