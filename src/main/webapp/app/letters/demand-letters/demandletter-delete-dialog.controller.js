(function () {
    'use strict';

    angular
        .module('walterApp')
        .controller('DemandLetterDeleteController', DemandLetterDeleteController);

    DemandLetterDeleteController.$inject = ['$uibModalInstance', 'entity', 'DemandLetter'];

    function DemandLetterDeleteController($uibModalInstance, entity, DemandLetter) {
        var vm = this;

        vm.demandletter = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            DemandLetter.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
