(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ClothingDialogController', ClothingDialogController);

    ClothingDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Clothing', 'PersonClothing', 'ClothingType'];

    function ClothingDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Clothing, PersonClothing, ClothingType) {
        var vm = this;

        vm.clothing = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.personclothings = PersonClothing.query();
        vm.clothingtypes = ClothingType.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.clothing.id !== null) {
                Clothing.update(vm.clothing, onSaveSuccess, onSaveError);
            } else {
                Clothing.save(vm.clothing, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('walterApp:clothingUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.purchaseDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
