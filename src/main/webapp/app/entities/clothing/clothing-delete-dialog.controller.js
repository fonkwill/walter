(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ClothingDeleteController',ClothingDeleteController);

    ClothingDeleteController.$inject = ['$uibModalInstance', 'entity', 'Clothing'];

    function ClothingDeleteController($uibModalInstance, entity, Clothing) {
        var vm = this;

        vm.clothing = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Clothing.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
