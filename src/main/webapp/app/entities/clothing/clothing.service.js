(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Clothing', Clothing);

    Clothing.$inject = ['$resource', 'DateUtils'];

    function Clothing ($resource, DateUtils) {
        var resourceUrl =  'api/clothing/:id';

        return $resource(resourceUrl, {}, {


            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.purchaseDate = DateUtils.convertLocalDateFromServer(data.purchaseDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.purchaseDate = DateUtils.convertLocalDateToServer(copy.purchaseDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.purchaseDate = DateUtils.convertLocalDateToServer(copy.purchaseDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
