(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('AddressDetailController', AddressDetailController);

    AddressDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Address', 'PersonAddress', 'Appointment', 'Company'];

    function AddressDetailController($scope, $rootScope, $stateParams, previousState, entity, Address, PersonAddress, Appointment, Company) {
        var vm = this;

        vm.address = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('walterApp:addressUpdate', function(event, result) {
            vm.address = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
