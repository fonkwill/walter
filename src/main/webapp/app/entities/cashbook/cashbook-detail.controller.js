(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookDetailController', CashbookDetailController);

    CashbookDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Cashbook', 'CashbookEntry'];

    function CashbookDetailController($scope, $rootScope, $stateParams, previousState, entity, Cashbook, CashbookEntry) {
        var vm = this;

        vm.cashbook = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('walterApp:cashbookUpdate', function(event, result) {
            vm.cashbook = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
