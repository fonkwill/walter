(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Cashbook', Cashbook);

    Cashbook.$inject = ['$resource'];

    function Cashbook ($resource) {
        var resourceUrl =  'api/cashbooks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
