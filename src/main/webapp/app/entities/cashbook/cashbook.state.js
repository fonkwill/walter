(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('cashbook', {
            parent: 'entity',
            url: '/cashbook?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.cashbook.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cashbook/cashbooks.html',
                    controller: 'CashbookController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cashbook');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('cashbook-detail', {
            parent: 'entity',
            url: '/cashbook/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.cashbook.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/cashbook/cashbook-detail.html',
                    controller: 'CashbookDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('cashbook');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Cashbook', function($stateParams, Cashbook) {
                    return Cashbook.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'cashbook',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('cashbook-detail.edit', {
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cashbook/cashbook-dialog.html',
                    controller: 'CashbookDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Cashbook', function(Cashbook) {
                            return Cashbook.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cashbook.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cashbook/cashbook-dialog.html',
                    controller: 'CashbookDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('cashbook', null, { reload: 'cashbook' });
                }, function() {
                    $state.go('cashbook');
                });
            }]
        })
        .state('cashbook.edit', {
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cashbook/cashbook-dialog.html',
                    controller: 'CashbookDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Cashbook', function(Cashbook) {
                            return Cashbook.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('cashbook', null, { reload: 'cashbook' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('cashbook.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/cashbook/cashbook-delete-dialog.html',
                    controller: 'CashbookDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Cashbook', function(Cashbook) {
                            return Cashbook.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('cashbook', null, { reload: 'cashbook' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
