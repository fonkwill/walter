(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookDialogController', CashbookDialogController);

    CashbookDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Cashbook', 'CashbookEntry'];

    function CashbookDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Cashbook, CashbookEntry) {
        var vm = this;

        vm.cashbook = entity;
        vm.clear = clear;
        vm.save = save;
        vm.cashbookentries = CashbookEntry.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.cashbook.id !== null) {
                Cashbook.update(vm.cashbook, onSaveSuccess, onSaveError);
            } else {
                Cashbook.save(vm.cashbook, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('walterApp:cashbookUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
