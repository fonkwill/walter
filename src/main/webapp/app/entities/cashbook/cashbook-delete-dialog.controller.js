(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CashbookDeleteController',CashbookDeleteController);

    CashbookDeleteController.$inject = ['$uibModalInstance', 'entity', 'Cashbook'];

    function CashbookDeleteController($uibModalInstance, entity, Cashbook) {
        var vm = this;

        vm.cashbook = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Cashbook.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
