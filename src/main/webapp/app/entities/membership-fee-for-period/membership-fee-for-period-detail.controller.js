(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('MembershipFeeForPeriodDetailController', MembershipFeeForPeriodDetailController);

    MembershipFeeForPeriodDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'MembershipFeeForPeriod', 'CashbookEntry', 'Membership'];

    function MembershipFeeForPeriodDetailController($scope, $rootScope, $stateParams, previousState, entity, MembershipFeeForPeriod, CashbookEntry, Membership) {
        var vm = this;

        vm.membershipFeeForPeriod = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('walterApp:membershipFeeForPeriodUpdate', function(event, result) {
            vm.membershipFeeForPeriod = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
