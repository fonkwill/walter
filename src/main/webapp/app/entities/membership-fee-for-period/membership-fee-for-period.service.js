(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('MembershipFeeForPeriod', MembershipFeeForPeriod);

    MembershipFeeForPeriod.$inject = ['$resource'];

    function MembershipFeeForPeriod ($resource) {
        var resourceUrl =  'api/membership-fee-for-periods/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
