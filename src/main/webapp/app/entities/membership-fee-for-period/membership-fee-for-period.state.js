(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('membership-fee-for-period', {
            parent: 'entity',
            url: '/membership-fee-for-period?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.membershipFeeForPeriod.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-periods.html',
                    controller: 'MembershipFeeForPeriodController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('membershipFeeForPeriod');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('membership-fee-for-period-detail', {
            parent: 'entity',
            url: '/membership-fee-for-period/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.membershipFeeForPeriod.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-period-detail.html',
                    controller: 'MembershipFeeForPeriodDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('membershipFeeForPeriod');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MembershipFeeForPeriod', function($stateParams, MembershipFeeForPeriod) {
                    return MembershipFeeForPeriod.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'membership-fee-for-period',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('membership-fee-for-period-detail.edit', {
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-period-dialog.html',
                    controller: 'MembershipFeeForPeriodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MembershipFeeForPeriod', function(MembershipFeeForPeriod) {
                            return MembershipFeeForPeriod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('membership-fee-for-period.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-period-dialog.html',
                    controller: 'MembershipFeeForPeriodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nr: null,
                                year: null,
                                referenceCode: null,
                                paid: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('membership-fee-for-period', null, { reload: 'membership-fee-for-period' });
                }, function() {
                    $state.go('membership-fee-for-period');
                });
            }]
        })
        .state('membership-fee-for-period.edit', {
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-period-dialog.html',
                    controller: 'MembershipFeeForPeriodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MembershipFeeForPeriod', function(MembershipFeeForPeriod) {
                            return MembershipFeeForPeriod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('membership-fee-for-period', null, { reload: 'membership-fee-for-period' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('membership-fee-for-period.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/membership-fee-for-period/membership-fee-for-period-delete-dialog.html',
                    controller: 'MembershipFeeForPeriodDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MembershipFeeForPeriod', function(MembershipFeeForPeriod) {
                            return MembershipFeeForPeriod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('membership-fee-for-period', null, { reload: 'membership-fee-for-period' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
