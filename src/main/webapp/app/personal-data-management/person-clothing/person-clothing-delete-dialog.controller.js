(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('PersonClothingDeleteController',PersonClothingDeleteController);

    PersonClothingDeleteController.$inject = ['$uibModalInstance', 'entity', 'PersonClothing'];

    function PersonClothingDeleteController($uibModalInstance, entity, PersonClothing) {
        var vm = this;

        vm.personClothing = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PersonClothing.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
