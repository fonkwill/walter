(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('person-instrument', {
            parent: 'entity',
            url: '/person-instrument?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personInstrument.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-instrument/person-instruments.html',
                    controller: 'PersonInstrumentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personInstrument');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('person-instrument.new', {

            url: '/new/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personInstrument.home.create'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-instrument/person-instrument-edit.html',
                    controller: 'PersonInstrumentEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personInstrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonInstrument', function($stateParams, PersonGroup) {
                    return {
                        beginDate: null,
                        endDate: null,
                        id: null
                    };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-instrument.edit', {

            url: '/{id}/edit/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personInstrument.home.edit'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-instrument/person-instrument-edit.html',
                    controller: 'PersonInstrumentEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personInstrument');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonInstrument', function($stateParams, PersonInstrument) {
                    return PersonInstrument.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-instrument.delete', {
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/personal-data-management/person-instrument/person-instrument-delete-dialog.html',
                    controller: 'PersonInstrumentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PersonInstrument', function(PersonInstrument) {
                            return PersonInstrument.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                     $state.go('person.detail.instrument', {"id": $state.params.personid } );
                }, function() {
                     $state.go('^');
                });
            }]
        });
    }

})();
