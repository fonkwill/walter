(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('PersonInstrument', PersonInstrument);

    PersonInstrument.$inject = ['$resource', 'DateUtils'];

    function PersonInstrument ($resource, DateUtils) {
        var resourceUrl =  'api/person-instruments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.beginDate = DateUtils.convertLocalDateFromServer(data.beginDate);
                        data.endDate = DateUtils.convertLocalDateFromServer(data.endDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginDate = DateUtils.convertLocalDateToServer(copy.beginDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginDate = DateUtils.convertLocalDateToServer(copy.beginDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
