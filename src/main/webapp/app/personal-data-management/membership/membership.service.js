(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Membership', Membership);

    Membership.$inject = ['$resource', 'DateUtils'];

    function Membership ($resource, DateUtils) {
        var resourceUrl =  'api/memberships/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.beginDate = DateUtils.convertLocalDateFromServer(data.beginDate);
                        data.endDate = DateUtils.convertLocalDateFromServer(data.endDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginDate = DateUtils.convertLocalDateToServer(copy.beginDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.beginDate = DateUtils.convertLocalDateToServer(copy.beginDate);
                    copy.endDate = DateUtils.convertLocalDateToServer(copy.endDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('PeriodsForPerson', PeriodsForPerson);

    PeriodsForPerson.$inject = ['$resource'];

    function PeriodsForPerson ($resource) {
        var resourceUrl =  'api/memberships/:id/periods';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'save': {
                method: 'POST',
                isArray: true
            }
        });
    }
})();
