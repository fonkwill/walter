(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('membership', {
            parent: 'entity',
            url: '/membership?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.membership.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/membership/memberships.html',
                    controller: 'MembershipController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('membership');
                    $translatePartialLoader.addPart('membershipFeeForPeriod');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('membership.new', {
            url: '/new/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.membership.home.create'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/membership/membership-edit.html',
                    controller: 'MembershipEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('membership');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Membership', function($stateParams, PersonGroup) {
                    return {
                        beginDate: null,
                        endDate: null,
                        membershipFeeId: null,
                        id: null
                    };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.instrument',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('membership.edit', {
            url: '/{id}/edit/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.membership.home.edit'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/membership/membership-edit.html',
                    controller: 'MembershipEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('membership');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Membership', function($stateParams, Membership) {
                    return Membership.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.membership',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('membership.delete', {
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/personal-data-management/membership/membership-delete-dialog.html',
                    controller: 'MembershipDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Membership', function(Membership) {
                            return Membership.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                     $state.go('person.detail.membership', {"id": $state.params.personid } );
                }, function() {
                     $state.go('^');
                });
            }]
        });
    }

})();
