(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('PeopleFees', PeopleFees);

    PeopleFees.$inject = ['$resource', 'DateUtils'];

    function PeopleFees ($resource, DateUtils) {
        var resourceUrl =  'api/peopleWithFees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
