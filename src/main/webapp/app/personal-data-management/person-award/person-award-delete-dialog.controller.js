(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('PersonAwardDeleteController',PersonAwardDeleteController);

    PersonAwardDeleteController.$inject = ['$uibModalInstance', 'entity', 'PersonAward'];

    function PersonAwardDeleteController($uibModalInstance, entity, PersonAward) {
        var vm = this;

        vm.personAward = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PersonAward.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
