(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('person-award', {
            parent: 'entity',
            url: '/person-award?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personAward.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-award/person-awards.html',
                    controller: 'PersonAwardController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personAward');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('person-award.new', {
            url: '/new/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personAward.home.create'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-award/person-award-edit.html',
                    controller: 'PersonAwardEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personAward');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonAward', function($stateParams, PersonGroup) {
                    return {
                        beginDate: null,
                        endDate: null,
                        id: null
                    };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.award',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-award.edit', {
            url: '/{id}/edit/person/{personid}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personAward.home.edit'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-award/person-award-edit.html',
                    controller: 'PersonAwardEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personAward');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonAward', function($stateParams, PersonAward) {
                    return PersonAward.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person.detail.award',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-award.delete', {
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/personal-data-management/person-award/person-award-delete-dialog.html',
                    controller: 'PersonAwardDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PersonAward', function(PersonAward) {
                            return PersonAward.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                     $state.go('person.detail.award', {"id": $state.params.personid } );
                }, function() {
                     $state.go('^');
                });
            }]
        });
    }

})();
