(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('MembershipFeeDeleteController',MembershipFeeDeleteController);

    MembershipFeeDeleteController.$inject = ['$uibModalInstance', 'entity', 'MembershipFee'];

    function MembershipFeeDeleteController($uibModalInstance, entity, MembershipFee) {
        var vm = this;

        vm.membershipFee = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MembershipFee.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
