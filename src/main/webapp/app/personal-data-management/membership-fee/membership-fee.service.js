(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('MembershipFee', MembershipFee);

    MembershipFee.$inject = ['$resource'];

    function MembershipFee ($resource) {
        var resourceUrl =  'api/membership-fees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('AmountsForMembershipFee', AmountsForMembershipFee);

    AmountsForMembershipFee.$inject = ['$resource'];
    
    function AmountsForMembershipFee ($resource) {
        var resourceUrl =  'api/membership-fees/:id/amounts';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
