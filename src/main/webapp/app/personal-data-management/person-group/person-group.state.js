(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('person-group', {
            parent: 'personal-data-management',
            url: '/person-group?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personGroup.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-group/person-groups.html',
                    controller: 'PersonGroupController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personGroup');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })

        .state('person-group.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personGroup.home.create'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-group/person-group-edit.html',
                    controller: 'PersonGroupEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personGroup');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonGroup', function($stateParams, PersonGroup) {
                    return {
                        name: null,
                         id: null
                };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person-group',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-group.edit', {
            url: '/{id}/edit',

            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.personGroup.home.edit'
            },
            views: {
                'content@': {
                    templateUrl: 'app/personal-data-management/person-group/person-group-edit.html',
                    controller: 'PersonGroupEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('personGroup');
                    $translatePartialLoader.addPart('error');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PersonGroup', function($stateParams, PersonGroup) {
                        return PersonGroup.get({id : $stateParams.id}).$promise;

                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person-group',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }




        })
        .state('person-group.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/personal-data-management/person-group/person-group-delete-dialog.html',
                    controller: 'PersonGroupDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PersonGroup', function(PersonGroup) {
                            return PersonGroup.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('person-group', null, { reload: 'person-group' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
