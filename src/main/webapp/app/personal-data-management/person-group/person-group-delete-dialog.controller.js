(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('PersonGroupDeleteController',PersonGroupDeleteController);

    PersonGroupDeleteController.$inject = ['$uibModalInstance', 'entity', 'PersonGroup'];

    function PersonGroupDeleteController($uibModalInstance, entity, PersonGroup) {
        var vm = this;

        vm.personGroup = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PersonGroup.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
