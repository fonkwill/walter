(function () {
    'use strict';

    angular
        .module('walterApp')
        .controller('AppointmentTypeDeleteController', AppointmentTypeDeleteController);

    AppointmentTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'AppointmentType'];

    function AppointmentTypeDeleteController($uibModalInstance, entity, AppointmentType) {
        var vm = this;

        vm.appointmentType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete(id) {
            AppointmentType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
