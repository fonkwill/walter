(function () {
    'use strict';
    angular
        .module('walterApp')
        .factory('AppointmentType', AppointmentType);

    AppointmentType.$inject = ['$resource'];

    function AppointmentType($resource) {
        var resourceUrl = 'api/appointment-types/:id';

        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': {method: 'PUT'},
            'save': {method: 'POST'}
        });
    }
})();
