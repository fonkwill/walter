(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('NewDirectoryDialogController',NewDirectoryDialogController);

    NewDirectoryDialogController.$inject = ['$uibModalInstance', '$filter', '$stateParams', 'Directory'];

    function NewDirectoryDialogController($uibModalInstance, $filter, $stateParams, Directory) {
        var vm = this;

        vm.directory = {
            "originalFile" : $filter('translate')('walterApp.documents.defaults.new-directory'),
            "type": "DIRECTORY",
            "parentId": $stateParams.directory
        }
        vm.clear = clear;
        vm.confirm = confirm;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirm () {
            Directory.save(vm.directory, function() {
                $uibModalInstance.close(true);
            });
        }
    }
})();
