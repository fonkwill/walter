(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('MoveDialogController',MoveDialogController);

    MoveDialogController.$inject = ['$uibModalInstance', '$rootScope', '$stateParams', '$filter', 'Directory', 'File', 'selectedData', 'directory'];

    function MoveDialogController($uibModalInstance, $rootScope, $stateParams, $filter, Directory, File, selectedData, directory) {
        var vm = this;

        vm.currentDirectory = directory;

        vm.clear = clear;
        vm.confirm = confirm;
        vm.breadCrumbs = [];
        vm.currentBreadCrumb = "";
        vm.formatFileSize = formatFileSize;
        vm.rowClick = rowClick;
        vm.hasBreadCrumbs = hasBreadCrumbs;
        vm.levelUp = levelUp;
        vm.changeDirectory = changeDirectory;
        vm.isSelectable = isSelectable;

        getData(vm.currentDirectory);

        function getData(directoryUuid) {
            Directory.query({
                uuid: directoryUuid,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = ['originalFile' + ',' + 'asc'];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.elements = data.content;
                setBreadCrumbs(data.breadCrumbs);
                vm.currentBreadCrumb = data.name;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function setBreadCrumbs(hashMap) {
            angular.forEach(hashMap, function (value, key) {
                this.push({uuid: key, text: value});
            }, vm.breadCrumbs);
            vm.breadCrumbs.reverse();
        }

        function formatFileSize(size) {
            if (size == null) {
                return "";
            }
            size /= 1024;
            size /= 1024;

            return size.toFixed(2) + ' Mb';
        }

        function rowClick(element) {
            if (element.type == 'DIRECTORY' && isSelectable(element)) {
                changeDirectory(element.id);
            }
        }

        function changeDirectory(uuid) {
            vm.currentDirectory = uuid;
            getData(uuid);
        }

        function hasBreadCrumbs() {
            if (vm.currentBreadCrumb != null) {
                return true;
            } else {
                return false;
            }
        }

        function levelUp() {
            if (hasBreadCrumbs()) {
                if (vm.breadCrumbs.length > 0) {
                    var toGo = vm.breadCrumbs[vm.breadCrumbs.length - 1];
                    changeDirectory(toGo.uuid);
                } else {
                    changeDirectory(null);
                }
            }
        }

        function isSelectable(element) {
            if (element.type == 'FILE') {
                return false;
            } else {
                var res = $filter('filter')(selectedData, {id: element.id}, true)[0];
                if (res) {
                    return false;
                }
                return true;
            }
        }

        function clear () {
            $uibModalInstance.dismiss();
        }

        function confirm (newLocation) {
            doMove(0, newLocation);
        }

        function doMove(index, newLocation) {
            selectedData[index].parentId = newLocation;
            if (selectedData[index].type == 'DIRECTORY') {
                Directory.update(selectedData[index], function () {
                    if (index != (selectedData.length - 1)) {
                        doMove(index + 1, newLocation);
                    } else {
                        $uibModalInstance.close();
                    }
                });
            } else {
                File.update(selectedData[index], function () {
                    if (index != (selectedData.length - 1)) {
                        doMove(index + 1);
                    } else {
                        $uibModalInstance.close();
                    }
                });
            }
        }
    }
})();
