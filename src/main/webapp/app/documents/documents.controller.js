(function () {
        'use strict';

        angular
            .module('walterApp')
            .controller('DocumentsController', DocumentsController);

        DocumentsController.$inject = ['$scope', '$state', '$stateParams', 'Directory', 'File', 'Upload', 'pagingParams', 'paginationConstants'];

        function DocumentsController($scope, $state, $stateParams, Directory, File, Upload, pagingParams, paginationConstants) {
            var vm = this;

            vm.currentDirectory = $stateParams.directory;
            vm.breadCrumbs = [];
            vm.currentBreadCrumb = "";
            vm.selectedData = [];
            vm.file = "";

            vm.predicate = pagingParams.predicate;
            vm.reverse = pagingParams.ascending;
            vm.transition = transition;
            vm.itemsPerPage = paginationConstants.itemsPerPage;

            vm.getData = getData;
            vm.rowClick = rowClick;
            vm.changeDirectory = changeDirectory;
            vm.hasBreadCrumbs = hasBreadCrumbs;
            vm.addRemoveSelectedData = addRemoveSelectedData;
            vm.uploadFile = uploadFile;
            vm.formatFileSize = formatFileSize;
            vm.levelUp = levelUp;

            getData();

            function getData() {
                Directory.query({
                    uuid: vm.currentDirectory,
                    sort: sort()
                }, onSuccess, onError);
                function sort() {
                    var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                    if (vm.predicate !== 'id') {
                        result.push('id');
                    }
                    return result;
                }
                function onSuccess(data, headers) {
                    vm.elements = data.content;
                    setBreadCrumbs(data.breadCrumbs);
                    vm.currentBreadCrumb = data.name;
                }
                function onError(error) {
                    AlertService.error(error.data.message);
                }
            }

            function transition () {
                $state.transitionTo($state.$current, {
                    sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                    directory: vm.currentDirectory
                });
            }

            function rowClick(element) {
                if (element.type == 'DIRECTORY') {
                    changeDirectory(element.id);
                } else {
                    downloadFile(element.id);
                }
            }

            function downloadFile(uuid) {
                File.download({
                    uuid: uuid
                }, onSuccess, onError);

                function onSuccess(data, headers) {
                    var blob = new Blob([data.data], { type: headers('content-type') });
                    saveAs(blob, data.filename);
                };

                function onError(error) {
                    AlertService.error(error.data.message);
                };
            }

            function changeDirectory(uuid) {
                $state.go('documents', {directory: uuid}, {reload: true});
            }

            function setBreadCrumbs(hashMap) {
                angular.forEach(hashMap, function (value, key) {
                    this.push({uuid: key, text: value});
                }, vm.breadCrumbs);
                vm.breadCrumbs.reverse();
            }

            function hasBreadCrumbs() {
                if (vm.currentBreadCrumb != null) {
                    return true;
                } else {
                    return false;
                }
            }

            function addRemoveSelectedData(data) {

                var removed = false;

                for (var i = 0; i < vm.selectedData.length; i++) {
                    if (vm.selectedData[i].id == data.id) {
                        vm.selectedData.splice(i, 1);
                        removed = true;
                    }
                }

                if (!removed) {
                    vm.selectedData.push(data);
                }
            }

            function uploadFile() {
                Upload.upload({
                    url: 'api/documents/file',
                    method: 'POST',
                    data: {
                        uploadFile: vm.file,
                        parentUuid: vm.currentDirectory
                    }
                })
                    .progress(function (evt) {
                        vm.progress = parseInt(100.0 * evt.loaded / evt.total);
                    })
                    .success(onSaveSuccess);
            }

            function formatFileSize(size) {
                if (size == null) {
                    return "";
                }
                size /= 1024;
                size /= 1024;

                return size.toFixed(2) + ' Mb';
            }

            function onSaveSuccess(result) {
                vm.file = "";
                getData();
            }

            function levelUp() {
                if (hasBreadCrumbs()) {
                    if (vm.breadCrumbs.length > 0) {
                        var toGo = vm.breadCrumbs[vm.breadCrumbs.length - 1];
                        changeDirectory(toGo.uuid);
                    } else {
                        changeDirectory(null);
                    }
                }
            }
        }
    }

)();
