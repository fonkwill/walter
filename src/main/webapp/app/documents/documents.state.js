(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('documents', {
                parent: 'app',
                url: '/documents?directory&sort',
                data: {
                    pageTitle: 'walterApp.documents.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/documents/documents.html',
                        controller: 'DocumentsController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    directory: {
                        value: '',
                        squash: true
                    },
                    sort: {
                        value: 'originalFile,asc',
                        squash: true
                    },
                },
                resolve: {
                    pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                        return {
                            sort: $stateParams.sort,
                            predicate: PaginationUtil.parsePredicate($stateParams.sort),
                            ascending: PaginationUtil.parseAscending($stateParams.sort)
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('documents');
                        $translatePartialLoader.addPart('directory');
                        $translatePartialLoader.addPart('file');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('documents.new-directory', {
                url: '/new-directory',
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/documents/new-directory-dialog.html',
                        controller: 'NewDirectoryDialogController',
                        controllerAs: 'vm',
                        size: 'md'
                    }).result.then(function() {
                        $state.go('documents', $stateParams, { reload: true });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('documents.confirm-delete', {
                url: '/confirm-delete',
                params: {
                    selectedData: null
                },
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/documents/confirm-delete-dialog.html',
                        controller: 'ConfirmDeleteDialogController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            selectedData: function() { return $stateParams.selectedData; }
                        }
                    }).result.then(function() {
                        $state.go('documents', $stateParams, { reload: true });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('documents.rename-directory', {
                url: '/rename',
                params: {
                    entity: null
                },
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/documents/rename-dialog.html',
                        controller: 'RenameDialogController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: function() { return $stateParams.entity; }
                        }
                    }).result.then(function() {
                        $state.go('documents', $stateParams, { reload: true });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('documents.rename-file', {
                url: '/rename',
                params: {
                    entity: null
                },
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/documents/rename-dialog.html',
                        controller: 'RenameDialogController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: function() { return $stateParams.entity; }
                        }
                    }).result.then(function() {
                        $state.go('documents', $stateParams, { reload: true });
                    }, function() {
                        $state.go('^');
                    });
                }]
            })
            .state('documents.move', {
                url: '/move',
                params: {
                    selectedData: null,
                    directory: null
                },
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/documents/move-dialog.html',
                        controller: 'MoveDialogController',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            selectedData: function() { return $stateParams.selectedData; },
                            directory: function() { return $stateParams.directory; }
                        }
                    }).result.then(function() {
                        $state.go('documents', $stateParams, { reload: true });
                    }, function() {
                        $state.go('^');
                    });
                }]
            });
    }

})();
