(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('RenameDialogController',RenameDialogController);

    RenameDialogController.$inject = ['$uibModalInstance', '$filter', '$stateParams', 'entity', 'Directory', 'File'];

    function RenameDialogController($uibModalInstance, $filter, $stateParams, entity, Directory, File) {
        var vm = this;
        vm.element = entity;
        vm.clear = clear;
        vm.confirm = confirm;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirm () {
            if (vm.element.type == 'DIRECTORY') {
                Directory.update(vm.element, function () {
                    $uibModalInstance.close(true);
                });
            } else {
                File.update(vm.element, function () {
                    $uibModalInstance.close(true);
                });
            }
        }
    }
})();
