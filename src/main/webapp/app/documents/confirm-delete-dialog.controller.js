(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ConfirmDeleteDialogController',ConfirmDeleteDialogController);

    ConfirmDeleteDialogController.$inject = ['$uibModalInstance', '$rootScope', '$stateParams', 'Directory', 'File', 'selectedData'];

    function ConfirmDeleteDialogController($uibModalInstance, $rootScope, $stateParams, Directory, File, selectedData) {
        var vm = this;

        vm.clear = clear;
        vm.confirm = confirm;

        console.log(selectedData);

        function clear () {
            $uibModalInstance.dismiss();
        }

        function confirm () {
            doDelete(0);
        }

        function doDelete(index) {
            if (selectedData[index].type == 'DIRECTORY') {
                Directory.delete({uuid: selectedData[index].id}, function () {
                    if (index != (selectedData.length - 1)) {
                        doDelete(index + 1);
                    } else {
                        $uibModalInstance.close();
                    }
                });
            } else {
                File.delete({uuid: selectedData[index].id}, function () {
                    if (index != (selectedData.length - 1)) {
                        doDelete(index + 1);
                    } else {
                        $uibModalInstance.close();
                    }
                });
            }
        }
    }
})();
