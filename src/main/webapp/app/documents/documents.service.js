(function () {
    'use strict';
    angular
        .module('walterApp')
        .factory('Directory', Directory);

    Directory.$inject = ['$resource'];

    function Directory($resource) {
        var resourceUrl = 'api/documents/directory/:uuid';

        return $resource(resourceUrl, {}, {
            'query': {
                params: {"uuid" : "@uuid"},
                method: 'GET'
            },
            'save': { method: 'POST' },
            'update': { method:'PUT' }
        });
    }
})();

(function () {
    'use strict';
    angular
        .module('walterApp')
        .factory('File', File);

    File.$inject = ['$resource'];

    function File($resource) {
        var resourceUrl = 'api/documents/file/:uuid';

        return $resource(resourceUrl, {}, {
            'update': { method:'PUT' },
            'download': {
                method: 'GET',
                url: 'api/documents/file/:uuid',
                responseType: 'arraybuffer',
                transformResponse: function(data, headers) {
                    var header = headers('content-disposition');
                    var result = header.split(';')[1].trim().split('=')[1];
                    var filename = result.replace(/"/g, '');

                    return {
                        data: data,
                        filename: filename
                    }
                }
            }
        });
    }
})();
