(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('MailLog', MailLog);

    MailLog.$inject = ['$resource'];

    function MailLog ($resource) {
        var resourceUrl =  'api/mail-log';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
