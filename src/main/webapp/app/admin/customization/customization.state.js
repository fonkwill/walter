(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('customization', {
            parent: 'entity',
            url: '/customization?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.customization.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/customization/customizations.html',
                    controller: 'CustomizationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('customization');
                    $translatePartialLoader.addPart('customizationName');
                    $translatePartialLoader.addPart('role');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            },
            onExit: ['NeededEntitiesCache', function(NeededEntitiesCache) {
                NeededEntitiesCache.invalidate();
            }]
        })
        .state('customization-detail', {
            parent: 'customization',
            url: '/customization/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.customization.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/customization/customization-detail.html',
                    controller: 'CustomizationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('customization');
                    $translatePartialLoader.addPart('customizationName');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Customization', function($stateParams, Customization) {
                    return Customization.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'customization',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('customization-detail.edit', {
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.customization.edit.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/customization/customization-edit.html',
                    controller: 'CustomizationEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('customization');
                    $translatePartialLoader.addPart('customizationName');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Customization', function($stateParams, Customization) {
                    return Customization.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'customization',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('customization.new', {
            url: '/new',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.customization.edit.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/customization/customization-edit.html',
                    controller: 'CustomizationEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('customization');
                    $translatePartialLoader.addPart('customizationName');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Customization', function ($stateParams, AppointmentType) {
                    return {
                        id: null,
                        customizationName: null,
                        data: null,
                        text: null
                    };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'customization',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('customization.edit', {
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'walterApp.customization.edit.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/customization/customization-edit.html',
                    controller: 'CustomizationEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('customization');
                    $translatePartialLoader.addPart('role');
                    $translatePartialLoader.addPart('customizationName');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Customization', function($stateParams, Customization) {
                    return Customization.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'customization',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('customization.delete', {
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/admin/customization/customization-delete-dialog.html',
                    controller: 'CustomizationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Customization', function(Customization) {
                            return Customization.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('customization', null, { reload: 'customization' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
