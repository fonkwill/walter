(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('CustomizationDetailController', CustomizationDetailController);

    CustomizationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Customization'];

    function CustomizationDetailController($scope, $rootScope, $stateParams, previousState, entity, Customization) {
        var vm = this;

        vm.customization = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('walterApp:customizationUpdate', function(event, result) {
            vm.customization = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
