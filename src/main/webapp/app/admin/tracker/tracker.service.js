(function() {
    'use strict';
    /* globals SockJS, Stomp */

    angular
        .module('walterApp')
        .factory('JhiTrackerService', JhiTrackerService);

    JhiTrackerService.$inject = ['$rootScope', '$window', '$cookies', '$http', '$q', '$timeout', 'AuthServerProvider'];

    function JhiTrackerService ($rootScope, $window, $cookies, $http, $q, $timeout,  AuthServerProvider) {
        var stompClient = null;
        var subscriber = null;
        var listener = $q.defer();
        var connected = $q.defer();
        var alreadyConnectedOnce = false;
        var userLogin = null;


        var service = {
            connect: connect,
            disconnect: disconnect,
            receive: receive,
            sendActivity: sendActivity,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };

        return service;

        function connect (user) {
            //building absolute path so that websocket doesnt fail when deploying with a context path
            var loc = $window.location;
            var url = '//' + loc.host + loc.pathname + 'that';
            var authToken = AuthServerProvider.getToken();
            if(authToken){
                url += '?access_token=' + authToken;
            }

            var domain = window.location.hostname;
            try {
                var tenant = domain ? domain.substr(0, domain.indexOf('.')) : null;
                if (tenant) {
                    url += '&tenant=' + tenant;
                }

            } catch(e) {
                //do nothing
            }
            var socket = new SockJS(url);
            stompClient = Stomp.over(socket);
            var stateChangeStart;

            var headers = {};
            stompClient.connect(headers, function() {
                connected.resolve('success');
                userLogin = user.login;

                sendActivity();
                if (!alreadyConnectedOnce) {
                    stateChangeStart = $rootScope.$on('$viewContentLoaded', function (event, state, params, fromState, fromParams) {
                        sendActivity();
                    });
                    alreadyConnectedOnce = true;
                }

            });
            $rootScope.$on('$destroy', function () {
                if(angular.isDefined(stateChangeStart) && stateChangeStart !== null){
                    stateChangeStart();
                }
            });
        }

        function disconnect () {
            if (stompClient !== null) {
                stompClient.disconnect();
                stompClient = null;
            }
        }

        function receive () {
            return listener.promise;
        }

        function sendActivity() {
            if ($rootScope.toState && userLogin) {
                if (stompClient !== null && stompClient.connected) {
                    stompClient
                        .send('/activity',
                            {},
                            angular.toJson({'page': $rootScope.toState.name, 'paramId': $rootScope.toStateParams.id, 'userLogin': userLogin} ));
                }
            }
        }

        function subscribe () {
            connected.promise.then(function() {
                subscriber = stompClient.subscribe('/topic/tracker', function(data) {
                    listener.notify(angular.fromJson(data.body));
                });
                // subscriber = stompClient.subscribe('/topic/activity', function(data) {
                //     listener.notify(angular.fromJson(data.body));
                // });
            }, null, null);
        }

        function unsubscribe () {
            if (subscriber !== null) {
                subscriber.unsubscribe();
            }
            listener = $q.defer();
        }
    }
})();
