(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('NotificationRule', NotificationRule);

    NotificationRule.$inject = ['$resource'];

    function NotificationRule ($resource) {
        var resourceUrl =  'api/notification-rules/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'createNotifications': {
                method: 'GET',
                url: 'api/notification-rules/create-notifications'
            },
            'validEntities': {
                method: 'GET',
                url: 'api/notification-rules/valid-entities',
                isArray: true
            },
            'validAuthorities': {
                method: 'GET',
                url: 'api/notification-rules/valid-authorities',
                isArray: true
            }
        });
    }
})();
