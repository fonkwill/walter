(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('TenantConfigurationDeleteController',TenantConfigurationDeleteController);

    TenantConfigurationDeleteController.$inject = ['$uibModalInstance', 'entity', 'TenantConfiguration'];

    function TenantConfigurationDeleteController($uibModalInstance, entity, TenantConfiguration) {
        var vm = this;

        vm.tenantConfiguration = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TenantConfiguration.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
