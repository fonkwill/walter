(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Role', Role);

    Role.$inject = ['$resource'];

    function Role ($resource) {
        var resourceUrl =  'api/roles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'getFeatureGroups': {
                method: 'GET',
                url: 'api/roles/feature-groups',
                isArray: true
            },
            'getAllAuthorities': {
                method: 'GET',
                url: 'api/roles/authorities',
                isArray: true
            }
        });
    }
})();
