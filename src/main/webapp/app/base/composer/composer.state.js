(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('composer', {
            parent: 'base',
            url: '/composer?page&sort&search',
            data: {
                pageTitle: 'walterApp.composer.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/composer/composers.html',
                    controller: 'ComposerController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('composer');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('composer.new', {
            url: '/new',
            data: {
                pageTitle: 'walterApp.composer.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/composer/composer-detail.html',
                    controller: 'ComposerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('composer');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Composer', function($stateParams, Composer) {
                        return {
                            name: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'composer',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('composer.edit', {
            url: '/{id}/edit',
            data: {
                pageTitle: 'walterApp.Composer.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/composer/composer-detail.html',
                    controller: 'ComposerDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('composer');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Composer', function($stateParams, Composer) {
                    return Composer.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'composer',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('composer.delete', {
            url: '/delete',
            data: {
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/base/composer/composer-delete-dialog.html',
                    controller: 'ComposerDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Composer', function(Composer) {
                            return Composer.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('composer', null, { reload: 'composer' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
