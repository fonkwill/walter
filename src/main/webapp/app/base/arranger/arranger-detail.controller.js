(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ArrangerDetailController', ArrangerDetailController);

    ArrangerDetailController.$inject = ['$timeout', '$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'Arranger'];

    function ArrangerDetailController($timeout, $scope, $rootScope, $state, $stateParams, previousState, entity, Arranger) {
        var vm = this;
        vm.previousState = previousState.name;
        vm.save = save;
        vm.arranger = entity;
        vm.goBack = goBack;

        if(entity.id != null) {
        	vm.createMode = false;
        }
        else {
        	vm.createMode = true;
        }
        
        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });
        
        function save () {
            vm.isSaving = true;
            if (vm.arranger.id !== null) {
                Arranger.update(vm.arranger, onSaveSuccess, onSaveError);
            } else {
                Arranger.save(vm.arranger, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('walterApp:ArrangerUpdate', result);
            $state.go(previousState.name, previousState.params, {reload : true});
        }
        
        function goBack() {
        	$state.go(previousState.name, previousState.params, {reload : true});
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
