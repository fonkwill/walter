(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('Arranger', Arranger);

    Arranger.$inject = ['$resource'];

    function Arranger ($resource) {
        var resourceUrl =  'api/arrangers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
