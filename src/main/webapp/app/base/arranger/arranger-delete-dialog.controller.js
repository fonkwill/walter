(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ArrangerDeleteController',ArrangerDeleteController);

    ArrangerDeleteController.$inject = ['$uibModalInstance', 'entity', 'Arranger'];

    function ArrangerDeleteController($uibModalInstance, entity, Arranger) {
        var vm = this;

        vm.arranger = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Arranger.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
