(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('MusicBook', MusicBook);

    MusicBook.$inject = ['$resource'];

    function MusicBook ($resource) {
        var resourceUrl =  'api/music-books/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
