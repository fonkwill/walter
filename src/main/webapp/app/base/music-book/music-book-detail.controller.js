(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('MusicBookDetailController', MusicBookDetailController);

    MusicBookDetailController.$inject = ['$timeout', '$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'MusicBook'];

    function MusicBookDetailController($timeout, $scope, $rootScope, $state, $stateParams, previousState, entity, MusicBook) {
        var vm = this;
        vm.previousState = previousState;
        vm.save = save;
        vm.musicBook = entity;

        if(entity.id != null) {
        	vm.createMode = false;
        }
        else {
        	vm.createMode = true;
        }
        
        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });
        
        function save () {
            vm.isSaving = true;
            if (vm.musicBook.id !== null) {
                MusicBook.update(vm.musicBook, onSaveSuccess, onSaveError);
            } else {
                MusicBook.save(vm.musicBook, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('walterApp:MusicBookUpdate', result);
            $state.go(previousState.name, previousState.params);
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
