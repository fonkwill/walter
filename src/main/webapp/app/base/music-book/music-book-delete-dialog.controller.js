(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('MusicBookDeleteController',MusicBookDeleteController);

    MusicBookDeleteController.$inject = ['$uibModalInstance', 'entity', 'MusicBook'];

    function MusicBookDeleteController($uibModalInstance, entity, MusicBook) {
        var vm = this;

        vm.musicBook = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            MusicBook.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
