(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('music-book', {
            parent: 'base',
            url: '/music-book?page&sort&search',
            data: {
                pageTitle: 'walterApp.musicBook.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/music-book/music-books.html',
                    controller: 'MusicBookController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('musicBook');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('music-book.new', {
            url: '/new',
            data: {
                pageTitle: 'walterApp.musicBook.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/music-book/music-book-detail.html',
                    controller: 'MusicBookDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('musicBook');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MusicBook', function($stateParams, MusicBook) {
                        return {
                            name: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'music-book',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('music-book.edit', {
            url: '/{id}/edit',
            data: {
                pageTitle: 'walterApp.musicBook.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/music-book/music-book-detail.html',
                    controller: 'MusicBookDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('musicBook');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MusicBook', function($stateParams, MusicBook) {
                    return MusicBook.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'music-book',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('music-book.delete', {
            url: '/delete',
            data: {
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/base/music-book/music-book-delete-dialog.html',
                    controller: 'MusicBookDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MusicBook', function(MusicBook) {
                            return MusicBook.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('music-book', null, { reload: 'music-book' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
