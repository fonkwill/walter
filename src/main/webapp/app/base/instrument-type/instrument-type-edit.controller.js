(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('InstrumentTypeEditController', InstrumentTypeEditController);

    InstrumentTypeEditController.$inject = ['$timeout', '$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'InstrumentType'];

    function InstrumentTypeEditController($timeout, $scope, $rootScope, $state, $stateParams, previousState, entity, InstrumentType) {
        var vm = this;
        vm.previousState = previousState.name;
        vm.save = save;
        vm.instrumentType = entity;

        if(entity.id != null) {
        	vm.createMode = false;
        }
        else {
        	vm.createMode = true;
        }
        
        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });
        
        function save () {
            vm.isSaving = true;
            if (vm.instrumentType.id !== null) {
                InstrumentType.update(vm.instrumentType, onSaveSuccess, onSaveError);
            } else {
            	InstrumentType.save(vm.instrumentType, onSaveSuccess, onSaveError);
            }
        }
        
        function onSaveSuccess (result) {
            $scope.$emit('walterApp:instrumentTypeUpdate', result);
            $state.go(previousState.name);
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
