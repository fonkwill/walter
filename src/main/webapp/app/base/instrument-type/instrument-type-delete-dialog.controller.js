(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('InstrumentTypeDeleteController',InstrumentTypeDeleteController);

    InstrumentTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'InstrumentType'];

    function InstrumentTypeDeleteController($uibModalInstance, entity, InstrumentType) {
        var vm = this;

        vm.instrumentType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InstrumentType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
