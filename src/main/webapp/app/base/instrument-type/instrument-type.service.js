(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('InstrumentType', InstrumentType);

    InstrumentType.$inject = ['$resource'];

    function InstrumentType ($resource) {
        var resourceUrl =  'api/instrument-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();

(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('InstrumentsForType', InstrumentsForType);

    InstrumentsForType.$inject = ['$resource'];
    
    function InstrumentsForType ($resource) {
        var resourceUrl =  'api/instrument-types/:id/instruments';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();