(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('instrument-type', {
            parent: 'base',
            url: '/instrument-type?page&sort&search',
            data: {
                pageTitle: 'walterApp.instrumentType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/instrument-type/instrument-types.html',
                    controller: 'InstrumentTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrumentType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('instrument-type.new', {
            url: '/new',
            data: {
                pageTitle: 'walterApp.instrumentType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/instrument-type/instrument-type-edit.html',
                    controller: 'InstrumentTypeEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrumentType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InstrumentType', function($stateParams, InstrumentType) {
                        return {
                            name: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-type.edit', {
            url: '/{id}/edit',
            data: {
                pageTitle: 'walterApp.instrumentType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/instrument-type/instrument-type-edit.html',
                    controller: 'InstrumentTypeEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('instrumentType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'InstrumentType', function($stateParams, InstrumentType) {
                    return InstrumentType.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'instrument-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('instrument-type.delete', {
            url: '/{id}/delete',
            data: {
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/base/instrument-type/instrument-type-delete-dialog.html',
                    controller: 'InstrumentTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InstrumentType', function(InstrumentType) {
                            return InstrumentType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('instrument-type', null, { reload: 'instrument-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
