(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('HonorDeleteController',HonorDeleteController);

    HonorDeleteController.$inject = ['$uibModalInstance', 'entity', 'Honor'];

    function HonorDeleteController($uibModalInstance, entity, Honor) {
        var vm = this;

        vm.honor = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Honor.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
