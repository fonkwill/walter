(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('HonorEditController', HonorEditController);

    HonorEditController.$inject = ['$timeout', '$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'Honor'];

    function HonorEditController($timeout, $scope, $rootScope, $state, $stateParams, previousState, entity, Honor) {
        var vm = this;
        vm.previousState = previousState.name;
        vm.save = save;
        vm.honor = entity;

        if(entity.id != null) {
        }
        else {
        	vm.createMode = true;
        }
        
        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });
        
        function save () {
            vm.isSaving = true;
            if (vm.honor.id !== null) {
                Honor.update(vm.honor, onSaveSuccess, onSaveError);
            } else {
            	Honor.save(vm.honor, onSaveSuccess, onSaveError);
            }
        }
        
        function onSaveSuccess (result) {
            $scope.$emit('walterApp:honorUpdate', result);
            $state.go(previousState.name);
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
