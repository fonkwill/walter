(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ColorCodeDeleteController',ColorCodeDeleteController);

    ColorCodeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ColorCode'];

    function ColorCodeDeleteController($uibModalInstance, entity, ColorCode) {
        var vm = this;

        vm.colorCode = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ColorCode.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
