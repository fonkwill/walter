(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('color-code', {
            parent: 'base',
            url: '/color-code?page&sort&search',
            data: {
                pageTitle: 'walterApp.ColorCode.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/color-code/color-codes.html',
                    controller: 'ColorCodeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('colorCode');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('color-code.new', {
            url: '/new',
            data: {
                pageTitle: 'walterApp.ColorCode.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/color-code/color-code-detail.html',
                    controller: 'ColorCodeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('colorCode');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ColorCode', function($stateParams, ColorCode) {
                        return {
                            name: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'color-code',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('color-code.edit', {
            url: '/{id}/edit',
            data: {
                pageTitle: 'walterApp.ColorCode.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/color-code/color-code-detail.html',
                    controller: 'ColorCodeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('colorCode');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ColorCode', function($stateParams, ColorCode) {
                    return ColorCode.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'color-code',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('color-code.delete', {
            url: '/delete',
            data: {
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/base/color-code/color-code-delete-dialog.html',
                    controller: 'ColorCodeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ColorCode', function(ColorCode) {
                            return ColorCode.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('color-code', null, { reload: 'color-code' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
