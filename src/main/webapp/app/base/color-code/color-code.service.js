(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('ColorCode', ColorCode);

    ColorCode.$inject = ['$resource'];

    function ColorCode ($resource) {
        var resourceUrl =  'api/color-codes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
