(function() {
    'use strict';

    angular
        .module('walterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('clothing-type', {
            parent: 'base',
            url: '/clothing-type?page&sort&search',
            data: {
                pageTitle: 'walterApp.clothingType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/clothing-type/clothing-types.html',
                    controller: 'ClothingTypeController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('clothing-type.new', {
            url: '/new',
            data: {
                pageTitle: 'walterApp.clothing-type.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/clothing-type/clothing-type-edit.html',
                    controller: 'ClothingTypeEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClothingType', function($stateParams, ClothingType) {
                        return {
                            name: null,
                            id: null
                        };
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'clothing-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('clothing-type.edit', {
            url: '/{id}/edit',
            data: {
                pageTitle: 'walterApp.clothing-type.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/base/clothing-type/clothing-type-edit.html',
                    controller: 'ClothingTypeEditController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('clothingType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ClothingType', function($stateParams, ClothingType) {
                    return ClothingType.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'clothing-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('clothing-type.delete', {
            url: '/{id}/delete',
            data: {
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/base/clothing-type/clothing-type-delete-dialog.html',
                    controller: 'ClothingTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ClothingType', function(ClothingType) {
                            return ClothingType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('clothing-type', null, { reload: 'clothing-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
