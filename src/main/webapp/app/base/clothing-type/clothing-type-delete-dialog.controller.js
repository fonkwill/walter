(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ClothingTypeDeleteController',ClothingTypeDeleteController);

    ClothingTypeDeleteController.$inject = ['$uibModalInstance', 'entity', 'ClothingType'];

    function ClothingTypeDeleteController($uibModalInstance, entity, ClothingType) {
        var vm = this;

        vm.clothingType = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ClothingType.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
