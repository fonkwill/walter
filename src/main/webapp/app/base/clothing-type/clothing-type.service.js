(function() {
    'use strict';
    angular
        .module('walterApp')
        .factory('ClothingType', ClothingType);

    ClothingType.$inject = ['$resource'];

    function ClothingType ($resource) {
        var resourceUrl =  'api/clothing-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
