(function() {
    'use strict';

    angular
        .module('walterApp')
        .controller('ClothingTypeEditController', ClothingTypeEditController);

    ClothingTypeEditController.$inject = ['$timeout', '$scope', '$rootScope', '$state', '$stateParams', 'previousState', 'entity', 'ClothingType'];

    function ClothingTypeEditController($timeout, $scope, $rootScope, $state, $stateParams, previousState, entity, ClothingType) {
        var vm = this;
        vm.previousState = previousState.name;
        vm.save = save;
        vm.clothingType = entity;

        if(entity.id != null) {
        	vm.createMode = false;
        }
        else {
        	vm.createMode = true;
        }
        
        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });
        
        function save () {
            vm.isSaving = true;
            if (vm.clothingType.id !== null) {
                ClothingType.update(vm.clothingType, onSaveSuccess, onSaveError);
            } else {
            	ClothingType.save(vm.clothingType, onSaveSuccess, onSaveError);
            }
        }
        
        function onSaveSuccess (result) {
            $scope.$emit('walterApp:clothingTypeUpdate', result);
            $state.go(previousState.name);
        }

        function onSaveError () {
            vm.isSaving = false;
        }
    }
})();
