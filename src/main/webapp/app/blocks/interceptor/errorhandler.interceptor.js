(function() {
    'use strict';

    angular
        .module('walterApp')
        .factory('errorHandlerInterceptor', errorHandlerInterceptor);

    errorHandlerInterceptor.$inject = ['$q', '$rootScope', '$location', '$window'];

    function errorHandlerInterceptor ($q, $rootScope, $location, $window) {
        var service = {
            responseError: responseError
        };

        return service;

        function responseError (response) {
            if (!(response.status === 401 && (response.data === '' || (response.data.path && response.data.path.indexOf('/api/account') === 0 )))) {
                $rootScope.$emit('walterApp.httpError', response);
            }
            if (response.status === 404) {
                //$location.path('/404');
              //  $window.location.href = '/404.html';
            }
            return $q.reject(response);
        }
    }
})();
