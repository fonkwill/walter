(function() {
    'use strict';

    angular
        .module('walterApp')
        .constant('paginationConstants', {
            'itemsPerPage': 50
        });
})();
