(function() {
    'use strict';

    var jhiAlert = {
        template: '<div class="alerts" ng-cloak="">' +
                        '<div ng-repeat="alert in $ctrl.alerts" ng-class="[alert.position, {\'toast\': alert.toast}]">' +
                            '<div uib-alert ng-cloak="" ng-class="\'alert-\' + (alert.type)" close="alert.close($ctrl.alerts)"><pre ng-bind-html="alert.msg"></pre></div>' +
                        '</div>' +
                  '</div>',
        controller: jhiAlertController
    };

    angular
        .module('walterApp')
        .component('jhiAlert', jhiAlert);

    jhiAlertController.$inject = ['$scope', 'AlertService'];

    function jhiAlertController($scope, AlertService) {
        var vm = this;

        vm.alerts = AlertService.get();
        $scope.$on('$destroy', function () {
            vm.alerts = [];
        });
    }
})();
