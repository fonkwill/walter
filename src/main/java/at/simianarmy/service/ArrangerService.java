package at.simianarmy.service;

import at.simianarmy.domain.Arranger;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.service.dto.ArrangerDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ArrangerMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Arranger.
 */
@Service
@Transactional
public class ArrangerService {

	private final Logger log = LoggerFactory.getLogger(ArrangerService.class);

	@Inject
	private ArrangerRepository arrangerRepository;

	@Inject
	private ArrangerMapper arrangerMapper;

	/**
	 * Save a arranger.
	 *
	 * @param arrangerDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ArrangerDTO save(ArrangerDTO arrangerDTO) {
		log.debug("Request to save Arranger : {}", arrangerDTO);
		Arranger arranger = arrangerMapper.arrangerDTOToArranger(arrangerDTO);
		arranger = arrangerRepository.save(arranger);
		ArrangerDTO result = arrangerMapper.arrangerToArrangerDTO(arranger);
		return result;
	}

	/**
	 * Get all the arrangers.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ArrangerDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Arrangers");
		Page<Arranger> result = arrangerRepository.findAll(pageable);
		return result.map(arranger -> arrangerMapper.arrangerToArrangerDTO(arranger));
	}

	/**
	 * Get one arranger by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ArrangerDTO findOne(Long id) {
		log.debug("Request to get Arranger : {}", id);
		Arranger arranger = arrangerRepository.findOneWithEagerRelationships(id);
		ArrangerDTO arrangerDTO = arrangerMapper.arrangerToArrangerDTO(arranger);
		return arrangerDTO;
	}

	/**
	 * Delete the arranger by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Arranger : {}", id);

		Arranger existing = this.arrangerRepository.findById(id).orElse(null);

		if (existing != null && existing.getCompositions().size() != 0) {
			throw new ServiceValidationException(ServiceErrorConstants.arrangerDataIntegrityViolation, null);
		}

		arrangerRepository.deleteById(id);

	}

	/**
	 * Get all arrangers for a Composition.
	 * 
	 * @param compositionId
	 *            id of the Composition
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ArrangerDTO> findByComposition(Long compositionId, Pageable pageable) {
		log.debug("Request to get Arrangers for a Composition");
		Page<Arranger> result = arrangerRepository.findByComposition(compositionId, pageable);
		return result.map(arranger -> arrangerMapper.arrangerToArrangerDTO(arranger));
	}
}
