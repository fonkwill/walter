package at.simianarmy.service;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.service.dto.ArrangerDTO;
import at.simianarmy.service.dto.ComposerDTO;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ArrangerMapper;
import at.simianarmy.service.mapper.ComposerMapper;
import at.simianarmy.service.mapper.CompositionMapper;
import at.simianarmy.service.specification.CompositionSpecification;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Composition.
 */
@Service
@Transactional
public class CompositionService {

	private static final String CASHBOOK_ENTRY_TEXT = "Notenankauf: ";
	private static final BigDecimal AMOUNT_ZERO = new BigDecimal(0);

	private final Logger log = LoggerFactory.getLogger(CompositionService.class);

	@Inject
	private CompositionRepository compositionRepository;

	@Inject
	private ComposerRepository composerRepository;

	@Inject
	private CompositionMapper compositionMapper;

	@Inject
	private ComposerMapper composerMapper;

	@Inject
	private ArrangerRepository arrangerRepository;

	@Inject
	private ArrangerMapper arrangerMapper;

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private CompositionSpecification compositionSpecification;

	@Inject
  private CustomizationService customizationService;

	/**
	 * Save a composition.
	 *
	 * @param compositionDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CompositionDTO save(CompositionDTO compositionDTO) {
		log.debug("Request to save Composition : {}", compositionDTO);

		CashbookEntry cashbookEntry = null;

		if (compositionDTO.getId() != null) {
			Composition existing = compositionRepository.findById(compositionDTO.getId()).orElse(null);
			if (existing.getNr() != compositionDTO.getNr()) {
				throw new ServiceValidationException(ServiceErrorConstants.compositionServiceNrNotWriteable, null);
			}

			CashbookEntry existingCashbookEntry = existing.getCashbookEntry();

			if (existingCashbookEntry == null) {
				// Create new Cashbook Entry if Amount is not null
				if (compositionDTO.getCashbookEntryAmount() != null
						&& !compositionDTO.getCashbookEntryAmount().equals(AMOUNT_ZERO)) {
					cashbookEntry = this.createCashbookEntryForComposition(compositionDTO);
				}
			} else {
				if (compositionDTO.getCashbookEntryAmount() != null) {
					cashbookEntry = existingCashbookEntry;
					cashbookEntry.setAmount(compositionDTO.getCashbookEntryAmount());
					cashbookEntry = this.cashbookEntryRepository.saveAndFlush(cashbookEntry);
				} else {
					throw new ServiceValidationException(
							ServiceErrorConstants.compositionServiceExistingCashbookEntryAmountNotNull, null);
				}
			}
		} else {
			// Create new Cashbook Entry if Amount is not null
			if (compositionDTO.getCashbookEntryAmount() != null
					&& !compositionDTO.getCashbookEntryAmount().equals(AMOUNT_ZERO)) {
				cashbookEntry = this.createCashbookEntryForComposition(compositionDTO);
			}
		}

		Composition composition = compositionMapper.compositionDTOToComposition(compositionDTO);
		composition.setCashbookEntry(cashbookEntry);
		composition = compositionRepository.save(composition);
		CompositionDTO result = compositionMapper.compositionToCompositionDTO(composition);
		return result;
	}

	/**
	 * Get all the compositions.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CompositionDTO> findAll(Integer nr, Long colorCodeId, String title, String genre, String composer,
			String arranger, String publisher, String orderingCompany, Long musicBookId, Boolean isActive,
			Pageable pageable) {
		log.debug("Request to get Compositions by specified Criteria");

		Page<Composition> result = compositionRepository.findAll(
				this.compositionSpecification.init().hasColorCode(colorCodeId).hasNr(nr).hasTitle(title).hasGenre(genre)
						.hasPublisher(publisher).hasOrderingCompany(orderingCompany).hasMusicBook(musicBookId)
						.hasComposer(composer).hasArranger(arranger).isActive(isActive).build(),
				pageable);

		return result.map(composition -> compositionMapper.compositionToCompositionDTO(composition));
	}

	/**
	 * Get one composition by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CompositionDTO findOne(Long id) {
		log.debug("Request to get Composition : {}", id);
		Composition composition = compositionRepository.findOneWithEagerRelationships(id);
		CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);
		return compositionDTO;
	}

	/**
	 * Delete the composition by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Composition : {}", id);

		// Der zugehörige Kassabucheintrag muss gelöscht werden!
		Composition composition = this.compositionRepository.findById(id).orElse(null);
		CashbookEntry cashbookEntry = composition.getCashbookEntry();

		if (cashbookEntry != null) {
			this.cashbookEntryRepository.deleteById(cashbookEntry.getId());
		}

		// Bei den Komponisten und Arrangeuren muss die Komposition gelöscht werden
		for (Composer composer : composition.getComposers()) {
			composer.removeCompositions(composition);
		}

		for (Arranger arranger : composition.getArrangers()) {
			arranger.removeCompositions(composition);
		}

		compositionRepository.deleteById(id);
	}

	/**
	 * Add a Composer to a Composition.
	 * 
	 * @param id
	 *            the id of the Composition
	 * @param composerDTO
	 *            the Composer to add
	 * @return the Composition
	 */
	public CompositionDTO addComposer(Long id, ComposerDTO composerDTO) {
		log.debug("Request to add Composer to Composition : {}", id, composerDTO);
		Composition composition = compositionRepository.findById(id).orElse(null);

		Composer composer = composerMapper.composerDTOToComposer(composerDTO);

		if (composer != null && !composition.getComposers().contains(composer)) {
			composer.addCompositions(composition);
			composerRepository.saveAndFlush(composer);
		}

		CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);
		return compositionDTO;
	}

	/**
	 * Remove the Composer from the Composition.
	 *
	 * @param id
	 *            the id of the entity
	 * @param composerId
	 *            the id of the composer to remove
	 */
	public void removeComposer(Long id, Long composerId) {
		log.debug("Request to remove Composer from Composition : {}", id, composerId);

		Composition composition = compositionRepository.findById(id).orElse(null);

		if (composition != null) {
			Composer composer = composerRepository.findById(composerId).orElse(null);
			if (composer != null) {
				composer.removeCompositions(composition);
				composerRepository.saveAndFlush(composer);
			}
		}
	}

	/**
	 * Add an Arranger to a Composition.
	 * 
	 * @param id
	 *            the id of the Composition
	 * @param arrangerDTO
	 *            the Arranger to add
	 * @return the Composition
	 */
	public CompositionDTO addArranger(Long id, ArrangerDTO arrangerDTO) {
		log.debug("Request to add Arranger to Composition : {}", id, arrangerDTO);
		Composition composition = compositionRepository.findById(id).orElse(null);

		Arranger arranger = arrangerMapper.arrangerDTOToArranger(arrangerDTO);

		if (composition != null && !composition.getArrangers().contains(arranger)) {
			arranger.addCompositions(composition);
			arrangerRepository.saveAndFlush(arranger);
		}

		CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);
		return compositionDTO;
	}

	/**
	 * Remove the Arranger from the Composition.
	 *
	 * @param id
	 *            the id of the entity
	 * @param arrangerId
	 *            the id of the arranger to remove
	 */
	public void removeArranger(Long id, Long arrangerId) {
		log.debug("Request to remove Arranger from Composition : {}", id, arrangerId);

		Composition composition = compositionRepository.findById(id).orElse(null);

		if (composition != null) {
			Arranger arranger = arrangerRepository.findById(arrangerId).orElse(null);
			if (arranger != null) {
				arranger.removeCompositions(composition);
				arrangerRepository.saveAndFlush(arranger);
			}
		}
	}

	/**
	 * Get all compositions for an Appointment.
	 * 
	 * @param officialAppointmentId
	 *            id of the officialAppointment
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CompositionDTO> findByOfficialAppointment(Long officialAppointmentId, Pageable pageable) {
		log.debug("Request to get Compositions for an Appointment");
		Page<Composition> result = compositionRepository.findByOfficialAppointment(officialAppointmentId, pageable);
		return result.map(composition -> compositionMapper.compositionToCompositionDTO(composition));
	}

	private CashbookEntry createCashbookEntryForComposition(CompositionDTO compositionDTO) {
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setTitle(CASHBOOK_ENTRY_TEXT + compositionDTO.getTitle());
		cashbookEntry.setAmount(compositionDTO.getCashbookEntryAmount());
		cashbookEntry.setType(CashbookEntryType.SPENDING);
		cashbookEntry.setDate(LocalDate.now());
		cashbookEntry.setCashbookAccount(this.customizationService.getDefaultCashbookAccount());
		cashbookEntry = this.cashbookEntryRepository.saveAndFlush(cashbookEntry);
		return cashbookEntry;
	}
}
