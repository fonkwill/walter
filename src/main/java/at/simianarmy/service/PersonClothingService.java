package at.simianarmy.service;

import at.simianarmy.domain.PersonClothing;
import at.simianarmy.repository.PersonClothingRepository;
import at.simianarmy.service.dto.PersonClothingDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonClothingMapper;
import at.simianarmy.service.specification.PersonSpecifications;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing PersonClothing.
 */
@Service
@Transactional
public class PersonClothingService {

	private final Logger log = LoggerFactory.getLogger(PersonClothingService.class);

	@Inject
	private PersonClothingRepository personClothingRepository;

	@Inject
	private PersonClothingMapper personClothingMapper;

	/**
	 * Save a personClothing.
	 *
	 * @param personClothingDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonClothingDTO save(PersonClothingDTO personClothingDTO) {
		log.debug("Request to save PersonClothing : {}", personClothingDTO);
		PersonClothing personClothing = personClothingMapper.personClothingDTOToPersonClothing(personClothingDTO);
		if (personClothingDTO.getBeginDate() != null && personClothingDTO.getEndDate() != null) {
			if (personClothingDTO.getBeginDate().isAfter(personClothingDTO.getEndDate())) {
				throw new ServiceValidationException(ServiceErrorConstants.personClothingEndBeforeBegin, null);
			}
		}
		personClothing = personClothingRepository.save(personClothing);
		PersonClothingDTO result = personClothingMapper.personClothingToPersonClothingDTO(personClothing);
		return result;
	}

	/**
	 * Get all the personClothings.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonClothingDTO> findAll(Long personId, Pageable pageable) {
		log.debug("Request to get all PersonClothings");

		Specification<PersonClothing> searchSpec = Specification
				.where(PersonSpecifications.hasPersonIdFromPersonClothing(personId));

		Page<PersonClothing> result = personClothingRepository.findAll(searchSpec, pageable);
		return result.map(personClothing -> personClothingMapper.personClothingToPersonClothingDTO(personClothing));
	}

	/**
	 * Get one personClothing by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonClothingDTO findOne(Long id) {
		log.debug("Request to get PersonClothing : {}", id);
		PersonClothing personClothing = personClothingRepository.findById(id).orElse(null);
		PersonClothingDTO personClothingDTO = personClothingMapper.personClothingToPersonClothingDTO(personClothing);
		return personClothingDTO;
	}

	/**
	 * Delete the personClothing by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PersonClothing : {}", id);
		personClothingRepository.deleteById(id);
	}
}
