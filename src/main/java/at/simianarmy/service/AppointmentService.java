package at.simianarmy.service;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.domain.Person;
import at.simianarmy.export.akm.AkmReportFactory;
import at.simianarmy.export.akm.IncompleteAppointmentException;
import at.simianarmy.export.akm.InvalidAssociationDataException;
import at.simianarmy.export.akm.xml.AkmReport;
import at.simianarmy.repository.AppointmentRepository;
import at.simianarmy.repository.OfficialAppointmentRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.AppointmentDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.AppointmentMapper;
import at.simianarmy.service.mapper.PersonMapper;
import at.simianarmy.service.specification.AppointmentSpecification;

import biweekly.ICalendar;
import biweekly.component.VEvent;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Appointment.
 */
@Service
@Transactional
public class AppointmentService {

	private final Logger log = LoggerFactory.getLogger(AppointmentService.class);

	@Inject
	private AppointmentRepository appointmentRepository;

	@Inject
	private OfficialAppointmentRepository officialAppointmentRepository;

	@Inject
	private PersonRepository personRepository;

	@Inject
	private AppointmentMapper appointmentMapper;

	@Inject
	private PersonMapper personMapper;

	@Inject
	private AppointmentSpecification appointmentSpecification;

	@Inject
	private AkmReportFactory akmReportFactory;

	/**
	 * Save an appointment.
	 *
	 * @param appointmentDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Transactional
	public AppointmentDTO save(AppointmentDTO appointmentDTO) {
		log.debug("Request to save Appointment : {}", appointmentDTO);
		Appointment appointment = appointmentMapper.appointmentDTOToAppointment(appointmentDTO);

		if (appointment.getEndDate().isBefore(appointment.getBeginDate())) {
			throw new ServiceValidationException(ServiceErrorConstants.appointmentEndBeforeBegin, null);
		}

		appointment = appointmentRepository.saveAndFlush(appointment);

		if (appointmentDTO.getIsOfficial() != null && appointmentDTO.getIsOfficial()) {
			if (appointmentDTO.getOrganizerName() == null || appointmentDTO.getOrganizerAddress() == null
					|| appointmentDTO.getIsHeadQuota() == null) {
				throw new ServiceValidationException(ServiceErrorConstants.appointmentOfficialAppointmentNotNull, null);
			}

			OfficialAppointment officialAppointment = new OfficialAppointment();
			if (appointmentDTO.getOfficialAppointmentId() != null) {
				officialAppointment = this.officialAppointmentRepository
						.findById(appointmentDTO.getOfficialAppointmentId()).orElse(null);
			}
			officialAppointment.setAppointment(appointment);
			officialAppointment.setOrganizerName(appointmentDTO.getOrganizerName());
			officialAppointment.setOrganizerAddress(appointmentDTO.getOrganizerAddress());
			officialAppointment.setIsHeadQuota(appointmentDTO.getIsHeadQuota());
			this.officialAppointmentRepository.saveAndFlush(officialAppointment);

			appointment.setOfficialAppointment(officialAppointment);
			appointment = appointmentRepository.saveAndFlush(appointment);
		}

		AppointmentDTO result = appointmentMapper.appointmentToAppointmentDTO(appointment);
		return result;
	}

	/**
	 * Get all the appointments, optional parameters are used as filters.
	 *
	 * @param appointmentTypeId
	 *            id of appointment type
	 * @param searchString
	 *            appointment name or description to look for
	 * @param earliestBegin
	 *            earliest begin of the appointments
	 * @param latestEnd
	 *            latest end of the appointments
	 * @param location
	 *            location or address to look for
	 * @param compositionName
	 *            the name of a composition played during the appointments
	 * @param isOfficial
	 *            filter for official (true) or unofficial (false) appointments
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AppointmentDTO> findAll(Long appointmentTypeId, String searchString, LocalDate earliestBegin,
			LocalDate latestEnd, String location, String compositionName, Boolean isOfficial, Pageable pageable) {
		log.debug("Request to get all Appointments matching the specified Criteria");
		Page<Appointment> result = appointmentRepository.findAll(
				this.appointmentSpecification.init().hasAppointmentType(appointmentTypeId)
						.hasNameOrDescription(searchString).doesNotBeginBefore(earliestBegin).doesNotEndAfter(latestEnd)
						.hasLocation(location).hasComposition(compositionName).isOfficial(isOfficial).build(),
				pageable);
		return result.map(appointment -> appointmentMapper.appointmentToAppointmentDTO(appointment));
	}

	/**
	 * Get all the appointments as ical, optional parameters are used as filters.
	 *
	 * @param appointmentTypeId
	 *            id of appointment type
	 * @param searchString
	 *            appointment name or description to look for
	 * @param earliestBegin
	 *            earliest begin of the appointments
	 * @param latestEnd
	 *            latest end of the appointments
	 * @param location
	 *            location or address to look for
	 * @param compositionName
	 *            the name of a composition played during the appointments
	 * @param isOfficial
	 *            filter for official (true) or unofficial (false) appointments
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public ICalendar getAppointmentsAsICal(Long appointmentTypeId, String searchString, LocalDate earliestBegin,
			LocalDate latestEnd, String location, String compositionName, Boolean isOfficial) {

		List<Appointment> result = appointmentRepository
				.findAll(this.appointmentSpecification.init().hasAppointmentType(appointmentTypeId)
						.hasNameOrDescription(searchString).doesNotBeginBefore(earliestBegin).doesNotEndAfter(latestEnd)
						.hasLocation(location).hasComposition(compositionName).isOfficial(isOfficial).build());

		ICalendar ical = new ICalendar();

		for (Appointment appointment : result) {
			VEvent event = new VEvent();
			event.setUid(appointment.getId().toString());
			event.setSummary(appointment.getName());
			event.setDescription(appointment.getDescription());
			event.setDateStart(Date.from(appointment.getBeginDate().toInstant()));
			event.setDateEnd(Date.from(appointment.getEndDate().toInstant()));
			event.setLocation(
					appointment.getStreetAddress() + "; " + appointment.getPostalCode() + " " + appointment.getCity());
			if (appointment.getType() != null) {
				event.addCategories(appointment.getType().getName());
			}
			ical.addEvent(event);
		}

		return ical;
	}

	/**
	 * Creates an XML report for AKM for all appointments from beginDate to endDate.
	 * 
	 * @param beginDate
	 *            begin of report
	 * @param endDate
	 *            end of report
	 * @return the XML file
	 */
	@Transactional(readOnly = true)
	public Resource getAkmReport(LocalDate beginDate, LocalDate endDate) {

		List<Appointment> appointments = appointmentRepository.findAll(this.appointmentSpecification.init()
				.doesNotBeginBefore(beginDate).doesNotEndAfter(endDate).isOfficial(true).build());

		try {
			JAXBContext jc = JAXBContext.newInstance(AkmReport.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();

			// workaround for getting rid of standalone="yes"
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
			xmlStream.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n".getBytes());

			
			AkmReport akmReport = akmReportFactory.createAkmReport(new HashSet<>(appointments));

			marshaller.marshal(akmReport, xmlStream);

			return new ByteArrayResource(xmlStream.toByteArray());

		} catch (IOException exc) {
			String[] params = new String[] { exc.getLocalizedMessage() };
			throw new ServiceValidationException(ServiceErrorConstants.appointmentOtherError, params);
		} catch (IncompleteAppointmentException exc) {
			String[] params = new String[] { exc.getLocalizedMessage() };
			throw new ServiceValidationException(ServiceErrorConstants.appointmentIncompleteAppointment, params);
		} catch (InvalidAssociationDataException exc) {
			throw new ServiceValidationException(ServiceErrorConstants.appointmentInvalidAssociationData, null);
		} catch (JAXBException exc) {
			String[] params = new String[] { exc.getLocalizedMessage() };
			throw new ServiceValidationException(ServiceErrorConstants.appointmentOtherError, params);
		}
	}

	/**
	 * Get all official appointments between earliestBegin and endBefore. Used for
	 * AKM report
	 *
	 * @param earliestBegin
	 *            earliest begin of official appointments to look for
	 * @param endBefore
	 *            official appointments to look for must end before this date
	 * @return the list of official appointments between earliestBegin and endBefore
	 */
	@Transactional(readOnly = true)
	public List<AppointmentDTO> findOfficialAppointmentsByDate(LocalDate earliestBegin, LocalDate endBefore) {
		return appointmentRepository
				.findAll(this.appointmentSpecification.init().doesNotBeginBefore(earliestBegin)
						.doesNotEndAfter(endBefore).isOfficial(true).build())
				.stream().map(appointment -> appointmentMapper.appointmentToAppointmentDTO(appointment))
				.collect(Collectors.toList());
	}

	/**
	 * Get one appointment by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AppointmentDTO findOne(Long id) {
		log.debug("Request to get Appointment : {}", id);
		Appointment appointment = appointmentRepository.findOneWithEagerRelationships(id);
		AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);
		return appointmentDTO;
	}

	/**
	 * Delete the appointment by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Appointment : {}", id);
		appointmentRepository.deleteById(id);
	}

	/**
	 * Add a Person to an Appointment.
	 *
	 * @param id
	 *            the id of the Appointment
	 * @param personDTO
	 *            the Person to add
	 * @return the Appointment
	 */
	public AppointmentDTO addPerson(Long id, PersonDTO personDTO) {
		log.debug("Request to add Person to Appointment : {}", id, personDTO);
		Appointment appointment = appointmentRepository.findById(id).orElse(null);

		Person person = personMapper.personDTOToPerson(personDTO);

		if (appointment != null && !appointment.getPersons().contains(person)) {
			person.addAppointments(appointment);
			personRepository.saveAndFlush(person);
		}

		AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);
		return appointmentDTO;
	}

	/**
	 * Remove the Person from the Appointment.
	 *
	 * @param id
	 *            the id of the entity
	 * @param personId
	 *            the id of the person to remove
	 */
	public void removePerson(Long id, Long personId) {
		log.debug("Request to remove Person from Appointment : {}", id, personId);

		Appointment appointment = appointmentRepository.findById(id).orElse(null);

		if (appointment != null) {
			Person person = personRepository.findById(personId).orElse(null);
			if (person != null) {
				person.removeAppointments(appointment);
				personRepository.saveAndFlush(person);
			}
		}
	}
}
