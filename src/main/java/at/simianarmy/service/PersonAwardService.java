package at.simianarmy.service;

import at.simianarmy.domain.PersonAward;
import at.simianarmy.repository.PersonAwardRepository;
import at.simianarmy.service.dto.PersonAwardDTO;
import at.simianarmy.service.mapper.PersonAwardMapper;
import at.simianarmy.service.specification.PersonSpecifications;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing PersonAward.
 */
@Service
@Transactional
public class PersonAwardService {

	private final Logger log = LoggerFactory.getLogger(PersonAwardService.class);

	@Inject
	private PersonAwardRepository personAwardRepository;

	@Inject
	private PersonAwardMapper personAwardMapper;

	/**
	 * Save a personAward.
	 *
	 * @param personAwardDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonAwardDTO save(PersonAwardDTO personAwardDTO) {
		log.debug("Request to save PersonAward : {}", personAwardDTO);
		PersonAward personAward = personAwardMapper.personAwardDTOToPersonAward(personAwardDTO);
		personAward = personAwardRepository.save(personAward);
		PersonAwardDTO result = personAwardMapper.personAwardToPersonAwardDTO(personAward);
		return result;
	}

	/**
	 * Get all the personAwards.
	 * 
	 * @param personId
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonAwardDTO> findAll(Long personId, Pageable pageable) {
		log.debug("Request to get all PersonAwards");
		Specification<PersonAward> searchSpec = Specification
				.where(PersonSpecifications.hasPersonIdFromPersonAward(personId));

		Page<PersonAward> result = personAwardRepository.findAll(searchSpec, pageable);
		return result.map(personAward -> personAwardMapper.personAwardToPersonAwardDTO(personAward));
	}

	/**
	 * Get all the personAwards for an Award.
	 * 
	 * @param awardId
	 *            the Award, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonAwardDTO> findByAward(Long awardId, Pageable pageable) {
		log.debug("Request to get all PersonHonors for an Award");
		Page<PersonAward> result = personAwardRepository.findByAward(awardId, pageable);
		return result.map(personAward -> personAwardMapper.personAwardToPersonAwardDTO(personAward));
	}

	/**
	 * Get one personAward by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonAwardDTO findOne(Long id) {
		log.debug("Request to get PersonAward : {}", id);
		PersonAward personAward = personAwardRepository.findById(id).orElse(null);
		PersonAwardDTO personAwardDTO = personAwardMapper.personAwardToPersonAwardDTO(personAward);
		return personAwardDTO;
	}

	/**
	 * Delete the personAward by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PersonAward : {}", id);
		personAwardRepository.deleteById(id);
	}
}
