package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentService;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.InstrumentRepository;
import at.simianarmy.repository.InstrumentServiceRepository;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.dto.InstrumentServiceDTO;
import at.simianarmy.service.mapper.CashbookEntryMapper;
import at.simianarmy.service.mapper.InstrumentServiceMapper;
import at.simianarmy.service.exception.ServiceValidationException;

import java.time.LocalDate;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing InstrumentService.
 */
@Service
@Transactional
public class InstrumentServiceService {

	private final Logger log = LoggerFactory.getLogger(InstrumentServiceService.class);

	@Inject
	private InstrumentServiceRepository instrumentServiceRepository;

	@Inject
	private InstrumentRepository instrumentRepository;

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private InstrumentServiceMapper instrumentServiceMapper;

	@Inject
	private CashbookEntryMapper cashbookEntryMapper;

	@Inject
  private CustomizationService customizationService;

	/**
	 * Save a instrumentService.
	 *
	 * @param instrumentServiceDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public InstrumentServiceDTO save(InstrumentServiceDTO instrumentServiceDTO) {
		log.debug("Request to save InstrumentService : {}", instrumentServiceDTO);
		InstrumentService instrumentService = instrumentServiceMapper
				.instrumentServiceDTOToInstrumentService(instrumentServiceDTO);
		LocalDate instrumentServiceDate = instrumentService.getDate();
		LocalDate today = LocalDate.now();
		LocalDate instrumentDate = null;
		Instrument instrument = null;
		if (instrumentService.getInstrument() != null) {
			instrument = instrumentRepository.findById(instrumentService.getInstrument().getId()).orElse(null);
			if (instrument.getPurchaseDate() != null) {
				instrumentDate = instrument.getPurchaseDate();
			}
		}
		if (instrumentService.getInstrument() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentServiceNoInstrument, null);
		}
		if (instrumentServiceDate.isAfter(today)) {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentServiceDateInFuture, null);
		} else if (instrumentDate != null && instrumentServiceDate.isBefore(instrumentDate)) {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentServiceDateBeforeInstrumentDate, null);
		} else {
			if (instrumentService.getCashbookEntry() == null && instrumentServiceDTO.getCashbookEntryAmount() != null
					&& instrumentServiceDTO.getCashbookEntryAmount().signum() != 0) { // make new Entry
				CashbookEntryDTO cashbookEntryDTO = new CashbookEntryDTO();
				cashbookEntryDTO.setAmount(instrumentServiceDTO.getCashbookEntryAmount());
				cashbookEntryDTO.setDate(instrumentServiceDate);
				String title = "Instrumenten-Service für Instrument '" + instrument.getName() + "'";
				cashbookEntryDTO.setTitle(title);
				CashbookEntryType type = CashbookEntryType.SPENDING;
				cashbookEntryDTO.setType(type);
				CashbookEntry cashbookEntry = cashbookEntryMapper.cashbookEntryDTOToCashbookEntry(cashbookEntryDTO);
				cashbookEntry.setCashbookAccount(this.customizationService.getDefaultCashbookAccount());
				cashbookEntry = cashbookEntryRepository.save(cashbookEntry);
				instrumentService.setCashbookEntry(cashbookEntry);
			} else if (instrumentService.getCashbookEntry() != null) {
				CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
						.cashbookEntryToCashbookEntryDTO(instrumentService.getCashbookEntry());
				cashbookEntryDTO.setAmount(instrumentServiceDTO.getCashbookEntryAmount());
				cashbookEntryDTO.setDate(instrumentServiceDate);
				String title = "Instrumenten-Service für Instrument '" + instrument.getName() + "'";
				cashbookEntryDTO.setTitle(title);
				CashbookEntryType type = CashbookEntryType.SPENDING;
				cashbookEntryDTO.setType(type);
				CashbookEntry cashbookEntry = cashbookEntryMapper.cashbookEntryDTOToCashbookEntry(cashbookEntryDTO);
				cashbookEntry = cashbookEntryRepository.save(cashbookEntry);
				instrumentService.setCashbookEntry(cashbookEntry);
			}
			instrumentService = instrumentServiceRepository.save(instrumentService);
			InstrumentServiceDTO result = instrumentServiceMapper
					.instrumentServiceToInstrumentServiceDTO(instrumentService);
			return result;
		}
	}

	/**
	 * Get all the instrumentServices.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<InstrumentServiceDTO> findAll(Pageable pageable) {
		log.debug("Request to get all InstrumentServices");
		Page<InstrumentService> result = instrumentServiceRepository.findAll(pageable);
		return result.map(instrumentService -> instrumentServiceMapper
				.instrumentServiceToInstrumentServiceDTO(instrumentService));
	}

	/**
	 * Get all the instrumentServices for an Instrument.
	 * 
	 * @param instrumentId
	 *            the Instrument, which we want to retrieve services from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<InstrumentServiceDTO> findByInstrument(Long instrumentId, Pageable pageable) {
		log.debug("Request to get all InstrumentServices for an Instrument");
		Page<InstrumentService> result = instrumentServiceRepository.findByInstrument(instrumentId, pageable);
		return result.map(instrumentService -> instrumentServiceMapper
				.instrumentServiceToInstrumentServiceDTO(instrumentService));
	}

	/**
	 * Get one instrumentService by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public InstrumentServiceDTO findOne(Long id) {
		log.debug("Request to get InstrumentService : {}", id);
		InstrumentService instrumentService = instrumentServiceRepository.findById(id).orElse(null);
		InstrumentServiceDTO instrumentServiceDTO = instrumentServiceMapper
				.instrumentServiceToInstrumentServiceDTO(instrumentService);
		return instrumentServiceDTO;
	}

	/**
	 * Delete the instrumentService by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete InstrumentService : {}", id);
		Long entryId = null;
		if (instrumentServiceRepository.findById(id).orElse(null).getCashbookEntry() != null) {
			entryId = instrumentServiceRepository.findById(id).orElse(null).getCashbookEntry().getId();
		}
		instrumentServiceRepository.deleteById(id);
		if (entryId != null) {
			cashbookEntryRepository.deleteById(entryId);
		}
	}
}
