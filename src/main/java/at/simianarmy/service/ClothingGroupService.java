package at.simianarmy.service;

import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.repository.ClothingRepository;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.service.dto.ClothingGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.specification.ClothingSpecification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Service Implementation for managing ClothingGroup.
 */
@Service
@Transactional
public class ClothingGroupService {

	private final Logger log = LoggerFactory.getLogger(ClothingGroupService.class);

	@Inject
	private ClothingRepository clothingRepository;

	@Inject
	private ClothingTypeRepository clothingTypeRepository;

	/**
	 * Save a clothingGroup.
	 *
	 * @param clothingGroupDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClothingGroupDTO save(ClothingGroupDTO clothingGroupDTO) {
		log.debug("Request to save ClothingGroup : {}", clothingGroupDTO);
		ClothingGroupDTO result = null;
		ClothingGroupDTO db = findOne(clothingGroupDTO.getCsize(), clothingGroupDTO.getTypeId());

		if (clothingGroupDTO.getQuantity() == 0) {
			clothingGroupDTO.setQuantity(0);
		}
		if (clothingGroupDTO.getQuantity() < db.getQuantity()) {
			clothingGroupDTO.setQuantity(db.getQuantity() - clothingGroupDTO.getQuantity());
			result = delete(clothingGroupDTO);

		} else if (clothingGroupDTO.getQuantity() > db.getQuantity()) {
			clothingGroupDTO.setQuantity(clothingGroupDTO.getQuantity() - db.getQuantity());
			if (clothingGroupDTO.getQuantity() != null) {
				Integer i = clothingGroupDTO.getQuantity();

				ClothingType clothingType = clothingTypeRepository.findById(clothingGroupDTO.getTypeId()).orElse(null);

				while (i != 0) {
					Clothing toSave = new Clothing();
					toSave.setSize(clothingGroupDTO.getCsize());
					toSave.setType(clothingType);
					clothingRepository.save(toSave);
					i = i - 1;

				}

				result = findOne(clothingGroupDTO.getCsize(), clothingGroupDTO.getTypeId());
			}
		}

		return result;
	}

	/**
	 * Get all the clothingGroups.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClothingGroupDTO> findAll(Pageable pageable) {
		log.debug("Request to get all ClothingGroups");

		List<ClothingGroupDTO> cgList = new ArrayList<>();

		List<Object[]> possibleValues = clothingRepository.getAllDistinctValues();
		for (Object[] o : possibleValues) {
			cgList.add(findOne((String) o[0], (Long) o[1]));
		}

		Page<ClothingGroupDTO> result = new PageImpl<>(cgList, pageable, cgList.size());
		return result;
	}

	/**
	 * Get one clothingGroup by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClothingGroupDTO findOne(String size, Long clothingTypeId) {
		log.debug("Request to get ClothingGroup : {}");

		ClothingType clothingType = clothingTypeRepository.findById(clothingTypeId).orElse(null);

		// currently available
		Specification<Clothing> currSpecs = ClothingSpecification.availableWith(LocalDate.now(), null, clothingType,
				size);
		Specification<Clothing> allSpecs = ClothingSpecification.allWith(clothingType, size);

		List<Clothing> currAvailable = clothingRepository.findAll(currSpecs);
		List<Clothing> allClothings = clothingRepository.findAll(allSpecs);

		ClothingGroupDTO result = new ClothingGroupDTO();
		result.setCsize(size);
		result.setTypeName(clothingType.getName());
		result.setQuantityAvailable(currAvailable.size());
		result.setTypeId(clothingType.getId());
		result.setQuantity(allClothings.size());

		return result;
	}

	/**
	 * Delete the clothingGroup by id.
	 *
	 * @param clothingGroup
	 *            the clothingGroup to delete
	 */
	@Transactional
	public ClothingGroupDTO delete(ClothingGroupDTO clothingGroup) {
		log.debug("Request to delete ClothingGroup : {}", clothingGroup);

		ClothingType clothingType = clothingTypeRepository.findById(clothingGroup.getTypeId()).orElse(null);

		Specification<Clothing> currSpecs = ClothingSpecification.allWithNoPerson(clothingType,
				clothingGroup.getCsize());
		List<Clothing> maxDeletable = clothingRepository.findAll(currSpecs);

		if (maxDeletable.size() < clothingGroup.getQuantity()) {
			throw new ServiceValidationException(ServiceErrorConstants.clothingGroupMaxDeletable,
					new String[] { new Integer(maxDeletable.size()).toString() });
		}
		int i = 0;
		for (Clothing c : maxDeletable) {
			log.debug("Request to delete Clothing: {}", c);

			clothingRepository.deleteById(c.getId());
			i++;
			if (i >= clothingGroup.getQuantity()) {
				break;
			}
		}
		// clothingRepository.flush();
		return findOne(clothingGroup.getCsize(), clothingGroup.getTypeId());
	}
}
