package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.dto.CashbookAccountTransferDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service for managing CashbookAccountTransfers
 */
@Service
@Transactional
public class CashbookAccountTransferService {

  private static final String TEXT_TO = "Kontenübertrag nach __ACCOUNT__";
  private static final String TEXT_FROM = "Kontenübertrag von __ACCOUNT__";
  private static final String ACCOUNT = "__ACCOUNT__";

  private final Logger log = LoggerFactory.getLogger(CashbookAccountTransferService.class);

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CustomizationService customizationService;

  /**
   * Submit a new CashbookAccountTransfer
   *
   * @param dto
   *          the CashbookAccountTransferDTO
   */
  public void save(CashbookAccountTransferDTO dto) {
    this.log.info("Request to conduct CashbookAccountTransfer: {}", dto);

    if (dto.getCashbookAccountToId().equals(dto.getCashbookAccountFromId())) {
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountTransferAccountFromAndToEqual, null);
    }

    CashbookAccount accountFrom = this.getCashbookAccountFromId(dto.getCashbookAccountFromId());
    CashbookAccount accountTo = this.getCashbookAccountFromId(dto.getCashbookAccountToId());
    BigDecimal amount = dto.getAmount();
    LocalDate date = LocalDate.now();

    String textFrom = TEXT_FROM.replace(ACCOUNT, accountFrom.getName());
    String textTo = TEXT_TO.replace(ACCOUNT, accountTo.getName());

    CashbookEntry entryFrom = new CashbookEntry();
    entryFrom.setCashbookCategory(this.customizationService.getDefaultCashbookCategoryForTransfers());
    entryFrom.setCashbookAccount(accountFrom);
    entryFrom.setType(CashbookEntryType.SPENDING);
    entryFrom.setAmount(amount);
    entryFrom.setDate(date);
    entryFrom.setTitle(textTo);

    CashbookEntry entryTo = new CashbookEntry();
    entryTo.setCashbookCategory(this.customizationService.getDefaultCashbookCategoryForTransfers());
    entryTo.setCashbookAccount(accountTo);
    entryTo.setType(CashbookEntryType.INCOME);
    entryTo.setAmount(amount);
    entryTo.setDate(date);
    entryTo.setTitle(textFrom);

    this.cashbookEntryRepository.save(entryFrom);
    this.cashbookEntryRepository.save(entryTo);
  }

  private CashbookAccount getCashbookAccountFromId(Long cashbookAccountId) {

    Optional<CashbookAccount> cashbookAccount = this.cashbookAccountRepository.findById(cashbookAccountId);

    if (!cashbookAccount.isPresent()) {
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountTransferCashbookAccountNotFound, null);
    } else {
      return cashbookAccount.get();
    }
  }

}
