package at.simianarmy.service;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.MailLogStatus;
import at.simianarmy.letter.SerialLetterFactory;
import at.simianarmy.letter.SerialLetterFactoryException;
import at.simianarmy.letter.service.SerialLetterService;
import at.simianarmy.letter.service.SerialLetterServiceFactory;
import at.simianarmy.letter.service.SerialLetterServiceFactoryException;
import at.simianarmy.letter.worker.SerialLetterWorker;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.repository.MailLogRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.dto.MailLetterDTO;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.LetterMapper;
import at.simianarmy.service.mapper.MailLetterMapper;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing MailLetter.
 */
@Service
@Transactional
public class MailLetterService {

  private final Logger log = LoggerFactory.getLogger(MailLetterService.class);

  @Inject
  private MailLetterMapper mailLetterMapper;

  @Inject
  private SerialLetterServiceFactory serialLetterServiceFactory;

  @Inject
  private SerialLetterFactory serialLetterFactory;

  @Inject
  private MailService mailService;

  @Inject
  private TemplateRepository templateRepository;

  @Inject
  private LetterRepository letterRepository;

  @Inject
  private MailLogRepository mailLogRepository;


  /**
   * Get one mail-letter by id.
   *
   * @param id
   *            the id of the entity
   * @return the entity
   */
  @Transactional(readOnly = true)
  public MailLetterDTO findOne(Long id) {
    log.debug("Request to get Letter : {}", id);
    Letter letter = letterRepository.findOneWithEagerRelationships(id);
    MailLetterDTO letterDTO = mailLetterMapper.letterToMailLetterDTO(letter);
    return letterDTO;
  }

  /**
   * Sends the given LetterDTO per Mail
   */

  @Transactional
  public void send(MailLetterDTO mailLetterDTO) {
    this.log.debug("Request to send letter per E-Mail {}", mailLetterDTO);
    Letter letter = this.mailLetterMapper.mailLetterDTOToLetter(mailLetterDTO);

    if (mailLetterDTO.isSaveInLetter()) {
      this.letterRepository.saveAndFlush(letter);
    }

    List<Person> personList = null;

    if (letter.getTemplate() != null && letter.getTemplate().getId() != null) {
      letter.setTemplate(templateRepository.findById(letter.getTemplate().getId()).orElse(null));
    }

    SerialLetterService serialLetterService;
    SerialLetterWorker serialLetterWorker;

    try {
      serialLetterService = this.serialLetterServiceFactory
        .createForLetter(letter);

      personList = serialLetterService.getRelevantPersonsForMailLetter(letter);

      if (personList.isEmpty()) {
        throw new ServiceValidationException(ServiceErrorConstants.serialLetterNoPersonsSelected, null);
      }
      serialLetterWorker = this.serialLetterFactory.createForLetter(letter);
    } catch (SerialLetterFactoryException | SerialLetterServiceFactoryException e) {
      throw new ServiceValidationException(e.getMessage(), null);
    }
    serialLetterWorker.setCompanies(new ArrayList<>());
    serialLetterWorker.setTemplatePageResource(serialLetterService.getTemplatePage(letter));

    this.sendLetters(serialLetterWorker, personList);
  }

  @Transactional
  @Async
  protected void sendLetters(SerialLetterWorker serialLetterWorker, List<Person> personList) {
    for (Person p : personList) {

      MailLog mailLog = new MailLog();
      mailLog.setDate(ZonedDateTime.now());
      mailLog.setEmail(p.getEmail());
      mailLog.setSubject(serialLetterWorker.getLetter().getSubject());
      mailLog.setMailLogStatus(MailLogStatus.WAITING);

      this.mailLogRepository.saveAndFlush(mailLog);

      serialLetterWorker.setPersons(new ArrayList<>(Arrays.asList(p)));

      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      BufferedOutputStream out = new BufferedOutputStream(bos);

      serialLetterWorker.setOut(out);

      try {

        serialLetterWorker.run();

        byte[] bytes = bos.toByteArray();

        ByteArrayDataSource attachment = new ByteArrayDataSource(bytes, "application/octet-stream");

        this.mailService
          .sendEmailWithAttachment(p.getEmail(), serialLetterWorker.getLetter().getSubject(),
            serialLetterWorker.getLetter().getMailText(), true, true, attachment,
            serialLetterWorker.getLetter().getAttachmentFileName(), mailLog);

      } catch(SerialLetterGenerationException e) {
        mailLog.setMailLogStatus(MailLogStatus.UNSUCCESSFUL);
        mailLog.setErrorMessage(ServiceErrorConstants.serialLetterNotGeneratable);
        this.mailLogRepository.saveAndFlush(mailLog);
      }
    }
  }

}
