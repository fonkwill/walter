package at.simianarmy.service;

import at.simianarmy.domain.PersonHonor;
import at.simianarmy.repository.PersonHonorRepository;
import at.simianarmy.service.dto.PersonHonorDTO;
import at.simianarmy.service.mapper.PersonHonorMapper;
import at.simianarmy.service.specification.PersonSpecifications;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing PersonHonor.
 */
@Service
@Transactional
public class PersonHonorService {

	private final Logger log = LoggerFactory.getLogger(PersonHonorService.class);

	@Inject
	private PersonHonorRepository personHonorRepository;

	@Inject
	private PersonHonorMapper personHonorMapper;

	/**
	 * Save a personHonor.
	 *
	 * @param personHonorDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonHonorDTO save(PersonHonorDTO personHonorDTO) {
		log.debug("Request to save PersonHonor : {}", personHonorDTO);
		PersonHonor personHonor = personHonorMapper.personHonorDTOToPersonHonor(personHonorDTO);
		personHonor = personHonorRepository.save(personHonor);
		PersonHonorDTO result = personHonorMapper.personHonorToPersonHonorDTO(personHonor);
		return result;
	}

	/**
	 * Get all the personHonors.
	 * 
	 * @param personId
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonHonorDTO> findAll(Long personId, Pageable pageable) {
		log.debug("Request to get all PersonHonors");
		Specification<PersonHonor> searchSpec = Specification
				.where(PersonSpecifications.hasPersonIdFromPersonHonor(personId));

		Page<PersonHonor> result = personHonorRepository.findAll(searchSpec, pageable);
		return result.map(personHonor -> personHonorMapper.personHonorToPersonHonorDTO(personHonor));
	}

	/**
	 * Get all the personHonors for a Honor.
	 * 
	 * @param honorId
	 *            the Honor, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonHonorDTO> findByHonor(Long honorId, Pageable pageable) {
		log.debug("Request to get all PersonHonors for a Honor");
		Page<PersonHonor> result = personHonorRepository.findByHonor(honorId, pageable);
		return result.map(personHonor -> personHonorMapper.personHonorToPersonHonorDTO(personHonor));
	}

	/**
	 * Get one personHonor by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonHonorDTO findOne(Long id) {
		log.debug("Request to get PersonHonor : {}", id);
		PersonHonor personHonor = personHonorRepository.findById(id).orElse(null);
		PersonHonorDTO personHonorDTO = personHonorMapper.personHonorToPersonHonorDTO(personHonor);
		return personHonorDTO;
	}

	/**
	 * Delete the personHonor by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PersonHonor : {}", id);
		personHonorRepository.deleteById(id);
	}
}
