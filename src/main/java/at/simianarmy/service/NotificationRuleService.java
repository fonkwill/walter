package at.simianarmy.service;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.notification.NotificationEntity;
import at.simianarmy.notification.NotificationEntityUtility;
import at.simianarmy.notification.NotificationException;
import at.simianarmy.repository.NotificationRepository;
import at.simianarmy.repository.NotificationRuleRepository;
import at.simianarmy.roles.repository.AuthorityRepository;
import at.simianarmy.roles.repository.RoleRepository;
import at.simianarmy.service.dto.NotificationRuleDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.NotificationRuleMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing NotificationRule.
 */
@Service
@Transactional
public class NotificationRuleService {

	private final Logger log = LoggerFactory.getLogger(NotificationRuleService.class);

	@Inject
	private NotificationRuleRepository notificationRuleRepository;

	@Inject
	private NotificationRepository notificationRepository;

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private NotificationRuleMapper notificationRuleMapper;

	@Inject
	private NotificationEntityUtility notificationEntityUtility;

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * get a list of valid entities for notifications.
	 * 
	 * @return the entity names
	 */
	public List<String> getValidNotificationEntities() {
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));
		Set<BeanDefinition> classes = provider.findCandidateComponents("at.simianarmy.domain");

		List<String> clazzes = new ArrayList<String>();
		try {
			for (BeanDefinition bean : classes) {
				Class<?> clazz = Class.forName(bean.getBeanClassName());

				if (Arrays.asList(clazz.getInterfaces()).contains(NotificationEntity.class)) {
					clazzes.add(clazz.getSimpleName());
				}
			}
		} catch (Exception exception) {
		}

		return clazzes;
	}

	/**
	 * get a list of valid authorities for notifications.
	 * 
	 * @return the authorities
	 */
	public List<String> getValidAuthorities() {
		return roleRepository.findAll().stream().map(role -> role.getName())
				.collect(Collectors.toList());
	}

	/**
	 * Save a notificationRule.
	 *
	 * @param notificationRuleDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public NotificationRuleDTO save(NotificationRuleDTO notificationRuleDTO) {
		log.debug("Request to save NotificationRule : {}", notificationRuleDTO);
		NotificationRule notificationRule = notificationRuleMapper
				.notificationRuleDTOToNotificationRule(notificationRuleDTO);

		this.checkNotificationRuleFieldValidity(notificationRule);

		notificationRule = notificationRuleRepository.save(notificationRule);
		NotificationRuleDTO result = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);
		return result;
	}

	/**
	 * check if a given notification rule is valid.
	 * 
	 * @param notificationRule
	 *            the rule
	 */
	private void checkNotificationRuleFieldValidity(NotificationRule notificationRule) {
		try {
			notificationEntityUtility.getEntityClassFromRule(notificationRule);
		} catch (NotificationException exception) {
			throw new ServiceValidationException(ServiceErrorConstants.notificationRuleUnknownClass, null);
		}

		try {
			notificationEntityUtility.getResultsFromRule(notificationRule);
		} catch (NotificationException exception) {
			String[] params = new String[] { exception.getMessage() };
			throw new ServiceValidationException(ServiceErrorConstants.notificationRuleWrongConditionSyntax, params);
		}
	}

	/**
	 * Get all the notificationRules.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<NotificationRuleDTO> findAll(Pageable pageable) {
		log.debug("Request to get all NotificationRules");
		Page<NotificationRule> result = notificationRuleRepository.findAll(pageable);
		return result.map(
				notificationRule -> notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule));
	}

	/**
	 * Get one notificationRule by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public NotificationRuleDTO findOne(Long id) {
		log.debug("Request to get NotificationRule : {}", id);
		NotificationRule notificationRule = notificationRuleRepository.findOneWithEagerRelationships(id);
		NotificationRuleDTO notificationRuleDTO = notificationRuleMapper
				.notificationRuleToNotificationRuleDTO(notificationRule);
		return notificationRuleDTO;
	}

	/**
	 * Delete the notificationRule by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete NotificationRule : {}", id);
		notificationRuleRepository.deleteById(id);
	}

  public void createNotifications() {
    log.debug("Request to create Notifications");

    List<NotificationRule> rules = notificationRuleRepository.findAllActive();
    for (NotificationRule rule : rules) {
      try {
        List<Notification> notifications = notificationEntityUtility.getNotificationsFromRule(rule);
        for (Notification notification : notifications) {
          List<Notification> notficationsInDatabase = notificationRepository
            .findByNotificationRuleAndEntityId(notification.getNotificationRule(),
              notification.getEntityId());
          if (notficationsInDatabase.isEmpty()) {
            log.debug("created notification for rule[{}] and entity[{}]",
              notification.getNotificationRule(), notification.getEntityId());
            notificationRepository.save(notification);
          }
        }
      } catch (NotificationException exception) {
        log.error("Failed to create Notification due to invalid Query: {}", exception.getMessage());
        continue;
      }
    }
  }
}
