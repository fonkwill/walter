package at.simianarmy.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the InstrumentService entity.
 */
public class InstrumentServiceDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 866403210069180037L;

	private Long id;

	@NotNull
	private LocalDate date;

	private String note;

	private Long cashbookEntryId;

	private BigDecimal cashbookEntryAmount;

	private Long instrumentId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public Long getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(Long instrumentId) {
		this.instrumentId = instrumentId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		InstrumentServiceDTO instrumentServiceDTO = (InstrumentServiceDTO) o;

		if (!Objects.equals(id, instrumentServiceDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "InstrumentServiceDTO{" + "id=" + id + ", date='" + date + "'" + ", note='" + note + "'" + '}';
	}

	public BigDecimal getCashbookEntryAmount() {
		return cashbookEntryAmount;
	}

	public void setCashbookEntryAmount(BigDecimal cashbookEntryAmount) {
		this.cashbookEntryAmount = cashbookEntryAmount;
	}
}
