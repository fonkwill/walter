package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * A DTO for the CashbookCategory entity.
 */
public class CashbookCategoryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2619440915519238755L;

	private Long id;

	@NotNull
	private String name;

	private String description;

	@NotNull
	@Pattern(regexp = "^#[A-Fa-f0-9]{6}$")
	private String color;

	private Long parentId;

	private String parentName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long cashbookCategoryId) {
		this.parentId = cashbookCategoryId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String cashbookCategoryName) {
		this.parentName = cashbookCategoryName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		CashbookCategoryDTO cashbookCategoryDTO = (CashbookCategoryDTO) obj;

		if (!Objects.equals(id, cashbookCategoryDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "CashbookCategoryDTO{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'"
				+ ", color='" + color + "'" + '}';
	}
}
