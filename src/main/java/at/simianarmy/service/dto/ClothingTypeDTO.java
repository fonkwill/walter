package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the ClothingType entity.
 */
public class ClothingTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7042992903881010507L;

	private Long id;

	@NotNull
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ClothingTypeDTO clothingTypeDTO = (ClothingTypeDTO) obj;

		if (!Objects.equals(id, clothingTypeDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClothingTypeDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
