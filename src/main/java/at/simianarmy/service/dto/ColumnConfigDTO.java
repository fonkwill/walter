package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ColumnConfig entity.
 */
public class ColumnConfigDTO implements Serializable {

    private Long id;

    @NotNull
    private String entity;

    @NotNull
    private String columnName;

    @NotNull
    private Boolean defaultVisible;

    @NotNull
    @Min(value = 1)
    private Integer defaultPosition;

    @NotNull
    private String columnDisplayName;

    @NotNull
    private String sortByName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Boolean isDefaultVisible() {
        return defaultVisible;
    }

    public void setDefaultVisible(Boolean defaultVisible) {
        this.defaultVisible = defaultVisible;
    }

    public Integer getDefaultPosition() {
        return defaultPosition;
    }

    public void setDefaultPosition(Integer defaultPosition) {
        this.defaultPosition = defaultPosition;
    }

    public String getColumnDisplayName() {
        return columnDisplayName;
    }

    public void setColumnDisplayName(String columnDisplayName) {
        this.columnDisplayName = columnDisplayName;
    }

    public String getSortByName() {
        return sortByName;
    }

    public void setSortByName(String sortByName) {
        this.sortByName = sortByName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ColumnConfigDTO columnConfigDTO = (ColumnConfigDTO) o;
        if (columnConfigDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), columnConfigDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ColumnConfigDTO{" +
            "id=" + getId() +
            ", entity='" + getEntity() + "'" +
            ", columnName='" + getColumnName() + "'" +
            ", defaultVisible='" + isDefaultVisible() + "'" +
            ", defaultPosition=" + getDefaultPosition() +
            ", columnDisplayName='" + getColumnDisplayName() + "'" +
            ", sortByName='" + getSortByName() + "'" +
            "}";
    }
}
