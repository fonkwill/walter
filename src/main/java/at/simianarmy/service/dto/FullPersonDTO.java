package at.simianarmy.service.dto;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class FullPersonDTO implements Serializable {


  private Long id;

  private LocalDateTime createdAt;

  @NotNull
  private String firstName;

  @NotNull
  private String lastName;

  private LocalDate birthDate;

  @NotNull
  private EmailType emailType;

  private String email;

  private String telephoneNumber;

  @NotNull
  private String streetAddress;

  @NotNull
  private String postalCode;

  @NotNull
  private String city;

  @NotNull
  private String country;

  private Gender gender;

  @NotNull
  private Boolean hasDirectDebit;

  private BigDecimal openMembershipAmount;

  private Set<PersonGroupDTO> personGroups = new HashSet<>();

  private Set<PersonClothingDTO> personClothings = new HashSet<>();

  private Set<PersonInstrumentDTO> personInstruments = new HashSet<>();

  private Set<PersonHonorDTO> personHonors = new HashSet<>();

  private Set<PersonAwardDTO> personAwards = new HashSet<>();

  private Set<MembershipDTO> memberships = new HashSet<>();

  private Set<AppointmentDTO> appointments = new HashSet<>();

  private Set<MembershipFeeForPeriodDTO> membershipFeeForPeriods = new HashSet<>();

  private Set<CashbookEntryDTO> cashbookEntries = new HashSet<>();

  private Set<BankImportDataDTO> bankImportData = new HashSet<>();


  private String organizationName;

  public Set<BankImportDataDTO> getBankImportData() {
    return bankImportData;
  }

  public void setBankImportData(Set<BankImportDataDTO> bankImportData) {
    this.bankImportData = bankImportData;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public EmailType getEmailType() {
    return emailType;
  }

  public void setEmailType(EmailType emailType) {
    this.emailType = emailType;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephoneNumber() {
    return telephoneNumber;
  }

  public void setTelephoneNumber(String telephoneNumber) {
    this.telephoneNumber = telephoneNumber;
  }

  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public Boolean getHasDirectDebit() {
    return hasDirectDebit;
  }

  public void setHasDirectDebit(Boolean hasDirectDebit) {
    this.hasDirectDebit = hasDirectDebit;
  }

  public BigDecimal getOpenMembershipAmount() {
    return openMembershipAmount;
  }

  public void setOpenMembershipAmount(BigDecimal openMembershipAmount) {
    this.openMembershipAmount = openMembershipAmount;
  }

  public Set<PersonGroupDTO> getPersonGroups() {
    return personGroups;
  }

  public void setPersonGroups(Set<PersonGroupDTO> personGroups) {
    this.personGroups = personGroups;
  }

  public Set<PersonClothingDTO> getPersonClothings() {
    return personClothings;
  }

  public void setPersonClothings(Set<PersonClothingDTO> personClothings) {
    this.personClothings = personClothings;
  }

  public Set<PersonInstrumentDTO> getPersonInstruments() {
    return personInstruments;
  }

  public void setPersonInstruments(Set<PersonInstrumentDTO> personInstruments) {
    this.personInstruments = personInstruments;
  }

  public Set<PersonHonorDTO> getPersonHonors() {
    return personHonors;
  }

  public void setPersonHonors(Set<PersonHonorDTO> personHonors) {
    this.personHonors = personHonors;
  }

  public Set<PersonAwardDTO> getPersonAwards() {
    return personAwards;
  }

  public void setPersonAwards(Set<PersonAwardDTO> personAwards) {
    this.personAwards = personAwards;
  }

  public Set<MembershipDTO> getMemberships() {
    return memberships;
  }

  public void setMemberships(Set<MembershipDTO> memberships) {
    this.memberships = memberships;
  }

  public Set<AppointmentDTO> getAppointments() {
    return appointments;
  }

  public void setAppointments(Set<AppointmentDTO> appointments) {
    this.appointments = appointments;
  }

  public Set<MembershipFeeForPeriodDTO> getMembershipFeeForPeriods() {
    return membershipFeeForPeriods;
  }

  public void setMembershipFeeForPeriods(Set<MembershipFeeForPeriodDTO> membershipFeeForPeriods) {
    this.membershipFeeForPeriods = membershipFeeForPeriods;
  }

  public Set<CashbookEntryDTO> getCashbookEntries() {
    return cashbookEntries;
  }

  public void setCashbookEntries(Set<CashbookEntryDTO> cashbookEntries) {
    this.cashbookEntries = cashbookEntries;
  }


  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }


  public void setOrganizationName(String organizationName) {
    this.organizationName = organizationName;
  }

  public String getOrganizationName() {
    return organizationName;
  }

}
