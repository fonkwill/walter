package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Member entity.
 */
public class MemberDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 377736429950467011L;

	public PersonDTO getPerson() {
		return person;
	}

	public void setPerson(PersonDTO person) {
		this.person = person;
	}

	public MembershipDTO getMembership() {
		return membership;
	}

	public void setMembership(MembershipDTO membership) {
		this.membership = membership;
	}

	@NotNull
	private PersonDTO person;

	@NotNull
	private MembershipDTO membership;

	@Override
	public int hashCode() {
		return Objects.hashCode(person.getId()) + Objects.hashCode(membership.getId());
	}

	@Override
	public String toString() {
		return "MemberDTO{" + person.toString() + membership.toString() + '}';
	}
}
