package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.BankImporterType;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the CashbookAccount entity.
 */
public class CashbookAccountDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Boolean bankAccount;

    private BankImporterType bankImporterType;

    @NotNull
    private String receiptCode;

    private String textForAutoTransferFrom;

    private String textForAutoTransferTo;

    @NotNull
    private Boolean defaultForCashbook;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(Boolean bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BankImporterType getBankImporterType() {
        return bankImporterType;
    }

    public void setBankImporterType(BankImporterType bankImporterType) {
        this.bankImporterType = bankImporterType;
    }

    public String getReceiptCode() {
        return receiptCode;
    }

    public void setReceiptCode(String receiptCode) {
        this.receiptCode = receiptCode;
    }

    public String getTextForAutoTransferFrom() {
      return textForAutoTransferFrom;
    }

    public void setTextForAutoTransferFrom(String textForAutoTransferFrom) {
      this.textForAutoTransferFrom = textForAutoTransferFrom;
    }

    public String getTextForAutoTransferTo() {
      return textForAutoTransferTo;
    }

    public void setTextForAutoTransferTo(String textForAutoTransferTo) {
      this.textForAutoTransferTo = textForAutoTransferTo;
    }

    public Boolean isDefaultForCashbook() {
      return defaultForCashbook;
    }

    public void setDefaultForCashbook(Boolean defaultForCashbook) {
      this.defaultForCashbook = defaultForCashbook;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CashbookAccountDTO cashbookAccountDTO = (CashbookAccountDTO) o;
        if (cashbookAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cashbookAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CashbookAccountDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", bankAccount='" + isBankAccount() + "'" +
            ", bankImporterType='" + getBankImporterType() + "'" +
            ", receiptCode='" + getReceiptCode() + "'" +
            ", textForAutoTransferFrom='" + getTextForAutoTransferFrom() + "'" +
            ", textForAutoTransferTo='" + getTextForAutoTransferTo() + "'" +
            ", defaultForCashbook='" + isDefaultForCashbook() + "'" +
            "}";
    }
}
