package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the InstrumentType entity.
 */
public class InstrumentTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4128326401808229250L;

	private Long id;

	@NotNull
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		InstrumentTypeDTO instrumentTypeDTO = (InstrumentTypeDTO) obj;

		if (!Objects.equals(id, instrumentTypeDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "InstrumentTypeDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
