package at.simianarmy.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Membership entity.
 */
public class MembershipDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1001300772797892835L;

	private Long id;

	@NotNull
	private LocalDate beginDate;

	private LocalDate endDate;

	private Long personId;

	private Long membershipFeeId;

	private String membershipFeeDescription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getMembershipFeeId() {
		return membershipFeeId;
	}

	public void setMembershipFeeId(Long membershipFeeId) {
		this.membershipFeeId = membershipFeeId;
	}

	public String getMembershipFeeDescription() {
		return membershipFeeDescription;
	}

	public void setMembershipFeeDescription(String membershipFeeDescription) {
		this.membershipFeeDescription = membershipFeeDescription;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MembershipDTO membershipDTO = (MembershipDTO) o;

		if (!Objects.equals(id, membershipDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MembershipDTO{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'" + '}';
	}
}
