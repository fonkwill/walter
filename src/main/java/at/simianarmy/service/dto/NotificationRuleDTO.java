package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.service.dto.RoleDTO;

/**
 * A DTO for the NotificationRule entity.
 */
public class NotificationRuleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6133311393073030729L;

	private Long id;

	@NotNull
	private String title;

	@NotNull
	private String entity;

	@NotNull
	private String conditions;

	@NotNull
	private Boolean active;

	@NotNull
	private String messageGroup;

	@NotNull
	private String messageSingle;

	private Set<RoleDTO> targetAudiences = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getConditions() {
		return conditions;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getMessageGroup() {
		return messageGroup;
	}

	public void setMessageGroup(String messageGroup) {
		this.messageGroup = messageGroup;
	}

	public String getMessageSingle() {
		return messageSingle;
	}

	public void setMessageSingle(String messageSingle) {
		this.messageSingle = messageSingle;
	}

	public Set<RoleDTO> getTargetAudiences() {
		return targetAudiences;
	}

	public void setTargetAudiences(Set<RoleDTO> authorities) {
		this.targetAudiences = authorities;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		NotificationRuleDTO notificationRuleDTO = (NotificationRuleDTO) obj;

		if (!Objects.equals(id, notificationRuleDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "NotificationRuleDTO{" + "id=" + id + ", title='" + title + "'" + ", entity='" + entity + "'"
				+ ", active='" + active + "'" + '}';
	}
}
