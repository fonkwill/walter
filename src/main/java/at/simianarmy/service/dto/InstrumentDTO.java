package at.simianarmy.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the Instrument entity.
 */
public class InstrumentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5085292079016920822L;

	private Long id;

	@NotNull
	private String name;

	private LocalDate purchaseDate;

	private Boolean privateInstrument;

	@DecimalMin(value = "0")
	private BigDecimal price;

	private Long typeId;

	private String typeName;

	private Long cashbookEntryId;

	private BigDecimal cashbookEntryAmount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Boolean getPrivateInstrument() {
		return privateInstrument;
	}

	public void setPrivateInstrument(Boolean privateInstrument) {
		this.privateInstrument = privateInstrument;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long instrumentTypeId) {
		this.typeId = instrumentTypeId;
	}

	public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public BigDecimal getCashbookEntryAmount() {
		return cashbookEntryAmount;
	}

	public void setCashbookEntryAmount(BigDecimal cashbookEntryAmount) {
		this.cashbookEntryAmount = cashbookEntryAmount;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		InstrumentDTO instrumentDTO = (InstrumentDTO) o;

		if (!Objects.equals(id, instrumentDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "InstrumentDTO{" + "id=" + id + ", name='" + name + "'" + ", purchaseDate='" + purchaseDate + "'"
				+ ", privateInstrument='" + privateInstrument + "'" + ", price='" + price + "'" + '}';
	}
}
