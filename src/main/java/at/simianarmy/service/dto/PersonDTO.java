package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.EmailType;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import at.simianarmy.domain.enumeration.Gender;

/**
 * A DTO for the Person entity.
 */
public class PersonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8972047651729780418L;

	private Long id;

	@NotNull
	private String firstName;

	@NotNull
	private String lastName;

	private LocalDate birthDate;

	@NotNull
	private EmailType emailType;

	private String email;

	private String telephoneNumber;

	@NotNull
	private String streetAddress;

	@NotNull
	private String postalCode;

	@NotNull
	private String city;

	@NotNull
	private String country;

	private Gender gender;

	@NotNull
  private Boolean hasDirectDebit;

  private LocalDate dateLostRelevance;

	private BigDecimal openMembershipAmount;

	private Set<PersonGroupDTO> personGroups = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

  public EmailType getEmailType() {
    return emailType;
  }

  public void setEmailType(EmailType emailType) {
    this.emailType = emailType;
  }

  public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Boolean getHasDirectDebit() { return hasDirectDebit; }

	public void setHasDirectDebit(Boolean hasDirectDebit) { this.hasDirectDebit = hasDirectDebit; }

	public Set<PersonGroupDTO> getPersonGroups() {
		return personGroups;
	}

	public void setPersonGroups(Set<PersonGroupDTO> personGroups) {
		this.personGroups = personGroups;
	}

	public BigDecimal getOpenMembershipAmount() {
		return openMembershipAmount;
	}

	public void setOpenMembershipAmount(BigDecimal openMembershipAmount) {
		this.openMembershipAmount = openMembershipAmount;
	}


  public LocalDate getDateLostRelevance() {
    return dateLostRelevance;
  }

  public void setDateLostRelevance(LocalDate dateLostRelevance) {
    this.dateLostRelevance = dateLostRelevance;
  }

  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PersonDTO personDTO = (PersonDTO) o;

		if (!Objects.equals(id, personDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonDTO{" + "id=" + id + ", firstName='" + firstName + "'" + ", lastName='" + lastName + "'"
				+ ", birthDate='" + birthDate + "'" + ", email='" + email + "'" + ", telephoneNumber='"
				+ telephoneNumber + "'" + ", streetAddress='" + streetAddress + "'" + ", postalCode='" + postalCode
				+ "'" + ", city='" + city + "'" + ", country='" + country + "'" + ", gender='" + gender + "'" + '}';
	}
}
