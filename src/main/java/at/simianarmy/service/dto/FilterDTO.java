package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Filter entity.
 */
public class FilterDTO implements Serializable {

    private Long id;

    @NotNull
    private String listName;

    @NotNull
    private String name;

    @NotNull
    private String filter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FilterDTO filterDTO = (FilterDTO) o;
        if (filterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FilterDTO{" +
            "id=" + getId() +
            ", listName='" + getListName() + "'" +
            ", name='" + getName() + "'" +
            ", filter='" + getFilter() + "'" +
            "}";
    }
}
