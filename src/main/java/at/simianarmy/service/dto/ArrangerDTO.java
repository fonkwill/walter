package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Arranger entity.
 */
public class ArrangerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String name;

	private Set<CompositionDTO> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CompositionDTO> getCompositions() {
		return compositions;
	}

	public void setCompositions(Set<CompositionDTO> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ArrangerDTO arrangerDTO = (ArrangerDTO) obj;

		if (!Objects.equals(id, arrangerDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ArrangerDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
