package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Report entity.
 */
public class ReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3748346118930652254L;

	private Long id;

	@NotNull
	private ZonedDateTime generationDate;

	@NotNull
	private String associationId;

	@NotNull
	private String associationName;

	@NotNull
	private String streetAddress;

	@NotNull
	private String postalCode;

	@NotNull
	private String city;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getGenerationDate() {
		return generationDate;
	}

	public void setGenerationDate(ZonedDateTime generationDate) {
		this.generationDate = generationDate;
	}

	public String getAssociationId() {
		return associationId;
	}

	public void setAssociationId(String associationId) {
		this.associationId = associationId;
	}

	public String getAssociationName() {
		return associationName;
	}

	public void setAssociationName(String associationName) {
		this.associationName = associationName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ReportDTO reportDTO = (ReportDTO) obj;

		if (!Objects.equals(id, reportDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ReportDTO{" + "id=" + id + ", generationDate='" + generationDate + "'" + ", associationId='"
				+ associationId + "'" + ", associationName='" + associationName + "'" + ", streetAddress='"
				+ streetAddress + "'" + ", postalCode='" + postalCode + "'" + ", city='" + city + "'" + '}';
	}
}
