package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.domain.enumeration.MailLogStatus;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the MailLog.
 */
public class MailLogDTO implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 4989832367498792194L;

  private Long id;

  @NotNull
  private ZonedDateTime date;

  @NotNull
  private String email;

  @NotNull
  private String subject;

  @NotNull
  private MailLogStatus mailLogStatus;

  private String errorMessage;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ZonedDateTime getDate() {
    return date;
  }

  public void setDate(ZonedDateTime date) {
    this.date = date;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public MailLogStatus getMailLogStatus() {
    return mailLogStatus;
  }

  public void setMailLogStatus(MailLogStatus mailLogStatus) {
    this.mailLogStatus = mailLogStatus;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MailLogDTO that = (MailLogDTO) o;
    return Objects.equals(id, that.id) &&
      date.equals(that.date) &&
      email.equals(that.email) &&
      subject.equals(that.subject) &&
      mailLogStatus == that.mailLogStatus &&
      Objects.equals(errorMessage, that.errorMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, date, email, subject, mailLogStatus, errorMessage);
  }

  @Override
  public String toString() {
    return "MailLogDTO{" +
      "id=" + id +
      ", date=" + date +
      ", email='" + email + '\'' +
      ", subject='" + subject + '\'' +
      ", mailLogStatus=" + mailLogStatus +
      ", errorMessage='" + errorMessage + '\'' +
      '}';
  }
}
