package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import at.simianarmy.domain.enumeration.MembershipFeePeriod;

/**
 * A DTO for the MembershipFee entity.
 */
public class MembershipFeeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5179706016168321656L;

	private Long id;

	@NotNull
	private String description;

	@NotNull
	private MembershipFeePeriod period;

	@Min(value = 1)
	@Max(value = 12)
	private Integer periodTimeFraction;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MembershipFeePeriod getPeriod() {
		return period;
	}

	public void setPeriod(MembershipFeePeriod period) {
		this.period = period;
	}

	public Integer getPeriodTimeFraction() {
		return periodTimeFraction;
	}

	public void setPeriodTimeFraction(Integer periodTimeFraction) {
		this.periodTimeFraction = periodTimeFraction;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MembershipFeeDTO membershipFeeDTO = (MembershipFeeDTO) o;

		if (!Objects.equals(id, membershipFeeDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MembershipFeeDTO{" + "id=" + id + ", description='" + description + "'" + ", period='" + period + "'"
				+ ", periodTimeFraction='" + periodTimeFraction + "'" + '}';
	}
}
