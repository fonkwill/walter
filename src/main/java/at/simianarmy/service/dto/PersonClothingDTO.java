package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PersonClothing entity.
 */
public class PersonClothingDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4358319397406581939L;

	private Long id;

	@NotNull
	private LocalDate beginDate;

	private LocalDate endDate;

	private Long clothingId;

	private Long personId;

	private String clothingTypeName;

	private String clothingSize;

	private String clothingTypeId;

	public String getClothingTypeId() {
		return clothingTypeId;
	}

	public void setClothingTypeId(String clothingTypeId) {
		this.clothingTypeId = clothingTypeId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClothingSize() {
		return clothingSize;
	}

	public void setClothingSize(String clothingSize) {
		this.clothingSize = clothingSize;
	}

	public String getClothingTypeName() {
		return clothingTypeName;
	}

	public void setClothingTypeName(String clothingTypeName) {
		this.clothingTypeName = clothingTypeName;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getClothingId() {
		return clothingId;
	}

	public void setClothingId(Long clothingId) {
		this.clothingId = clothingId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		PersonClothingDTO personClothingDTO = (PersonClothingDTO) obj;

		if (!Objects.equals(id, personClothingDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonClothingDTO{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'"
				+ '}';
	}
}
