package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Honor entity.
 */
public class HonorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -70330805904529630L;

	private Long id;

	@NotNull
	private String name;

	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		HonorDTO honorDTO = (HonorDTO) obj;

		if (!Objects.equals(id, honorDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "HonorDTO{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'" + '}';
	}
}
