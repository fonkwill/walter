package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the AppointmentType entity.
 */
public class AppointmentTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String name;

	@NotNull
	private Boolean isOfficial;

	private Long personGroupId;

	private String personGroupName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsOfficial() {
		return isOfficial;
	}

	public void setIsOfficial(Boolean isOfficial) {
		this.isOfficial = isOfficial;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		AppointmentTypeDTO appointmentTypeDTO = (AppointmentTypeDTO) obj;

		if (!Objects.equals(id, appointmentTypeDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "AppointmentTypeDTO{" + "id=" + id + ", name='" + name + "'" + ", isOfficial='" + isOfficial + "'" + '}';
	}

	public Long getPersonGroupId() {
		return personGroupId;
	}

	public void setPersonGroupId(Long personGroupId) {
		this.personGroupId = personGroupId;
	}

	public String getPersonGroupName() {
		return personGroupName;
	}

	public void setPersonGroupName(String personGroupName) {
		this.personGroupName = personGroupName;
	}
}
