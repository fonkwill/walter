package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Genre entity.
 */
public class GenreDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8028430900809137548L;

	private Long id;

	@NotNull
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		GenreDTO genreDTO = (GenreDTO) obj;

		if (!Objects.equals(id, genreDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "GenreDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
