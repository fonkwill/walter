package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.CashbookEntryType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the CashbookEntry entity.
 */
public class CashbookEntryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 118813481482309356L;

	private Long id;

	@NotNull
	private String title;

	@NotNull
	private CashbookEntryType type;

	@NotNull
	private LocalDate date;

	@NotNull
	@DecimalMin(value = "0")
	private BigDecimal amount;

	private Long appointmentId;
	private String appointmentName;
	private ZonedDateTime appointmentBeginDate;

	private Long receiptId;
	private String receiptIdentifier;
	private String receiptFile;

	private String cashbookCategoryName;
	private String cashbookCategoryColor;
	private Long cashbookCategoryId;

	private Long membershipFeeForPeriodId;
	private Integer membershipFeeForPeriodNr;
	private Integer membershipFeeForPeriodYear;
	private String membershipFeeForPeriodReferenceCode;
	private Long membershipFeeForPeriodPersonId;

	private Long instrumentServiceId;
	private Long instrumentServiceInstrumentId;
	private String instrumentServiceInstrumentName;

	private Long instrumentId;
	private String instrumentName;

	@NotNull
	private Long cashbookAccountId;
	private String cashbookAccountName;

	public String getCashbookAccountName() {
		return cashbookAccountName;
	}

	public void setCashbookAccountName(String cashbookAccountName) {
		this.cashbookAccountName = cashbookAccountName;
	}

	private Set<CompositionDTO> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CashbookEntryType getType() {
		return type;
	}

	public void setType(CashbookEntryType type) {
		this.type = type;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(Long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public String getAppointmentName() {
		return appointmentName;
	}

	public void setAppointmentName(String appointmentName) {
		this.appointmentName = appointmentName;
	}

	public ZonedDateTime getAppointmentBeginDate() {
		return appointmentBeginDate;
	}

	public void setAppointmentBeginDate(ZonedDateTime appointmentBegin) {
		this.appointmentBeginDate = appointmentBegin;
	}

	public Long getCashbookCategoryId() {
		return cashbookCategoryId;
	}

	public void setCashbookCategoryId(Long cashbookCategoryId) {
		this.cashbookCategoryId = cashbookCategoryId;
	}

	public Long getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(Long receiptId) {
		this.receiptId = receiptId;
	}

	public String getReceiptIdentifier() {
		return receiptIdentifier;
	}

	public void setReceiptIdentifier(String receiptIdentifier) {
		this.receiptIdentifier = receiptIdentifier;
	}

	public String getCashbookCategoryName() {
		return cashbookCategoryName;
	}

	public void setCashbookCategoryName(String cashbookCategoryName) {
		this.cashbookCategoryName = cashbookCategoryName;
	}

	public String getCashbookCategoryColor() {
		return cashbookCategoryColor;
	}

	public void setCashbookCategoryColor(String cashbookCategoryColor) {
		this.cashbookCategoryColor = cashbookCategoryColor;
	}

	public Long getMembershipFeeForPeriodId() {
		return membershipFeeForPeriodId;
	}

	public void setMembershipFeeForPeriodId(Long memberhsipFeeForPeriodId) {
		this.membershipFeeForPeriodId = memberhsipFeeForPeriodId;
	}

	public Integer getMembershipFeeForPeriodNr() {
		return membershipFeeForPeriodNr;
	}

	public void setMembershipFeeForPeriodNr(Integer membershipFeeForPeriodNr) {
		this.membershipFeeForPeriodNr = membershipFeeForPeriodNr;
	}

	public Integer getMembershipFeeForPeriodYear() {
		return membershipFeeForPeriodYear;
	}

	public void setMembershipFeeForPeriodYear(Integer membershipFeeForPeriodYear) {
		this.membershipFeeForPeriodYear = membershipFeeForPeriodYear;
	}

	public String getMembershipFeeForPeriodReferenceCode() {
		return membershipFeeForPeriodReferenceCode;
	}

	public void setMembershipFeeForPeriodReferenceCode(String membershipFeeForPeriodReferenceCode) {
		this.membershipFeeForPeriodReferenceCode = membershipFeeForPeriodReferenceCode;
	}

	public Long getMembershipFeeForPeriodPersonId() {
		return membershipFeeForPeriodPersonId;
	}

	public void setMembershipFeeForPeriodPersonId(Long membershipFeeForPeriodPersonId) {
		this.membershipFeeForPeriodPersonId = membershipFeeForPeriodPersonId;
	}

	public Long getInstrumentServiceId() {
		return instrumentServiceId;
	}

	public void setInstrumentServiceId(Long instrumentServiceId) {
		this.instrumentServiceId = instrumentServiceId;
	}

	public Long getInstrumentServiceInstrumentId() {
		return instrumentServiceInstrumentId;
	}

	public void setInstrumentServiceInstrumentId(Long instrumentServiceInstrumentId) {
		this.instrumentServiceInstrumentId = instrumentServiceInstrumentId;
	}

	public String getInstrumentServiceInstrumentName() {
		return instrumentServiceInstrumentName;
	}

	public void setInstrumentServiceInstrumentName(String instrumentServiceInstrumentName) {
		this.instrumentServiceInstrumentName = instrumentServiceInstrumentName;
	}

	public Long getInstrumentId() {
		return instrumentId;
	}

	public void setInstrumentId(Long instrumentId) {
		this.instrumentId = instrumentId;
	}

	public String getInstrumentName() {
		return instrumentName;
	}

	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	public Set<CompositionDTO> getCompositions() {
		return compositions;
	}

	public void setCompositions(Set<CompositionDTO> compositions) {
		this.compositions = compositions;
	}

	public Long getCashbookAccountId() {
		return cashbookAccountId;
	}

	public void setCashbookAccountId(Long cashbookAccountId) {
		this.cashbookAccountId = cashbookAccountId;
	}

  public String getReceiptFile() {
    return receiptFile;
  }

  public void setReceiptFile(String receiptFile) {
    this.receiptFile = receiptFile;
  }

  @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		CashbookEntryDTO cashbookEntryDTO = (CashbookEntryDTO) obj;

		if (!Objects.equals(id, cashbookEntryDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "CashbookEntryDTO{" + "id=" + id + ", title='" + title + "'" + ", type='" + type + "'" + ", date='"
				+ date + "'" + ", amount='" + amount + "'" + '}';
	}
}
