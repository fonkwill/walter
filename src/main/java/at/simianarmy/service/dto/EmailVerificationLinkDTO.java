package at.simianarmy.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the EmailVerificationLink entity.
 */
public class EmailVerificationLinkDTO implements Serializable {

    private Long id;

    @NotNull
    private String secretKey;

    @NotNull
    private ZonedDateTime creationDate;

    private ZonedDateTime validationDate;

    @NotNull
    private Boolean invalid;

    private Long personId;

    @NotNull
    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(ZonedDateTime validationDate) {
        this.validationDate = validationDate;
    }

    public Boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(Boolean invalid) {
        this.invalid = invalid;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getLink() {
      return link;
    }

    public void setLink(String link) {
      this.link = link;
    }

  @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmailVerificationLinkDTO emailVerificationLinkDTO = (EmailVerificationLinkDTO) o;
        if (emailVerificationLinkDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emailVerificationLinkDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmailVerificationLinkDTO{" +
            "id=" + getId() +
            ", secretKey='" + getSecretKey() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", validationDate='" + getValidationDate() + "'" +
            ", invalid='" + isInvalid() + "'" +
            ", person=" + getPersonId() +
            ", link='" + getLink() + "'" +
            "}";
    }
}
