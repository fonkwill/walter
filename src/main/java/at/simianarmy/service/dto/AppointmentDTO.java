package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Appointment entity.
 */
public class AppointmentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	@NotNull
	private String name;

	private Long typeId;

	private String typeName;

	private String description;

	private String streetAddress;

	private String postalCode;

	private String city;

	private String country;

	@NotNull
	private ZonedDateTime beginDate;

	@NotNull
	private ZonedDateTime endDate;

	private Long personGroupId;

	private Set<PersonDTO> persons = new HashSet<>();

	private Boolean isOfficial;

	private Long officialAppointmentId;

	private String organizerName;

	private String organizerAddress;

	private Boolean isHeadQuota;

	private Set<CompositionDTO> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(ZonedDateTime beginDate) {
		this.beginDate = beginDate;
	}

	public ZonedDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public Long getOfficialAppointmentId() {
		return officialAppointmentId;
	}

	public void setOfficialAppointmentId(Long officialAppointmentId) {
		this.officialAppointmentId = officialAppointmentId;
	}

	public Set<PersonDTO> getPersons() {
		return persons;
	}

	public void setPersons(Set<PersonDTO> people) {
		this.persons = people;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long appointmentTypeId) {
		this.typeId = appointmentTypeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getOrganizerName() {
		return organizerName;
	}

	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}

	public String getOrganizerAddress() {
		return organizerAddress;
	}

	public void setOrganizerAddress(String organizerAddress) {
		this.organizerAddress = organizerAddress;
	}

	public Boolean getIsHeadQuota() {
		return isHeadQuota;
	}

	public void setIsHeadQuota(Boolean isHeadQuota) {
		this.isHeadQuota = isHeadQuota;
	}

	public Boolean getIsOfficial() {
		return isOfficial;
	}

	public void setIsOfficial(Boolean isOfficial) {
		this.isOfficial = isOfficial;
	}

	public Set<CompositionDTO> getCompositions() {
		return compositions;
	}

	public void setCompositions(Set<CompositionDTO> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		AppointmentDTO appointmentDTO = (AppointmentDTO) obj;

		return Objects.equals(id, appointmentDTO.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		String str = "AppointmentDTO{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'"
				+ ", streetAddress='" + streetAddress + "'" + ", postalCode='" + postalCode + "'" + ", city='" + city
				+ "'" + ", country='" + country + "'" + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate
				+ "'";
		if (this.officialAppointmentId != null) {
			str += "officialAppointmendId=" + officialAppointmentId + ", organizerName='" + organizerName + "'"
					+ ", organizerAddress='" + organizerAddress + "'" + ", isHeadQuota='" + isHeadQuota + "'";
		}
		str += '}';
		return str;
	}

	public Long getPersonGroupId() {
		return personGroupId;
	}

	public void setPersonGroupId(Long personGroupId) {
		this.personGroupId = personGroupId;
	}
}
