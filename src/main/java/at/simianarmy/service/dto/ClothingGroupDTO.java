package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the ClothingGroup entity.
 */
public class ClothingGroupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2417239641625061670L;

	private Long id;

	private String csize;

	private Integer quantity;

	private Integer quantityAvailable;

	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	private Long typeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCsize() {
		return csize;
	}

	public void setCsize(String csize) {
		this.csize = csize;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getQuantityAvailable() {
		return quantityAvailable;
	}

	public void setQuantityAvailable(Integer quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long clothingTypeId) {
		this.typeId = clothingTypeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ClothingGroupDTO clothingGroupDTO = (ClothingGroupDTO) o;

		if (!Objects.equals(id, clothingGroupDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClothingGroupDTO{" + "id=" + id + ", size='" + csize + "'" + ", quantity='" + quantity + "'"
				+ ", quantityAvailable='" + quantityAvailable + "'" + '}';
	}
}
