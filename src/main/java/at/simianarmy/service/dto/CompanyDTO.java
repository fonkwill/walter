package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Company entity.
 */
public class CompanyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5338822883949398072L;

	private Long id;

	@NotNull
	private String name;

	private String streetAddress;

	private String postalCode;

	private String city;

	private String country;

	private String recipient;

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	private Set<PersonGroupDTO> personGroups = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Set<PersonGroupDTO> getPersonGroups() {
		return personGroups;
	}

	public void setPersonGroups(Set<PersonGroupDTO> personGroups) {
		this.personGroups = personGroups;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		CompanyDTO companyDTO = (CompanyDTO) o;
		if (companyDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), companyDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "CompanyDTO{" + "id=" + getId() + ", name='" + getName() + "'" + ", streetAddress='" + getStreetAddress()
				+ "'" + ", postalCode='" + getPostalCode() + "'" + ", city='" + getCity() + "'" + ", country='"
				+ getCountry() + "'" + ", recipient='" + getRecipient() + "'" + "}";
	}
}
