package at.simianarmy.service.dto;

import javax.validation.constraints.*;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Template entity.
 */
public class TemplateDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3037918527136696603L;

	private Long id;

	@NotNull
	private String title;

	private String file;

	private MultipartFile uploadedFile;

	private Float marginLeft;

	private Float marginRight;

	private Float marginTop;

	private Float marginBottom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public MultipartFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(MultipartFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Float getMarginLeft() {
		return marginLeft;
	}

	public void setMarginLeft(Float marginLeft) {
		this.marginLeft = marginLeft;
	}

	public Float getMarginRight() {
		return marginRight;
	}

	public void setMarginRight(Float marginRight) {
		this.marginRight = marginRight;
	}

	public Float getMarginTop() {
		return marginTop;
	}

	public void setMarginTop(Float marginTop) {
		this.marginTop = marginTop;
	}

	public Float getMarginBottom() {
		return marginBottom;
	}

	public void setMarginBottom(Float marginBottom) {
		this.marginBottom = marginBottom;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TemplateDTO templateDTO = (TemplateDTO) o;

		if (!Objects.equals(id, templateDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "TemplateDTO{" + "id=" + id + ", title='" + title + "'" + ", file='" + file + "'" + ", marginLeft='"
				+ marginLeft + "'" + ", marginRight='" + marginRight + "'" + ", marginTop='" + marginTop + "'"
				+ ", marginBottom='" + marginBottom + "'" + '}';
	}
}
