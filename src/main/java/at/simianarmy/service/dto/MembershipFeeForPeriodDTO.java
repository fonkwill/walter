package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MembershipFeeForPeriod entity.
 */
public class MembershipFeeForPeriodDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3890716518935197757L;

	private Long id;

	@NotNull
	private Integer nr;

	@NotNull
	private Integer year;

	private String referenceCode;

	private Boolean paid;

	private String note;

	private Long cashbookEntryId;

	private Long membershipId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNr() {
		return nr;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean isPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public Long getMembershipId() {
		return membershipId;
	}

	public void setMembershipId(Long membershipId) {
		this.membershipId = membershipId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = (MembershipFeeForPeriodDTO) o;
		if (membershipFeeForPeriodDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), membershipFeeForPeriodDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "MembershipFeeForPeriodDTO{" + "id=" + getId() + ", nr='" + getNr() + "'" + ", year='" + getYear() + "'"
				+ ", referenceCode='" + getReferenceCode() + "'" + ", paid='" + isPaid() + "'" + ", note='" + getNote()
				+ "'" + "}";
	}
}
