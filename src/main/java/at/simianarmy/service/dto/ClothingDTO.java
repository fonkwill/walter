package at.simianarmy.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Clothing entity.
 */
public class ClothingDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6788539646805713578L;

	private Long id;

	private String size;

	private LocalDate purchaseDate;

	private Boolean notAvailable;

	private String typeName;

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	private Long typeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Boolean getNotAvailable() {
		return notAvailable;
	}

	public void setNotAvailable(Boolean notAvailable) {
		this.notAvailable = notAvailable;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long clothingTypeId) {
		this.typeId = clothingTypeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ClothingDTO clothingDTO = (ClothingDTO) o;

		if (!Objects.equals(id, clothingDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClothingDTO{" + "id=" + id + ", size='" + size + "'" + ", purchaseDate='" + purchaseDate + "'"
				+ ", notAvailable='" + notAvailable + "'" + '}';
	}
}
