package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the PersonGroup entity.
 */
public class PersonGroupDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7967968512653580775L;

	private Long id;

	@NotNull
	private String name;

	private Long parentId;

	private String parentName;


  @NotNull
	private Boolean hasPersonRelevance;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long personGroupId) {
		this.parentId = personGroupId;
	}

  public Boolean getHasPersonRelevance() {
    return hasPersonRelevance;
  }

  public void setHasPersonRelevance(Boolean hasPersonRelevance) {
    this.hasPersonRelevance = hasPersonRelevance;
  }


  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		PersonGroupDTO personGroupDTO = (PersonGroupDTO) o;

		if (!Objects.equals(id, personGroupDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonGroupDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
