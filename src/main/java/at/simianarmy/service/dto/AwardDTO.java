package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Award entity.
 */
public class AwardDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4961172966049607180L;

	private Long id;

	@NotNull
	private String name;

	private String descripton;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripton() {
		return descripton;
	}

	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		AwardDTO awardDTO = (AwardDTO) obj;

		if (!Objects.equals(id, awardDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "AwardDTO{" + "id=" + id + ", name='" + name + "'" + ", descripton='" + descripton + "'" + '}';
	}
}
