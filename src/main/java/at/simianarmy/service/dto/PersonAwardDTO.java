package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PersonAward entity.
 */
public class PersonAwardDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8107603028072340909L;

	private Long id;

	@NotNull
	private LocalDate date;

	private Long awardId;

	private Long personId;

	private String awardName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Long getAwardId() {
		return awardId;
	}

	public void setAwardId(Long awardId) {
		this.awardId = awardId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getAwardName() {
		return awardName;
	}

	public void setAwardName(String awardName) {
		this.awardName = awardName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		PersonAwardDTO personAwardDTO = (PersonAwardDTO) obj;

		if (!Objects.equals(id, personAwardDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonAwardDTO{" + "id=" + id + ", date='" + date + "'" + '}';
	}
}
