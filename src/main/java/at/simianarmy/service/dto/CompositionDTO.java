package at.simianarmy.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Composition entity.
 */
public class CompositionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7911256671490052597L;

	private Long id;

	private Integer nr;

	@NotNull
	private String title;

	private String note;

	@NotNull
	private Integer akmCompositionNr;

	private Set<OfficialAppointmentDTO> appointments = new HashSet<>();

	private Long cashbookEntryId;

	private BigDecimal cashbookEntryAmount;

	private Long orderingCompanyId;

	private String orderingCompanyName;

	private Long publisherId;

	private String publisherName;

	private Long genreId;

	private String genreName;

	private Long musicBookId;

	private String musicBookName;

	private Long colorCodeId;

	private String colorCodeName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNr() {
		return nr;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getAkmCompositionNr() {
		return akmCompositionNr;
	}

	public void setAkmCompositionNr(Integer akmCompositionNr) {
		this.akmCompositionNr = akmCompositionNr;
	}

	public Set<OfficialAppointmentDTO> getAppointments() {
		return appointments;
	}

	public void setAppointments(Set<OfficialAppointmentDTO> officialAppointments) {
		this.appointments = officialAppointments;
	}

	public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public BigDecimal getCashbookEntryAmount() {
		return cashbookEntryAmount;
	}

	public void setCashbookEntryAmount(BigDecimal cashbookEntryAmount) {
		this.cashbookEntryAmount = cashbookEntryAmount;
	}

	public Long getOrderingCompanyId() {
		return orderingCompanyId;
	}

	public void setOrderingCompanyId(Long companyId) {
		this.orderingCompanyId = companyId;
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long companyId) {
		this.publisherId = companyId;
	}

	public Long getGenreId() {
		return genreId;
	}

	public void setGenreId(Long genreId) {
		this.genreId = genreId;
	}

	public Long getMusicBookId() {
		return musicBookId;
	}

	public void setMusicBookId(Long musicBookId) {
		this.musicBookId = musicBookId;
	}

	public Long getColorCodeId() {
		return colorCodeId;
	}

	public void setColorCodeId(Long colorCodeId) {
		this.colorCodeId = colorCodeId;
	}

	public String getOrderingCompanyName() {
		return orderingCompanyName;
	}

	public void setOrderingCompanyName(String orderingCompanyName) {
		this.orderingCompanyName = orderingCompanyName;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	public String getMusicBookName() {
		return musicBookName;
	}

	public void setMusicBookName(String musicBookName) {
		this.musicBookName = musicBookName;
	}

	public String getColorCodeName() {
		return colorCodeName;
	}

	public void setColorCodeName(String colorCodeName) {
		this.colorCodeName = colorCodeName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		CompositionDTO compositionDTO = (CompositionDTO) obj;

		if (!Objects.equals(id, compositionDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "CompositionDTO{" + "id=" + id + ", nr='" + nr + "'" + ", title='" + title + "'" + ", note='" + note
				+ "'" + ", akmCompositionNr='" + akmCompositionNr + "'" + '}';
	}
}
