package at.simianarmy.service.dto;

import java.io.Serializable;

public class EmailVerificationRequestDTO implements Serializable {

  private Long personId;

  private String email;

  private String secretKey;

  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }
}
