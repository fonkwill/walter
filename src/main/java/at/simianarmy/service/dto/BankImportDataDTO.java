package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.BankImporterType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the BankImportData entity.
 */
public class BankImportDataDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8074113091254094315L;

	private Long id;

	@NotNull
	private LocalDate entryDate;

	@NotNull
	private BigDecimal entryValue;

	private String entryText;

	private String partnerName;

	@NotNull
	private byte[] entryReference;

	@NotNull
	private Long cashbookAccountId;

	private Long cashbookEntryId;

	private String cashbookEntryTitle;

	private Long cashbookCategoryId;

	private String cashbookCategoryName;

	@NotNull
  private Boolean isTransfer;

	private Long cashbookAccountFromId;

	private String cashbookAccountFromName;

	private Long cashbookAccountToId;

	private String cashbookAccountToName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}

	public BigDecimal getEntryValue() {
		return entryValue;
	}

	public void setEntryValue(BigDecimal entryValue) {
		this.entryValue = entryValue;
	}

	public String getEntryText() {
		return entryText;
	}

	public void setEntryText(String entryText) {
		this.entryText = entryText;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

  public byte[] getEntryReference() {
    return entryReference;
  }

  public void setEntryReference(byte[] entryReference) {
    this.entryReference = entryReference;
  }

  public Long getCashbookAccountId() {
    return cashbookAccountId;
  }

  public void setCashbookAccountId(Long cashbookAccountId) {
    this.cashbookAccountId = cashbookAccountId;
  }

  public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public String getCashbookEntryTitle() {
		return cashbookEntryTitle;
	}

	public void setCashbookEntryTitle(String cashbookEntryTitle) {
		this.cashbookEntryTitle = cashbookEntryTitle;
	}

  public Long getCashbookCategoryId() {
    return cashbookCategoryId;
  }

  public void setCashbookCategoryId(Long cashbookCategoryId) {
    this.cashbookCategoryId = cashbookCategoryId;
  }

  public String getCashbookCategoryName() {
    return cashbookCategoryName;
  }

  public void setCashbookCategoryName(String cashbookCategoryName) {
    this.cashbookCategoryName = cashbookCategoryName;
  }

  public Boolean getTransfer() {
    return isTransfer;
  }

  public void setTransfer(Boolean transfer) {
    isTransfer = transfer;
  }

  public Long getCashbookAccountFromId() {
    return cashbookAccountFromId;
  }

  public void setCashbookAccountFromId(Long cashbookAccountFromId) {
    this.cashbookAccountFromId = cashbookAccountFromId;
  }

  public String getCashbookAccountFromName() {
    return cashbookAccountFromName;
  }

  public void setCashbookAccountFromName(String cashbookAccountFromName) {
    this.cashbookAccountFromName = cashbookAccountFromName;
  }

  public Long getCashbookAccountToId() {
    return cashbookAccountToId;
  }

  public void setCashbookAccountToId(Long cashbookAccountToId) {
    this.cashbookAccountToId = cashbookAccountToId;
  }

  public String getCashbookAccountToName() {
    return cashbookAccountToName;
  }

  public void setCashbookAccountToName(String cashbookAccountToName) {
    this.cashbookAccountToName = cashbookAccountToName;
  }

  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BankImportDataDTO bankImportDataDTO = (BankImportDataDTO) o;

		if (!Objects.equals(id, bankImportDataDTO.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "BankImportDataDTO{" + "id=" + id + ", entryDate='" + entryDate + "'" + ", entryValue='" + entryValue
				+ "'" + ", entryText='" + entryText + "'" + ", partnerName='" + partnerName + "'" + ", entryReference='"
				+ entryReference + "'" + ", cashbookAccountId='" + cashbookAccountId + "'" + '}';
	}
}
