package at.simianarmy.service.dto;

import at.simianarmy.domain.enumeration.LetterType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;

/**
 * A DTO for the MailLetterRequest.
 */
public class MailLetterDTO {

  /**
   *
   */
  private static final long serialVersionUID = 4989832367498792194L;

  private Long id;

  private String title;

  @NotNull
  private LetterType letterType;

  @Lob
  private String text;

  private Boolean payment;

  private Boolean currentPeriod;

  private Boolean previousPeriod;

  private Long templateId;

  private String templateTitle;

  private Set<PersonGroupDTO> personGroups = new HashSet<>();

  private List<PersonDTO> persons = new ArrayList<>();

  private String subject;

  private String mailText;

  private String attachmentFileName;

  private Boolean saveInLetter = false;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public LetterType getLetterType() {
    return letterType;
  }

  public void setLetterType(LetterType letterType) {
    this.letterType = letterType;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Boolean getPayment() {
    return payment;
  }

  public void setPayment(Boolean payment) {
    this.payment = payment;
  }

  public Boolean getCurrentPeriod() {
    return currentPeriod;
  }

  public void setCurrentPeriod(Boolean currentPeriod) {
    this.currentPeriod = currentPeriod;
  }

  public Boolean getPreviousPeriod() {
    return previousPeriod;
  }

  public void setPreviousPeriod(Boolean previousPeriod) {
    this.previousPeriod = previousPeriod;
  }

  public Long getTemplateId() {
    return templateId;
  }

  public void setTemplateId(Long templateId) {
    this.templateId = templateId;
  }

  public String getTemplateTitle() {
    return templateTitle;
  }

  public void setTemplateTitle(String templateTitle) {
    this.templateTitle = templateTitle;
  }

  public Set<PersonGroupDTO> getPersonGroups() {
    return personGroups;
  }

  public void setPersonGroups(Set<PersonGroupDTO> personGroups) {
    this.personGroups = personGroups;
  }

  public List<PersonDTO> getPersons() {
    return persons;
  }

  public void setPersons(List<PersonDTO> persons) {
    this.persons = persons;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getMailText() {
    return mailText;
  }

  public void setMailText(String mailText) {
    this.mailText = mailText;
  }

  public String getAttachmentFileName() {
    return attachmentFileName;
  }

  public void setAttachmentFileName(String attachmentFileName) {
    this.attachmentFileName = attachmentFileName;
  }

  public Boolean isSaveInLetter() {
    return saveInLetter;
  }

  public void setSaveInLetter(Boolean saveInLetter) {
    this.saveInLetter = saveInLetter;
  }

  @Override
  public String toString() {
    return "MailLetterDTO{" +
      "id=" + id +
      ", title='" + title + '\'' +
      ", letterType=" + letterType +
      ", text='" + text + '\'' +
      ", payment=" + payment +
      ", currentPeriod=" + currentPeriod +
      ", previousPeriod=" + previousPeriod +
      ", templateId=" + templateId +
      ", templateTitle='" + templateTitle + '\'' +
      ", personGroups=" + personGroups +
      ", persons=" + persons +
      ", subject='" + subject + '\'' +
      ", mailText='" + mailText + '\'' +
      ", attachmentFileName='" + attachmentFileName + '\'' +
      ", saveInLetter=" + saveInLetter +
      '}';
  }
}
