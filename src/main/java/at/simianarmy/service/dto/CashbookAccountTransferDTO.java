package at.simianarmy.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * DTO for CashbookAccount Transfers.
 */
public class CashbookAccountTransferDTO implements Serializable {

  @NotNull
  private Long cashbookAccountFromId;

  @NotNull
  private Long cashbookAccountToId;

  @NotNull
  @DecimalMin(value = "0")
  private BigDecimal amount;

  public Long getCashbookAccountFromId() {
    return cashbookAccountFromId;
  }

  public void setCashbookAccountFromId(Long cashbookAccountFromId) {
    this.cashbookAccountFromId = cashbookAccountFromId;
  }

  public Long getCashbookAccountToId() {
    return cashbookAccountToId;
  }

  public void setCashbookAccountToId(Long cashbookAccountToId) {
    this.cashbookAccountToId = cashbookAccountToId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CashbookAccountTransferDTO that = (CashbookAccountTransferDTO) o;
    return cashbookAccountFromId.equals(that.cashbookAccountFromId) &&
      cashbookAccountToId.equals(that.cashbookAccountToId) &&
      amount.equals(that.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cashbookAccountFromId, cashbookAccountToId, amount);
  }

  @Override
  public String toString() {
    return "CashbookAccountTransferDTO{" +
      "cashbookAccountFromId=" + getCashbookAccountFromId() + "" +
      ", cashbookAccountToId=" + getCashbookAccountToId() + "" +
      ", amount='" + getAmount() + "'" +
      "}";
  }
}
