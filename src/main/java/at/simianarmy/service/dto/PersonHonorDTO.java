package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the PersonHonor entity.
 */
public class PersonHonorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6098876235804243175L;

	private Long id;

	@NotNull
	private LocalDate date;

	private Long honorId;

	private Long personId;

	private String honorName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Long getHonorId() {
		return honorId;
	}

	public void setHonorId(Long honorId) {
		this.honorId = honorId;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getHonorName() {
		return honorName;
	}

	public void setHonorName(String honorName) {
		this.honorName = honorName;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		PersonHonorDTO personHonorDTO = (PersonHonorDTO) obj;

		if (!Objects.equals(id, personHonorDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonHonorDTO{" + "id=" + id + ", date='" + date + "'" + '}';
	}
}
