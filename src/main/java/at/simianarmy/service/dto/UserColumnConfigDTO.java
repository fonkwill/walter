package at.simianarmy.service.dto;

import java.util.Objects;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class UserColumnConfigDTO {
  private Long id;

  @NotNull
  private String entity;

  @NotNull
  private String columnName;

  @NotNull
  private Boolean visible;

  @NotNull
  @Min(value = 1)
  private Integer position;

  @NotNull
  private String columnDisplayName;

  @NotNull
  private String sortByName;

  @NotNull
  private Long columnConfigId;

  @NotNull
  private Long userId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEntity() {
    return entity;
  }

  public void setEntity(String entity) {
    this.entity = entity;
  }

  public String getColumnName() {
    return columnName;
  }

  public void setColumnName(String columnName) {
    this.columnName = columnName;
  }

  public Boolean isVisible() {
    return visible;
  }

  public void setVisible(Boolean visible) {
    this.visible = visible;
  }

  public Long getColumnConfigId() { return columnConfigId; }

  public void setColumnConfigId(Long columnConfigId) { this.columnConfigId = columnConfigId; }

  public Long getUserId() { return userId; }

  public void setUserId(Long userId) { this.userId = userId; }

  public Integer getPosition() { return position; }

  public void setPosition(Integer position) { this.position = position; }

  public String getColumnDisplayName() { return this.columnDisplayName; }

  public void setColumnDisplayName(String columnDisplayName) { this.columnDisplayName = columnDisplayName; }

  public String getSortByName() { return this.sortByName; }

  public void setSortByName(String sortByName) { this.sortByName = sortByName; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    UserColumnConfigDTO userColumnConfigDTO = (UserColumnConfigDTO) o;
    if (userColumnConfigDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), userColumnConfigDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "UserColumnConfigDTO{" +
      "id=" + getId() +
      ", entity='" + getEntity() + "'" +
      ", columnName='" + getColumnName() + "'" +
      ", columnDisplayName='" + getColumnDisplayName() + "'" +
      ", visible='" + isVisible() + "'" +
      ", columnConfigId=" + getColumnConfigId() +
      ", userId=" + getUserId() +
      ", position=" + getPosition() +
      ", sortByName='" + getSortByName() + "'" +
      "}";
  }
}
