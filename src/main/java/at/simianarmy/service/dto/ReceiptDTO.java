package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

/**
 * A DTO for the Receipt entity.
 */
public class ReceiptDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 92376901549239847L;

	private Long id;

	@NotNull
	private Long cashbookEntryId;

	@NotNull
	private LocalDate date;

	private String note;

	@NotNull
	private String title;

	private String file;

	@NotNull
	private String identifier;

	@NotNull
	private Boolean overwriteIdentifier;

	private MultipartFile uploadedFile;

	private Long companyId;

	private String companyName;

	private Integer entriesCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCashbookEntryId() {
		return cashbookEntryId;
	}

	public void setCashbookEntryId(Long cashbookEntryId) {
		this.cashbookEntryId = cashbookEntryId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Boolean isOverwriteIdentifier() { return this.overwriteIdentifier; }

	public void setOverwriteIdentifier(Boolean overwriteIdentifier) { this.overwriteIdentifier = overwriteIdentifier; }

	public MultipartFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(MultipartFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getEntriesCount() {
		return entriesCount;
	}

	public void setEntriesCount(Integer entriesCount) {
		this.entriesCount = entriesCount;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ReceiptDTO receiptDTO = (ReceiptDTO) obj;

		if (!Objects.equals(id, receiptDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ReceiptDTO{" + "id=" + id + ", date='" + date + "'" + ", note='" + note + "'" + ", title='" + title
				+ "'" + ", file='" + file + "'" + '}';
	}
}
