package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the OfficialAppointment entity.
 */
public class OfficialAppointmentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -460990202726700340L;

	private Long id;

	@NotNull
	private String organizerName;

	@NotNull
	private String organizerAddress;

	@NotNull
	private Boolean isHeadQuota;

	private Long reportId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrganizerName() {
		return organizerName;
	}

	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}

	public String getOrganizerAddress() {
		return organizerAddress;
	}

	public void setOrganizerAddress(String organizerAddress) {
		this.organizerAddress = organizerAddress;
	}

	public Boolean getIsHeadQuota() {
		return isHeadQuota;
	}

	public void setIsHeadQuota(Boolean isHeadQuota) {
		this.isHeadQuota = isHeadQuota;
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		OfficialAppointmentDTO officialAppointmentDTO = (OfficialAppointmentDTO) obj;

		if (!Objects.equals(id, officialAppointmentDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "OfficialAppointmentDTO{" + "id=" + id + ", organizerName='" + organizerName + "'"
				+ ", organizerAddress='" + organizerAddress + "'" + ", isHeadQuota='" + isHeadQuota + "'" + '}';
	}
}
