package at.simianarmy.service.dto;

import java.util.Collection;
import java.util.List;

public class PersonCollectionDTO {

  private List<PersonDTO> persons;

  private List<MemberDTO> members;

  public List<PersonDTO> getPersons() {
    return persons;
  }

  public void setPersons(List<PersonDTO> persons) {
    this.persons = persons;
  }

  public List<MemberDTO> getMembers() {
    return members;
  }

  public void setMembers(List<MemberDTO> members) {
    this.members = members;
  }

  public boolean add(PersonDTO personDTO) {
    return persons.add(personDTO);
  }

  public boolean addAllPersons(Collection<PersonDTO> c) {
    return persons.addAll(c);
  }

  public boolean add(MemberDTO memberDTO) {
    return members.add(memberDTO);
  }

  public boolean addAllMembers(Collection<? extends MemberDTO> c) {
    return members.addAll(c);
  }





}
