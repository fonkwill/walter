package at.simianarmy.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Notification entity.
 */
public class NotificationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5536439015018572051L;

	private Long id;

	private Long entityId;

	@NotNull
	private ZonedDateTime createdAt;

	private Long readByUserId;
	private String readByUserFirstName;
	private String readByUserLastName;

	private Long notificationRuleId;

	private String notificationRuleEntity;
	private String notificationRuleMessageGroup;
	private String notificationRuleMessageSingle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public Long getReadByUserId() {
		return readByUserId;
	}

	public void setReadByUserId(Long userId) {
		this.readByUserId = userId;
	}

	public String getReadByUserFirstName() {
		return readByUserFirstName;
	}

	public void setReadByUserFirstName(String readByUserFirstName) {
		this.readByUserFirstName = readByUserFirstName;
	}

	public String getReadByUserLastName() {
		return readByUserLastName;
	}

	public void setReadByUserLastName(String readByUserLastName) {
		this.readByUserLastName = readByUserLastName;
	}

	public Long getNotificationRuleId() {
		return notificationRuleId;
	}

	public void setNotificationRuleId(Long notificationRuleId) {
		this.notificationRuleId = notificationRuleId;
	}

	public String getNotificationRuleEntity() {
		return notificationRuleEntity;
	}

	public void setNotificationRuleEntity(String notificationRuleEntity) {
		this.notificationRuleEntity = notificationRuleEntity;
	}

	public String getNotificationRuleMessageGroup() {
		return notificationRuleMessageGroup;
	}

	public void setNotificationRuleMessageGroup(String notificationRuleMessageGroup) {
		this.notificationRuleMessageGroup = notificationRuleMessageGroup;
	}

	public String getNotificationRuleMessageSingle() {
		return notificationRuleMessageSingle;
	}

	public void setNotificationRuleMessageSingle(String notificationRuleMessageSingle) {
		this.notificationRuleMessageSingle = notificationRuleMessageSingle;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		NotificationDTO notificationDTO = (NotificationDTO) obj;

		if (!Objects.equals(id, notificationDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "NotificationDTO{" + "id=" + id + ", entityId='" + entityId + "'" + ", createdAt='" + createdAt + "'"
				+ '}';
	}
}
