package at.simianarmy.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the MembershipFeeAmount entity.
 */
public class MembershipFeeAmountDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4929045223059662924L;

	private Long id;

	@NotNull
	private LocalDate beginDate;

	private LocalDate endDate;

	@NotNull
	private BigDecimal amount;

	private Long membershipFeeId;

	private String membershipFeeDescription;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getMembershipFeeId() {
		return membershipFeeId;
	}

	public void setMembershipFeeId(Long membershipFeeId) {
		this.membershipFeeId = membershipFeeId;
	}

	public String getMembershipFeeDescription() {
		return membershipFeeDescription;
	}

	public void setMembershipFeeDescription(String membershipFeeDescription) {
		this.membershipFeeDescription = membershipFeeDescription;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		MembershipFeeAmountDTO membershipFeeAmountDTO = (MembershipFeeAmountDTO) obj;

		if (!Objects.equals(id, membershipFeeAmountDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MembershipFeeAmountDTO{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate
				+ "'" + ", amount='" + amount + "'" + '}';
	}
}
