package at.simianarmy.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import at.simianarmy.domain.enumeration.CustomizationName;

/**
 * A DTO for the Customization entity.
 */
public class CustomizationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2561579889265542554L;

	private Long id;

	@NotNull
	private CustomizationName name;

	@NotNull
	private String data;

	private String text;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CustomizationName getName() {
		return name;
	}

	public void setName(CustomizationName name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		CustomizationDTO customizationDTO = (CustomizationDTO) o;
		if (customizationDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), customizationDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "CustomizationDTO{" + "id=" + getId() + ", name='" + getName() + "'" + ", data='" + getData() + "'"
				+ ", text='" + getText() + "'" + "}";
	}
}
