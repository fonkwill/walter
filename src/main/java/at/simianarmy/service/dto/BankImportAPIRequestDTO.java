package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;

public class BankImportAPIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5288513781525817219L;

	@NotNull
	private String username;

	@NotNull
	private String password;

	@NotNull
	private Long cashbookAccountId;

	@NotNull
	private Date from;

	@NotNull
	private Date to;

  private Long cashbookCategoryId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

  public Long getCashbookAccountId() {
    return cashbookAccountId;
  }

  public void setCashbookAccountId(Long cashbookAccountId) {
    this.cashbookAccountId = cashbookAccountId;
  }

  public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

  public Long getCashbookCategoryId() {
    return cashbookCategoryId;
  }

  public void setCashbookCategoryId(Long cashbookCategoryId) {
    this.cashbookCategoryId = cashbookCategoryId;
  }

  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		BankImportAPIRequestDTO dto = (BankImportAPIRequestDTO) o;

		if (!Objects.equals(username, dto.username) || !Objects.equals(password, dto.password)
				|| !Objects.equals(from, dto.from) || !Objects.equals(to, dto.to)
				|| !Objects.equals(cashbookAccountId, dto.cashbookAccountId))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, password, from, to, cashbookAccountId);
	}

	@Override
	public String toString() {
		return "BankImportAPIRequestDTO{" + "from='" + from + "'" + ", to='" + to + "'" + ", cashbookAccountId='"
				+ cashbookAccountId + "'" + '}';
	}

}
