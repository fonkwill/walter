package at.simianarmy.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

/**
 * A DTO for the Composer entity.
 */
public class ComposerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6187980013602272568L;

	private Long id;

	@NotNull
	private String name;

	private Set<CompositionDTO> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CompositionDTO> getCompositions() {
		return compositions;
	}

	public void setCompositions(Set<CompositionDTO> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		ComposerDTO composerDTO = (ComposerDTO) obj;

		if (!Objects.equals(id, composerDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ComposerDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
