package at.simianarmy.service;

import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;
import at.simianarmy.service.mapper.MembershipFeeForPeriodMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Service Implementation for managing MembershipFeeForPeriod.
 */
@Service
@Transactional
public class MembershipFeeForPeriodService {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeForPeriodService.class);

	@Inject
	private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;

	@Inject
	private MembershipFeeForPeriodMapper membershipFeeForPeriodMapper;

	/**
	 * Save a membershipFeeForPeriod.
	 *
	 * @param membershipFeeForPeriodDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MembershipFeeForPeriodDTO save(MembershipFeeForPeriodDTO membershipFeeForPeriodDTO) {
		log.debug("Request to save MembershipFeeForPeriod : {}", membershipFeeForPeriodDTO);
		MembershipFeeForPeriod membershipFeeForPeriod = membershipFeeForPeriodMapper
				.membershipFeeForPeriodDTOToMembershipFeeForPeriod(membershipFeeForPeriodDTO);
		membershipFeeForPeriod = membershipFeeForPeriodRepository.save(membershipFeeForPeriod);
		MembershipFeeForPeriodDTO result = membershipFeeForPeriodMapper
				.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod);
		return result;
	}

	/**
	 * Get all the membershipFeeForPeriods.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MembershipFeeForPeriodDTO> findAll(Pageable pageable) {
		log.debug("Request to get all MembershipFeeForPeriods");
		Page<MembershipFeeForPeriod> result = membershipFeeForPeriodRepository.findAll(pageable);
		return result.map(membershipFeeForPeriod -> membershipFeeForPeriodMapper
				.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod));
	}

	/**
	 * Get one membershipFeeForPeriod by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MembershipFeeForPeriodDTO findOne(Long id) {
		log.debug("Request to get MembershipFeeForPeriod : {}", id);
		MembershipFeeForPeriod membershipFeeForPeriod = membershipFeeForPeriodRepository.findById(id).orElse(null);
		MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper
				.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod);
		return membershipFeeForPeriodDTO;
	}

	/**
	 * Delete the membershipFeeForPeriod by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete MembershipFeeForPeriod : {}", id);
		membershipFeeForPeriodRepository.deleteById(id);
	}
}
