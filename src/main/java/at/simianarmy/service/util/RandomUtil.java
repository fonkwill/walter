package at.simianarmy.service.util;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtil {

	private static final int DEF_COUNT = 20;
	private static final int DEF_SECRET_KEY_COUNT = 8;

	private RandomUtil() {
	}

	/**
	 * Generates a password.
	 *
	 * @return the generated password
	 */
	public static String generatePassword() {
		return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
	}

	/**
	 * Generates an activation key.
	 *
	 * @return the generated activation key
	 */
	public static String generateActivationKey() {
		return RandomStringUtils.randomNumeric(DEF_COUNT);
	}

	/**
	 * Generates a reset key.
	 *
	 * @return the generated reset key
	 */
	public static String generateResetKey() {
		return RandomStringUtils.randomNumeric(DEF_COUNT);
	}

  /**
   * Generates a secret key.
   *
   * @return the generated secret key
   */
	public static String generateSecretKey() { return RandomStringUtils.randomAlphanumeric(DEF_SECRET_KEY_COUNT); }
}
