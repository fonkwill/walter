package at.simianarmy.service.util;

public abstract class BackupUtil {

	public abstract void backup();

	public static BackupUtil getInstance(String database) {
		if (database.equals("H2")) {
			return new H2BackupUtil();
		} else {
			return null;
		}
	}

}
