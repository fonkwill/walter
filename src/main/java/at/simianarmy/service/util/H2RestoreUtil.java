package at.simianarmy.service.util;

import java.io.File;

import org.h2.tools.Restore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class H2RestoreUtil extends RestoreUtil {

	private static final Logger log = LoggerFactory.getLogger(H2RestoreUtil.class);

	@Value("${walter.db.backup-file:./backup/db.zip}")
	private String backupFile;

	@Value("${walter.db.path:./db}")
	private String dbPath;

	@Value("${walter.db.name:db}")
	private String dbName;

	@Override
	public void restore() {

		File backup = new File(backupFile);
		File db = new File(dbPath + dbName + ".mv.db");

		if (backup.exists() && !db.exists()) {
			log.debug("Creating restore for db {} from file {}", backupFile, dbPath + dbName);
			Restore.execute(backupFile, dbPath, dbName);

		}
	}

}
