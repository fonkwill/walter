package at.simianarmy.service.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class H2BackupUtil extends BackupUtil {
	private static Logger logger = LoggerFactory.getLogger(H2BackupUtil.class);

	@Inject
	private DataSource ds;

	@Value("${walter.db.backup-file:./backup/db.zip}")
	private String backupFile;

	@Override
	public void backup() {
		Connection c = null;
		try {
			c = ds.getConnection();
			Statement stmt = c.createStatement();
			logger.info("Going to backup to " + backupFile);
			stmt.execute("backup to '" + backupFile + "'");

		} catch (SQLException e) {
			logger.error("SQL Exception", e);
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (SQLException e) {
					// do nothing
				}
			}
		}

	}

}
