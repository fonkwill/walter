package at.simianarmy.service.util;

public abstract class RestoreUtil {

	public abstract void restore();

	public static RestoreUtil getInstance(String database) {
		if (database.equals("H2")) {
			return new H2RestoreUtil();
		} else {
			return null;
		}
	}

}
