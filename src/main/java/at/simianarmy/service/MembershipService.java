package at.simianarmy.service;

import at.simianarmy.domain.Customization;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.repository.CustomizationRepository;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MembershipFeeForPeriodMapper;
import at.simianarmy.service.mapper.MembershipMapper;
import at.simianarmy.service.specification.MembershipSpecification;
import at.simianarmy.service.specification.PersonSpecifications;

import java.util.List;
import java.util.Set;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Membership.
 */
@Service
@Transactional
public class MembershipService {

	private final Logger log = LoggerFactory.getLogger(MembershipService.class);

	@Inject
	private MembershipRepository membershipRepository;

	@Inject
	private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;

	@Inject
	private MembershipFeeForPeriodService membershipFeeForPeriodService;

	@Inject
	private MembershipMapper membershipMapper;

	@Inject
	private MembershipFeeForPeriodMapper membershipFeeForPeriodMapper;

	@Inject
	private PersonRepository personRepository;

	@Inject
	private CustomizationService customizationService;

	@Inject
	private PersonGroupRepository personGroupRepository;

	/**
	 * Save a membership.
	 *
	 * @param membershipDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MembershipDTO save(MembershipDTO membershipDTO) {
		log.debug("Request to save Membership : {}", membershipDTO);
		Membership membership = membershipMapper.membershipDTOToMembership(membershipDTO);
		if (membershipDTO.getBeginDate() != null && membershipDTO.getEndDate() != null) {
			if (membershipDTO.getBeginDate().isAfter(membershipDTO.getEndDate())) {
				throw new ServiceValidationException(ServiceErrorConstants.membershipEndBeforeBegin, null);
			}
		}
		if (membership.getBeginDate().getMonthValue() != 1 || membership.getBeginDate().getDayOfMonth() != 1) {
			throw new ServiceValidationException(ServiceErrorConstants.membershipBeginDate, null);
		}
		if (membership.getEndDate() != null
				&& (membership.getEndDate().getMonthValue() != 12 || membership.getEndDate().getDayOfMonth() != 31)) {
			throw new ServiceValidationException(ServiceErrorConstants.membershipEndDate, null);
		}
		if (membership.getPerson() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.membershipNoPersonGiven, null);
		}
		Page<Membership> allMembershipsPage = membershipRepository.findByPerson(membership.getPerson().getId(), null);
		if (allMembershipsPage != null) {
			List<Membership> allAmounts = allMembershipsPage.getContent();
			for (Membership m : allAmounts) {
				if (m.getId() == membership.getId()) {
					continue;
				}
				if (m.getEndDate() == null && membership.getEndDate() == null) {
					if (membership.getBeginDate().isBefore(m.getBeginDate())) {
						membership.setEndDate(m.getBeginDate().minusDays(1));
					} else {
						m.setEndDate(membership.getBeginDate().minusDays(1));
						m = membershipRepository.save(m);
					}
				} else if (m.getEndDate() == null && membership.getEndDate() != null) {
					if (membership.getBeginDate().isAfter(m.getBeginDate())) {
						m.setEndDate(membership.getBeginDate().minusDays(1));
						m = membershipRepository.save(m);
					}
					if (membership.getBeginDate().isBefore(m.getBeginDate())
							&& membership.getEndDate().isAfter(m.getBeginDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}
				} else if (m.getEndDate() != null && membership.getEndDate() == null) {
					if (membership.getBeginDate().isAfter(m.getBeginDate())
							&& membership.getBeginDate().isBefore(m.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}
					if (membership.getBeginDate().isBefore(m.getBeginDate())) {
						membership.setEndDate(m.getBeginDate().minusDays(1));
					}
				} else if (m.getEndDate() != null && membership.getEndDate() != null) {
					if (membership.getBeginDate().isAfter(m.getBeginDate())
							&& membership.getBeginDate().isBefore(m.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}
					if (membership.getBeginDate().isBefore(m.getBeginDate())
							&& membership.getEndDate().isAfter(m.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}
					if (membership.getEndDate().isAfter(m.getBeginDate())
							&& membership.getEndDate().isBefore(m.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}
				}
			}
		}
		membership = membershipRepository.save(membership);
		MembershipDTO result = membershipMapper.membershipToMembershipDTO(membership);
		// create MembershipFeeForPeriod
		Page<Membership> membershipsForPersonPage = membershipRepository.findByPerson(membership.getPerson().getId(),
				null);
		List<MembershipFeeForPeriod> ffpList = membershipFeeForPeriodRepository.findAll();
		Integer year = Calendar.getInstance().get(Calendar.YEAR);
		if (membership.getMembershipFee() == null && membership.getBeginDate().getYear() <= year
				&& (membership.getMembershipFeeForPeriods() != null
						|| !membership.getMembershipFeeForPeriods().isEmpty())
				&& (membership.getEndDate() == null || membership.getEndDate().getYear() >= year)) {
			for (MembershipFeeForPeriod ffpOld : membership.getMembershipFeeForPeriods()) {
				if (ffpOld.getYear().equals(year) && ffpOld.getNr().equals(1)) {
					membershipFeeForPeriodService.delete(ffpOld.getId());
				}
			}
		}
		if (membershipsForPersonPage != null && (ffpList != null)) {
			List<Membership> membershipsForPerson = membershipsForPersonPage.getContent();
			for (Membership m : membershipsForPerson) {
				for (MembershipFeeForPeriod ffp : ffpList) {
					if (ffp.getMembership().equals(m) && ffp.getYear().equals(year) && ffp.getNr().equals(1)) {
						return result;
					}
				}
			}
		}
		if (membership.getMembershipFee() != null && membership.getBeginDate().getYear() <= year
				&& (membership.getEndDate() == null || membership.getEndDate().getYear() >= year)) {
			log.info("Adding new MembershipFeeForPeriod for Membership with id {}", membership.getId());
			String membershipId = String.format("%05d", membership.getId());
			String nr = "01";
			String yearStr = Integer.toString(year);
			String referenceCode = membershipId + nr + yearStr;
			MembershipFeeForPeriod ffpNew = new MembershipFeeForPeriod();
			ffpNew.setMembership(membership);
			ffpNew.setNr(1);
			ffpNew.setPaid(false);
			ffpNew.setReferenceCode(referenceCode);
			ffpNew.setYear(year);
			MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper
					.membershipFeeForPeriodToMembershipFeeForPeriodDTO(ffpNew);
			membershipFeeForPeriodService.save(membershipFeeForPeriodDTO);
			membership.addMembershipFeeForPeriods(ffpNew);
		}
		membership = membershipRepository.save(membership);
		result = membershipMapper.membershipToMembershipDTO(membership);

		// update membershipGroup
		updateMembershipGroup(membership.getPerson().getId());

		return result;
	}

	/**
	 * Get all the memberships.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MembershipDTO> findAll(Long personId, Pageable pageable) {
		log.debug("Request to get all Memberships");
		Specification<Membership> searchSpec = Specification
				.where(PersonSpecifications.hasPersonIdFromMembership(personId));

		Page<Membership> result = membershipRepository.findAll(searchSpec, pageable);
		return result.map(membership -> membershipMapper.membershipToMembershipDTO(membership));
	}

	/**
	 * Updates the given person concerning the membership group
	 * 
	 * @param personId
	 *            The id of the person to check
	 */
	@Transactional
	public void updateMembershipGroup(Long personId) {
		if (personId == null) {
			return;
		}

		Person p = getUpdatedPersonWithMembershipGroup(personId);
		// if it's null - nothing to do
		if (p != null) {
			personRepository.save(p);
		}

	}

	/**
	 * It's possible that there's a persongroup which should be assigned if the
	 * person has a membership. This persongroup is stored as customization. If
	 * that's the case this method checks if the group has to be assigned or removed
	 * (based on the current Date)
	 * 
	 * @param personId
	 *            The id of the person to check
	 * @return The updated person, null if there's no need for an update
	 */
	public Person getUpdatedPersonWithMembershipGroup(Long personId) {
		
		Long membershipPersongroup = customizationService.getMemberPersonGroup();
		
		if (membershipPersongroup == null) {
			// no need for an update
			return null;
		}

		Person p = personRepository.findById(personId).orElse(null);
		if (p == null) {
			return null;
		}

		// check if given person has membership at given date
		List<Membership> memberships = membershipRepository.findByPersonAndDueDate(personId, LocalDate.now());
		Set<PersonGroup> personGroups = p.getPersonGroups();
		boolean update = false;

		PersonGroup pgToHandle = null;
		for (PersonGroup pg : personGroups) {
			if (pg.getId().equals(membershipPersongroup)) {
				pgToHandle = pg;
			}
		}
		if (memberships == null || memberships.isEmpty()) {
			// remove group if present
			if (pgToHandle != null) {
				personGroups.remove(pgToHandle);
				update = true;
			}
		} else {
			// add group if not present
			if (pgToHandle == null) {
				PersonGroup mPg = personGroupRepository.findById(membershipPersongroup).orElse(null);
				if (mPg != null) {
					personGroups.add(mPg);
					update = true;
				}
			}
		}

		// update person
		if (update) {
			p.setPersonGroups(personGroups);
			return p;
		} else {
			return null;
		}

	}

	/**
	 * Get one membership by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MembershipDTO findOne(Long id) {
		log.debug("Request to get Membership : {}", id);
		Membership membership = membershipRepository.findById(id).orElse(null);
		MembershipDTO membershipDTO = membershipMapper.membershipToMembershipDTO(membership);
		return membershipDTO;
	}

	/**
	 * Delete the membership by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Membership : {}", id);
		Membership membership = membershipRepository.findById(id).orElse(null);
		if (membership.getMembershipFeeForPeriods() == null || membership.getMembershipFeeForPeriods().isEmpty()) {
			membershipRepository.deleteById(id);
			updateMembershipGroup(membership.getPerson().getId());
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.membershipNotDeletable, null);
		}
	}

	/**
	 * Get all the MembershipFeeForPeriod for a Person.
	 * 
	 * @param personId
	 *            the Person, which we want to retrieve periods from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	public Page<MembershipFeeForPeriodDTO> findByPerson(Long personId, Pageable pageable) {
		log.debug("Request to get all Memberships for a Person");
		Page<Membership> membershipsForPersonPage = membershipRepository.findByPerson(personId, null);
		List<MembershipFeeForPeriod> ffpList = membershipFeeForPeriodRepository.findAll();
		List<MembershipFeeForPeriod> result = new ArrayList<MembershipFeeForPeriod>();
		if (membershipsForPersonPage != null && (ffpList != null)) {
			List<Membership> membershipsForPerson = membershipsForPersonPage.getContent();
			for (Membership m : membershipsForPerson) {
				for (MembershipFeeForPeriod ffp : ffpList) {
					if (ffp.getMembership().equals(m)) {
						result.add(ffp);
					}
				}
			}
		}
		List<MembershipFeeForPeriodDTO> dtoList = membershipFeeForPeriodMapper
				.membershipFeeForPeriodsToMembershipFeeForPeriodDTOs(result);
		final Page<MembershipFeeForPeriodDTO> resultPage = new PageImpl<>(dtoList);
		return resultPage;
	}

	/**
	 * Creates a memberhsipfee for a given period for a person
	 * 
	 * @param personId
	 *            The id of the concerning person
	 * @param year
	 *            the period
	 */
	@Transactional
	public List<MembershipDTO> createMembershipFeeForPeriodFor(Long personId, int year) {
		if (personId == null || year == 0) {
			throw new IllegalArgumentException("personid or year null");
		}

		// check if there are memberships for which fees are possible (resp not created)
		Specification<Membership> specm = Specification
				.where(MembershipSpecification.hasNoMembershipFeeForPeriodIn(year))
				.and(PersonSpecifications.hasPersonIdFromMembership(personId));

		List<Membership> memberships = membershipRepository.findAll(specm);
		if (memberships == null || memberships.isEmpty()) {
			throw new ServiceValidationException(ServiceErrorConstants.noMembershipForMembershipFeeForPeriod, null);
		}
		List<MembershipDTO> result = new ArrayList<>();
		Membership mToReturn;
		for (Membership m : memberships) {
			mToReturn = createMembershipForPeriod(year, m);
			result.add(membershipMapper.membershipToMembershipDTO(mToReturn));
		}
		return result;

	}

  public void createMembershipFeeForPeriodForMembership() {
    log.info("Checking for missing MembershipFeeForPeriods");
    int year = LocalDate.now().getYear();
    Specification<Membership> specs = MembershipSpecification.hasNoMembershipFeeForPeriodIn(year);
    List<Membership> memberships = membershipRepository.findAll(specs);
    for (Membership membership : memberships) {
      createMembershipForPeriod(year, membership);
    }
  }

	public Membership createMembershipForPeriod(int year, Membership membership) {
		String membershipIdString;
		String nrString;
		String referenceCode;
		log.info("Adding new MembershipFeeForPeriod for Membership with id {}", membership.getId());
		MembershipFeeForPeriod membershipFeeForPeriod = new MembershipFeeForPeriod();
		membershipFeeForPeriod.setMembership(membership);
		membershipFeeForPeriod.setNr(1);
		membershipFeeForPeriod.setYear(year);
		membershipFeeForPeriod.setPaid(false);

		membershipIdString = String.format("%05d", membership.getId());
		nrString = String.format("%02d", 1);
		referenceCode = membershipIdString + nrString + year;

		membershipFeeForPeriod.setReferenceCode(referenceCode);
		membershipFeeForPeriod = membershipFeeForPeriodRepository.save(membershipFeeForPeriod);
		membership.addMembershipFeeForPeriods(membershipFeeForPeriod);
		return membershipRepository.save(membership);
	}


}
