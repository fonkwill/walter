package at.simianarmy.service;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Template;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.service.TemplateFileStoreService;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.dto.TemplateDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.TemplateMapper;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service Implementation for managing Template.
 */
@Service
@Transactional
public class TemplateService {

	private final Logger log = LoggerFactory.getLogger(TemplateService.class);

	@Inject
	private TemplateRepository templateRepository;

	@Inject
	private LetterRepository letterRepository;

	@Inject
	private TemplateMapper templateMapper;

	@Inject
  private TemplateFileStoreService fileStore;

	/**
	 * Save a template.
	 *
	 * @param templateDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public TemplateDTO save(TemplateDTO templateDTO) {
		log.debug("Request to save Template : {}", templateDTO);
		Template template = templateMapper.templateDTOToTemplate(templateDTO);
		Float defaultMarginLeft = (com.lowagie.text.Utilities
				.pointsToMillimeters(SerialLetterConstants.defaultMarginLeft)) / 10;
		Float defaultMarginRight = (com.lowagie.text.Utilities
				.pointsToMillimeters(SerialLetterConstants.defaultMarginRight)) / 10;
		Float defaultMarginTop = (com.lowagie.text.Utilities
				.pointsToMillimeters(SerialLetterConstants.defaultMarginTop)) / 10;
		;
		Float defaultMarginBottom = (com.lowagie.text.Utilities
				.pointsToMillimeters(SerialLetterConstants.defaultMarginBottom)) / 10;
		if (template.getMarginLeft() == null) {
			template.setMarginLeft(defaultMarginLeft);
		}
		if (template.getMarginRight() == null) {
			template.setMarginRight(defaultMarginRight);
		}
		if (template.getMarginTop() == null) {
			template.setMarginTop(defaultMarginTop);
		}
		if (template.getMarginBottom() == null) {
			template.setMarginBottom(defaultMarginBottom);
		}
		if (template.getMarginLeft() < 1 || template.getMarginLeft() > 10 || template.getMarginRight() < 1
				|| template.getMarginRight() > 10 || template.getMarginTop() < 1 || template.getMarginTop() > 10
				|| template.getMarginBottom() < 1 || template.getMarginBottom() > 10) {
			throw new ServiceValidationException(ServiceErrorConstants.templateMarginBorders, null);
		}

		if (template.getId() != null) {
			Template templateFromDb = templateRepository.findById(template.getId()).orElse(null);
			if (templateFromDb != null && templateFromDb.getFile() != null) {
				template.setFile(templateFromDb.getFile());
			}
		}

		if (templateDTO.getUploadedFile() != null) {
			MultipartFile file = templateDTO.getUploadedFile();

			if (!file.getContentType().equals("application/pdf")) {
				throw new ServiceValidationException(ServiceErrorConstants.templateNoPdf, null);
			} else {
				try {
					PdfReader reader = new PdfReader(file.getBytes());
					if (reader.getNumberOfPages() != 1) {
						throw new ServiceValidationException(ServiceErrorConstants.templateNumberOfPages, null);
					}
					if (reader.getPageSize(1).getHeight() < reader.getPageSize(1).getWidth()) {
						throw new ServiceValidationException(ServiceErrorConstants.templateNotPortraitOrientation,
								null);
					}
					int height = Math.round(reader.getPageSize(1).getHeight());
					int width = Math.round(reader.getPageSize(1).getWidth());
					int a4height = Math.round(PageSize.A4.getHeight());
					int a4width = Math.round(PageSize.A4.getWidth());
					if (height != a4height && width != a4width) {
						throw new ServiceValidationException(ServiceErrorConstants.templateNotA4, null);
					}
				} catch (IOException e) {
					log.debug("PdfReader could not read file. Exception: " + e);
					throw new ServiceValidationException(ServiceErrorConstants.templateNotSaved, null);
				}
			}
			try {
				if (template.getFile() != null) {
					fileStore.deleteTemplate(UUID.fromString(template.getFile()));
				}
				String filename = fileStore.saveTemplate(templateDTO.getUploadedFile());
				template.setFile(filename);
			} catch (FileStoreException exception) {
				throw new ServiceValidationException(ServiceErrorConstants.templateNotSaved, null);
			}
		}

		template = templateRepository.save(template);
		TemplateDTO result = templateMapper.templateToTemplateDTO(template);
		return result;
	}

	/**
	 * Get all the templates.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<TemplateDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Templates");
		Page<Template> result = templateRepository.findAll(pageable);
		return result.map(template -> templateMapper.templateToTemplateDTO(template));
	}

	/**
	 * Get one template by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public TemplateDTO findOne(Long id) {
		log.debug("Request to get Template : {}", id);
		Template template = templateRepository.findById(id).orElse(null);
		TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);
		return templateDTO;
	}

	/**
	 * Delete the template by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Template : {}", id);
		List<Letter> letterList = new ArrayList<Letter>();
		letterList = letterRepository.findAll();
		if (!letterList.isEmpty() || letterList != null) {
			for (Letter letter : letterList) {
				if (letter.getTemplate() != null && letter.getTemplate().getId() == id) {
					throw new ServiceValidationException(ServiceErrorConstants.templateNotDeletable, null);
				}
			}
		}
		Template template = templateRepository.findById(id).orElse(null);
		if (template.getFile() != null) {
			try {
				fileStore.deleteTemplate(UUID.fromString(template.getFile()));
			} catch (FileStoreException e) {
				throw new ServiceValidationException(ServiceErrorConstants.templateDeletionError, null);
			}
		}
		templateRepository.deleteById(id);
	}
}
