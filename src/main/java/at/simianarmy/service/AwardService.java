package at.simianarmy.service;

import at.simianarmy.domain.Award;
import at.simianarmy.repository.AwardRepository;
import at.simianarmy.service.dto.AwardDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.AwardMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Award.
 */
@Service
@Transactional
public class AwardService {

	private final Logger log = LoggerFactory.getLogger(AwardService.class);

	@Inject
	private AwardRepository awardRepository;

	@Inject
	private AwardMapper awardMapper;

	/**
	 * Save a award.
	 *
	 * @param awardDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AwardDTO save(AwardDTO awardDTO) {
		log.debug("Request to save Award : {}", awardDTO);
		Award award = awardMapper.awardDTOToAward(awardDTO);
		award = awardRepository.save(award);
		AwardDTO result = awardMapper.awardToAwardDTO(award);
		return result;
	}

	/**
	 * Get all the awards.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AwardDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Awards");
		Page<Award> result = awardRepository.findAll(pageable);
		return result.map(award -> awardMapper.awardToAwardDTO(award));
	}

	/**
	 * Get one award by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AwardDTO findOne(Long id) {
		log.debug("Request to get Award : {}", id);
		Award award = awardRepository.findById(id).orElse(null);
		AwardDTO awardDTO = awardMapper.awardToAwardDTO(award);
		return awardDTO;
	}

	/**
	 * Delete the award by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Award : {}", id);
		Award award = awardRepository.findById(id).orElse(null);
		if (award.getPersonAwards() == null || award.getPersonAwards().isEmpty()) {
			awardRepository.deleteById(id);
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.awardNotDeletable, null);
		}
	}
}
