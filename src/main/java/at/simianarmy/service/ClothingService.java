package at.simianarmy.service;

import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.repository.ClothingRepository;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.service.dto.ClothingDTO;
import at.simianarmy.service.dto.ClothingGroupDTO;
import at.simianarmy.service.dto.PersonClothingDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ClothingMapper;
import at.simianarmy.service.specification.ClothingSpecification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.List;

/**
 * Service Implementation for managing Clothing.
 */
@Service
@Transactional
public class ClothingService {

	private final Logger log = LoggerFactory.getLogger(ClothingService.class);

	@Inject
	private ClothingRepository clothingRepository;

	@Inject
	private ClothingMapper clothingMapper;

	@Inject
	private ClothingTypeRepository clothingTypeRepository;

	/**
	 * Save a clothing.
	 *
	 * @param clothingDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClothingDTO save(ClothingDTO clothingDTO) {
		log.debug("Request to save Clothing : {}", clothingDTO);
		Clothing clothing = clothingMapper.clothingDTOToClothing(clothingDTO);
		clothing = clothingRepository.save(clothing);
		ClothingDTO result = clothingMapper.clothingToClothingDTO(clothing);
		return result;
	}

	/**
	 * Get all the clothing.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClothingDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Clothing");
		Page<Clothing> result = clothingRepository.findAll(pageable);
		return result.map(clothing -> clothingMapper.clothingToClothingDTO(clothing));
	}

	/**
	 * Get one clothing by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClothingDTO findOne(Long id) {
		log.debug("Request to get Clothing : {}", id);
		Clothing clothing = clothingRepository.findById(id).orElse(null);
		ClothingDTO clothingDTO = clothingMapper.clothingToClothingDTO(clothing);
		return clothingDTO;
	}

	/**
	 * Delete the clothing by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Clothing : {}", id);
		clothingRepository.deleteById(id);
	}

	@Transactional
	public ClothingDTO getWithClothingGroupAndPersonClothing(ClothingGroupDTO clothingGroup,
			PersonClothingDTO personClothing) {
		log.debug("Request to get Clothing with Clothing Group {}", clothingGroup, personClothing);

		LocalDate begin = null;
		LocalDate end = null;

		if (personClothing == null || personClothing.getBeginDate() == null) {
			begin = LocalDate.now();
		} else {
			begin = personClothing.getBeginDate();
		}
		if (personClothing == null) {
			end = null;
		} else {
			end = personClothing.getEndDate();
		}

		ClothingType type = clothingTypeRepository.findById(clothingGroup.getTypeId()).orElse(null);
		Specification<Clothing> spec = ClothingSpecification.availableWith(begin, end, type, clothingGroup.getCsize());

		List<Clothing> available = clothingRepository.findAll(spec);

		ClothingDTO ret = new ClothingDTO();
		if (available.size() <= 0) {
			throw new ServiceValidationException(ServiceErrorConstants.clothingForThisClothingGroupNotAvailable, null);
		} else {
			ret = clothingMapper.clothingToClothingDTO(available.get(0));
		}

		return ret;
	}
}
