package at.simianarmy.service;

import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.enumeration.MailLogStatus;
import at.simianarmy.repository.MailLogRepository;
import at.simianarmy.service.dto.MailLogDTO;
import at.simianarmy.service.mapper.MailLogMapper;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for getting MailLogs.
 */
@Service
@Transactional
public class MailLogService {

  private final Logger log = LoggerFactory.getLogger(MailLogService.class);

  @Inject
  private MailLogRepository mailLogRepository;

  @Inject
  private MailLogMapper mailLogMapper;

  /**
   * Find all Maillogs for the Date and the given MailLogStatusList
   *
   * @param date
   * @param mailLogStatusList
   * @param pageable
   * @return the MailLogDTOs
   */
  @Transactional(readOnly = true)
  public Page<MailLogDTO> findByDateAndMailLogStatus(LocalDate date, List<MailLogStatus> mailLogStatusList, Pageable pageable) {

    log.debug("Request to get a Page of MailLogDTOs {} {}", date, mailLogStatusList);

    if (mailLogStatusList != null && mailLogStatusList.size() == 0) {
      mailLogStatusList = null;
    }

    Page<MailLog> result = this.mailLogRepository.findByDateAndMailLogStatus(date, mailLogStatusList, pageable);
    List<MailLogDTO> resultDTO = this.mailLogMapper.mailLogsToMailLogDTOs(result.getContent());

    return new PageImpl<MailLogDTO>(resultDTO, pageable, result.getTotalElements());
  }

}
