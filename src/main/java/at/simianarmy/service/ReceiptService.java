package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Receipt;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.service.ReceiptFileStoreService;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.ReceiptRepository;
import at.simianarmy.service.dto.ReceiptDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ReceiptMapper;
import at.simianarmy.service.specification.ReceiptSpecification;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Receipt.
 */
@Service
@Transactional
public class ReceiptService {

  private final static String REGEX_RUNNING_RECEIPT_CODES = "^([A-Za-z]{1,})([0-9]{1,})$";

	private final Logger log = LoggerFactory.getLogger(ReceiptService.class);

	@Inject
	private ReceiptRepository receiptRepository;

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private ReceiptMapper receiptMapper;

	@Inject
	private ReceiptFileStoreService fileStore;

	@Inject
  private CashbookAccountRepository cashbookAccountRepository;

	/**
	 * Save a receipt.
	 *
	 * @param receiptDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ReceiptDTO save(ReceiptDTO receiptDTO) throws ServiceValidationException {
		log.debug("Request to save Receipt : {}", receiptDTO);

		Receipt receipt = receiptMapper.receiptDTOToReceipt(receiptDTO);

		this.checkReceiptDate(receipt);

    Receipt receiptFromDb = this.getCurrentReceiptForReceipt(receipt);

    if (receiptFromDb != null) {
      receipt.setFile(receiptFromDb.getFile());
    }

    CashbookEntry cashbookEntry;

    if (receipt.getId() == null) {
      cashbookEntry = this.getCashbookEntryFromReceiptDTO(receiptDTO);
    } else {
      cashbookEntry = receiptFromDb.getInitialCashbookEntry();
    }

    receipt.setInitialCashbookEntry(cashbookEntry);

		//Identifier vergeben oder überschreiben
    if (receipt.getId() == null) {
		  //Neuer Beleg
      receipt = this.calculateIdentifiertForNewReceipt(receipt, cashbookEntry);
    } else {
		  //Bestehender Beleg
      receipt = this.calculateIdentifierForExistingReceipt(receipt, receiptFromDb, cashbookEntry);
    }

		if (receiptDTO.getUploadedFile() != null) {
			try {
				if (receipt.getFile() != null) {
					fileStore.deleteReceipt(UUID.fromString(receipt.getFile()));
				}
				String filename = fileStore.saveReceipt(receiptDTO.getUploadedFile());
				receipt.setFile(filename);
			} catch (FileStoreException exception) {
				// TODO: handling?
				exception.printStackTrace();
			}
		}

		receipt = receiptRepository.save(receipt);

		if (cashbookEntry != null) {
				cashbookEntry.setReceipt(receipt);
				cashbookEntryRepository.save(cashbookEntry);
		}

		ReceiptDTO result = receiptMapper.receiptToReceiptDTO(receipt);

		return result;
	}

	private void checkReceiptIdentifier(Receipt receipt) {

	  if (this.receiptRepository.findOneByIdentifier(receipt.getIdentifier()).isPresent()) {
	    throw new ServiceValidationException(ServiceErrorConstants.receiptIdentifierAlreadyUsed, null);
    }

    if (receipt.isOverwriteIdentifier() && receipt.getIdentifier().matches(REGEX_RUNNING_RECEIPT_CODES)) {
	    //Es wird überprüft, ob der Identifier ein Beleg-Kürzel aus einem Kassabuch-Konto verwendet
      //Wenn das so ist, darf das nicht zugelassen werden um die fortlaufende Nummerierung von
      //Begel-NRs zu garantieren

      Pattern p = Pattern.compile(REGEX_RUNNING_RECEIPT_CODES);
      Matcher m = p.matcher(receipt.getIdentifier());

      if (m.find()) {
        String receiptCode = m.group(1);

        if (this.cashbookAccountRepository.findOneByReceiptCode(receiptCode).isPresent()) {
          throw new ServiceValidationException(ServiceErrorConstants.autoReceiptIdentifierNotAllowed, null);
        }

      }

    }

  }

	private Receipt calculateIdentifiertForNewReceipt(Receipt receipt, CashbookEntry cashbookEntry) {
    if (receipt.getIdentifier() != null && !receipt.getIdentifier().equals("") && !receipt.isOverwriteIdentifier()) {
      throw new ServiceValidationException(ServiceErrorConstants.receiptIdentifierOverwrite, null);
    }

    if (!receipt.isOverwriteIdentifier()) {
      //Identifier muss automatisch vergeben werden
      CashbookAccount account = this.cashbookAccountRepository.findById(cashbookEntry.getCashbookAccount().getId()).get();

      receipt = this.setAndGetNextReceiptIdentifierForReceiptAndCashbookAccount(receipt, account);
    } else {
      this.checkReceiptIdentifier(receipt);
    }

    return receipt;
  }

  private Receipt calculateIdentifierForExistingReceipt(Receipt receipt, Receipt existingReceipt, CashbookEntry cashbookEntry) {
    if (!receipt.isOverwriteIdentifier() &&
      receipt.getIdentifier() != null &&
      !receipt.getIdentifier().equals("") &&
      !receipt.getIdentifier().equals(existingReceipt.getIdentifier())) {
      throw new ServiceValidationException(ServiceErrorConstants.receiptIdentifierOverwrite, null);
    }

    if (!receipt.isOverwriteIdentifier() && existingReceipt.isOverwriteIdentifier()) {
      //Wurde geändert, muss jetzt also vergeben werden
      CashbookAccount account = cashbookEntry.getCashbookAccount();
      receipt = this.setAndGetNextReceiptIdentifierForReceiptAndCashbookAccount(receipt, account);
    } else if (receipt.isOverwriteIdentifier() &&
      !receipt.getIdentifier().equals(existingReceipt.getIdentifier())) {
      this.checkReceiptIdentifier(receipt);
    }

    return receipt;
  }

	private CashbookEntry getCashbookEntryFromReceiptDTO(ReceiptDTO receiptDTO) {
    if (receiptDTO.getCashbookEntryId() != null) {
      CashbookEntry cashbookEntry = cashbookEntryRepository.findById(receiptDTO.getCashbookEntryId())
        .orElse(null);
      return cashbookEntry;
    } else {
      return null;
    }
  }

  private Receipt getCurrentReceiptForReceipt(Receipt receipt) {
    if (receipt.getId() != null) {
      Receipt receiptFromDb = receiptRepository.findById(receipt.getId()).orElse(null);
      return receiptFromDb;
    } else {
      return null;
    }
  }

  private Receipt setAndGetNextReceiptIdentifierForReceiptAndCashbookAccount(Receipt receipt, CashbookAccount cashbookAccount)
  {
    Integer nextNumber = cashbookAccount.getCurrentNumber() + 1;
    String nextIdentifier = cashbookAccount.getReceiptCode() + nextNumber.toString();
    receipt.setIdentifier(nextIdentifier);
    cashbookAccount.setCurrentNumber(nextNumber);
    this.cashbookAccountRepository.save(cashbookAccount);
    return receipt;
  }

	/**
	 * checks if the date is not in the future.
	 *
	 * @param receipt
	 *            the receipt
	 */
	private void checkReceiptDate(Receipt receipt) {
		if (receipt.getDate().isAfter(LocalDate.now())) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptDateInFuture, null);
		}
	}

	/**
	 * Get all the receipts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ReceiptDTO> findAll(String sword, Pageable pageable) {
		log.debug("Request to get all Receipts");

		ReceiptSpecification specification = new ReceiptSpecification();

		Page<Receipt> result = receiptRepository.findAll(specification.hasTitleOrCompany(sword).build(), pageable);

		return result.map(receipt -> receiptMapper.receiptToReceiptDTO(receipt));
	}

	/**
	 * Get one receipt by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ReceiptDTO findOne(Long id) {
		log.debug("Request to get Receipt : {}", id);
		Receipt receipt = receiptRepository.findById(id).orElse(null);
		ReceiptDTO receiptDTO = receiptMapper.receiptToReceiptDTO(receipt);
		return receiptDTO;
	}

	/**
	 * Delete the receipt by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Receipt : {}", id);

		Receipt receipt = receiptRepository.findById(id).orElse(null);
		if (receipt != null && receipt.getFile() != null) {
			try {
			  fileStore.deleteReceipt(UUID.fromString(receipt.getFile()));
			} catch (FileStoreException exception) {
				// TODO: handling?
				exception.printStackTrace();
			}

			Iterator<CashbookEntry> it = receipt.getEntries().iterator();
			while (it.hasNext()) {
				CashbookEntry cashbookEntry = it.next();
				cashbookEntry.setReceipt(null);
				cashbookEntryRepository.save(cashbookEntry);
			}
		}

		receiptRepository.deleteById(id);
	}

	/**
	 * links a cashbook entry to a receipt.
	 *
	 * @param receiptDTO
	 *            the receipt data transfer object
	 */
	public void linkToCashbookEntry(ReceiptDTO receiptDTO) {
		if (receiptDTO.getId() == null || receiptDTO.getCashbookEntryId() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptLinkingBadRequest, null);
		}

		Receipt receipt = receiptRepository.findById(receiptDTO.getId()).orElse(null);
		if (receipt == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptLinkingReceiptNotFound, null);
		}

		CashbookEntry cashbookEntry = cashbookEntryRepository.findById(receiptDTO.getCashbookEntryId()).orElse(null);
		if (cashbookEntry == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptLinkingCashbookEntryNotFound, null);
		}

		if (cashbookEntry.getReceipt() != null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptLinkingCashbookEntryAlreadyLinked, null);
		}

		cashbookEntry.setReceipt(receipt);
		cashbookEntryRepository.save(cashbookEntry);
	}

	/**
	 * unlinks a cashbook entry from a receipt.
	 *
	 * @param receiptDTO
	 *            the receipt data transfer object
	 */
	public void unlinkCashbookEntry(ReceiptDTO receiptDTO) {
		if (receiptDTO.getId() == null || receiptDTO.getCashbookEntryId() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptUnlinkingBadRequest, null);
		}

		Receipt receipt = receiptRepository.findById(receiptDTO.getId()).orElse(null);
		if (receipt == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptUnlinkingReceiptNotFound, null);
		}

		CashbookEntry cashbookEntry = cashbookEntryRepository.findById(receiptDTO.getCashbookEntryId()).orElse(null);
		if (cashbookEntry == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptUnlinkingCashbookEntryNotFound, null);
		}

		if (cashbookEntry.getReceipt() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.receiptUnlinkingCashbookEntryNotLinked, null);
		}

		cashbookEntry.setReceipt(null);
		cashbookEntryRepository.save(cashbookEntry);
	}
}
