package at.simianarmy.service;

import at.simianarmy.domain.Filter;
import at.simianarmy.repository.FilterRepository;
import at.simianarmy.service.dto.FilterDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.FilterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Filter.
 */
@Service
@Transactional
public class FilterService {

    private final Logger log = LoggerFactory.getLogger(FilterService.class);

    private final FilterRepository filterRepository;

    private final FilterMapper filterMapper;

    public FilterService(FilterRepository filterRepository, FilterMapper filterMapper) {
        this.filterRepository = filterRepository;
        this.filterMapper = filterMapper;
    }

    /**
     * Save a filter.
     *
     * @param filterDTO the entity to save
     * @return the persisted entity
     */
    public FilterDTO save(FilterDTO filterDTO) {
        log.debug("Request to save Filter : {}", filterDTO);

        Filter filter = filterMapper.toEntity(filterDTO);

        if (this.filterRepository.existsByListNameAndName(filter.getListName(), filter.getName())) {
          throw new ServiceValidationException(ServiceErrorConstants.filterWithNameAlreadyExistent, null);
        }

        filter = filterRepository.save(filter);
        return filterMapper.toDto(filter);
    }

    /**
     * Get all the filters.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<FilterDTO> findAllByListName(String listName) {
        log.debug("Request to get all Filters by ListName {}", listName);
        return filterRepository.findByListName(listName).stream()
            .map(filterMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one filter by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public FilterDTO findOne(Long id) {
        log.debug("Request to get Filter : {}", id);
        Filter filter = this.filterRepository.findById(id).orElse(null);

        return this.filterMapper.toDto(filter);
    }

    /**
     * Delete the filter by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Filter : {}", id);
        filterRepository.deleteById(id);
    }
}
