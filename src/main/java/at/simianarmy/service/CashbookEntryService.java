package at.simianarmy.service;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.reports.CashbookAppointmentReport;
import at.simianarmy.reports.CashbookOverallReport;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookEntryMapper;
import at.simianarmy.service.specification.CashbookEntrySpecification;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing CashbookEntry.
 */
@Service
@Transactional
public class CashbookEntryService {

	private final Logger log = LoggerFactory.getLogger(CashbookEntryService.class);

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private BankImportDataRepository bankImportDataRepository;

	@Inject
	private CashbookEntryMapper cashbookEntryMapper;

	@Inject
	private CashbookCategoryRepository cashbookCategoryRepository;

	@Inject
  private CashbookOverallReport cashbookOverallReport;

	@Inject
  private CashbookAppointmentReport cashbookAppointmentReport;

	/**
	 * Save a cashbookEntry.
	 *
	 * @param cashbookEntryDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CashbookEntryDTO save(CashbookEntryDTO cashbookEntryDTO) {
		log.debug("Request to save CashbookEntry : {}", cashbookEntryDTO);
		CashbookEntry cashbookEntry = cashbookEntryMapper.cashbookEntryDTOToCashbookEntry(cashbookEntryDTO);

		this.checkEditOfProtectedFields(cashbookEntry, cashbookEntryDTO);

		if (cashbookEntry.getId() != null && (cashbookEntry.getInstrumentService() != null
				|| cashbookEntry.getInstrument() != null || cashbookEntry.getMembershipFeeForPeriod() != null)) {
			CashbookEntry oldEntry = cashbookEntryRepository.findById(cashbookEntry.getId()).orElse(null);
			if (oldEntry != null) {
				oldEntry.setCashbookCategory(cashbookEntry.getCashbookCategory());
				cashbookEntry = oldEntry;
			}
		}

		cashbookEntry = cashbookEntryRepository.save(cashbookEntry);
		CashbookEntryDTO result = cashbookEntryMapper.cashbookEntryToCashbookEntryDTO(cashbookEntry);
		return result;
	}

	/**
	 * checks if a protected field was edited.
	 * 
	 * @param cashbookEntry
	 *            the cashbook entry
	 * @param cashbookEntryDTO
	 *            the cashbook entry data transfer object
	 */
	private void checkEditOfProtectedFields(CashbookEntry cashbookEntry, CashbookEntryDTO cashbookEntryDTO) {
		if (cashbookEntry.getId() != null) {
			if (cashbookEntryDTO.getCompositions().isEmpty() == false
					|| cashbookEntryDTO.getMembershipFeeForPeriodId() != null
					|| cashbookEntryDTO.getInstrumentServiceId() != null
					|| cashbookEntryDTO.getInstrumentId() != null) {
				CashbookEntry oldCashbookEntry = cashbookEntryRepository.findById(cashbookEntry.getId()).orElse(null);
				if (oldCashbookEntry != null) {
					if (cashbookEntry.getTitle().compareTo(oldCashbookEntry.getTitle()) != 0
							|| cashbookEntry.getType().compareTo(oldCashbookEntry.getType()) != 0
							|| cashbookEntry.getAmount().compareTo(oldCashbookEntry.getAmount()) != 0
							|| cashbookEntry.getDate().compareTo(oldCashbookEntry.getDate()) != 0) {
						throw new ServiceValidationException(ServiceErrorConstants.cashbookEntryEditOfProtectedFields,
								null);
					}
				}
			}
		}
	}

	/**
	 * Get all the cashbookEntries.
	 *
	 * @param title
	 *            the title
	 * @param categoryId
	 *            the id of the category
	 * @param type
	 *            the cashbook entry type
	 * @param dateFrom
	 *            the lower bound of the date range
	 * @param dateTo
	 *            the upper bound of the date range
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CashbookEntryDTO> findAll(String title, Long categoryId, CashbookEntryType type, LocalDate dateFrom,
			LocalDate dateTo, Long accountId, Pageable pageable) {
		log.debug("Request to get all CashbookEntries");

		CashbookEntrySpecification specification = new CashbookEntrySpecification();

		List<Long> categoryIds = null;
		if (categoryId != null) {
			categoryIds = new ArrayList<>();
			CashbookCategory cat = cashbookCategoryRepository.findById(categoryId).orElse(null);

			if (cat != null) {
				addCategoryIdsForCategory(cat, categoryIds);
			} else {
				categoryIds.add(categoryId);
			}

		}

		Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification.hasTitle(title)
				.belongsToCategory(categoryIds).hasType(type).hasDate(dateFrom, dateTo).hasAccount(accountId).build(),
				pageable);

		return result.map(cashbookEntry -> cashbookEntryMapper.cashbookEntryToCashbookEntryDTO(cashbookEntry));
	}

	/**
	 * get all the cashbookEntries where InstrumentService is null.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<CashbookEntryDTO> findAllWhereInstrumentServiceIsNull() {
		log.debug("Request to get all cashbookEntries where InstrumentService is null");
		return StreamSupport.stream(cashbookEntryRepository.findAll().spliterator(), false)
				.filter(cashbookEntry -> cashbookEntry.getInstrumentService() == null)
				.map(cashbookEntryMapper::cashbookEntryToCashbookEntryDTO)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * get all the cashbookEntries where MembershipFeeForPeriod is null.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<CashbookEntryDTO> findAllWhereMembershipFeeForPeriodIsNull() {
		log.debug("Request to get all cashbookEntries where MembershipFeeForPeriod is null");
		return StreamSupport.stream(cashbookEntryRepository.findAll().spliterator(), false)
				.filter(cashbookEntry -> cashbookEntry.getMembershipFeeForPeriod() == null)
				.map(cashbookEntryMapper::cashbookEntryToCashbookEntryDTO)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get one cashbookEntry by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CashbookEntryDTO findOne(Long id) {
		log.debug("Request to get CashbookEntry : {}", id);
		CashbookEntry cashbookEntry = cashbookEntryRepository.findById(id).orElse(null);
		CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper.cashbookEntryToCashbookEntryDTO(cashbookEntry);
		return cashbookEntryDTO;
	}

	/**
	 * Delete the cashbookEntry by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete CashbookEntry : {}", id);

		this.checkCashbookEntryRelations(id);

		// Import-Daten von Cashbook-Entry müssen gelöscht werden
		BankImportData bankImportData = this.bankImportDataRepository.findByCashbookEntry(id);

		if (bankImportData != null) {
			this.bankImportDataRepository.delete(bankImportData);
		}

		cashbookEntryRepository.deleteById(id);
	}

	/**
	 * checks if the entry has relations which makes it undeletable.
	 * 
	 * @param id
	 *            the id of the entity
	 */
	private void checkCashbookEntryRelations(Long id) {
		CashbookEntry entry = cashbookEntryRepository.findById(id).orElse(null);
		if (entry != null) {
			if (entry.getMembershipFeeForPeriod() != null) {
				throw new ServiceValidationException(ServiceErrorConstants.cashbookEntryBelongsToMembershipFee, null);
			}
			if (entry.getInstrumentService() != null) {
				throw new ServiceValidationException(ServiceErrorConstants.cashbookEntryBelongsToInstrumentService,
						null);
			}
			if (entry.getInstrument() != null) {
				throw new ServiceValidationException(ServiceErrorConstants.cashbookEntryBelongsToInstrument, null);
			}
			if (entry.getCompostions().isEmpty() == false) {
				throw new ServiceValidationException(ServiceErrorConstants.cashbookEntryBelongsToComposition, null);
			}
		}
	}

	/**
	 * Get all Sub-CashbookCategories for a Category recursively without duplicates.
	 * 
	 * @param category
	 *            The Category to start with
	 * @param categoryIds
	 *            The result list
	 */
	public void addCategoryIdsForCategory(CashbookCategory category, List<Long> categoryIds) {

		if (!categoryIds.contains(category.getId())) {
			categoryIds.add(category.getId());
		}

		for (CashbookCategory subGroup : category.getChildren()) {
			this.addCategoryIdsForCategory(subGroup, categoryIds);
		}

	}

  /**
   * Generates the Overall Report
   *
   * @param year
   * @param cashbookAccountIds
   * @param cashbookCategoryIds
   * @return
   */
  public Resource getCashbookOverallReport(Integer year, List<Long> cashbookAccountIds, List<Long> cashbookCategoryIds) {
    return this.cashbookOverallReport.create(year, cashbookAccountIds, cashbookCategoryIds);
  }


  /**
   * Generates the Appointment Report
   *
   * @param dateFrom
   * @param dateTo
   * @param appointmentTypeId
   * @return
   */
  public Resource getCashbookAppointmentReport(LocalDate dateFrom, LocalDate dateTo, List<Long> appointmentTypeId) {

    if (dateFrom.isEqual(dateTo) || dateFrom.isAfter(dateTo)) {
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAppointmentReportInvalidDates, null);
    }

    return this.cashbookAppointmentReport.create(dateFrom, dateTo, appointmentTypeId);
  }

}
