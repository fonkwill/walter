package at.simianarmy.service;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipFeeRepository;
import at.simianarmy.service.dto.MembershipFeeDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MembershipFeeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Service Implementation for managing MembershipFee.
 */
@Service
@Transactional
public class MembershipFeeService {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeService.class);

	@Inject
	private MembershipFeeRepository membershipFeeRepository;

	@Inject
	private MembershipFeeAmountRepository membershipFeeAmountRepository;

	@Inject
	private MembershipFeeMapper membershipFeeMapper;

	/**
	 * Save a membershipFee.
	 *
	 * @param membershipFeeDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MembershipFeeDTO save(MembershipFeeDTO membershipFeeDTO) {
		log.debug("Request to save MembershipFee : {}", membershipFeeDTO);
		MembershipFee membershipFee = membershipFeeMapper.membershipFeeDTOToMembershipFee(membershipFeeDTO);
		membershipFee = membershipFeeRepository.save(membershipFee);
		MembershipFeeDTO result = membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee);
		return result;
	}

	/**
	 * Get all the membershipFees.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MembershipFeeDTO> findAll(Pageable pageable) {
		log.debug("Request to get all MembershipFees");
		Page<MembershipFee> result = membershipFeeRepository.findAll(pageable);
		return result.map(membershipFee -> membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee));
	}

	/**
	 * Get one membershipFee by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MembershipFeeDTO findOne(Long id) {
		log.debug("Request to get MembershipFee : {}", id);
		MembershipFee membershipFee = membershipFeeRepository.findById(id).orElse(null);
		MembershipFeeDTO membershipFeeDTO = membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee);
		return membershipFeeDTO;
	}

	/**
	 * Delete the membershipFee by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete MembershipFee : {}", id);
		MembershipFee fee = membershipFeeRepository.findById(id).orElse(null);
		if (fee.getMemberships() == null || fee.getMemberships().isEmpty()) {
			if (fee.getMembershipFeeAmounts() != null && !fee.getMembershipFeeAmounts().isEmpty()) {
				for (MembershipFeeAmount a : fee.getMembershipFeeAmounts()) {
					membershipFeeAmountRepository.deleteById(a.getId());
				}
			}
			membershipFeeRepository.deleteById(id);
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.membershipFeeNotDeletable, null);
		}
	}
}
