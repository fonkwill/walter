package at.simianarmy.service;

import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.service.dto.CashbookCategoryDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookCategoryMapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing CashbookCategory.
 */
@Service
@Transactional
public class CashbookCategoryService {

	private final Logger log = LoggerFactory.getLogger(CashbookCategoryService.class);

	@Inject
	private CashbookCategoryRepository cashbookCategoryRepository;

	@Inject
	private CashbookCategoryMapper cashbookCategoryMapper;

	/**
	 * Save a cashbookCategory.
	 *
	 * @param cashbookCategoryDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CashbookCategoryDTO save(CashbookCategoryDTO cashbookCategoryDTO) {
		log.debug("Request to save CashbookCategory : {}", cashbookCategoryDTO);
		CashbookCategory cashbookCategory = cashbookCategoryMapper
				.cashbookCategoryDTOToCashbookCategory(cashbookCategoryDTO);

		if (cashbookCategory.getParent() != null) {
			// Check for circular references!
			this.checkCircularReference(cashbookCategory, cashbookCategory.getParent());
		}

		cashbookCategory = cashbookCategoryRepository.save(cashbookCategory);
		CashbookCategoryDTO result = cashbookCategoryMapper.cashbookCategoryToCashbookCategoryDTO(cashbookCategory);
		return result;
	}

	@Transactional
	private void checkCircularReference(CashbookCategory category, CashbookCategory parent) {
		// If the category has no id (is new), there is no possibilty of
		// circular references
		if (category.getId() == null) {
			return;
		}

		// to get the children, we have to query the database
		category = cashbookCategoryRepository.findById(category.getId()).orElse(null);

		// searchedParent must not a child of personGroup
		for (CashbookCategory cc : category.getChildren()) {
			if (cc.equals(parent)) {

				throw new ServiceValidationException(ServiceErrorConstants.cashbookCategoryCircularReference, null);
			}
			checkCircularReference(cc, parent);
		}

	}

	/**
	 * Get all the cashbookCategories.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CashbookCategoryDTO> findAll(Pageable pageable) {
		log.debug("Request to get all CashbookCategories");
		Page<CashbookCategory> result = cashbookCategoryRepository.findAll(pageable);
		return result.map(
				cashbookCategory -> cashbookCategoryMapper.cashbookCategoryToCashbookCategoryDTO(cashbookCategory));
	}

	/**
	 * Get one cashbookCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CashbookCategoryDTO findOne(Long id) {
		log.debug("Request to get CashbookCategory : {}", id);
		CashbookCategory cashbookCategory = cashbookCategoryRepository.findById(id).orElse(null);
		CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryMapper
				.cashbookCategoryToCashbookCategoryDTO(cashbookCategory);
		return cashbookCategoryDTO;
	}

	/**
	 * Delete the cashbookCategory by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Transactional
	public void delete(Long id) {
		log.debug("Request to delete CashbookCategory : {}", id);
		CashbookCategory toDelete = this.cashbookCategoryRepository.findById(id).orElse(null);

		if (toDelete != null && toDelete.getCashbookEntries().size() != 0) {
			throw new ServiceValidationException(ServiceErrorConstants.cashbookCategoryDataIntegrityViolation, null);
		}

		if (toDelete != null && toDelete.getChildren() != null) {
			for (CashbookCategory cc : toDelete.getChildren()) {
				cc.setParent(toDelete.getParent());
				this.cashbookCategoryRepository.save(cc);
			}
		}

		cashbookCategoryRepository.deleteById(id);
	}

	public Set<CashbookCategory> getAllCashbookCategoriesRecursive(
    List<CashbookCategory> cashbookCategories) {
    Set<CashbookCategory> result = new HashSet<>();

    for (CashbookCategory category : cashbookCategories) {
      result.add(category);
      Set<CashbookCategory> children = getAllCashbookCategoriesRecursive(new ArrayList<>(category.getChildren()));
      result.addAll(children);
    }
    return result;
  }
}
