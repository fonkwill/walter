package at.simianarmy.service;

import at.simianarmy.domain.Company;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.service.dto.CompanyDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CompanyMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Company.
 */
@Service
@Transactional
public class CompanyService {

	private final Logger log = LoggerFactory.getLogger(CompanyService.class);

	@Inject
	private CompanyRepository companyRepository;

	@Inject
	private CompanyMapper companyMapper;

	/**
	 * Save a company.
	 *
	 * @param companyDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CompanyDTO save(CompanyDTO companyDTO) {
		log.debug("Request to save Company : {}", companyDTO);
		Company company = companyMapper.companyDTOToCompany(companyDTO);
		company = companyRepository.save(company);
		CompanyDTO result = companyMapper.companyToCompanyDTO(company);
		return result;
	}

	/**
	 * Get all the companies.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CompanyDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Companies");
		Page<Company> result = companyRepository.findAll(pageable);
		return result.map(company -> companyMapper.companyToCompanyDTO(company));
	}

	/**
	 * Get one company by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CompanyDTO findOne(Long id) {
		log.debug("Request to get Company : {}", id);
		Company company = companyRepository.findById(id).orElse(null);
		CompanyDTO companyDTO = companyMapper.companyToCompanyDTO(company);
		return companyDTO;
	}

	/**
	 * Delete the company by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Company : {}", id);

		Company existing = this.companyRepository.findById(id).orElse(null);

		if (existing != null) {
			if (existing.getOrderedCompositions().size() != 0 || existing.getPublishedCompositions().size() != 0
					|| existing.getReceipts().size() != 0) {
				throw new ServiceValidationException(ServiceErrorConstants.companyDataIntegrityViolation, null);
			}
		}
		companyRepository.deleteById(id);
	}
}
