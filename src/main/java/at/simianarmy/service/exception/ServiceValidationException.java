package at.simianarmy.service.exception;

import at.simianarmy.web.rest.errors.CustomParameterizedException;

public class ServiceValidationException extends CustomParameterizedException {

	/**
	 * CustomParameterizedException for validation errors. The provided message will
	 * be visible on the GUI.
	 *
	 * @param message:
	 *            JSON path of the error message. Preferably, use a constant defined
	 *            in ServiceErrorConstants. Don't forget to specify a user friendly
	 *            error message in the specified JSON.
	 * @param params:
	 *            Optional additional parameters used by the error message
	 */
	private static final long serialVersionUID = 1L;

	public ServiceValidationException(String message, String[] params) {
		super(message, params);
	}

}
