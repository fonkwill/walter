package at.simianarmy.service.exception;

public class TenantIdentifierException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public TenantIdentifierException(String string) {
		super(string);
	}

	public TenantIdentifierException(String message, Exception exception) {
		super(message, exception);
	}



}
