package at.simianarmy.service;

import at.simianarmy.domain.AppointmentType;
import at.simianarmy.repository.AppointmentTypeRepository;
import at.simianarmy.service.dto.AppointmentTypeDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.AppointmentTypeMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing AppointmentType.
 */
@Service
@Transactional
public class AppointmentTypeService {

	private final Logger log = LoggerFactory.getLogger(AppointmentTypeService.class);

	@Inject
	private AppointmentTypeRepository appointmentTypeRepository;

	@Inject
	private AppointmentTypeMapper appointmentTypeMapper;

	/**
	 * Save a appointmentType.
	 *
	 * @param appointmentTypeDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public AppointmentTypeDTO save(AppointmentTypeDTO appointmentTypeDTO) {
		log.debug("Request to save AppointmentType : {}", appointmentTypeDTO);
		AppointmentType appointmentType = appointmentTypeMapper.appointmentTypeDTOToAppointmentType(appointmentTypeDTO);
		appointmentType = appointmentTypeRepository.save(appointmentType);
		AppointmentTypeDTO result = appointmentTypeMapper.appointmentTypeToAppointmentTypeDTO(appointmentType);
		return result;
	}

	/**
	 * Get all the appointmentTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<AppointmentTypeDTO> findAll(Pageable pageable) {
		log.debug("Request to get all AppointmentTypes");
		Page<AppointmentType> result = appointmentTypeRepository.findAll(pageable);
		return result
				.map(appointmentType -> appointmentTypeMapper.appointmentTypeToAppointmentTypeDTO(appointmentType));
	}

	/**
	 * Get one appointmentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AppointmentTypeDTO findOne(Long id) {
		log.debug("Request to get AppointmentType : {}", id);
		AppointmentType appointmentType = appointmentTypeRepository.findById(id).orElse(null);
		AppointmentTypeDTO appointmentTypeDTO = appointmentTypeMapper
				.appointmentTypeToAppointmentTypeDTO(appointmentType);
		return appointmentTypeDTO;
	}

	/**
	 * Delete the appointmentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete AppointmentType : {}", id);
		AppointmentType appointmentType = appointmentTypeRepository.findById(id).orElse(null);
		if (appointmentType != null && appointmentType.getAppointments().size() > 0) {
			String[] params = new String[] { appointmentType.getName() };
			throw new ServiceValidationException(ServiceErrorConstants.appointmentTypeHasAppointments, params);
		}
		appointmentTypeRepository.deleteById(id);
	}
}
