package at.simianarmy.service;

public class ServiceErrorConstants {

	public static final String appointmentEndBeforeBegin = "walterApp.appointment.error.endBeforeBegin";
	public static final String appointmentInvalidAssociationData = "walterApp.appointment.error.invalidAssociationData";
	public static final String appointmentIncompleteAppointment = "walterApp.appointment.error.incompleteAppointment";
	public static final String appointmentOtherError = "walterApp.appointment.error.otherError";

	public static final String appointmentTypeHasAppointments = "walterApp.appointmentType.error.hasAppointments";

	public static final String appointmentOfficialAppointmentNotNull = "walterApp.appointmentType.error.officialAppointmentNotNull";

	public static final String cashbookEntryBelongsToInstrumentService = "walterApp.cashbookEntry.error.belongsToInstrumentService";
	public static final String cashbookEntryBelongsToInstrument = "walterApp.cashbookEntry.error.belongsToInstrument";
	public static final String cashbookEntryBelongsToMembershipFee = "walterApp.cashbookEntry.error.belongsToMembershipFee";
	public static final String cashbookEntryBelongsToComposition = "walterApp.cashbookEntry.error.belongsToCompositions";
	public static final String cashbookEntryEditOfProtectedFields = "walterApp.cashbookEntry.error.editOfProtectedFields";

	public static final String personGroupCircularReference = "walterApp.personGroup.error.circularReference";
	public static final String personGroupNotFound = "walterApp.personGroup.error.notFound";

	public static final String cashbookCategoryCircularReference = "walterApp.cashbookCategory.error.circularReference";
	public static final String cashbookCategoryDataIntegrityViolation = "walterApp.cashbookCategory.error.dataIntegrity";

	public static final String cashbookAccountReceiptCodeUnique = "walterApp.cashbookAccount.error.receiptCodeUnique";
  public static final String cashbookAccountReceiptCodeOnlyLettersAllowed = "walterApp.cashbookAccount.error.receiptCodeOnlyLettersAllowed";
  public static final String cashbookAccountReceiptCodeUpdateNotAllowed = "walterApp.cashbookAccount.receiptCodeUpdateNotAllowed";
  public static final String cashbookAccountBankAccountWithoutImporterType = "walterApp.cashbookAccount.error.bankAccountWithoutImporterType";
  public static final String cashbookAccountAutoTransferTextFromAndToEqual = "walterApp.cashbookAccount.error.autoTransferTextFromAndToEqual";
  public static final String cashbookAccountAutoTransferTextUnique = "walterApp.cashbookAccount.error.autoTransferTextUnique";
  public static final String cashbookAccountWithDefaultForCashbookUnique = "walterApp.cashbookAccount.error.defaultForCashbookUnique";

	public static final String instrumentNoType = "walterApp.instrument.error.noType";

	public static final String instrumentDateInFuture = "walterApp.instrument.error.dateInFuture";

	public static final String instrumentNotDeletable = "walterApp.instrument.error.notDeletable";

	public static final String instrumentTypeNotDeletable = "walterApp.instrumentType.error.notDeletable";

	public static final String instrumentServiceNoInstrument = "walterApp.instrumentService.error.noInstrument";

	public static final String instrumentServiceDateInFuture = "walterApp.instrumentService.error.dateInFuture";

	public static final String instrumentServiceDateBeforeInstrumentDate = "walterApp.instrumentService.error.dateBeforeInstrumentDate";

	public static final String instrumentServiceNotDeletable = "walterApp.instrumentService.error.notDeletable";

	public static final String compositionServiceNrNotWriteable = "walterApp.composition.error.nrNotWriteable";

	public static final String honorNotDeletable = "walterApp.honor.error.notDeletable";
	public static final String awardNotDeletable = "walterApp.award.error.notDeletable";
	public static final String clothingTypeNotDeletable = "walterApp.clothingType.error.notDeletable";

	public static final String compositionServiceExistingCashbookEntryAmountNotNull = "walterApp.composition.error.cashbookEntryAmountNotNull";

	public static final String companyDataIntegrityViolation = "walterApp.company.error.dataIntegrity";

	public static final String colorCodeDataIntegrityViolation = "walterApp.colorCode.error.dataIntegrity";

	public static final String composerDataIntegrityViolation = "walterApp.composer.error.dataIntegrity";

	public static final String arrangerDataIntegrityViolation = "walterApp.arranger.error.dataIntegrity";

	public static final String genreDataIntegrityViolation = "walterApp.genre.error.dataIntegrity";

	public static final String musicBookDataIntegrityViolation = "walterApp.musicBook.error.dataIntegrity";

	public static final String receiptDateInFuture = "walterApp.receipt.error.dateInFuture";
	public static final String receiptLinkingBadRequest = "walterApp.receipt.error.linkingBadRequest";
	public static final String receiptLinkingReceiptNotFound = "walterApp.receipt.error.linkingReceiptNotFound";
	public static final String receiptLinkingCashbookEntryNotFound = "walterApp.receipt.error.linkingCashbookEntryNotFound";
	public static final String receiptLinkingCashbookEntryAlreadyLinked = "walterApp.receipt.error.linkingCashbookEntryAlreadyLinked";
	public static final String receiptUnlinkingBadRequest = "walterApp.receipt.error.unlinkingBadRequest";
	public static final String receiptUnlinkingReceiptNotFound = "walterApp.receipt.error.unlinkingReceiptNotFound";
	public static final String receiptUnlinkingCashbookEntryNotFound = "walterApp.receipt.error.unlinkingCashbookEntryNotFound";
	public static final String receiptUnlinkingCashbookEntryNotLinked = "walterApp.receipt.error.unlinkingCashbookEntryNotLinked";
  public static final String receiptIdentifierOverwrite = "walterApp.receipt.error.identifierOverwrite";
  public static final String receiptIdentifierAlreadyUsed = "walterApp.receipt.error.identifierAlreadyUsed";
  public static final String autoReceiptIdentifierNotAllowed = "walterApp.receipt.error.autoReceiptIdentifierNotAllowed";

	public static final String personSearchEndBeforeBegin = "walterApp.person.error.searchEndbeforebegin";

	public static final String personInstrumentEndBeforeBegin = "walterApp.personInstrument.error.personInstrumentEndBeforeBegin";

	public static final String personClothingEndBeforeBegin = "walterApp.personClothing.error.personClothingEndBeforeBegin";

	public static final String clothingForThisClothingGroupNotAvailable = "walterApp.clothing.error.clothingForThisClothingGroupNotAvailable";
	public static final String clothingGroupMaxDeletable = "walterApp.clothingGroup.error.maxDeletable";

	public static final String membershipEndBeforeBegin = "walterApp.membership.error.membershipEndBeforeBegin";

	public static final String bankImportUnknownImporterType = "walterApp.bankImportData.error.unknownImporterType";
  public static final String unimplementedImporterType = "walterApp.bankImportData.error.unimplementedImporterType";

	public static final String bankImportWrongFileFormat = "walterApp.bankImportData.csv.error.wrongFileFormat";
	public static final String bankImportWrongCSVFormat = "walterApp.bankImportData.csv.error.wrongCSVFormat";
	public static final String bankImportDateFromBeforeTo = "walterApp.bankImportData.api.error.dateFromBeforeTo";
	public static final String bankImportAuthenticationFailure = "walterApp.bankImportData.api.error.authenticationFailure";
	public static final String bankImportOtherErrors = "walterApp.bankImportData.error.other";
	public static final String bankImportUnknownCashbookAccount = "walterApp.bankImportData.error.unknownCashbookAccount";

	public static final String templateMarginBorders = "walterApp.template.error.templateMarginBorders";
	public static final String templateNotDeletable = "walterApp.template.error.notDeletable";
	public static final String templateDeletionError = "walterApp.template.error.templateDeletionError";
	public static final String templateNoPdf = "walterApp.template.error.noPdf";
	public static final String templateNumberOfPages = "walterApp.template.error.numberOfPages";
	public static final String templateNotA4 = "walterApp.template.error.notA4";
	public static final String templateNotSaved = "walterApp.template.error.notSaved";
	public static final String templateNotPortraitOrientation = "walterApp.template.error.notPortraitOrientation";

	public static final String serialLetterNotParseable = "walterApp.letter.error.notParseable";
	public static final String serialLetterNotGeneratable = "walterApp.letter.error.notGeneratable";
	public static final String serialLetterNoPersonsSelected = "walterApp.letter.error.noPersonsSelected";
	public static final String serialLetterTemplateNotReadable = "walterApp.letter.error.templateNotReadable";
  public static final String serialLetterPersonGroupsAndPersonsNotAllowed = "walterApp.letter.error.PersonGroupsAndPeronsNotAllowed";

	public static final String notificationRuleUnknownClass = "walterApp.notificationRule.error.unknownClass";
	public static final String notificationRuleWrongConditionSyntax = "walterApp.notificationRule.error.wrongConditionSyntax";

	public static final String notificationNotFound = "walterApp.notification.error.notFound";

	public static final String membershipfeeAmountBeginDateAfterEndDate = "walterApp.membershipFeeAmount.error.beginDateAfterEndDate";
	public static final String membershipfeeAmountDatesOverlap = "walterApp.membershipFeeAmount.error.datesOverlap";

	public static final String membershipFeeNotDeletable = "walterApp.membershipFee.error.notDeletable";
	public static final String letterNotSet = "walterApp.membershipFee.error.noLetterSet";
	public static String serialLetterNoPeriodSelected = "walterApp.membershipFee.error.noPeriodSelected";

	public static final String membershipNotDeletable = "walterApp.membership.error.notDeletable";
	public static final String membershipDatesOverlap = "walterApp.membership.error.datesOverlap";
	public static final String membershipEndDate = "walterApp.membership.error.endDate";
	public static final String membershipBeginDate = "walterApp.membership.error.beginDate";
	public static final String membershipNoPersonGiven = "walterApp.membership.error.noPersonGiven";

	public static final String customizationNameAlreadyUsed = "walterApp.customization.error.nameAlreadyUsed";
	public static final String noMembershipForMembershipFeeForPeriod = "walterApp.membership.error.noMembershipForMembershipFeeForPeriod";
	
	public static final String roleChangeNotAllowed = "walterApp.role.error.changeOfRoleNotAllowed";

	public static final String personNotFound = "walterApp.emailVerificationLink.error.personNotFound";
	public static final String personAlreadyHasActiveEmailVerificationLink = "walterApp.emailVerificationLink.error.personAlreadyHasActiveEmailVerificationLink";
	public static final String personHasNoActiveEmailVerificationLink = "walterApp.emailVerificationLink.error.personHasNoActiveEmailVerificationLink";
	public static final String emailVerificationSecretKeyNotValid = "walterApp.emailVerificationLink.error.secretKeyNotValid";

  public static final String filterWithNameAlreadyExistent = "walterApp.filter.error.filterWithNameAlreadyExistent";

  public static final String userColumnConfigUpdateNotAllowed = "walterApp.userColumnConfig.updateNotAllowed";
  public static final String userColumnConfigUpdateForSeveralLists = "walterApp.userColumnConfig.updateForSeveralLists";
  public static final String userColumnConfigUpdateMinimumOneSelected = "walterApp.userColumnConfig.minimumOneSelected";
  public static final String wrongCustomizationData = "walterApp.customization.wrongData";

  public static final String mailLogSendError = "walterApp.mailLog.sendError";

  public static final String csvImportFileCouldNotBeRead =  "walterApp.dataImport.error.fileCouldNotBeRead";
  public static final String csvImportColumnNameNotMapped = "walterApp.dataImport.error.columnNameCouldNotBeMapped";
  public static final String dateFormatNotParseable = "walterApp.dataImport.error.dateFormatNotParseable";
  public static final String constraintViolationException = "walterApp.global.error.validationFailed";

  public static final String defaultCashbookAccountUndefined = "walterApp.customization.error.defaultCashbookAccountUndefined";

  public static final String cashbookAccountTransferCashbookAccountNotFound = "walterApp.cashbookAccountTransfer.error.cashbookAccountNotFound";
  public static final String cashbookAccountTransferAccountFromAndToEqual = "walterApp.cashbookAccountTransfer.error.accountFromAndToEqual";

  public static final String cashbookAppointmentReportInvalidDates = "walterApp.cashbookAppointmentReport.error.invalidDates";

  public static final String documentsDirectoryNotCreated = "walterApp.documents.error.directoryNotCreated";
  public static final String documentsDirectoryNotDeleted = "walterApp.documents.error.directoryNotDeleted";
  public static final String documentsFileNotCreated = "walterApp.documents.error.fileNotCreated";
  public static final String documentsFileNotDeleted = "walterApp.documents.error.fileNotDeleted";
  public static final String documentsFileUnallowedManipulation = "walterApp.documents.error.unallowedFileManipulation";
  public static final String documentsOverallError = "walterApp.documents.error.overall";

}
