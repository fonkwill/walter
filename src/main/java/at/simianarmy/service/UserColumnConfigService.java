package at.simianarmy.service;

import at.simianarmy.domain.ColumnConfig;
import at.simianarmy.domain.User;
import at.simianarmy.domain.UserColumnConfig;
import at.simianarmy.repository.ColumnConfigRepository;
import at.simianarmy.repository.UserColumnConfigRepository;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.security.SecurityUtils;
import at.simianarmy.service.dto.UserColumnConfigDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.DefaultUserColumnConfigMapper;
import at.simianarmy.service.mapper.UserColumnConfigMapper;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing UserColumnConfig.
 */
@Service
@Transactional
public class UserColumnConfigService {

  private static final Logger log = LoggerFactory.getLogger(UserColumnConfigService.class);

  @Inject
  private ColumnConfigRepository columnConfigRepository;

  @Inject
  private UserColumnConfigRepository userColumnConfigRepository;

  @Inject
  private UserRepository userRepository;

  @Inject
  private UserColumnConfigMapper userColumnConfigMapper;

  @Inject
  private DefaultUserColumnConfigMapper defaultUserColumnConfigMapper;

  /**
   * Returns all UserColumnConfigs for an Entity and the current User
   * @param entity
   * @return
   */
  public List<UserColumnConfigDTO> getColumnConfigsForEntityAndCurrentUser(String entity, Boolean showDefault) {
    log.debug("Request to get all UserColumnConfigs by Entity {}", entity);

    if (showDefault) {
      return this.userColumnConfigRepository.findByEntityAndUserIsCurrentUser(entity).stream()
        .map(this.defaultUserColumnConfigMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
    } else {
      return this.userColumnConfigRepository.findByEntityAndUserIsCurrentUser(entity).stream()
        .map(this.userColumnConfigMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
    }
  }

  /**
   * Returns all visible UserColumnConfigs for an Entity and the current User
   * @param entity
   * @return
   */
  public List<UserColumnConfigDTO> getVisibleColumnConfigsForEntityAndCurrentUser(String entity, Boolean showDefault) {
    log.debug("Request to get all visible UserColumnConfigs by Entity {}", entity);

    if (showDefault) {
      return this.userColumnConfigRepository.findByEntityAndVisibleAndUserIsCurrentUser(entity)
        .stream()
        .map(this.defaultUserColumnConfigMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
    } else {
      return this.userColumnConfigRepository.findByEntityAndVisibleAndUserIsCurrentUser(entity)
        .stream()
        .map(this.userColumnConfigMapper::toDto)
        .collect(Collectors.toCollection(LinkedList::new));
    }
  }

  /**
   * Updates the UserColumnConfig
   * @param userColumnConfigDTOs
   * @return the updated UserColumnConfigDTOs
   */
  public List<UserColumnConfigDTO> update(List<UserColumnConfigDTO> userColumnConfigDTOs) {
    log.debug("Request to update UserColumnConfig {}", userColumnConfigDTOs);

    List<UserColumnConfig> userColumnConfigs = this.checkUserColumnConfigs(this.userColumnConfigMapper.toEntity(userColumnConfigDTOs));
    List<UserColumnConfig> result = new ArrayList<UserColumnConfig>();

    for (UserColumnConfig userColumnConfig : userColumnConfigs) {
      result.add(this.userColumnConfigRepository.saveAndFlush(userColumnConfig));
    }

    return this.userColumnConfigMapper.toDto(result);
  }

  /**
   * Generates all missing ColumnConfigs for a User
   * @param user
   */
  public void generateColumnConfigsForUser(User user) {
    List<ColumnConfig> columnConfigs = this.columnConfigRepository.findAll();

    for(ColumnConfig columnConfig : columnConfigs) {
      if (!this.userColumnConfigRepository.existsByColumnConfigAndUser(columnConfig, user)) {

        this.log.debug("Generating ColumnConfig {} for User {}", columnConfig.getEntity() + "." + columnConfig.getColumnName(), user.getLogin());

        UserColumnConfig newConfig = new UserColumnConfig();
        newConfig.setColumnConfig(columnConfig);
        newConfig.setUser(user);
        newConfig.setVisible(columnConfig.isDefaultVisible());
        newConfig.setPosition(columnConfig.getDefaultPosition());

        this.userColumnConfigRepository.saveAndFlush(newConfig);
      }
    }
  }

  /**
   * Deletes all ColumnConfigs for a User
   * @param user
   */
  public void deleteColumnConfigsForUser(User user) {
    List<UserColumnConfig> toDelete = this.userColumnConfigRepository.findByUser(user);

    for (UserColumnConfig userColumnConfig : toDelete) {
      this.userColumnConfigRepository.delete(userColumnConfig);
    }
  }

  private List<UserColumnConfig> checkUserColumnConfigs(List<UserColumnConfig> userColumnConfigs) {
    List<UserColumnConfig> result = new ArrayList<UserColumnConfig>();

    if (userColumnConfigs.size() > 0) {

      String listEntity = null;
      int countVisible = 0;

      for (UserColumnConfig userColumnConfig : userColumnConfigs) {
        ColumnConfig columnConfigFromDB = this.columnConfigRepository.getOne(userColumnConfig.getColumnConfig().getId());
        User userFromDB = this.userRepository.getOne(userColumnConfig.getUser().getId());

        if (listEntity == null) {
          listEntity = columnConfigFromDB.getEntity();
        }

        if (!userFromDB.getLogin().equals(SecurityUtils.getCurrentUserLogin())) {
          throw new ServiceValidationException(ServiceErrorConstants.userColumnConfigUpdateNotAllowed,
            null);
        }

        if (!listEntity.equals(columnConfigFromDB.getEntity())) {
          throw new ServiceValidationException(ServiceErrorConstants.userColumnConfigUpdateForSeveralLists, null);
        }

        if (userColumnConfig.isVisible()) {
          countVisible++;
        }

        result.add(userColumnConfig);
      }

      if (countVisible == 0) {
        throw new ServiceValidationException(ServiceErrorConstants.userColumnConfigUpdateMinimumOneSelected, null);
      }

    }

    return result;
  }
}
