package at.simianarmy.service;

import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.repository.PersonInstrumentRepository;
import at.simianarmy.service.dto.PersonInstrumentDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonInstrumentMapper;
import at.simianarmy.service.specification.PersonSpecifications;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing PersonInstrument.
 */
@Service
@Transactional
public class PersonInstrumentService {

	private final Logger log = LoggerFactory.getLogger(PersonInstrumentService.class);

	@Inject
	private PersonInstrumentRepository personInstrumentRepository;

	@Inject
	private PersonInstrumentMapper personInstrumentMapper;

	/**
	 * Save a personInstrument.
	 *
	 * @param personInstrumentDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonInstrumentDTO save(PersonInstrumentDTO personInstrumentDTO) {
		log.debug("Request to save PersonInstrument : {}", personInstrumentDTO);
		PersonInstrument personInstrument = personInstrumentMapper
				.personInstrumentDTOToPersonInstrument(personInstrumentDTO);
		if (personInstrumentDTO.getBeginDate() != null && personInstrumentDTO.getEndDate() != null) {
			if (personInstrumentDTO.getBeginDate().isAfter(personInstrumentDTO.getEndDate())) {
				throw new ServiceValidationException(ServiceErrorConstants.personInstrumentEndBeforeBegin, null);
			}
		}
		personInstrument = personInstrumentRepository.save(personInstrument);
		PersonInstrumentDTO result = personInstrumentMapper.personInstrumentToPersonInstrumentDTO(personInstrument);
		return result;
	}

	/**
	 * Get all the personInstruments.
	 * 
	 * @param personId
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonInstrumentDTO> findAll(Long personId, Pageable pageable) {
		log.debug("Request to get all PersonInstruments");

		Specification<PersonInstrument> searchSpec = Specification
				.where(PersonSpecifications.hasPersonIdFromPersonInstrument(personId));

		Page<PersonInstrument> result = personInstrumentRepository.findAll(searchSpec, pageable);
		return result.map(
				personInstrument -> personInstrumentMapper.personInstrumentToPersonInstrumentDTO(personInstrument));
	}

	/**
	 * Get all the personInstruments for an Instrument.
	 * 
	 * @param instrumentId
	 *            the Instrument, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonInstrumentDTO> findByInstrument(Long instrumentId, Pageable pageable) {
		log.debug("Request to get all PersonInstruments for an Instrument");
		Page<PersonInstrument> result = personInstrumentRepository.findByInstrument(instrumentId, pageable);
		return result.map(
				personInstrument -> personInstrumentMapper.personInstrumentToPersonInstrumentDTO(personInstrument));
	}

	/**
	 * Get one personInstrument by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonInstrumentDTO findOne(Long id) {
		log.debug("Request to get PersonInstrument : {}", id);
		PersonInstrument personInstrument = personInstrumentRepository.findById(id).orElse(null);
		PersonInstrumentDTO personInstrumentDTO = personInstrumentMapper
				.personInstrumentToPersonInstrumentDTO(personInstrument);
		return personInstrumentDTO;
	}

	/**
	 * Delete the personInstrument by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete PersonInstrument : {}", id);
		personInstrumentRepository.deleteById(id);
	}
}
