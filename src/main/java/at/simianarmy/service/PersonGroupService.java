package at.simianarmy.service;

import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonGroupMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service Implementation for managing PersonGroup.
 */
@Service
@Transactional
public class PersonGroupService {

	private final Logger log = LoggerFactory.getLogger(PersonGroupService.class);

	@Inject
	private PersonGroupRepository personGroupRepository;

	@Inject
	private PersonGroupMapper personGroupMapper;

	/**
	 * Save a personGroup.
	 *
	 * @param personGroupDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonGroupDTO save(PersonGroupDTO personGroupDTO) {
		log.debug("Request to save PersonGroup : {}", personGroupDTO);
		PersonGroup personGroup = personGroupMapper.personGroupDTOToPersonGroup(personGroupDTO);
		if (personGroup.getParent() != null) {
			checkCircularReference(personGroup, personGroup.getParent());
		}
		personGroup = personGroupRepository.save(personGroup);
		PersonGroupDTO result = personGroupMapper.personGroupToPersonGroupDTO(personGroup);
		return result;
	}

	@Transactional
	private void checkCircularReference(PersonGroup personGroup, PersonGroup searchedParent) {
		// If the personGroup has no id (is new), there is no possibilty of
		// circular references
		if (personGroup.getId() == null) {
			return;
		}

		// to get the children, we have to query the database
		personGroup = personGroupRepository.findById(personGroup.getId()).orElse(null);
		// searchedParent must not a child of personGroup
		for (PersonGroup pg : personGroup.getChildren()) {
			if (pg.equals(searchedParent)) {
				throw new ServiceValidationException(ServiceErrorConstants.personGroupCircularReference, null);
			}
			checkCircularReference(pg, searchedParent);

		}

	}

	/**
	 * Get all the personGroups.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<PersonGroupDTO> findAll(Pageable pageable) {
		log.debug("Request to get all PersonGroups");
		Page<PersonGroup> result = personGroupRepository.findAll(pageable);
		return result.map(personGroup -> personGroupMapper.personGroupToPersonGroupDTO(personGroup));
	}

	/**
	 * Get one personGroup by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonGroupDTO findOne(Long id) {
		log.debug("Request to get PersonGroup : {}", id);
		PersonGroup personGroup = personGroupRepository.findById(id).orElse(null);
		PersonGroupDTO personGroupDTO = personGroupMapper.personGroupToPersonGroupDTO(personGroup);
		return personGroupDTO;
	}

	/**
	 * Delete the personGroup by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Transactional
	public void delete(Long id) {
		// Is the person parent of any other PersonGroups
		PersonGroup toDelete = personGroupRepository.findById(id).orElse(null);
		if (toDelete.getChildren() != null) {
			for (PersonGroup pg : toDelete.getChildren()) {
				pg.setParent(toDelete.getParent());
				personGroupRepository.save(pg);
			}
		}

		log.debug("Request to delete PersonGroup : {}", id);
		personGroupRepository.deleteById(id);
	}

  /**
   * Gets all person groups which are marked as "person-relevant" and their children
   * @return A list of person groups
   */
	@Transactional
  public List<PersonGroup> getAllPersonRelevantPersonGroups() {

	  List<PersonGroup> personRelevantParents = personGroupRepository.findByHasPersonRelevanceIsTrue();
    Set<PersonGroup> personRelevant = getAllPersonRelevantPersonGroupsRecursive(personRelevantParents);
	  return new ArrayList<>(personRelevant);
  }

  private Set<PersonGroup> getAllPersonRelevantPersonGroupsRecursive(List<PersonGroup> personRelevantParents) {
    Set<PersonGroup> result = new HashSet<>();

	  for (PersonGroup personRelevantParent : personRelevantParents ) {
      result.add(personRelevantParent);
      Set<PersonGroup> children = getAllPersonRelevantPersonGroupsRecursive(new ArrayList<>(personRelevantParent.getChildren()));
      result.addAll(children);
    }
    return result;

	}
}
