package at.simianarmy.service;

import at.simianarmy.domain.*;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.imports.data.service.DataImporterService;
import at.simianarmy.letter.service.SerialLetterService;
import at.simianarmy.letter.service.SerialLetterServiceFactory;
import at.simianarmy.letter.service.SerialLetterServiceFactoryException;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.dto.MailLetterDTO;
import at.simianarmy.service.dto.MemberDTO;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.dto.PersonCollectionDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.dto.*;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.LetterMapper;
import at.simianarmy.service.mapper.MailLetterMapper;
import at.simianarmy.service.mapper.FullPersonMapper;
import at.simianarmy.service.mapper.PersonMapper;
import at.simianarmy.service.specification.PersonSpecifications;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lowagie.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 * Service Implementation for managing Person.
 */
@Service
@Transactional
public class PersonService {

	private final Logger log = LoggerFactory.getLogger(PersonService.class);

	private final Long MONTHS_TO_DELETE_PERSON = 48L;

	@Inject
	private PersonRepository personRepository;

	@Inject
	private PersonGroupRepository personGroupRepository;

	@Inject
	private MembershipRepository membershipRepository;

	@Inject
	private MembershipService membershipService;

	@Inject
	private MembershipFeeAmountRepository membershipFeeAmountRepository;

	@Inject
  private EmailVerificationLinkService emailVerificationLinkService;

	@Inject
	private PersonMapper personMapper;

	@Inject
  private MailLetterMapper mailLetterMapper;

	@Inject
  private SerialLetterServiceFactory serialLetterServiceFactory;

	@Inject
  private FullPersonMapper fullPersonMapper;

	@Inject
  private DataImporterService dataImporterService;

	@Inject
  private CustomizationService customizationService;

  @Inject
  private SpringTemplateEngine templateEngine;

  @Inject
  private PersonGroupService personGroupService;
	/**
	 * Save a person.
	 *
	 * @param personDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public PersonDTO save(PersonDTO personDTO) {
		log.debug("Request to save Person : {}", personDTO);

		EmailType currentEmailType = this.getEmailTypeForPerson(personDTO.getId());
		boolean hasEmailVerificationLink = this.personHasEmailVerificationLink(personDTO);
		boolean needsEmailVerificationLink = this.personNeedsEmailVerificationLink(personDTO);

		Person person = personMapper.personDTOToPerson(personDTO);

		person = personRepository.saveAndFlush(person);

		if (needsEmailVerificationLink) {
		  if (hasEmailVerificationLink) {
		    this.emailVerificationLinkService.invalidateEmailVerificationLinkForPerson(person.getId());
      }
		  this.emailVerificationLinkService.createEmailVerificationLinkForPerson(person.getId());
    }

    if (currentEmailType != null &&
      currentEmailType.equals(EmailType.VERIFIED) &&
      person.getEmailType().equals(EmailType.MANUALLY) &&
      hasEmailVerificationLink) {
      this.emailVerificationLinkService.invalidateEmailVerificationLinkForPerson(person.getId());
    }

		PersonDTO result = personMapper.personToPersonDTO(person);
		return result;
	}

	/**
	 * get all the people where User is null.
	 *
	 * @return the list of entities
	 */
//	@Transactional(readOnly = true)
//	public List<PersonDTO> findAllWhereUserIsNull() {
//		log.debug("Request to get all people where User is null");
//		return StreamSupport.stream(personRepository.findAll().spliterator(), false)
//				.filter(person -> person.getUser() == null).map(personMapper::personToPersonDTO)
//				.collect(Collectors.toCollection(LinkedList::new));
//	}

	/**
	 * Get one person by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public PersonDTO findOne(Long id) {
		log.debug("Request to get Person : {}", id);
		Person person = personRepository.findOneWithEagerPersonGroupRelations(id);
		PersonDTO personDTO = personMapper.personToPersonDTO(person);
		return personDTO;
	}

	/**
	 * Delete the person by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Person : {}", id);
		personRepository.deleteById(id);
	}

	/**
	 * Finds all Persons with the text-pattern in the first name or last name and
	 * with birthdates between begin and enddate.
	 *
	 * @param pageable
	 *            the pagination information
	 * @param namePattern
	 *            pattern which is in the first name or last name
	 * @param begin
	 *            begin-date of the selection
	 * @param end
	 *            end-date of the selection
	 *
	 * @return PersonDTOs which match the selection
	 */
	@Transactional(readOnly = true)
	public Page<PersonDTO> findAll(String namePattern, LocalDate begin, LocalDate end, Gender gender,
			List<Long> personGroupIds, Pageable pageable) {
		log.debug("Request to get matching People");

		// Check if enddate before begindate
		if (end != null & begin != null) {
			if (end.isBefore(begin)) {
				throw new ServiceValidationException(ServiceErrorConstants.personSearchEndBeforeBegin, null);
			}
		}

		List<Long> personGroupIdsRecursive = null;

		if (personGroupIds != null) {
			personGroupIdsRecursive = new ArrayList<Long>(personGroupIds);

			for (Long id : personGroupIds) {
				PersonGroup personGroup = this.personGroupRepository.findById(id).orElse(null);
				this.getPersonGroupIdsForPersonGroup(personGroup, personGroupIdsRecursive);
			}
		}

		Specification<Person> searchSpec = Specification.where(PersonSpecifications.betweenDates(begin, end))
				.and(PersonSpecifications.hasGender(gender))
				.and(PersonSpecifications.firstOrLastNameContain(namePattern))
				.and(PersonSpecifications.isInPersonGroup(personGroupIdsRecursive));

		Page<Person> result = personRepository.findAll(searchSpec, pageable);
		return result.map(person -> personMapper.personToPersonDTO(person));
	}

	/**
	 * Find all Persons for a specified personGroup.
	 *
	 * @param personGroupId
	 *            the personGroupId to search for
	 * @return PersonDTOs which are in the specified Group
	 */
	@Transactional(readOnly = true)
	public Page<PersonDTO> findByPersonGroup(Long personGroupId, Pageable pageable) {

		PersonGroup personGroup = this.personGroupRepository.findById(personGroupId).orElse(null);

		if (personGroup == null) {
			throw new ServiceValidationException(ServiceErrorConstants.personGroupNotFound, null);
		}

		List<Long> personGroupIds = new ArrayList<Long>();
		this.getPersonGroupIdsForPersonGroup(personGroup, personGroupIds);

		Page<Person> result = this.personRepository.findByPersonGroups(personGroupIds, pageable);

		return result.map(person -> personMapper.personToPersonDTO(person));
	}

	/**
	 * Find all Persons for a specified Appointment.
	 *
	 * @param appointmentId
	 *            the officialAppointmentId to search for
	 * @return PersonDTOs which are in the specified Group
	 */
	@Transactional(readOnly = true)
	public Page<PersonDTO> findByAppointment(Long appointmentId, Pageable pageable) {
		log.debug("Request to get Persons for an Appointment");
		Page<Person> result = personRepository.findByAppointment(appointmentId, pageable);
		return result.map(person -> personMapper.personToPersonDTO(person));
	}

	/**
	 * Find all Persons with a currently active paid membership.
	 *
	 * @return PersonDTOs with a currently active paid membership
	 */
	@Transactional(readOnly = true)
	public Page<PersonDTO> findAllWithCurrentFees(Pageable pageable) {
		log.debug("Request to get a page of all Persons with current paid Memberships");
		Page<Person> result = personRepository.findAllWithCurrentFees(pageable);

		List<PersonDTO> resultDTO = new ArrayList<PersonDTO>();

		for (Person person : result) {
			BigDecimal amount = this.getOpenMembershipAmount(person, null, null);
			PersonDTO dto = personMapper.personToPersonDTO(person);
			dto.setOpenMembershipAmount(amount);
			resultDTO.add(dto);
		}

		return new PageImpl<PersonDTO>(resultDTO, pageable, result.getTotalElements());
	}

  /**
   * Find all Persons with an invalid Mail
   *
   * @param mailLetterDTO
   * @return PersonDTOs with an invalid Mail
   */
  @Transactional(readOnly = true)
  public List<PersonDTO> findAllWithValidMailByMailLetter(MailLetterDTO mailLetterDTO) {

    log.debug("Reqeust to get a list of all Persons with vvalid Mail-Adresses {}", mailLetterDTO);

    Letter letter = this.mailLetterMapper.mailLetterDTOToLetter(mailLetterDTO);

    try {
      SerialLetterService serialLetterService = this.serialLetterServiceFactory
        .createForLetter(letter);

      List<Person> result = serialLetterService.getRelevantPersonsForMailLetter(letter);

      return personMapper.peopleToPersonDTOs(result);
    } catch (SerialLetterServiceFactoryException e) {
      throw new ServiceValidationException(e.getMessage(), null);
    }

  }

  /**
   * Find all Persons with an invalid Mail
   *
   * @param mailLetterDTO
   * @return PersonDTOs with an invalid Mail
   */
	@Transactional(readOnly = true)
  public List<PersonDTO> findAllWithInvalidMailByMailLetter(MailLetterDTO mailLetterDTO) {

	  log.debug("Reqeust to get a list of all Persons with invalid Mail-Adresses {}", mailLetterDTO);

	  Letter letter = this.mailLetterMapper.mailLetterDTOToLetter(mailLetterDTO);

	  try {
      SerialLetterService serialLetterService = this.serialLetterServiceFactory
        .createForLetter(letter);

      List<Person> result = serialLetterService.getPersonsWithInactiveMail(letter);

      return personMapper.peopleToPersonDTOs(result);
    } catch (SerialLetterServiceFactoryException e) {
	    throw new ServiceValidationException(e.getMessage(), null);
    }

  }

	/**
	 * Find all Persons with a currently active paid membership.
	 *
	 * @return PersonDTOs with a currently active paid membership
	 */
	@Transactional(readOnly = true)
	public Page<PersonDTO> findAllWithUnpaidFees(Boolean previousPeriods, Boolean currentPeriod, Pageable pageable) {
		log.debug("Request to get a page of all Persons with unpaid MembershipFeesForPeriod(s)");

		int yearFrom;
		int yearTo;

		if (Boolean.TRUE.equals(previousPeriods)) {
			yearFrom = 1900;
		} else {
			yearFrom = LocalDate.now().getYear();
		}

		if (Boolean.TRUE.equals(currentPeriod)) {
			yearTo = LocalDate.now().getYear();
		} else {
			yearTo = LocalDate.now().getYear() - 1;
		}

		Page<Person> result = personRepository.findAllWithUnpaidFeesAsPage(yearFrom, yearTo, pageable);
		List<PersonDTO> resultDTO = new ArrayList<PersonDTO>();

		for (Person person : result) {
			BigDecimal amount = this.getOpenMembershipAmount(person, currentPeriod, previousPeriods);
			PersonDTO dto = personMapper.personToPersonDTO(person);
			if (amount.compareTo(BigDecimal.ZERO) == 1) {
				dto.setOpenMembershipAmount(amount);
				resultDTO.add(dto);
			}

		}

		return new PageImpl<PersonDTO>(resultDTO, pageable, result.getTotalElements());
	}

	/**
	 * Get all PersonGroupIds for a PersonGroup recursively without duplicates.
	 *
	 * @param personGroup
	 *            The PersonGroup to search for
	 * @param personGroupIds
	 *            The Result-List
	 */
	public void getPersonGroupIdsForPersonGroup(PersonGroup personGroup, List<Long> personGroupIds) {

		if (!personGroupIds.contains(personGroup.getId())) {
			personGroupIds.add(personGroup.getId());
		}

		for (PersonGroup subGroup : personGroup.getChildren()) {
			this.getPersonGroupIdsForPersonGroup(subGroup, personGroupIds);
		}

	}

	@Transactional
	public BigDecimal getOpenMembershipAmount(Person person, Boolean currentPeriod, Boolean previousPeriod) {
		BigDecimal amount = new BigDecimal(0);
		Membership membership;
		MembershipFeeAmount feeAmount;
		for (MembershipFeeForPeriod mfp : getOpenMembershipFeeForPeriods(person, currentPeriod, previousPeriod)) {
			membership = mfp.getMembership();
			if (membership != null && membership.getId() != null && mfp.getYear() != null
					&& membership.getMembershipFee() != null) {
				feeAmount = membershipFeeAmountRepository
						.findByMembershipFeeAndYear(membership.getMembershipFee().getId(), mfp.getYear());
				if (feeAmount != null) {
					amount = amount.add(feeAmount.getAmount());
				}
			}
		}

		return amount;
	}

	@Transactional
	public List<MembershipFeeForPeriod> getOpenMembershipFeeForPeriods(Person p, Boolean currentPeriod,
			Boolean previousPeriod) {

		List<MembershipFeeForPeriod> openMembershipFeeForPeriods = new ArrayList<>();
		if ((currentPeriod != null && currentPeriod == true) || (previousPeriod != null && previousPeriod == true)) {
			log.debug("reading openMembershipFeeForPeriods for Person {}", p);
			int currentYear = LocalDate.now().getYear();
			List<Membership> memberships;
			if (currentPeriod == true) {
				memberships = membershipRepository.findByPersonAndYear(p.getId(), LocalDate.now().getYear());
			} else {
				memberships = membershipRepository.findByPerson(p.getId());
			}

			for (Membership m : memberships) {
				Set<MembershipFeeForPeriod> membershipFeeForPeriods = m.getMembershipFeeForPeriods();
				for (MembershipFeeForPeriod mfp : membershipFeeForPeriods) {
					if (mfp.getYear().equals(currentYear) && currentPeriod && mfp.isPaid() != true) {
						openMembershipFeeForPeriods.add(mfp);
					}
					if (mfp.getYear() < currentYear && previousPeriod && mfp.isPaid() != true) {
						openMembershipFeeForPeriods.add(mfp);
					}
				}

			}
		}
		return openMembershipFeeForPeriods;
	}

	/**
	 * Creates a new member. That is a new person with a membership
	 * 
	 * @param memberDTO
	 *            The member data
	 * @return A memberDTO filled with the ids
	 */
	@Transactional
	public MemberDTO saveMember(MemberDTO memberDTO) {

		PersonDTO person = save(memberDTO.getPerson());

		MembershipDTO membership = memberDTO.getMembership();
		membership.setPersonId(person.getId());

		membershipService.save(membership);

		memberDTO.setMembership(membership);
		memberDTO.setPerson(person);
		return memberDTO;
	}

  /**
   * Imports Person data from given file
   * @param file
   * @return
   */
	@Transactional
  public PersonCollectionDTO importPersonData(MultipartFile file) {
    PersonCollectionDTO personCollectionDTO = dataImporterService.importPersonsFromFile(file);
    return  personCollectionDTO;
  }

  /**
   * Persists the given PersonCollection and returns the erroneous entities
   * @param personCollectionDTO Collection of persons to persistFile
   * @return Erroneous entities
   */
  public PersonCollectionDTO savePersonCollection(PersonCollectionDTO personCollectionDTO) {

	  List<PersonDTO> personsError = new ArrayList<>();
	  List<MemberDTO> membersError = new ArrayList<>();

	  List<PersonDTO> persons = personCollectionDTO.getPersons();
	  List<MemberDTO> members = personCollectionDTO.getMembers();

	  if (persons != null) {
      for (PersonDTO person : persons) {
        try {
          save(person);
        } catch (DataAccessException ex) {
          personsError.add(person);
          log.error("Couldn't persistFile person {}", person, ex);
        }
      }
    }
	  if (members != null) {
      for (MemberDTO member : members) {
        try {
          saveMember(member);
        } catch (DataAccessException ex) {
          membersError.add(member);
          log.error("Couldn't persistFile person {}", member, ex);
        }
      }
    }

    PersonCollectionDTO errorPersonCollectionDTO = new PersonCollectionDTO();
    errorPersonCollectionDTO.setPersons(personsError);
    errorPersonCollectionDTO.setMembers(membersError);

	  return errorPersonCollectionDTO;
  }

  /**
   * Gets the person dto with all stored information for this person
   * @param personId The id of the person
   * @return A dto with all information stored for the given person
   */
  @Transactional
  public FullPersonDTO findOneWithFullInformation(Long personId) {

    Person person = personRepository.findOneWithAllEagerRelations(personId);

    FullPersonDTO fullPersonDTO = fullPersonMapper.personToFullPersonDTO(person);


    Customization company = customizationService.getOne(CustomizationName.ORGANIZATION_COMPANY);
    if (company != null && company.getText() != null) {
       fullPersonDTO.setOrganizationName(company.getText());
    }

    fullPersonDTO.setCreatedAt(LocalDateTime.now());

    //remove persons from appointments
    fullPersonDTO.getAppointments().stream().forEach(ap -> ap.setPersons(null));

    return fullPersonDTO;

  }

  /**
   * Returs all information to a person as json-File
   * @param personId The id of the person to get the information for
   * @return A file with the person information in json-format
   */
  public Resource findOneWithFullInformationAsJson(Long personId) {

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

    FullPersonDTO fullPersonDTO = findOneWithFullInformation(personId);

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    try {
      mapper.writeValue(out, fullPersonDTO);
    } catch (IOException e) {
       log.error("Could not write full information to file", e);
    }

    ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());

    return new InputStreamResource(bis);

  }


  /**
   * Sets the lost-relevance-date for all persons which are not relevant anymore and are not marked as such. That means that
   * the date for "lost-relevance" is not filled.
   *
   * The lost-relevance-date is the date of the last day where relevance is given
   *
   * The relevance of a person at a given date is defined as follows:
   * - the person has a persongroup with "person-relevance" or
   * - the person has a membership at the given date
   *
   * @param dueDate the date for the described check
   */
  public void markNotRelevantPersons(LocalDate dueDate) {
    if (dueDate == null) {
      throw new IllegalArgumentException("due-Date has to be filled");
    }
    List<Long> relevantPersonGroupIds = getPersonRelevantPersonGroupIds();

    List<Person> personsToMark = personRepository.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(dueDate, relevantPersonGroupIds);

    if (personsToMark == null || personsToMark.size() < 1) {
      return;
    }
    log.debug("got persons to mark as not relevant {}", personsToMark.size());
    LocalDate dateToSet;
    for (Person p : personsToMark) {
      dateToSet = dueDate;
      Set<Membership> memberships = p.getMemberships();
      //get highest membership-enddate
      if (memberships != null && memberships.size() > 0) {
        LocalDate latestEnd = null;
        for (Membership m : memberships) {
           if (latestEnd == null || m.getEndDate().isAfter(latestEnd)) {
             latestEnd = m.getEndDate();
           }
        }
        dateToSet = latestEnd;
      }
      p.setDateLostRelevance(dateToSet);
    }
    personRepository.saveAll(personsToMark);
  }

  /**
   * Deletes the lost-relevance-date for all persons which are relevant but are marked as not relevant. That means that
   * the date for "lost-relevance" is  filled.
   *
   * The relevance of a person at a given date is defined as follows:
   * - the person has a persongroup with "person-relevance" or
   * - the person has a membership at the given date
   *
   * @param dueDate the date for the described check
   */
  public void unmarkRelevantPersons(LocalDate dueDate) {
    if (dueDate == null) {
      throw new IllegalArgumentException("due-Date has to be filled");
    }
    List<Long> relevantPersonGroupIds = getPersonRelevantPersonGroupIds();

    List<Person> personsToUnmark = personRepository.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(dueDate, relevantPersonGroupIds);
    if (personsToUnmark == null || personsToUnmark.size() < 1) {
      return;
    }

    log.debug("Got persons to unmark from 'non-relevance': {}", personsToUnmark.size());
    for (Person p : personsToUnmark) {
      p.setDateLostRelevance(null);
    }
    personRepository.saveAll(personsToUnmark);

  }

  /**
   * Deletes persons which are marked as not relevant after the configured delete-period
   * @param dueDate The date for checking the persons to delete
   */
  public void deletePersonsWhichAreNotRelevant(LocalDate dueDate) {

    if (dueDate == null) {
      throw new IllegalArgumentException("due-date can't be null");
    }
    Long monthsToDelete = customizationService.getMonthsToDeletePersonData();
    if (monthsToDelete == null) {
      monthsToDelete = MONTHS_TO_DELETE_PERSON;
    }
    log.debug("Delete persons by {} and with delete-period: {} ", dueDate, monthsToDelete);

    LocalDate deleteDate = dueDate.minusMonths(monthsToDelete);

    List<Person> personsToDelete = personRepository.findAllByDateLostRelevanceIsBefore(deleteDate);

    if (personsToDelete == null || personsToDelete.size() < 1) {
      return;
    }
    log.debug("Got persons to delete: {}", personsToDelete.size());
    personRepository.deleteAll(personsToDelete);
  }

  private List<Long> getPersonRelevantPersonGroupIds() {
    List<PersonGroup> relevantPersonGroups = personGroupService.getAllPersonRelevantPersonGroups();
    List<Long> relevantPersonGroupIds = new ArrayList<>();
    if (relevantPersonGroups != null) {
      relevantPersonGroupIds = relevantPersonGroups.stream().map(group -> group.getId()).collect(Collectors.toList());
    }
    if (relevantPersonGroupIds.isEmpty()) {
      return null;
    }
    return relevantPersonGroupIds;
  }

  /**
   * Returs all information to a person as pdf-File
   * @param personId The id of the person to get the information for
   * @return A file with the person information in pdf-format
   */
  public Resource findOneWithFullInformationAsPDF(Long personId) {

    FullPersonDTO fullPersonDTO = findOneWithFullInformation(personId);


    String fullInformationAsHtml = "<html><h1>Hallo!</h1></html>";

    Locale locale = Locale.forLanguageTag("DE");
    Context context = new Context(locale);
    context.setVariable("person", fullPersonDTO);

    fullInformationAsHtml = templateEngine.process("reports/personInfo", context);


    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ITextRenderer renderer = new ITextRenderer();
    renderer.setDocumentFromString(fullInformationAsHtml);
    renderer.layout();
    try {
      renderer.createPDF(bos);
      ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());

      return new InputStreamResource(bis);
    } catch (DocumentException e) {
      throw new ServiceValidationException("Could not write full Information", new String[]{personId.toString()});
    }

  }





  private boolean personHasEmailVerificationLink(PersonDTO personDTO) {
    if (personDTO.getId() == null) {
      return false;
    } else {
      return this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(personDTO.getId()) != null;
    }
  }

  private boolean personNeedsEmailVerificationLink(PersonDTO personDTO) {

    if (personDTO.getId() == null) {
      if (personDTO.getEmailType().equals(EmailType.VERIFIED)) {
        return true;
      } else if (personDTO.getEmailType().equals(EmailType.MANUALLY )) {
        return false;
      }
    } else {

      EmailType currentEmailType = this.getEmailTypeForPerson(personDTO.getId());

      if (!currentEmailType.equals(personDTO.getEmailType())) {
        if (personDTO.getEmailType().equals(EmailType.VERIFIED)) {
          return true;
        } else if (personDTO.getEmailType().equals(EmailType.MANUALLY )) {
          return false;
        }
      }
    }

    return false;
  }

  private EmailType getEmailTypeForPerson(Long personId) {
    if (personId == null) {
      return null;
    }

    Person currentPerson = this.personRepository.findById(personId).get();
    return currentPerson.getEmailType();
  }



}
