package at.simianarmy.service;

import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.InstrumentRepository;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.mapper.CashbookEntryMapper;
import at.simianarmy.service.mapper.InstrumentMapper;
import at.simianarmy.service.specification.InstrumentSpecification;
import at.simianarmy.service.exception.ServiceValidationException;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Instrument.
 */
@Service
@Transactional
public class InstrumentService {

	private final Logger log = LoggerFactory.getLogger(InstrumentService.class);

	@Inject
	private InstrumentRepository instrumentRepository;

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private InstrumentMapper instrumentMapper;

	@Inject
	private CashbookEntryMapper cashbookEntryMapper;

	@Inject
	private CustomizationService customizationService;

	/**
	 * Save a instrument.
	 *
	 * @param instrumentDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public InstrumentDTO save(InstrumentDTO instrumentDTO) {
		log.debug("Request to save Instrument : {}", instrumentDTO);
		Instrument instrument = instrumentMapper.instrumentDTOToInstrument(instrumentDTO);
		LocalDate today = LocalDate.now();
		LocalDate instrumentDate = instrument.getPurchaseDate();
		if (instrument.getType() == null) {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentNoType, null);
		} else if (instrumentDate != null && instrumentDate.isAfter(today)) {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentDateInFuture, null);
		} else {
			if (instrument.isPrivateInstrument() == null) {
				instrument.setPrivateInstrument(false);
			}
			if (!instrument.isPrivateInstrument() && instrument.getCashbookEntry() == null
					&& instrument.getPrice() != null && instrument.getPrice().signum() != 0) { // Instrument != private
																								// -> mach
																								// Kassabucheintrag
				CashbookEntryDTO cashbookEntryDTO = new CashbookEntryDTO();
				cashbookEntryDTO.setAmount(instrumentDTO.getPrice());
				cashbookEntryDTO.setDate(instrumentDate);
				String title = "Instrumentenankauf: '" + instrument.getName() + "'";
				cashbookEntryDTO.setTitle(title);
				CashbookEntryType type = CashbookEntryType.SPENDING;
				cashbookEntryDTO.setType(type);
				CashbookEntry cashbookEntry = cashbookEntryMapper.cashbookEntryDTOToCashbookEntry(cashbookEntryDTO);
				cashbookEntry.setCashbookAccount(this.customizationService.getDefaultCashbookAccount());
				cashbookEntry = cashbookEntryRepository.save(cashbookEntry);
				instrument.setCashbookEntry(cashbookEntry);
			} else if (instrument.getCashbookEntry() != null) {
				if (instrument.isPrivateInstrument()) {
					Long entryId = instrument.getCashbookEntry().getId();
					instrument.setCashbookEntry(null);
					cashbookEntryRepository.deleteById(entryId);
				} else {
					CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
							.cashbookEntryToCashbookEntryDTO(instrument.getCashbookEntry());
					cashbookEntryDTO.setAmount(instrumentDTO.getPrice());
					cashbookEntryDTO.setDate(instrumentDate);
					String title = "Instrumentenankauf: '" + instrument.getName() + "'";
					cashbookEntryDTO.setTitle(title);
					CashbookEntryType type = CashbookEntryType.SPENDING;
					cashbookEntryDTO.setType(type);
					CashbookEntry cashbookEntry = cashbookEntryMapper.cashbookEntryDTOToCashbookEntry(cashbookEntryDTO);
					cashbookEntry = cashbookEntryRepository.save(cashbookEntry);
					instrument.setCashbookEntry(cashbookEntry);
				}
			}
			instrument = instrumentRepository.save(instrument);
			InstrumentDTO result = instrumentMapper.instrumentToInstrumentDTO(instrument);
			return result;
		}
	}

	/**
	 * Get all the instruments.
	 * 
	 * @param name
	 *            the name
	 * @param type
	 *            the name of the instrument type
	 * @param privateInstrument
	 *            if the instrument is private
	 * @param dateBegin
	 *            the lower bound of the date range
	 * @param dateEnd
	 *            the upper bound of the date range
	 * @param priceBegin
	 *            the lower bound of the price range
	 * @param priceEnd
	 *            the upper bound of the price range
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<InstrumentDTO> findAll(String name, String type, Boolean privateInstrument, LocalDate dateBegin,
			LocalDate dateEnd, BigDecimal priceBegin, BigDecimal priceEnd, Pageable pageable) {
		log.debug("Request to get all Instruments");

		InstrumentSpecification specification = new InstrumentSpecification();

		Page<Instrument> result = instrumentRepository.findAll(specification.hasName(name).hasType(type)
				.hasPrivate(privateInstrument).hasDate(dateBegin, dateEnd).hasPrice(priceBegin, priceEnd).build(),
				pageable);
		return result.map(instrument -> instrumentMapper.instrumentToInstrumentDTO(instrument));
	}

	/**
	 * Get all the instruments for an InstrumentType.
	 * 
	 * @param typeId
	 *            the Type, which we want to retrieve instruments from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<InstrumentDTO> findByType(Long typeId, Pageable pageable) {
		log.debug("Request to get all Instruments for an InstrumentType");
		Page<Instrument> result = instrumentRepository.findByType(typeId, pageable);
		return result.map(instrument -> instrumentMapper.instrumentToInstrumentDTO(instrument));
	}

	/**
	 * Get one instrument by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public InstrumentDTO findOne(Long id) {
		log.debug("Request to get Instrument : {}", id);
		Instrument instrument = instrumentRepository.findById(id).orElse(null);
		InstrumentDTO instrumentDTO = instrumentMapper.instrumentToInstrumentDTO(instrument);
		return instrumentDTO;
	}

	/**
	 * Delete the instrument by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Instrument : {}", id);
		Instrument instrument = instrumentRepository.findById(id).orElse(null);
		if ((instrument.getInstrumentServices() == null || instrument.getInstrumentServices().isEmpty())
				&& (instrument.getPersonInstruments() == null || instrument.getPersonInstruments().isEmpty())) {
			Long entryId = null;
			if (instrumentRepository.findById(id).orElse(null).getCashbookEntry() != null) {
				entryId = instrumentRepository.findById(id).orElse(null).getCashbookEntry().getId();
			}
			instrumentRepository.deleteById(id);
			if (entryId != null) {
				cashbookEntryRepository.deleteById(entryId);
			}
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentNotDeletable, null);
		}
	}
}
