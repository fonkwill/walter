package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Customization;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.export.akm.xml.Allgemeines;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.CustomizationRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.service.dto.CustomizationDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CustomizationMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Service Implementation for managing Customization.
 */
@Service
@Transactional
public class CustomizationService {

	private final Logger log = LoggerFactory.getLogger(CustomizationService.class);

	private final CustomizationRepository customizationRepository;

	private final CustomizationMapper customizationMapper;

	private final PersonGroupRepository personGroupRepository;

	private final CashbookAccountRepository cashbookAccountRepository;
	
	private final AuthConfigRepository authConfigRepository;

	private final CompanyRepository companyRepository;

	private final CashbookCategoryRepository cashbookCategoryRepository;

	private ObjectMapper objectMapper = new ObjectMapper();

	public CustomizationService(CustomizationRepository customizationRepository,
			CustomizationMapper customizationMapper, PersonGroupRepository personGroupRepository, 
			AuthConfigRepository authConfigRepository, CompanyRepository companyRepository, CashbookAccountRepository cashbookAccountRepository,
      CashbookCategoryRepository cashbookCategoryRepository) {
		this.customizationRepository = customizationRepository;
		this.customizationMapper = customizationMapper;
		this.personGroupRepository = personGroupRepository;
		this.authConfigRepository = authConfigRepository;
		this.companyRepository = companyRepository;
		this.cashbookAccountRepository = cashbookAccountRepository;
		this.cashbookCategoryRepository = cashbookCategoryRepository;
	}


  /**
	 * Save a customization.
	 *
	 * @param customizationDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CustomizationDTO save(CustomizationDTO customizationDTO) {
		log.debug("Request to save Customization : {}", customizationDTO);
		Customization customization = customizationMapper.toEntity(customizationDTO);
		
		validate(customization);
		
		if (customization.getId() == null) {
			// check if there's already one with this name
			Customization customizationExample = new Customization();
			customizationExample.setName(customization.getName());
			Example<Customization> example = Example.of(customizationExample);

			List<Customization> result = customizationRepository.findAll(example);
			if (result == null || result.isEmpty()) {
				customization = customizationRepository.save(customization);
			} else {
				throw new ServiceValidationException(ServiceErrorConstants.customizationNameAlreadyUsed, null);

			}
		} else {
			// update
			customizationRepository.save(customization);
		}

		return customizationMapper.toDto(customization);
	}
	

	/**
	 * Get all the customizations.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CustomizationDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Customizations");

		return customizationRepository.findAll(pageable).map(this::setText).map(customizationMapper::toDto);
	}

	/**
	 * Get one customization by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CustomizationDTO findOne(Long id) {
		log.debug("Request to get Customization : {}", id);
		Customization customization = customizationRepository.findById(id).orElse(null);
		if (customization != null) {
			setText(customization);
		}
		return customizationMapper.toDto(customization);
	}

	@Transactional
  public Customization getOne(CustomizationName customizationName) {
    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(customizationName);
    Customization c = customizationRepository.findOne(Example.of(exampleCustomization)).orElse(null);
    if (c != null) {
      setText(c);
    }
    return c;
  }


	/**
	 * Sets the text of the customization for the output
	 * 
	 * @param customization The concerning customization
	 * 
	 * @return The customization with added or adapted text
	 */
	private Customization setText(Customization customization) {
		// set text
		switch (customization.getName()) {
			case MEMBER_PERSONGROUP: 
				Long personGroupId = getLongFromCostumizationData(customization.getData());
				if (personGroupId != null) {
						PersonGroup pg = personGroupRepository.findById(personGroupId).orElse(null);
						if (pg != null) {
							customization.setText(pg.getName());
						} else {
              customization.setData("0");
            }
				}
				break;
      case ORGANIZATION_COMPANY:
        Long companyId = getLongFromCostumizationData(customization.getData());
        if (companyId != null) {
          Company company = companyRepository.findById(companyId).orElse(null);
          if (company != null) {
            customization.setText(company.getName());
          } else {
            customization.setData("0");
          }
        }
        break;
			case DEACTIVATED_FEATUREGROUPS:
				List<String> deactivatedFeatureGroups = getDeactivatedFeatureGroupsFromCustomizationData(customization.getData());
				customization.setText(deactivatedFeatureGroups.stream().collect(Collectors.joining(", ")));
				break;
			case HOURS_TO_INVALIDATE_VERIFICATION:
				Long hours = getLongFromCostumizationData(customization.getData());
				if (hours != null) {
					customization.setText(hours.toString());
				}
				break;
      case MONTHS_TO_DELETE_PERSON_DATA:
        Long months = getLongFromCostumizationData(customization.getData());
        if (months != null) {
          customization.setText(months.toString());
        }
        break;
      case DAYS_TO_DELETE_MAILLOG:
        Long hoursMailLog = getLongFromCostumizationData(customization.getData());
        if (hoursMailLog != null) {
          customization.setText(hoursMailLog.toString());
        }
        break;
			case AKM_DATA:
//				no text
				break;
      case DEFAULT_CASHBOOK_ACCOUNT:
        Long cashbookAccountId = getLongFromCostumizationData(customization.getData());
        if (cashbookAccountId != null) {
          CashbookAccount cashbookAccount = cashbookAccountRepository.findById(cashbookAccountId).orElse(null);
          if (cashbookAccount != null) {
            customization.setText(cashbookAccount.getName());
          } else {
            customization.setData("0");
          }
        }
        break;
      case DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER:
        Long cashbookCategoryId = getLongFromCostumizationData(customization.getData());
        if (cashbookCategoryId != null) {
          CashbookCategory cashbookCategory = cashbookCategoryRepository.findById(cashbookCategoryId).orElse(null);
          if (cashbookCategory != null) {
            customization.setText(cashbookCategory.getName());
          } else {
            customization.setData("0");
          }
        }
        break;
			default:
				break;
		}
		return customization;	
			
	}

	/**
	 * Gets the id of the member person group
	 * 	
	 * @return Long with the id or null, if it's not available or in a wrong format
	 */
	public Long getMemberPersonGroup() {
		String customizationData = getRawCustomizationString(CustomizationName.MEMBER_PERSONGROUP);
		return getLongFromCostumizationData(customizationData);		
	}
	
	/**
	 * Gets the hours for the invalidation of the verification link from the database
	 * 
	 * @return Long with the hours or null, if it's not available or in a wrong format
	 */
	public Long getHoursToInvalidateVerificationLink() {
		String customizationData = getRawCustomizationString(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);
		return getLongFromCostumizationData(customizationData);
	}

  /**
   * Gets the days for the deletion of the MailLog from the Database
   *
   * @return Long with the hours or null, if it's not available or in a wrong format
   */
  public Long getDaysToDeleteMailLog() {
    String customizationData = getRawCustomizationString(CustomizationName.DAYS_TO_DELETE_MAILLOG);
    return getLongFromCostumizationData(customizationData);
  }

	/**
	 * Gets the deactivated feature groups from the database
	 * 
	 * @return list of deactivated feature groups or null, if it's not available or in a wrong format
	 */
	public List<String> getDeactivatedFeatureGroups() {
		String customizationData = getRawCustomizationString(CustomizationName.DEACTIVATED_FEATUREGROUPS);
		return getDeactivatedFeatureGroupsFromCustomizationData(customizationData);
	}
	
	/**
	 * Gets the AKM Data from the database
	 * 
	 * @return AKM data or null, if it's not available or in a wrong format
	 */
	public Allgemeines getAKMData() {
		String customizationData = getRawCustomizationString(CustomizationName.AKM_DATA);
		return getAKMDataFromCustomizationData(customizationData);
	}

  /**
   * Gets the the default cashbook account
   *
   * @return the CashbookAccount or throws ServiceValidationException if not available
   */
  public CashbookAccount getDefaultCashbookAccount() {
    String customizationData = getRawCustomizationString(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);
    Long id = getLongFromCostumizationData(customizationData);
    CashbookAccount cashbookAccount;

    if (id == null) {
      throw new ServiceValidationException(ServiceErrorConstants.defaultCashbookAccountUndefined, null);
    } else {
      Optional<CashbookAccount> optional = this.cashbookAccountRepository.findById(id);

      if (!optional.isPresent()) {
        throw new ServiceValidationException(ServiceErrorConstants.defaultCashbookAccountUndefined, null);
      } else {
        cashbookAccount = optional.get();
      }
    }

    return cashbookAccount;
  }

  /**
   * Gets the the default cashbook category for Transfers
   *
   * @return the CashbookCategory
   */
  public CashbookCategory getDefaultCashbookCategoryForTransfers() {
    String customizationData = getRawCustomizationString(CustomizationName.DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER);
    Long id = getLongFromCostumizationData(customizationData);

    if (id == null) {
      return null;
    }

    return this.cashbookCategoryRepository.findById(id).orElse(null);
  }


	/**
	 * Gets the AKM data from the raw customization data
	 * 
	 * @param customizationData The customization Data with the AKM data
	 * @return The AKM Data or null, if it's not parseable
	 */
	private Allgemeines getAKMDataFromCustomizationData(String customizationData) {
		try {
			Allgemeines akmData = objectMapper.readValue(customizationData, Allgemeines.class);
			if (akmData != null) {
				return akmData;
			}
		} catch (IOException e) {
			log.error("Could not load AKM Data", e);
		}
		return null;
	}

	/**
	 * Gets a List of deactivated Feature groups from the raw customization data
	 * 
	 * @param customizationData The customization Data with the deactivated feature groups
	 * @return The list of strings or null, if it's not parseable
	 */
	private List<String> getDeactivatedFeatureGroupsFromCustomizationData(String customizationData) {
		try {
			List<String> deactivatedFeatureGroups = objectMapper.readValue(customizationData, new TypeReference<List<String>>() {});
			if (deactivatedFeatureGroups != null) {
				return deactivatedFeatureGroups;
			}
			return new ArrayList<>();
		} catch (IOException e) {
			log.error("Could not load deactivatedFeatureGroups", e);
		}
		return new ArrayList<>();
	}
	
	/**
	 * Gets the Long from the raw customization data
	 * 
	 * @param customizationData The customization Data with the Long
	 * @return The long value or null, if it's not parseable
	 */
	private Long getLongFromCostumizationData(String customizationData) {
		Long longValue;
		try {
			longValue = Long.parseLong(customizationData);
		} catch (NumberFormatException e) {
			log.error("Could not parse long from string", e);
			return null;
		}
		return longValue;
	}


	/**
	 * Returns the raw data stored for this customization
	 * 
	 * @param customizationName Name for customization
	 * 
	 * @return The raw data for the customization
	 */
	public String getRawCustomizationString(CustomizationName customizationName) {
		Customization c = getOne(customizationName);
		if (c == null || c.getData() == null) {
			return null;
		}
		return c.getData();
	}
	/**
	 * Delete the customization by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Customization : {}", id);
		customizationRepository.deleteById(id);
	}
	
	/**
	 * Validates the customization data and throws an Service Validation Exception
	 *  
	 * @param customization The customization to validate
	 */
	public void validate(Customization customization) {
		boolean valid = true;
		
		switch (customization.getName()) {
		case MEMBER_PERSONGROUP: 
			Long personGroupId = getLongFromCostumizationData(customization.getData());
			if (personGroupId == null || personGroupId.compareTo(new Long(0)) < 0 ) {
				valid = false;
			}
			break;
      case ORGANIZATION_COMPANY:
      Long companyId = getLongFromCostumizationData(customization.getData());
      if (companyId == null || companyId.compareTo(new Long(0)) < 0 ) {
        valid = false;
      }
      break;
		case DEACTIVATED_FEATUREGROUPS:
			List<String> deactivatedFeatureGroups = getDeactivatedFeatureGroupsFromCustomizationData(customization.getData());
			if (deactivatedFeatureGroups == null 
					|| deactivatedFeatureGroups.isEmpty()
					|| deactivatedFeatureGroups.contains("ADMIN") 
					|| deactivatedFeatureGroups.contains("TENANT_CONFIGURATION")) {
				valid = false;
			}		
			break;
		case HOURS_TO_INVALIDATE_VERIFICATION:
      Long hours = getLongFromCostumizationData(customization.getData());
      if (hours == null || hours.compareTo(new Long(1)) < 0 ) {
        valid = false;
      }
      break;
    case MONTHS_TO_DELETE_PERSON_DATA:
      Long months = getLongFromCostumizationData(customization.getData());
      if (months == null || months.compareTo(new Long(1)) < 0 ) {
        valid = false;
      }
      break;
      case DAYS_TO_DELETE_MAILLOG:
        Long hoursMailLog = getLongFromCostumizationData(customization.getData());
        if (hoursMailLog == null || hoursMailLog.compareTo(new Long(1)) < 0 ) {
          valid = false;
        }
        break;
		case AKM_DATA:
			Allgemeines akmData = getAKMDataFromCustomizationData(customization.getData());
			if (akmData == null) {
				valid = false;
			}
			break;
      case DEFAULT_CASHBOOK_ACCOUNT:
      Long cashbookAccountId = getLongFromCostumizationData(customization.getData());
      if (cashbookAccountId == null || cashbookAccountId.compareTo(new Long(0)) < 0 ) {
        valid = false;
      }
      break;
      case DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER:
        Long cashbookCategoryId = getLongFromCostumizationData(customization.getData());
        if (cashbookCategoryId == null || cashbookCategoryId.compareTo(new Long(0)) < 0 ) {
          valid = false;
        }
        break;
		default:
			valid = false;
			break;
		}
			
		if (!valid) {
			throw new ServiceValidationException(ServiceErrorConstants.wrongCustomizationData, new String[]{});
		}
				
	}
	
	/**
	 * Get all possible feature groups
	 * 
	 * @return A set with all possible feature groups as strings
	 */
	public Set<String> getAllFeatureGroups() {
		return authConfigRepository.getAllFeatureGroups().stream().map(fg -> fg.getGroup()).collect(Collectors.toSet());
	}

  /**
   * Gets the company id for the organization which uses the application
   *
   * @return A Long with the id of the company corresponding to the organization using this application
   */
  public Long getOrganizationId() {
    String customizationData = getRawCustomizationString(CustomizationName.ORGANIZATION_COMPANY);
    return getLongFromCostumizationData(customizationData);
	}

  /**
   * Gets the amount of months after which the personal data has to be deleted
   *
   * @return A Long with the amount of data after which the personal data has to be deleted
   */
  public Long getMonthsToDeletePersonData() {
    String customizationData = getRawCustomizationString(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);
    return getLongFromCostumizationData(customizationData);
  }
}
