package at.simianarmy.service;

import at.simianarmy.domain.Composition;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.repository.OfficialAppointmentRepository;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.dto.OfficialAppointmentDTO;
import at.simianarmy.service.mapper.CompositionMapper;
import at.simianarmy.service.mapper.OfficialAppointmentMapper;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing OfficialAppointment.
 */
@Service
@Transactional
public class OfficialAppointmentService {

	private final Logger log = LoggerFactory.getLogger(OfficialAppointmentService.class);

	@Inject
	private OfficialAppointmentRepository officialAppointmentRepository;

	@Inject
	private CompositionRepository compositionRepository;

	@Inject
	private OfficialAppointmentMapper officialAppointmentMapper;

	@Inject
	private CompositionMapper compositionMapper;

	/**
	 * Save an officialAppointment.
	 *
	 * @param officialAppointmentDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public OfficialAppointmentDTO save(OfficialAppointmentDTO officialAppointmentDTO) {
		log.debug("Request to save OfficialAppointment : {}", officialAppointmentDTO);
		OfficialAppointment officialAppointment = officialAppointmentMapper
				.officialAppointmentDTOToOfficialAppointment(officialAppointmentDTO);
		officialAppointment = officialAppointmentRepository.save(officialAppointment);
		OfficialAppointmentDTO result = officialAppointmentMapper
				.officialAppointmentToOfficialAppointmentDTO(officialAppointment);
		return result;
	}

	/**
	 * Get all the officialAppointments.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<OfficialAppointmentDTO> findAll(Pageable pageable) {
		log.debug("Request to get all OfficialAppointments");
		Page<OfficialAppointment> result = officialAppointmentRepository.findAll(pageable);
		return result.map(officialAppointment -> officialAppointmentMapper
				.officialAppointmentToOfficialAppointmentDTO(officialAppointment));
	}

	/**
	 * get all the officialAppointments where Appointment is null.
	 *
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<OfficialAppointmentDTO> findAllWhereAppointmentIsNull() {
		log.debug("Request to get all officialAppointments where Appointment is null");
		return StreamSupport.stream(officialAppointmentRepository.findAll().spliterator(), false)
				.filter(officialAppointment -> officialAppointment.getAppointment() == null)
				.map(officialAppointmentMapper::officialAppointmentToOfficialAppointmentDTO)
				.collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Get one officialAppointment by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public OfficialAppointmentDTO findOne(Long id) {
		log.debug("Request to get OfficialAppointment : {}", id);
		OfficialAppointment officialAppointment = officialAppointmentRepository.findById(id).orElse(null);
		OfficialAppointmentDTO officialAppointmentDTO = officialAppointmentMapper
				.officialAppointmentToOfficialAppointmentDTO(officialAppointment);
		return officialAppointmentDTO;
	}

	/**
	 * Delete the officialAppointment by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete OfficialAppointment : {}", id);
		officialAppointmentRepository.deleteById(id);
	}

	/**
	 * Add an Composition to a OfficialAppointment.
	 *
	 * @param id
	 *            the id of the OfficialAppointment
	 * @param compositionDTO
	 *            the Composition to add
	 * @return the OfficialAppointment
	 */
	public OfficialAppointmentDTO addComposition(Long id, CompositionDTO compositionDTO) {
		log.debug("Request to add Composition to OfficialAppointment : {}", id, compositionDTO);
		OfficialAppointment officialAppointment = this.officialAppointmentRepository.findById(id).orElse(null);

		Composition composition = compositionMapper.compositionDTOToComposition(compositionDTO);

		if (officialAppointment != null && !officialAppointment.getCompositions().contains(composition)) {
			composition.addAppointments(officialAppointment);
			this.compositionRepository.saveAndFlush(composition);
		}

		OfficialAppointmentDTO officialAppointmentDTO = officialAppointmentMapper
				.officialAppointmentToOfficialAppointmentDTO(officialAppointment);
		return officialAppointmentDTO;
	}

	/**
	 * Remove the Composition from the OfficialAppointment.
	 *
	 * @param id
	 *            the id of the entity
	 * @param compositionId
	 *            the id of the composition to remove
	 */
	public void removeComposition(Long id, Long compositionId) {
		log.debug("Request to remove Composition from OfficialAppointment : {}", id, compositionId);

		OfficialAppointment officialAppointment = officialAppointmentRepository.findById(id).orElse(null);

		if (officialAppointment != null) {
			Composition composition = compositionRepository.findById(compositionId).orElse(null);
			if (composition != null) {
				composition.removeAppointments(officialAppointment);
				this.compositionRepository.saveAndFlush(composition);
			}
		}
	}
}
