package at.simianarmy.service;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.EmailVerificationLinkDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.EmailVerificationLinkMapper;
import at.simianarmy.service.mapper.PersonMapper;
import at.simianarmy.service.util.RandomUtil;
import java.time.ZonedDateTime;
import java.util.Optional;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmailVerificationLinkService {

  private final Logger log = LoggerFactory.getLogger(EmailVerificationLink.class);

  @Inject
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Inject
  private PersonRepository personRepository;

  @Inject
  private EmailVerificationLinkMapper emailVerificationLinkMapper;

  @Inject
  private PersonMapper personMapper;

  /**
   * Creates a new EmailVerificationLink for a Person
   *
   * @param personId
   * @return the created EmailVerificationLinkDTO
   */
  public EmailVerificationLinkDTO createEmailVerificationLinkForPerson(Long personId) {
    this.log.info("Creating EmailVerificationLink for Person : {}", personId);

    Person person = this.getPersonById(personId);

    if (this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)) {
      throw new ServiceValidationException(ServiceErrorConstants.personAlreadyHasActiveEmailVerificationLink, null);
    }

    EmailVerificationLink link = new EmailVerificationLink();
    link.setPerson(person);
    link.setCreationDate(ZonedDateTime.now());
    link.setValidationDate(null);
    link.setInvalid(false);
    link.setSecretKey(RandomUtil.generateSecretKey());

    this.emailVerificationLinkRepository.save(link);

    return this.emailVerificationLinkMapper.toDto(link);
  }

  /**
   * Returns the currently active EmailVerificationLink for a Person or null if none existent
   *
   * @param personId
   * @return the active EmailVerificationLink
   */
  public EmailVerificationLinkDTO getActiveEmailVerificationLinkForPerson(Long personId) {
    this.log.info("Get currently active EmailVerificationLink for Person : {}", personId);

    Person person = this.getPersonById(personId);

    EmailVerificationLink link = this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person);

    return this.emailVerificationLinkMapper.toDto(link);
  }

  /**
   * Returns the EmailVerificationLink for a Person or null if none existent
   *
   * @param personId
   * @return the EmailVerificationLink
   */
  public EmailVerificationLinkDTO getEmailVerificationLinkForPerson(Long personId) {
    this.log.info("Get EmailVerificationLink for Person : {}", personId);

    Person person = this.getPersonById(personId);

    EmailVerificationLink link = this.emailVerificationLinkRepository.findFirstByPersonOrderByCreationDateDesc(person);

    return this.emailVerificationLinkMapper.toDto(link);
  }

  /**
   * Invalidates the currently active EmailVerificationLink for a Person
   *
   * @param personId
   * @return the invalidated EmailVerificationLink
   */
  public EmailVerificationLinkDTO invalidateEmailVerificationLinkForPerson(Long personId) {
    this.log.info("Invalidating EmailVerificationLink for Person : {}", personId);

    Person person = this.getPersonById(personId);

    this.checkPersonHasActiveEmailVerificationLink(person);

    EmailVerificationLink link = this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person);
    link.setInvalid(true);
    this.emailVerificationLinkRepository.save(link);

    return this.emailVerificationLinkMapper.toDto(link);
  }

  /**
   * Verifies the currently active EmailVerificationLink for a Person and sets the verified Email
   *
   * @param personId
   * @param email
   * @param secretKey
   */
  public void verifyEmailVerificationLinkForPersonWithEmailAndSecretKey(Long personId, String email, String secretKey) {
    this.log.info("Verifying EmailVerificationLink for Person : {}, New Email : {}", personId, email);

    Person person = this.getPersonById(personId);

    this.checkPersonHasActiveEmailVerificationLink(person);

    EmailVerificationLink link = this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person);

    if (!link.getSecretKey().equals(secretKey)) {
      throw new ServiceValidationException(ServiceErrorConstants.emailVerificationSecretKeyNotValid, null);
    }

    person.setEmail(email);
    this.personRepository.save(person);

    link.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.save(link);
  }

  private void checkPersonHasActiveEmailVerificationLink(Person person) {
    if (!this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)) {
      throw new ServiceValidationException(ServiceErrorConstants.personHasNoActiveEmailVerificationLink, null);
    }
  }

  private Person getPersonById(Long personId) {
    Optional<Person> personOptional = this.personRepository.findById(personId);

    if (!personOptional.isPresent()) {
      throw new ServiceValidationException(ServiceErrorConstants.personNotFound, null);
    }

    return personOptional.get();
  }
}
