package at.simianarmy.service;

import at.simianarmy.domain.Company;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.Template;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.letter.SerialLetterFactory;
import at.simianarmy.letter.SerialLetterFactoryException;
import at.simianarmy.letter.service.SerialLetterService;
import at.simianarmy.letter.service.SerialLetterServiceFactory;
import at.simianarmy.letter.service.SerialLetterServiceFactoryException;
import at.simianarmy.letter.worker.SerialLetterWorker;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.LetterMapper;
import at.simianarmy.service.mapper.PersonMapper;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Letter.
 */
@Service
@Transactional
public class LetterService {

	private final Logger log = LoggerFactory.getLogger(LetterService.class);

	@Inject
	private LetterRepository letterRepository;

	@Inject
	private TemplateRepository templateRepository;

	@Inject
	private LetterMapper letterMapper;

	@Inject
	private SerialLetterServiceFactory serialLetterServiceFactory;

	@Inject
	private TaskExecutor taskExecutor;

	@Inject
  private SerialLetterFactory serialLetterFactory;

	@Inject
  private PersonMapper personMapper;

	@Inject
  private TenantIdentiferResolver tenantIdentiferResolver;
	/**
	 * Save a letter.
	 *
	 * @param letterDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public LetterDTO save(LetterDTO letterDTO) {
		log.debug("Request to save Letter : {}", letterDTO);
		Letter letter = letterMapper.letterDTOToLetter(letterDTO);

		// Overwrite the concerning letter if the given title exists already
		for (Letter l : letterRepository.findAll()) {
			if (letter.getTitle() == l.getTitle()) {
				letter.setId(l.getId());
			}
		}

		letter = letterRepository.save(letter);
		LetterDTO result = letterMapper.letterToLetterDTO(letter);
		return result;
	}

	/**
	 * Get all the letters.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<LetterDTO> findAll(LetterType letterType, Pageable pageable) {
		log.debug("Request to get all Letters with Type {}", letterType);

		Page<Letter> result = letterRepository.findByLetterType(letterType, pageable);
		return result.map(letter -> letterMapper.letterToLetterDTO(letter));
	}

	/**
	 * Get one letter by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public LetterDTO findOne(Long id) {
		log.debug("Request to get Letter : {}", id);
		Letter letter = letterRepository.findOneWithEagerRelationships(id);
		LetterDTO letterDTO = letterMapper.letterToLetterDTO(letter);
		return letterDTO;
	}

	/**
	 * Delete the letter by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Letter : {}", id);
		letterRepository.deleteById(id);
	}

	/**
	 * Generates a serial letter with the given LetterDTO.
	 *
	 * @param letterDTO
	 *            A letterDTO with the needed information for the serial Letter
	 * @return A Resource containing the serial letter in a PDF
	 */
	@Transactional
	public Resource generate(LetterDTO letterDTO) {
		Letter letter = letterMapper.letterDTOToLetter(letterDTO);

    SerialLetterService serialLetterService = this.prepareSerialLetterService(letter);

    List<Person> persons;

    if (letterDTO.getPersons() != null && !letterDTO.getPersons().isEmpty()) {
      if (letter.getPersonGroups() != null && !letter.getPersonGroups().isEmpty()) {
        throw new ServiceValidationException(ServiceErrorConstants.serialLetterPersonGroupsAndPersonsNotAllowed, null);
      }
      persons = personMapper.personDTOsToPeople(letterDTO.getPersons());
    } else {
      persons = serialLetterService.getRelevantPersons(letter);
    }

		return this.generateLetter(letter,
      persons,
      serialLetterService.getRelevantCompanies(letter),
      serialLetterService.getTemplatePage(letter));

	}

  private SerialLetterService prepareSerialLetterService(Letter letter) {
    try {
      return this.serialLetterServiceFactory
        .createForLetter(letter);
    } catch (SerialLetterServiceFactoryException e) {
      throw new ServiceValidationException(e.getMessage(), null);
    }
  }

  private Resource generateLetter(Letter letter, List<Person> persons, List<Company> companies, Resource templatePage) {
    if (letter.getTemplate() != null && letter.getTemplate().getId() != null) {
      letter.setTemplate(templateRepository.findById(letter.getTemplate().getId()).orElse(null));
    }
    if (persons.isEmpty() && companies.isEmpty()) {
      throw new ServiceValidationException(ServiceErrorConstants.serialLetterNoPersonsSelected, null);
    }

    PipedInputStream in = new PipedInputStream();
    PipedOutputStream out = null;
    try {
      out = new PipedOutputStream(in);
    } catch (IOException exc) {
      log.error("Couldn't open PipedOutputStream");
      exc.printStackTrace();
    }
    if (out != null) {
      try {
        SerialLetterWorker worker = this.serialLetterFactory
          .createForLetter(letter);
        worker.setOut(out);
        worker.setPersons(persons);
        worker.setCompanies(companies);
        worker.setTemplatePageResource(templatePage);
        worker.setTenant(tenantIdentiferResolver.resolveCurrentTenantIdentifier());

        taskExecutor.execute(worker);
      } catch (SerialLetterFactoryException e) {
        throw new ServiceValidationException(e.getMessage(), null);
      }
    }

    return new InputStreamResource(in);
  }
}
