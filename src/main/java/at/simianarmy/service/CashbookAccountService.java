package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.dto.CashbookAccountDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing CashbookAccount.
 */
@Service
@Transactional
public class CashbookAccountService {

  public final static Integer START_CURRENT_NUMBER = 0;

	private final Logger log = LoggerFactory.getLogger(CashbookAccountService.class);

	private final CashbookAccountRepository cashbookAccountRepository;

	private final CashbookAccountMapper cashbookAccountMapper;

	public CashbookAccountService(CashbookAccountRepository cashbookAccountRepository,
			CashbookAccountMapper cashbookAccountMapper) {
		this.cashbookAccountRepository = cashbookAccountRepository;
		this.cashbookAccountMapper = cashbookAccountMapper;
	}

	/**
	 * Save a new cashbookAccount.
	 *
	 * @param cashbookAccountDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public CashbookAccountDTO save(CashbookAccountDTO cashbookAccountDTO) {
		log.debug("Request to save new CashbookAccount : {}", cashbookAccountDTO);
		CashbookAccount cashbookAccount = cashbookAccountMapper.toEntity(cashbookAccountDTO);

    this.checkTextForAutoTransferDistinct(cashbookAccount.getTextForAutoTransferFrom(), cashbookAccount.getTextForAutoTransferTo());

    this.checkTextForAutoTransferUnique(cashbookAccount.getTextForAutoTransferFrom());
    this.checkTextForAutoTransferUnique(cashbookAccount.getTextForAutoTransferTo());

		this.checkReceiptCode(cashbookAccount);

		cashbookAccount.setCurrentNumber(START_CURRENT_NUMBER);

    this.checkBankImportTypeIfBankAccountSet(cashbookAccount);

    if (cashbookAccountDTO.isDefaultForCashbook()) {
      this.checkDefaultForCashbook(cashbookAccount);
    }

		cashbookAccount = cashbookAccountRepository.save(cashbookAccount);
		return cashbookAccountMapper.toDto(cashbookAccount);
	}

  /**
   * Save an existing cashbookAccount.
   *
   * @param cashbookAccountDTO
   *            the entity to save
   * @return the persisted entity
   */
  public CashbookAccountDTO update(CashbookAccountDTO cashbookAccountDTO) {
    log.debug("Request to save existing CashbookAccount : {}", cashbookAccountDTO);
    CashbookAccount cashbookAccount = this.cashbookAccountRepository.findById(cashbookAccountDTO.getId()).get();

    if (!cashbookAccount.getReceiptCode().equals(cashbookAccountDTO.getReceiptCode())) {
      //Das ändern der Beleg-Codes bei bestehenden Kassabuch-Konten ist nicht erlaubt!
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountReceiptCodeUpdateNotAllowed, null);
    }

    this.checkTextForAutoTransferDistinct(cashbookAccountDTO.getTextForAutoTransferFrom(), cashbookAccountDTO.getTextForAutoTransferTo());

    if (cashbookAccount.getTextForAutoTransferFrom() != null && !cashbookAccount.getTextForAutoTransferFrom().equals(cashbookAccountDTO.getTextForAutoTransferFrom())) {
      this.checkTextForAutoTransferUnique(cashbookAccountDTO.getTextForAutoTransferFrom());
    }

    if (cashbookAccount.getTextForAutoTransferTo() != null && !cashbookAccount.getTextForAutoTransferTo().equals(cashbookAccountDTO.getTextForAutoTransferTo())) {
      this.checkTextForAutoTransferUnique(cashbookAccountDTO.getTextForAutoTransferTo());
    }

    cashbookAccount.setName(cashbookAccountDTO.getName());
    cashbookAccount.setBankAccount(cashbookAccountDTO.isBankAccount());
    cashbookAccount.setReceiptCode(cashbookAccountDTO.getReceiptCode());
    cashbookAccount.setBankImporterType(cashbookAccountDTO.getBankImporterType());
    cashbookAccount.setTextForAutoTransferFrom(cashbookAccountDTO.getTextForAutoTransferFrom());
    cashbookAccount.setTextForAutoTransferTo(cashbookAccountDTO.getTextForAutoTransferTo());
    cashbookAccount.setDefaultForCashbook(cashbookAccountDTO.isDefaultForCashbook());

    this.checkBankImportTypeIfBankAccountSet(cashbookAccount);

    if (cashbookAccount.isDefaultForCashbook()) {
      this.checkDefaultForCashbook(cashbookAccount);
    }

    cashbookAccount = cashbookAccountRepository.save(cashbookAccount);
    return cashbookAccountMapper.toDto(cashbookAccount);
  }

	/**
	 * Get all the cashbookAccounts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<CashbookAccountDTO> findAll(Boolean bankAccount, Pageable pageable) {
		log.debug("Request to get all CashbookAccounts");

    if (bankAccount != null) {
      return cashbookAccountRepository.findByBankAccount(bankAccount, pageable).map(cashbookAccountMapper::toDto);
    }

		return cashbookAccountRepository.findAll(pageable).map(cashbookAccountMapper::toDto);
	}

	/**
	 * Get one cashbookAccount by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public CashbookAccountDTO findOne(Long id) {
		log.debug("Request to get CashbookAccount : {}", id);
		CashbookAccount cashbookAccount = cashbookAccountRepository.findById(id).orElse(null);
		return cashbookAccountMapper.toDto(cashbookAccount);
	}

	/**
	 * Delete the cashbookAccount by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete CashbookAccount : {}", id);
		cashbookAccountRepository.deleteById(id);
	}

	private void checkReceiptCode(CashbookAccount cashbookAccount) {
    //Receipt-Code darf nur aus Buchstaben bestehen!
    if (!cashbookAccount.getReceiptCode().matches("[A-Za-z]{1,}")) {
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountReceiptCodeOnlyLettersAllowed, null);
    }

	  if (this.cashbookAccountRepository.findOneByReceiptCode(cashbookAccount.getReceiptCode()).isPresent()) {
	    throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountReceiptCodeUnique, null);
    }
  }

  private void checkBankImportTypeIfBankAccountSet(CashbookAccount cashbookAccount) {
	  if ((cashbookAccount.isBankAccount() && cashbookAccount.getBankImporterType() == null) ||
      (!cashbookAccount.isBankAccount() && cashbookAccount.getBankImporterType() != null)) {
	    throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountBankAccountWithoutImporterType, null);
    }
  }

  private void checkTextForAutoTransferDistinct(String textFrom, String textTo) {
	  if (textFrom != null && textTo != null && !textFrom.equals("") && !textTo.equals("") && textFrom.equals(textTo)) {
      throw new ServiceValidationException(ServiceErrorConstants.cashbookAccountAutoTransferTextFromAndToEqual, null);
    }
  }

  private void checkTextForAutoTransferUnique(String text) {
	  if (text != null && !text.equals("")) {
      if (this.cashbookAccountRepository.existsByTextForAutoTransferFrom(text) ||
        this.cashbookAccountRepository.existsByTextForAutoTransferTo(text)) {
        throw new ServiceValidationException(
          ServiceErrorConstants.cashbookAccountAutoTransferTextUnique, null);
      }
    }
  }

  private void checkDefaultForCashbook(CashbookAccount cashbookAccount) {
    CashbookAccount defaultForCashbook = this.cashbookAccountRepository.findFirstByDefaultForCashbookIsTrue();

    if (defaultForCashbook != null && cashbookAccount.getId() != null && defaultForCashbook.getId() != cashbookAccount.getId()) {
      throw new ServiceValidationException(
        ServiceErrorConstants.cashbookAccountWithDefaultForCashbookUnique, null);
    } else if (defaultForCashbook != null && cashbookAccount.getId() == null) {
      throw new ServiceValidationException(
        ServiceErrorConstants.cashbookAccountWithDefaultForCashbookUnique, null);
    }
  }
}
