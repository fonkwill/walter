package at.simianarmy.service;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.domain.User;
import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.roles.repository.AuthorityRepository;
import at.simianarmy.roles.repository.RoleRepository;
import at.simianarmy.roles.service.dto.UserDTO;
import at.simianarmy.security.AuthoritiesConstants;
import at.simianarmy.security.SecurityConstants;
import at.simianarmy.security.SecurityUtils;
import at.simianarmy.service.util.RandomUtil;
import at.simianarmy.web.rest.vm.ManagedUserVM;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	@Inject
	private PasswordEncoder passwordEncoder;

	@Inject
	private UserRepository userRepository;

	@Inject
	private AuthorityRepository authorityRepository;
	
	@Inject
	private RoleRepository roleRepository;
	
	@Inject
	private AuthConfigRepository authConfigRepository;

	@Inject
	private UserColumnConfigService userColumnConfigService;
	
	@Inject
	private CustomizationService customizationService;

	@Inject
  private WalterProperties walterProperties;

	@Inject
  private TenantIdentiferResolver tenantIdentiferResolver;



	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			userRepository.save(user);
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key).filter(user -> {
			ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
			return user.getResetDate().isAfter(oneDayAgo.toInstant());
		}).map(user -> {
			user.setPassword(passwordEncoder.encode(newPassword));
			user.setResetKey(null);
			user.setResetDate(null);
			userRepository.save(user);
			return user;
		});
	}

	public Optional<User> requestPasswordReset(String mail) {
		return userRepository.findOneByEmail(mail).filter(User::getActivated).map(user -> {
			user.setResetKey(RandomUtil.generateResetKey());
			user.setResetDate(ZonedDateTime.now().toInstant());
			userRepository.save(user);
			return user;
		});
	}

	public User createUser(String login, String password, String firstName, String lastName, String email,
			String langKey) {

		User newUser = new User();
		String encryptedPassword = passwordEncoder.encode(password);
		newUser.setLogin(login);
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setEmail(email);
		newUser.setLangKey(langKey);
		// new user is not active
		newUser.setActivated(false);
		// new user gets registration key
		newUser.setActivationKey(RandomUtil.generateActivationKey());
		Role authority = roleRepository.findByName(AuthoritiesConstants.USER).orElse(null);
		Set<Role> authorities = new HashSet<>();
		authorities.add(authority);
		newUser.setRoles(authorities);
		userRepository.saveAndFlush(newUser);
		this.userColumnConfigService.generateColumnConfigsForUser(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public User createUser(ManagedUserVM managedUserVM) {
		User user = new User();
		user.setLogin(managedUserVM.getLogin());
		user.setFirstName(managedUserVM.getFirstName());
		user.setLastName(managedUserVM.getLastName());
		user.setEmail(managedUserVM.getEmail());
		if (managedUserVM.getLangKey() == null) {
			user.setLangKey("de"); // default language
		} else {
			user.setLangKey(managedUserVM.getLangKey());
		}
		if (managedUserVM.getRoles() != null) {
			Set<Role> authorities = new HashSet<>();
			managedUserVM.getRoles().stream()
					.forEach(role -> authorities.add(roleRepository.findByName(role).orElse(null)));
			user.setRoles(authorities);
		}
		String encryptedPassword = passwordEncoder.encode(SecurityConstants.initPassword);
		user.setPassword(encryptedPassword);
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(ZonedDateTime.now().toInstant());
		user.setActivated(true);
		userRepository.saveAndFlush(user);
		this.userColumnConfigService.generateColumnConfigsForUser(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	public void updateUser(String firstName, String lastName, String email, String langKey) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
			u.setFirstName(firstName);
			u.setLastName(lastName);
			u.setEmail(email);
			u.setLangKey(langKey);
			userRepository.save(u);
			log.debug("Changed Information for User: {}", u);
		});
	}

	public void updateUser(Long id, String login, String firstName, String lastName, String email, boolean activated,
			String langKey, Set<String> authorities) {

		userRepository.findOneById(id).ifPresent(u -> {
			u.setLogin(login);
			u.setFirstName(firstName);
			u.setLastName(lastName);
			u.setEmail(email);
			u.setActivated(activated);
			u.setLangKey(langKey);
			Set<Role> managedAuthorities = u.getRoles();
			managedAuthorities.clear();
			authorities.stream()
					.forEach(authority -> managedAuthorities.add(roleRepository.findByName(authority).orElse(null)));
			log.debug("Changed Information for User: {}", u);
		});
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(u -> {
		  this.userColumnConfigService.deleteColumnConfigsForUser(u);
			userRepository.delete(u);
			log.debug("Deleted User: {}", u);
		});
	}

	public void changePassword(String password) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
			String encryptedPassword = passwordEncoder.encode(password);
			u.setPassword(encryptedPassword);
			userRepository.save(u);
			log.debug("Changed password for User: {}", u);
		});
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneByLogin(login).map(u -> {
			u.getRoles().size();
			return u;
		});
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities(Long id) {
		User user = userRepository.findById(id).orElse(null);
		user.getRoles().size(); // eagerly load the association
		return user;
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities() {
		Optional<User> optionalUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
		User user = null;
		if (optionalUser.isPresent()) {
			user = optionalUser.orElse(null);
			user.getRoles().size(); // eagerly load the association
		}
		return user;
	}

	public UserDTO getUserWithElements() {
		User user = getUserWithAuthorities();
		UserDTO userDTO = new UserDTO(user);
		
		Set<String> displayElements = new HashSet<>();
		Set<String> writeElements = new HashSet<>();
		
		List<String> deactivatedFeatureGroups = customizationService.getDeactivatedFeatureGroups();
		
		List<String> deactivatedFeatures = authConfigRepository.getAllAuthoritiesForFeatureGroups(deactivatedFeatureGroups);

    if(!walterProperties.getMultitenancy().isEnabled() || !tenantIdentiferResolver.resolveCurrentTenantIdentifier().equals(TenantIdentiferResolver.MASTER_TENANT)) {
      deactivatedFeatures.add("TENANT_CONFIGURATION");
    }

		user.getRoles().stream().forEach(
				role -> role.getAuthorities().stream().
						filter(authority -> !deactivatedFeatures.contains(authority.getName())).
						forEach(
						auth -> {
							displayElements.addAll(authConfigRepository.getElementsForAuthority(auth.getName()));
							if (auth.getWrite()) {
								writeElements.addAll(authConfigRepository.getElementsForAuthority(auth.getName()));
							}
						}
					));
		userDTO.setDisplayElements(displayElements);
		userDTO.setWriteElements(writeElements);
		return userDTO;
	}
}
