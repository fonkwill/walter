package at.simianarmy.service.mapper;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentService;
import at.simianarmy.domain.Receipt;
import at.simianarmy.service.dto.CashbookEntryDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CashbookEntry and its DTO CashbookEntryDTO.
 */
@Mapper(componentModel = "spring", uses = { CompositionMapper.class, })
public interface CashbookEntryMapper {

	@Mapping(source = "appointment.id", target = "appointmentId")
	@Mapping(source = "appointment.name", target = "appointmentName")
	@Mapping(source = "appointment.beginDate", target = "appointmentBeginDate")
	@Mapping(source = "cashbookCategory.id", target = "cashbookCategoryId")
	@Mapping(source = "cashbookCategory.name", target = "cashbookCategoryName")
	@Mapping(source = "cashbookCategory.color", target = "cashbookCategoryColor")
	@Mapping(source = "membershipFeeForPeriod.id", target = "membershipFeeForPeriodId")
	@Mapping(source = "membershipFeeForPeriod.nr", target = "membershipFeeForPeriodNr")
	@Mapping(source = "membershipFeeForPeriod.year", target = "membershipFeeForPeriodYear")
	@Mapping(source = "membershipFeeForPeriod.referenceCode", target = "membershipFeeForPeriodReferenceCode")
	@Mapping(source = "membershipFeeForPeriod.membership.person.id", target = "membershipFeeForPeriodPersonId")
	@Mapping(source = "instrumentService.id", target = "instrumentServiceId")
	@Mapping(source = "instrumentService.instrument.id", target = "instrumentServiceInstrumentId")
	@Mapping(source = "instrumentService.instrument.name", target = "instrumentServiceInstrumentName")
	@Mapping(source = "instrument.id", target = "instrumentId")
	@Mapping(source = "instrument.name", target = "instrumentName")
	@Mapping(source = "receipt.id", target = "receiptId")
	@Mapping(source = "receipt.identifier", target = "receiptIdentifier")
  @Mapping(source = "receipt.file", target = "receiptFile")
	@Mapping(source = "compostions", target = "compositions")
	@Mapping(source = "cashbookAccount.id", target = "cashbookAccountId")
	@Mapping(source = "cashbookAccount.name", target = "cashbookAccountName")
	CashbookEntryDTO cashbookEntryToCashbookEntryDTO(CashbookEntry cashbookEntry);

	List<CashbookEntryDTO> cashbookEntriesToCashbookEntryDTOs(List<CashbookEntry> cashbookEntries);

	@Mapping(target = "compostions", ignore = true)
	@Mapping(target = "membershipFeeForPeriod", ignore = true)
	@Mapping(source = "instrumentId", target = "instrument")
	@Mapping(source = "instrumentServiceId", target = "instrumentService")
	@Mapping(source = "appointmentId", target = "appointment")
	@Mapping(source = "cashbookCategoryId", target = "cashbookCategory")
	@Mapping(source = "receiptId", target = "receipt")
	@Mapping(source = "cashbookAccountId", target = "cashbookAccount")
  @Mapping(target = "bankImportData", ignore = true)
	CashbookEntry cashbookEntryDTOToCashbookEntry(CashbookEntryDTO cashbookEntryDTO);

	List<CashbookEntry> cashbookEntryDTOsToCashbookEntries(List<CashbookEntryDTO> cashbookEntryDTOs);

	default Appointment appointmentFromId(Long id) {
		if (id == null) {
			return null;
		}
		Appointment appointment = new Appointment();
		appointment.setId(id);
		return appointment;
	}

	default CashbookAccount cashbookAccountFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookAccount cashbookAccount = new CashbookAccount();
		cashbookAccount.setId(id);
		return cashbookAccount;
	}

	default InstrumentService instrumentServiceFromId(Long id) {
		if (id == null) {
			return null;
		}
		InstrumentService instrumentService = new InstrumentService();
		instrumentService.setId(id);
		return instrumentService;
	}

	default Instrument instrumentFromId(Long id) {
		if (id == null) {
			return null;
		}
		Instrument instrument = new Instrument();
		instrument.setId(id);
		return instrument;
	}

	default CashbookCategory cashbookCategoryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookCategory cashbookCategory = new CashbookCategory();
		cashbookCategory.setId(id);
		return cashbookCategory;
	}

	default Receipt receiptFromId(Long id) {
		if (id == null) {
			return null;
		}
		Receipt receipt = new Receipt();
		receipt.setId(id);
		return receipt;
	}
}
