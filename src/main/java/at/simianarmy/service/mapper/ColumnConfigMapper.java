package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.ColumnConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ColumnConfig and its DTO ColumnConfigDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ColumnConfigMapper extends EntityMapper<ColumnConfigDTO, ColumnConfig> {



    default ColumnConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        ColumnConfig columnConfig = new ColumnConfig();
        columnConfig.setId(id);
        return columnConfig;
    }
}
