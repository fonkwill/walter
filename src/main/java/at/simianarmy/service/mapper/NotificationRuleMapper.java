package at.simianarmy.service.mapper;

import at.simianarmy.domain.NotificationRule;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.service.mapper.AuthorityMapper;
import at.simianarmy.service.dto.NotificationRuleDTO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mapstruct.Mapper;

/**
 * Mapper for the entity NotificationRule and its DTO NotificationRuleDTO.
 */
@Mapper(componentModel = "spring", uses = {AuthorityMapper.class})
public interface NotificationRuleMapper {

	NotificationRuleDTO notificationRuleToNotificationRuleDTO(NotificationRule notificationRule);

	List<NotificationRuleDTO> notificationRulesToNotificationRuleDTOs(List<NotificationRule> notificationRules);

	NotificationRule notificationRuleDTOToNotificationRule(NotificationRuleDTO notificationRuleDTO);

	List<NotificationRule> notificationRuleDTOsToNotificationRules(List<NotificationRuleDTO> notificationRuleDTOs);

	
}
