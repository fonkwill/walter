package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.MembershipDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Membership and its DTO MembershipDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MembershipMapper {

	@Mapping(source = "person.id", target = "personId")
	@Mapping(source = "membershipFee.id", target = "membershipFeeId")
	@Mapping(source = "membershipFee.description", target = "membershipFeeDescription")
	MembershipDTO membershipToMembershipDTO(Membership membership);

	List<MembershipDTO> membershipsToMembershipDTOs(List<Membership> memberships);

	@Mapping(target = "membershipFeeForPeriods", ignore = true)
	@Mapping(source = "personId", target = "person")
	@Mapping(source = "membershipFeeId", target = "membershipFee")
	Membership membershipDTOToMembership(MembershipDTO membershipDTO);

	List<Membership> membershipDTOsToMemberships(List<MembershipDTO> membershipDTOs);

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}

	default MembershipFee membershipFeeFromId(Long id) {
		if (id == null) {
			return null;
		}
		MembershipFee membershipFee = new MembershipFee();
		membershipFee.setId(id);
		return membershipFee;
	}
}
