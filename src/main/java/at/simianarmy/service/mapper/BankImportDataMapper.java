package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.BankImportDataDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity BankImportData and its DTO BankImportDataDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BankImportDataMapper {

	@Mapping(source = "cashbookEntry.id", target = "cashbookEntryId")
	@Mapping(source = "cashbookEntry.title", target = "cashbookEntryTitle")
  @Mapping(source = "cashbookCategory.id", target = "cashbookCategoryId")
  @Mapping(source = "cashbookCategory.name", target = "cashbookCategoryName")
  @Mapping(source = "cashbookAccount.id", target = "cashbookAccountId")
  @Mapping(source = "cashbookAccountFrom.id", target = "cashbookAccountFromId")
  @Mapping(source = "cashbookAccountFrom.name", target = "cashbookAccountFromName")
  @Mapping(source = "cashbookAccountTo.id", target = "cashbookAccountToId")
  @Mapping(source = "cashbookAccountTo.name", target = "cashbookAccountToName")
  BankImportDataDTO bankImportDataToBankImportDataDTO(BankImportData bankImportData);

	List<BankImportDataDTO> bankImportDataToBankImportDataDTOs(List<BankImportData> bankImportData);

	@Mapping(source = "cashbookEntryId", target = "cashbookEntry")
  @Mapping(source = "cashbookCategoryId", target = "cashbookCategory")
  @Mapping(source = "cashbookAccountId", target = "cashbookAccount")
  @Mapping(source = "cashbookAccountFromId", target = "cashbookAccountFrom")
  @Mapping(source = "cashbookAccountToId", target = "cashbookAccountTo")
  BankImportData bankImportDataDTOToBankImportData(BankImportDataDTO bankImportDataDTO);

	List<BankImportData> bankImportDataDTOsToBankImportData(List<BankImportDataDTO> bankImportDataDTOs);

	default CashbookEntry cashbookEntryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setId(id);
		return cashbookEntry;
	}

	default CashbookCategory cashbookCategoryFromId(Long id) {
	  if (id == null) {
	    return null;
    }
    CashbookCategory cashbookCategory = new CashbookCategory();
	  cashbookCategory.setId(id);
	  return cashbookCategory;
  }

  default CashbookAccount cashbookAccountFromId(Long id) {
    if (id == null) {
      return null;
    }
    CashbookAccount cashbookAccount = new CashbookAccount();
    cashbookAccount.setId(id);
    return cashbookAccount;
  }
}
