package at.simianarmy.service.mapper;

import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.service.dto.CashbookCategoryDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity CashbookCategory and its DTO CashbookCategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CashbookCategoryMapper {

	@Mapping(source = "parent.id", target = "parentId")
	@Mapping(source = "parent.name", target = "parentName")
	CashbookCategoryDTO cashbookCategoryToCashbookCategoryDTO(CashbookCategory cashbookCategory);

	List<CashbookCategoryDTO> cashbookCategoriesToCashbookCategoryDTOs(List<CashbookCategory> cashbookCategories);

	@Mapping(target = "children", ignore = true)
	@Mapping(target = "cashbookEntries", ignore = true)
	@Mapping(source = "parentId", target = "parent")
	CashbookCategory cashbookCategoryDTOToCashbookCategory(CashbookCategoryDTO cashbookCategoryDTO);

	List<CashbookCategory> cashbookCategoryDTOsToCashbookCategories(List<CashbookCategoryDTO> cashbookCategoryDTOs);

	default CashbookCategory cashbookCategoryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookCategory cashbookCategory = new CashbookCategory();
		cashbookCategory.setId(id);
		return cashbookCategory;
	}
}
