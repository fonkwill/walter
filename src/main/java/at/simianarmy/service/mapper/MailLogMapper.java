package at.simianarmy.service.mapper;

import at.simianarmy.domain.MailLog;
import at.simianarmy.service.dto.MailLogDTO;
import java.util.List;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity MailLog and its DTO MailLogDTO
 */
@Mapper(componentModel = "spring")
public interface MailLogMapper {

	MailLogDTO mailLogToMailLogDTO(MailLog mailLog);

	MailLog mailLogDTOToMailLog(MailLogDTO mailLogDTO);

	List<MailLogDTO> mailLogsToMailLogDTOs(List<MailLog> mailLogs);

	List<MailLog> mailLogDTOsToMailLogs(List<MailLogDTO> mailLogDTOs);
}
