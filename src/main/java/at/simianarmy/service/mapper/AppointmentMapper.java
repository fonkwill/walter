package at.simianarmy.service.mapper;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.AppointmentType;
import at.simianarmy.domain.Person;
import at.simianarmy.service.dto.AppointmentDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Appointment and its DTO AppointmentDTO.
 */
@Mapper(componentModel = "spring", uses = { PersonMapper.class, CompositionMapper.class })
public interface AppointmentMapper {

	@Mapping(source = "officialAppointment.id", target = "officialAppointmentId")
	@Mapping(source = "officialAppointment.organizerName", target = "organizerName")
	@Mapping(source = "officialAppointment.organizerAddress", target = "organizerAddress")
	@Mapping(source = "officialAppointment.isHeadQuota", target = "isHeadQuota", defaultValue = "false")
	@Mapping(source = "type.id", target = "typeId")
	@Mapping(source = "type.name", target = "typeName")
	@Mapping(source = "type.personGroup.id", target = "personGroupId")
	@Mapping(source = "type.isOfficial", target = "isOfficial", defaultValue = "false")
	@Mapping(source = "officialAppointment.compositions", target = "compositions")
	AppointmentDTO appointmentToAppointmentDTO(Appointment appointment);

	List<AppointmentDTO> appointmentsToAppointmentDTOs(List<Appointment> appointments);

	@Mapping(source = "officialAppointmentId", target = "officialAppointment")
	@Mapping(target = "cashbookEntries", ignore = true)
	@Mapping(source = "typeId", target = "type")
	Appointment appointmentDTOToAppointment(AppointmentDTO appointmentDTO);

	List<Appointment> appointmentDTOsToAppointments(List<AppointmentDTO> appointmentDTOs);

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}

	default AppointmentType appointmentTypeFromId(Long id) {
		if (id == null) {
			return null;
		}
		AppointmentType appointmentType = new AppointmentType();
		appointmentType.setId(id);
		return appointmentType;
	}
}
