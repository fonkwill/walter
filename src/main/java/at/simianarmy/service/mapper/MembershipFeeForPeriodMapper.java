package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity MembershipFeeForPeriod and its DTO
 * MembershipFeeForPeriodDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MembershipFeeForPeriodMapper {

	@Mapping(source = "cashbookEntry.id", target = "cashbookEntryId")
	@Mapping(source = "membership.id", target = "membershipId")
	MembershipFeeForPeriodDTO membershipFeeForPeriodToMembershipFeeForPeriodDTO(
			MembershipFeeForPeriod membershipFeeForPeriod);

	List<MembershipFeeForPeriodDTO> membershipFeeForPeriodsToMembershipFeeForPeriodDTOs(
			List<MembershipFeeForPeriod> membershipFeeForPeriods);

	@Mapping(source = "cashbookEntryId", target = "cashbookEntry")
	@Mapping(source = "membershipId", target = "membership")
	MembershipFeeForPeriod membershipFeeForPeriodDTOToMembershipFeeForPeriod(
			MembershipFeeForPeriodDTO membershipFeeForPeriodDTO);

	List<MembershipFeeForPeriod> membershipFeeForPeriodDTOsToMembershipFeeForPeriods(
			List<MembershipFeeForPeriodDTO> membershipFeeForPeriodDTOs);

	default CashbookEntry cashbookEntryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setId(id);
		return cashbookEntry;
	}

	default Membership membershipFromId(Long id) {
		if (id == null) {
			return null;
		}
		Membership membership = new Membership();
		membership.setId(id);
		return membership;
	}
}
