package at.simianarmy.service.mapper;

import at.simianarmy.domain.Honor;
import at.simianarmy.service.dto.HonorDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Honor and its DTO HonorDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HonorMapper {

	HonorDTO honorToHonorDTO(Honor honor);

	List<HonorDTO> honorsToHonorDTOs(List<Honor> honors);

	@Mapping(target = "personHonors", ignore = true)
	Honor honorDTOToHonor(HonorDTO honorDTO);

	List<Honor> honorDTOsToHonors(List<HonorDTO> honorDTOs);
}
