package at.simianarmy.service.mapper;

import at.simianarmy.domain.MusicBook;
import at.simianarmy.service.dto.MusicBookDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity MusicBook and its DTO MusicBookDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MusicBookMapper {

	MusicBookDTO musicBookToMusicBookDTO(MusicBook musicBook);

	List<MusicBookDTO> musicBooksToMusicBookDTOs(List<MusicBook> musicBooks);

	@Mapping(target = "compositions", ignore = true)
	MusicBook musicBookDTOToMusicBook(MusicBookDTO musicBookDTO);

	List<MusicBook> musicBookDTOsToMusicBooks(List<MusicBookDTO> musicBookDTOs);
}
