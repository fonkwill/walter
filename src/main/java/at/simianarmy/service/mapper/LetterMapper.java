package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.LetterDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Letter and its DTO LetterDTO.
 */
@Mapper(componentModel = "spring", uses = { PersonGroupMapper.class, })
public interface LetterMapper {

	@Mapping(source = "template.id", target = "templateId")
	@Mapping(source = "template.title", target = "templateTitle")
  @Mapping(target = "persons", ignore = true)
	LetterDTO letterToLetterDTO(Letter letter);

	List<LetterDTO> lettersToLetterDTOs(List<Letter> letters);

	@Mapping(source = "templateId", target = "template")
  @Mapping(target = "subject", ignore = true)
  @Mapping(target = "mailText", ignore = true)
  @Mapping(target = "attachmentFileName", ignore = true)
	Letter letterDTOToLetter(LetterDTO letterDTO);

	List<Letter> letterDTOsToLetters(List<LetterDTO> letterDTOs);

	default Template templateFromId(Long id) {
		if (id == null) {
			return null;
		}
		Template template = new Template();
		template.setId(id);
		return template;
	}

	default PersonGroup personGroupFromId(Long id) {
		if (id == null) {
			return null;
		}
		PersonGroup personGroup = new PersonGroup();
		personGroup.setId(id);
		return personGroup;
	}
}
