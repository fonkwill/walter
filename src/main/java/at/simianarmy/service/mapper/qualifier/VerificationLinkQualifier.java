package at.simianarmy.service.mapper.qualifier;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import javax.inject.Inject;
import javax.inject.Named;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Component;

@Component
public class VerificationLinkQualifier {

  @Inject
  private ServerProperties serverProperties;

  @Inject
  private WalterProperties walterProperties;

  @Inject
  private TenantIdentiferResolver tenantIdentiferResolver;

  @Named("verificationLinkForPersonId")
  public String verificationLinkForPersonId(Long personId) {

    String host = walterProperties.getEmailVerificationHost();
    Integer port = serverProperties.getPort();

    String protocol = "http";

    if (port == 443) {
      protocol = "https";
    } else {
      protocol = "http";
    }

    String tenant = null;

    System.out.println(walterProperties.getMultitenancy().isEnabled());

    if (walterProperties.getMultitenancy().isEnabled()) {
      tenant = tenantIdentiferResolver.resolveCurrentTenantIdentifier();
    }

    String emailVerificationUrl = walterProperties.getEmailVerificationUrl();

    String url = "";
    url += protocol + "://" + host + "/" + emailVerificationUrl + "?id=" + personId;

    if (tenant != null) {
      url += "&tenant=" + tenant;
    }

    return url;
  }

}
