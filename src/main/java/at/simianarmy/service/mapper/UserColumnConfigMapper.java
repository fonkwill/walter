package at.simianarmy.service.mapper;

import at.simianarmy.domain.ColumnConfig;
import at.simianarmy.domain.User;
import at.simianarmy.domain.UserColumnConfig;
import at.simianarmy.service.dto.UserColumnConfigDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Composition and its DTO CompositionDTO.
 */
@Mapper(componentModel = "spring", uses = { ColumnConfigMapper.class, UserMapper.class })
public interface UserColumnConfigMapper extends EntityMapper<UserColumnConfigDTO, UserColumnConfig> {

  @Mapping(target = "entity", source = "columnConfig.entity")
  @Mapping(target = "columnName", source = "columnConfig.columnName")
  @Mapping(target = "columnConfigId", source = "columnConfig.id")
  @Mapping(target = "userId", source = "user.id")
  @Mapping(target = "columnDisplayName", source = "columnConfig.columnDisplayName")
  @Mapping(target = "sortByName", source = "columnConfig.sortByName")
  UserColumnConfigDTO toDto(UserColumnConfig userColumnConfig);

  @Mapping(target = "columnConfig", source = "columnConfigId")
  @Mapping(target = "user", source = "userId")
  UserColumnConfig toEntity(UserColumnConfigDTO userColumnConfigDTO);

  default UserColumnConfig fromId(Long id) {
    if (id == null) {
      return null;
    }
    UserColumnConfig userColumnConfig = new UserColumnConfig();
    userColumnConfig.setId(id);
    return userColumnConfig;
  }

}
