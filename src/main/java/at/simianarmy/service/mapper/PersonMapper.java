package at.simianarmy.service.mapper;

import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.service.dto.PersonDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Person and its DTO PersonDTO.
 */
@Mapper(componentModel = "spring", uses = { PersonGroupMapper.class })
public interface PersonMapper {

	@Mapping(ignore = true, target = "openMembershipAmount")
	PersonDTO personToPersonDTO(Person person);

	List<PersonDTO> peopleToPersonDTOs(List<Person> people);

	@Mapping(target = "personClothings", ignore = true)
	@Mapping(target = "personInstruments", ignore = true)
	@Mapping(target = "personHonors", ignore = true)
	@Mapping(target = "personAwards", ignore = true)
	@Mapping(target = "memberships", ignore = true)
	@Mapping(target = "appointments", ignore = true)
  @Mapping(target = "emailVerificationLinks", ignore = true)
	Person personDTOToPerson(PersonDTO personDTO);

	List<Person> personDTOsToPeople(List<PersonDTO> personDTOs);

	default PersonGroup personGroupFromId(Long id) {
		if (id == null) {
			return null;
		}
		PersonGroup personGroup = new PersonGroup();
		personGroup.setId(id);
		return personGroup;
	}
}
