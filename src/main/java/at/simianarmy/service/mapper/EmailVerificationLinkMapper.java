package at.simianarmy.service.mapper;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import at.simianarmy.service.dto.EmailVerificationLinkDTO;
import at.simianarmy.service.mapper.qualifier.VerificationLinkQualifier;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity EmailVerificationLink and its DTO EmailVerificationLinkDTO.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, VerificationLinkQualifier.class})
public interface EmailVerificationLinkMapper extends EntityMapper<EmailVerificationLinkDTO, EmailVerificationLink> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.id", target = "link", qualifiedByName = "verificationLinkForPersonId")
    EmailVerificationLinkDTO toDto(EmailVerificationLink emailVerificationLink);

    @Mapping(source = "personId", target = "person")
    EmailVerificationLink toEntity(EmailVerificationLinkDTO emailVerificationLinkDTO);

    default EmailVerificationLink fromId(Long id) {
        if (id == null) {
            return null;
        }
        EmailVerificationLink emailVerificationLink = new EmailVerificationLink();
        emailVerificationLink.setId(id);
        return emailVerificationLink;
    }

    default Person personFromId(Long id) {
      if (id == null) {
        return null;
      }
      Person person = new Person();
      person.setId(id);
      return person;
    }
}
