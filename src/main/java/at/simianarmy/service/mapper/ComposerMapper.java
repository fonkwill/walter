package at.simianarmy.service.mapper;

import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.service.dto.ComposerDTO;

import java.util.List;

import org.mapstruct.Mapper;

/**
 * Mapper for the entity Composer and its DTO ComposerDTO.
 */
@Mapper(componentModel = "spring", uses = { CompositionMapper.class, })
public interface ComposerMapper {

	ComposerDTO composerToComposerDTO(Composer composer);

	List<ComposerDTO> composersToComposerDTOs(List<Composer> composers);

	Composer composerDTOToComposer(ComposerDTO composerDTO);

	List<Composer> composerDTOsToComposers(List<ComposerDTO> composerDTOs);

	default Composition compositionFromId(Long id) {
		if (id == null) {
			return null;
		}
		Composition composition = new Composition();
		composition.setId(id);
		return composition;
	}
}
