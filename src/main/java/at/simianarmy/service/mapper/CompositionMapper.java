package at.simianarmy.service.mapper;

import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.Genre;
import at.simianarmy.domain.MusicBook;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.service.dto.CompositionDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Composition and its DTO CompositionDTO.
 */
@Mapper(componentModel = "spring", uses = { OfficialAppointmentMapper.class, })
public interface CompositionMapper {

	@Mapping(source = "cashbookEntry.id", target = "cashbookEntryId")
	@Mapping(source = "cashbookEntry.amount", target = "cashbookEntryAmount")
	@Mapping(source = "orderingCompany.id", target = "orderingCompanyId")
	@Mapping(source = "orderingCompany.name", target = "orderingCompanyName")
	@Mapping(source = "publisher.id", target = "publisherId")
	@Mapping(source = "publisher.name", target = "publisherName")
	@Mapping(source = "genre.id", target = "genreId")
	@Mapping(source = "genre.name", target = "genreName")
	@Mapping(source = "musicBook.id", target = "musicBookId")
	@Mapping(source = "musicBook.name", target = "musicBookName")
	@Mapping(source = "colorCode.id", target = "colorCodeId")
	@Mapping(source = "colorCode.name", target = "colorCodeName")
	CompositionDTO compositionToCompositionDTO(Composition composition);

	List<CompositionDTO> compositionsToCompositionDTOs(List<Composition> compositions);

	@Mapping(source = "cashbookEntryId", target = "cashbookEntry")
	@Mapping(source = "orderingCompanyId", target = "orderingCompany")
	@Mapping(source = "publisherId", target = "publisher")
	@Mapping(source = "genreId", target = "genre")
	@Mapping(source = "musicBookId", target = "musicBook")
	@Mapping(target = "arrangers", ignore = true)
	@Mapping(target = "composers", ignore = true)
	@Mapping(source = "colorCodeId", target = "colorCode")
	@Mapping(target = "oldColorCode", ignore = true)
	@Mapping(target = "oldNumber", ignore = true)
	Composition compositionDTOToComposition(CompositionDTO compositionDTO);

	List<Composition> compositionDTOsToCompositions(List<CompositionDTO> compositionDTOs);

	default OfficialAppointment officialAppointmentFromId(Long id) {
		if (id == null) {
			return null;
		}
		OfficialAppointment officialAppointment = new OfficialAppointment();
		officialAppointment.setId(id);
		return officialAppointment;
	}

	default CashbookEntry cashbookEntryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setId(id);
		return cashbookEntry;
	}

	default Company companyFromId(Long id) {
		if (id == null) {
			return null;
		}
		Company company = new Company();
		company.setId(id);
		return company;
	}

	default Genre genreFromId(Long id) {
		if (id == null) {
			return null;
		}
		Genre genre = new Genre();
		genre.setId(id);
		return genre;
	}

	default MusicBook musicBookFromId(Long id) {
		if (id == null) {
			return null;
		}
		MusicBook musicBook = new MusicBook();
		musicBook.setId(id);
		return musicBook;
	}

	default ColorCode colorCodeFromId(Long id) {
		if (id == null) {
			return null;
		}
		ColorCode colorCode = new ColorCode();
		colorCode.setId(id);
		return colorCode;
	}
}
