package at.simianarmy.service.mapper;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.service.dto.MembershipFeeDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity MembershipFee and its DTO MembershipFeeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MembershipFeeMapper {

	MembershipFeeDTO membershipFeeToMembershipFeeDTO(MembershipFee membershipFee);

	List<MembershipFeeDTO> membershipFeesToMembershipFeeDTOs(List<MembershipFee> membershipFees);

	@Mapping(target = "membershipFeeAmounts", ignore = true)
	@Mapping(target = "memberships", ignore = true)
	MembershipFee membershipFeeDTOToMembershipFee(MembershipFeeDTO membershipFeeDTO);

	List<MembershipFee> membershipFeeDTOsToMembershipFees(List<MembershipFeeDTO> membershipFeeDTOs);
}
