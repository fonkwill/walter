package at.simianarmy.service.mapper;

import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.domain.Report;
import at.simianarmy.service.dto.OfficialAppointmentDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity OfficialAppointment and its DTO OfficialAppointmentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OfficialAppointmentMapper {

	@Mapping(source = "report.id", target = "reportId")
	OfficialAppointmentDTO officialAppointmentToOfficialAppointmentDTO(OfficialAppointment officialAppointment);

	List<OfficialAppointmentDTO> officialAppointmentsToOfficialAppointmentDTOs(
			List<OfficialAppointment> officialAppointments);

	@Mapping(target = "appointment", ignore = true)
	@Mapping(source = "reportId", target = "report")
	@Mapping(target = "compositions", ignore = true)
	OfficialAppointment officialAppointmentDTOToOfficialAppointment(OfficialAppointmentDTO officialAppointmentDTO);

	List<OfficialAppointment> officialAppointmentDTOsToOfficialAppointments(
			List<OfficialAppointmentDTO> officialAppointmentDTOs);

	default Report reportFromId(Long id) {
		if (id == null) {
			return null;
		}
		Report report = new Report();
		report.setId(id);
		return report;
	}
}
