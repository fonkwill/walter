package at.simianarmy.service.mapper;

import at.simianarmy.domain.Genre;
import at.simianarmy.service.dto.GenreDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Genre and its DTO GenreDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GenreMapper {

	GenreDTO genreToGenreDTO(Genre genre);

	List<GenreDTO> genresToGenreDTOs(List<Genre> genres);

	@Mapping(target = "compositions", ignore = true)
	Genre genreDTOToGenre(GenreDTO genreDTO);

	List<Genre> genreDTOsToGenres(List<GenreDTO> genreDTOs);
}
