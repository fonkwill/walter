package at.simianarmy.service.mapper;

import at.simianarmy.domain.ColorCode;
import at.simianarmy.service.dto.ColorCodeDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ColorCode and its DTO ColorCodeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ColorCodeMapper {

	ColorCodeDTO colorCodeToColorCodeDTO(ColorCode colorCode);

	List<ColorCodeDTO> colorCodesToColorCodeDTOs(List<ColorCode> colorCodes);

	@Mapping(target = "compositions", ignore = true)
	ColorCode colorCodeDTOToColorCode(ColorCodeDTO colorCodeDTO);

	List<ColorCode> colorCodeDTOsToColorCodes(List<ColorCodeDTO> colorCodeDTOs);
}
