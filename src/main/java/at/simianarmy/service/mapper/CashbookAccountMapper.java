package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.CashbookAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CashbookAccount and its DTO CashbookAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CashbookAccountMapper extends EntityMapper<CashbookAccountDTO, CashbookAccount> {

    @Mapping(target = "currentNumber", ignore = true)
    @Mapping(target = "cashbookEntries", ignore = true)
    CashbookAccount toEntity(CashbookAccountDTO cashbookAccountDTO);

    default CashbookAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        CashbookAccount cashbookAccount = new CashbookAccount();
        cashbookAccount.setId(id);
        return cashbookAccount;
    }
}
