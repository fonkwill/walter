package at.simianarmy.service.mapper;

import at.simianarmy.domain.Company;
import at.simianarmy.domain.Receipt;
import at.simianarmy.service.dto.ReceiptDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Receipt and its DTO ReceiptDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReceiptMapper {

	@Mapping(source = "company.id", target = "companyId")
	@Mapping(source = "company.name", target = "companyName")
	@Mapping(expression = "java( receipt.getEntries().size() )", target = "entriesCount")
	@Mapping(ignore = true, target = "uploadedFile")
	@Mapping(ignore = true, target = "cashbookEntryId")
	ReceiptDTO receiptToReceiptDTO(Receipt receipt);

	List<ReceiptDTO> receiptsToReceiptDTOs(List<Receipt> receipts);

	@Mapping(target = "entries", ignore = true)
	@Mapping(source = "companyId", target = "company")
  @Mapping(ignore = true, target = "initialCashbookEntry")
	Receipt receiptDTOToReceipt(ReceiptDTO receiptDTO);

	List<Receipt> receiptDTOsToReceipts(List<ReceiptDTO> receiptDTOs);

	default Company companyFromId(Long id) {
		if (id == null) {
			return null;
		}
		Company company = new Company();
		company.setId(id);
		return company;
	}
}
