package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.ClothingDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Clothing and its DTO ClothingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClothingMapper {

	@Mapping(source = "type.id", target = "typeId")
	@Mapping(source = "type.name", target = "typeName")
	ClothingDTO clothingToClothingDTO(Clothing clothing);

	List<ClothingDTO> clothingToClothingDTOs(List<Clothing> clothing);

	@Mapping(target = "personClothings", ignore = true)
	@Mapping(source = "typeId", target = "type")
	Clothing clothingDTOToClothing(ClothingDTO clothingDTO);

	List<Clothing> clothingDTOsToClothing(List<ClothingDTO> clothingDTOs);

	default ClothingType clothingTypeFromId(Long id) {
		if (id == null) {
			return null;
		}
		ClothingType clothingType = new ClothingType();
		clothingType.setId(id);
		return clothingType;
	}
}
