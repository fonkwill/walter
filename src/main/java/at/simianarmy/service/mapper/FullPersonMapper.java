package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.*;
import at.simianarmy.service.util.BeanUtil;
import com.sun.jersey.core.impl.provider.entity.XMLRootObjectProvider;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.Member;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Person and its DTO FullPersonDTO, with all information from a person
 */
@Mapper(componentModel = "spring", uses= {PersonGroupMapper.class,
  PersonInstrumentMapper.class,
  PersonClothingMapper.class,
  PersonAwardMapper.class,
  PersonHonorMapper.class,
  AppointmentMapper.class,
  MembershipMapper.class,
  CashbookEntryMapper.class
})
public interface FullPersonMapper {

  MembershipFeeForPeriodMapper membershipFeeForPeriodMapper = Mappers.getMapper(MembershipFeeForPeriodMapper.class);

  CashbookEntryMapper cashbookEntryMapper = Mappers.getMapper(CashbookEntryMapper.class);

  BankImportDataMapper bankImportDataMapper = Mappers.getMapper(BankImportDataMapper.class);

  @Mapping(target = "membershipFeeForPeriods", source = "memberships", qualifiedByName = "membershipFeeForPeriods")
	@Mapping(target = "cashbookEntries", source = "memberships", qualifiedByName = "cashbookEntries")
  @Mapping(target = "bankImportData", source = "memberships", qualifiedByName = "bankImportData")
  @Mapping(ignore = true, target = "openMembershipAmount")
  @Mapping(ignore = true, target = "createdAt")
  @Mapping(ignore = true, target = "organizationName")
  FullPersonDTO personToFullPersonDTO(Person person);

  @Named("membershipFeeForPeriods")
  default Set<MembershipFeeForPeriodDTO> membershipsToMembershipFeeForPeriods(Set<Membership> memberships) {
    Set<MembershipFeeForPeriodDTO> result = new HashSet<>();

    memberships.forEach(m ->
      result.addAll(
        membershipFeeForPeriodMapper.membershipFeeForPeriodsToMembershipFeeForPeriodDTOs(
          new ArrayList<>(m.getMembershipFeeForPeriods()))
      )
    );
    return result;
  }

  @Named("cashbookEntries")
  default Set<CashbookEntryDTO> membershipsToCashbookEntries(Set<Membership> memberships) {
    Set<CashbookEntryDTO> result = new HashSet<>();

    memberships.forEach(m -> result.addAll(
        m.getMembershipFeeForPeriods().stream().map(
          mfp -> cashbookEntryMapper.cashbookEntryToCashbookEntryDTO(mfp.getCashbookEntry())
        ).filter(Objects::nonNull).collect(Collectors.toSet())
      )
    );
    return result;
  }

  @Named("bankImportData")
  default Set<BankImportDataDTO> membershipsToBankImportData(Set<Membership> memberships) {
    Set<BankImportDataDTO> result = new HashSet<>();

    memberships.forEach(m ->
      result.addAll(
      m.getMembershipFeeForPeriods().stream().
        map(mfp -> mfp.getCashbookEntry()).
        filter(Objects::nonNull).map(cbe ->
          bankImportDataMapper.bankImportDataToBankImportDataDTO(cbe.getBankImportData()))
        .collect(Collectors.toSet())
      )
    );
   return result;
  }

}
