package at.simianarmy.service.mapper;

import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentService;
import at.simianarmy.service.dto.InstrumentServiceDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity InstrumentService and its DTO InstrumentServiceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InstrumentServiceMapper {

	@Mapping(source = "cashbookEntry.id", target = "cashbookEntryId")
	@Mapping(source = "instrument.id", target = "instrumentId")
	@Mapping(source = "cashbookEntry.amount", target = "cashbookEntryAmount")
	InstrumentServiceDTO instrumentServiceToInstrumentServiceDTO(InstrumentService instrumentService);

	List<InstrumentServiceDTO> instrumentServicesToInstrumentServiceDTOs(List<InstrumentService> instrumentServices);

	@Mapping(source = "cashbookEntryId", target = "cashbookEntry")
	@Mapping(source = "instrumentId", target = "instrument")
	InstrumentService instrumentServiceDTOToInstrumentService(InstrumentServiceDTO instrumentServiceDTO);

	List<InstrumentService> instrumentServiceDTOsToInstrumentServices(List<InstrumentServiceDTO> instrumentServiceDTOs);

	default CashbookEntry cashbookEntryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setId(id);
		return cashbookEntry;
	}

	default Instrument instrumentFromId(Long id) {
		if (id == null) {
			return null;
		}
		Instrument instrument = new Instrument();
		instrument.setId(id);
		return instrument;
	}
}
