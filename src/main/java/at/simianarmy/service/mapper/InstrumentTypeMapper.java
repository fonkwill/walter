package at.simianarmy.service.mapper;

import at.simianarmy.domain.InstrumentType;
import at.simianarmy.service.dto.InstrumentTypeDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity InstrumentType and its DTO InstrumentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InstrumentTypeMapper {

	InstrumentTypeDTO instrumentTypeToInstrumentTypeDTO(InstrumentType instrumentType);

	List<InstrumentTypeDTO> instrumentTypesToInstrumentTypeDTOs(List<InstrumentType> instrumentTypes);

	@Mapping(target = "instruments", ignore = true)
	InstrumentType instrumentTypeDTOToInstrumentType(InstrumentTypeDTO instrumentTypeDTO);

	List<InstrumentType> instrumentTypeDTOsToInstrumentTypes(List<InstrumentTypeDTO> instrumentTypeDTOs);
}
