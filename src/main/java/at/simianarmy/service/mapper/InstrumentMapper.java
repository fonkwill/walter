package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.InstrumentDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Instrument and its DTO InstrumentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InstrumentMapper {

	@Mapping(source = "type.id", target = "typeId")
	@Mapping(source = "type.name", target = "typeName")
	@Mapping(source = "cashbookEntry.id", target = "cashbookEntryId")
	@Mapping(source = "cashbookEntry.amount", target = "cashbookEntryAmount")
	InstrumentDTO instrumentToInstrumentDTO(Instrument instrument);

	List<InstrumentDTO> instrumentsToInstrumentDTOs(List<Instrument> instruments);

	@Mapping(target = "instrumentServices", ignore = true)
	@Mapping(target = "personInstruments", ignore = true)
	@Mapping(source = "typeId", target = "type")
	@Mapping(source = "cashbookEntryId", target = "cashbookEntry")
	Instrument instrumentDTOToInstrument(InstrumentDTO instrumentDTO);

	List<Instrument> instrumentDTOsToInstruments(List<InstrumentDTO> instrumentDTOs);

	default InstrumentType instrumentTypeFromId(Long id) {
		if (id == null) {
			return null;
		}
		InstrumentType instrumentType = new InstrumentType();
		instrumentType.setId(id);
		return instrumentType;
	}

	default CashbookEntry cashbookEntryFromId(Long id) {
		if (id == null) {
			return null;
		}
		CashbookEntry cashbookEntry = new CashbookEntry();
		cashbookEntry.setId(id);
		return cashbookEntry;
	}
}
