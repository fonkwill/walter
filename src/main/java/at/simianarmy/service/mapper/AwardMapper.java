package at.simianarmy.service.mapper;

import at.simianarmy.domain.Award;
import at.simianarmy.service.dto.AwardDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Award and its DTO AwardDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AwardMapper {

	AwardDTO awardToAwardDTO(Award award);

	List<AwardDTO> awardsToAwardDTOs(List<Award> awards);

	@Mapping(target = "personAwards", ignore = true)
	Award awardDTOToAward(AwardDTO awardDTO);

	List<Award> awardDTOsToAwards(List<AwardDTO> awardDTOs);
}
