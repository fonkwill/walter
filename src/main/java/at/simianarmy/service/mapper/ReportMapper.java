package at.simianarmy.service.mapper;

import at.simianarmy.domain.Report;
import at.simianarmy.service.dto.ReportDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Report and its DTO ReportDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReportMapper {

	ReportDTO reportToReportDTO(Report report);

	List<ReportDTO> reportsToReportDTOs(List<Report> reports);

	@Mapping(target = "appointments", ignore = true)
	Report reportDTOToReport(ReportDTO reportDTO);

	List<Report> reportDTOsToReports(List<ReportDTO> reportDTOs);
}
