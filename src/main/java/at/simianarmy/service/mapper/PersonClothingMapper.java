package at.simianarmy.service.mapper;

import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonClothing;
import at.simianarmy.service.dto.PersonClothingDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PersonClothing and its DTO PersonClothingDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonClothingMapper {

	@Mapping(source = "clothing.id", target = "clothingId")
	@Mapping(source = "person.id", target = "personId")
	@Mapping(source = "clothing.size", target = "clothingSize")
	@Mapping(source = "clothing.type.name", target = "clothingTypeName")
	@Mapping(source = "clothing.type.id", target = "clothingTypeId")
	PersonClothingDTO personClothingToPersonClothingDTO(PersonClothing personClothing);

	List<PersonClothingDTO> personClothingsToPersonClothingDTOs(List<PersonClothing> personClothings);

	@Mapping(source = "clothingId", target = "clothing")
	@Mapping(source = "personId", target = "person")
	PersonClothing personClothingDTOToPersonClothing(PersonClothingDTO personClothingDTO);

	List<PersonClothing> personClothingDTOsToPersonClothings(List<PersonClothingDTO> personClothingDTOs);

	default Clothing clothingFromId(Long id) {
		if (id == null) {
			return null;
		}
		Clothing clothing = new Clothing();
		clothing.setId(id);
		return clothing;
	}

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}
}
