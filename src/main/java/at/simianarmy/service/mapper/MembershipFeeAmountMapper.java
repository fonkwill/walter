package at.simianarmy.service.mapper;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity MembershipFeeAmount and its DTO MembershipFeeAmountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MembershipFeeAmountMapper {

	@Mapping(source = "membershipFee.id", target = "membershipFeeId")
	@Mapping(source = "membershipFee.description", target = "membershipFeeDescription")
	MembershipFeeAmountDTO membershipFeeAmountToMembershipFeeAmountDTO(MembershipFeeAmount membershipFeeAmount);

	List<MembershipFeeAmountDTO> membershipFeeAmountsToMembershipFeeAmountDTOs(
			List<MembershipFeeAmount> membershipFeeAmounts);

	@Mapping(source = "membershipFeeId", target = "membershipFee")
	MembershipFeeAmount membershipFeeAmountDTOToMembershipFeeAmount(MembershipFeeAmountDTO membershipFeeAmountDTO);

	List<MembershipFeeAmount> membershipFeeAmountDTOsToMembershipFeeAmounts(
			List<MembershipFeeAmountDTO> membershipFeeAmountDTOs);

	default MembershipFee membershipFeeFromId(Long id) {
		if (id == null) {
			return null;
		}
		MembershipFee membershipFee = new MembershipFee();
		membershipFee.setId(id);
		return membershipFee;
	}
}
