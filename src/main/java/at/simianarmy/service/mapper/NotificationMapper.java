package at.simianarmy.service.mapper;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.service.dto.NotificationDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Notification and its DTO NotificationDTO.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, })
public interface NotificationMapper {

	@Mapping(source = "readByUser.id", target = "readByUserId")
	@Mapping(source = "readByUser.firstName", target = "readByUserFirstName")
	@Mapping(source = "readByUser.lastName", target = "readByUserLastName")
	@Mapping(source = "notificationRule.id", target = "notificationRuleId")
	@Mapping(source = "notificationRule.entity", target = "notificationRuleEntity")
	@Mapping(source = "notificationRule.messageGroup", target = "notificationRuleMessageGroup")
	@Mapping(source = "notificationRule.messageSingle", target = "notificationRuleMessageSingle")
	NotificationDTO notificationToNotificationDTO(Notification notification);

	List<NotificationDTO> notificationsToNotificationDTOs(List<Notification> notifications);

	@Mapping(source = "readByUserId", target = "readByUser")
	@Mapping(source = "notificationRuleId", target = "notificationRule")
	Notification notificationDTOToNotification(NotificationDTO notificationDTO);

	List<Notification> notificationDTOsToNotifications(List<NotificationDTO> notificationDTOs);

	default NotificationRule notificationRuleFromId(Long id) {
		if (id == null) {
			return null;
		}
		NotificationRule notificationRule = new NotificationRule();
		notificationRule.setId(id);
		return notificationRule;
	}
}
