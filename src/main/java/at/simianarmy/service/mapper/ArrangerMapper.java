package at.simianarmy.service.mapper;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.Composition;
import at.simianarmy.service.dto.ArrangerDTO;

import java.util.List;

import org.mapstruct.Mapper;

/**
 * Mapper for the entity Arranger and its DTO ArrangerDTO.
 */
@Mapper(componentModel = "spring", uses = { CompositionMapper.class, })
public interface ArrangerMapper {

	ArrangerDTO arrangerToArrangerDTO(Arranger arranger);

	List<ArrangerDTO> arrangersToArrangerDTOs(List<Arranger> arrangers);

	Arranger arrangerDTOToArranger(ArrangerDTO arrangerDTO);

	List<Arranger> arrangerDTOsToArrangers(List<ArrangerDTO> arrangerDTOs);

	default Composition compositionFromId(Long id) {
		if (id == null) {
			return null;
		}
		Composition composition = new Composition();
		composition.setId(id);
		return composition;
	}
}
