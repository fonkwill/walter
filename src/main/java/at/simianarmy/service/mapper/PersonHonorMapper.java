package at.simianarmy.service.mapper;

import at.simianarmy.domain.Honor;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonHonor;
import at.simianarmy.service.dto.PersonHonorDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PersonHonor and its DTO PersonHonorDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonHonorMapper {

	@Mapping(source = "honor.id", target = "honorId")
	@Mapping(source = "person.id", target = "personId")
	@Mapping(source = "honor.name", target = "honorName")
	PersonHonorDTO personHonorToPersonHonorDTO(PersonHonor personHonor);

	List<PersonHonorDTO> personHonorsToPersonHonorDTOs(List<PersonHonor> personHonors);

	@Mapping(source = "honorId", target = "honor")
	@Mapping(source = "personId", target = "person")
	PersonHonor personHonorDTOToPersonHonor(PersonHonorDTO personHonorDTO);

	List<PersonHonor> personHonorDTOsToPersonHonors(List<PersonHonorDTO> personHonorDTOs);

	default Honor honorFromId(Long id) {
		if (id == null) {
			return null;
		}
		Honor honor = new Honor();
		honor.setId(id);
		return honor;
	}

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}
}
