package at.simianarmy.service.mapper;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.Template;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.dto.MailLetterDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Letter and its DTO LetterDTO.
 */
@Mapper(componentModel = "spring", uses = { PersonGroupMapper.class, })
public interface MailLetterMapper {

  @Mapping(target = "saveInLetter", ignore = true)
  @Mapping(target = "persons", ignore = true)
  @Mapping(source = "template.id", target = "templateId")
  @Mapping(source = "template.title", target = "templateTitle")
	MailLetterDTO letterToMailLetterDTO(Letter letter);

	List<MailLetterDTO> lettersToMailLetterDTOs(List<Letter> letters);

  @Mapping(source = "templateId", target = "template")
	Letter mailLetterDTOToLetter(MailLetterDTO mailLetterDTO);

	List<Letter> mailLetterDTOsToLetters(List<MailLetterDTO> mailLetterDTOs);

  default Template templateFromId(Long id) {
    if (id == null) {
      return null;
    }
    Template template = new Template();
    template.setId(id);
    return template;
  }

  default PersonGroup personGroupFromId(Long id) {
    if (id == null) {
      return null;
    }
    PersonGroup personGroup = new PersonGroup();
    personGroup.setId(id);
    return personGroup;
  }
}
