package at.simianarmy.service.mapper;

import at.simianarmy.domain.Company;
import at.simianarmy.service.dto.CompanyDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Company and its DTO CompanyDTO.
 */
@Mapper(componentModel = "spring", uses = { PersonGroupMapper.class })
public interface CompanyMapper {

	CompanyDTO companyToCompanyDTO(Company company);

	List<CompanyDTO> companiesToCompanyDTOs(List<Company> companies);

	@Mapping(target = "orderedCompositions", ignore = true)
	@Mapping(target = "publishedCompositions", ignore = true)
	@Mapping(target = "receipts", ignore = true)
	Company companyDTOToCompany(CompanyDTO companyDTO);

	List<Company> companyDTOsToCompanies(List<CompanyDTO> companyDTOs);
}
