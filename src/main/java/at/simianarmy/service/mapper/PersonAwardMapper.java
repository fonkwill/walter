package at.simianarmy.service.mapper;

import at.simianarmy.domain.Award;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonAward;
import at.simianarmy.service.dto.PersonAwardDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PersonAward and its DTO PersonAwardDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonAwardMapper {

	@Mapping(source = "award.id", target = "awardId")
	@Mapping(source = "person.id", target = "personId")
	@Mapping(source = "award.name", target = "awardName")
	PersonAwardDTO personAwardToPersonAwardDTO(PersonAward personAward);

	List<PersonAwardDTO> personAwardsToPersonAwardDTOs(List<PersonAward> personAwards);

	@Mapping(source = "awardId", target = "award")
	@Mapping(source = "personId", target = "person")
	PersonAward personAwardDTOToPersonAward(PersonAwardDTO personAwardDTO);

	List<PersonAward> personAwardDTOsToPersonAwards(List<PersonAwardDTO> personAwardDTOs);

	default Award awardFromId(Long id) {
		if (id == null) {
			return null;
		}
		Award award = new Award();
		award.setId(id);
		return award;
	}

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}
}
