package at.simianarmy.service.mapper;

import at.simianarmy.domain.User;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.service.dto.UserDTO;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity User and its DTO UserDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserMapper {

	@Mapping(target = "writeElements", ignore=true)
	@Mapping(target = "displayElements", ignore=true)
	UserDTO userToUserDTO(User user);

	List<UserDTO> usersToUserDTOs(List<User> users);

	@Mapping(target = "createdBy", ignore = true)
	@Mapping(target = "createdDate", ignore = true)
	@Mapping(target = "lastModifiedBy", ignore = true)
	@Mapping(target = "lastModifiedDate", ignore = true)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "activationKey", ignore = true)
	@Mapping(target = "resetKey", ignore = true)
	@Mapping(target = "resetDate", ignore = true)
	@Mapping(target = "password", ignore = true)
	User userDTOToUser(UserDTO userDTO);

	List<User> userDTOsToUsers(List<UserDTO> userDTOs);

	default User userFromId(Long id) {
		if (id == null) {
			return null;
		}
		User user = new User();
		user.setId(id);
		return user;
	}

	default Set<String> stringsFromRoles(Set<Role> roles) {
		return roles.stream().map(Role::getName).collect(Collectors.toSet());
	}

	default Set<Role> rolesFromStrings(Set<String> strings) {
		return strings.stream().map(string -> {
			Role auth = new Role();
			auth.setName(string);
			return auth;
		}).collect(Collectors.toSet());
	}
}
