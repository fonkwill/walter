package at.simianarmy.service.mapper;

import at.simianarmy.domain.ClothingType;
import at.simianarmy.service.dto.ClothingTypeDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity ClothingType and its DTO ClothingTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClothingTypeMapper {

	ClothingTypeDTO clothingTypeToClothingTypeDTO(ClothingType clothingType);

	List<ClothingTypeDTO> clothingTypesToClothingTypeDTOs(List<ClothingType> clothingTypes);

	@Mapping(target = "clothings", ignore = true)
	ClothingType clothingTypeDTOToClothingType(ClothingTypeDTO clothingTypeDTO);

	List<ClothingType> clothingTypeDTOsToClothingTypes(List<ClothingTypeDTO> clothingTypeDTOs);
}
