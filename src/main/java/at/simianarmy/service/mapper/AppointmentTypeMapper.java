package at.simianarmy.service.mapper;

import at.simianarmy.domain.AppointmentType;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.service.dto.AppointmentTypeDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity AppointmentType and its DTO AppointmentTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AppointmentTypeMapper {

	@Mapping(source = "personGroup.id", target = "personGroupId")
	@Mapping(source = "personGroup.name", target = "personGroupName")
	AppointmentTypeDTO appointmentTypeToAppointmentTypeDTO(AppointmentType appointmentType);

	List<AppointmentTypeDTO> appointmentTypesToAppointmentTypeDTOs(List<AppointmentType> appointmentTypes);

	@Mapping(source = "personGroupId", target = "personGroup")
	@Mapping(target = "appointments", ignore = true)
	AppointmentType appointmentTypeDTOToAppointmentType(AppointmentTypeDTO appointmentTypeDTO);

	List<AppointmentType> appointmentTypeDTOsToAppointmentTypes(List<AppointmentTypeDTO> appointmentTypeDTOs);

	default PersonGroup personGroupFromId(Long id) {
		if (id == null) {
			return null;
		}
		PersonGroup personGroup = new PersonGroup();
		personGroup.setId(id);
		return personGroup;
	}
}
