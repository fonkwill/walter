package at.simianarmy.service.mapper;

import at.simianarmy.domain.*;
import at.simianarmy.service.dto.TemplateDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Template and its DTO TemplateDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TemplateMapper {

	@Mapping(ignore = true, target = "uploadedFile")
	TemplateDTO templateToTemplateDTO(Template template);

	List<TemplateDTO> templatesToTemplateDTOs(List<Template> templates);

	Template templateDTOToTemplate(TemplateDTO templateDTO);

	List<Template> templateDTOsToTemplates(List<TemplateDTO> templateDTOs);
}
