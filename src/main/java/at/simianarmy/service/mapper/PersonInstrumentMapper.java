package at.simianarmy.service.mapper;

import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.service.dto.PersonInstrumentDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PersonInstrument and its DTO PersonInstrumentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonInstrumentMapper {

	@Mapping(source = "instrument.id", target = "instrumentId")
	@Mapping(source = "person.id", target = "personId")
	@Mapping(source = "person.firstName", target = "firstName")
	@Mapping(source = "person.lastName", target = "lastName")
	@Mapping(source = "instrument.name", target = "instrumentName")
	PersonInstrumentDTO personInstrumentToPersonInstrumentDTO(PersonInstrument personInstrument);

	List<PersonInstrumentDTO> personInstrumentsToPersonInstrumentDTOs(List<PersonInstrument> personInstruments);

	@Mapping(source = "instrumentId", target = "instrument")
	@Mapping(source = "personId", target = "person")
	PersonInstrument personInstrumentDTOToPersonInstrument(PersonInstrumentDTO personInstrumentDTO);

	List<PersonInstrument> personInstrumentDTOsToPersonInstruments(List<PersonInstrumentDTO> personInstrumentDTOs);

	default Instrument instrumentFromId(Long id) {
		if (id == null) {
			return null;
		}
		Instrument instrument = new Instrument();
		instrument.setId(id);
		return instrument;
	}

	default Person personFromId(Long id) {
		if (id == null) {
			return null;
		}
		Person person = new Person();
		person.setId(id);
		return person;
	}
}
