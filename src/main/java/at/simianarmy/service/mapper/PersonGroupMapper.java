package at.simianarmy.service.mapper;

import at.simianarmy.domain.PersonGroup;
import at.simianarmy.service.dto.PersonGroupDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PersonGroup and its DTO PersonGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PersonGroupMapper {

	@Mapping(source = "parent.id", target = "parentId")
	@Mapping(source = "parent.name", target = "parentName")
	PersonGroupDTO personGroupToPersonGroupDTO(PersonGroup personGroup);

	List<PersonGroupDTO> personGroupsToPersonGroupDTOs(List<PersonGroup> personGroups);

	@Mapping(target = "children", ignore = true)
	@Mapping(source = "parentId", target = "parent")
	@Mapping(target = "persons", ignore = true)
	@Mapping(target = "companies", ignore = true)
	PersonGroup personGroupDTOToPersonGroup(PersonGroupDTO personGroupDTO);

	List<PersonGroup> personGroupDTOsToPersonGroups(List<PersonGroupDTO> personGroupDTOs);

	default PersonGroup personGroupFromId(Long id) {
		if (id == null) {
			return null;
		}
		PersonGroup personGroup = new PersonGroup();
		personGroup.setId(id);
		return personGroup;
	}
}
