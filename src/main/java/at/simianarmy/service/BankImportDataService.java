package at.simianarmy.service;

import at.simianarmy.aop.logging.annotation.NoLogging;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterFactory;
import at.simianarmy.imports.accounting.api.BankImporterAPI;
import at.simianarmy.imports.accounting.api.client.AuthenticationFailureException;
import at.simianarmy.imports.accounting.csv.BankImporterCSV;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.service.dto.BankImportDataDTO;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.dto.ReceiptDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.BankImportDataMapper;
import at.simianarmy.service.mapper.CashbookEntryMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service Implementation for managing BankImportData.
 */
@Service
@Transactional
public class BankImportDataService {

	private final Logger log = LoggerFactory.getLogger(BankImportDataService.class);
	public static final String CONTENT_TYPE_CSV = "application/csv";

	@Inject
	private BankImportDataRepository bankImportDataRepository;

	@Inject
	private BankImportDataMapper bankImportDataMapper;

	@Inject
	private CashbookEntryRepository cashbookEntryRepository;

	@Inject
	private CashbookEntryMapper cashbookEntryMapper;

	@Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

	@Inject
	private MembershipFeeForPeriodRepository memberShipFeeForPeriodRepository;

	@Inject
	private MembershipFeeAmountRepository memberShipFeeAmountRepository;

	@Inject
	private BankImporterFactory bankImporterFactory;

	@Inject
  private CashbookAccountRepository cashbookAccountRepository;

	@Inject
  private ReceiptService receiptService;

  @Inject
  private MessageSource messageSource;

	/**
	 * Get all the bankImportData.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<BankImportDataDTO> findAll() {
		log.debug("Request to get all BankImportData");
		List<BankImportDataDTO> result = bankImportDataRepository.findAllNotImported().stream()
				.map(bankImportDataMapper::bankImportDataToBankImportDataDTO)
				.collect(Collectors.toCollection(LinkedList::new));

		return result;
	}

  /**
   * Get one bankImportData by id.
   *
   * @param id
   *            the id of the entity
   * @return the entity
   */
  @Transactional(readOnly = true)
  public BankImportDataDTO findOne(Long id) {
    log.debug("Request to get BankImportData : {}", id);

    BankImportData bankImportData = bankImportDataRepository.findById(id).orElse(null);
    BankImportDataDTO bankImportDataDTO = bankImportDataMapper.bankImportDataToBankImportDataDTO(bankImportData);

    return bankImportDataDTO;
  }

  /**
   * Save bankImportData.
   *
   * @param bankImportDataDTO
   *            the entity to save
   * @return the persisted entity
   */
  public BankImportDataDTO save(BankImportDataDTO bankImportDataDTO) {
    log.debug("Request to save BankImportData : {}", bankImportDataDTO);

    BankImportData bankImportData = bankImportDataMapper.bankImportDataDTOToBankImportData(bankImportDataDTO);
    bankImportData = bankImportDataRepository.save(bankImportData);
    BankImportDataDTO result = bankImportDataMapper.bankImportDataToBankImportDataDTO(bankImportData);

    return result;
  }

	/**
	 * Import a List of BankImportDTO into the Cashbook.
	 * 
	 * @param bankImportDataDTOs
	 *            The DTOs to import
	 * @return The Imported CashbookEntries
	 */
	@Transactional
	public List<CashbookEntryDTO> importBankImportData(List<BankImportDataDTO> bankImportDataDTOs, Boolean createReceipts) {
		log.debug("Request to import BankImportData: {}", bankImportDataDTOs);
		List<BankImportData> bankImportData = this.bankImportDataMapper
				.bankImportDataDTOsToBankImportData(bankImportDataDTOs);
		List<CashbookEntry> cashbookEntries = new ArrayList<CashbookEntry>();

		for (BankImportData data : bankImportData) {
			CashbookEntry entry = new CashbookEntry();
			entry.setDate(data.getEntryDate());

			if (data.getEntryValue().signum() == -1) {
				entry.setType(CashbookEntryType.SPENDING);
			} else {
				entry.setType(CashbookEntryType.INCOME);
			}

			entry.setAmount(data.getEntryValue().abs());
			entry.setTitle(data.getEntryText());
			entry.setCashbookCategory(data.getCashbookCategory());
			entry.setCashbookAccount(data.getCashbookAccount());
			this.cashbookEntryRepository.saveAndFlush(entry);
			cashbookEntries.add(entry);

			data.setCashbookEntry(entry);
			this.bankImportDataRepository.saveAndFlush(data);

			this.handleMembershipFeePayment(entry);

			if (createReceipts) {
			  this.createReceiptForCashbookEntry(entry);
      }

			/** Wenn es ein Transfer ist, dann muss die Gegenbuchung noch erfasst werden */
			if (data.getTransfer()) {
			  CashbookEntry transferEntry = new CashbookEntry();
			  transferEntry.setDate(data.getEntryDate());
			  if (data.getEntryValue().signum() == -1) {
			    transferEntry.setType(CashbookEntryType.INCOME);
          transferEntry.setCashbookAccount(data.getCashbookAccountTo());
        } else {
          transferEntry.setType(CashbookEntryType.SPENDING);
          transferEntry.setCashbookAccount(data.getCashbookAccountFrom());
        }
			  transferEntry.setAmount(data.getEntryValue().abs());
			  transferEntry.setTitle(data.getEntryText());
			  transferEntry.setCashbookCategory(data.getCashbookCategory());
			  this.cashbookEntryRepository.saveAndFlush(transferEntry);
			  cashbookEntries.add(transferEntry);

			  if (createReceipts) {
			    this.createReceiptForCashbookEntry(transferEntry);
        }
      }
		}

		this.deleteBankImportDataWithoutCashbookEntry();

		return this.cashbookEntryMapper.cashbookEntriesToCashbookEntryDTOs(cashbookEntries);
	}

	/**
	 * Upload a CSV-File and generate the Import-Data, which was not imported
	 * before.
	 * 
	 * @param csvFile
	 *            The file to Import
	 * @param cashbookAccountId
	 *            The Cashbook-Account to use
	 * @return the list of created Imports
	 */
	@Transactional
	public List<BankImportDataDTO> uploadCSVFile(MultipartFile csvFile, Long cashbookAccountId, Long cashbookCategoryId) {

		log.debug("Request to upload CSV File for Importing!");
		// TODO: check csv file
		// if (!csvFile.getContentType().equals(CONTENT_TYPE_CSV)) {
		// throw new
		// ServiceValidationException(ServiceErrorConstants.bankImportWrongFileFormat,
		// null);
		// }

    CashbookCategory cashbookCategory = null;

    if (cashbookCategoryId != null) {
      cashbookCategory = this.cashbookCategoryRepository.findById(cashbookCategoryId).get();
    }

    Optional<CashbookAccount> cashbookAccountOptional = this.cashbookAccountRepository.findById(cashbookAccountId);

    if (!cashbookAccountOptional.isPresent()) {
      throw new ServiceValidationException(ServiceErrorConstants.bankImportUnknownCashbookAccount, null);
    }

    CashbookAccount cashbookAccount = cashbookAccountOptional.get();

		List<BankImportData> result = new ArrayList<BankImportData>();

		try {
			BankImporterCSV importer = this.bankImporterFactory.createCSVImporter(cashbookAccount);
			result = importer.readData(csvFile, cashbookAccount, cashbookCategory);
		} catch (IllegalStateException ex) {
			ServiceValidationException serviceException = new ServiceValidationException(
					ServiceErrorConstants.bankImportOtherErrors, null);
			serviceException.initCause(ex);
			throw serviceException;
		} catch (BankImporterException ex) {
			String errorConstant = ServiceErrorConstants.bankImportOtherErrors;

			if (ex.getCause() instanceof IllegalArgumentException) {
				errorConstant = ServiceErrorConstants.bankImportWrongCSVFormat;
			}

			ServiceValidationException serviceException = new ServiceValidationException(errorConstant, null);
			serviceException.initCause(ex);
			throw serviceException;
		}

		return this.bankImportDataMapper.bankImportDataToBankImportDataDTOs(result);
	}

	/**
	 * Import Data from API.
	 * 
	 * @param username
	 *            Username for APIConnection
	 * @param password
	 *            Password for APIConnection
	 * @param from
	 *            Date from
	 * @param to
	 *            Date to
	 * @param cashbookAccountId
	 *            Cashbook-Account to use
	 * @return The Imported Data
	 */
	@Transactional
	public List<BankImportDataDTO> importFromAPI(@NoLogging String username, @NoLogging String password, Date from,
			Date to, Long cashbookAccountId, Long cashbookCategoryId) {

		log.debug("Request to import Data from API!");

		if (to.before(from)) {
			throw new ServiceValidationException(ServiceErrorConstants.bankImportDateFromBeforeTo, null);
		}

		CashbookCategory cashbookCategory = null;

		if (cashbookCategoryId != null) {
      cashbookCategory = this.cashbookCategoryRepository.findById(cashbookCategoryId).get();
    }

    Optional<CashbookAccount> cashbookAccountOptional = this.cashbookAccountRepository.findById(cashbookAccountId);

    if (!cashbookAccountOptional.isPresent()) {
      throw new ServiceValidationException(ServiceErrorConstants.bankImportUnknownCashbookAccount, null);
    }

    CashbookAccount cashbookAccount = cashbookAccountOptional.get();

		List<BankImportData> result = new ArrayList<BankImportData>();
		try {
			BankImporterAPI importer = this.bankImporterFactory.createAPIImporter(cashbookAccount);
			result = importer.readData(username, password, from, to, cashbookAccount, cashbookCategory);

		} catch (BankImporterException ex) {

			String errorConstant = ServiceErrorConstants.bankImportOtherErrors;

			if (ex.getCause() instanceof AuthenticationFailureException) {
				errorConstant = ServiceErrorConstants.bankImportAuthenticationFailure;
			}

			ServiceValidationException serviceException = new ServiceValidationException(errorConstant, null);
			serviceException.initCause(ex);
			throw serviceException;
		}

		return this.bankImportDataMapper.bankImportDataToBankImportDataDTOs(result);
	}

	/**
	 * Deletes All BankImportdata without CashbookEntries.
	 *
	 *            The Type of the Importer
	 * @return The persisted Data
	 */
	@Transactional
	protected void deleteBankImportDataWithoutCashbookEntry() {
		for (BankImportData data : this.bankImportDataRepository.findAllNotImported()) {
			this.bankImportDataRepository.delete(data);
		}

		this.bankImportDataRepository.flush();
	}

	private void handleMembershipFeePayment(CashbookEntry entry) {
		// Ist der Eintrag eine Zahlung eines Mitgliedsbeitrages?
		String entryText = entry.getTitle();

		if (entryText.contains(" ")) {
			entryText = entryText.substring(0, entryText.indexOf(" "));
		}

		MembershipFeeForPeriod membershipFeeForPeriod = this.memberShipFeeForPeriodRepository
				.findByReferenceCode(entryText);

		if (membershipFeeForPeriod != null) {
			// Ja, dieser Mitgliedsbeitrag existiert

			BigDecimal entryValue = entry.getAmount();
			MembershipFeeAmount amount = this.memberShipFeeAmountRepository.findByMembershipFeeAndYear(
					membershipFeeForPeriod.getMembership().getMembershipFee().getId(),
					membershipFeeForPeriod.getYear());
			BigDecimal amountToPay = amount.getAmount();

			// Wurde genügend eingezahlt, damit der Mitgliedsbeitrag beglichen ist?
			if (entryValue.compareTo(amountToPay) == 0 || entryValue.compareTo(amountToPay) == 1) {
				membershipFeeForPeriod.setPaid(true);
			}

			membershipFeeForPeriod.setCashbookEntry(entry);
			this.memberShipFeeForPeriodRepository.saveAndFlush(membershipFeeForPeriod);
		}

	}

	private void createReceiptForCashbookEntry(CashbookEntry entry) {
    ReceiptDTO receiptDTO = new ReceiptDTO();
    receiptDTO.setTitle(entry.getTitle());
    receiptDTO.setDate(entry.getDate());
    receiptDTO.setOverwriteIdentifier(false);
    receiptDTO.setNote(messageSource.getMessage("bankImport.receiptAutoGenerated", null, Locale.getDefault()));
    receiptDTO.setCashbookEntryId(entry.getId());

    this.receiptService.save(receiptDTO);
  }
}
