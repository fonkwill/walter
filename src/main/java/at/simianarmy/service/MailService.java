package at.simianarmy.service;

import at.simianarmy.config.JHipsterProperties;
import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.User;
import at.simianarmy.domain.enumeration.MailLogStatus;
import at.simianarmy.repository.MailLogRepository;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import javax.activation.DataSource;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

/**
 * Service for sending e-mails.
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {

	private final Logger log = LoggerFactory.getLogger(MailService.class);

	private static final String USER = "user";
	private static final String BASE_URL = "baseUrl";

	@Inject
	private JHipsterProperties jHipsterProperties;

	@Inject
	private JavaMailSenderImpl javaMailSender;

	@Inject
	private MessageSource messageSource;

	@Inject
	private SpringTemplateEngine templateEngine;

	@Inject
  private MailLogRepository mailLogRepository;

	@Async
  public void sendEmailWithAttachment(String to, String subject, String content, boolean isMultipart, boolean isHtml, DataSource attachment, String attachmentFileName, MailLog mailLog) {
    log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
      isHtml, to, subject, content);

    // Prepare message using a Spring helper
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();
    try {
      MimeMessageHelper message = this.prepareMessage(mimeMessage, to, subject, content, isMultipart, isHtml);

      if (attachment != null) {

        if (attachmentFileName == null) {
          attachmentFileName = "Attachment";
        }

        message.addAttachment(attachmentFileName, attachment);
      }

      javaMailSender.send(mimeMessage);
      log.debug("Sent e-mail to User '{}'", to);
      this.writeMailLogSuccess(mailLog);
    } catch (Exception exception) {
      log.warn("E-mail could not be sent to user '{}'", to, exception);
      this.writeMailLogFailure(mailLog);
    }
  }

	@Async
  public void sendEmailWithAttachment(String to, String subject, String content, boolean isMultipart, boolean isHtml, DataSource attachment, String attachmentFileName) {
    this.sendEmailWithAttachment(to, subject, content, isMultipart, isHtml, attachment, attachmentFileName, null);
  }

  @Async
  public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml, MailLog mailLog) {
    log.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
      isHtml, to, subject, content);

    // Prepare message using a Spring helper
    MimeMessage mimeMessage = javaMailSender.createMimeMessage();
    try {
      MimeMessageHelper message = this.prepareMessage(mimeMessage, to, subject, content, isMultipart, isHtml);
      javaMailSender.send(mimeMessage);
      log.debug("Sent e-mail to User '{}'", to);
      this.writeMailLogSuccess(mailLog);
    } catch (Exception exception) {
      log.warn("E-mail could not be sent to user '{}'", to, exception);
      this.writeMailLogFailure(mailLog);
    }
  }

	@Async
	public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
		this.sendEmail(to, subject, content, isMultipart, isHtml, null);
	}

	@Async
	public void sendActivationEmail(User user, String baseUrl) {
		log.debug("Sending activation e-mail to '{}'", user.getEmail());
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(BASE_URL, baseUrl);
		String content = templateEngine.process("mails/activationEmail", context);
		String subject = messageSource.getMessage("email.activation.title", null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);
	}

	@Async
	public void sendCreationEmail(User user, String baseUrl) {
		log.debug("Sending creation e-mail to '{}'", user.getEmail());
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(BASE_URL, baseUrl);
		String content = templateEngine.process("mails/creationEmail", context);
		String subject = messageSource.getMessage("email.activation.title", null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);
	}

	@Async
	public void sendPasswordResetMail(User user, String baseUrl) {
		log.debug("Sending password reset e-mail to '{}'", user.getEmail());
		Locale locale = Locale.forLanguageTag(user.getLangKey());
		Context context = new Context(locale);
		context.setVariable(USER, user);
		context.setVariable(BASE_URL, baseUrl);
		String content = templateEngine.process("mails/passwordResetEmail", context);
		String subject = messageSource.getMessage("email.reset.title", null, locale);
		sendEmail(user.getEmail(), subject, content, false, true);
	}

	private MimeMessageHelper prepareMessage(MimeMessage mimeMessage, String to, String subject, String content, boolean isMultipart, boolean isHtml)
    throws MessagingException {
    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
    message.setTo(to);
    message.setFrom(jHipsterProperties.getMail().getFrom());
    message.setSubject(subject);
    message.setText(content, isHtml);
    return message;
  }

  private void writeMailLogSuccess(MailLog mailLog) {
	  if (mailLog != null) {
      mailLog.setMailLogStatus(MailLogStatus.SUCCESSFUL);
      this.mailLogRepository.saveAndFlush(mailLog);
    }
  }

  private void writeMailLogFailure(MailLog mailLog) {
    if (mailLog != null) {
      mailLog.setMailLogStatus(MailLogStatus.UNSUCCESSFUL);
      mailLog.setErrorMessage(ServiceErrorConstants.mailLogSendError);
      this.mailLogRepository.saveAndFlush(mailLog);
    }
  }
}
