package at.simianarmy.service;

import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MembershipFeeAmountMapper;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing MembershipFeeAmount.
 */
@Service
@Transactional
public class MembershipFeeAmountService {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeAmountService.class);

	@Inject
	private MembershipFeeAmountRepository membershipFeeAmountRepository;

	@Inject
	private MembershipFeeAmountMapper membershipFeeAmountMapper;

	/**
	 * Save a membershipFeeAmount.
	 *
	 * @param membershipFeeAmountDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MembershipFeeAmountDTO save(MembershipFeeAmountDTO membershipFeeAmountDTO) {
		log.debug("Request to save MembershipFeeAmount : {}", membershipFeeAmountDTO);
		MembershipFeeAmount membershipFeeAmount = membershipFeeAmountMapper
				.membershipFeeAmountDTOToMembershipFeeAmount(membershipFeeAmountDTO);
		if (membershipFeeAmount.getEndDate() != null
				&& membershipFeeAmount.getBeginDate().isAfter(membershipFeeAmount.getEndDate())) {
			throw new ServiceValidationException(ServiceErrorConstants.membershipfeeAmountBeginDateAfterEndDate, null);
		}
		Page<MembershipFeeAmount> allAmountsPage = membershipFeeAmountRepository
				.findByMembershipFee(membershipFeeAmount.getMembershipFee().getId(), null);
		if (allAmountsPage != null) {
			List<MembershipFeeAmount> allAmounts = allAmountsPage.getContent();
			for (MembershipFeeAmount amount : allAmounts) {
				if (amount.getEndDate() == null && membershipFeeAmount.getEndDate() == null) {
					if (membershipFeeAmount.getBeginDate().isBefore(amount.getBeginDate())) {
						membershipFeeAmount.setEndDate(amount.getBeginDate().minusDays(1));
					} else {
						amount.setEndDate(membershipFeeAmount.getBeginDate().minusDays(1));
						amount = membershipFeeAmountRepository.save(amount);
					}
				} else if (amount.getEndDate() == null && membershipFeeAmount.getEndDate() != null) {
					if (membershipFeeAmount.getBeginDate().isAfter(amount.getBeginDate())) {
						amount.setEndDate(membershipFeeAmount.getBeginDate().minusDays(1));
						amount = membershipFeeAmountRepository.save(amount);
					}
					if (membershipFeeAmount.getBeginDate().isBefore(amount.getBeginDate())
							&& membershipFeeAmount.getEndDate().isAfter(amount.getBeginDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipfeeAmountDatesOverlap,
								null);
					}
				} else if (amount.getEndDate() != null && membershipFeeAmount.getEndDate() == null) {
					if (membershipFeeAmount.getBeginDate().isAfter(amount.getBeginDate())
							&& membershipFeeAmount.getBeginDate().isBefore(amount.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipfeeAmountDatesOverlap,
								null);
					}
					if (membershipFeeAmount.getBeginDate().isBefore(amount.getBeginDate())) {
						membershipFeeAmount.setEndDate(amount.getBeginDate().minusDays(1));
					}
				} else if (amount.getEndDate() != null && membershipFeeAmount.getEndDate() != null) {
					if (membershipFeeAmount.getBeginDate().isAfter(amount.getBeginDate())
							&& membershipFeeAmount.getBeginDate().isBefore(amount.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipfeeAmountDatesOverlap,
								null);
					}
					if (membershipFeeAmount.getBeginDate().isBefore(amount.getBeginDate())
							&& membershipFeeAmount.getEndDate().isAfter(amount.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipDatesOverlap, null);
					}

					if (membershipFeeAmount.getEndDate().isAfter(amount.getBeginDate())
							&& membershipFeeAmount.getEndDate().isBefore(amount.getEndDate())) {
						throw new ServiceValidationException(ServiceErrorConstants.membershipfeeAmountDatesOverlap,
								null);
					}
				}
			}
		}
		membershipFeeAmount = membershipFeeAmountRepository.save(membershipFeeAmount);
		MembershipFeeAmountDTO result = membershipFeeAmountMapper
				.membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount);
		return result;
	}

	/**
	 * Get all the membershipFeeAmounts.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MembershipFeeAmountDTO> findAll(Pageable pageable) {
		log.debug("Request to get all MembershipFeeAmounts");
		Page<MembershipFeeAmount> result = membershipFeeAmountRepository.findAll(pageable);
		return result.map(membershipFeeAmount -> membershipFeeAmountMapper
				.membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount));
	}

	/**
	 * Get one membershipFeeAmount by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MembershipFeeAmountDTO findOne(Long id) {
		log.debug("Request to get MembershipFeeAmount : {}", id);
		MembershipFeeAmount membershipFeeAmount = membershipFeeAmountRepository.findById(id).orElse(null);
		MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountMapper
				.membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount);
		return membershipFeeAmountDTO;
	}

	/**
	 * Delete the membershipFeeAmount by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete MembershipFeeAmount : {}", id);
		membershipFeeAmountRepository.deleteById(id);
	}

	/**
	 * Get all the membership Fee Amounts for a Membership Fee.
	 * 
	 * @param membershipFeeId
	 *            the MembershipFee, which we want to retrieve services from
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MembershipFeeAmountDTO> findByMembershipFee(Long membershipFeeId, Pageable pageable) {
		log.debug("Request to get all MembershipFeeAmounts for a MembershipFee");
		Page<MembershipFeeAmount> result = membershipFeeAmountRepository.findByMembershipFee(membershipFeeId, pageable);
		return result.map(membershipFeeAmount -> membershipFeeAmountMapper
				.membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount));
	}
}
