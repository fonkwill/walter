package at.simianarmy.service;

import at.simianarmy.domain.ClothingType;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.service.dto.ClothingTypeDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ClothingTypeMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing ClothingType.
 */
@Service
@Transactional
public class ClothingTypeService {

	private final Logger log = LoggerFactory.getLogger(ClothingTypeService.class);

	@Inject
	private ClothingTypeRepository clothingTypeRepository;

	@Inject
	private ClothingTypeMapper clothingTypeMapper;

	/**
	 * Save a clothingType.
	 *
	 * @param clothingTypeDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ClothingTypeDTO save(ClothingTypeDTO clothingTypeDTO) {
		log.debug("Request to save ClothingType : {}", clothingTypeDTO);
		ClothingType clothingType = clothingTypeMapper.clothingTypeDTOToClothingType(clothingTypeDTO);
		clothingType = clothingTypeRepository.save(clothingType);
		ClothingTypeDTO result = clothingTypeMapper.clothingTypeToClothingTypeDTO(clothingType);
		return result;
	}

	/**
	 * Get all the clothingTypes.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ClothingTypeDTO> findAll(Pageable pageable) {
		log.debug("Request to get all ClothingTypes");
		Page<ClothingType> result = clothingTypeRepository.findAll(pageable);
		return result.map(clothingType -> clothingTypeMapper.clothingTypeToClothingTypeDTO(clothingType));
	}

	/**
	 * Get one clothingType by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ClothingTypeDTO findOne(Long id) {
		log.debug("Request to get ClothingType : {}", id);
		ClothingType clothingType = clothingTypeRepository.findById(id).orElse(null);
		ClothingTypeDTO clothingTypeDTO = clothingTypeMapper.clothingTypeToClothingTypeDTO(clothingType);
		return clothingTypeDTO;
	}

	/**
	 * Delete the clothingType by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ClothingType : {}", id);
		ClothingType clothingType = clothingTypeRepository.findById(id).orElse(null);
		if (clothingType.getClothings() == null || clothingType.getClothings().isEmpty()) {
			clothingTypeRepository.deleteById(id);
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.clothingTypeNotDeletable, null);
		}
		;
	}
}
