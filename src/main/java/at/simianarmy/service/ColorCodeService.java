package at.simianarmy.service;

import at.simianarmy.domain.ColorCode;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.service.dto.ColorCodeDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ColorCodeMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing ColorCode.
 */
@Service
@Transactional
public class ColorCodeService {

	private final Logger log = LoggerFactory.getLogger(ColorCodeService.class);

	@Inject
	private ColorCodeRepository colorCodeRepository;

	@Inject
	private ColorCodeMapper colorCodeMapper;

	/**
	 * Save a colorCode.
	 *
	 * @param colorCodeDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ColorCodeDTO save(ColorCodeDTO colorCodeDTO) {
		log.debug("Request to save ColorCode : {}", colorCodeDTO);
		ColorCode colorCode = colorCodeMapper.colorCodeDTOToColorCode(colorCodeDTO);
		colorCode = colorCodeRepository.save(colorCode);
		ColorCodeDTO result = colorCodeMapper.colorCodeToColorCodeDTO(colorCode);
		return result;
	}

	/**
	 * Get all the colorCodes.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ColorCodeDTO> findAll(Pageable pageable) {
		log.debug("Request to get all ColorCodes");
		Page<ColorCode> result = colorCodeRepository.findAll(pageable);
		return result.map(colorCode -> colorCodeMapper.colorCodeToColorCodeDTO(colorCode));
	}

	/**
	 * Get one colorCode by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ColorCodeDTO findOne(Long id) {
		log.debug("Request to get ColorCode : {}", id);
		ColorCode colorCode = colorCodeRepository.findById(id).orElse(null);
		ColorCodeDTO colorCodeDTO = colorCodeMapper.colorCodeToColorCodeDTO(colorCode);
		return colorCodeDTO;
	}

	/**
	 * Delete the colorCode by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete ColorCode : {}", id);

		ColorCode existing = this.colorCodeRepository.findById(id).orElse(null);

		if (existing != null) {
			if (existing.getCompositions().size() != 0) {
				throw new ServiceValidationException(ServiceErrorConstants.colorCodeDataIntegrityViolation, null);
			}
		}

		colorCodeRepository.deleteById(id);
	}
}
