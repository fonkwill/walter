package at.simianarmy.service;

import at.simianarmy.domain.InstrumentType;
import at.simianarmy.repository.InstrumentTypeRepository;
import at.simianarmy.service.dto.InstrumentTypeDTO;
import at.simianarmy.service.mapper.InstrumentTypeMapper;
import at.simianarmy.service.exception.ServiceValidationException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing InstrumentType.
 */
@Service
@Transactional
public class InstrumentTypeService {

	private final Logger log = LoggerFactory.getLogger(InstrumentTypeService.class);

	@Inject
	private InstrumentTypeRepository instrumentTypeRepository;

	@Inject
	private InstrumentTypeMapper instrumentTypeMapper;

	/**
	 * Save a instrumentType.
	 *
	 * @param instrumentTypeDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public InstrumentTypeDTO save(InstrumentTypeDTO instrumentTypeDTO) {
		log.debug("Request to save InstrumentType : {}", instrumentTypeDTO);
		InstrumentType instrumentType = instrumentTypeMapper.instrumentTypeDTOToInstrumentType(instrumentTypeDTO);
		instrumentType = instrumentTypeRepository.save(instrumentType);
		InstrumentTypeDTO result = instrumentTypeMapper.instrumentTypeToInstrumentTypeDTO(instrumentType);
		return result;
	}

	/**
	 * Get all the instrumentTypes.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<InstrumentTypeDTO> findAll(Pageable pageable) {
		log.debug("Request to get all InstrumentTypes");
		Page<InstrumentType> result = instrumentTypeRepository.findAll(pageable);
		return result.map(instrumentType -> instrumentTypeMapper.instrumentTypeToInstrumentTypeDTO(instrumentType));
	}

	/**
	 * Get one instrumentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public InstrumentTypeDTO findOne(Long id) {
		log.debug("Request to get InstrumentType : {}", id);
		InstrumentType instrumentType = instrumentTypeRepository.findById(id).orElse(null);
		InstrumentTypeDTO instrumentTypeDTO = instrumentTypeMapper.instrumentTypeToInstrumentTypeDTO(instrumentType);
		return instrumentTypeDTO;
	}

	/**
	 * Delete the instrumentType by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete InstrumentType : {}", id);
		InstrumentType type = instrumentTypeRepository.findById(id).orElse(null);
		if (type.getInstruments() == null || type.getInstruments().isEmpty()) {
			instrumentTypeRepository.deleteById(id);
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.instrumentTypeNotDeletable, null);
		}
	}
}
