package at.simianarmy.service;

import at.simianarmy.domain.Composer;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.service.dto.ComposerDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ComposerMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Composer.
 */
@Service
@Transactional
public class ComposerService {

	private final Logger log = LoggerFactory.getLogger(ComposerService.class);

	@Inject
	private ComposerRepository composerRepository;

	@Inject
	private ComposerMapper composerMapper;

	/**
	 * Save a composer.
	 *
	 * @param composerDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public ComposerDTO save(ComposerDTO composerDTO) {
		log.debug("Request to save Composer : {}", composerDTO);
		Composer composer = composerMapper.composerDTOToComposer(composerDTO);
		composer = composerRepository.save(composer);
		ComposerDTO result = composerMapper.composerToComposerDTO(composer);
		return result;
	}

	/**
	 * Get all the composers.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ComposerDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Composers");
		Page<Composer> result = composerRepository.findAll(pageable);
		return result.map(composer -> composerMapper.composerToComposerDTO(composer));
	}

	/**
	 * Get one composer by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public ComposerDTO findOne(Long id) {
		log.debug("Request to get Composer : {}", id);
		Composer composer = composerRepository.findOneWithEagerRelationships(id);
		ComposerDTO composerDTO = composerMapper.composerToComposerDTO(composer);
		return composerDTO;
	}

	/**
	 * Delete the composer by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Composer : {}", id);

		Composer existing = this.composerRepository.findById(id).orElse(null);

		if (existing != null && existing.getCompositions().size() != 0) {
			throw new ServiceValidationException(ServiceErrorConstants.composerDataIntegrityViolation, null);
		}
		composerRepository.deleteById(id);
	}

	/**
	 * Get all composers for a Composition.
	 * 
	 * @param compositionId
	 *            id of the composition
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<ComposerDTO> findByComposition(Long compositionId, Pageable pageable) {
		log.debug("Request to get all Composers for a Composition");
		Page<Composer> result = composerRepository.findByComposition(compositionId, pageable);
		return result.map(composer -> composerMapper.composerToComposerDTO(composer));
	}
}
