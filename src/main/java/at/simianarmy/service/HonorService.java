package at.simianarmy.service;

import at.simianarmy.domain.Honor;
import at.simianarmy.repository.HonorRepository;
import at.simianarmy.service.dto.HonorDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.HonorMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Honor.
 */
@Service
@Transactional
public class HonorService {

	private final Logger log = LoggerFactory.getLogger(HonorService.class);

	@Inject
	private HonorRepository honorRepository;

	@Inject
	private HonorMapper honorMapper;

	/**
	 * Save a honor.
	 *
	 * @param honorDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public HonorDTO save(HonorDTO honorDTO) {
		log.debug("Request to save Honor : {}", honorDTO);
		Honor honor = honorMapper.honorDTOToHonor(honorDTO);
		honor = honorRepository.save(honor);
		HonorDTO result = honorMapper.honorToHonorDTO(honor);
		return result;
	}

	/**
	 * Get all the honors.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<HonorDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Honors");
		Page<Honor> result = honorRepository.findAll(pageable);
		return result.map(honor -> honorMapper.honorToHonorDTO(honor));
	}

	/**
	 * Get one honor by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public HonorDTO findOne(Long id) {
		log.debug("Request to get Honor : {}", id);
		Honor honor = honorRepository.findById(id).orElse(null);
		HonorDTO honorDTO = honorMapper.honorToHonorDTO(honor);
		return honorDTO;
	}

	/**
	 * Delete the honor by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Honor : {}", id);
		Honor honor = honorRepository.findById(id).orElse(null);
		if (honor.getPersonHonors() == null || honor.getPersonHonors().isEmpty()) {
			honorRepository.deleteById(id);
		} else {
			throw new ServiceValidationException(ServiceErrorConstants.honorNotDeletable, null);
		}
	}
}
