package at.simianarmy.service.specification;

import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonAward;
import at.simianarmy.domain.PersonClothing;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.PersonHonor;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.domain.enumeration.Gender;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

public class PersonSpecifications {

	/**
	 * The given pattern has to be in the firstname (at least a part). This method
	 * returns a specification which consists of this constraint.
	 * 
	 * @param pattern
	 *            A string pattern which has to be in the firstname. There is no
	 *            constraint if it's null.
	 * @return A specification containing the restriction
	 */
	public static Specification<Person> firstNameContain(String pattern) {
		String searchPattern = "%" + pattern + "%";

		return new Specification<Person>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 3395416603176420425L;

			@Override
			public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (pattern == null) {
					return cb.and(); // all results
				}
				return cb.like(cb.lower(root.get("firstName")), searchPattern.toLowerCase());
			}
		};

	}

	/**
	 * The given pattern has to be in the lastname (at least a part). This method
	 * returns a specification which consists of this constraint.
	 * 
	 * @param pattern
	 *            A string pattern which has to be in the lastname. There is no
	 *            constraint if it's null.
	 * @return A specification containing the restriction
	 */
	public static Specification<Person> lastNameContain(String pattern) {

		return new Specification<Person>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 8118502565892975703L;

			@Override
			public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (pattern == null) {
					return cb.and(); // all results
				}

				String searchPattern = "%" + pattern + "%";
				return cb.like(cb.lower(root.get("lastName")), searchPattern.toLowerCase());
			}
		};
	}

	/**
	 * Returns specification for given gender
	 * 
	 * @param gender
	 *            The gender which should be applied to this specification
	 * @return A specification which supplies persons with the given gender. If
	 *         gender is null, it should return a specification for all persons
	 */
	public static Specification<Person> hasGender(Gender gender) {

		return new Specification<Person>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -8439655057510728409L;

			@Override
			public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (gender == null) {
					return cb.and(); // all results
				}
				return cb.equal(root.get("gender"), gender);
			}
		};
	}

	/**
	 * The given pattern has to be in the lastname or the firstname(at least a
	 * part). This method returns a specification which consists of this constraint.
	 * 
	 * @param pattern
	 *            A string pattern which has to be in the lastname or the firstname.
	 *            There is no constraint if it's null.
	 * @return A specification containing the restriction
	 */
	public static Specification<Person> firstOrLastNameContain(String pattern) {

		return Specification.where(lastNameContain(pattern)).or(firstNameContain(pattern));

	}

	/**
	 * With the begin and end date a range for the birthdate is defined. This method
	 * returns a specification which consists of this constraint.
	 * 
	 * @param begin
	 *            The begin date of the range concerning the birthdate. There is no
	 *            constraint if it's null.
	 * @param end
	 *            The end date of the range concerning the birthdate. There is no
	 *            constraint if it's null.
	 * @return A Specification containing the restriction
	 */
	public static Specification<Person> betweenDates(LocalDate begin, LocalDate end) {
		return new Specification<Person>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 250930764244122183L;

			@Override
			public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				Predicate currPredicate;

				if (begin != null) {
					currPredicate = cb.greaterThanOrEqualTo(root.get("birthDate"), begin);
					predicates.add(currPredicate);
				}
				if (end != null) {
					currPredicate = cb.lessThanOrEqualTo(root.get("birthDate"), end);
					predicates.add(currPredicate);
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));

			}
		};

	}

	/**
	 * Person within these Person-Groups. This method returns a specification which
	 * consists of this constraint.
	 * 
	 * @param personGroupIds
	 *            personGroupIds The List of personGroups.
	 * @return A Specification containing the restriction
	 */
	public static Specification<Person> isInPersonGroup(List<Long> personGroupIds) {
		return new Specification<Person>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6330567348420170536L;

			@Override
			public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				if (personGroupIds != null && !personGroupIds.isEmpty()) {
					Subquery<Long> sq = query.subquery(Long.class);
					Root<PersonGroup> personGroup = sq.from(PersonGroup.class);

					Join<Person, PersonGroup> personGroupJoin = personGroup.join("persons");

					sq.select(personGroupJoin.get("id")).where(cb.in(personGroup.get("id")).value(personGroupIds));

					predicates.add(cb.in(root.get("id")).value(sq));
				}

				return cb.and(predicates.toArray(new Predicate[predicates.size()]));

			}
		};

	}

	public static Specification<PersonInstrument> hasPersonIdFromPersonInstrument(Long personId) {

		return new Specification<PersonInstrument>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4864232959764835762L;

			@Override
			public Predicate toPredicate(Root<PersonInstrument> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (personId == null) {
					return cb.and();
				} else {
					root.get("person").get("id");
					return cb.equal(root.get("person").get("id"), personId);
				}
			}
		};

	}

	public static Specification<PersonClothing> hasPersonIdFromPersonClothing(Long personId) {

		return new Specification<PersonClothing>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -3797963251076068026L;

			@Override
			public Predicate toPredicate(Root<PersonClothing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (personId == null) {
					return cb.and();
				} else {
					root.get("person").get("id");
					return cb.equal(root.get("person").get("id"), personId);
				}
			}
		};

	}

	public static Specification<Membership> hasPersonIdFromMembership(Long personId) {

		return new Specification<Membership>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -3339601068998656274L;

			@Override
			public Predicate toPredicate(Root<Membership> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (personId == null) {
					return cb.and();
				} else {
					root.get("person").get("id");
					return cb.equal(root.get("person").get("id"), personId);
				}
			}
		};

	}

	public static Specification<PersonAward> hasPersonIdFromPersonAward(Long personId) {

		return new Specification<PersonAward>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -269932350815282621L;

			@Override
			public Predicate toPredicate(Root<PersonAward> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (personId == null) {
					return cb.and();
				} else {
					root.get("person").get("id");
					return cb.equal(root.get("person").get("id"), personId);
				}
			}
		};

	}

	public static Specification<PersonHonor> hasPersonIdFromPersonHonor(Long personId) {

		return new Specification<PersonHonor>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4959349782458273085L;

			@Override
			public Predicate toPredicate(Root<PersonHonor> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				if (personId == null) {
					return cb.and();
				} else {
					root.get("person").get("id");
					return cb.equal(root.get("person").get("id"), personId);
				}
			}
		};

	}

}
