package at.simianarmy.service.specification;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class CompositionSpecification {

	private Specification<Composition> specifications;

	public CompositionSpecification() {
		this.init();
	}

	/**
	 * Initialize the Specifications.
	 * 
	 * @return the Specification
	 */
	public CompositionSpecification init() {
		Specification<Composition> spec = new Specification<Composition>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 166606364944560405L;

			@Override
			public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(cb.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
		return this;
	}

	/**
	 * Add Predicate for ColorCode.
	 * 
	 * @param colorCodeId
	 *            Id of the ColorCode to match
	 * @return the Specification
	 */
	public CompositionSpecification hasColorCode(Long colorCodeId) {

		if (colorCodeId != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1931945509108837379L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

					return cb.equal(root.get("colorCode").get("id"), colorCodeId);
				}
			};

			this.specifications = this.specifications.and(specification);
		}

		return this;
	}

	/**
	 * Add Predicate for Nr.
	 * 
	 * @param nr
	 *            the Nr. to Match
	 * @return the Specification
	 */
	public CompositionSpecification hasNr(Integer nr) {

		if (nr != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 8705951309951829135L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.equal(root.get("nr"), nr);
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Title.
	 * 
	 * @param title
	 *            the Title to match
	 * @return the Specification
	 */
	public CompositionSpecification hasTitle(String title) {

		if (title != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 2818341426692463581L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.like(root.get("title"), "%" + title + "%");
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Ordering Company Name.
	 * 
	 * @param orderingCompany
	 *            Name of the Ordering Company to match
	 * @return the Specification
	 */
	public CompositionSpecification hasOrderingCompany(String orderingCompany) {
		if (orderingCompany != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1286985398956479803L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.like(root.get("orderingCompany").get("name"), "%" + orderingCompany + "%");
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Publisher.
	 * 
	 * @param publisher
	 *            Name of the Publisher to match
	 * @return the Specification
	 */
	public CompositionSpecification hasPublisher(String publisher) {

		if (publisher != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1856330529466269422L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.like(root.get("publisher").get("name"), "%" + publisher + "%");
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Composer.
	 * 
	 * @param composerName
	 *            Name of the Composer to match
	 * @return the Specification
	 */
	public CompositionSpecification hasComposer(String composerName) {

		if (composerName != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4983773820072175431L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

					Subquery<Long> sq = query.subquery(Long.class);
					Root<Composer> composer = sq.from(Composer.class);
					Join<Composer, Composition> composerJoin = composer.join("compositions");

					sq.select(composerJoin.get("id")).where(cb.like(composer.get("name"), "%" + composerName + "%"));

					return cb.in(root.get("id")).value(sq);
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Arranger.
	 * 
	 * @param arrangerName
	 *            Name of the Arranger to match
	 * @return the Specification
	 */
	public CompositionSpecification hasArranger(String arrangerName) {

		if (arrangerName != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4076048668936203201L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

					Subquery<Long> sq = query.subquery(Long.class);
					Root<Arranger> arranger = sq.from(Arranger.class);
					Join<Arranger, Composition> arrangerJoin = arranger.join("compositions");

					sq.select(arrangerJoin.get("id")).where(cb.like(arranger.get("name"), "%" + arrangerName + "%"));

					return cb.in(root.get("id")).value(sq);
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for Genre.
	 * 
	 * @param genre
	 *            Name of the Genre to match
	 * @return the Specification
	 */
	public CompositionSpecification hasGenre(String genre) {

		if (genre != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -5987688360601863711L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.like(root.get("genre").get("name"), "%" + genre + "%");
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for MusicBook.
	 * 
	 * @param musicBookId
	 *            Id of the MusicBook to match
	 * @return the Specification
	 */
	public CompositionSpecification hasMusicBook(Long musicBookId) {

		if (musicBookId != null) {

			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 6565694947761352587L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.equal(root.get("musicBook").get("id"), musicBookId);
				}
			};

			this.specifications = this.specifications.and(specification);

		}

		return this;
	}

	/**
	 * Add Predicate for isActive.
	 * 
	 * @param isActive
	 *            whether the compositions are active or not
	 * @return the Specification
	 */
	public CompositionSpecification isActive(Boolean isActive) {
		if (isActive != null) {
			Specification<Composition> specification = new Specification<Composition>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7747887392289858634L;

				@Override
				public Predicate toPredicate(Root<Composition> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					if (isActive) {
						return cb.isNotNull(root.get("musicBook"));
					} else {
						return cb.isNull(root.get("musicBook"));
					}
				}
			};

			this.specifications = this.specifications.and(specification);
		}

		return this;
	}

	/**
	 * Build the Specifications.
	 * 
	 * @return the Specifications for Spring Data Repositories
	 */
	public Specification<Composition> build() {
		return this.specifications;
	}
}
