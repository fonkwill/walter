package at.simianarmy.service.specification;

import at.simianarmy.domain.Instrument;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class InstrumentSpecification {

	private Specification<Instrument> specifications;

	/**
	 * initialize the specifications.
	 */
	public InstrumentSpecification() {
		this.init();
	}

	/**
	 * initialize the specifications.
	 */
	public void init() {
		Specification<Instrument> spec = new Specification<Instrument>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 721410460659159149L;

			@Override
			public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.equal(builder.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
	}

	/**
	 * build the specifications.
	 * 
	 * @return itself for chaining purposes
	 */
	public Specification<Instrument> build() {
		return this.specifications;
	}

	/**
	 * add name checks to specifications.
	 * 
	 * @param name
	 *            the name
	 * @return itself for chaining purposes
	 */
	public InstrumentSpecification hasName(String name) {
		if (name != null) {
			Specification<Instrument> specification = new Specification<Instrument>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 8201468152180136754L;

				@Override
				public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					return builder.like(builder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add type checks to specifications.
	 * 
	 * @param type
	 *            the type of the instrument
	 * @return itself for chaining purposes
	 */
	public InstrumentSpecification hasType(String type) {
		if (type != null) {
			Specification<Instrument> specification = new Specification<Instrument>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4158520490559693973L;

				@Override
				public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					return builder.equal(root.get("type").get("name"), type);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add privateInstrument checks to specifications.
	 * 
	 * @param privateInstrument
	 *            if the instrument is private or not
	 * @return itself for chaining purposes
	 */
	public InstrumentSpecification hasPrivate(Boolean privateInstrument) {
		if (privateInstrument != null) {
			Specification<Instrument> specification = new Specification<Instrument>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1420728187683988326L;

				@Override
				public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					return builder.equal(root.get("privateInstrument"), privateInstrument);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add date checks to specifications.
	 * 
	 * @param lowerBound
	 *            the lower date bound
	 * @param upperBound
	 *            the upper date bound
	 * @return itself for chaining purposes
	 */
	public InstrumentSpecification hasDate(LocalDate lowerBound, LocalDate upperBound) {
		if (lowerBound != null || upperBound != null) {
			Specification<Instrument> specification = new Specification<Instrument>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -4912154983676657886L;

				@Override
				public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					if (lowerBound != null && upperBound != null) {
						return builder.between(root.get("purchaseDate"), lowerBound, upperBound);
					} else if (lowerBound != null) {
						return builder.greaterThanOrEqualTo(root.get("purchaseDate"), lowerBound);
					} else {
						return builder.lessThanOrEqualTo(root.get("purchaseDate"), upperBound);
					}
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add price checks to specifications.
	 * 
	 * @param lowerBound
	 *            the lower price bound
	 * @param upperBound
	 *            the upper price bound
	 * @return itself for chaining purposes
	 */
	public InstrumentSpecification hasPrice(BigDecimal lowerBound, BigDecimal upperBound) {
		if (lowerBound != null || upperBound != null) {
			Specification<Instrument> specification = new Specification<Instrument>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 4040000953937804457L;

				@Override
				public Predicate toPredicate(Root<Instrument> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					if (lowerBound != null && upperBound != null) {
						return builder.between(root.get("price"), lowerBound, upperBound);
					} else if (lowerBound != null) {
						return builder.greaterThanOrEqualTo(root.get("price"), lowerBound);
					} else {
						return builder.lessThanOrEqualTo(root.get("price"), upperBound);
					}
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

}
