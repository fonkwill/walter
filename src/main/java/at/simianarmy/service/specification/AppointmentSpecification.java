package at.simianarmy.service.specification;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.Composition;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class AppointmentSpecification {
	private Specification<Appointment> specifications;

	public AppointmentSpecification() {
		this.init();
	}

	/**
	 * Initialize the Specification.
	 *
	 * @return the Specification
	 */
	public AppointmentSpecification init() {
		Specification<Appointment> spec = new Specification<Appointment>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4212745168218814064L;

			@Override
			public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(cb.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
		return this;
	}

	/**
	 * Add predicate for AppointmentType ID.
	 *
	 * @param appointmentTypeId
	 *            Id of the appointmentType to match
	 * @return the Specification
	 */
	public AppointmentSpecification hasAppointmentType(Long appointmentTypeId) {
		if (appointmentTypeId != null) {
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1866206429920552556L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.equal(root.get("type").get("id"), appointmentTypeId);
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for appointment name.
	 *
	 * @param searchString
	 *            name or description of the appointment to match
	 * @return the Specification
	 */
	public AppointmentSpecification hasNameOrDescription(String searchString) {
		if (searchString != null) {
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1640090169087582425L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.or(
							// convert to lowercase in order to ignore case
							cb.like(cb.lower(root.get("name")), "%" + searchString.toLowerCase() + "%"),
							cb.like(cb.lower(root.get("description")), "%" + searchString.toLowerCase() + "%"));
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for beginDate.
	 *
	 * @param earliestBegin
	 *            earliest beginDate to match
	 * @return the Specification
	 */
	public AppointmentSpecification doesNotBeginBefore(LocalDate earliestBegin) {
		if (earliestBegin != null) {
			ZonedDateTime earliestBeginTime = earliestBegin.atStartOfDay(ZoneOffset.UTC);
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -9131573957282825607L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.greaterThanOrEqualTo(root.get("beginDate"), earliestBeginTime);
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for endDate.
	 *
	 * @param latestEnd
	 *            latest endDate to match
	 * @return the Specification
	 */
	public AppointmentSpecification doesNotEndAfter(LocalDate latestEnd) {
		if (latestEnd != null) {
			// for <= latestEnd, add 1 day to begin of endDate and search for strictly lower
			ZonedDateTime latestEndTime = latestEnd.atStartOfDay(ZoneOffset.UTC).plusDays(1);
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1112275210783056509L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.lessThan(root.get("endDate"), latestEndTime);
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for location.
	 *
	 * @param location
	 *            street address or city to match
	 * @return the Specification
	 */
	public AppointmentSpecification hasLocation(String location) {
		if (location != null) {
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7973936320021257171L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.or(
							// convert to lowercase in order to ignore case
							cb.like(cb.lower(root.get("streetAddress")), "%" + location.toLowerCase() + "%"),
							cb.like(cb.lower(root.get("city")), "%" + location.toLowerCase() + "%"));
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for compositionName.
	 *
	 * @param compositionName
	 *            composition played during the appointment to match
	 * @return the Specification
	 */
	public AppointmentSpecification hasComposition(String compositionName) {
		if (compositionName != null) {
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -1819159534853442464L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					Subquery<Long> sq = query.subquery(Long.class);
					Root<Composition> composition = sq.from(Composition.class);
					Join<Composition, Appointment> compositionJoin = composition.join("appointments");
					sq.select(compositionJoin.get("id"))
							// convert to lowercase in order to ignore case
							.where(cb.like(cb.lower(composition.get("name")),
									"%" + compositionName.toLowerCase() + "%"));
					return cb.in(root.get("id")).value(sq);
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Add predicate for isOfficial.
	 *
	 * @param isOfficial
	 *            whether the appointment is an official appointment
	 * @return the Specification
	 */
	public AppointmentSpecification isOfficial(Boolean isOfficial) {
		if (isOfficial != null) {
			Specification<Appointment> specification = new Specification<Appointment>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1806117834307656989L;

				@Override
				public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					if (isOfficial) {
						return cb.isNotNull(root.get("officialAppointment"));
					} else {
						return cb.isNull(root.get("officialAppointment"));
					}
				}
			};
			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * Build the Specifications.
	 *
	 * @return the Specifications for Spring Data Repositories
	 */
	public Specification<Appointment> build() {
		return this.specifications;
	}

}
