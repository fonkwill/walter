package at.simianarmy.service.specification;

import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.CashbookEntryType;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class CashbookEntrySpecification {

	private Specification<CashbookEntry> specifications;

	/**
	 * initialize the specifications.
	 */
	public CashbookEntrySpecification() {
		this.init();
	}

	/**
	 * initialize the specifications.
	 */
	public void init() {
		Specification<CashbookEntry> spec = new Specification<CashbookEntry>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 5329465468233177253L;

			@Override
			public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.equal(builder.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
	}

	/**
	 * build the specifications.
	 * 
	 * @return itself for chaining purposes
	 */
	public Specification<CashbookEntry> build() {
		return this.specifications;
	}

	/**
	 * add title checks to specifications.
	 * 
	 * @param title
	 *            the title
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification hasTitle(String title) {
		if (title != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -5397741166431103751L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					return builder.like(builder.lower(root.get("title")), "%" + title.toLowerCase() + "%");
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add category checks to specifications.
	 * 
	 * @param categoryId
	 *            the if of the category
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification hasCategory(Long categoryId) {
		if (categoryId != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -4709221657555429285L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					return builder.equal(root.get("cashbookCategory").get("id"), categoryId);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add type checks to specifications.
	 * 
	 * @param type
	 *            the cashbook entry type
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification hasType(CashbookEntryType type) {
		if (type != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1412939670340130577L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					return builder.equal(root.get("type"), type);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add account checks to specifications.
	 * 
	 * @param account
	 *            the account of entry type
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification hasAccount(Long account) {
		if (account != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -7291969901281825791L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					return builder.equal(root.get("cashbookAccount").get("id"), account);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * add date checks to specifications.
	 * 
	 * @param lowerBound
	 *            the lower date bound
	 * @param upperBound
	 *            the upper date bound
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification hasDate(LocalDate lowerBound, LocalDate upperBound) {
		if (lowerBound != null || upperBound != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 6871049782946316640L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					if (lowerBound != null && upperBound != null) {
						return builder.between(root.get("date"), lowerBound, upperBound);
					} else if (lowerBound != null) {
						return builder.greaterThanOrEqualTo(root.get("date"), lowerBound);
					} else {
						return builder.lessThanOrEqualTo(root.get("date"), upperBound);
					}
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

	/**
	 * checks if an entry belongs to a category.
	 * 
	 * @param categoryIds
	 *            a list of category ids
	 * @return itself for chaining purposes
	 */
	public CashbookEntrySpecification belongsToCategory(List<Long> categoryIds) {
		if (categoryIds != null) {
			Specification<CashbookEntry> specification = new Specification<CashbookEntry>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 5839277186236486754L;

				@Override
				public Predicate toPredicate(Root<CashbookEntry> root, CriteriaQuery<?> query,
						CriteriaBuilder builder) {
					return root.get("cashbookCategory").in(categoryIds);
				}
			};

			this.specifications = this.specifications.and(specification);
		}
		return this;
	}

}
