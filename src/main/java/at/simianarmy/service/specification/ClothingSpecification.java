package at.simianarmy.service.specification;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.domain.PersonClothing;

public class ClothingSpecification {

	/**
	 * Brings back all "free" Clothings on a particular date
	 * 
	 * @param begin
	 *            Begin-date to check
	 * @param end
	 *            End-date to check
	 * @param clothingType
	 *            The clothingType to search for
	 * @param size
	 *            The size to search for
	 * @return a Specification to find all "free" clothings
	 */
	public static Specification<Clothing> availableWith(LocalDate begin, LocalDate end, ClothingType type,
			String size) {

		return new Specification<Clothing>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5043456647769712427L;

			@Override
			public Predicate toPredicate(Root<Clothing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				Predicate currPredicate;
				Predicate predicateDate;

				currPredicate = cb.equal(root.get("notAvailable"), false);
				predicates.add(currPredicate);

				Predicate predicateOr;
				predicateOr = cb.isEmpty(root.<Set<PersonClothing>>get("personClothings"));

				if (begin != null) {
					predicateDate = cb.or(cb.or(),
							cb.lessThan(root.join("personClothings", JoinType.LEFT).get("endDate"), begin));
					if (end != null) {
						Predicate currPredicate2;
						currPredicate2 = cb.isNotNull(root.join("personClothings", JoinType.LEFT).get("beginDate"));
						currPredicate2 = cb.and(currPredicate2,
								cb.greaterThan(root.join("personClothings", JoinType.LEFT).get("beginDate"), end));
						predicateDate = cb.or(predicateDate, currPredicate2);
					}

				} else {
					predicateDate = cb.or();
				}

				predicates.add(cb.or(predicateOr, cb.or(), predicateDate));

				if (type != null) {
					currPredicate = cb.equal(root.get("type"), type);
					predicates.add(currPredicate);
				}
				if (size != null) {
					currPredicate = cb.equal(root.get("size"), size);
					predicates.add(currPredicate);
				}
				currPredicate = cb.and(predicates.toArray(new Predicate[predicates.size()]));
				return currPredicate;
			}
		};
	}

	/**
	 * Brings back all Clothings for the given parameter
	 * 
	 * @param clothingType
	 *            The clothingType to search for
	 * @param size
	 *            The size to search for
	 * @return a Specification to find all clothings with the given parameter
	 */
	public static Specification<Clothing> allWith(ClothingType clothingType, String size) {

		return new Specification<Clothing>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -246438852158626307L;

			@Override
			public Predicate toPredicate(Root<Clothing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				Predicate currPredicate = cb.and();

				if (clothingType != null) {
					currPredicate = cb.equal(root.get("type"), clothingType);
					predicates.add(currPredicate);
				}
				if (size != null) {
					currPredicate = cb.equal(root.get("size"), size);
					predicates.add(currPredicate);
				}
				currPredicate = cb.and(predicates.toArray(new Predicate[predicates.size()]));
				return currPredicate;
			}

		};

	}

	/**
	 * Brings back all "free" Clothings
	 * 
	 * @param clothingType
	 *            The clothingType to search for
	 * @param size
	 *            The size to search for
	 * @return a Specification to find all "free" clothings
	 */
	public static Specification<Clothing> allWithNoPerson(ClothingType clothingType, String size) {

		return new Specification<Clothing>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -7271621180351592801L;

			@Override
			public Predicate toPredicate(Root<Clothing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();

				Predicate currPredicate = cb.isEmpty(root.<Set<PersonClothing>>get("personClothings"));
				predicates.add(currPredicate);

				if (clothingType != null) {
					currPredicate = cb.equal(root.get("type"), clothingType);
					predicates.add(currPredicate);
				}
				if (size != null) {
					currPredicate = cb.equal(root.get("size"), size);
					predicates.add(currPredicate);
				}
				currPredicate = cb.and(predicates.toArray(new Predicate[predicates.size()]));
				return currPredicate;
			}

		};

	}

	/**
	 * Brings back all distinct clothing with the given clothingTYpe and the given
	 * size
	 * 
	 * @param clothingType
	 *            Given clothingType
	 * @param size
	 *            Given size
	 * @return A specification of clothings
	 */
	public static Specification<Clothing> allDistinct(ClothingType clothingType, String size) {

		return new Specification<Clothing>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1263523226545104338L;

			@Override
			public Predicate toPredicate(Root<Clothing> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate currPredicate = cb.and();
				List<Predicate> predicates = new ArrayList<>();

				if (clothingType != null) {
					currPredicate = cb.equal(root.get("type"), clothingType);
					predicates.add(currPredicate);
				}
				if (size != null) {
					currPredicate = cb.equal(root.get("size"), size);
					predicates.add(currPredicate);
				}
				currPredicate = cb.and(predicates.toArray(new Predicate[predicates.size()]));

				query.distinct(true);
				return currPredicate;

			}
		};

	}
}
