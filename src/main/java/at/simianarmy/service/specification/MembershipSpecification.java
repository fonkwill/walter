package at.simianarmy.service.specification;

import java.time.LocalDate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFeeForPeriod;

public class MembershipSpecification {

	public static Specification<Membership> hasNoMembershipFeeForPeriodIn(int year) {

		return new Specification<Membership>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 4234120136095659451L;

			@Override
			public Predicate toPredicate(Root<Membership> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				Subquery<MembershipFeeForPeriod> subquery = query.subquery(MembershipFeeForPeriod.class);
				Root<MembershipFeeForPeriod> membershipFee = subquery.from(MembershipFeeForPeriod.class);
				subquery.select(membershipFee);
				Predicate p = cb.equal(root.get("id"), membershipFee.get("membership").get("id"));
				subquery.where(cb.and(p, cb.equal(membershipFee.get("year"), year)));

				Predicate notExistsSubqueryPred = cb.not(cb.exists(subquery));

				Predicate membershipFeePred = cb.isNotNull(root.get("membershipFee"));
				Predicate beginDatePred = cb.lessThanOrEqualTo(root.get("beginDate"), LocalDate.of(year, 12, 31));
				Predicate endDatePred = cb.or(cb.isNull(root.get("endDate")),
						cb.greaterThanOrEqualTo(root.get("endDate"), LocalDate.of(year, 01, 01)));
				Predicate relevantForGivenYearPred = cb.and(beginDatePred, endDatePred);

				return cb.and(membershipFeePred, relevantForGivenYearPred, notExistsSubqueryPred);

			}
		};

	}

}
