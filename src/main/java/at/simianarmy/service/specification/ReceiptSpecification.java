package at.simianarmy.service.specification;

import at.simianarmy.domain.Receipt;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class ReceiptSpecification {

	private Specification<Receipt> specifications;

	/**
	 * initialize the specifications.
	 */
	public ReceiptSpecification() {
		this.init();
	}

	/**
	 * initialize the specifications.
	 */
	public void init() {
		Specification<Receipt> spec = new Specification<Receipt>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 7882349666189513316L;

			@Override
			public Predicate toPredicate(Root<Receipt> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.equal(builder.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
	}

	/**
	 * build the specifications.
	 * 
	 * @return itself for chaining purposes
	 */
	public Specification<Receipt> build() {
		return this.specifications;
	}

	/**
	 * add title/company checks to specifications.
	 * 
	 * @param sword
	 *            the search word
	 * @return itself for chaining purposes
	 */
	public ReceiptSpecification hasTitleOrCompany(String sword) {
		if (sword != null) {
			Specification<Receipt> titleSpec = new Specification<Receipt>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 2909077937461404299L;

				@Override
				public Predicate toPredicate(Root<Receipt> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					return builder.like(builder.lower(root.get("title")), "%" + sword.toLowerCase() + "%");
				}
			};

			Specification<Receipt> companySpec = new Specification<Receipt>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = -3295963232038700396L;

				@Override
				public Predicate toPredicate(Root<Receipt> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
					return builder.like(builder.lower(root.join("company", JoinType.LEFT).get("name")),
							"%" + sword.toLowerCase() + "%");
				}
			};

			this.specifications = Specification.where(titleSpec);
			this.specifications = this.specifications.or(companySpec);
		}

		return this;
	}

}
