package at.simianarmy.service.specification;

import at.simianarmy.domain.Letter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class LetterSpecification {

	private Specification<Letter> specifications;

	/**
	 * initialize the specifications.
	 */
	public LetterSpecification() {
		this.init();
	}

	/**
	 * initialize the specifications.
	 */
	public void init() {
		Specification<Letter> spec = new Specification<Letter>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 2803257292823707238L;

			@Override
			public Predicate toPredicate(Root<Letter> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				return builder.equal(builder.literal(true), true);
			}
		};

		this.specifications = Specification.where(spec);
	}

	/**
	 * build the specifications.
	 *
	 * @return itself for chaining purposes
	 */
	public Specification<Letter> build() {
		return this.specifications;
	}

	/**
	 * add payment check to specifications.
	 *
	 * @param payment
	 *            if payment is needed
	 * @return itself for chaining purposes
	 */
	public LetterSpecification hasPayment(Boolean payment) {
		if (payment != null) {
			Specification<Letter> paymentSpec = new Specification<Letter>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1465182292985412062L;

				@Override
				public Predicate toPredicate(Root<Letter> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					if (payment) {
						return cb.or(cb.equal(root.get("currentPeriod"), true),
								cb.equal(root.get("previousPeriod"), true));
					} else {
						return cb.and(cb.isNull(root.get("currentPeriod")), cb.isNull(root.get("previousPeriod")));
					}
				}
			};
			this.specifications = Specification.where(paymentSpec);
		}
		return this;
	}

}
