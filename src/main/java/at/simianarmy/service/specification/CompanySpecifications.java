package at.simianarmy.service.specification;

import at.simianarmy.domain.Company;
import at.simianarmy.domain.PersonGroup;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.jpa.domain.Specification;

public class CompanySpecifications {

	/**
	 * Person within these Person-Groups. This method returns a specification which
	 * consists of this constraint.
	 * 
	 * @param personGroupIds
	 *            personGroupIds The List of personGroups.
	 * @return A Specification containing the restriction
	 */
	public static Specification<Company> isInPersonGroup(List<Long> personGroupIds) {
		return new Specification<Company>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 8351885220059698952L;

			@Override
			public Predicate toPredicate(Root<Company> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();

				if (personGroupIds != null && !personGroupIds.isEmpty()) {
					Subquery<Long> sq = query.subquery(Long.class);
					Root<PersonGroup> personGroup = sq.from(PersonGroup.class);

					Join<Company, PersonGroup> personGroupJoin = personGroup.join("companies");

					sq.select(personGroupJoin.get("id")).where(cb.in(personGroup.get("id")).value(personGroupIds));

					predicates.add(cb.in(root.get("id")).value(sq));
				} else {
				  //always false
				  return cb.or();
        }

				return cb.and(predicates.toArray(new Predicate[predicates.size()]));

			}
		};

	}

}
