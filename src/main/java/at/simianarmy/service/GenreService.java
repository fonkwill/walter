package at.simianarmy.service;

import at.simianarmy.domain.Genre;
import at.simianarmy.repository.GenreRepository;
import at.simianarmy.service.dto.GenreDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.GenreMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Genre.
 */
@Service
@Transactional
public class GenreService {

	private final Logger log = LoggerFactory.getLogger(GenreService.class);

	@Inject
	private GenreRepository genreRepository;

	@Inject
	private GenreMapper genreMapper;

	/**
	 * Save a genre.
	 *
	 * @param genreDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public GenreDTO save(GenreDTO genreDTO) {
		log.debug("Request to save Genre : {}", genreDTO);
		Genre genre = genreMapper.genreDTOToGenre(genreDTO);
		genre = genreRepository.save(genre);
		GenreDTO result = genreMapper.genreToGenreDTO(genre);
		return result;
	}

	/**
	 * Get all the genres.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<GenreDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Genres");
		Page<Genre> result = genreRepository.findAll(pageable);
		return result.map(genre -> genreMapper.genreToGenreDTO(genre));
	}

	/**
	 * Get one genre by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public GenreDTO findOne(Long id) {
		log.debug("Request to get Genre : {}", id);
		Genre genre = genreRepository.findById(id).orElse(null);
		GenreDTO genreDTO = genreMapper.genreToGenreDTO(genre);
		return genreDTO;
	}

	/**
	 * Delete the genre by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Genre : {}", id);

		Genre existing = this.genreRepository.findById(id).orElse(null);

		if (existing != null && existing.getCompositions().size() != 0) {
			throw new ServiceValidationException(ServiceErrorConstants.genreDataIntegrityViolation, null);
		}

		genreRepository.deleteById(id);
	}
}
