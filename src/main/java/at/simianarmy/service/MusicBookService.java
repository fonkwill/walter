package at.simianarmy.service;

import at.simianarmy.domain.MusicBook;
import at.simianarmy.repository.MusicBookRepository;
import at.simianarmy.service.dto.MusicBookDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MusicBookMapper;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing MusicBook.
 */
@Service
@Transactional
public class MusicBookService {

	private final Logger log = LoggerFactory.getLogger(MusicBookService.class);

	@Inject
	private MusicBookRepository musicBookRepository;

	@Inject
	private MusicBookMapper musicBookMapper;

	/**
	 * Save a musicBook.
	 *
	 * @param musicBookDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public MusicBookDTO save(MusicBookDTO musicBookDTO) {
		log.debug("Request to save MusicBook : {}", musicBookDTO);
		MusicBook musicBook = musicBookMapper.musicBookDTOToMusicBook(musicBookDTO);
		musicBook = musicBookRepository.save(musicBook);
		MusicBookDTO result = musicBookMapper.musicBookToMusicBookDTO(musicBook);
		return result;
	}

	/**
	 * Get all the musicBooks.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<MusicBookDTO> findAll(Pageable pageable) {
		log.debug("Request to get all MusicBooks");
		Page<MusicBook> result = musicBookRepository.findAll(pageable);
		return result.map(musicBook -> musicBookMapper.musicBookToMusicBookDTO(musicBook));
	}

	/**
	 * Get one musicBook by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public MusicBookDTO findOne(Long id) {
		log.debug("Request to get MusicBook : {}", id);
		MusicBook musicBook = musicBookRepository.findById(id).orElse(null);
		MusicBookDTO musicBookDTO = musicBookMapper.musicBookToMusicBookDTO(musicBook);
		return musicBookDTO;
	}

	/**
	 * Delete the musicBook by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete MusicBook : {}", id);

		MusicBook existing = this.musicBookRepository.findById(id).orElse(null);

		if (existing != null && existing.getCompositions().size() != 0) {
			throw new ServiceValidationException(ServiceErrorConstants.musicBookDataIntegrityViolation, null);
		}

		musicBookRepository.deleteById(id);
	}
}
