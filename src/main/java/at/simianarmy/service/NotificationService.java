package at.simianarmy.service;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.User;
import at.simianarmy.repository.NotificationRepository;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.service.dto.NotificationDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.NotificationMapper;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Notification.
 */
@Service
@Transactional
public class NotificationService {

	private final Logger log = LoggerFactory.getLogger(NotificationService.class);

	@Inject
	private NotificationRepository notificationRepository;

	@Inject
	private UserRepository userRepository;

	@Inject
	private NotificationMapper notificationMapper;

	/**
	 * Save a notification.
	 *
	 * @param notificationDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public NotificationDTO save(NotificationDTO notificationDTO) {
		log.debug("Request to save Notification : {}", notificationDTO);
		Notification notification = notificationMapper.notificationDTOToNotification(notificationDTO);
		notification = notificationRepository.save(notification);
		NotificationDTO result = notificationMapper.notificationToNotificationDTO(notification);
		return result;
	}

	/**
	 * Get all the notifications.
	 * 
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public List<NotificationDTO> findAll() {
		log.debug("Request to get all Notifications");

		User user = userRepository.findCurrentUser();
		List<String> authorities = user.getRoles().stream().map(authority -> authority.getName())
				.collect(Collectors.toList());

		if (authorities.contains("ROLE_ADMIN")) {
			return notificationRepository.findAll().stream()
					.map(notification -> notificationMapper.notificationToNotificationDTO(notification))
					.collect(Collectors.toList());
		} else {
			return notificationRepository.findAllForAuthorities(authorities).stream()
					.map(notification -> notificationMapper.notificationToNotificationDTO(notification))
					.collect(Collectors.toList());
		}
	}

	/**
	 * Delete the notification by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Notification : {}", id);
		notificationRepository.deleteById(id);
	}

	/**
	 * Returns the total count for all notifications available for the current user.
	 * 
	 * @return the count
	 */
	public Long getTotalCount() {
		log.debug("Request to count total number of notifications");

		User user = userRepository.findCurrentUser();
		List<String> authorities = user.getRoles().stream().map(authority -> authority.getName())
				.collect(Collectors.toList());

		if (authorities.contains("ROLE_ADMIN")) {
			return notificationRepository.countTotal();
		} else {
			return notificationRepository.countTotalForAuthorities(authorities);
		}
	}

	/**
	 * Returns the count for all unread notifications available for the current
	 * user.
	 * 
	 * @return the count
	 */
	public Long getUnreadCount() {
		log.debug("Request to count number of unread notifications");

		User user = userRepository.findCurrentUser();
		List<String> authorities = user.getRoles().stream().map(authority -> authority.getName())
				.collect(Collectors.toList());

		if (authorities.contains("ROLE_ADMIN")) {
			return notificationRepository.countUnread();
		} else {
			return notificationRepository.countUnreadForAuthorities(authorities);
		}
	}

	/**
	 * Mark a single notification as read.
	 * 
	 * @param id
	 *            the notification id
	 */
	public void markNotificationAsRead(Long id) {
		log.debug("Request to mark notification with id {} as read", id);

		Notification notification = notificationRepository.findById(id).orElse(null);
		if (notification == null) {
			throw new ServiceValidationException(ServiceErrorConstants.notificationNotFound, null);
		}

		User user = userRepository.findCurrentUser();

		notification.setReadByUser(user);
		notificationRepository.save(notification);
	}
}
