package at.simianarmy.roles.service.mapper;

import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.service.dto.AuthorityDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Authority and its DTO AuthorityDTO.
 */
@Mapper(componentModel = "spring")
public interface AuthorityMapper {

	
	AuthorityDTO authorityToAuthorityDTO(Authority authority);

	List<AuthorityDTO> authorityToAppointmentDTOs(List<Authority> authorities);

	@Mapping(target = "role", ignore = true)
	Authority authorityDTOToAuthority(AuthorityDTO authorityDTO);

	List<Authority> authorityDTOsToAuthorities(List<AuthorityDTO> authorityDTOs);

}
