package at.simianarmy.roles.service.mapper;

import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.service.dto.RoleDTO;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Role and its DTO RoleDTO.
 */
@Mapper(componentModel = "spring", uses = AuthorityMapper.class)
public interface RoleMapper {

	RoleDTO roleToRoleDTO(Role role);

	List<RoleDTO> rolesToRoleDTOs(List<Role> roles);

	Role roleDTOToRole(RoleDTO roleDTO);

	List<Role> roleDTOsToRoles(List<RoleDTO> roleDTOs);

	
}
