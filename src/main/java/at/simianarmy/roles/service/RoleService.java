package at.simianarmy.roles.service;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.FeatureGroup;
import at.simianarmy.roles.domain.RequiredAuth;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.roles.repository.RoleRepository;
import at.simianarmy.roles.service.dto.RoleDTO;
import at.simianarmy.roles.service.mapper.RoleMapper;
import at.simianarmy.service.CustomizationService;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Role.
 */
@Service
@Transactional
public class RoleService {

	private final Logger log = LoggerFactory.getLogger(RoleService.class);

	@Inject
	private RoleRepository roleRepository;

	@Inject
	private RoleMapper roleMapper;
	
	@Inject
	private AuthConfigRepository authConfigRepository;

	@Inject
	private CustomizationService customizationService;

	@Inject
  private WalterProperties walterProperties;

	@Inject
  private TenantIdentiferResolver tenantIdentiferResolver;
	/**
	 * Save a role.
	 *
	 * @param roleDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	public RoleDTO save(RoleDTO roleDTO) {
		log.debug("Request to save Role : {}", roleDTO);

		Role role = roleMapper.roleDTOToRole(roleDTO);
		if (role.getId() != null && ( role.getId().equals(new Long(1)) || role.getId().equals(new Long(2)) ) ) {
			throw new ServiceValidationException(ServiceErrorConstants.roleChangeNotAllowed,   new String[]{} );
		}
		role = fillRequiredAuthorities(role);

    final Role roleS = role;
    role.getAuthorities().stream().forEach(a -> a.setRole(roleS));
		role = roleRepository.save(role);
		RoleDTO result = roleMapper.roleToRoleDTO(role);
		return result;
	}

  /**
   * Some write-authorities need another read-authority. These have to be set before the role is saved or updated.
   * @param role The role to check for and fill with the required authorities
   */
  private Role fillRequiredAuthorities(Role role) {
    List<RequiredAuth> requiredAuths = authConfigRepository.getRequiredAuths();

    //find all authorities which require another authority
    Set<Authority> roleAuthorities = role.getAuthorities();
    Set<String> authoritiesToUpdate = new HashSet<>();
    for (Authority roleAuth : roleAuthorities) {
      if (roleAuth.getWrite().equals(true)) {
        List<RequiredAuth> requiredAuthorityForThis = requiredAuths.stream()
          .filter(requiredAuth -> requiredAuth.getAuthority().equals(roleAuth.getName()))
          .collect(Collectors.toList());
        requiredAuthorityForThis.forEach(reqAuthForThis -> authoritiesToUpdate.addAll(reqAuthForThis.getRequiredAuthorities()));
      }
    }

    //update authorities in role if not exists already
    for (String authorityToUpdate : authoritiesToUpdate) {
      Authority alreadyAssigend = roleAuthorities.stream().filter(ra -> ra.getName().equals(authorityToUpdate)).findFirst().orElse(null);
      if (alreadyAssigend == null) {
        Authority toAssign = new Authority();
        toAssign.setName(authorityToUpdate);
        toAssign.setWrite(false);
        roleAuthorities.add(toAssign);
        role.setAuthorities(roleAuthorities);
      }

    }

    return role;
  }

  /**
	 * Get all the roles.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<RoleDTO> findAll(Pageable pageable) {
		log.debug("Request to get all Roles");
		Page<Role> result = roleRepository.findAll(pageable);
		return result.map(role -> roleMapper.roleToRoleDTO(role));
	}

	/**
	 * Get one role by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public RoleDTO findOne(Long id) {
		log.debug("Request to get Role : {}", id);
		Role role = roleRepository.findById(id).orElse(null);
		RoleDTO roleDTO = roleMapper.roleToRoleDTO(role);
		return roleDTO;
	}

	/**
	 * Delete the role by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete Role : {}", id);
		if (id.equals(new Long(1)) || id.equals(new Long(2))) {
			throw new ServiceValidationException(ServiceErrorConstants.roleChangeNotAllowed,   new String[]{} );
		}
		roleRepository.deleteById(id);
	}
	
	/**
	 * Returns all possible Authorities
	 * 
	 * @return A List of Strings with all possible authorities
	 */
	public List<String> getAllPossibleAuthorities() {
		log.debug("Request to get all possible Authorities");
		return authConfigRepository.getAllPossibleAuthorities(); 
	}

	/**
	 * Returns all feature groups with their authorities, deactivated Feature groups are excluded
	 * 
	 * @return A list of feature groups
	 */
	public List<FeatureGroup> getAllFeatureGroups() {
		log.debug("Request to get all feature groups");
		
		List<String> deactivatedFeatureGroups = customizationService.getDeactivatedFeatureGroups();

    if(!walterProperties.getMultitenancy().isEnabled() || !tenantIdentiferResolver.resolveCurrentTenantIdentifier().equals(TenantIdentiferResolver.MASTER_TENANT)) {
      deactivatedFeatureGroups.add("TENANT_CONFIGURATION");
    }
		return authConfigRepository.getAllFeatureGroups().stream().
				filter(fg -> !deactivatedFeatureGroups.contains(fg.getGroup())).
				collect(Collectors.toList());		
	}
	
	
	
	
	}
