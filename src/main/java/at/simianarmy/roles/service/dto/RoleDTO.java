package at.simianarmy.roles.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.constraints.NotNull;

import at.simianarmy.roles.domain.Authority;

/**
 * A DTO for the Role entity.
 */
public class RoleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7077916934358430190L;

	private Long id;

	@NotNull
	private String name;
	
	private Set<AuthorityDTO> authorities;


	public Set<AuthorityDTO> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<AuthorityDTO> authorities) {
		this.authorities = authorities;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		RoleDTO roleDTO = (RoleDTO) obj;

		if (!Objects.equals(id, roleDTO.id)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "RoleDTO{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
