package at.simianarmy.roles.repository;

import at.simianarmy.domain.User;
import at.simianarmy.roles.domain.Authority;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {

	Optional<User> findByName(String user);
}
