package at.simianarmy.roles.repository;

import at.simianarmy.roles.domain.Role;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Role entity.
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {



	Optional<Role> findByName(String name);



}
