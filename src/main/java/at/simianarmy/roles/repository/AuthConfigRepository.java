package at.simianarmy.roles.repository;

import java.util.List;

import at.simianarmy.roles.domain.FeatureGroup;
import at.simianarmy.roles.domain.RequestAuth;
import at.simianarmy.roles.domain.RequiredAuth;

public interface AuthConfigRepository {

	
	public List<RequestAuth> getAllRequestAuthConfig();
	
	public List<String> getAllPossibleAuthorities();
	
	public List<String> getAllAuthoritiesForRequest(String request);

	public List<String> getElementsForAuthority(String string);

	public String getFeatureGroupForAuthority(String auth);

	List<FeatureGroup> getAllFeatureGroups();

	List<String> getAllAuthoritiesForFeatureGroups(List<String> pfeatureGroups);

  List<RequiredAuth> getRequiredAuths();
}
