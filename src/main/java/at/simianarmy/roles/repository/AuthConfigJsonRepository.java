package at.simianarmy.roles.repository;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceException;

import at.simianarmy.roles.domain.RequiredAuth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.simianarmy.roles.domain.AuthConfig;
import at.simianarmy.roles.domain.FeatureGroup;
import at.simianarmy.roles.domain.RequestAuth;

@Repository
public class AuthConfigJsonRepository implements AuthConfigRepository {

	private List<RequestAuth> requestAuthConfigs = new ArrayList<>();
	
	private List<FeatureGroup> featureGroups = new ArrayList<>();

  private List<RequiredAuth> requiredAuths = new ArrayList<>();
	
	private Map<Pattern, RequestAuth> requestAuthConfigMap = new HashMap<>();
	
	private Map<String, String> authorityFeatureGroup = new HashMap<>();

	private Map<String, Set<String>> authRelatedElements = new HashMap<>();
	
	private Set<String> possibleAuthorities = new HashSet<>();
	
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Value("${walter.multitenancy.authConfig:classpath:config/authConfig.json}")
	private Resource authConfig;
	
	
	@Override
	public List<RequestAuth> getAllRequestAuthConfig() {
		return requestAuthConfigs;
	}

	@Override
	public List<String> getAllPossibleAuthorities() {
		return new ArrayList<>(possibleAuthorities);
	}

	@Override
	public List<String> getAllAuthoritiesForRequest(String request) {
		RequestAuth reqAuth = requestAuthConfigMap.entrySet().
									stream().filter(entry -> entry.getKey().matcher(request).matches()).
									findFirst().
									map(entry -> entry.getValue()).
									orElse(null);
		if (reqAuth == null) {
			return new ArrayList<>();
		}
		
		return reqAuth.getAuthorities();
	
	}
	
	@PostConstruct
	public void initialize() throws PersistenceException{
		authorityFeatureGroup.clear();
		requestAuthConfigs.clear();
		requestAuthConfigMap.clear();
		possibleAuthorities.clear();
		authRelatedElements.clear();
		featureGroups.clear();
		requiredAuths.clear();
		try {
			InputStream f = authConfig.getInputStream();
			AuthConfig config;
			mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
			config = mapper.readValue(f, AuthConfig.class);

			
			if (config != null) {
			  //store required Auths
        requiredAuths = config.getRequiredAuths();

				//get feature group for authority
				for (FeatureGroup fg: config.getFeatureGroups()) {
					featureGroups.add(fg);
					for (String auth : fg.getAuthorities()) {
						authorityFeatureGroup.put(auth, fg.getGroup());
					}
				}
				
				
				requestAuthConfigs = config.getRequestAuths();
				for (RequestAuth reqAuth : requestAuthConfigs) {
								
					
					Pattern pattern = Pattern.compile(reqAuth.getRequest());
					requestAuthConfigMap.put(pattern, reqAuth);
					
					List<String> elements = reqAuth.getElements();
					Set<String> storedElements = null;
					for (String auth : reqAuth.getAuthorities()) {
						
						storedElements = authRelatedElements.get(auth);
						if (storedElements == null) {
							storedElements = new HashSet<>();
						}
						if (elements != null) {
							storedElements.addAll(elements);
							authRelatedElements.put(auth, storedElements);
						}
						
						possibleAuthorities.add(auth);
					}
				}

			}
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	@Override
	public List<String> getElementsForAuthority(String auth) {
		Set<String> relatedElements = authRelatedElements.get(auth);
		if (relatedElements != null) {
			return new ArrayList<>(relatedElements);
		} else {
			return new ArrayList<>();
		}
	}
	
	@Override
	public String getFeatureGroupForAuthority(String auth) {
		return authorityFeatureGroup.get(auth);
	}
	
	@Override
	public List<FeatureGroup> getAllFeatureGroups() {
		return featureGroups;
	}
	
	public List<String> getAllAuthoritiesForFeatureGroup(String featureGroup) {
		List<String> authorities = new ArrayList<>();
		
		FeatureGroup lfeatureGroup = featureGroups.stream().
										filter(f -> f.getGroup().equals(featureGroup)).
										findFirst().
										orElse(null);
		
		if (lfeatureGroup != null) {
			authorities.addAll(lfeatureGroup.getAuthorities());
		}
		return authorities;
	}
	
	@Override
	public List<String> getAllAuthoritiesForFeatureGroups(List<String> pfeatureGroups) {
		List<String> authorities = new ArrayList<>();
		
		List<FeatureGroup> lfeatureGroups = featureGroups.stream().
										filter(f -> pfeatureGroups.contains(f.getGroup())).
										collect(Collectors.toList());
		
		if (lfeatureGroups != null) {
			lfeatureGroups.stream().forEach(f -> authorities.addAll(f.getAuthorities()));
		}
		return authorities;
	}

  @Override
  public List<RequiredAuth> getRequiredAuths() {
    return requiredAuths;
  }
	

}
