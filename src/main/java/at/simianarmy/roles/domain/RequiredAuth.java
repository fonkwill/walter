package at.simianarmy.roles.domain;

import java.util.List;
import java.util.Objects;

public class RequiredAuth {

  private String authority;

  private List<String> requiredAuthorities;

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public List<String> getRequiredAuthorities() {
    return requiredAuthorities;
  }

  public void setRequiredAuthorities(List<String> requiredAuthorities) {
    this.requiredAuthorities = requiredAuthorities;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RequiredAuth that = (RequiredAuth) o;
    return Objects.equals(authority, that.authority) &&
      Objects.equals(requiredAuthorities, that.requiredAuthorities);
  }

  @Override
  public int hashCode() {
    return Objects.hash(authority, requiredAuthorities);
  }


}
