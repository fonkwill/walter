package at.simianarmy.roles.domain;

import java.util.List;

public class RequestAuth {
	
	
	private String request;

	private List<String> authorities;

	private List<String> elements;
	
	public List<String> getElements() {
		return elements;
	}

	public void setElements(List<String> elements) {
		this.elements = elements;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public List<String> getAuthorities() {
		return authorities;
	}
	

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RequestAuth other = (RequestAuth) obj;
		if (request == null) {
			if (other.request != null)
				return false;
		} else if (!request.equals(other.request))
			return false;
		return true;
	}


}
