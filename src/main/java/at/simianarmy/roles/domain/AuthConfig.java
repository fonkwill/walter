package at.simianarmy.roles.domain;

import java.util.List;

public class AuthConfig {
	
	private List<RequestAuth> requestAuths;
	
	private List<FeatureGroup> featureGroups;

  private List<RequiredAuth> requiredAuths;


  public List<RequiredAuth> getRequiredAuths() {
    return requiredAuths;
  }

  public void setRequiredAuths(List<RequiredAuth> requiredAuths) {
    this.requiredAuths = requiredAuths;
  }

	public List<RequestAuth> getRequestAuths() {
		return requestAuths;
	}

	public void setRequestAuths(List<RequestAuth> auths) {
		this.requestAuths = auths;
	}
	
	public List<FeatureGroup> getFeatureGroups() {
		return featureGroups;
	}

	public void setFeatureGroups(List<FeatureGroup> featureGroups) {
		this.featureGroups = featureGroups;
	}

}
