package at.simianarmy.roles.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * An authority  
 */
@Entity
@Table(name = "jhi_authority")
public class Authority implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String separator = ":";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Boolean write;
	
	@NotNull
	@Size(min = 0, max = 50)
	private String name;
	
	@ManyToOne
	@JoinColumn(name="ROLE_ID")
	private Role role;



	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Boolean getWrite() {
		return write;
	}

	public void setWrite(Boolean write) {
		this.write = write;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		Authority authority = (Authority) obj;

		if (!Objects.equals(name, authority.name)) {
			return false;
		}

		return true;
	}
	
	public String getGrantedAuthRepr() {
		return name+separator+write;
	}
	
	public static Authority getFromGrantedAuthRepr(String grantedAuthRepr) {
		if (grantedAuthRepr == null) {
			return null;
		}
		String[] params = grantedAuthRepr.split(separator);
		if (params.length < 2) {
			return null;
		}
		Authority auth = new Authority();
		auth.setName(params[0]);
		auth.setWrite(new Boolean(params[1]));
		return auth;
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Authority{" + "name='" + name + '\'' + "}";
	}
	
	
}
