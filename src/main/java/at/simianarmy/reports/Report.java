package at.simianarmy.reports;

import at.simianarmy.service.exception.ServiceValidationException;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Locale;
import javax.inject.Inject;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

@Service
public abstract class Report {

  @Inject
  private SpringTemplateEngine templateEngine;

  @Inject
  protected MessageSource messageSource;

  protected Context context = new Context(Locale.forLanguageTag("DE"));


  public Resource createReport() {
    String fullInformationAsHtml = templateEngine.process(this.getReportTemplate(), context);

    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ITextRenderer renderer = new ITextRenderer();
    renderer.setDocumentFromString(fullInformationAsHtml);
    renderer.layout();
    try {
      renderer.createPDF(bos);
      ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());

      return new InputStreamResource(bis);
    } catch (DocumentException e) {
      throw new ServiceValidationException("Could not write full Information", null);
    }
  }

  protected abstract String getReportTemplate();

}
