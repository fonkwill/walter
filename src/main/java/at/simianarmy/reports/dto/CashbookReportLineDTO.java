package at.simianarmy.reports.dto;

import java.math.BigDecimal;

public class CashbookReportLineDTO {

  private String name;

  private BigDecimal income;

  private BigDecimal spending;

  private String color;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getIncome() {
    if (this.income == null) {
      return new BigDecimal(0);
    }
    return this.income;
  }

  public void setIncome(BigDecimal income) {
    this.income = income;
  }

  public BigDecimal getSpending() {
    if (this.spending == null) {
      return new BigDecimal(0);
    }
    return spending;
  }

  public void setSpending(BigDecimal spending) {
    this.spending = spending;
  }

  public BigDecimal getSaldo() {
    return this.getIncome().subtract(this.getSpending());
  }

  public String getColor() {
    if (this.color == null) {
      return "#ffffff";
    } else {
      return this.color;
    }
  }

  public void setColor(String color) {
    this.color = color;
  }
}
