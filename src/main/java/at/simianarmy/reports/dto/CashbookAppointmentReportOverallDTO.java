package at.simianarmy.reports.dto;

import at.simianarmy.domain.CashbookCategory;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class CashbookAppointmentReportOverallDTO {

  private HashMap<CashbookCategory, CashbookReportLineDTO> categoryLines = new HashMap<>();

  public void addLineForCategory(CashbookCategory category, CashbookReportLineDTO line) {
    if (this.categoryLines.containsKey(category)) {
      CashbookReportLineDTO lineInMap = this.categoryLines.get(category);
      lineInMap.setSpending(lineInMap.getSpending().add(line.getSpending()));
      lineInMap.setIncome(lineInMap.getIncome().add(line.getIncome()));
    } else {
      CashbookReportLineDTO lineForMap = new CashbookReportLineDTO();
      lineForMap.setName(line.getName());
      lineForMap.setColor(line.getColor());
      lineForMap.setIncome(line.getIncome());
      lineForMap.setSpending(line.getSpending());
      this.categoryLines.put(category, lineForMap);
    }
  }

  public List<CashbookReportLineDTO> getCategoryLines() {

    if (categoryLines.isEmpty()) {
      return new ArrayList<>();
    }

    List<CashbookReportLineDTO> categoryLinesAsList = new ArrayList<CashbookReportLineDTO>();

    for (CashbookCategory category : this.categoryLines.keySet()) {
      if (category != null) {
        categoryLinesAsList.add(this.categoryLines.get(category));
      }
    }

    Collections.sort(categoryLinesAsList, new Comparator<CashbookReportLineDTO>() {
      @Override
      public int compare(CashbookReportLineDTO o1, CashbookReportLineDTO o2) {
        return o1.getName().compareTo(o2.getName());
      }
    });

    if (this.categoryLines.keySet().contains(null)) {
      categoryLinesAsList.add(this.categoryLines.get(null));
    }

    return categoryLinesAsList;
  }

  public CashbookReportLineDTO getFinalOutcomeLine() {
    CashbookReportLineDTO finalOutcome = new CashbookReportLineDTO();
    finalOutcome.setIncome(new BigDecimal(0));
    finalOutcome.setSpending(new BigDecimal(0));

    for (CashbookReportLineDTO dto : categoryLines.values()) {
      finalOutcome.setIncome(finalOutcome.getIncome().add(dto.getIncome()));
      finalOutcome.setSpending(finalOutcome.getSpending().add(dto.getSpending()));
    }

    return finalOutcome;
  }

}
