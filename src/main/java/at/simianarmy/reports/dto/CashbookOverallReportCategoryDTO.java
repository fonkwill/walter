package at.simianarmy.reports.dto;

import java.util.List;

public class CashbookOverallReportCategoryDTO {

  private List<CashbookReportLineDTO> lines;

  public List<CashbookReportLineDTO> getLines() {
    return lines;
  }

  public void setLines(List<CashbookReportLineDTO> lines) {
    this.lines = lines;
  }
}
