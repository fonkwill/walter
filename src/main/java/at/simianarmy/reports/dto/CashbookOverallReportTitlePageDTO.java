package at.simianarmy.reports.dto;

import java.math.BigDecimal;
import java.util.List;

public class CashbookOverallReportTitlePageDTO {

  private List<CashbookReportLineDTO> linesPerBegin;

  private List<CashbookReportLineDTO> linesPerEnd;

  private List<CashbookReportLineDTO> outcomeLines;

  public List<CashbookReportLineDTO> getLinesPerBegin() {
    return linesPerBegin;
  }

  public void setLinesPerBegin(
    List<CashbookReportLineDTO> linesPerBegin) {
    this.linesPerBegin = linesPerBegin;
  }

  public List<CashbookReportLineDTO> getLinesPerEnd() {
    return linesPerEnd;
  }

  public void setLinesPerEnd(
    List<CashbookReportLineDTO> linesPerEnd) {
    this.linesPerEnd = linesPerEnd;
  }

  public List<CashbookReportLineDTO> getOutcomeLines() {
    return outcomeLines;
  }

  public void setOutcomeLines(
    List<CashbookReportLineDTO> outcomeLines) {
    this.outcomeLines = outcomeLines;
  }

  public BigDecimal getFinalSaldoPerBegin() {
    return this.getFinalSaldo(this.linesPerBegin);
  }

  public BigDecimal getFinalSaldoPerEnd() {
    return this.getFinalSaldo(this.linesPerEnd);
  }

  public CashbookReportLineDTO getFinalOutcomeLine() {
    BigDecimal income = new BigDecimal(0);
    BigDecimal spending = new BigDecimal(0);

    for (CashbookReportLineDTO line : this.outcomeLines) {
      income = income.add(line.getIncome());
      spending = spending.add(line.getSpending());
    }

    CashbookReportLineDTO finalOutcome = new CashbookReportLineDTO();
    finalOutcome.setIncome(income);
    finalOutcome.setSpending(spending);

    return finalOutcome;
  }

  private BigDecimal getFinalSaldo(List<CashbookReportLineDTO> lines) {
    BigDecimal finalSaldo = new BigDecimal(0);

    for (CashbookReportLineDTO line : lines) {
      finalSaldo = finalSaldo.add(line.getSaldo());
    }

    return finalSaldo;
  }
}
