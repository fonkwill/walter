package at.simianarmy.reports.dto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CashbookAppointmentReportMainDTO {

  private LocalDate dateFrom;

  private LocalDate dateTo;

  private String appointmentType;

  private List<CashbookAppointmentReportDTO> reportDTOs = new ArrayList<>();

  private CashbookAppointmentReportDTO overallDTO;

  public String getDateFrom() {
    return dateFrom.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public String getDateTo() {
    return dateTo.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }

  public String getAppointmentType() {
    return appointmentType;
  }

  public void setAppointmentType(String appointmentType) {
    this.appointmentType = appointmentType;
  }

  public List<CashbookAppointmentReportDTO> getReportDTOs() {
    return reportDTOs;
  }

  public void addReportDTO(
    CashbookAppointmentReportDTO reportDTO) {
    this.reportDTOs.add(reportDTO);
  }

  public CashbookAppointmentReportDTO getOverallDTO() {
    return overallDTO;
  }

  public void setOverallDTO(CashbookAppointmentReportDTO overallDTO) {
    this.overallDTO = overallDTO;
  }
}
