package at.simianarmy.reports.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CashbookAppointmentReportDTO {

  private String text;

  private List<CashbookReportLineDTO> lines = new ArrayList<>();

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public List<CashbookReportLineDTO> getLines() {
    return lines;
  }

  public void addLine(CashbookReportLineDTO line) {
    this.lines.add(line);
  }

  public CashbookReportLineDTO getFinalOutcomeLine() {
    BigDecimal income = new BigDecimal(0);
    BigDecimal spending = new BigDecimal(0);

    for (CashbookReportLineDTO line : this.lines) {
      income = income.add(line.getIncome());
      spending = spending.add(line.getSpending());
    }

    CashbookReportLineDTO finalOutcome = new CashbookReportLineDTO();
    finalOutcome.setIncome(income);
    finalOutcome.setSpending(spending);

    return finalOutcome;
  }
}
