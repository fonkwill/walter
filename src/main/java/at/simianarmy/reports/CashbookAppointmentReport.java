package at.simianarmy.reports;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.AppointmentType;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.reports.dto.CashbookAppointmentReportDTO;
import at.simianarmy.reports.dto.CashbookAppointmentReportMainDTO;
import at.simianarmy.reports.dto.CashbookAppointmentReportOverallDTO;
import at.simianarmy.reports.dto.CashbookReportLineDTO;
import at.simianarmy.repository.AppointmentRepository;
import at.simianarmy.repository.AppointmentTypeRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class CashbookAppointmentReport extends Report {

  @Inject
  private AppointmentTypeRepository appointmentTypeRepository;

  @Inject
  private AppointmentRepository appointmentRepository;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  public Resource create(LocalDate dateFrom, LocalDate dateTo, List<Long> appointmentTypeIds) {

    List<CashbookAppointmentReportMainDTO> mainDTOs = new ArrayList<>();
    HashMap<AppointmentType, CashbookAppointmentReportOverallDTO> overallDTOs = new HashMap<>();

    for (Long appointmentTypeId : appointmentTypeIds) {

      CashbookAppointmentReportMainDTO mainDTO = new CashbookAppointmentReportMainDTO();
      mainDTO.setDateFrom(dateFrom);
      mainDTO.setDateTo(dateTo);

      AppointmentType appointmentType = this.appointmentTypeRepository.findById(appointmentTypeId)
        .get();
      CashbookAppointmentReportOverallDTO perAppointmentDTO = new CashbookAppointmentReportOverallDTO();
      overallDTOs.put(appointmentType, perAppointmentDTO);

      List<Appointment> appointments = this.appointmentRepository
        .findByAppointmentTypeAndMinAndMaxDate(
          appointmentTypeId, dateFrom, dateTo
        );

      mainDTO.setAppointmentType(appointmentType.getName());

      CashbookAppointmentReportDTO overallDTO = new CashbookAppointmentReportDTO();
      overallDTO.setText(
        this.messageSource.getMessage("cashbook.appointmentReport.overallView", null, Locale
          .getDefault()));

      for (Appointment appointment : appointments) {
        CashbookAppointmentReportDTO dto = new CashbookAppointmentReportDTO();
        dto.setText(
          appointment.getBeginDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + ": "
            + appointment.getName());
        List<CashbookCategory> categories = this.cashbookEntryRepository
          .getCashbookCategoriesForAppointment(appointment);

        for (CashbookCategory category : categories) {
          CashbookReportLineDTO lineDTO = new CashbookReportLineDTO();
          lineDTO.setName(category.getName());
          lineDTO.setIncome(this.cashbookEntryRepository
            .getIncomeSumForAppointmentAndCashbookCategory(appointment, category));
          lineDTO.setSpending(this.cashbookEntryRepository
            .getSpendingSumForAppointmentAndCashbookCategory(appointment, category));
          lineDTO.setColor(category.getColor());
          dto.addLine(lineDTO);
          perAppointmentDTO.addLineForCategory(category, lineDTO);
        }

        BigDecimal incomeWithoutCategory = this.cashbookEntryRepository
          .getIncomeSumForAppointmentWithNoCashbookCategory(appointment);
        BigDecimal spendingWithoutCategory = this.cashbookEntryRepository
          .getSpendingSumForAppointmentWithNoCashbookCategory(appointment);

        if (incomeWithoutCategory != null || spendingWithoutCategory != null) {
          CashbookReportLineDTO lineWithoutCategory = new CashbookReportLineDTO();
          lineWithoutCategory
            .setName(this.messageSource.getMessage("cashbook.overallReport.noCategory", null, Locale
              .getDefault()));
          lineWithoutCategory.setIncome(incomeWithoutCategory);
          lineWithoutCategory.setSpending(spendingWithoutCategory);

          dto.addLine(lineWithoutCategory);
          perAppointmentDTO.addLineForCategory(null, lineWithoutCategory);
        }

        mainDTO.addReportDTO(dto);

        CashbookReportLineDTO lineForOverallView = new CashbookReportLineDTO();
        lineForOverallView.setName(dto.getText());
        lineForOverallView.setIncome(dto.getFinalOutcomeLine().getIncome());
        lineForOverallView.setSpending(dto.getFinalOutcomeLine().getSpending());
        overallDTO.addLine(lineForOverallView);
      }

      mainDTO.setOverallDTO(overallDTO);

      mainDTOs.add(mainDTO);

    }

    this.context.setVariable("dateFrom", dateFrom.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    this.context.setVariable("dateTo", dateTo.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
    this.context.setVariable("mainDTOs", mainDTOs);
    this.context.setVariable("overallDTOs", overallDTOs);

    return this.createReport();
  }

  @Override
  protected String getReportTemplate() {
    return "reports/cashbookAppointmentReport";
  }
}
