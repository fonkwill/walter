package at.simianarmy.reports;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.reports.dto.CashbookOverallReportCategoryDTO;
import at.simianarmy.reports.dto.CashbookReportLineDTO;
import at.simianarmy.reports.dto.CashbookOverallReportTitlePageDTO;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.CashbookCategoryService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class CashbookOverallReport extends Report {

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  private CashbookCategoryService cashbookCategoryService;

  /**
   * Generates the Report
   *
   * @param year
   * @param cashbookAccountIds
   * @param cashbookCategoryIds
   * @return
   */
  public Resource create(Integer year, List<Long> cashbookAccountIds, List<Long> cashbookCategoryIds) {
    this.context.setVariable("year", year);

    LocalDate maxDateLastYear = LocalDate.ofYearDay(year, 1);
    LocalDate maxDateCurrentYear = LocalDate.ofYearDay(year + 1, 1);

    CashbookOverallReportTitlePageDTO titlePageDTO = new CashbookOverallReportTitlePageDTO();
    titlePageDTO.setLinesPerBegin(this.getCashbookAccountLines(maxDateLastYear, cashbookAccountIds));
    titlePageDTO.setOutcomeLines(this.getCashbookAccountOutcomeLines(maxDateLastYear, maxDateCurrentYear, cashbookAccountIds));
    titlePageDTO.setLinesPerEnd(this.getCashbookAccountLines(maxDateCurrentYear, cashbookAccountIds));

    CashbookOverallReportCategoryDTO categoryDTO = new CashbookOverallReportCategoryDTO();
    categoryDTO.setLines(this.getCategoryLines(maxDateLastYear, maxDateCurrentYear, cashbookCategoryIds));

    this.context.setVariable("titlePageDTO", titlePageDTO);
    this.context.setVariable("categoryDTO", categoryDTO);

    return this.createReport();
  }

  private List<CashbookReportLineDTO> getCashbookAccountLines(LocalDate maxDate, List<Long> cashbookAccountIds) {

    if (cashbookAccountIds == null || cashbookAccountIds.isEmpty()) {
      cashbookAccountIds =  this.cashbookAccountRepository.findAll().stream().map(account -> account.getId()).collect(
        Collectors.toList());
    }

    List<CashbookReportLineDTO> lines = new ArrayList<>();

    for (Long cashbookAccountId : cashbookAccountIds) {
      CashbookAccount account = this.cashbookAccountRepository.findById(cashbookAccountId).get();
      List<Long> currentCashbookAccountId = new ArrayList<>(Arrays.asList(cashbookAccountId));
      CashbookReportLineDTO line = new CashbookReportLineDTO();
      line.setIncome(this.cashbookEntryRepository.getIncomeSumForMaxDateAndCashbookAccountId(maxDate, currentCashbookAccountId));
      line.setSpending(this.cashbookEntryRepository.getSpendingSumForMaxDateAndCashbookAccountId(maxDate, currentCashbookAccountId));
      line.setName(account.getName());
      lines.add(line);
    }

    lines.sort(Comparator.comparing(CashbookReportLineDTO::getName));

    return lines;
  }

  private List<CashbookReportLineDTO> getCashbookAccountOutcomeLines(LocalDate minDate, LocalDate maxDate, List<Long> cashbookAccountIds) {

    if (cashbookAccountIds == null || cashbookAccountIds.isEmpty()) {
      cashbookAccountIds =  this.cashbookAccountRepository.findAll().stream().map(account -> account.getId()).collect(
        Collectors.toList());
    }

    List<CashbookReportLineDTO> lines = new ArrayList<>();

    for (Long cashbookAccountId : cashbookAccountIds) {
      CashbookAccount account = this.cashbookAccountRepository.findById(cashbookAccountId).get();
      List<Long> currentCashbookAccountId = new ArrayList<>(Arrays.asList(cashbookAccountId));
      CashbookReportLineDTO line = new CashbookReportLineDTO();
      line.setIncome(this.cashbookEntryRepository.getIncomeSumForMinAndMaxDateAndCashbookAccountId(minDate, maxDate, currentCashbookAccountId));
      line.setSpending(this.cashbookEntryRepository.getSpendingSumForMinAndMaxDateAndCashbookAccountId(minDate, maxDate, currentCashbookAccountId));
      line.setName(account.getName());
      lines.add(line);
    }

    lines.sort(Comparator.comparing(CashbookReportLineDTO::getName));

    return lines;
  }

  private List<CashbookReportLineDTO> getCategoryLines(LocalDate minDate, LocalDate maxDate, List<Long> cashbookCategoryIds) {
    Boolean printNoCategory = false;
    if (cashbookCategoryIds == null || cashbookCategoryIds.isEmpty()) {
      printNoCategory = true;
      cashbookCategoryIds =  this.cashbookCategoryRepository.findAll().stream().map(category -> category.getId()).collect(
        Collectors.toList());
    } else {
      cashbookCategoryIds = this.getAllCashbookCagegoryIdsRecursive(cashbookCategoryIds);
    }

    List<CashbookReportLineDTO> lines = new ArrayList<>();

    for (Long cashbookCategoryId : cashbookCategoryIds) {
      CashbookReportLineDTO lineDTO = new CashbookReportLineDTO();
      CashbookCategory category = this.cashbookCategoryRepository.findById(cashbookCategoryId).get();
      List<Long> currentCashbookCategoryId = new ArrayList<Long>(
        Arrays.asList(cashbookCategoryId));
      lineDTO.setName(category.getName());
      lineDTO.setColor(category.getColor());
      lineDTO.setIncome(this.cashbookEntryRepository.getIncomeSumForMinAndMaxDateAndCashbookCategoryId(minDate, maxDate, currentCashbookCategoryId));
      lineDTO.setSpending(this.cashbookEntryRepository.getSpendingSumForMinAndMaxDateAndCashbookCategoryId(minDate, maxDate, currentCashbookCategoryId));
      lines.add(lineDTO);
    }

    lines.sort(Comparator.comparing(CashbookReportLineDTO::getName));

    if (printNoCategory) {
      CashbookReportLineDTO lineDTO = new CashbookReportLineDTO();
      lineDTO.setName(this.messageSource.getMessage("cashbook.overallReport.noCategory", null, Locale.getDefault()));
      lineDTO.setIncome(
        this.cashbookEntryRepository.getIncomeSumForMinAndMaxDateWithoutCategory(minDate, maxDate));
      lineDTO.setSpending(
        this.cashbookEntryRepository.getSpendingSumForMinAndMaxDateWithoutCategory(minDate, maxDate));

      lines.add(lineDTO);
    }

    return lines;
  }

  private List<Long> getAllCashbookCagegoryIdsRecursive(List<Long> cashbookCategoryIds) {
    List<Long> result;

    List<CashbookCategory> categories = new ArrayList<>();

    for (Long id : cashbookCategoryIds) {
      categories.add(this.cashbookCategoryRepository.findById(id).get());
    }

    List<CashbookCategory> categoriesWithChildren = new ArrayList<>(this.cashbookCategoryService.getAllCashbookCategoriesRecursive(categories));

    result = categoriesWithChildren.stream().map(category -> category.getId()).collect(
      Collectors.toList());

    return result;
  }

  @Override
  protected String getReportTemplate() {
    return "reports/cashbookOverallReport";
  }
}
