package at.simianarmy.multitenancy.repository;

import java.io.File;
import java.io.InputStream;
import java.util.*;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.simianarmy.multitenancy.MultitenancyConfiguration;
import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;

@Component
public class TenantConfigurationJsonRepository implements TenantConfigurationRepository {

	private static final Logger logger = LoggerFactory.getLogger(TenantConfigurationJsonRepository.class);

	private Map<String, TenantConfiguration> configurations = new HashMap<>();

	private List<DBConfiguration> connections = new ArrayList<>();

	private MultitenancyConfiguration config;

	private ObjectMapper mapper = new ObjectMapper();

	@Value("${walter.multitenancy.tenantConfig:classpath:config/tenantConfig.json}")
	private Resource tenantConfig;

	@Override
	public TenantConfiguration getTenantConfigurationForTenant(String tenantId) {
		TenantConfiguration config = configurations.get(tenantId);
		if (config == null) {
			initialize();
			config = configurations.get(tenantId);
		}
		return config;
	}

	@Override
	public Map<String, TenantConfiguration> getAllTenantConfigurations() {
		if (configurations.isEmpty()) {
			initialize();
		}
		return configurations;
	}

	@PostConstruct
	public void initialize() throws PersistenceException{
		configurations.clear();
		connections.clear();
		try {
			InputStream f = tenantConfig.getInputStream();
			MultitenancyConfiguration config;
			mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
			config = mapper.readValue(f, MultitenancyConfiguration.class);

			if (config != null) {
				this.config = config;

				connections = config.getConnections();
				for (TenantConfiguration tenant : config.getTenants()) {
					configurations.put(tenant.getTenantId(), tenant);
				}

			}
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	@Override
	public Page<TenantConfiguration> findAll(Pageable pageable) {
		List<TenantConfiguration> list = new ArrayList<>(configurations.values());
		Page<TenantConfiguration> page = new PageImpl<>(list, pageable, list.size());
		return page;
	}

	@Override
	public void deleteForTenant(String tenant) {
		try {
			File f = tenantConfig.getFile();
			if (config != null) {

				List<TenantConfiguration> newConfigurations = new ArrayList<>();
				for (TenantConfiguration tenantC : config.getTenants()) {
					if (tenantC.getTenantId().equals(tenant)) {
						configurations.remove(tenantC.getTenantId()).getTenantId();
					} else {
						newConfigurations.add(tenantC);
					}
				}
				config.setTenants(newConfigurations);
				mapper.writer().writeValue(f, config);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	@Override
	public TenantConfiguration save(TenantConfiguration tenantConfiguration) {
		try {
			File f = tenantConfig.getFile();
			boolean alreadySaved = false;
			if (config != null) {
				List<TenantConfiguration> newConfigurations = new ArrayList<>();
				for (TenantConfiguration tenantC : config.getTenants()) {
					if (tenantC.getTenantId().equals(tenantConfiguration.getTenantId())) {

						tenantConfiguration.setJwtSecret(tenantC.getJwtSecret());
						
						setJwtSecret(tenantConfiguration);

						configurations.put(tenantC.getTenantId(), tenantConfiguration);
						alreadySaved = true;
						newConfigurations.add(tenantConfiguration);
					} else {
						newConfigurations.add(tenantC);
					}
				}
				
				if (!alreadySaved) {
					if (tenantConfiguration.getJwtSecret() == null) {
						tenantConfiguration.setJwtSecret(generateJwtSecret());
					}
					configurations.put(tenantConfiguration.getTenantId(), tenantConfiguration);
					newConfigurations.add(tenantConfiguration);
				}
				config.setTenants(newConfigurations);
				mapper.writer().writeValue(f, config);
				return tenantConfiguration;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;

	}

	private TenantConfiguration setJwtSecret(TenantConfiguration tenantConfiguration) {
		if (!StringUtils.hasText(tenantConfiguration.getJwtSecret())) {
			tenantConfiguration.setJwtSecret(generateJwtSecret());
		}
		return tenantConfiguration;
	}

	private String generateJwtSecret() {
		return RandomStringUtils.randomAlphanumeric(64);
	}

	@Override
  public Set<String> getAllTenantIDs() {
	  Set<String> tenantIds = new HashSet<>();
	  if (configurations.keySet() == null) {
	    return new HashSet<>();
    }
    configurations.keySet().stream().forEach(h -> tenantIds.add(h));
	  return tenantIds;
  }

	@Override
	public List<DBConfiguration> getAllConnections() {
		if (connections.isEmpty()) {
			initialize();
		}
		return connections;
	}

}
