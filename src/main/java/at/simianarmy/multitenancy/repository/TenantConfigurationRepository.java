package at.simianarmy.multitenancy.repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;

public interface TenantConfigurationRepository {
	
	public TenantConfiguration getTenantConfigurationForTenant(String tenantId);
	
	public Map<String, TenantConfiguration> getAllTenantConfigurations();

	public Page<TenantConfiguration> findAll(Pageable pageable);

	public void deleteForTenant(String tenant);

	public TenantConfiguration save(TenantConfiguration tenantConfiguration);

  Set<String> getAllTenantIDs();

  public List<DBConfiguration> getAllConnections();
	
	public void initialize() throws PersistenceException;

}
