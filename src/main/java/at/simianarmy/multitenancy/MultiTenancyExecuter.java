package at.simianarmy.multitenancy;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.repository.TenantConfigurationRepository;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class MultiTenancyExecuter {

  private TenantConfigurationRepository tenantConfigurationRepository;

  private TaskExecutor taskExecutor;

  private WalterProperties walterProperties;

  public MultiTenancyExecuter(TenantConfigurationRepository tenantConfigurationRepository, TaskExecutor taskExecutor, WalterProperties walterProperties) {
    this.tenantConfigurationRepository = tenantConfigurationRepository;
    this.taskExecutor = taskExecutor;
    this.walterProperties = walterProperties;
  }

  public void execute(Runnable r) {

    if (r == null) {
      return;
    }

    if(walterProperties.getMultitenancy().isEnabled()) {

      Set<String> tenants = tenantConfigurationRepository.getAllTenantIDs();
      tenants.add(TenantIdentiferResolver.MASTER_TENANT);

      for (String tenant : tenants) {
        taskExecutor.execute(() -> {
          TenantContext.setCurrentTenant(tenant);
          r.run();
        });

      }
    } else {
      r.run();
    }

  }

}
