package at.simianarmy.multitenancy.domain;

public class TenantConfiguration {

	private String tenantId;

	private DBConfiguration dbConfiguration;
	private String schema;
	private String jwtSecret;

	public String getJwtSecret() {
		return jwtSecret;
	}

	public void setJwtSecret(String jwtSecret) {
		this.jwtSecret = jwtSecret;
	}

	public DBConfiguration getDbConfiguration() {
		return dbConfiguration;
	}

	public void setDbConfiguration(DBConfiguration dbConfiguration) {
		this.dbConfiguration = dbConfiguration;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
