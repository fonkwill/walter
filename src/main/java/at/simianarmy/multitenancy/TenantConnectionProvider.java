package at.simianarmy.multitenancy;

import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationJsonRepository;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO.ConfigurationConnectionStatus;
import at.simianarmy.service.util.BeanUtil;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.cfg.Environment;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.hibernate.service.spi.ServiceRegistryAwareService;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TenantConnectionProvider implements MultiTenantConnectionProvider, ServiceRegistryAwareService {

	private static final Logger logger = LoggerFactory.getLogger(TenantConnectionProvider.class);
	
	private static final long serialVersionUID = 1L;
	
	private DataSource datasource;
	
	@Autowired
	private TenantDataSourceHolder tenantDatasources;
	
	@Autowired
	private TenantConfigurationJsonRepository repo;
	

	@Override
	public void injectServices(ServiceRegistryImplementor serviceRegistry) {
		Map<?, ?> settings =  serviceRegistry.getService(ConfigurationService.class).getSettings();
		
		datasource = (DataSource) settings.get(Environment.DATASOURCE);
		
	}
	

	@Override
	public Connection getAnyConnection() throws SQLException {
		return datasource.getConnection();
	}

	@Override
	public Connection getConnection(String tenantId) throws SQLException {
		logger.debug("Getting db-connection for tenant {}", tenantId);
		if (tenantId.equals(TenantIdentiferResolver.MASTER_TENANT)) {
			logger.debug("Returned 'anyConnection'");
			return getAnyConnection();
		}
	    DataSource ds = getDataSource(tenantId);
	    try {
	    	return ds.getConnection();
	    } catch (SQLException ex) {
	    	logger.error("Couldn't retrieve connection for tenant {}", tenantId);
	    	tenantDatasources.remove(tenantId);
	    	TenantDataSourceBuilder.closeDs(ds);
	    }
	    return null;
	}

	public DataSource getDataSource(String tenantId) {
		if (this.tenantDatasources == null) {
			this.tenantDatasources = BeanUtil.getBean(TenantDataSourceHolder.class);
		}
		DataSource result = tenantDatasources.get(tenantId);
		if (result != null) {
			return result;
		}
		
		if (this.repo == null) {
			this.repo = BeanUtil.getBean(TenantConfigurationJsonRepository.class);
		}
				
		TenantConfiguration tenantConfig = repo.getTenantConfigurationForTenant(tenantId);
		
		TenantConnectionStatusDTO status = TenantDataSourceBuilder.getConnectionStatus(tenantConfig
				);
		
		if (status.getConfigurationConnectionStatus().equals(ConfigurationConnectionStatus.OK)) {
			result = TenantDataSourceBuilder.getDataSource(tenantConfig);
			tenantDatasources.put(tenantId, result);
		}
		
		return result;

	}

	private void getRanChangeSets() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void releaseAnyConnection(Connection connection) throws SQLException {
		connection.close();
		
	}

	@Override
	public void releaseConnection(String tenantId, Connection connection) throws SQLException {
		connection.close();
		
	}


	@Override
	public boolean isUnwrappableAs(@SuppressWarnings("rawtypes") Class unwrapType) {
		return false;
	}


	@Override
	public <T> T unwrap(Class<T> unwrapType) {
		return null;
	}


	@Override
	public boolean supportsAggressiveRelease() {
		return false;
	}






}
