package at.simianarmy.multitenancy;

import java.util.List;

import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;

public class MultitenancyConfiguration {

	private List<DBConfiguration> connections;
	private List<TenantConfiguration> tenants;

	public List<DBConfiguration> getConnections() {
		return connections;
	}

	public void setConnections(List<DBConfiguration> connections) {
		this.connections = connections;
	}

	public List<TenantConfiguration> getTenants() {
		return tenants;
	}

	public void setTenants(List<TenantConfiguration> tenants) {
		this.tenants = tenants;
	}

}
