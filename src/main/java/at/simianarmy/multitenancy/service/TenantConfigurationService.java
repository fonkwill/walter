package at.simianarmy.multitenancy.service;

import at.simianarmy.config.liquibase.MultipleAsyncSpringLiquibase;
import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.multitenancy.TenantDataSourceBuilder;
import at.simianarmy.multitenancy.TenantDataSourceHolder;
import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationRepository;
import at.simianarmy.multitenancy.service.dto.TenantConfigurationDTO;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO.ConfigurationConnectionStatus;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO.UsedConnectionStatus;
import at.simianarmy.multitenancy.service.mapper.TenantConfigurationMapper;
import at.simianarmy.scheduling.task.UserColumnConfigGeneratorTask;
import at.simianarmy.service.exception.ServiceValidationException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceSchemaCreatedEvent;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.async.DeferredResult;

import com.sun.jersey.api.client.TerminatingClientHandler;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Service Implementation for managing tenant configurations.
 */
@Service
@Transactional
public class TenantConfigurationService {

	private final Logger log = LoggerFactory.getLogger(TenantConfigurationService.class);

	@Inject
	private TenantConfigurationRepository tenantConfigurationRepository;
	
	@Inject
	private TenantConfigurationMapper tenantConfigurationMapper;
	
	@Inject
	private TenantDataSourceHolder tenantDatasources;
	
	@Inject
	private TaskExecutor taskExecutor;
	
	@Inject
	private UserColumnConfigGeneratorTask userColumnConfigGeneratorTask;
	
	@Autowired(required = false)
	private MultipleAsyncSpringLiquibase liquibase;
	/**
	 * Save a tenantConfiguration.
	 *
	 * @param tenantConfiguration
	 *            the entity to save
	 * @return the persisted entity
	 */
	public TenantConfigurationDTO save(TenantConfigurationDTO tenantConfiguration) {
		log.debug("Request to save tenantConfiguration : {}", tenantConfiguration);
		
		
		if (tenantConfiguration.getDbUrl() == null ||
			tenantConfiguration.getSchema() == null || 
			tenantConfiguration.getTenantId() == null) {
				throw new ServiceValidationException("Wrong input for TenantConfiguration to save", new String[] {tenantConfiguration.getTenantId()} );
			}
		
		if (!tenantConfiguration.getTenantId().matches("^\\w{3,}$")) {
			throw new ServiceValidationException("Wrong input for tenantId", new String[] {tenantConfiguration.getTenantId()} );
		}
		
		if (!tenantConfiguration.getSchema().matches("^\\w{3,}$")) {
			throw new ServiceValidationException("Wrong input for schema", new String[] {tenantConfiguration.getSchema()} );
		}
		
//		check if there's already a tenant with given db-url and schema
//		only if new
		for (TenantConfiguration config : tenantConfigurationRepository.getAllTenantConfigurations().values()) {
			if (config.getTenantId().equals(tenantConfiguration.getTenantId())) {
				continue;
			}
			
			if ( config.getDbConfiguration().getUrl().equals(tenantConfiguration.getDbUrl()) &&
				 config.getSchema().equals(tenantConfiguration.getSchema())) {
				throw new ServiceValidationException("DB-Connection with Schema exists already", 
							new String[] {tenantConfiguration.getDbUrl(), tenantConfiguration.getSchema() });
				
			}
		}
		
		TenantConfiguration result = tenantConfigurationMapper.tenantConfigurationDTOToTenantConfiguration(tenantConfiguration);
		
//	   set DB-Configuration because of given url
	   for (DBConfiguration dbConfig : tenantConfigurationRepository.getAllConnections()) {
		   if (dbConfig.getUrl().equals(tenantConfiguration.getDbUrl())) {
			   result.setDbConfiguration(dbConfig);
		   }
	   }
		
		result = tenantConfigurationRepository.save(result);
		return tenantConfigurationMapper.tenantConfigurationToTenantConfigurationDTO(result);
	}

	/**
	 * Get all the tenantConfigurations.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<TenantConfigurationDTO> findAll(Pageable pageable) {
		log.debug("Request to get all tenantConfigurations");
		Page<TenantConfiguration> result = tenantConfigurationRepository.findAll(pageable);

		return result.map(config -> tenantConfigurationMapper.tenantConfigurationToTenantConfigurationDTO(config));
	}

	/**
	 * Get one tenantConfiguration by id.
	 *
	 * @param tenant
	 *            the id of the entity
	 * @return the entity
	 */
	public TenantConfigurationDTO findOne(String tenant) {
		log.debug("Request to get tenantConfiguration : {}", tenant);
		TenantConfiguration tenantConfiguration = tenantConfigurationRepository.getTenantConfigurationForTenant(tenant);
		if (tenantConfiguration == null) {
			return null;
		}
		
		TenantConfigurationDTO tenantConfigurationDTO = tenantConfigurationMapper.tenantConfigurationToTenantConfigurationDTO(tenantConfiguration);
		
		DataSource ds = tenantDatasources.get(tenant);
		

    TenantConnectionStatusDTO 	status = TenantDataSourceBuilder.getConnectionStatus(tenantConfiguration);
		if (ds != null) {
			try {
				HikariDataSource hds = ds.unwrap(HikariDataSource.class);
				if (hds.getSchema().equals(tenantConfiguration.getSchema()) && 
				    hds.getJdbcUrl().equals(tenantConfigurationDTO.getDbUrl() )) {
					status.setUsedConnectionStatus(UsedConnectionStatus.CURRENT);
				} else {
					status.setUsedConnectionStatus(UsedConnectionStatus.OTHER);
				}
						
				
			} catch (SQLException e) {
				log.error("Couldn't get url and schema from currently used datasource for tenant {}", tenant);
			}
			
		} else {		
			status.setUsedConnectionStatus(UsedConnectionStatus.NONE);
		}
		tenantConfigurationDTO.setStatus(status);
		
		return tenantConfigurationDTO;
	}

	/**
	 * Delete the tenantConfiguration by id.
	 *
	 * @param tenant
	 *            the id of the entity
	 */
	public void delete(String tenant) {
		log.debug("Request to delete tenantConfiguration : {}", tenant);
		tenantConfigurationRepository.deleteForTenant(tenant);
		

	}
	
	/**
	 * Get all db-connections
	 * 
	 * @return A List with all db-connections
	 */
	public List<String> getAllConnections() {
		log.debug("Request to get all DB-Connections");
		List<String> result = new ArrayList<>();
		List<DBConfiguration> dbConfigs = tenantConfigurationRepository.getAllConnections();
		//hide username and password 
		for (DBConfiguration dbConfig : dbConfigs ) {
			result.add(dbConfig.getUrl());
		}
		
		return result;
	}
	
	/**
	 * Loads the the stored configuration (eg. json-file)
	 */
	public void updateMultitenancyConfiguration() throws ServiceValidationException {
		log.debug("Going to update MultitenancyConfiguration from stored config");
		try {
			tenantConfigurationRepository.initialize();
		}catch(PersistenceException e) {
			log.error("Got a persistence exception at loading configuration", e);
			throw new ServiceValidationException("walterApp.tenantConfiguration.configloadingError", new String[] {});
		}
		log.debug("MultitenancyConfiguration updated");
		
	}
	
	
	/**
	 * Setup the connection for a tenant
	 *  - checks the schema
	 *  - runs liquibase
	 *  - changes the DataSource for the tenant
	 *  
	 * @param tenantId id of the tenant to setup
	 */
	@Async
	public void setupDatasource(String tenantId) {
		log.debug("Request to update db-Connection for tenant {}", tenantId);
		DataSource ds = tenantDatasources.get(tenantId);
		TenantConfiguration tenantConfig = tenantConfigurationRepository.getTenantConfigurationForTenant(tenantId);
		if (tenantConfig == null) {
			log.debug("Couldnt' find a configuration");
			return;
		}
//		check if schema is available		
		boolean schemaGenerated = createSchema(tenantConfig);

		if (!schemaGenerated) {
			return;
		}
		
//		get DS for new schema
		DataSource tenantDs = TenantDataSourceBuilder.getDataSource(tenantConfig);
		
//		run liquibase
		log.debug("Going to run liquibase for Datasource");
		liquibase.runLiquibase(tenantDs, tenantConfig, true);

		
		if (ds != null) {
			log.debug("Changing current datasource for tenant: {}", tenantId );
//			change ds for tenant
			tenantDatasources.put(tenantId, tenantDs);
	
//		close old ds
			TenantDataSourceBuilder.closeDs(ds);
		}
		
		//colummn config init
		taskExecutor.execute(() -> {
			 TenantContext.setCurrentTenant(tenantId);
			 this.userColumnConfigGeneratorTask.run(); 
		});
		

	}

	/**
	 * Creates schema for tenant if it not exists
	 * 
	 * @param tenantConfig The tenant-configuration
	 * @return true if the schema is available or was created, false if this was not possible
	 */
	private boolean createSchema(TenantConfiguration tenantConfig) {
		boolean schemaGenerated;
		DataSource generalDs = TenantDataSourceBuilder.createDataSource(tenantConfig.getDbConfiguration());

		Connection genCon = null;
		try {
			log.debug("Going to create schema if it not exists: {} ", tenantConfig.getSchema());
			genCon = generalDs.getConnection();
			genCon.createStatement().executeUpdate("CREATE SCHEMA IF NOT EXISTS " + tenantConfig.getSchema());
      schemaGenerated = true;
		} catch (SQLException e) {
			// Schema couldn't be created
			log.error("Couldn't create schema {}", tenantConfig.getSchema());
			schemaGenerated = false;
		} finally {
			if (genCon != null) {
				try {
					genCon.close();
				} catch (SQLException e) {
					// do nothing }
				}
			}
			TenantDataSourceBuilder.closeDs(generalDs);
		
		}
		return schemaGenerated;
	}
}
