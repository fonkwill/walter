package at.simianarmy.multitenancy.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.service.dto.TenantConfigurationDTO;

@Mapper(componentModel = "spring", uses = {})
public interface TenantConfigurationMapper {

	@Mapping(target = "status", ignore = true)
	@Mapping(source = "dbConfiguration.url", target = "dbUrl")
	TenantConfigurationDTO tenantConfigurationToTenantConfigurationDTO(TenantConfiguration tenantConfiguration);
	
	List<TenantConfigurationDTO> tenantConfigurationToTenantConfigurationDTO(List<TenantConfiguration> tenantConfiguration);
	
	@Mapping(source = "dbUrl", target = "dbConfiguration.url")
  @Mapping(target = "jwtSecret", ignore = true)
	TenantConfiguration tenantConfigurationDTOToTenantConfiguration(TenantConfigurationDTO tenantConfigurationDTO);
	
	
	List<TenantConfiguration> tenantConfigurationDTOToTenantConfiguration(List<TenantConfigurationDTO> tenantConfigurationDTO);
}
