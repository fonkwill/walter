package at.simianarmy.multitenancy.service.dto;

import java.io.Serializable;

public class TenantConfigurationDTO implements Serializable{
	

	
    /**
	 * 
	 */
	private static final long serialVersionUID = 4187156317413616458L;

	private String tenantId;

	private String dbUrl;

	private String schema;

	private TenantConnectionStatusDTO status;
	


	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	
	

	public String getSchema() {
		return schema;
	}
	public void setSchema(String schema) {
		this.schema = schema;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	public TenantConnectionStatusDTO getStatus() {
		return status;
	}
	public void setStatus(TenantConnectionStatusDTO status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tenantId == null) ? 0 : tenantId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TenantConfigurationDTO other = (TenantConfigurationDTO) obj;
		if (tenantId == null) {
			if (other.tenantId != null)
				return false;
		} else if (!tenantId.equals(other.tenantId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TenantConfiguration [tenantId=" + tenantId + ", dbUrl=" + dbUrl + ", schema=" + schema + "]";
	}
	


	
	


}
