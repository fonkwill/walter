package at.simianarmy.multitenancy.service.dto;

public class TenantConnectionStatusDTO {

	
	public enum UsedConnectionStatus {
		CURRENT,
		NONE,
		OTHER
	}
	
	public enum ConfigurationConnectionStatus {
		OK,
		NOK
	}
	
	private UsedConnectionStatus usedConnectionStatus;

	private ConfigurationConnectionStatus configurationConnectionStatus;
	
	private String errorMessage;
	
	private String tag;
	

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public ConfigurationConnectionStatus getConfigurationConnectionStatus() {
		return configurationConnectionStatus;
	}

	public void setConfigurationConnectionStatus(ConfigurationConnectionStatus configurationConnectionStatus) {
		this.configurationConnectionStatus = configurationConnectionStatus;
	}

	public UsedConnectionStatus getUsedConnectionStatus() {
		return usedConnectionStatus;
	}

	public void setUsedConnectionStatus(UsedConnectionStatus usedConnectionStatus) {
		this.usedConnectionStatus = usedConnectionStatus;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
