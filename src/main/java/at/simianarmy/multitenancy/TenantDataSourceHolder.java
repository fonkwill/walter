package at.simianarmy.multitenancy;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.stereotype.Component;

@Component
public class TenantDataSourceHolder {

	
	private Map<String, DataSource> tenantDatasources = new HashMap<>();

	public DataSource get(String key) {
		return tenantDatasources.get(key);
	}
	
	public DataSource remove(String key) {
		return tenantDatasources.get(key);
	}

	public DataSource put(String key, DataSource value) {
		return tenantDatasources.put(key, value);
	}
}
