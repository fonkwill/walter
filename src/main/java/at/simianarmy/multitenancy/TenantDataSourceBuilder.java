package at.simianarmy.multitenancy;

import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO;
import at.simianarmy.multitenancy.service.dto.TenantConnectionStatusDTO.ConfigurationConnectionStatus;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import javax.sql.DataSource;
import liquibase.changelog.ChangeLogHistoryServiceFactory;
import liquibase.changelog.RanChangeSet;
import liquibase.changelog.StandardChangeLogHistoryService;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jdbc.DataSourceBuilder;

public class TenantDataSourceBuilder {
	
	private static Logger logger = LoggerFactory.getLogger(TenantDataSourceBuilder.class);

	public static DataSource getDataSource(TenantConfiguration tenantConfig) {
		DataSource result = null;
		DBConfiguration dbConfiguration = tenantConfig.getDbConfiguration();
		if (dbConfiguration != null) {
			result = createDataSource(dbConfiguration);
			
			try {
				result.unwrap(HikariDataSource.class).setSchema(tenantConfig.getSchema());
			} catch (SQLException e) {
				logger.error("Couldn't set schema", e);
			}
		}
		return result;
	}

	public static DataSource createDataSource(DBConfiguration dbConfiguration) {
		DataSource result;
		result = DataSourceBuilder.create()
				.url(dbConfiguration.getUrl())
				.type(com.zaxxer.hikari.HikariDataSource.class)
				.username(dbConfiguration.getUsername())
				.password(dbConfiguration.getPassword())
				.build();
		return result;
	}

	public static void closeDs(DataSource ds) {
		try {
			ds.unwrap(HikariDataSource.class).close();
		} catch (SQLException e) {
			logger.error("Couldn't close DataSource");
		} 		
	}

	public static TenantConnectionStatusDTO getConnectionStatus(TenantConfiguration dbConfiguration) {
		TenantConnectionStatusDTO connectionStatus = new TenantConnectionStatusDTO();
		connectionStatus.setConfigurationConnectionStatus(ConfigurationConnectionStatus.NOK);
		DataSource ds = getDataSource(dbConfiguration);
	    if (ds ==  null) {
	    	logger.debug("Could not create a dataSource");
	    	connectionStatus.setErrorMessage("walterApp.tenantConfiguration.error.noDataSource");
	    	return connectionStatus;
	    }
	    
	    Database database;
		boolean isAvailable = false;
		Connection connection = null;
		try {
			connection = ds.getConnection();
			database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
			
			StandardChangeLogHistoryService service = ((StandardChangeLogHistoryService) ChangeLogHistoryServiceFactory.getInstance().getChangeLogService(database));
			isAvailable = service.hasDatabaseChangeLogTable();
			if (isAvailable) {
				logger.debug("Liquibase was executed");
				connectionStatus.setConfigurationConnectionStatus(ConfigurationConnectionStatus.OK);
				
				RanChangeSet last = null;
				Date lastOrder = null;
				for(RanChangeSet rcs : service.getRanChangeSets()) {
					if (last == null) {
						last = rcs;
						lastOrder = rcs.getDateExecuted();
					}
					if (rcs.getDateExecuted().after(lastOrder)) {
						last = rcs;
						lastOrder = rcs.getDateExecuted();
					}
				};
				if (last.getTag() != null) {
					logger.debug("Got Tag for last change");
					connectionStatus.setTag(last.getTag());
				}
				
				
			} else {
				logger.debug("Liquibase wasn't executed");
				connectionStatus.setErrorMessage("walterApp.tenantConfiguration.error.databaseNotInitialized");
			}
		} catch (DatabaseException | SQLException e) {
			connectionStatus.setConfigurationConnectionStatus(ConfigurationConnectionStatus.NOK);
			logger.error("Error at getting configuration connection status");
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				logger.error("error at closing connection", e);
			}
			closeDs(ds);
		}
		
		return connectionStatus;
	}

}
