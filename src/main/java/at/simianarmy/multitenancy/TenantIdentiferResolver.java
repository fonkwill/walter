package at.simianarmy.multitenancy;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TenantIdentiferResolver implements CurrentTenantIdentifierResolver {

	public static String MASTER_TENANT = "MASTER_TENANT";


	@Override
	public String resolveCurrentTenantIdentifier() {

		String tenantId = TenantContext.getCurrentTenant();
		if (tenantId != null) {
			return tenantId;
		}
		return MASTER_TENANT;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}

}
