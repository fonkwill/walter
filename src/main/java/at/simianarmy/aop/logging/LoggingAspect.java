package at.simianarmy.aop.logging;

import at.simianarmy.aop.logging.annotation.NoLogging;
import at.simianarmy.config.Constants;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

/**
 * Aspect for logging execution of service and repository Spring components.
 */
@Aspect
public class LoggingAspect {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Inject
	private Environment env;

	@Pointcut("within(at.simianarmy.repository..*) || " + "within(at.simianarmy.service..*) || "
			+ "within(at.simianarmy.web.rest..*)")
	public void loggingPointcut() {
	}

	@AfterThrowing(pointcut = "loggingPointcut()", throwing = "e")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
		if (env.acceptsProfiles(Constants.SPRING_PROFILE_DEVELOPMENT)) {
			log.error("Exception in {}.{}() with cause = \'{}\' and exception = \'{}\'",
					joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(),
					e.getCause() != null ? e.getCause() : "NULL", e.getMessage(), e);

		} else {
			log.error("Exception in {}.{}() with cause = {}", joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName(), e.getCause() != null ? e.getCause() : "NULL");
		}
	}

	@Around("loggingPointcut()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		if (log.isDebugEnabled()) {

			ArrayList<Object> arguments = new ArrayList<Object>();

			MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
			String methodName = methodSignature.getMethod().getName();
			Class<?>[] parameterTypes = methodSignature.getMethod().getParameterTypes();

			Parameter[] parameters = joinPoint.getTarget().getClass().getMethod(methodName, parameterTypes)
					.getParameters();

			for (int i = 0; i < parameters.length; i++) {
				Parameter param = parameters[i];
				boolean printParameter = true;

				for (Annotation annotation : param.getAnnotations()) {
					if (annotation.annotationType().equals(NoLogging.class)) {
						printParameter = false;
						break;
					}
				}

				if (printParameter) {
					arguments.add(joinPoint.getArgs()[i]);
				}
			}

			log.debug("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
					methodName, Arrays.toString(arguments.toArray()));
		}
		try {
			Object result = joinPoint.proceed();
			if (log.isDebugEnabled()) {
				log.debug("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
						joinPoint.getSignature().getName(), result);
			}
			return result;
		} catch (IllegalArgumentException e) {
			log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
					joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());

			throw e;
		}
	}
}
