package at.simianarmy.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.common.net.MediaType;

import at.simianarmy.WalterApp;
import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.web.rest.errors.CustomParameterizedException;
import at.simianarmy.web.rest.errors.ErrorVM;
import at.simianarmy.web.rest.errors.ExceptionTranslator;
import at.simianarmy.web.rest.errors.ParameterizedErrorVM;

@WebFilter(asyncSupported = true, urlPatterns = {"/api/*", "/that/*"})
public class TenantFilter implements Filter {

	private static final String TENANT_HEADER = "X-TenantID";
	private static final String TENANT_PARAM = "tenant";
	
	//@Value("${walter.multitenancy.enabled:false}")
	private Boolean multitenancy;
	
	public TenantFilter(WalterProperties props) {
		multitenancy = props.getMultitenancy().isEnabled();
	}
	
//	public TenantFilter(Boolean multitenancy) {
//		this.multitenancy = multitenancy;
//	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

	      if (multitenancy) {
          HttpServletRequest request = (HttpServletRequest) servletRequest;
          String tenantHeader = request.getHeader(TENANT_HEADER);
          //check parameter
          if (!StringUtils.hasText(tenantHeader)) {
            String tenantParam = request.getParameter(TENANT_PARAM);
            tenantHeader = tenantParam;
          }

          if (StringUtils.hasText(tenantHeader)) {
            TenantContext.setCurrentTenant(tenantHeader);
          } else {
            TenantContext.clear();
            if (multitenancy) {

              //CustomParameterizedException ex =  new CustomParameterizedException("error tenancy", new String[] {} );
              // ParameterizedErrorVM eVM = exTranslator.processParameterizedValidationError(ex);
              //  ((HttpServletResponse) servletResponse).sendError(HttpServletResponse.SC_NOT_FOUND, new ObjectMapper().writeValueAsString(eVM));
              // return;
            }

          }
        }
		    filterChain.doFilter(servletRequest, servletResponse);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
	

}
