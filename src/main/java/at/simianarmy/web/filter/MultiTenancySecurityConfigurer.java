package at.simianarmy.web.filter;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.security.jwt.JWTFilter;

public class MultiTenancySecurityConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	private WalterProperties walterProperties;
	
	public MultiTenancySecurityConfigurer(WalterProperties walterProperties) {
		this.walterProperties = walterProperties;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		TenantFilter customFilter = new TenantFilter(walterProperties);
		http.addFilterBefore(customFilter, JWTFilter.class);
	}
}
