package at.simianarmy.web.rest.errors;

import java.util.List;
import java.util.Set;

import at.simianarmy.domain.Person;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.ServiceValidationException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;

/**
 * Controller advice to translate the server side exceptions to client-friendly
 * json structures.
 */
@ControllerAdvice
public class ExceptionTranslator {

	@ExceptionHandler(ConcurrencyFailureException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
	public ErrorVM processConcurencyError(ConcurrencyFailureException ex) {
		return new ErrorVM(ErrorConstants.ERR_CONCURRENCY_FAILURE);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorVM processValidationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();

		return processFieldErrors(fieldErrors);
	}

	@ExceptionHandler(CustomParameterizedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ParameterizedErrorVM processParameterizedValidationError(CustomParameterizedException ex) {
		return ex.getErrorVM();
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorVM processAccessDeniedException(AccessDeniedException ex) {
		return new ErrorVM(ErrorConstants.ERR_ACCESS_DENIED, ex.getMessage());
	}

	private ErrorVM processFieldErrors(List<FieldError> fieldErrors) {
		ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);

		for (FieldError fieldError : fieldErrors) {
			dto.add(fieldError.getObjectName(), fieldError.getField(), fieldError.getCode());
		}

		return dto;
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public ErrorVM processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
		return new ErrorVM(ErrorConstants.ERR_METHOD_NOT_SUPPORTED, exception.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorVM> processRuntimeException(Exception ex) {
		BodyBuilder builder;
		ErrorVM errorVM;
		ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
		if (responseStatus != null) {
			builder = ResponseEntity.status(responseStatus.value());
			errorVM = new ErrorVM("error." + responseStatus.value().value(), responseStatus.reason());
		} else {
			builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
			errorVM = new ErrorVM(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, "Internal server error");
		}
		return builder.body(errorVM);
	}

	@ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorVM processValidationError(ConstraintViolationException ex) {

    ErrorVM dto = new ErrorVM(ServiceErrorConstants.constraintViolationException);

    Set<ConstraintViolation<?>> constraintViolationSet = ex.getConstraintViolations();
    for (ConstraintViolation constraintViolation : constraintViolationSet) {
      if (constraintViolation.getRootBeanClass().equals(Person.class)) {
        dto.add("person", constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
      }
    }
    return dto;

  }
}
