package at.simianarmy.web.rest;

import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.service.ReceiptFileStoreService;
import at.simianarmy.service.ReceiptService;
import at.simianarmy.service.dto.ReceiptDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * REST controller for managing Receipt.
 */
@RestController
@RequestMapping("/api")
public class ReceiptResource {

	private final Logger log = LoggerFactory.getLogger(ReceiptResource.class);

	@Inject
	private ReceiptService receiptService;

	@Inject
	private ReceiptFileStoreService fileStore;

	/**
	 * POST /receipts : Create a new receipt.
	 *
	 * @param uploadedFile
	 *            the newly uploaded file
	 * @param id
	 *            the receipt id
	 * @param identifier
	 *            the receipt identifier
	 * @param title
	 *            the title
	 * @param cashbookEntryId
	 *            the cashbook entry id
	 * @param date
	 *            the date
	 * @param note
	 *            the note
	 * @param companyId
	 *            the id of the company
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         receiptDTO, or with status 400 (Bad Request) if the receipt has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/receipts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReceiptDTO> createReceipt(
			@RequestParam(value = "uploadFile", required = false) MultipartFile uploadedFile,
			@RequestParam(value = "receipt[id]", required = false) Long id,
			@RequestParam(value = "receipt[identifier]", required = true) String identifier,
			@RequestParam(value = "receipt[overwriteIdentifier]", required = true) Boolean overwriteIdentifier,
			@RequestParam(value = "receipt[title]", required = true) String title,
			@RequestParam(value = "receipt[cashbookEntryId]", required = true) Long cashbookEntryId,
			@RequestParam(value = "receipt[date]", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
			@RequestParam(value = "receipt[note]", required = false) String note,
			@RequestParam(value = "receipt[companyId]", required = false) Long companyId) throws URISyntaxException {
    ReceiptDTO receiptDTO = createReceiptDTO(uploadedFile, id, identifier, overwriteIdentifier, title, cashbookEntryId, date, note, companyId);

    log.debug("REST request to save Receipt : {}", receiptDTO);
		if (receiptDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("receipt", "idexists", "A new receipt cannot already have an ID"))
					.body(null);
		}

		ReceiptDTO result = receiptService.save(receiptDTO);

		return ResponseEntity.created(new URI("/api/receipts/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("receipt", result.getId().toString())).body(result);
	}

  private ReceiptDTO createReceiptDTO( MultipartFile uploadedFile, Long id, String identifier,Boolean overwriteIdentifier, String title,  Long cashbookEntryId, LocalDate date, String note,  Long companyId) {
    ReceiptDTO receiptDTO = new ReceiptDTO();
    receiptDTO.setId(id);
    receiptDTO.setIdentifier(identifier);
    receiptDTO.setOverwriteIdentifier(overwriteIdentifier);
    receiptDTO.setTitle(title);
    receiptDTO.setDate(date);
    receiptDTO.setNote(note);
    receiptDTO.setCashbookEntryId(cashbookEntryId);
    receiptDTO.setCompanyId(companyId);
    receiptDTO.setUploadedFile(uploadedFile);
    return receiptDTO;
  }

  /**
	 * PUT /receipts : Updates an existing receipt.
	 *
	 * @param uploadedFile
	 *            the newly uploaded file
	 * @param id
	 *            the id
	 * @param title
	 *            the title
	 * @param identifier
	 *            the identifier
	 * @param date
	 *            the date
	 * @param note
	 *            the note
	 * @param companyId
	 *            the id of the company
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         receiptDTO, or with status 400 (Bad Request) if the receiptDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the receiptDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/receipts/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReceiptDTO> updateReceipt(
			@RequestParam(value = "uploadFile", required = false) MultipartFile uploadedFile, @PathVariable Long id,
			@RequestParam(value = "receipt[title]", required = true) String title,
			@RequestParam(value = "receipt[identifier]", required = true) String identifier,
			@RequestParam(value = "receipt[overwriteIdentifier]", required = true) Boolean overwriteIdentifier,
			@RequestParam(value = "receipt[date]", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
			@RequestParam(value = "receipt[note]", required = false) String note,
			@RequestParam(value = "receipt[companyId]", required = false) Long companyId) throws URISyntaxException {


	  ReceiptDTO receiptDTO = createReceiptDTO(uploadedFile, id, identifier, overwriteIdentifier, title, null, date, note ,  companyId);

		log.debug("REST request to update Receipt : {}", receiptDTO);
		if (receiptDTO.getId() == null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("receipt", "idunavailable", "A existing receipt must have an ID"))
					.body(null);
		}
		ReceiptDTO result = receiptService.save(receiptDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("receipt", receiptDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /receipts : get all the receipts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of receipts in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/receipts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ReceiptDTO>> getAllReceipts(
			@RequestParam(value = "sword", required = false) String sword, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Receipts");
		Page<ReceiptDTO> page = receiptService.findAll(sword, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/receipts");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /receipts/:id : get the "id" receipt.
	 *
	 * @param id
	 *            the id of the receiptDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the receiptDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/receipts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReceiptDTO> getReceipt(@PathVariable Long id) {
		log.debug("REST request to get Receipt : {}", id);
		ReceiptDTO receiptDTO = receiptService.findOne(id);
		return Optional.ofNullable(receiptDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /receipts/:id : delete the "id" receipt.
	 *
	 * @param id
	 *            the id of the receiptDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/receipts/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteReceipt(@PathVariable Long id) {
		log.debug("REST request to delete Receipt : {}", id);
		receiptService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("receipt", id.toString())).build();
	}

	/**
	 * returns the contents of a file.
	 *
	 * @param uuid
	 *            the uuid of the file
	 * @return the ResponseEntity
	 */
	@GetMapping("/receipts/file/{uuid:.+}")
	@Timed
	public ResponseEntity<Resource> getResource(@PathVariable String uuid) {
		try {
			FileResourceDTO file = fileStore.getReceipt(UUID.fromString(uuid));
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename=\"" + file.getFileName() + "\"")
					.body(file.getResource());
		} catch (FileStoreException exception) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * links a cashbook entry to a receipt.
	 *
	 * @param receiptDTO
	 *            the receipt data transfer object
	 * @return the ResponseEntity
	 * @throws URISyntaxException
	 *             if there is an error
	 */
	@RequestMapping(value = "/receipts/linktocashbookentry", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReceiptDTO> linkToCashbookEntry(@RequestBody ReceiptDTO receiptDTO)
			throws URISyntaxException {
		log.debug("REST request to link Receipt {} to Cashbook Entry {}", receiptDTO.getId(),
				receiptDTO.getCashbookEntryId());
		receiptService.linkToCashbookEntry(receiptDTO);
		return ResponseEntity.ok().body(null);
	}

	/**
	 * unlinks a cashbook entry from a receipt.
	 *
	 * @param receiptDTO
	 *            the receipt data transfer object
	 * @return the ReponseEntity
	 * @throws URISyntaxException
	 *             if there is an error
	 */
	@RequestMapping(value = "/receipts/unlinkcashbookentry", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReceiptDTO> unlinkCashbookEntry(@RequestBody ReceiptDTO receiptDTO)
			throws URISyntaxException {
		log.debug("REST request to link Receipt {} to Cashbook Entry {}", receiptDTO.getId(),
				receiptDTO.getCashbookEntryId());
		receiptService.unlinkCashbookEntry(receiptDTO);
		return ResponseEntity.ok().body(null);
	}

}
