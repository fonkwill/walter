package at.simianarmy.web.rest;

import at.simianarmy.service.NotificationService;
import at.simianarmy.service.dto.NotificationDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.web.rest.util.HeaderUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Notification.
 */
@RestController
@RequestMapping("/api")
public class NotificationResource {

	private final Logger log = LoggerFactory.getLogger(NotificationResource.class);

	@Inject
	private NotificationService notificationService;

	/**
	 * Mark a single notification as read by the current user.
	 * 
	 * @param id
	 *            the notification id
	 * @return the ReponseEntity
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/notifications/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> readNotification(@PathVariable Long id) throws URISyntaxException {
		log.debug("REST request to read Notification with ID : {}", id);
		try {
			notificationService.markNotificationAsRead(id);
			return ResponseEntity.ok().build();
		} catch (ServiceValidationException exception) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Returns two numbers. The first one is the total count, the second the count
	 * of the unread notifications.
	 * 
	 * @return the ReponseEntity
	 */
	@RequestMapping(value = "/notifications/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<Long>> getNotificationCount() {
		log.debug("REST request to get a notification count");

		Long unreadCount = notificationService.getUnreadCount();
		Long totalCount = notificationService.getTotalCount();

		List<Long> list = new ArrayList<Long>();
		list.add(totalCount);
		list.add(unreadCount);

		return ResponseEntity.ok().body(list);
	}

	/**
	 * GET /notifications : get all the notifications.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of notifications
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/notifications", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<NotificationDTO>> getAllNotifications() throws URISyntaxException {
		log.debug("REST request to get a page of Notifications");
		List<NotificationDTO> list = notificationService.findAll();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	/**
	 * DELETE /notifications/:id : delete the "id" notification.
	 *
	 * @param id
	 *            the id of the notificationDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/notifications/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteNotification(@PathVariable Long id) {
		log.debug("REST request to delete Notification : {}", id);
		notificationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("notification", id.toString())).build();
	}

}
