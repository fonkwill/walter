package at.simianarmy.web.rest;

import at.simianarmy.service.PersonHonorService;
import at.simianarmy.service.dto.PersonHonorDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing PersonHonor.
 */
@RestController
@RequestMapping("/api")
public class


PersonHonorResource {

	private final Logger log = LoggerFactory.getLogger(PersonHonorResource.class);

	@Inject
	private PersonHonorService personHonorService;

	/**
	 * POST /person-honors : Create a new personHonor.
	 *
	 * @param personHonorDTO
	 *            the personHonorDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personHonorDTO, or with status 400 (Bad Request) if the personHonor
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-honors", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonHonorDTO> createPersonHonor(@Valid @RequestBody PersonHonorDTO personHonorDTO)
			throws URISyntaxException {
		log.debug("REST request to save PersonHonor : {}", personHonorDTO);
		if (personHonorDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("personHonor", "idexists",
					"A new personHonor cannot already have an ID")).body(null);
		}
		PersonHonorDTO result = personHonorService.save(personHonorDTO);
		return ResponseEntity.created(new URI("/api/person-honors/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("personHonor", result.getId().toString())).body(result);
	}

	/**
	 * PUT /person-honors : Updates an existing personHonor.
	 *
	 * @param personHonorDTO
	 *            the personHonorDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personHonorDTO, or with status 400 (Bad Request) if the
	 *         personHonorDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the personHonorDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-honors", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonHonorDTO> updatePersonHonor(@Valid @RequestBody PersonHonorDTO personHonorDTO)
			throws URISyntaxException {
		log.debug("REST request to update PersonHonor : {}", personHonorDTO);
		if (personHonorDTO.getId() == null) {
			return createPersonHonor(personHonorDTO);
		}
		PersonHonorDTO result = personHonorService.save(personHonorDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("personHonor", personHonorDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /person-honors : get all the personHonors.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of personHonors
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/person-honors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonHonorDTO>> getAllPersonHonors(
			@RequestParam(name = "personId", required = false) Long personId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of PersonHonors");
		Page<PersonHonorDTO> page = personHonorService.findAll(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-honors");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /person-honors/:id : get the "id" personHonor.
	 *
	 * @param id
	 *            the id of the personHonorDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         personHonorDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/person-honors/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonHonorDTO> getPersonHonor(@PathVariable Long id) {
		log.debug("REST request to get PersonHonor : {}", id);
		PersonHonorDTO personHonorDTO = personHonorService.findOne(id);
		return Optional.ofNullable(personHonorDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /person-honors/:id : delete the "id" personHonor.
	 *
	 * @param id
	 *            the id of the personHonorDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/person-honors/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePersonHonor(@PathVariable Long id) {
		log.debug("REST request to delete PersonHonor : {}", id);
		personHonorService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personHonor", id.toString())).build();
	}

}
