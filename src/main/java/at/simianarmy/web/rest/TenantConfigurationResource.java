package at.simianarmy.web.rest;

import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.multitenancy.service.TenantConfigurationService;
import at.simianarmy.multitenancy.service.dto.TenantConfigurationDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing TenantConfiguration.
 */
@RestController
@RequestMapping("/api")
@ConditionalOnProperty(name="walter.multitenancy.enabled", havingValue="true", matchIfMissing=false)
public class TenantConfigurationResource {

	private final Logger log = LoggerFactory.getLogger(TenantConfigurationResource.class);

	@Inject
	private TenantConfigurationService tenantConfigurationService;

	@Inject
	private TenantIdentiferResolver tenantIdentiferResolver;
	
	
	/**
	 * POST /tenant-configurations : Create a new tenantConfiguration.
	 *
	 * @param tenantConfiguration the tenantConfiguration to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         tenantConfiguration, or with status 400 (Bad Request) if the
	 *         tenantConfiguration has already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/tenant-configurations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TenantConfigurationDTO> createTenantConfiguration(
			@Valid @RequestBody TenantConfigurationDTO tenantConfiguration) throws URISyntaxException {
	
		log.debug("REST request to save tenantConfiguration : {}", tenantConfiguration);
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);

    TenantConfigurationDTO savedTenantConfiguration = tenantConfigurationService
				.findOne(tenantConfiguration.getTenantId());
		if (savedTenantConfiguration != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "idexists",
					"A configuration for this tenant exists already")).body(null);
		}

		TenantConfigurationDTO result = tenantConfigurationService.save(tenantConfiguration);
		return ResponseEntity.created(new URI("/api/tenantConfigurations/" + result.getTenantId()))
				.headers(HeaderUtil.createEntityCreationAlert("tenantConfiguration", result.getTenantId().toString()))
				.body(result);
	}

	/**
	 * PUT /tenant-configurations : Updates an existing tenantConfigurations.
	 *
	 * @param tenantConfiguration the tenantConfigurations to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         tenantConfigurations, or with status 400 (Bad Request) if the
	 *         tenantConfigurations is not valid, or with status 500 (Internal
	 *         Server Error) if the tenantConfigurations couldnt be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/tenant-configurations", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TenantConfigurationDTO> updateTenantConfiguration(
			@Valid @RequestBody TenantConfigurationDTO tenantConfiguration) throws URISyntaxException {
		log.debug("REST request to update TenantConfiguration : {}", tenantConfiguration);
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);
    TenantConfigurationDTO savedTenantConfiguration = tenantConfigurationService
				.findOne(tenantConfiguration.getTenantId());
		if (savedTenantConfiguration == null) {
			return createTenantConfiguration(tenantConfiguration);
		}
		TenantConfigurationDTO result = tenantConfigurationService.save(tenantConfiguration);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("tenantConfiguration", result.getTenantId())).body(result);
	}

  private boolean isMasterTenant() {
    if (!tenantIdentiferResolver.resolveCurrentTenantIdentifier().equals(TenantIdentiferResolver.MASTER_TENANT)) {
      log.debug("Tried to change master-tenant-configuration from other tenant");
      return true;
    }
    return false;
  }

  /**
	 * GET /tenant-configurations : get all the tenantConfigurations.
	 *
	 * @param pageable the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         tenantConfigurations in body
	 * @throws URISyntaxException if there is an error to generate the pagination
	 *                            HTTP headers
	 */
	@RequestMapping(value = "/tenant-configurations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<TenantConfigurationDTO>> getAllTenantConfigurations(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of tenantConfigurations");
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);

    Page<TenantConfigurationDTO> page = tenantConfigurationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tenantConfigurations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /tenant-configurations/:id : get the "id" tenantConfiguration.
	 *
	 * @param id the id of the tenantConfiguration to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         tenantConfiguration, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/tenant-configurations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TenantConfigurationDTO> getTenantConfiguration(@PathVariable String id) {
		log.debug("REST request to get tenantConfigurations : {}", id);
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);
    TenantConfigurationDTO tenantConfiguration = tenantConfigurationService.findOne(id);
		return Optional.ofNullable(tenantConfiguration).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /tenant-configurations/:id : delete the "id" tenantConfiguration.
	 *
	 * @param id the id of the tenantConfiguration to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/tenant-configurations/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteTenantConfiguration(@PathVariable String id) {
		log.debug("REST request to delete TenantConfiguration : {}", id);
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);
    tenantConfigurationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tenantConfiguration", id.toString()))
				.build();
	}
	
	/**
	 * GET /db-connections : get all the tenantConfigurations.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         tenantConfigurations in body
	 * @throws URISyntaxException if there is an error to generate the pagination
	 *                            HTTP headers
	 */
	@RequestMapping(value = "tenant-configurations/db-connections", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<String>> getAllDBConnections()
			throws URISyntaxException {
		log.debug("REST request to get a list of all db-connections");
		//only for the master-tenant
    if (isMasterTenant())
      return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
        "Master-tenant-configuration is not possible from this tenant")).body(null);

    List<String> result = tenantConfigurationService.getAllConnections();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	/**
	 * PUT /tenant-configurations/config : Updates the multitenancy configuration with the stored config.
	 *
	 * @return Status 202 (Accepted)
	 * 		  The processing is done afterwards
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/tenant-configurations/config", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> updateMultiTenancyConfiguration() throws URISyntaxException {
		log.debug("REST request to update multitenancy configuration");
		//only for the master-tenant
    if (isMasterTenant()) return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("tenantConfiguration", "notmastertenant",
      "Master-tenant-configuration is not possible from this tenant")).build();
		tenantConfigurationService.updateMultitenancyConfiguration();

		return ResponseEntity.noContent().headers(HeaderUtil.createAlert("walterApp.tenantConfiguration.configLoaded", null)).build();

	}
	
	/**
	 * PUT /tenant-configurations/db-connections/:id : Updates the db-connection for the given tenant.
	 *
	 * @param id with the tenant id to update the db-connection
	 * @return Status 202 (Accepted)
	 * 		  The processing is done afterwards
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/tenant-configurations/db-connections", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> updateTenantDbConnection(
			@RequestBody String id) throws URISyntaxException {
		log.debug("REST request to update db-Connection of tenant : {}", id);
		//only for the master-tenant
    if (isMasterTenant()) return ResponseEntity.badRequest().build();
    if (id != null) {
			tenantConfigurationService.setupDatasource(id);
			return ResponseEntity.accepted().headers(HeaderUtil.createAlert("walterApp.tenantConfiguration.updateConnection", null)).build();
		}
		return ResponseEntity.notFound().build();
	
	}

}
