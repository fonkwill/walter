package at.simianarmy.web.rest;

import at.simianarmy.service.CashbookCategoryService;
import at.simianarmy.service.dto.CashbookCategoryDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing CashbookCategory.
 */
@RestController
@RequestMapping("/api")
public class CashbookCategoryResource {

	private final Logger log = LoggerFactory.getLogger(CashbookCategoryResource.class);

	@Inject
	private CashbookCategoryService cashbookCategoryService;

	/**
	 * POST /cashbook-categories : Create a new cashbookCategory.
	 *
	 * @param cashbookCategoryDTO
	 *            the cashbookCategoryDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         cashbookCategoryDTO, or with status 400 (Bad Request) if the
	 *         cashbookCategory has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/cashbook-categories", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<CashbookCategoryDTO> createCashbookCategory(
			@Valid @RequestBody CashbookCategoryDTO cashbookCategoryDTO) throws URISyntaxException {
		log.debug("REST request to save CashbookCategory : {}", cashbookCategoryDTO);
		if (cashbookCategoryDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("cashbookCategory", "idexists",
					"A new cashbookCategory cannot already have an ID")).body(null);
		}
		CashbookCategoryDTO result = cashbookCategoryService.save(cashbookCategoryDTO);
		return ResponseEntity.created(new URI("/api/cashbook-categories/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("cashbookCategory", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /cashbook-categories : Updates an existing cashbookCategory.
	 *
	 * @param cashbookCategoryDTO
	 *            the cashbookCategoryDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         cashbookCategoryDTO, or with status 400 (Bad Request) if the
	 *         cashbookCategoryDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the cashbookCategoryDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/cashbook-categories", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<CashbookCategoryDTO> updateCashbookCategory(
			@Valid @RequestBody CashbookCategoryDTO cashbookCategoryDTO) throws URISyntaxException {
		log.debug("REST request to update CashbookCategory : {}", cashbookCategoryDTO);
		if (cashbookCategoryDTO.getId() == null) {
			return createCashbookCategory(cashbookCategoryDTO);
		}
		CashbookCategoryDTO result = cashbookCategoryService.save(cashbookCategoryDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("cashbookCategory", cashbookCategoryDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /cashbook-categories : get all the cashbookCategories.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         cashbookCategories in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/cashbook-categories", "/customizations/cashbook-categories"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<CashbookCategoryDTO>> getAllCashbookCategories(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of CashbookCategories");
		Page<CashbookCategoryDTO> page = cashbookCategoryService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cashbook-categories");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /cashbook-categories/:id : get the "id" cashbookCategory.
	 *
	 * @param id
	 *            the id of the cashbookCategoryDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         cashbookCategoryDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/cashbook-categories/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<CashbookCategoryDTO> getCashbookCategory(@PathVariable Long id) {
		log.debug("REST request to get CashbookCategory : {}", id);
		CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryService.findOne(id);
		return Optional.ofNullable(cashbookCategoryDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /cashbook-categories/:id : delete the "id" cashbookCategory.
	 *
	 * @param id
	 *            the id of the cashbookCategoryDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/cashbook-categories/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteCashbookCategory(@PathVariable Long id) {
		log.debug("REST request to delete CashbookCategory : {}", id);
		cashbookCategoryService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cashbookCategory", id.toString()))
				.build();
	}

}
