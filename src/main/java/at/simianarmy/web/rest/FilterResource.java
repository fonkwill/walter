package at.simianarmy.web.rest;

import at.simianarmy.service.FilterService;
import at.simianarmy.service.dto.FilterDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Filter.
 */
@RestController
@RequestMapping("/api")
public class FilterResource {

    private final Logger log = LoggerFactory.getLogger(FilterResource.class);

    private static final String ENTITY_NAME = "filter";

    private final FilterService filterService;

    public FilterResource(FilterService filterService) {
        this.filterService = filterService;
    }

    /**
     * POST  /filters : Create a new filter.
     *
     * @param filterDTO the filterDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new filterDTO, or with status 400 (Bad Request) if the filter has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/filter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FilterDTO> createFilter(@Valid @RequestBody FilterDTO filterDTO) throws URISyntaxException {
        log.debug("REST request to save Filter : {}", filterDTO);
        if (filterDTO.getId() != null) {
          return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
            "A new filter cannot already have an ID")).body(null);
        }
        FilterDTO result = filterService.save(filterDTO);
        return ResponseEntity.created(new URI("/api/filters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /filters : Updates an existing filter.
     *
     * @param filterDTO the filterDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated filterDTO,
     * or with status 400 (Bad Request) if the filterDTO is not valid,
     * or with status 500 (Internal Server Error) if the filterDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/filter", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FilterDTO> updateFilter(@Valid @RequestBody FilterDTO filterDTO) throws URISyntaxException {
        log.debug("REST request to update Filter : {}", filterDTO);
        if (filterDTO.getId() == null) {
          return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idnull",
            "Invalid id")).body(null);
        }
        FilterDTO result = filterService.save(filterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, filterDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /filters : get all the filters.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of filters in body
     */
    @RequestMapping(value = "/filter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<FilterDTO> getAllFiltersByListName(
      @RequestParam(name = "listName", required = true) String listName
    ) {
        log.debug("REST request to get all Filters by listName {}", listName);
        return filterService.findAllByListName(listName);
    }

    /**
     * GET  /filters/:id : get the "id" filter.
     *
     * @param id the id of the filterDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the filterDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/filter/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<FilterDTO> getFilter(@PathVariable Long id) {
        log.debug("REST request to get Filter : {}", id);
        FilterDTO filterDTO = filterService.findOne(id);

        return Optional.ofNullable(filterDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
          .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /filters/:id : delete the "id" filter.
     *
     * @param id the id of the filterDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/filter/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteFilter(@PathVariable Long id) {
        log.debug("REST request to delete Filter : {}", id);
        filterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
