package at.simianarmy.web.rest;

import at.simianarmy.service.NotificationRuleService;
import at.simianarmy.service.dto.NotificationRuleDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing NotificationRule.
 */
@RestController
@RequestMapping("/api")
public class NotificationRuleResource {

	private final Logger log = LoggerFactory.getLogger(NotificationRuleResource.class);

	@Inject
	private NotificationRuleService notificationRuleService;

	/**
	 * POST /notification-rules : Create a new notificationRule.
	 *
	 * @param notificationRuleDTO
	 *            the notificationRuleDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         notificationRuleDTO, or with status 400 (Bad Request) if the
	 *         notificationRule has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/notification-rules", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<NotificationRuleDTO> createNotificationRule(
			@Valid @RequestBody NotificationRuleDTO notificationRuleDTO) throws URISyntaxException {
		log.debug("REST request to save NotificationRule : {}", notificationRuleDTO);
		if (notificationRuleDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("notificationRule", "idexists",
					"A new notificationRule cannot already have an ID")).body(null);
		}
		NotificationRuleDTO result = notificationRuleService.save(notificationRuleDTO);
		return ResponseEntity.created(new URI("/api/notification-rules/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("notificationRule", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /notification-rules : Updates an existing notificationRule.
	 *
	 * @param notificationRuleDTO
	 *            the notificationRuleDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         notificationRuleDTO, or with status 400 (Bad Request) if the
	 *         notificationRuleDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the notificationRuleDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/notification-rules", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<NotificationRuleDTO> updateNotificationRule(
			@Valid @RequestBody NotificationRuleDTO notificationRuleDTO) throws URISyntaxException {
		log.debug("REST request to update NotificationRule : {}", notificationRuleDTO);
		if (notificationRuleDTO.getId() == null) {
			return createNotificationRule(notificationRuleDTO);
		}
		NotificationRuleDTO result = notificationRuleService.save(notificationRuleDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("notificationRule", notificationRuleDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /notification-rules : get all the notificationRules.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         notificationRules in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/notification-rules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<NotificationRuleDTO>> getAllNotificationRules(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of NotificationRules");
		Page<NotificationRuleDTO> page = notificationRuleService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/notification-rules");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /notification-rules/:id : get the "id" notificationRule.
	 *
	 * @param id
	 *            the id of the notificationRuleDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         notificationRuleDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/notification-rules/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<NotificationRuleDTO> getNotificationRule(@PathVariable Long id) {
		log.debug("REST request to get NotificationRule : {}", id);
		NotificationRuleDTO notificationRuleDTO = notificationRuleService.findOne(id);
		return Optional.ofNullable(notificationRuleDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /notification-rules/:id : delete the "id" notificationRule.
	 *
	 * @param id
	 *            the id of the notificationRuleDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/notification-rules/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteNotificationRule(@PathVariable Long id) {
		log.debug("REST request to delete NotificationRule : {}", id);
		notificationRuleService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("notificationRule", id.toString()))
				.build();
	}

	/**
	 * create notifications.
	 * 
	 * @return the ReponseEntity
	 */
	@RequestMapping(value = "/notification-rules/create-notifications", produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> createNotifications() {
		log.debug("REST request to create Notifications");
		notificationRuleService.createNotifications();
		return ResponseEntity.ok().build();
	}

	/**
	 * get a list with valid entities for notifications.
	 * 
	 * @return the ReponseEntity
	 */
	@RequestMapping(value = "/notification-rules/valid-entities", produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<String>> getValidEntities() {
		log.debug("REST request to get a list of valid entities for Notifications");
		return ResponseEntity.ok().body(notificationRuleService.getValidNotificationEntities());
	}

	/**
	 * get a list with valid authorities for notifications.
	 * 
	 * @return the ReponseEntity
	 */
	@RequestMapping(value = "/notification-rules/valid-authorities", produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<String>> getValidAuthorities() {
		log.debug("REST request to get a list of valid authorities for Notifications");
		return ResponseEntity.ok().body(notificationRuleService.getValidAuthorities());
	}

}
