package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;
import at.simianarmy.service.ClothingService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.ClothingDTO;
import at.simianarmy.service.dto.ClothingGroupDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Clothing.
 */
@RestController
@RequestMapping("/api")
public class ClothingResource {

	private final Logger log = LoggerFactory.getLogger(ClothingResource.class);

	@Inject
	private ClothingService clothingService;

	/**
	 * POST /clothing : Create a new clothing.
	 *
	 * @param clothingDTO
	 *            the clothingDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         clothingDTO, or with status 400 (Bad Request) if the clothing has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingDTO> createClothing(@RequestBody ClothingDTO clothingDTO) throws URISyntaxException {
		log.debug("REST request to save Clothing : {}", clothingDTO);
		if (clothingDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("clothing", "idexists", "A new clothing cannot already have an ID"))
					.body(null);
		}
		ClothingDTO result = clothingService.save(clothingDTO);
		return ResponseEntity.created(new URI("/api/clothing/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("clothing", result.getId().toString())).body(result);
	}

	/**
	 * PUT /clothing : Updates an existing clothing.
	 *
	 * @param clothingDTO
	 *            the clothingDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clothingDTO, or with status 400 (Bad Request) if the clothingDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         clothingDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingDTO> updateClothing(@RequestBody ClothingDTO clothingDTO) throws URISyntaxException {
		log.debug("REST request to update Clothing : {}", clothingDTO);
		if (clothingDTO.getId() == null) {
			return createClothing(clothingDTO);
		}
		ClothingDTO result = clothingService.save(clothingDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clothing", clothingDTO.getId().toString())).body(result);
	}

	/**
	 * GET /clothing : get all the clothing.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of clothing in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/clothing", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ClothingDTO>> getAllClothing(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Clothing");
		Page<ClothingDTO> page = clothingService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clothing");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /clothing/:id : get the "id" clothing.
	 *
	 * @param id
	 *            the id of the clothingDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clothingDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/clothing/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingDTO> getClothing(@PathVariable Long id) {
		log.debug("REST request to get Clothing : {}", id);
		ClothingDTO clothingDTO = clothingService.findOne(id);
		return Optional.ofNullable(clothingDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /clothing/:id : delete the "id" clothing.
	 *
	 * @param id
	 *            the id of the clothingDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/clothing/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteClothing(@PathVariable Long id) {
		log.debug("REST request to delete Clothing : {}", id);
		clothingService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clothing", id.toString())).build();
	}

	/**
	 * Get Clothing with given clothing group and persongroup
	 * 
	 * @param clothingGroupDTO
	 * @param personClothingDTO
	 * @return
	 */
	@RequestMapping(value = "/clothing/clothingGroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClothingDTO> getClothingWithClothingGroupAndPersonClothing(
			@RequestParam(name = "typeId", required = false) Long typeId,
			@RequestParam(name = "csize", required = false) String cSize) {
		ClothingGroupDTO clothingGroupDTO = new ClothingGroupDTO();
		clothingGroupDTO.setCsize(cSize);
		clothingGroupDTO.setTypeId(typeId);
		ClothingDTO result = clothingService.getWithClothingGroupAndPersonClothing(clothingGroupDTO, null);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("clothing", result.getId().toString()))
				.body(result);
	}

}
