package at.simianarmy.web.rest;

import at.simianarmy.service.CashbookAccountTransferService;
import at.simianarmy.service.dto.CashbookAccountTransferDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import javax.inject.Inject;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing CashbookAccountTransfers.
 */
@RestController
@RequestMapping("/api")
public class CashbookAccountTransferResource {

  private final Logger log = LoggerFactory.getLogger(CashbookAccountTransferResource.class);

  private static final String ENTITY_NAME = "cashbookAccountTransfer";

  @Inject
  private CashbookAccountTransferService cashbookAccountTransferService;

  /**
   * POST /cashbook-account-transfer : Create a new CashbookAccountTransfer
   *
   * @param cashbookAccountTransferDTO
   *        The CashbookAccountTransferDTO to conduct
   * @return
   *        Response Status 200 OK
   */
  @PostMapping("/cashbook-account-transfer")
  @Timed
  public ResponseEntity<Void> saveCashbookAccountTransfer(@Valid @RequestBody CashbookAccountTransferDTO cashbookAccountTransferDTO) {
    this.log.info("REST-Request to conduct CashbookAccountTransfer {}", cashbookAccountTransferDTO);
    this.cashbookAccountTransferService.save(cashbookAccountTransferDTO);
    return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, null)).build();
  }

}
