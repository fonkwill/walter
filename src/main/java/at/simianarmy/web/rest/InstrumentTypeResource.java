package at.simianarmy.web.rest;

import at.simianarmy.service.InstrumentService;
import at.simianarmy.service.InstrumentTypeService;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.dto.InstrumentTypeDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing InstrumentType.
 */
@RestController
@RequestMapping("/api")
public class InstrumentTypeResource {

	private final Logger log = LoggerFactory.getLogger(InstrumentTypeResource.class);

	@Inject
	private InstrumentTypeService instrumentTypeService;

	@Inject
	private InstrumentService instrumentService;

	/**
	 * POST /instrument-types : Create a new instrumentType.
	 *
	 * @param instrumentTypeDTO
	 *            the instrumentTypeDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         instrumentTypeDTO, or with status 400 (Bad Request) if the
	 *         instrumentType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instrument-types", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentTypeDTO> createInstrumentType(
			@Valid @RequestBody InstrumentTypeDTO instrumentTypeDTO) throws URISyntaxException {
		log.debug("REST request to save InstrumentType : {}", instrumentTypeDTO);
		if (instrumentTypeDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("instrumentType", "idexists",
					"A new instrumentType cannot already have an ID")).body(null);
		}
		InstrumentTypeDTO result = instrumentTypeService.save(instrumentTypeDTO);
		return ResponseEntity.created(new URI("/api/instrument-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("instrumentType", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /instrument-types : Updates an existing instrumentType.
	 *
	 * @param instrumentTypeDTO
	 *            the instrumentTypeDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         instrumentTypeDTO, or with status 400 (Bad Request) if the
	 *         instrumentTypeDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the instrumentTypeDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instrument-types", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentTypeDTO> updateInstrumentType(
			@Valid @RequestBody InstrumentTypeDTO instrumentTypeDTO) throws URISyntaxException {
		log.debug("REST request to update InstrumentType : {}", instrumentTypeDTO);
		if (instrumentTypeDTO.getId() == null) {
			return createInstrumentType(instrumentTypeDTO);
		}
		InstrumentTypeDTO result = instrumentTypeService.save(instrumentTypeDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("instrumentType", instrumentTypeDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /instrument-types : get all the instrumentTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         instrumentTypes in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/instrument-types", "/instruments/instrument-types"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<InstrumentTypeDTO>> getAllInstrumentTypes(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of InstrumentTypes");
		Page<InstrumentTypeDTO> page = instrumentTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instrument-types");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /instrument-types/:id : get the "id" instrumentType.
	 *
	 * @param id
	 *            the id of the instrumentTypeDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         instrumentTypeDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/instrument-types/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentTypeDTO> getInstrumentType(@PathVariable Long id) {
		log.debug("REST request to get InstrumentType : {}", id);
		InstrumentTypeDTO instrumentTypeDTO = instrumentTypeService.findOne(id);
		return Optional.ofNullable(instrumentTypeDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /instrument-types/:id : delete the "id" instrumentType.
	 *
	 * @param id
	 *            the id of the instrumentTypeDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/instrument-types/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteInstrumentType(@PathVariable Long id) {
		log.debug("REST request to delete InstrumentType : {}", id);
		instrumentTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instrumentType", id.toString()))
				.build();
	}

	/**
	 * GET instrument-types/:id/instruments : get all the instruments for an
	 * InstrumentType.
	 *
	 * @param typeId
	 *            the id of the Type, which we want to retrieve instruments from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of instruments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/instrument-types/{typeId}/instruments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<InstrumentDTO>> getInstrumentByType(@PathVariable Long typeId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get all Instruments for an InstrumentType");
		Page<InstrumentDTO> page = instrumentService.findByType(typeId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instruments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
