package at.simianarmy.web.rest;

import at.simianarmy.filestore.dto.DirectoryMetadataDTO;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.dto.FileStoreMetadataDTO;
import at.simianarmy.filestore.service.DocumentsFileStoreService;
import at.simianarmy.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * REST controller for managing Documents.
 */
@RestController
@RequestMapping("/api")
public class DocumentsResource {

  private final Logger log = LoggerFactory.getLogger(DocumentsResource.class);

  @Inject
  private DocumentsFileStoreService documentsFileStoreService;

  /**
   * GET /documents/directory : get the content of a directory
   *
   * @param uuid
   *          The UUID of the directory
   * @return
   *          A List of FileStoreMetadatas contained in the directory
   */
  @RequestMapping(value = {"/documents/directory", "/documents/directory/{uuid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<DirectoryMetadataDTO> getDirectory(@PathVariable @Valid Optional<UUID> uuid, Pageable pageable) {

    DirectoryMetadataDTO fileStoreMetadataDTO;

    if (uuid.isPresent()) {
      log.debug("REST Request to get Directory: {}", uuid.get());
      fileStoreMetadataDTO = this.documentsFileStoreService.getDirectory(uuid.get(), pageable);
    } else {
      log.debug("REST Request to get Root Directory");
      fileStoreMetadataDTO = this.documentsFileStoreService.getDirectory(null, pageable);
    }

    return Optional.ofNullable(fileStoreMetadataDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * POST /documents/directory : create a new directory
   *
   * @param fileStoreMetadataDTO
   *          The DTO to persist
   * @return
   *          The created directory
   * @throws URISyntaxException
   */
  @RequestMapping(value = "/documents/directory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<FileStoreMetadataDTO> createDirectory(@RequestBody FileStoreMetadataDTO fileStoreMetadataDTO) throws URISyntaxException {
    log.debug("REST Request to store new directory: {}", fileStoreMetadataDTO);

    FileStoreMetadataDTO result = this.documentsFileStoreService.createDirectory(fileStoreMetadataDTO);

    return ResponseEntity.ok()
      .headers(HeaderUtil.createEntityUpdateAlert("directory", result.getId().toString())).body(result);
  }

  /**
   * DELETE /documents/directory : delete a directory
   *
   * @param uuid
   *          The UUID of the directory to delete
   * @return
   */
  @RequestMapping(value = "/documents/directory/{uuid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> deleteDirectory(@PathVariable @Valid UUID uuid) {
    log.debug("REST Request to delete directory: {}", uuid);

    this.documentsFileStoreService.deleteDirectory(uuid);

    return ResponseEntity.ok().build();
  }

  /**
   * POST /documents/file : create a new document
   *
   * @param uploadFile
   *          The file to upload
   * @param parentUuid
   *          The Parent-UUID for the file
   * @return
   *          The created Metadata for the Document
   */
  @RequestMapping(value = "/documents/file", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<FileStoreMetadataDTO> createFile(
    @RequestParam(value = "uploadFile") MultipartFile uploadFile,
    @RequestParam(value = "parentUuid", required = false) @Valid UUID parentUuid) {

    log.debug("REST Request to upload new File in Parent Directory {}", parentUuid);

    FileStoreMetadataDTO result = this.documentsFileStoreService.saveDocument(uploadFile, parentUuid);

    return ResponseEntity.ok()
      .headers(HeaderUtil.createEntityUpdateAlert("file", result.getId().toString())).body(result);

  }

  /**
   * GET /documents/file : download a file
   *
   * @param uuid
   *          the UUID of the file to download
   * @return
   *          The Resource-Stream for the File
   */
  @GetMapping("/documents/file/{uuid}")
  @Timed
  public ResponseEntity<Resource> getFile(@PathVariable @Valid UUID uuid) {
    FileResourceDTO file = this.documentsFileStoreService.getDocument(uuid);

    if (file == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return ResponseEntity.ok()
      .header(HttpHeaders.CONTENT_DISPOSITION,
        "attachment; filename=\"" + file.getFileName() + "\"")
      .body(file.getResource());
  }

  /**
   * DELETE /documents/file : Delete a file
   *
   * @param uuid
   *          The UUID of the file to delete
   * @return
   */
  @RequestMapping(value = "/documents/file/{uuid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> deleteFile(@PathVariable @Valid UUID uuid) {
    log.debug("REST Request to delete file: {}", uuid);

    this.documentsFileStoreService.deleteDocument(uuid);

    return ResponseEntity.ok().build();
  }

  @RequestMapping(value = {"documents/file", "documents/directory"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<FileStoreMetadataDTO> updateMetadata(@RequestBody FileStoreMetadataDTO dto) {
    log.debug("REST Request to update metadata: {}", dto);

    FileStoreMetadataDTO result = this.documentsFileStoreService.updateMetadata(dto);

    return ResponseEntity.ok().body(result);
  }

}
