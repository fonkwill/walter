package at.simianarmy.web.rest;

import at.simianarmy.service.AppointmentTypeService;
import at.simianarmy.service.dto.AppointmentTypeDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing AppointmentType.
 */
@RestController
@RequestMapping("/api")
public class AppointmentTypeResource {

	private final Logger log = LoggerFactory.getLogger(AppointmentTypeResource.class);

	@Inject
	private AppointmentTypeService appointmentTypeService;

	/**
	 * POST /appointment-types : Create a new appointmentType.
	 *
	 * @param appointmentTypeDTO
	 *            the appointmentTypeDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         appointmentTypeDTO, or with status 400 (Bad Request) if the
	 *         appointmentType has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/appointment-types")
	@Timed
	public ResponseEntity<AppointmentTypeDTO> createAppointmentType(
			@Valid @RequestBody AppointmentTypeDTO appointmentTypeDTO) throws URISyntaxException {
		log.debug("REST request to save AppointmentType : {}", appointmentTypeDTO);
		if (appointmentTypeDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("appointmentType", "idexists",
					"A new appointmentType cannot already have an ID")).body(null);
		}
		AppointmentTypeDTO result = appointmentTypeService.save(appointmentTypeDTO);
		return ResponseEntity.created(new URI("/api/appointment-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("appointmentType", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /appointment-types : Updates an existing appointmentType.
	 *
	 * @param appointmentTypeDTO
	 *            the appointmentTypeDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         appointmentTypeDTO, or with status 400 (Bad Request) if the
	 *         appointmentTypeDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the appointmentTypeDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/appointment-types")
	@Timed
	public ResponseEntity<AppointmentTypeDTO> updateAppointmentType(
			@Valid @RequestBody AppointmentTypeDTO appointmentTypeDTO) throws URISyntaxException {
		log.debug("REST request to update AppointmentType : {}", appointmentTypeDTO);
		if (appointmentTypeDTO.getId() == null) {
			return createAppointmentType(appointmentTypeDTO);
		}
		AppointmentTypeDTO result = appointmentTypeService.save(appointmentTypeDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("appointmentType", appointmentTypeDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /appointment-types : get all the appointmentTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         appointmentTypes in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/appointment-types")
	@Timed
	public ResponseEntity<List<AppointmentTypeDTO>> getAllAppointmentTypes(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of AppointmentTypes");
		Page<AppointmentTypeDTO> page = appointmentTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointment-types");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /appointment-types/:id : get the "id" appointmentType.
	 *
	 * @param id
	 *            the id of the appointmentTypeDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         appointmentTypeDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/appointment-types/{id}")
	@Timed
	public ResponseEntity<AppointmentTypeDTO> getAppointmentType(@PathVariable Long id) {
		log.debug("REST request to get AppointmentType : {}", id);
		AppointmentTypeDTO appointmentTypeDTO = appointmentTypeService.findOne(id);
		return Optional.ofNullable(appointmentTypeDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /appointment-types/:id : delete the "id" appointmentType.
	 *
	 * @param id
	 *            the id of the appointmentTypeDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/appointment-types/{id}")
	@Timed
	public ResponseEntity<Void> deleteAppointmentType(@PathVariable Long id) {
		log.debug("REST request to delete AppointmentType : {}", id);
		appointmentTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("appointmentType", id.toString()))
				.build();
	}

}
