package at.simianarmy.web.rest;

import at.simianarmy.service.UserColumnConfigService;
import at.simianarmy.service.dto.UserColumnConfigDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URISyntaxException;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing UserColumnConfig.
 */
@RestController
@RequestMapping("/api")
public class UserColumnConfigResource {

  private final Logger log = LoggerFactory.getLogger(UserColumnConfigResource.class);

  private static final String ENTITY_NAME = "columnConfig";

  private final UserColumnConfigService userColumnConfigService;

  public UserColumnConfigResource(
    UserColumnConfigService userColumnConfigService) {
    this.userColumnConfigService = userColumnConfigService;
  }

  /**
   * GET  /column-config : get all the userColumnConfigs.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of userColumnConfigs in body
   */
  @RequestMapping(value = "/column-config", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<UserColumnConfigDTO> getAllUserColumnConfigsByEntity(
    @RequestParam(name = "entity", required = true) String entity,
    @RequestParam(name = "showOnlyVisible", required = false, defaultValue = "false") Boolean showOnlyVisible,
    @RequestParam(name = "showDefault", required = false, defaultValue = "false") Boolean showDefault
  ) {
    log.debug("REST request to get all UserColumnConfigs by entity {}, show only visible: {}, showDefault: {}", entity, showOnlyVisible, showDefault);

    if (showOnlyVisible) {
      return userColumnConfigService.getVisibleColumnConfigsForEntityAndCurrentUser(entity, showDefault);
    } else {
      return userColumnConfigService.getColumnConfigsForEntityAndCurrentUser(entity, showDefault);
    }
  }

  /**
   * PUT  /column-config : Updates an existing userColumnConfg.
   *
   * @param userColumnConfigDTOs the userColumnConfigDTOs to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated userColumnConfigDTO,
   * or with status 400 (Bad Request) if the userColumnConfigDTO is not valid,
   * or with status 500 (Internal Server Error) if the userColumnConfigDTO couldn't be updated
   * @throws URISyntaxException if the Location URI syntax is incorrect
   */
  @RequestMapping(value = "/column-config", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<UserColumnConfigDTO>> updateUserColumnConfig(@Valid @RequestBody List<UserColumnConfigDTO> userColumnConfigDTOs) throws URISyntaxException {
    log.debug("REST request to update UserColumnConfig : {}", userColumnConfigDTOs);
    List<UserColumnConfigDTO> result = userColumnConfigService.update(userColumnConfigDTOs);
    return ResponseEntity.ok()
      .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "ok!"))
      .body(result);
  }
}
