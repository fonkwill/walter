package at.simianarmy.web.rest.vm;

import at.simianarmy.domain.User;
import at.simianarmy.roles.service.dto.UserDTO;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Set;

import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user
 * management UI.
 */
public class ManagedUserVM extends UserDTO {

	public static final int PASSWORD_MIN_LENGTH = 4;
	public static final int PASSWORD_MAX_LENGTH = 100;

	private Long id;

	private String createdBy;

	private Instant createdDate;

	private String lastModifiedBy;

	private Instant lastModifiedDate;

	@Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
	private String password;

	public ManagedUserVM() {
	}

	public ManagedUserVM(User user) {
		super(user);
		this.id = user.getId();
		this.createdBy = user.getCreatedBy();
		this.createdDate = user.getCreatedDate();
		this.lastModifiedBy = user.getLastModifiedBy();
		this.lastModifiedDate = user.getLastModifiedDate();
		this.password = null;
	}

	public ManagedUserVM(Long id, String login, String password, String firstName, String lastName, String email,
			boolean activated, String langKey, Set<String> authorities, String createdBy, ZonedDateTime createdDate,
			String lastModifiedBy, ZonedDateTime lastModifiedDate) {
		super(login, firstName, lastName, email, activated, langKey, authorities);
		this.id = id;
		this.createdBy = createdBy;
		this.createdDate = createdDate == null ? null : createdDate.toInstant();
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedDate = lastModifiedDate == null ? null : lastModifiedDate.toInstant();
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Instant getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Instant createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Instant getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Instant lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "ManagedUserVM{" + "id=" + id + ", createdBy=" + createdBy + ", createdDate=" + createdDate
				+ ", lastModifiedBy='" + lastModifiedBy + '\'' + ", lastModifiedDate=" + lastModifiedDate + "} "
				+ super.toString();
	}
}
