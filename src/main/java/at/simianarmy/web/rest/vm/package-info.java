/**
 * View Models used by Spring MVC REST controllers.
 */
package at.simianarmy.web.rest.vm;
