package at.simianarmy.web.rest;

import at.simianarmy.service.MembershipFeeAmountService;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing MembershipFeeAmount.
 */
@RestController
@RequestMapping("/api")
public class MembershipFeeAmountResource {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeAmountResource.class);

	@Inject
	private MembershipFeeAmountService membershipFeeAmountService;

	/**
	 * POST /membership-fee-amounts : Create a new membershipFeeAmount.
	 *
	 * @param membershipFeeAmountDTO
	 *            the membershipFeeAmountDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         membershipFeeAmountDTO, or with status 400 (Bad Request) if the
	 *         membershipFeeAmount has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fee-amounts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeAmountDTO> createMembershipFeeAmount(
			@Valid @RequestBody MembershipFeeAmountDTO membershipFeeAmountDTO) throws URISyntaxException {
		log.debug("REST request to save MembershipFeeAmount : {}", membershipFeeAmountDTO);
		if (membershipFeeAmountDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("membershipFeeAmount", "idexists",
					"A new membershipFeeAmount cannot already have an ID")).body(null);
		}
		MembershipFeeAmountDTO result = membershipFeeAmountService.save(membershipFeeAmountDTO);
		return ResponseEntity.created(new URI("/api/membership-fee-amounts/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("membershipFeeAmount", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /membership-fee-amounts : Updates an existing membershipFeeAmount.
	 *
	 * @param membershipFeeAmountDTO
	 *            the membershipFeeAmountDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         membershipFeeAmountDTO, or with status 400 (Bad Request) if the
	 *         membershipFeeAmountDTO is not valid, or with status 500 (Internal
	 *         Server Error) if the membershipFeeAmountDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fee-amounts", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeAmountDTO> updateMembershipFeeAmount(
			@Valid @RequestBody MembershipFeeAmountDTO membershipFeeAmountDTO) throws URISyntaxException {
		log.debug("REST request to update MembershipFeeAmount : {}", membershipFeeAmountDTO);
		if (membershipFeeAmountDTO.getId() == null) {
			return createMembershipFeeAmount(membershipFeeAmountDTO);
		}
		MembershipFeeAmountDTO result = membershipFeeAmountService.save(membershipFeeAmountDTO);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert("membershipFeeAmount", membershipFeeAmountDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /membership-fee-amounts : get all the membershipFeeAmounts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         membershipFeeAmounts in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/membership-fee-amounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipFeeAmountDTO>> getAllMembershipFeeAmounts(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of MembershipFeeAmounts");
		Page<MembershipFeeAmountDTO> page = membershipFeeAmountService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/membership-fee-amounts");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /membership-fee-amounts/:id : get the "id" membershipFeeAmount.
	 *
	 * @param id
	 *            the id of the membershipFeeAmountDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         membershipFeeAmountDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/membership-fee-amounts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeAmountDTO> getMembershipFeeAmount(@PathVariable Long id) {
		log.debug("REST request to get MembershipFeeAmount : {}", id);
		MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountService.findOne(id);
		return Optional.ofNullable(membershipFeeAmountDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /membership-fee-amounts/:id : delete the "id" membershipFeeAmount.
	 *
	 * @param id
	 *            the id of the membershipFeeAmountDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/membership-fee-amounts/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteMembershipFeeAmount(@PathVariable Long id) {
		log.debug("REST request to delete MembershipFeeAmount : {}", id);
		membershipFeeAmountService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("membershipFeeAmount", id.toString()))
				.build();
	}

}
