package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;

import at.simianarmy.roles.domain.FeatureGroup;
import at.simianarmy.service.CustomizationService;
import at.simianarmy.service.PersonGroupService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.CustomizationDTO;
import at.simianarmy.service.dto.PersonGroupDTO;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing Customization.
 */
@RestController
@RequestMapping("/api")
public class CustomizationResource {

	private final Logger log = LoggerFactory.getLogger(CustomizationResource.class);

	private static final String ENTITY_NAME = "customization";

	private final CustomizationService customizationService;
	
	private final PersonGroupService personGroupService;

	public CustomizationResource(CustomizationService customizationService, PersonGroupService personGroupService) {
		this.customizationService = customizationService;
		this.personGroupService = personGroupService;
	}

	/**
	 * POST /customizations : Create a new customization.
	 *
	 * @param customizationDTO
	 *            the customizationDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         customizationDTO, or with status 400 (Bad Request) if the
	 *         customization has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/customizations")
	@Timed
	public ResponseEntity<CustomizationDTO> createCustomization(@Valid @RequestBody CustomizationDTO customizationDTO)
			throws URISyntaxException {
		log.debug("REST request to save Customization : {}", customizationDTO);
		if (customizationDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new customization cannot already have an ID")).body(null);
		}
		CustomizationDTO result = customizationService.save(customizationDTO);
		return ResponseEntity.created(new URI("/api/customizations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /customizations : Updates an existing customization.
	 *
	 * @param customizationDTO
	 *            the customizationDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         customizationDTO, or with status 400 (Bad Request) if the
	 *         customizationDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the customizationDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/customizations")
	@Timed
	public ResponseEntity<CustomizationDTO> updateCustomization(@Valid @RequestBody CustomizationDTO customizationDTO)
			throws URISyntaxException {
		log.debug("REST request to update Customization : {}", customizationDTO);
		if (customizationDTO.getId() == null) {
			return createCustomization(customizationDTO);
		}
		CustomizationDTO result = customizationService.save(customizationDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customizationDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /customizations : get all the customizations.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         customizations in body
	 */
	@GetMapping("/customizations")
	@Timed
	public ResponseEntity<List<CustomizationDTO>> getAllCustomizations(@ApiParam Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Customizations");
		Page<CustomizationDTO> page = customizationService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/customizations");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /customizations/:id : get the "id" customization.
	 *
	 * @param id
	 *            the id of the customizationDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         customizationDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/customizations/{id}")
	@Timed
	public ResponseEntity<CustomizationDTO> getCustomization(@PathVariable Long id) {
		log.debug("REST request to get Customization : {}", id);
		CustomizationDTO customizationDTO = customizationService.findOne(id);
		return Optional.ofNullable(customizationDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	/**
	 * DELETE /customizations/:id : delete the "id" customization.
	 *
	 * @param id
	 *            the id of the customizationDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/customizations/{id}")
	@Timed
	public ResponseEntity<Void> deleteCustomization(@PathVariable Long id) {
		log.debug("REST request to delete Customization : {}", id);
		customizationService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
	
	/**
	 * GET /roles/feature-groups : get all possible Feature Groups with Authorities.
	 *
	 * 
	 * @return the ResponseEntity with status 200 (OK) and the list of feature groups as string in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/customizations/feature-groups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Set<String>> getAllFeatureGroups() throws URISyntaxException {
		log.debug("REST request to get all possible Feature Groups");
		return new ResponseEntity<>(new HashSet<>(customizationService.getAllFeatureGroups()), HttpStatus.OK);
	}

}
