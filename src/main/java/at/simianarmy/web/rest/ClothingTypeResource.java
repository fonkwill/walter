package at.simianarmy.web.rest;

import at.simianarmy.service.ClothingTypeService;
import at.simianarmy.service.dto.ClothingTypeDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing ClothingType.
 */
@RestController
@RequestMapping("/api")
public class ClothingTypeResource {

	private final Logger log = LoggerFactory.getLogger(ClothingTypeResource.class);

	@Inject
	private ClothingTypeService clothingTypeService;

	/**
	 * POST /clothing-types : Create a new clothingType.
	 *
	 * @param clothingTypeDTO
	 *            the clothingTypeDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         clothingTypeDTO, or with status 400 (Bad Request) if the clothingType
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing-types", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingTypeDTO> createClothingType(@Valid @RequestBody ClothingTypeDTO clothingTypeDTO)
			throws URISyntaxException {
		log.debug("REST request to save ClothingType : {}", clothingTypeDTO);
		if (clothingTypeDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clothingType", "idexists",
					"A new clothingType cannot already have an ID")).body(null);
		}
		ClothingTypeDTO result = clothingTypeService.save(clothingTypeDTO);
		return ResponseEntity.created(new URI("/api/clothing-types/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("clothingType", result.getId().toString())).body(result);
	}

	/**
	 * PUT /clothing-types : Updates an existing clothingType.
	 *
	 * @param clothingTypeDTO
	 *            the clothingTypeDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clothingTypeDTO, or with status 400 (Bad Request) if the
	 *         clothingTypeDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the clothingTypeDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing-types", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingTypeDTO> updateClothingType(@Valid @RequestBody ClothingTypeDTO clothingTypeDTO)
			throws URISyntaxException {
		log.debug("REST request to update ClothingType : {}", clothingTypeDTO);
		if (clothingTypeDTO.getId() == null) {
			return createClothingType(clothingTypeDTO);
		}
		ClothingTypeDTO result = clothingTypeService.save(clothingTypeDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clothingType", clothingTypeDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /clothing-types : get all the clothingTypes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of clothingTypes
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/clothing-types", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ClothingTypeDTO>> getAllClothingTypes(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of ClothingTypes");
		Page<ClothingTypeDTO> page = clothingTypeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clothing-types");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /clothing-types/:id : get the "id" clothingType.
	 *
	 * @param id
	 *            the id of the clothingTypeDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clothingTypeDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/clothing-types/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingTypeDTO> getClothingType(@PathVariable Long id) {
		log.debug("REST request to get ClothingType : {}", id);
		ClothingTypeDTO clothingTypeDTO = clothingTypeService.findOne(id);
		return Optional.ofNullable(clothingTypeDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /clothing-types/:id : delete the "id" clothingType.
	 *
	 * @param id
	 *            the id of the clothingTypeDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/clothing-types/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteClothingType(@PathVariable Long id) {
		log.debug("REST request to delete ClothingType : {}", id);
		clothingTypeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("clothingType", id.toString())).build();
	}

}
