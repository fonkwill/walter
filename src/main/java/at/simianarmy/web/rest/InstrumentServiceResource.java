package at.simianarmy.web.rest;

import at.simianarmy.service.InstrumentServiceService;
import at.simianarmy.service.dto.InstrumentServiceDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing InstrumentService.
 */
@RestController
@RequestMapping("/api")
public class InstrumentServiceResource {

	private final Logger log = LoggerFactory.getLogger(InstrumentServiceResource.class);

	@Inject
	private InstrumentServiceService instrumentServiceService;

	/**
	 * POST /instrument-services : Create a new instrumentService.
	 *
	 * @param instrumentServiceDTO
	 *            the instrumentServiceDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         instrumentServiceDTO, or with status 400 (Bad Request) if the
	 *         instrumentService has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instrument-services", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentServiceDTO> createInstrumentService(
			@Valid @RequestBody InstrumentServiceDTO instrumentServiceDTO) throws URISyntaxException {
		log.debug("REST request to save InstrumentService : {}", instrumentServiceDTO);
		if (instrumentServiceDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("instrumentService", "idexists",
					"A new instrumentService cannot already have an ID")).body(null);
		}
		InstrumentServiceDTO result = instrumentServiceService.save(instrumentServiceDTO);
		return ResponseEntity.created(new URI("/api/instrument-services/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("instrumentService", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /instrument-services : Updates an existing instrumentService.
	 *
	 * @param instrumentServiceDTO
	 *            the instrumentServiceDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         instrumentServiceDTO, or with status 400 (Bad Request) if the
	 *         instrumentServiceDTO is not valid, or with status 500 (Internal
	 *         Server Error) if the instrumentServiceDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instrument-services", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentServiceDTO> updateInstrumentService(
			@Valid @RequestBody InstrumentServiceDTO instrumentServiceDTO) throws URISyntaxException {
		log.debug("REST request to update InstrumentService : {}", instrumentServiceDTO);
		if (instrumentServiceDTO.getId() == null) {
			return createInstrumentService(instrumentServiceDTO);
		}
		InstrumentServiceDTO result = instrumentServiceService.save(instrumentServiceDTO);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityUpdateAlert("instrumentService", instrumentServiceDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /instrument-services : get all the instrumentServices.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         instrumentServices in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/instrument-services", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<InstrumentServiceDTO>> getAllInstrumentServices(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of InstrumentServices");
		Page<InstrumentServiceDTO> page = instrumentServiceService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instrument-services");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /instrument-services/:id : get the "id" instrumentService.
	 *
	 * @param id
	 *            the id of the instrumentServiceDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         instrumentServiceDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/instrument-services/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentServiceDTO> getInstrumentService(@PathVariable Long id) {
		log.debug("REST request to get InstrumentService : {}", id);
		InstrumentServiceDTO instrumentServiceDTO = instrumentServiceService.findOne(id);
		return Optional.ofNullable(instrumentServiceDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /instrument-services/:id : delete the "id" instrumentService.
	 *
	 * @param id
	 *            the id of the instrumentServiceDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/instrument-services/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteInstrumentService(@PathVariable Long id) {
		log.debug("REST request to delete InstrumentService : {}", id);
		instrumentServiceService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instrumentService", id.toString()))
				.build();
	}

}
