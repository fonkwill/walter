package at.simianarmy.web.rest;

import at.simianarmy.service.AwardService;
import at.simianarmy.service.PersonAwardService;
import at.simianarmy.service.dto.AwardDTO;
import at.simianarmy.service.dto.PersonAwardDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Award.
 */
@RestController
@RequestMapping("/api")
public class AwardResource {

	private final Logger log = LoggerFactory.getLogger(AwardResource.class);

	@Inject
	private AwardService awardService;

	@Inject
	private PersonAwardService personAwardService;

	/**
	 * POST /awards : Create a new award.
	 *
	 * @param awardDTO
	 *            the awardDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         awardDTO, or with status 400 (Bad Request) if the award has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/awards", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<AwardDTO> createAward(@Valid @RequestBody AwardDTO awardDTO) throws URISyntaxException {
		log.debug("REST request to save Award : {}", awardDTO);
		if (awardDTO.getId() != null) {
			return ResponseEntity.badRequest()
					.headers(
							HeaderUtil.createFailureAlert("award", "idexists", "A new award cannot already have an ID"))
					.body(null);
		}
		AwardDTO result = awardService.save(awardDTO);
		return ResponseEntity.created(new URI("/api/awards/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("award", result.getId().toString())).body(result);
	}

	/**
	 * PUT /awards : Updates an existing award.
	 *
	 * @param awardDTO
	 *            the awardDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         awardDTO, or with status 400 (Bad Request) if the awardDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the awardDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/awards", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<AwardDTO> updateAward(@Valid @RequestBody AwardDTO awardDTO) throws URISyntaxException {
		log.debug("REST request to update Award : {}", awardDTO);
		if (awardDTO.getId() == null) {
			return createAward(awardDTO);
		}
		AwardDTO result = awardService.save(awardDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("award", awardDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /awards : get all the awards.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of awards in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/awards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<AwardDTO>> getAllAwards(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Awards");
		Page<AwardDTO> page = awardService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/awards");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /awards/:id : get the "id" award.
	 *
	 * @param id
	 *            the id of the awardDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the awardDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/awards/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<AwardDTO> getAward(@PathVariable Long id) {
		log.debug("REST request to get Award : {}", id);
		AwardDTO awardDTO = awardService.findOne(id);
		return Optional.ofNullable(awardDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /awards/:id : delete the "id" award.
	 *
	 * @param id
	 *            the id of the awardDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/awards/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteAward(@PathVariable Long id) {
		log.debug("REST request to delete Award : {}", id);
		awardService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("award", id.toString())).build();
	}

	/**
	 * GET awards/:id/personAwards : get all the personAwards for a award.
	 *
	 * @param awardId
	 *            the id of the Award, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of instruments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/awards/{awardId}/personAwards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonAwardDTO>> getPersonsByAward(@PathVariable Long awardId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get all Instruments for an InstrumentType");
		Page<PersonAwardDTO> page = personAwardService.findByAward(awardId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/awards");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
