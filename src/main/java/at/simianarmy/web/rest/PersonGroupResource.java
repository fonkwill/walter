package at.simianarmy.web.rest;

import at.simianarmy.service.PersonGroupService;
import at.simianarmy.service.PersonService;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing PersonGroup.
 */
@RestController
@RequestMapping("/api")
public class PersonGroupResource {

	private final Logger log = LoggerFactory.getLogger(PersonGroupResource.class);

	@Inject
	private PersonGroupService personGroupService;

	@Inject
	private PersonService personService;

	/**
	 * POST /person-groups : Create a new personGroup.
	 *
	 * @param personGroupDTO
	 *            the personGroupDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personGroupDTO, or with status 400 (Bad Request) if the personGroup
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-groups", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonGroupDTO> createPersonGroup(@Valid @RequestBody PersonGroupDTO personGroupDTO)
			throws URISyntaxException {
		log.debug("REST request to save PersonGroup : {}", personGroupDTO);
		if (personGroupDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("personGroup", "idexists",
					"A new personGroup cannot already have an ID")).body(null);
		}
		PersonGroupDTO result = personGroupService.save(personGroupDTO);
		return ResponseEntity.created(new URI("/api/person-groups/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("personGroup", result.getId().toString())).body(result);
	}

	/**
	 * PUT /person-groups : Updates an existing personGroup.
	 *
	 * @param personGroupDTO
	 *            the personGroupDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personGroupDTO, or with status 400 (Bad Request) if the
	 *         personGroupDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the personGroupDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-groups", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonGroupDTO> updatePersonGroup(@Valid @RequestBody PersonGroupDTO personGroupDTO)
			throws URISyntaxException {
		log.debug("REST request to update PersonGroup : {}", personGroupDTO);
		if (personGroupDTO.getId() == null) {
			return createPersonGroup(personGroupDTO);
		}
		PersonGroupDTO result = personGroupService.save(personGroupDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("personGroup", personGroupDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /person-groups : get all the personGroups.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of personGroups
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/person-groups", "people/import/person-groups", "people/person-groups", "/customizations/person-groups", "/letters/person-groups", "/email-verification-letters/person-groups"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonGroupDTO>> getAllPersonGroups(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of PersonGroups");
		Page<PersonGroupDTO> page = personGroupService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-groups");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /person-groups/:id : get the "id" personGroup.
	 *
	 * @param id
	 *            the id of the personGroupDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         personGroupDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/person-groups/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonGroupDTO> getPersonGroup(@PathVariable Long id) {
		log.debug("REST request to get PersonGroup : {}", id);
		PersonGroupDTO personGroupDTO = personGroupService.findOne(id);
		return Optional.ofNullable(personGroupDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /person-groups/:id : delete the "id" personGroup.
	 *
	 * @param id
	 *            the id of the personGroupDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/person-groups/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePersonGroup(@PathVariable Long id) {
		log.debug("REST request to delete PersonGroup : {}", id);
		personGroupService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personGroup", id.toString())).build();
	}

	/**
	 * GET /person-groups/:id/people : get all people in a personGroup.
	 *
	 * @param id
	 *            the id of the personGroup
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of personGroups
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/person-groups/{id}/people", "/appointments/person-groups/{id}/people"} , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonDTO>> getPeopleForPersonGroup(@PathVariable Long id, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of People in a PersonGroup : {}", id);
		Page<PersonDTO> page = this.personService.findByPersonGroup(id, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-groups");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
