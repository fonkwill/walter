package at.simianarmy.web.rest;

import at.simianarmy.service.PersonInstrumentService;
import at.simianarmy.service.dto.PersonInstrumentDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing PersonInstrument.
 */
@RestController
@RequestMapping("/api")
public class PersonInstrumentResource {

	private final Logger log = LoggerFactory.getLogger(PersonInstrumentResource.class);

	@Inject
	private PersonInstrumentService personInstrumentService;

	/**
	 * POST /person-instruments : Create a new personInstrument.
	 *
	 * @param personInstrumentDTO
	 *            the personInstrumentDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personInstrumentDTO, or with status 400 (Bad Request) if the
	 *         personInstrument has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-instruments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonInstrumentDTO> createPersonInstrument(
			@Valid @RequestBody PersonInstrumentDTO personInstrumentDTO) throws URISyntaxException {
		log.debug("REST request to save PersonInstrument : {}", personInstrumentDTO);
		if (personInstrumentDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("personInstrument", "idexists",
					"A new personInstrument cannot already have an ID")).body(null);
		}
		PersonInstrumentDTO result = personInstrumentService.save(personInstrumentDTO);
		return ResponseEntity.created(new URI("/api/person-instruments/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("personInstrument", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /person-instruments : Updates an existing personInstrument.
	 *
	 * @param personInstrumentDTO
	 *            the personInstrumentDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personInstrumentDTO, or with status 400 (Bad Request) if the
	 *         personInstrumentDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the personInstrumentDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-instruments", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonInstrumentDTO> updatePersonInstrument(
			@Valid @RequestBody PersonInstrumentDTO personInstrumentDTO) throws URISyntaxException {
		log.debug("REST request to update PersonInstrument : {}", personInstrumentDTO);
		if (personInstrumentDTO.getId() == null) {
			return createPersonInstrument(personInstrumentDTO);
		}
		PersonInstrumentDTO result = personInstrumentService.save(personInstrumentDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("personInstrument", personInstrumentDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /person-instruments : get all the personInstruments.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         personInstruments in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/person-instruments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonInstrumentDTO>> getAllPersonInstruments(
			@RequestParam(name = "personId", required = false) Long personId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of PersonInstruments");
		Page<PersonInstrumentDTO> page = personInstrumentService.findAll(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-instruments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /person-instruments/:id : get the "id" personInstrument.
	 *
	 * @param id
	 *            the id of the personInstrumentDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         personInstrumentDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/person-instruments/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonInstrumentDTO> getPersonInstrument(@PathVariable Long id) {
		log.debug("REST request to get PersonInstrument : {}", id);
		PersonInstrumentDTO personInstrumentDTO = personInstrumentService.findOne(id);
		return Optional.ofNullable(personInstrumentDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /person-instruments/:id : delete the "id" personInstrument.
	 *
	 * @param id
	 *            the id of the personInstrumentDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/person-instruments/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePersonInstrument(@PathVariable Long id) {
		log.debug("REST request to delete PersonInstrument : {}", id);
		personInstrumentService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personInstrument", id.toString()))
				.build();
	}

}
