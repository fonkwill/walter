package at.simianarmy.web.rest;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.service.LetterService;
import at.simianarmy.service.MailLetterService;
import at.simianarmy.service.PersonGroupService;
import at.simianarmy.service.TemplateService;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.dto.MailLetterDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.dto.TemplateDTO;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing Letter.
 */
@RestController
@RequestMapping("/api")
public class LetterResource {

	private final Logger log = LoggerFactory.getLogger(LetterResource.class);

	@Inject
	private LetterService letterService;

	@Inject
  private MailLetterService mailLetterService;
	
	@Inject
	private TemplateService templateService;
		
	@Inject
	private PersonGroupService personGroupService;

	/**
	 * POST /letters : Create a new letter.
	 *
	 * @param letterDTO
	 *            the letterDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         letterDTO, or with status 400 (Bad Request) if the letter has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = {"/letters", "demand-letters", "email-verification-letters"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<LetterDTO> createLetter(@RequestBody LetterDTO letterDTO) throws URISyntaxException {
		log.debug("REST request to save Letter : {}", letterDTO);
		if (letterDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("letter", "idexists", "A new letter cannot already have an ID"))
					.body(null);
		}
		LetterDTO result = letterService.save(letterDTO);
		return ResponseEntity.created(new URI("/api/letters/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("letter", result.getId().toString())).body(result);
	}

	/**
	 * PUT /letters : Updates an existing letter.
	 *
	 * @param letterDTO
	 *            the letterDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         letterDTO, or with status 400 (Bad Request) if the letterDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the letterDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = {"/letters", "demand-letters", "email-verification-letters"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<LetterDTO> updateLetter(@RequestBody LetterDTO letterDTO) throws URISyntaxException {
		log.debug("REST request to update Letter : {}", letterDTO);
		if (letterDTO.getId() == null) {
			return createLetter(letterDTO);
		}
		LetterDTO result = letterService.save(letterDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("letter", letterDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /letters : get all the letters.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of letters in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/letters", "demand-letters", "email-verification-letters"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<LetterDTO>> getAllLetters(
			@RequestParam(value = "letterType", required = true) LetterType letterType, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Letters");
		Page<LetterDTO> page = letterService.findAll(letterType, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/letters");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /letters/:id : get the "id" letter.
	 *
	 * @param id
	 *            the id of the letterDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the letterDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = {"/letters/{id}", "demand-letters/{id}", "email-verification-letters/{id}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<LetterDTO> getLetter(@PathVariable Long id) {
		log.debug("REST request to get Letter : {}", id);
		LetterDTO letterDTO = letterService.findOne(id);
		return Optional.ofNullable(letterDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /letters/:id : delete the "id" letter.
	 *
	 * @param id
	 *            the id of the letterDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = {"/letters/{id}", "demand-letters/{id}", "email-verification-letters/{id}"}, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteLetter(@PathVariable Long id) {
		log.debug("REST request to delete Letter : {}", id);
		letterService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("letter", id.toString())).build();
	}

	/**
	 * POST /letters : Create a new letter.
	 *
	 * @param letterDTO
	 *            the letterDTO to generate
	 * @return the ResponseEntity with inputstream to PDF
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = {"/letters/{id}", "demand-letters/{id}", "email-verification-letters/{id}"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_PDF_VALUE)
	@Timed
	public ResponseEntity<Resource> generateLetter(@RequestBody LetterDTO letterDTO) throws URISyntaxException {
		log.debug("REST request to generate Letter : {}", letterDTO);

		try {
      Resource result = letterService.generate(letterDTO);
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
					.header(HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename=\"" + SerialLetterConstants.generatedSerialLettersFile + "\"")
					.headers(HeaderUtil.createAlert("walterApp.letter.generated", null)).body(result);
		} catch (ServiceValidationException | SerialLetterGenerationException exc) {
			// is not catched with the exception handler - workaround
			String json = "{ \"message\" : \"" + exc.getMessage() + "\" }";
			return ResponseEntity.badRequest().body(new ByteArrayResource(json.getBytes()));
		}
	}

  /**
   * GET /mail-letters/:id : get the "id" mail-letter.
   *
   * @param id
   *            the id of the mailLetterDTO to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the letterDTO,
   *         or with status 404 (Not Found)
   */
  @RequestMapping(value = {"/mail-letters/{id}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<MailLetterDTO> getMailLetter(@PathVariable Long id) {
    log.debug("REST request to get Letter : {}", id);
    MailLetterDTO mailLetterDTO = mailLetterService.findOne(id);
    return Optional.ofNullable(mailLetterDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * PUT /mail-letters : Send the given LetterDTO per E-Mail
   *
   * @param mailLetterDTO
   */
  @RequestMapping(value = {"/mail-letters"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Resource> generateMailLetter(@RequestBody MailLetterDTO mailLetterDTO) {
    log.debug("REST request to mail Letter : {}", mailLetterDTO);

    this.mailLetterService.send(mailLetterDTO);


    return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
      .headers(HeaderUtil.createAlert("walterApp.mail-letter.generated", null)).build();
  }
}
