package at.simianarmy.web.rest;

import at.simianarmy.service.AppointmentService;
import at.simianarmy.service.PersonService;
import at.simianarmy.service.dto.AppointmentDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import biweekly.Biweekly;
import biweekly.ICalendar;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Appointment.
 */
@RestController
@RequestMapping("/api")
public class AppointmentResource {

	private final Logger log = LoggerFactory.getLogger(AppointmentResource.class);

	@Inject
	private AppointmentService appointmentService;

	@Inject
	private PersonService personService;

	/**
	 * POST /appointments : Create a new appointment.
	 *
	 * @param appointmentDTO
	 *            the appointmentDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         appointmentDTO, or with status 400 (Bad Request) if the appointment
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/appointments")
	@Timed
	public ResponseEntity<AppointmentDTO> createAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO)
			throws URISyntaxException {
		log.debug("REST request to save Appointment : {}", appointmentDTO);
		if (appointmentDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("appointment", "idexists",
					"A new appointment cannot already have an ID")).body(null);
		}
		AppointmentDTO result = appointmentService.save(appointmentDTO);
		return ResponseEntity.created(new URI("/api/appointments/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("appointment", result.getId().toString())).body(result);
	}

	/**
	 * PUT /appointments : Updates an existing appointment.
	 *
	 * @param appointmentDTO
	 *            the appointmentDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         appointmentDTO, or with status 400 (Bad Request) if the
	 *         appointmentDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the appointmentDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/appointments")
	@Timed
	public ResponseEntity<AppointmentDTO> updateAppointment(@Valid @RequestBody AppointmentDTO appointmentDTO)
			throws URISyntaxException {
		log.debug("REST request to update Appointment : {}", appointmentDTO);
		if (appointmentDTO.getId() == null) {
			return createAppointment(appointmentDTO);
		}
		AppointmentDTO result = appointmentService.save(appointmentDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("appointment", appointmentDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /appointments : Get all the appointments, optional parameters are used as
	 * filters.
	 *
	 * @param appointmentTypeId
	 *            id of appointment type
	 * @param searchString
	 *            appointment name or description to look for
	 * @param earliestBegin
	 *            earliest begin of the appointments
	 * @param latestEnd
	 *            latest end of the appointments
	 * @param location
	 *            location of the appointments
	 * @param composition
	 *            the name of a composition played during the appointments
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of appointments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/appointments")
	@Timed
	public ResponseEntity<List<AppointmentDTO>> getAllAppointments(
			@RequestParam(value = "appointmentTypeId", required = false) Long appointmentTypeId,
			@RequestParam(value = "searchString", required = false) String searchString,
			@RequestParam(value = "earliestBegin", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate earliestBegin,
			@RequestParam(value = "latestEnd", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate latestEnd,
			@RequestParam(value = "location", required = false) String location,
			@RequestParam(value = "composition", required = false) String composition,
			@RequestParam(value = "isOfficial", required = false) Boolean isOfficial, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Appointments");
		Page<AppointmentDTO> page = appointmentService.findAll(appointmentTypeId, searchString, earliestBegin,
				latestEnd, location, composition, isOfficial, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /appointments/ical : Get all the appointments, optional parameters are
	 * used as filters.
	 *
	 * @param appointmentTypeId
	 *            id of appointment type
	 * @param searchString
	 *            appointment name or description to look for
	 * @param earliestBegin
	 *            earliest begin of the appointments
	 * @param latestEnd
	 *            latest end of the appointments
	 * @param location
	 *            location of the appointments
	 * @param composition
	 *            the name of a composition played during the appointments
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of appointments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/appointments/ical")
	@Timed
	public ResponseEntity<String> getAppointmentsAsICal(
			@RequestParam(value = "appointmentTypeId", required = false) Long appointmentTypeId,
			@RequestParam(value = "searchString", required = false) String searchString,
			@RequestParam(value = "earliestBegin", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate earliestBegin,
			@RequestParam(value = "latestEnd", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate latestEnd,
			@RequestParam(value = "location", required = false) String location,
			@RequestParam(value = "composition", required = false) String composition,
			@RequestParam(value = "isOfficial", required = false) Boolean isOfficial, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get Appointments in iCal format");

		ICalendar ical = this.appointmentService.getAppointmentsAsICal(appointmentTypeId, searchString, earliestBegin,
				latestEnd, location, composition, isOfficial);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/calendar; charset=utf-8");
		responseHeaders.add("Content-Disposition", "attachment; filename=\"walterCalendar.ical\"");

		return new ResponseEntity<>(Biweekly.write(ical).go(), responseHeaders, HttpStatus.OK);
	}

	/**
	 * Creates an XML report for AKM for all appointments from beginDate to endDate.
	 * 
	 * @param beginDate
	 *            begin of report
	 * @param endDate
	 *            end of report
	 * @return the XML file
	 */
	@PostMapping("/appointments/akmexport")
	@Timed
	public ResponseEntity<Resource> createAkmExport(
			@RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate beginDate,
			@RequestParam(value = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate)
			throws URISyntaxException {
		log.debug("REST request to get an AKM report");

		Resource xml = this.appointmentService.getAkmReport(beginDate, endDate);

		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String fileName = "AKM_Export_" + sdfDate.format(now) + ".xml";
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/xml; charset=ISO-8859-1");
		responseHeaders.add("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		return new ResponseEntity<>(xml, responseHeaders, HttpStatus.OK);
	}

	/**
	 * GET /appointments/:id : get the "id" appointment.
	 *
	 * @param id
	 *            the id of the appointmentDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         appointmentDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/appointments/{id}")
	@Timed
	public ResponseEntity<AppointmentDTO> getAppointment(@PathVariable Long id) {
		log.debug("REST request to get Appointment : {}", id);
		AppointmentDTO appointmentDTO = appointmentService.findOne(id);
		return Optional.ofNullable(appointmentDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /appointments/:id : delete the "id" appointment.
	 *
	 * @param id
	 *            the id of the appointmentDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/appointments/{id}")
	@Timed
	public ResponseEntity<Void> deleteAppointment(@PathVariable Long id) {
		log.debug("REST request to delete Appointment : {}", id);
		appointmentService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("appointment", id.toString())).build();
	}

	/**
	 * GET /appointments/:id/people : get all people for an appointment.
	 *
	 * @param id
	 *            the appointment
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of appointments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/appointments/{id}/people")
	@Timed
	public ResponseEntity<List<PersonDTO>> getPeopleForAppointment(@PathVariable Long id, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Peoeple for an Appointment : {}", id);
		Page<PersonDTO> page = personService.findByAppointment(id, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/appointments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	


	/**
	 * PUT /appointments/:id/people : Add person to an appointment.
	 *
	 * @param id
	 *            the appointment where to add the person
	 * @param personDTO
	 *            the person to add
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compositionDTO, or with status 400 (Bad Request) if the composition
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/appointments/{id}/people")
	@Timed
	public ResponseEntity<AppointmentDTO> addPersonToAppointment(@PathVariable("id") Long id,
			@Valid @RequestBody PersonDTO personDTO) throws URISyntaxException {
		log.debug("REST request to add a Person to Appointment : {}", id, personDTO);

		AppointmentDTO result = appointmentService.addPerson(id, personDTO);

		return ResponseEntity.created(new URI("/api/appointments/" + result.getId())).body(result);
	}

	/**
	 * DELETE /appointments/:id/people/:personId : remove the "personId" person.
	 *
	 * @param id
	 *            the id of the appointmentDTO
	 * @param personId
	 *            the id of the person to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/appointments/{id}/people/{personId}")
	@Timed
	public ResponseEntity<Void> removePersonFromAppointment(@PathVariable Long id, @PathVariable Long personId) {
		log.debug("REST request to delete Person from Appointment : {}", id, personId);
		appointmentService.removePerson(id, personId);
		return ResponseEntity.ok().build();
	}
}
