package at.simianarmy.web.rest;

import at.simianarmy.service.ArrangerService;
import at.simianarmy.service.ColorCodeService;
import at.simianarmy.service.ComposerService;
import at.simianarmy.service.CompositionService;
import at.simianarmy.service.MusicBookService;
import at.simianarmy.service.dto.ArrangerDTO;
import at.simianarmy.service.dto.ColorCodeDTO;
import at.simianarmy.service.dto.ComposerDTO;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.dto.MusicBookDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Composition.
 */
@RestController
@RequestMapping("/api")
public class CompositionResource {

	private final Logger log = LoggerFactory.getLogger(CompositionResource.class);

	@Inject
	private CompositionService compositionService;

	@Inject
	private ArrangerService arrangerService;

	@Inject
	private ComposerService composerService;
	
	@Inject
	private MusicBookService musicBookService;

	@Inject
	private ColorCodeService colorCodeService;
	/**
	 * POST /compositions : Create a new composition.
	 *
	 * @param compositionDTO
	 *            the compositionDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compositionDTO, or with status 400 (Bad Request) if the composition
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/compositions")
	@Timed
	public ResponseEntity<CompositionDTO> createComposition(@Valid @RequestBody CompositionDTO compositionDTO)
			throws URISyntaxException {
		log.debug("REST request to save Composition : {}", compositionDTO);
		if (compositionDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("composition", "idexists",
					"A new composition cannot already have an ID")).body(null);
		}
		CompositionDTO result = compositionService.save(compositionDTO);
		return ResponseEntity.created(new URI("/api/compositions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("composition", result.getId().toString())).body(result);
	}

	/**
	 * PUT /compositions : Updates an existing composition.
	 *
	 * @param compositionDTO
	 *            the compositionDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         compositionDTO, or with status 400 (Bad Request) if the
	 *         compositionDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the compositionDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/compositions")
	@Timed
	public ResponseEntity<CompositionDTO> updateComposition(@Valid @RequestBody CompositionDTO compositionDTO)
			throws URISyntaxException {
		log.debug("REST request to update Composition : {}", compositionDTO);
		if (compositionDTO.getId() == null) {
			return createComposition(compositionDTO);
		}
		CompositionDTO result = compositionService.save(compositionDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("composition", compositionDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /compositions : get all the compositions.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of compositions
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value = "/compositions")
	@Timed
	public ResponseEntity<List<CompositionDTO>> getAllCompositions(
			@RequestParam(value = "nr", required = false) Integer nr,
			@RequestParam(value = "colorCodeId", required = false) Long colorCodeId,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "genre", required = false) String genre,
			@RequestParam(value = "composer", required = false) String composer,
			@RequestParam(value = "arranger", required = false) String arranger,
			@RequestParam(value = "publishingCompany", required = false) String publishingCompany,
			@RequestParam(value = "orderingCompany", required = false) String orderingCompany,
			@RequestParam(value = "musicBookId", required = false) Long musicBookId,
			@RequestParam(value = "active", required = false) Boolean isActive, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Compositions");
		Page<CompositionDTO> page;
		page = compositionService.findAll(nr, colorCodeId, title, genre, composer, arranger, publishingCompany,
				orderingCompany, musicBookId, isActive, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/compositions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /compositions/:id : get the "id" composition.
	 *
	 * @param id
	 *            the id of the compositionDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         compositionDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/compositions/{id}")
	@Timed
	public ResponseEntity<CompositionDTO> getComposition(@PathVariable Long id) {
		log.debug("REST request to get Composition : {}", id);
		CompositionDTO compositionDTO = compositionService.findOne(id);
		return Optional.ofNullable(compositionDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /compositions/:id : delete the "id" composition.
	 *
	 * @param id
	 *            the id of the compositionDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compositions/{id}")
	@Timed
	public ResponseEntity<Void> deleteComposition(@PathVariable Long id) {
		log.debug("REST request to delete Composition : {}", id);
		compositionService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("composition", id.toString())).build();
	}

	/**
	 * GET /compositions/:id/composers : get all composers for a composition.
	 *
	 * @param compositionId
	 *            id of the composition
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of compositions
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value = "/compositions/{id}/composers")
	@Timed
	public ResponseEntity<List<ComposerDTO>> getComposersForComposition(@PathVariable("id") Long compositionId,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Composers for a Composition");
		Page<ComposerDTO> page;
		page = composerService.findByComposition(compositionId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/compositions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * PUT /compositions/:id/composers : Create a new composition.
	 *
	 * @param id
	 *            the composition where to add the composer
	 * @param composerDTO
	 *            the composer to add
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compositionDTO, or with status 400 (Bad Request) if the composition
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/compositions/{id}/composers")
	@Timed
	public ResponseEntity<CompositionDTO> addComposerToComposition(@PathVariable("id") Long id,
			@Valid @RequestBody ComposerDTO composerDTO) throws URISyntaxException {
		log.debug("REST request to add a Composer to Compostion : {}", id, composerDTO);

		CompositionDTO result = compositionService.addComposer(id, composerDTO);

		return ResponseEntity.created(new URI("/api/compositions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("composition", result.getId().toString())).body(result);
	}

	/**
	 * DELETE /compositions/:id/composers/:composerId : remove the "composerId"
	 * composer.
	 *
	 * @param id
	 *            the id of the compositionDTO
	 * @param composerId
	 *            the id of the composer to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compositions/{id}/composers/{composerId}")
	@Timed
	public ResponseEntity<Void> removeComposerFromComposition(@PathVariable Long id, @PathVariable Long composerId) {
		log.debug("REST request to delete Composer From Composition : {}", id, composerId);
		compositionService.removeComposer(id, composerId);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("composition", id.toString())).build();
	}

	/**
	 * GET /compositions/:id/arrangers : get all arrangers for a composition.
	 *
	 * @param id
	 *            id of the composition
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of compositions
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value = "/compositions/{id}/arrangers")
	@Timed
	public ResponseEntity<List<ArrangerDTO>> getArrangersForComposition(@PathVariable("id") Long id, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Arrangers for a Composition");
		Page<ArrangerDTO> page;
		page = arrangerService.findByComposition(id, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/compositions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * PUT /compositions/:id/arrangers : Create a new composition.
	 *
	 * @param id
	 *            the composition where to add the composer
	 * @param arrangerDTO
	 *            the arranger to add
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compositionDTO, or with status 400 (Bad Request) if the composition
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/compositions/{id}/arrangers")
	@Timed
	public ResponseEntity<CompositionDTO> addArrangerToComposition(@PathVariable("id") Long id,
			@Valid @RequestBody ArrangerDTO arrangerDTO) throws URISyntaxException {
		log.debug("REST request to add an Arranger to Compostion : {}", id, arrangerDTO);

		CompositionDTO result = compositionService.addArranger(id, arrangerDTO);

		return ResponseEntity.created(new URI("/api/compositions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("composition", result.getId().toString())).body(result);
	}

	/**
	 * DELETE /compositions/:id/arrangers/:arrangerId : remove the "arrangerId"
	 * composer.
	 *
	 * @param id
	 *            the id of the compositionDTO
	 * @param arrangerId
	 *            the id of the arranger to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/compositions/{id}/arrangers/{arrangerId}")
	@Timed
	public ResponseEntity<Void> removeArrangerFromComposition(@PathVariable Long id, @PathVariable Long arrangerId) {
		log.debug("REST request to delete Composer From Composition : {}", id, arrangerId);
		compositionService.removeArranger(id, arrangerId);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("composition", id.toString())).build();
	}
	
	



	

}
