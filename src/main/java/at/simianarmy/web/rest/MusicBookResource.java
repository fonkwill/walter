package at.simianarmy.web.rest;

import at.simianarmy.service.MusicBookService;
import at.simianarmy.service.dto.MusicBookDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing MusicBook.
 */
@RestController
@RequestMapping("/api")
public class MusicBookResource {

	private final Logger log = LoggerFactory.getLogger(MusicBookResource.class);

	@Inject
	private MusicBookService musicBookService;

	/**
	 * POST /music-books : Create a new musicBook.
	 *
	 * @param musicBookDTO
	 *            the musicBookDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         musicBookDTO, or with status 400 (Bad Request) if the musicBook has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/music-books", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MusicBookDTO> createMusicBook(@Valid @RequestBody MusicBookDTO musicBookDTO)
			throws URISyntaxException {
		log.debug("REST request to save MusicBook : {}", musicBookDTO);
		if (musicBookDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("musicBook", "idexists", "A new musicBook cannot already have an ID"))
					.body(null);
		}
		MusicBookDTO result = musicBookService.save(musicBookDTO);
		return ResponseEntity.created(new URI("/api/music-books/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("musicBook", result.getId().toString())).body(result);
	}

	/**
	 * PUT /music-books : Updates an existing musicBook.
	 *
	 * @param musicBookDTO
	 *            the musicBookDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         musicBookDTO, or with status 400 (Bad Request) if the musicBookDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         musicBookDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/music-books", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MusicBookDTO> updateMusicBook(@Valid @RequestBody MusicBookDTO musicBookDTO)
			throws URISyntaxException {
		log.debug("REST request to update MusicBook : {}", musicBookDTO);
		if (musicBookDTO.getId() == null) {
			return createMusicBook(musicBookDTO);
		}
		MusicBookDTO result = musicBookService.save(musicBookDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("musicBook", musicBookDTO.getId().toString())).body(result);
	}

	/**
	 * GET /music-books : get all the musicBooks.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of musicBooks in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/music-books","compositions/music-books"},  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MusicBookDTO>> getAllMusicBooks(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of MusicBooks");
		Page<MusicBookDTO> page = musicBookService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/music-books");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /music-books/:id : get the "id" musicBook.
	 *
	 * @param id
	 *            the id of the musicBookDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         musicBookDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/music-books/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MusicBookDTO> getMusicBook(@PathVariable Long id) {
		log.debug("REST request to get MusicBook : {}", id);
		MusicBookDTO musicBookDTO = musicBookService.findOne(id);
		return Optional.ofNullable(musicBookDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /music-books/:id : delete the "id" musicBook.
	 *
	 * @param id
	 *            the id of the musicBookDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/music-books/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteMusicBook(@PathVariable Long id) {
		log.debug("REST request to delete MusicBook : {}", id);
		musicBookService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("musicBook", id.toString())).build();
	}

}
