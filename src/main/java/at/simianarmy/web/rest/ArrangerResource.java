package at.simianarmy.web.rest;

import at.simianarmy.service.ArrangerService;
import at.simianarmy.service.dto.ArrangerDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Arranger.
 */
@RestController
@RequestMapping("/api")
public class ArrangerResource {

	private final Logger log = LoggerFactory.getLogger(ArrangerResource.class);

	@Inject
	private ArrangerService arrangerService;

	/**
	 * POST /arrangers : Create a new arranger.
	 *
	 * @param arrangerDTO
	 *            the arrangerDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         arrangerDTO, or with status 400 (Bad Request) if the arranger has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/arrangers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ArrangerDTO> createArranger(@Valid @RequestBody ArrangerDTO arrangerDTO)
			throws URISyntaxException {
		log.debug("REST request to save Arranger : {}", arrangerDTO);
		if (arrangerDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("arranger", "idexists", "A new arranger cannot already have an ID"))
					.body(null);
		}
		ArrangerDTO result = arrangerService.save(arrangerDTO);
		return ResponseEntity.created(new URI("/api/arrangers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("arranger", result.getId().toString())).body(result);
	}

	/**
	 * PUT /arrangers : Updates an existing arranger.
	 *
	 * @param arrangerDTO
	 *            the arrangerDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         arrangerDTO, or with status 400 (Bad Request) if the arrangerDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         arrangerDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/arrangers", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ArrangerDTO> updateArranger(@Valid @RequestBody ArrangerDTO arrangerDTO)
			throws URISyntaxException {
		log.debug("REST request to update Arranger : {}", arrangerDTO);
		if (arrangerDTO.getId() == null) {
			return createArranger(arrangerDTO);
		}
		ArrangerDTO result = arrangerService.save(arrangerDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("arranger", arrangerDTO.getId().toString())).body(result);
	}

	/**
	 * GET /arrangers : get all the arrangers.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of arrangers in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/arrangers", "compositions/arrangers"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ArrangerDTO>> getAllArrangers(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Arrangers");
		Page<ArrangerDTO> page = arrangerService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/arrangers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /arrangers/:id : get the "id" arranger.
	 *
	 * @param id
	 *            the id of the arrangerDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         arrangerDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/arrangers/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ArrangerDTO> getArranger(@PathVariable Long id) {
		log.debug("REST request to get Arranger : {}", id);
		ArrangerDTO arrangerDTO = arrangerService.findOne(id);
		return Optional.ofNullable(arrangerDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /arrangers/:id : delete the "id" arranger.
	 *
	 * @param id
	 *            the id of the arrangerDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/arrangers/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteArranger(@PathVariable Long id) {
		log.debug("REST request to delete Arranger : {}", id);
		arrangerService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("arranger", id.toString())).build();
	}

}
