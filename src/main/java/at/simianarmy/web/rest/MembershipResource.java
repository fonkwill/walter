package at.simianarmy.web.rest;

import at.simianarmy.service.MembershipService;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Membership.
 */
@RestController
@RequestMapping("/api")
public class MembershipResource {

	private final Logger log = LoggerFactory.getLogger(MembershipResource.class);

	@Inject
	private MembershipService membershipService;

	/**
	 * POST /memberships : Create a new membership.
	 *
	 * @param membershipDTO
	 *            the membershipDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         membershipDTO, or with status 400 (Bad Request) if the membership has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/memberships", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipDTO> createMembership(@Valid @RequestBody MembershipDTO membershipDTO)
			throws URISyntaxException {
		log.debug("REST request to save Membership : {}", membershipDTO);
		if (membershipDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("membership", "idexists",
					"A new membership cannot already have an ID")).body(null);
		}
		MembershipDTO result = membershipService.save(membershipDTO);
		return ResponseEntity.created(new URI("/api/memberships/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("membership", result.getId().toString())).body(result);
	}

	/**
	 * PUT /memberships : Updates an existing membership.
	 *
	 * @param membershipDTO
	 *            the membershipDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         membershipDTO, or with status 400 (Bad Request) if the membershipDTO
	 *         is not valid, or with status 500 (Internal Server Error) if the
	 *         membershipDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/memberships", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipDTO> updateMembership(@Valid @RequestBody MembershipDTO membershipDTO)
			throws URISyntaxException {
		log.debug("REST request to update Membership : {}", membershipDTO);
		if (membershipDTO.getId() == null) {
			return createMembership(membershipDTO);
		}
		MembershipDTO result = membershipService.save(membershipDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("membership", membershipDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /memberships : get all the memberships.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of memberships
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/memberships", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipDTO>> getAllMemberships(
			@RequestParam(name = "personId", required = false) Long personId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Memberships");
		Page<MembershipDTO> page = membershipService.findAll(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/memberships");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /memberships/:id : get the "id" membership.
	 *
	 * @param id
	 *            the id of the membershipDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         membershipDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/memberships/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipDTO> getMembership(@PathVariable Long id) {
		log.debug("REST request to get Membership : {}", id);
		MembershipDTO membershipDTO = membershipService.findOne(id);
		return Optional.ofNullable(membershipDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /memberships/:id : delete the "id" membership.
	 *
	 * @param id
	 *            the id of the membershipDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/memberships/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteMembership(@PathVariable Long id) {
		log.debug("REST request to delete Membership : {}", id);
		membershipService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("membership", id.toString())).build();
	}

	/**
	 * GET memberships/:id/periods : get all the membershipFeeForPeriod for a
	 * Person.
	 *
	 * @param personId
	 *            the id of the Person, which we want to retrieve periods from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         instrumentServices in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/memberships/{personId}/periods", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipFeeForPeriodDTO>> getPeriodsByPerson(@PathVariable Long personId,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get all Periods for a Person");
		Page<MembershipFeeForPeriodDTO> page = membershipService.findByPerson(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/periods");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/memberships/{personId}/periods", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipDTO>> createNewMembershipForPeriod(@RequestBody MembershipFeeForPeriodDTO mfp,
			@PathVariable Long personId) throws URISyntaxException {
		log.debug("REST request to create new MemberhipFeeForPeriod");
		List<MembershipDTO> list = membershipService.createMembershipFeeForPeriodFor(personId, mfp.getYear());
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}
