package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;
import at.simianarmy.service.MembershipFeeForPeriodService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MembershipFeeForPeriod.
 */
@RestController
@RequestMapping("/api")
public class MembershipFeeForPeriodResource {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeForPeriodResource.class);

	@Inject
	private MembershipFeeForPeriodService membershipFeeForPeriodService;

	/**
	 * POST /membership-fee-for-periods : Create a new membershipFeeForPeriod.
	 *
	 * @param membershipFeeForPeriodDTO
	 *            the membershipFeeForPeriodDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         membershipFeeForPeriodDTO, or with status 400 (Bad Request) if the
	 *         membershipFeeForPeriod has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fee-for-periods", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeForPeriodDTO> createMembershipFeeForPeriod(
			@Valid @RequestBody MembershipFeeForPeriodDTO membershipFeeForPeriodDTO) throws URISyntaxException {
		log.debug("REST request to save MembershipFeeForPeriod : {}", membershipFeeForPeriodDTO);
		if (membershipFeeForPeriodDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("membershipFeeForPeriod",
					"idexists", "A new membershipFeeForPeriod cannot already have an ID")).body(null);
		}
		MembershipFeeForPeriodDTO result = membershipFeeForPeriodService.save(membershipFeeForPeriodDTO);
		return ResponseEntity.created(new URI("/api/membership-fee-for-periods/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("membershipFeeForPeriod", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /membership-fee-for-periods : Updates an existing membershipFeeForPeriod.
	 *
	 * @param membershipFeeForPeriodDTO
	 *            the membershipFeeForPeriodDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         membershipFeeForPeriodDTO, or with status 400 (Bad Request) if the
	 *         membershipFeeForPeriodDTO is not valid, or with status 500 (Internal
	 *         Server Error) if the membershipFeeForPeriodDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fee-for-periods", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeForPeriodDTO> updateMembershipFeeForPeriod(
			@Valid @RequestBody MembershipFeeForPeriodDTO membershipFeeForPeriodDTO) throws URISyntaxException {
		log.debug("REST request to update MembershipFeeForPeriod : {}", membershipFeeForPeriodDTO);
		if (membershipFeeForPeriodDTO.getId() == null) {
			return createMembershipFeeForPeriod(membershipFeeForPeriodDTO);
		}
		MembershipFeeForPeriodDTO result = membershipFeeForPeriodService.save(membershipFeeForPeriodDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("membershipFeeForPeriod",
				membershipFeeForPeriodDTO.getId().toString())).body(result);
	}

	/**
	 * GET /membership-fee-for-periods : get all the membershipFeeForPeriods.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         membershipFeeForPeriods in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/membership-fee-for-periods", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipFeeForPeriodDTO>> getAllMembershipFeeForPeriods(Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of MembershipFeeForPeriods");
		Page<MembershipFeeForPeriodDTO> page = membershipFeeForPeriodService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/membership-fee-for-periods");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /membership-fee-for-periods/:id : get the "id" membershipFeeForPeriod.
	 *
	 * @param id
	 *            the id of the membershipFeeForPeriodDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         membershipFeeForPeriodDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/membership-fee-for-periods/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeForPeriodDTO> getMembershipFeeForPeriod(@PathVariable Long id) {
		log.debug("REST request to get MembershipFeeForPeriod : {}", id);
		MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodService.findOne(id);
		return Optional.ofNullable(membershipFeeForPeriodDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /membership-fee-for-periods/:id : delete the "id"
	 * membershipFeeForPeriod.
	 *
	 * @param id
	 *            the id of the membershipFeeForPeriodDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/membership-fee-for-periods/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteMembershipFeeForPeriod(@PathVariable Long id) {
		log.debug("REST request to delete MembershipFeeForPeriod : {}", id);
		membershipFeeForPeriodService.delete(id);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityDeletionAlert("membershipFeeForPeriod", id.toString())).build();
	}

}
