package at.simianarmy.web.rest;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.service.BankImportDataService;
import at.simianarmy.service.dto.BankImportAPIRequestDTO;
import at.simianarmy.service.dto.BankImportDataDTO;
import at.simianarmy.service.dto.CashbookEntryDTO;

import at.simianarmy.web.rest.util.HeaderUtil;
import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import java.util.Optional;
import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * REST controller for managing BankImportData.
 */
@RestController
@RequestMapping("/api")
public class BankImportDataResource {

	private final Logger log = LoggerFactory.getLogger(BankImportDataResource.class);

	@Inject
	private BankImportDataService bankImportDataService;

	/**
	 * GET /bank-import-data : get all the bankImportData.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         bankImportData in body
	 */
	@RequestMapping(value = "/bank-import-data", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<BankImportDataDTO> getAllBankImportData() {
		log.debug("REST request to get all BankImportData");
		return bankImportDataService.findAll();
	}

	/**
	 * POST /bank-import-data : imporat bankImportData into Cashbook.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         CashbookEntries in body
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "/bank-import-data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<CashbookEntryDTO>> importBankImportData(
			@Valid @RequestBody List<BankImportDataDTO> bankImportDataDTOs,
      @RequestParam(value = "createReceipts", defaultValue = "true") Boolean createReceipts) throws URISyntaxException {
		log.debug("REST request to importBankImportData: {} {}", createReceipts, bankImportDataDTOs);

		List<CashbookEntryDTO> result = this.bankImportDataService.importBankImportData(bankImportDataDTOs, createReceipts);

		return ResponseEntity.created(new URI("/api/bank-import-data/")).body(result);
	}

	/**
	 * POST /bank-import-data/csv : upload a CSV-File for Importing into Cashbook.
	 * 
	 * @param csvFile
	 *            The CSV-File
	 * @param cashbookAccountId
	 *            The Cashbook-Account to use
	 * @return List of Created Imports
	 */
	@RequestMapping(value = "/bank-import-data/csv", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<BankImportDataDTO>> uploadCSVFile(
			@RequestParam(value = "csvFile", required = true) MultipartFile csvFile,
			@RequestParam(value = "cashbookAccountId", required = true) Long cashbookAccountId,
      @RequestParam(value = "cashbookCategoryId", required = false) Long cashbookCategoryId)
			throws URISyntaxException {
		log.debug("REST request to upload CSV for Bank-Import!");

		List<BankImportDataDTO> result = this.bankImportDataService.uploadCSVFile(csvFile, cashbookAccountId, cashbookCategoryId);

		return ResponseEntity.created(new URI("/api/bank-import-data/csv")).body(result);
	}

	/**
	 * POST /bank-import-data/api : Import Data from API for Importing into
	 * Cashbook.
	 *
   * @param dto
   *          The DTO to import
	 * @return List of Created imports
	 */
	@RequestMapping(value = "/bank-import-data/api", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<BankImportDataDTO>> importDataFromAPI(@Valid @RequestBody BankImportAPIRequestDTO dto)
			throws URISyntaxException {
		log.debug("REST request to import Data from API: " + dto);

		List<BankImportDataDTO> result = this.bankImportDataService.importFromAPI(dto.getUsername(), dto.getPassword(),
				dto.getFrom(), dto.getTo(), dto.getCashbookAccountId(), dto.getCashbookCategoryId());
		return ResponseEntity.created(new URI("/api/bank-import-data/api")).body(result);
	}

  /**
   * GET /bank-import-data/:id : get the "id" bank-import-data.
   *
   * @param id
   *            the id of the bankImportDataDTO to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the awardDTO,
   *         or with status 404 (Not Found)
   */
  @RequestMapping(value = "/bank-import-data/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<BankImportDataDTO> getBankImportData(@PathVariable Long id) {
    log.debug("REST request to get BankImportData : {}", id);
    BankImportDataDTO bankImportDataDTO = bankImportDataService.findOne(id);
    return Optional
      .ofNullable(bankImportDataDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * PUT /bank-import-data : Updates an existing award.
   *
   * @param bankImportDataDTO
   *            the bankImportDataDTO to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated
   *         bankImportDataDTO, or with status 400 (Bad Request) if the bankImportDataDTO is not
   *         valid, or with status 500 (Internal Server Error) if the bankImportDataDTO
   *         couldnt be updated
   * @throws URISyntaxException
   *             if the Location URI syntax is incorrect
   */
  @RequestMapping(value = "/bank-import-data", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<BankImportDataDTO> updateBankImportData(@Valid @RequestBody BankImportDataDTO bankImportDataDTO) throws URISyntaxException {
    log.debug("REST request to update Award : {}", bankImportDataDTO);
    if (bankImportDataDTO.getId() == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    BankImportDataDTO result = bankImportDataService.save(bankImportDataDTO);
    return ResponseEntity.ok().headers(
      HeaderUtil.createEntityUpdateAlert("bank-import-data", bankImportDataDTO.getId().toString()))
      .body(result);
  }
}
