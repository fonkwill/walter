package at.simianarmy.web.rest;

import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.service.TemplateFileStoreService;
import at.simianarmy.service.TemplateService;
import at.simianarmy.service.dto.TemplateDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * REST controller for managing Template.
 */
@RestController
@RequestMapping("/api")
public class TemplateResource {

	private final Logger log = LoggerFactory.getLogger(TemplateResource.class);

	@Inject
	private TemplateService templateService;

	@Inject
	private TemplateFileStoreService fileStore;

	/**
	 * POST /receipts : Create a new template.
	 *
	 * @param uploadedFile
	 *            the newly uploaded file
	 * @param id
	 *            the template id
	 * @param title
	 *            the title
	 * @param marginLeft
	 *            the left margin
	 * @param marginRight
	 *            the right margin
	 * @param marginTop
	 *            the top margin
	 * @param marginBottom
	 *            the bottom margin
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         templateDTO, or with status 400 (Bad Request) if the template has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/templates", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TemplateDTO> createTemplate(
			@RequestParam(value = "uploadFile", required = false) MultipartFile uploadedFile,
			@RequestParam(value = "template[id]", required = false) Long id,
			@RequestParam(value = "template[title]", required = true) String title,
			@RequestParam(value = "template[marginLeft]", required = false) Float marginLeft,
			@RequestParam(value = "template[marginRight]", required = false) Float marginRight,
			@RequestParam(value = "template[marginTop]", required = false) Float marginTop,
			@RequestParam(value = "template[marginBottom]", required = false) Float marginBottom)
			throws URISyntaxException {
    TemplateDTO templateDTO = createTemplateDTO(uploadedFile, id, title, marginLeft, marginRight, marginTop, marginBottom);

    log.debug("REST request to save Template : {}", templateDTO);
		if (templateDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("template", "idexists", "A new template cannot already have an ID"))
					.body(null);
		}
		TemplateDTO result = templateService.save(templateDTO);
		return ResponseEntity.created(new URI("/api/templates/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("template", result.getId().toString())).body(result);
	}

	/**
	 * PUT /templates : Updates an existing template.
	 *
	 * @param uploadedFile
	 *            the newly uploaded file
	 * @param id
	 *            the template id
	 * @param title
	 *            the title
	 * @param marginLeft
	 *            the left margin
	 * @param marginRight
	 *            the right margin
	 * @param marginTop
	 *            the top margin
	 * @param marginBottom
	 *            the bottom margin
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         templateDTO, or with status 400 (Bad Request) if the templateDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         templateDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/templates/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TemplateDTO> updateTemplate(
			@RequestParam(value = "uploadFile", required = false) MultipartFile uploadedFile,
			@RequestParam(value = "template[id]", required = false) Long id,
			@RequestParam(value = "template[title]", required = true) String title,
			@RequestParam(value = "template[marginLeft]", required = false) Float marginLeft,
			@RequestParam(value = "template[marginRight]", required = false) Float marginRight,
			@RequestParam(value = "template[marginTop]", required = false) Float marginTop,
			@RequestParam(value = "template[marginBottom]", required = false) Float marginBottom)
			throws URISyntaxException {
    TemplateDTO templateDTO = createTemplateDTO(uploadedFile, id, title, marginLeft, marginRight, marginTop, marginBottom);

    log.debug("REST request to update Template : {}", templateDTO);
		if (templateDTO.getId() == null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("template", "idunavailable", "A existing template must have an ID"))
					.body(null);
		}
		TemplateDTO result = templateService.save(templateDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("template", templateDTO.getId().toString())).body(result);
	}

  private TemplateDTO createTemplateDTO(@RequestParam(value = "uploadFile", required = false) MultipartFile uploadedFile, @RequestParam(value = "template[id]", required = false) Long id, @RequestParam(value = "template[title]", required = true) String title, @RequestParam(value = "template[marginLeft]", required = false) Float marginLeft, @RequestParam(value = "template[marginRight]", required = false) Float marginRight, @RequestParam(value = "template[marginTop]", required = false) Float marginTop, @RequestParam(value = "template[marginBottom]", required = false) Float marginBottom) {
    TemplateDTO templateDTO = new TemplateDTO();
    templateDTO.setId(id);
    templateDTO.setTitle(title);
    templateDTO.setMarginLeft(marginLeft);
    templateDTO.setMarginRight(marginRight);
    templateDTO.setMarginTop(marginTop);
    templateDTO.setMarginBottom(marginBottom);
    templateDTO.setUploadedFile(uploadedFile);
    return templateDTO;
  }

  /**
	 * GET /templates : get all the templates.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of templates in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/templates","/letters/templates", "/demand-letters/templates", "/email-verification-letters/templates"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<TemplateDTO>> getAllTemplates(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Templates");
		Page<TemplateDTO> page = templateService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/templates");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /templates/:id : get the "id" template.
	 *
	 * @param id
	 *            the id of the templateDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         templateDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/templates/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<TemplateDTO> getTemplate(@PathVariable Long id) {
		log.debug("REST request to get Template : {}", id);
		TemplateDTO templateDTO = templateService.findOne(id);
		return Optional.ofNullable(templateDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /templates/:id : delete the "id" template.
	 *
	 * @param id
	 *            the id of the templateDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/templates/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteTemplate(@PathVariable Long id) {
		log.debug("REST request to delete Template : {}", id);
		templateService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("template", id.toString())).build();
	}

	/**
	 * returns the contents of a file.
	 * 
	 * @param uuid
	 *            the uuid of the file
	 * @return the ResponseEntity
	 */
	@GetMapping("/templates/file/{uuid:.+}")
	@Timed
	public ResponseEntity<Resource> getResource(@PathVariable String uuid) {
		try {
			FileResourceDTO file = this.fileStore.getTemplate(UUID.fromString(uuid));
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION,
							"attachment; filename=\"" + file.getFileName() + "\"")
					.body(file.getResource());
		} catch (FileStoreException exception) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
