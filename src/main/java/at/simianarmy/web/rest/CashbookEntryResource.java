package at.simianarmy.web.rest;

import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.service.CashbookEntryService;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import javax.ws.rs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing CashbookEntry.
 */
@RestController
@RequestMapping("/api")
public class CashbookEntryResource {

	private final Logger log = LoggerFactory.getLogger(CashbookEntryResource.class);

	@Inject
	private CashbookEntryService cashbookEntryService;

  /**
   * GET /cashbook-overall-report/pdf : Get the Overall Report with the given params
   *
   * @param dateFrom
   *          the date from
   * @param dateTo
   *          the date to
   * @param appointmentTypeId
   *          the Appointment Type
   * @return the PDF-Report
   */
	@RequestMapping(value = "/cashbook-appointment-report/pdf", method = RequestMethod.GET)
	@Timed
	public ResponseEntity<Resource> generateCashbookAppointmentReport(
      @RequestParam(value = "dateFrom") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
      @RequestParam(value = "dateTo") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
      @RequestParam(value = "appointmentTypeId") List<Long> appointmentTypeId) throws URISyntaxException {
      log.debug("REST request to get Cashbook Appointments Report : {} {} {}", dateFrom, dateTo,
        appointmentTypeId);
      Resource file = cashbookEntryService
        .getCashbookAppointmentReport(dateFrom, dateTo, appointmentTypeId);
      return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"report.pdf\"")
        .body(file);
	}

  /**
   * GET /cashbook-overall-report/pdf : Get the Overall Report with the given params
   *
   * @param year
   *          the year
   * @param cashbookAccountIds
   *          the cashbookAccountIds
   * @param cashbookCategoryIds
   *          the cashbookCategoryIds
   * @return the PDF-Report
   */
  @RequestMapping(value = "/cashbook-overall-report/pdf", method = RequestMethod.GET)
  @Timed
  public ResponseEntity<Resource> generateCashbookOverallReport(
    @RequestParam(value = "year") Integer year,
    @RequestParam(value = "cashbookAccountIds", required = false) List<Long> cashbookAccountIds,
    @RequestParam(value = "cashbookCategoryIds", required = false) List<Long> cashbookCategoryIds) throws URISyntaxException {
      log.debug("REST request to get Cashbook Overall Report : {} {} {}", year, cashbookAccountIds, cashbookCategoryIds);
      Resource file = cashbookEntryService.getCashbookOverallReport(year, cashbookAccountIds, cashbookCategoryIds);
      return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"report.pdf\"")
        .body(file);

  }

	/**
	 * POST /cashbook-entries : Create a new cashbookEntry.
	 *
	 * @param cashbookEntryDTO
	 *            the cashbookEntryDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         cashbookEntryDTO, or with status 400 (Bad Request) if the
	 *         cashbookEntry has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/cashbook-entries")
	@Timed
	public ResponseEntity<CashbookEntryDTO> createCashbookEntry(@Valid @RequestBody CashbookEntryDTO cashbookEntryDTO)
			throws URISyntaxException {
		log.debug("REST request to save CashbookEntry : {}", cashbookEntryDTO);
		if (cashbookEntryDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("cashbookEntry", "idexists",
					"A new cashbookEntry cannot already have an ID")).body(null);
		}
		CashbookEntryDTO result = cashbookEntryService.save(cashbookEntryDTO);
		return ResponseEntity.created(new URI("/api/cashbook-entries/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("cashbookEntry", result.getId().toString())).body(result);
	}

	/**
	 * PUT /cashbook-entries : Updates an existing cashbookEntry.
	 *
	 * @param cashbookEntryDTO
	 *            the cashbookEntryDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         cashbookEntryDTO, or with status 400 (Bad Request) if the
	 *         cashbookEntryDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the cashbookEntryDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/cashbook-entries")
	@Timed
	public ResponseEntity<CashbookEntryDTO> updateCashbookEntry(@Valid @RequestBody CashbookEntryDTO cashbookEntryDTO)
			throws URISyntaxException {
		log.debug("REST request to update CashbookEntry : {}", cashbookEntryDTO);
		if (cashbookEntryDTO.getId() == null) {
			return createCashbookEntry(cashbookEntryDTO);
		}
		CashbookEntryDTO result = cashbookEntryService.save(cashbookEntryDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("cashbookEntry", cashbookEntryDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /cashbook-entries : get all the cashbookEntries.
	 *
	 * @param title
	 *            the title
	 * @param categoryId
	 *            the id of the category
	 * @param type
	 *            the cashbook entry type
	 * @param dateFrom
	 *            the lower bound of the date range
	 * @param dateTo
	 *            the upper bound of the date range
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         cashbookEntries in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/cashbook-entries")
	@Timed
	public ResponseEntity<List<CashbookEntryDTO>> getAllCashbookEntries(
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "categoryId", required = false) Long categoryId,
			@RequestParam(value = "accountId", required = false) Long accountId,
			@RequestParam(value = "type", required = false) CashbookEntryType type,
			@RequestParam(value = "dateFrom", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
			@RequestParam(value = "dateTo", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of CashbookEntries");
		Page<CashbookEntryDTO> page = cashbookEntryService.findAll(title, categoryId, type, dateFrom, dateTo, accountId,
				pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cashbook-entries");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /cashbook-entries/:id : get the "id" cashbookEntry.
	 *
	 * @param id
	 *            the id of the cashbookEntryDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         cashbookEntryDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/cashbook-entries/{id}")
	@Timed
	public ResponseEntity<CashbookEntryDTO> getCashbookEntry(@PathVariable Long id) {
		log.debug("REST request to get CashbookEntry : {}", id);
		CashbookEntryDTO cashbookEntryDTO = cashbookEntryService.findOne(id);
		return Optional.ofNullable(cashbookEntryDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /cashbook-entries/:id : delete the "id" cashbookEntry.
	 *
	 * @param id
	 *            the id of the cashbookEntryDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/cashbook-entries/{id}")
	@Timed
	public ResponseEntity<Void> deleteCashbookEntry(@PathVariable Long id) {
		log.debug("REST request to delete CashbookEntry : {}", id);
		cashbookEntryService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("cashbookEntry", id.toString()))
				.build();
	}

}
