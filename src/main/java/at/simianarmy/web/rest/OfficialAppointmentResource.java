package at.simianarmy.web.rest;

import at.simianarmy.service.CompositionService;
import at.simianarmy.service.OfficialAppointmentService;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.dto.OfficialAppointmentDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing OfficialAppointment.
 */
@RestController
@RequestMapping("/api")
public class OfficialAppointmentResource {

	private final Logger log = LoggerFactory.getLogger(OfficialAppointmentResource.class);

	@Inject
	private OfficialAppointmentService officialAppointmentService;

	@Inject
	private CompositionService compositionService;




	/**
	 * GET /official-appointments/id/compositions : get all compositions for an
	 * officialAppointment.
	 *
	 * @param id
	 *            id of the officialAppointment
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of compositions
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value = "/official-appointments/{id}/compositions")
	@Timed
	public ResponseEntity<List<CompositionDTO>> getCompositionsForAppointment(@PathVariable("id") Long id,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Compositions for an official Appointment");
		Page<CompositionDTO> page;
		page = compositionService.findByOfficialAppointment(id, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/official-appointments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * PUT /official-appointments/:id/compositions : Add a Composition to an
	 * officialAppointment.
	 *
	 * @param id
	 *            the officialAppointment where to add the Composition
	 * @param compositionDTO
	 *            the composition to add
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         compositionDTO, or with status 400 (Bad Request) if the composition
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/official-appointments/{id}/compositions")
	@Timed
	public ResponseEntity<OfficialAppointmentDTO> addCompositionToOfficialAppointment(@PathVariable("id") Long id,
			@Valid @RequestBody CompositionDTO compositionDTO) throws URISyntaxException {
		log.debug("REST request to add a Composition to an officialAppointment : {}", id, compositionDTO);

		OfficialAppointmentDTO result = this.officialAppointmentService.addComposition(id, compositionDTO);

		return ResponseEntity.created(new URI("/api/official-appointments/" + id))
				.headers(HeaderUtil.createEntityCreationAlert("officialAppointment", id.toString())).body(result);
	}

	/**
	 * DELETE /official-appointments/:id/compositions/:compositionId : remove the
	 * "compositionId" composition.
	 *
	 * @param id
	 *            the id of the officialAppointment
	 * @param compositionId
	 *            the id of the composition to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/official-appointments/{id}/compositions/{compositionId}")
	@Timed
	public ResponseEntity<Void> removeCompositionFromOfficialAppointment(@PathVariable Long id,
			@PathVariable Long compositionId) {
		log.debug("REST request to delete Composition from officialAppointment : {}", id, compositionId);
		this.officialAppointmentService.removeComposition(id, compositionId);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("official-appointments", id.toString()))
				.build();
	}
	
	/**
	 * GET /compositions : get all the compositions for addding to appointment.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of compositions
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value = "/official-appointments/compositions")
	@Timed
	public ResponseEntity<List<CompositionDTO>> getAllCompositionsetAllCompositions(
			@RequestParam(value = "active", required = false) Boolean isActive, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Compositions");
		Page<CompositionDTO> page;
		page = compositionService.findAll(null, null, null, null, null, null, null,
				null, null, isActive, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/compositions");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
