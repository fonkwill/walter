package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;
import at.simianarmy.service.ClothingGroupService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.ClothingGroupDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ClothingGroup.
 */
@RestController
@RequestMapping("/api")
public class ClothingGroupResource {

	private final Logger log = LoggerFactory.getLogger(ClothingGroupResource.class);

	@Inject
	private ClothingGroupService clothingGroupService;

	/**
	 * POST /clothing-groups : Create a new clothingGroup.
	 *
	 * @param clothingGroupDTO
	 *            the clothingGroupDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         clothingGroupDTO, or with status 400 (Bad Request) if the
	 *         clothingGroup has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing-groups", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingGroupDTO> createClothingGroup(@RequestBody ClothingGroupDTO clothingGroupDTO)
			throws URISyntaxException {
		log.debug("REST request to save ClothingGroup : {}", clothingGroupDTO);
		if (clothingGroupDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("clothingGroup", "idexists",
					"A new clothingGroup cannot already have an ID")).body(null);
		}
		ClothingGroupDTO result = clothingGroupService.save(clothingGroupDTO);
		return ResponseEntity.created(new URI("/api/clothing-groups/"))
				.headers(HeaderUtil.createEntityCreationAlert("clothingGroup", "group")).body(result);
	}

	/**
	 * PUT /clothing-groups : Updates an existing clothingGroup.
	 *
	 * @param clothingGroupDTO
	 *            the clothingGroupDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         clothingGroupDTO, or with status 400 (Bad Request) if the
	 *         clothingGroupDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the clothingGroupDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/clothing-groups", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingGroupDTO> updateClothingGroup(@RequestBody ClothingGroupDTO clothingGroupDTO)
			throws URISyntaxException {
		log.debug("REST request to update ClothingGroup : {}", clothingGroupDTO);
		if (clothingGroupDTO.getId() == null) {
			return createClothingGroup(clothingGroupDTO);
		}
		ClothingGroupDTO result = clothingGroupService.save(clothingGroupDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("clothingGroup", clothingGroupDTO.toString())).body(result);
	}

	/**
	 * GET /clothing-groups : get all the clothingGroups.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         clothingGroups in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/clothing-groups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ClothingGroupDTO>> getAllClothingGroups(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of ClothingGroups");
		Page<ClothingGroupDTO> page = clothingGroupService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clothing-groups");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /clothing-groups/:id : get the "id" clothingGroup.
	 *
	 * @param id
	 *            the id of the clothingGroupDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         clothingGroupDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/clothing-groups/{csize}/{typeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ClothingGroupDTO> getClothingGroup(@PathVariable String csize, @PathVariable Long typeId) {
		log.debug("REST request to get ClothingGroup : {}");
		ClothingGroupDTO clothingGroupDTO = clothingGroupService.findOne(csize, typeId);
		return Optional.ofNullable(clothingGroupDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /clothing-groups/:id : delete the "id" clothingGroup.
	 *
	 * @param id
	 *            the id of the clothingGroupDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/clothing-groups/", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteClothingGroup(@RequestBody ClothingGroupDTO clothingGroupDTO) {
		log.debug("REST request to delete ClothingGroup : {}", clothingGroupDTO);
		clothingGroupService.delete(clothingGroupDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityDeletionAlert("clothingGroup", clothingGroupDTO.toString())).build();
	}

}
