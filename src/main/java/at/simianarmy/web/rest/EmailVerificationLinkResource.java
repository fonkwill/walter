package at.simianarmy.web.rest;

import at.simianarmy.service.EmailVerificationLinkService;
import at.simianarmy.service.dto.EmailVerificationLinkDTO;
import at.simianarmy.service.dto.EmailVerificationRequestDTO;
import com.codahale.metrics.annotation.Timed;
import java.util.Optional;
import javax.inject.Inject;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing EmailVerificationLink.
 */
@RestController
@RequestMapping("/api")
public class EmailVerificationLinkResource {

  private final Logger log = LoggerFactory.getLogger(EmailVerificationLinkResource.class);

  @Inject
  private EmailVerificationLinkService emailVerificationLinkService;

  @RequestMapping(value = "/email-verification/{personId}", method = RequestMethod.GET)
  @Timed
  public ResponseEntity checkActiveEmailVerificationLink(
    @PathVariable Long personId) {

    this.log.debug("REST-Request to check if Person {} has active VerificationLink", personId);

    if (this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(personId) != null) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @RequestMapping(value = "/email-verification", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  @ResponseStatus(value = HttpStatus.OK)
  public void verifyEmailAddress(
    @RequestBody @Valid EmailVerificationRequestDTO emailVerificationRequestDTO) {

    this.log.debug("REST-Request to verify EmailAddress personId: {}, email:{}", emailVerificationRequestDTO.getPersonId(), emailVerificationRequestDTO.getEmail());

    this.emailVerificationLinkService.verifyEmailVerificationLinkForPersonWithEmailAndSecretKey(
      emailVerificationRequestDTO.getPersonId(),
      emailVerificationRequestDTO.getEmail(),
      emailVerificationRequestDTO.getSecretKey()
    );
  }

  @RequestMapping(value = "/email-verification-link/{personId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<EmailVerificationLinkDTO> createNewEmailVerificationLinkForPerson(
    @PathVariable Long personId) {

    log.debug("REST request to create new EmailVerificationLink for Person : {}", personId);

    EmailVerificationLinkDTO dto = this.emailVerificationLinkService.createEmailVerificationLinkForPerson(personId);

    return Optional.ofNullable(dto).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
  }

  @RequestMapping(value = "/email-verification-link/{personId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<EmailVerificationLinkDTO> getEmailVerificationLinkForPerson(
    @PathVariable Long personId) {

    log.debug("REST request to get current active EmailVerificationLink for Person: {}", personId);

    EmailVerificationLinkDTO dto = this.emailVerificationLinkService.getEmailVerificationLinkForPerson(personId);

    return Optional.ofNullable(dto).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.OK));
  }

  @RequestMapping(value = "/email-verification-link/{personId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<EmailVerificationLinkDTO> invalidateEmailVerificationLinkForPerson(
    @PathVariable Long personId) {

    log.debug("REST request to invalidate current active EmailVerificationLink for Person: {}", personId);

    EmailVerificationLinkDTO dto = this.emailVerificationLinkService.invalidateEmailVerificationLinkForPerson(personId);

    return Optional.ofNullable(dto).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
  }

}
