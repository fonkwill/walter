package at.simianarmy.web.rest;

import at.simianarmy.service.HonorService;
import at.simianarmy.service.PersonHonorService;
import at.simianarmy.service.dto.HonorDTO;
import at.simianarmy.service.dto.PersonHonorDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Honor.
 */
@RestController
@RequestMapping("/api")
public class HonorResource {

	private final Logger log = LoggerFactory.getLogger(HonorResource.class);

	@Inject
	private HonorService honorService;

	@Inject
	private PersonHonorService personHonorService;

	/**
	 * POST /honors : Create a new honor.
	 *
	 * @param honorDTO
	 *            the honorDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         honorDTO, or with status 400 (Bad Request) if the honor has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/honors", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<HonorDTO> createHonor(@Valid @RequestBody HonorDTO honorDTO) throws URISyntaxException {
		log.debug("REST request to save Honor : {}", honorDTO);
		if (honorDTO.getId() != null) {
			return ResponseEntity.badRequest()
					.headers(
							HeaderUtil.createFailureAlert("honor", "idexists", "A new honor cannot already have an ID"))
					.body(null);
		}
		HonorDTO result = honorService.save(honorDTO);
		return ResponseEntity.created(new URI("/api/honors/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("honor", result.getId().toString())).body(result);
	}

	/**
	 * PUT /honors : Updates an existing honor.
	 *
	 * @param honorDTO
	 *            the honorDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         honorDTO, or with status 400 (Bad Request) if the honorDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the honorDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/honors", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<HonorDTO> updateHonor(@Valid @RequestBody HonorDTO honorDTO) throws URISyntaxException {
		log.debug("REST request to update Honor : {}", honorDTO);
		if (honorDTO.getId() == null) {
			return createHonor(honorDTO);
		}
		HonorDTO result = honorService.save(honorDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("honor", honorDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /honors : get all the honors.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of honors in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/honors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<HonorDTO>> getAllHonors(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Honors");
		Page<HonorDTO> page = honorService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/honors");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /honors/:id : get the "id" honor.
	 *
	 * @param id
	 *            the id of the honorDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the honorDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/honors/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<HonorDTO> getHonor(@PathVariable Long id) {
		log.debug("REST request to get Honor : {}", id);
		HonorDTO honorDTO = honorService.findOne(id);
		return Optional.ofNullable(honorDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /honors/:id : delete the "id" honor.
	 *
	 * @param id
	 *            the id of the honorDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/honors/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteHonor(@PathVariable Long id) {
		log.debug("REST request to delete Honor : {}", id);
		honorService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("honor", id.toString())).build();
	}

	/**
	 * GET honors/:id/personHonors : get all the personHonors for a honor.
	 *
	 * @param honorId
	 *            the id of the Honor, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of instruments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/honors/{honorId}/personHonors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonHonorDTO>> getPersonsByHonor(@PathVariable Long honorId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get all Instruments for an InstrumentType");
		Page<PersonHonorDTO> page = personHonorService.findByHonor(honorId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/honors");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
