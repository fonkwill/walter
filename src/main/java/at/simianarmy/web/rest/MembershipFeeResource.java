package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;

import at.simianarmy.service.MembershipFeeAmountService;
import at.simianarmy.service.MembershipFeeService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;
import at.simianarmy.service.dto.MembershipFeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MembershipFee.
 */
@RestController
@RequestMapping("/api")
public class MembershipFeeResource {

	private final Logger log = LoggerFactory.getLogger(MembershipFeeResource.class);

	@Inject
	private MembershipFeeService membershipFeeService;

	@Inject
	private MembershipFeeAmountService membershipFeeAmountService;

	/**
	 * POST /membership-fees : Create a new membershipFee.
	 *
	 * @param membershipFeeDTO
	 *            the membershipFeeDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         membershipFeeDTO, or with status 400 (Bad Request) if the
	 *         membershipFee has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fees", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeDTO> createMembershipFee(@Valid @RequestBody MembershipFeeDTO membershipFeeDTO)
			throws URISyntaxException {
		log.debug("REST request to save MembershipFee : {}", membershipFeeDTO);
		if (membershipFeeDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("membershipFee", "idexists",
					"A new membershipFee cannot already have an ID")).body(null);
		}
		MembershipFeeDTO result = membershipFeeService.save(membershipFeeDTO);
		return ResponseEntity.created(new URI("/api/membership-fees/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("membershipFee", result.getId().toString())).body(result);
	}

	/**
	 * PUT /membership-fees : Updates an existing membershipFee.
	 *
	 * @param membershipFeeDTO
	 *            the membershipFeeDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         membershipFeeDTO, or with status 400 (Bad Request) if the
	 *         membershipFeeDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the membershipFeeDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/membership-fees", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeDTO> updateMembershipFee(@Valid @RequestBody MembershipFeeDTO membershipFeeDTO)
			throws URISyntaxException {
		log.debug("REST request to update MembershipFee : {}", membershipFeeDTO);
		if (membershipFeeDTO.getId() == null) {
			return createMembershipFee(membershipFeeDTO);
		}
		MembershipFeeDTO result = membershipFeeService.save(membershipFeeDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("membershipFee", membershipFeeDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /membership-fees : get all the membershipFees.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         membershipFees in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/membership-fees", "people/member/membership-fees"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipFeeDTO>> getAllMembershipFees(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of MembershipFees");
		Page<MembershipFeeDTO> page = membershipFeeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/membership-fees");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /membership-fees/:id : get the "id" membershipFee.
	 *
	 * @param id
	 *            the id of the membershipFeeDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         membershipFeeDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/membership-fees/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MembershipFeeDTO> getMembershipFee(@PathVariable Long id) {
		log.debug("REST request to get MembershipFee : {}", id);
		MembershipFeeDTO membershipFeeDTO = membershipFeeService.findOne(id);
		return Optional.ofNullable(membershipFeeDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /membership-fees/:id : delete the "id" membershipFee.
	 *
	 * @param id
	 *            the id of the membershipFeeDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/membership-fees/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteMembershipFee(@PathVariable Long id) {
		log.debug("REST request to delete MembershipFee : {}", id);
		membershipFeeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("membershipFee", id.toString()))
				.build();
	}

	/**
	 * GET membership-fees/:id/amounts : get all the membershipFeeAmounts for an
	 * Instrument.
	 *
	 * @param membershipFeeId
	 *            the id of the MembershipFee, which we want to retrieve services
	 *            from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         instrumentServices in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/membership-fees/{membershipFeeId}/amounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<MembershipFeeAmountDTO>> getMembershipFeeAmountsByMembershipFee(
			@PathVariable Long membershipFeeId, Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get all Amounts for a Membership Fee");
		Page<MembershipFeeAmountDTO> page = membershipFeeAmountService.findByMembershipFee(membershipFeeId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instrument-services");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

}
