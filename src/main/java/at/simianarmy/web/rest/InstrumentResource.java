package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import at.simianarmy.service.InstrumentService;
import at.simianarmy.service.InstrumentServiceService;
import at.simianarmy.service.InstrumentTypeService;
import at.simianarmy.service.PersonInstrumentService;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.dto.InstrumentServiceDTO;
import at.simianarmy.service.dto.InstrumentTypeDTO;
import at.simianarmy.service.dto.PersonInstrumentDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Instrument.
 */
@RestController
@RequestMapping("/api")
public class InstrumentResource {

	private final Logger log = LoggerFactory.getLogger(InstrumentResource.class);

	@Inject
	private InstrumentService instrumentService;

	@Inject
	private InstrumentServiceService instrumentServiceService;

	@Inject
	private PersonInstrumentService personInstrumentService;
	
	@Inject
	private InstrumentTypeService instrumentTypeService;

	/**
	 * POST /instruments : Create a new instrument.
	 *
	 * @param instrumentDTO
	 *            the instrumentDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         instrumentDTO, or with status 400 (Bad Request) if the instrument has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instruments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentDTO> createInstrument(@Valid @RequestBody InstrumentDTO instrumentDTO)
			throws URISyntaxException {
		log.debug("REST request to save Instrument : {}", instrumentDTO);
		if (instrumentDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("instrument", "idexists",
					"A new instrument cannot already have an ID")).body(null);
		}
		InstrumentDTO result = instrumentService.save(instrumentDTO);
		return ResponseEntity.created(new URI("/api/instruments/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("instrument", result.getId().toString())).body(result);
	}

	/**
	 * PUT /instruments : Updates an existing instrument.
	 *
	 * @param instrumentDTO
	 *            the instrumentDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         instrumentDTO, or with status 400 (Bad Request) if the instrumentDTO
	 *         is not valid, or with status 500 (Internal Server Error) if the
	 *         instrumentDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/instruments", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentDTO> updateInstrument(@Valid @RequestBody InstrumentDTO instrumentDTO)
			throws URISyntaxException {
		log.debug("REST request to update Instrument : {}", instrumentDTO);
		if (instrumentDTO.getId() == null) {
			return createInstrument(instrumentDTO);
		}
		InstrumentDTO result = instrumentService.save(instrumentDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("instrument", instrumentDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /instruments : get all the instruments.
	 *
	 * @param name
	 *            the name
	 * @param type
	 *            the name of the instrument type
	 * @param privateInstrument
	 *            if the instrument is private
	 * @param dateBegin
	 *            the lower bound of the date range
	 * @param dateEnd
	 *            the upper bound of the date range
	 * @param priceBegin
	 *            the lower bound of the price range
	 * @param priceEnd
	 *            the upper bound of the price range
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of instruments
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/instruments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<InstrumentDTO>> getAllInstruments(
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "privateInstrument", required = false) Boolean privateInstrument,
			@RequestParam(value = "dateBegin", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateBegin,
			@RequestParam(value = "dateEnd", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateEnd,
			@RequestParam(value = "priceBegin", required = false) BigDecimal priceBegin,
			@RequestParam(value = "priceEnd", required = false) BigDecimal priceEnd, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of Instruments");
		Page<InstrumentDTO> page = instrumentService.findAll(name, type, privateInstrument, dateBegin, dateEnd,
				priceBegin, priceEnd, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instruments");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /instruments/:id : get the "id" instrument.
	 *
	 * @param id
	 *            the id of the instrumentDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         instrumentDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/instruments/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<InstrumentDTO> getInstrument(@PathVariable Long id) {
		log.debug("REST request to get Instrument : {}", id);
		InstrumentDTO instrumentDTO = instrumentService.findOne(id);
		return Optional.ofNullable(instrumentDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /instruments/:id : delete the "id" instrument.
	 *
	 * @param id
	 *            the id of the instrumentDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/instruments/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteInstrument(@PathVariable Long id) {
		log.debug("REST request to delete Instrument : {}", id);
		instrumentService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("instrument", id.toString())).build();
	}

	/**
	 * GET instrument/:id/instrument-services : get all the instrumentServices for
	 * an Instrument.
	 *
	 * @param instrumentId
	 *            the id of the Instrument, which we want to retrieve services from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         instrumentServices in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/instruments/{instrumentId}/instrument-services", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<InstrumentServiceDTO>> getInstrumentServicesByInstrument(@PathVariable Long instrumentId,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get all Services for an Instrument");
		Page<InstrumentServiceDTO> page = instrumentServiceService.findByInstrument(instrumentId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instrument-services");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET instrument/:id/personInstruments : get all the personInstruments for an
	 * Instrument.
	 *
	 * @param instrumentId
	 *            the id of the Instrument, which we want to retrieve persons from
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         personInstruments in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/instruments/{instrumentId}/personInstruments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonInstrumentDTO>> getPersonInstrumentByInstrument(@PathVariable Long instrumentId,
			Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get all Persons for an Instrument");
		Page<PersonInstrumentDTO> page = personInstrumentService.findByInstrument(instrumentId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/instrument-services");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	


}
