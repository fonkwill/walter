package at.simianarmy.web.rest;

import at.simianarmy.domain.enumeration.MailLogStatus;
import at.simianarmy.security.AuthoritiesConstants;
import at.simianarmy.service.MailLogService;
import at.simianarmy.service.dto.MailLogDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for getting MailLogs.
 */
@RestController
@RequestMapping("/api")
public class MailLogResource {

  private final Logger log = LoggerFactory.getLogger(MailLogResource.class);

  @Inject
  private MailLogService mailLogService;

  @RequestMapping(value = "/mail-log", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<MailLogDTO>> findByDateAndMailLogStatusList(
    Pageable pageable,
    @RequestParam(name = "date", required = true) @DateTimeFormat(iso = ISO.DATE) LocalDate date,
    @RequestParam(name = "mailLogStatusList", required = false) List<MailLogStatus> mailLogStatusList
  ) throws URISyntaxException {

    log.debug("REST Request to get MailLogs {} {}", date, mailLogStatusList);

    Page<MailLogDTO> page = this.mailLogService.findByDateAndMailLogStatus(date, mailLogStatusList, pageable);
    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/mail-log");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

}
