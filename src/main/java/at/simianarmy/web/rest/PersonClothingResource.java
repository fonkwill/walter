package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;

import at.simianarmy.service.PersonClothingService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.PersonClothingDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PersonClothing.
 */
@RestController
@RequestMapping("/api")
public class PersonClothingResource {

	private final Logger log = LoggerFactory.getLogger(PersonClothingResource.class);

	@Inject
	private PersonClothingService personClothingService;

	/**
	 * POST /person-clothings : Create a new personClothing.
	 *
	 * @param personClothingDTO
	 *            the personClothingDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personClothingDTO, or with status 400 (Bad Request) if the
	 *         personClothing has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-clothings", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonClothingDTO> createPersonClothing(
			@Valid @RequestBody PersonClothingDTO personClothingDTO) throws URISyntaxException {
		log.debug("REST request to save PersonClothing : {}", personClothingDTO);
		if (personClothingDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("personClothing", "idexists",
					"A new personClothing cannot already have an ID")).body(null);
		}
		PersonClothingDTO result = personClothingService.save(personClothingDTO);
		return ResponseEntity.created(new URI("/api/person-clothings/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("personClothing", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /person-clothings : Updates an existing personClothing.
	 *
	 * @param personClothingDTO
	 *            the personClothingDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personClothingDTO, or with status 400 (Bad Request) if the
	 *         personClothingDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the personClothingDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-clothings", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonClothingDTO> updatePersonClothing(
			@Valid @RequestBody PersonClothingDTO personClothingDTO) throws URISyntaxException {
		log.debug("REST request to update PersonClothing : {}", personClothingDTO);
		if (personClothingDTO.getId() == null) {
			return createPersonClothing(personClothingDTO);
		}
		PersonClothingDTO result = personClothingService.save(personClothingDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("personClothing", personClothingDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /person-clothings : get all the personClothings.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         personClothings in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/person-clothings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonClothingDTO>> getAllPersonClothings(
			@RequestParam(name = "personId", required = false) Long personId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of PersonClothings");
		Page<PersonClothingDTO> page = personClothingService.findAll(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-clothings");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /person-clothings/:id : get the "id" personClothing.
	 *
	 * @param id
	 *            the id of the personClothingDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         personClothingDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/person-clothings/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonClothingDTO> getPersonClothing(@PathVariable Long id) {
		log.debug("REST request to get PersonClothing : {}", id);
		PersonClothingDTO personClothingDTO = personClothingService.findOne(id);
		return Optional.ofNullable(personClothingDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /person-clothings/:id : delete the "id" personClothing.
	 *
	 * @param id
	 *            the id of the personClothingDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/person-clothings/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePersonClothing(@PathVariable Long id) {
		log.debug("REST request to delete PersonClothing : {}", id);
		personClothingService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personClothing", id.toString()))
				.build();
	}

}
