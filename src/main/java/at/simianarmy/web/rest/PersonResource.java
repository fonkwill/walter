package at.simianarmy.web.rest;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.service.PersonGroupService;
import at.simianarmy.service.PersonService;
import at.simianarmy.service.dto.MailLetterDTO;
import at.simianarmy.service.dto.MemberDTO;
import at.simianarmy.service.dto.PersonCollectionDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * REST controller for managing Person.
 */
@RestController
@RequestMapping("/api")
public class PersonResource {

	private final Logger log = LoggerFactory.getLogger(PersonResource.class);

	@Inject
	private PersonService personService;
	
	@Inject
  private ObjectMapper objectMapper;

	/**
	 * POST /people : Create a new person.
	 *
	 * @param personDTO
	 *            the personDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personDTO, or with status 400 (Bad Request) if the person has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/people", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonDTO> createPerson(@Valid @RequestBody PersonDTO personDTO) throws URISyntaxException {
		log.debug("REST request to save Person : {}", personDTO);
		if (personDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("person", "idexists", "A new person cannot already have an ID"))
					.body(null);
		}
		PersonDTO result = personService.save(personDTO);
		return ResponseEntity.created(new URI("/api/people/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("person", result.getId().toString())).body(result);
	}

	/**
	 * POST /people/member : Create a new member.
	 *
	 * @param memberDTO
	 *            the personDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personDTO, or with status 400 (Bad Request) if the person has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/people/member", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MemberDTO> createMember(@Valid @RequestBody MemberDTO memberDTO) throws URISyntaxException {
		log.debug("REST request to save Member : {}", memberDTO);
		if (memberDTO.getPerson().getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("person", "idexists", "A new person cannot already have an ID"))
					.body(null);
		}
		MemberDTO result = personService.saveMember(memberDTO);
		return ResponseEntity.created(new URI("/api/people/member" + result.getPerson().getId()))
				.headers(HeaderUtil.createEntityCreationAlert("person", result.getPerson().getId().toString()))
				.body(result);
	}

	/**
	 * PUT /people : Updates an existing person.
	 *
	 * @param personDTO
	 *            the personDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personDTO, or with status 400 (Bad Request) if the personDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the personDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/people", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonDTO> updatePerson(@Valid @RequestBody PersonDTO personDTO) throws URISyntaxException {
		log.debug("REST request to update Person : {}", personDTO);
		if (personDTO.getId() == null) {
			return createPerson(personDTO);
		}
		PersonDTO result = personService.save(personDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("person", personDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /people : get all the people.
	 *
	 * @param namePattern
	 *            a pattern which is searched in the firstname and lastname
	 * @param bdBegin
	 *            begin date of the birthdate
	 * @param bdEnd
	 *            end date of the birthdate
	 * @param pageable
	 *            the pagination information
	 * @param filter
	 *            the filter of the request
	 * @return the ResponseEntity with status 200 (OK) and the list of people in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/people", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonDTO>> getAllPeople(Pageable pageable,
			@RequestParam(required = false) String filter,
			@RequestParam(name = "namePattern", required = false) String namePattern,
			@RequestParam(name = "bdBegin", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bdBegin,
			@RequestParam(name = "bdEnd", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate bdEnd,
			@RequestParam(name = "gender", required = false) Gender gender,
			@RequestParam(value = "personGroups", required = false) List<Long> personGroupIds)
			throws URISyntaxException {
//		if ("user-is-null".equals(filter)) {
//			log.debug("REST request to get all Persons where user is null");
//			return new ResponseEntity<>(personService.findAllWhereUserIsNull(), HttpStatus.OK);
//		}
		log.debug("REST request to get a page of People");
		Page<PersonDTO> page = personService.findAll(namePattern, bdBegin, bdEnd, gender, personGroupIds, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/people");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /peopleWithFees: Get people with membership fees. When no period
	 * parameters are provided, a list of all people with current membership fees
	 * (paid or unpaid) is returned, i.e. active paying members. If previousPeriods
	 * and/or currentPeriod are true, people with UNPAID fees for the specified
	 * period(s) are returned.
	 *
	 * @param previousPeriods
	 *            true if unpaid fees from previous periouds should be considered
	 * @param currentPeriod
	 *            true if unpaid fee for the current period should be considered
	 * @return A list of paying members.
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/peopleWithFees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonDTO>> getPeopleWithFees(Pageable pageable,
			@RequestParam(name = "previousPeriods", required = false) Boolean previousPeriods,
			@RequestParam(name = "currentPeriod", required = false) Boolean currentPeriod) throws URISyntaxException {
		Page<PersonDTO> page;

		if (!(Boolean.TRUE.equals(previousPeriods) || Boolean.TRUE.equals(currentPeriod))) {
			log.debug("REST request to get a page of all Persons with current paid Memberships");
			page = personService.findAllWithCurrentFees(pageable);
		} else {
			log.debug("REST request to get a page of all Persons with unpaid MembershipFeesForPeriod(s)");
			page = personService.findAllWithUnpaidFees(previousPeriods, currentPeriod, pageable);
		}
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/peoplewithfees");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

  /**
   * GET /peopleWithValidMail: Get people with currently vail Mails (verified)
   *
   * @param mailLetterDTOAsString
   * @return A list of Persons with no valid Mail Address
   * @throws URISyntaxException
   */

  @RequestMapping(value = "/peopleWithValidMail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<PersonDTO> getPeopleWithValidEmailBySerialLetter(
    @RequestParam(name = "mailLetterDTO") String mailLetterDTOAsString
  ) throws IOException {

    MailLetterDTO mailLetterDTO = objectMapper.readValue(mailLetterDTOAsString, MailLetterDTO.class);

    log.debug("REST request to get a page of all Persons with invalid Emails {}", mailLetterDTO);

    List<PersonDTO> page = this.personService.findAllWithValidMailByMailLetter(mailLetterDTO);

    return page;
  }

  /**
   * GET /peopleWithInvalidMail: Get people with currently invalid Mails (either no Mail or not yet Verified)
   *
   * @param mailLetterDTOAsString
   * @return A list of Persons with no valid Mail Address
   * @throws URISyntaxException
   */

	@RequestMapping(value = "/peopleWithInvalidMail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public List<PersonDTO> getPeopleWithInvalidEmailBySerialLetter(
    @RequestParam(name = "mailLetterDTO") String mailLetterDTOAsString
  ) throws IOException {

    MailLetterDTO mailLetterDTO = objectMapper.readValue(mailLetterDTOAsString, MailLetterDTO.class);

	  log.debug("REST request to get a page of all Persons with invalid Emails {}", mailLetterDTO);

    List<PersonDTO> page = this.personService.findAllWithInvalidMailByMailLetter(mailLetterDTO);

    return page;
  }

	/**
	 * GET /people/:id : get the "id" person.
	 *
	 * @param id
	 *            the id of the personDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the personDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/people/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonDTO> getPerson(@PathVariable Long id) {
		log.debug("REST request to get Person : {}", id);
		PersonDTO personDTO = personService.findOne(id);
		return Optional.ofNullable(personDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /people/:id : delete the "id" person.
	 *
	 * @param id
	 *            the id of the personDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/people/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePerson(@PathVariable Long id) {
		log.debug("REST request to delete Person : {}", id);
		personService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("person", id.toString())).build();
	}


  /**
   * POST /people/import/csv : upload a CSV-File for Importing people.
   *
   * @param csvFile
   *            The CSV-File
   * @return List of read in persons (members and persons)
   */
  @RequestMapping(value = "/people/import/csv", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<PersonCollectionDTO> uploadCSVFile(
    @RequestParam(value = "csvFile") MultipartFile csvFile)
    throws URISyntaxException {
    log.debug("REST request to upload CSV for Person-Import!");

   PersonCollectionDTO result = this.personService.importPersonData(csvFile);

    return ResponseEntity.created(new URI("/api/people/import/csv")).body(result);
  }

  /**
   * POST /people/bulk : save multiple persons at once
   *
   * @param personCollectionDTO
   *            A collection of all persons and members which should be saved
   * @return A Collection of persons and members which were NOT successfully saved
   */
  @RequestMapping(value = "/people/bulk", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<PersonCollectionDTO> createPersonsAndMembers(@Valid @RequestBody PersonCollectionDTO personCollectionDTO)
    throws URISyntaxException {
    log.debug("REST request to save multiple persons and members!");

    PersonCollectionDTO result = this.personService.savePersonCollection(personCollectionDTO);

    return ResponseEntity.created(new URI("/api/people/bulk")).body(result);
  }

  /**
   * Returns a json-file with all personal information for one person
   * @param id The id of the person
   * @return A json-file with all information to the given person
   */
  @RequestMapping(value = {"/people/{id}/json"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  @Timed
  public ResponseEntity<Resource> getPersonAsJson(@PathVariable Long id) {
    log.debug("REST request to get full information for person as json-file: {}", id);

    Resource resource = personService.findOneWithFullInformationAsJson(id);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
      .header(HttpHeaders.CONTENT_DISPOSITION,
        "attachment; filename=\"" + id + "_info.json" + "\"").body(resource);

  }


  /**
   * Returns a pdf-file with all personal information for one person
   * @param id The id of the person
   * @return A pdf-file with all information to the given person
   */
  @RequestMapping(value = {"/people/{id}/pdf"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
  @Timed
  public ResponseEntity<Resource> getPersonAsPdf(@PathVariable Long id) {
    log.debug("REST request to get full information for person as pdf-file: {}", id);

    Resource resource = personService.findOneWithFullInformationAsPDF(id);
    return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF)
      .header(HttpHeaders.CONTENT_DISPOSITION,
        "attachment; filename=\"" + id + "_info.pdf" + "\"").body(resource);

  }


}
