package at.simianarmy.web.rest;

import at.simianarmy.service.ReportService;
import at.simianarmy.service.dto.ReportDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Report.
 */
@RestController
@RequestMapping("/api")
public class ReportResource {

	private final Logger log = LoggerFactory.getLogger(ReportResource.class);

	@Inject
	private ReportService reportService;

	/**
	 * POST /reports : Create a new report.
	 *
	 * @param reportDTO
	 *            the reportDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         reportDTO, or with status 400 (Bad Request) if the report has already
	 *         an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/reports", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReportDTO> createReport(@Valid @RequestBody ReportDTO reportDTO) throws URISyntaxException {
		log.debug("REST request to save Report : {}", reportDTO);
		if (reportDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("report", "idexists", "A new report cannot already have an ID"))
					.body(null);
		}
		ReportDTO result = reportService.save(reportDTO);
		return ResponseEntity.created(new URI("/api/reports/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("report", result.getId().toString())).body(result);
	}

	/**
	 * PUT /reports : Updates an existing report.
	 *
	 * @param reportDTO
	 *            the reportDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         reportDTO, or with status 400 (Bad Request) if the reportDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the reportDTO
	 *         couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/reports", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReportDTO> updateReport(@Valid @RequestBody ReportDTO reportDTO) throws URISyntaxException {
		log.debug("REST request to update Report : {}", reportDTO);
		if (reportDTO.getId() == null) {
			return createReport(reportDTO);
		}
		ReportDTO result = reportService.save(reportDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("report", reportDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /reports : get all the reports.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of reports in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/reports", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ReportDTO>> getAllReports(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Reports");
		Page<ReportDTO> page = reportService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reports");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /reports/:id : get the "id" report.
	 *
	 * @param id
	 *            the id of the reportDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the reportDTO,
	 *         or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/reports/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ReportDTO> getReport(@PathVariable Long id) {
		log.debug("REST request to get Report : {}", id);
		ReportDTO reportDTO = reportService.findOne(id);
		return Optional.ofNullable(reportDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /reports/:id : delete the "id" report.
	 *
	 * @param id
	 *            the id of the reportDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/reports/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteReport(@PathVariable Long id) {
		log.debug("REST request to delete Report : {}", id);
		reportService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("report", id.toString())).build();
	}

}
