package at.simianarmy.web.rest;

import at.simianarmy.service.ComposerService;
import at.simianarmy.service.dto.ComposerDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Composer.
 */
@RestController
@RequestMapping("/api")
public class ComposerResource {

	private final Logger log = LoggerFactory.getLogger(ComposerResource.class);

	@Inject
	private ComposerService composerService;

	/**
	 * POST /composers : Create a new composer.
	 *
	 * @param composerDTO
	 *            the composerDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         composerDTO, or with status 400 (Bad Request) if the composer has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/composers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ComposerDTO> createComposer(@Valid @RequestBody ComposerDTO composerDTO)
			throws URISyntaxException {
		log.debug("REST request to save Composer : {}", composerDTO);
		if (composerDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("composer", "idexists", "A new composer cannot already have an ID"))
					.body(null);
		}
		ComposerDTO result = composerService.save(composerDTO);
		return ResponseEntity.created(new URI("/api/composers/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("composer", result.getId().toString())).body(result);
	}

	/**
	 * PUT /composers : Updates an existing composer.
	 *
	 * @param composerDTO
	 *            the composerDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         composerDTO, or with status 400 (Bad Request) if the composerDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         composerDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/composers", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ComposerDTO> updateComposer(@Valid @RequestBody ComposerDTO composerDTO)
			throws URISyntaxException {
		log.debug("REST request to update Composer : {}", composerDTO);
		if (composerDTO.getId() == null) {
			return createComposer(composerDTO);
		}
		ComposerDTO result = composerService.save(composerDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("composer", composerDTO.getId().toString())).body(result);
	}

	/**
	 * GET /composers : get all the composers.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of composers in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = {"/composers", "compositions/composers"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<ComposerDTO>> getAllComposers(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Composers");
		Page<ComposerDTO> page = composerService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/composers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /composers/:id : get the "id" composer.
	 *
	 * @param id
	 *            the id of the composerDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         composerDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/composers/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ComposerDTO> getComposer(@PathVariable Long id) {
		log.debug("REST request to get Composer : {}", id);
		ComposerDTO composerDTO = composerService.findOne(id);
		return Optional.ofNullable(composerDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /composers/:id : delete the "id" composer.
	 *
	 * @param id
	 *            the id of the composerDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/composers/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deleteComposer(@PathVariable Long id) {
		log.debug("REST request to delete Composer : {}", id);
		composerService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("composer", id.toString())).build();
	}

}
