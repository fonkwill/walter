package at.simianarmy.web.rest;

import at.simianarmy.service.PersonAwardService;
import at.simianarmy.service.dto.PersonAwardDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing PersonAward.
 */
@RestController
@RequestMapping("/api")
public class PersonAwardResource {

	private final Logger log = LoggerFactory.getLogger(PersonAwardResource.class);

	@Inject
	private PersonAwardService personAwardService;

	/**
	 * POST /person-awards : Create a new personAward.
	 *
	 * @param personAwardDTO
	 *            the personAwardDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         personAwardDTO, or with status 400 (Bad Request) if the personAward
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-awards", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonAwardDTO> createPersonAward(@Valid @RequestBody PersonAwardDTO personAwardDTO)
			throws URISyntaxException {
		log.debug("REST request to save PersonAward : {}", personAwardDTO);
		if (personAwardDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("personAward", "idexists",
					"A new personAward cannot already have an ID")).body(null);
		}
		PersonAwardDTO result = personAwardService.save(personAwardDTO);
		return ResponseEntity.created(new URI("/api/person-awards/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("personAward", result.getId().toString())).body(result);
	}

	/**
	 * PUT /person-awards : Updates an existing personAward.
	 *
	 * @param personAwardDTO
	 *            the personAwardDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         personAwardDTO, or with status 400 (Bad Request) if the
	 *         personAwardDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the personAwardDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@RequestMapping(value = "/person-awards", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonAwardDTO> updatePersonAward(@Valid @RequestBody PersonAwardDTO personAwardDTO)
			throws URISyntaxException {
		log.debug("REST request to update PersonAward : {}", personAwardDTO);
		if (personAwardDTO.getId() == null) {
			return createPersonAward(personAwardDTO);
		}
		PersonAwardDTO result = personAwardService.save(personAwardDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("personAward", personAwardDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /person-awards : get all the personAwards.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of personAwards
	 *         in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/person-awards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<PersonAwardDTO>> getAllPersonAwards(
			@RequestParam(name = "personId", required = false) Long personId, Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of PersonAwards");
		Page<PersonAwardDTO> page = personAwardService.findAll(personId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-awards");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /person-awards/:id : get the "id" personAward.
	 *
	 * @param id
	 *            the id of the personAwardDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         personAwardDTO, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/person-awards/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<PersonAwardDTO> getPersonAward(@PathVariable Long id) {
		log.debug("REST request to get PersonAward : {}", id);
		PersonAwardDTO personAwardDTO = personAwardService.findOne(id);
		return Optional.ofNullable(personAwardDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /person-awards/:id : delete the "id" personAward.
	 *
	 * @param id
	 *            the id of the personAwardDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/person-awards/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> deletePersonAward(@PathVariable Long id) {
		log.debug("REST request to delete PersonAward : {}", id);
		personAwardService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("personAward", id.toString())).build();
	}

}
