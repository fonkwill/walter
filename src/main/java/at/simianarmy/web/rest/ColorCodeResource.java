package at.simianarmy.web.rest;

import at.simianarmy.service.ColorCodeService;
import at.simianarmy.service.dto.ColorCodeDTO;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing ColorCode.
 */
@RestController
@RequestMapping("/api")
public class ColorCodeResource {

	private final Logger log = LoggerFactory.getLogger(ColorCodeResource.class);

	@Inject
	private ColorCodeService colorCodeService;

	/**
	 * POST /color-codes : Create a new colorCode.
	 *
	 * @param colorCodeDTO
	 *            the colorCodeDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         colorCodeDTO, or with status 400 (Bad Request) if the colorCode has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/color-codes")
	@Timed
	public ResponseEntity<ColorCodeDTO> createColorCode(@Valid @RequestBody ColorCodeDTO colorCodeDTO)
			throws URISyntaxException {
		log.debug("REST request to save ColorCode : {}", colorCodeDTO);
		if (colorCodeDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("colorCode", "idexists", "A new colorCode cannot already have an ID"))
					.body(null);
		}
		ColorCodeDTO result = colorCodeService.save(colorCodeDTO);
		return ResponseEntity.created(new URI("/api/color-codes/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("colorCode", result.getId().toString())).body(result);
	}

	/**
	 * PUT /color-codes : Updates an existing colorCode.
	 *
	 * @param colorCodeDTO
	 *            the colorCodeDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         colorCodeDTO, or with status 400 (Bad Request) if the colorCodeDTO is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         colorCodeDTO couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/color-codes")
	@Timed
	public ResponseEntity<ColorCodeDTO> updateColorCode(@Valid @RequestBody ColorCodeDTO colorCodeDTO)
			throws URISyntaxException {
		log.debug("REST request to update ColorCode : {}", colorCodeDTO);
		if (colorCodeDTO.getId() == null) {
			return createColorCode(colorCodeDTO);
		}
		ColorCodeDTO result = colorCodeService.save(colorCodeDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("colorCode", colorCodeDTO.getId().toString())).body(result);
	}

	/**
	 * GET /color-codes : get all the colorCodes.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of colorCodes in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping(value={"/color-codes","compositions/color-codes"})
	@Timed
	public ResponseEntity<List<ColorCodeDTO>> getAllColorCodes(Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of ColorCodes");
		Page<ColorCodeDTO> page = colorCodeService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/color-codes");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /color-codes/:id : get the "id" colorCode.
	 *
	 * @param id
	 *            the id of the colorCodeDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         colorCodeDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/color-codes/{id}")
	@Timed
	public ResponseEntity<ColorCodeDTO> getColorCode(@PathVariable Long id) {
		log.debug("REST request to get ColorCode : {}", id);
		ColorCodeDTO colorCodeDTO = colorCodeService.findOne(id);
		return Optional.ofNullable(colorCodeDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /color-codes/:id : delete the "id" colorCode.
	 *
	 * @param id
	 *            the id of the colorCodeDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/color-codes/{id}")
	@Timed
	public ResponseEntity<Void> deleteColorCode(@PathVariable Long id) {
		log.debug("REST request to delete ColorCode : {}", id);
		colorCodeService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("colorCode", id.toString())).build();
	}

}
