package at.simianarmy.web.rest;

import com.codahale.metrics.annotation.Timed;
import at.simianarmy.service.CashbookAccountService;
import at.simianarmy.web.rest.util.HeaderUtil;
import at.simianarmy.web.rest.util.PaginationUtil;
import at.simianarmy.service.dto.CashbookAccountDTO;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CashbookAccount.
 */
@RestController
@RequestMapping("/api")
public class CashbookAccountResource {

	private final Logger log = LoggerFactory.getLogger(CashbookAccountResource.class);

	private static final String ENTITY_NAME = "cashbookAccount";

	private final CashbookAccountService cashbookAccountService;

	public CashbookAccountResource(CashbookAccountService cashbookAccountService) {
		this.cashbookAccountService = cashbookAccountService;
	}

	/**
	 * POST /cashbook-accounts : Create a new cashbookAccount.
	 *
	 * @param cashbookAccountDTO
	 *            the cashbookAccountDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         cashbookAccountDTO, or with status 400 (Bad Request) if the
	 *         cashbookAccount has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/cashbook-accounts")
	@Timed
	public ResponseEntity<CashbookAccountDTO> createCashbookAccount(
			@Valid @RequestBody CashbookAccountDTO cashbookAccountDTO) throws URISyntaxException {
		log.debug("REST request to save CashbookAccount : {}", cashbookAccountDTO);
		if (cashbookAccountDTO.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new cashbookAccount cannot already have an ID")).body(null);
		}
		CashbookAccountDTO result = cashbookAccountService.save(cashbookAccountDTO);
		return ResponseEntity.created(new URI("/api/cashbook-accounts/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /cashbook-accounts : Updates an existing cashbookAccount.
	 *
	 * @param cashbookAccountDTO
	 *            the cashbookAccountDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         cashbookAccountDTO, or with status 400 (Bad Request) if the
	 *         cashbookAccountDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the cashbookAccountDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/cashbook-accounts")
	@Timed
	public ResponseEntity<CashbookAccountDTO> updateCashbookAccount(
			@Valid @RequestBody CashbookAccountDTO cashbookAccountDTO) throws URISyntaxException {
		log.debug("REST request to update CashbookAccount : {}", cashbookAccountDTO);
		if (cashbookAccountDTO.getId() == null) {
			return createCashbookAccount(cashbookAccountDTO);
		}
		CashbookAccountDTO result = cashbookAccountService.update(cashbookAccountDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cashbookAccountDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /cashbook-accounts : get all the cashbookAccounts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         cashbookAccounts in body
	 */
	@RequestMapping(value = {"/cashbook-accounts", "/customizations/cashbook-accounts"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<CashbookAccountDTO>> getAllCashbookAccounts(
	  @RequestParam(value = "bankAccount", required = false) Boolean bankAccount,
	  @ApiParam Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of CashbookAccounts");
		Page<CashbookAccountDTO> page = cashbookAccountService.findAll(bankAccount, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/cashbook-accounts");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /cashbook-accounts/:id : get the "id" cashbookAccount.
	 *
	 * @param id
	 *            the id of the cashbookAccountDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         cashbookAccountDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/cashbook-accounts/{id}")
	@Timed
	public ResponseEntity<CashbookAccountDTO> getCashbookAccount(@PathVariable Long id) {
		log.debug("REST request to get CashbookAccount : {}", id);
		CashbookAccountDTO cashbookAccountDTO = cashbookAccountService.findOne(id);
		return Optional.ofNullable(cashbookAccountDTO).map(result -> new ResponseEntity<>(result, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	/**
	 * DELETE /cashbook-accounts/:id : delete the "id" cashbookAccount.
	 *
	 * @param id
	 *            the id of the cashbookAccountDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/cashbook-accounts/{id}")
	@Timed
	public ResponseEntity<Void> deleteCashbookAccount(@PathVariable Long id) {
		log.debug("REST request to delete CashbookAccount : {}", id);
		cashbookAccountService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
