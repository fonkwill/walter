/**
 * Data Access Objects used by WebSocket services.
 */
package at.simianarmy.web.websocket.dto;
