package at.simianarmy.scheduling;

import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.scheduling.task.MailLogDeletionTask;
import javax.inject.Inject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MailLogDeletionTaskScheduler {

  @Inject
  private MailLogDeletionTask mailLogDeletionTask;

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  /**
   * Scheduled Task runs every minute and starts first after 1 Minute
   */
  @Scheduled(fixedRate = 60000, initialDelay = 60000)
  public void invalidateEmailVerificationLinks() {
    multiTenancyExecuter.execute(this.mailLogDeletionTask);
  }

}
