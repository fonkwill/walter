package at.simianarmy.scheduling;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.domain.User;
import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.notification.NotificationEntityUtility;
import at.simianarmy.notification.NotificationException;
import at.simianarmy.repository.NotificationRepository;
import at.simianarmy.repository.NotificationRuleRepository;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.scheduling.task.CreateNotificationsTask;
import at.simianarmy.service.NotificationRuleService;
import at.simianarmy.service.NotificationService;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class NotificationTaskScheduler {

  private static final Logger log = LoggerFactory.getLogger(NotificationTaskScheduler.class);

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  @Inject
  private CreateNotificationsTask createNotificationsTask;

  /**
   * Creates notifications from available notification rules. this is scheduled
   * daily at 1 o'clock.
   */
  @Scheduled(cron = "0 0 1 * * *")
  public void createNotifications() {
    this.multiTenancyExecuter.execute(createNotificationsTask);
  }

}
