package at.simianarmy.scheduling;

import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.notification.NotificationEntityUtility;
import at.simianarmy.notification.NotificationException;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.repository.NotificationRepository;
import at.simianarmy.repository.NotificationRuleRepository;
import at.simianarmy.scheduling.task.CreateMembershipFeeForPeriodForMembershipTask;
import at.simianarmy.scheduling.task.UpdateMembershipGroupTask;
import at.simianarmy.service.MembershipService;
import at.simianarmy.service.specification.MembershipSpecification;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MembershipTaskScheduler {

  private static final Logger log = LoggerFactory.getLogger(MembershipTaskScheduler.class);

  @Inject
  private CreateMembershipFeeForPeriodForMembershipTask createMembershipFeeForPeriodForMembershipTask;

  @Inject
  private UpdateMembershipGroupTask updateMembershipGroupTask;

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  /**
   * Checks every day (at 01:01) if there are Memberships with MembershipFees
   * which have no MembershipFeeForPeriods for this period Missing
   * MembershipFeeForPeriods will be created
   */
  @Scheduled(cron = "0 1 1 * * ?")
  @Transactional
  public void createMembershipFeeForPeriodForMembership() {
    this.multiTenancyExecuter.execute(this.createMembershipFeeForPeriodForMembershipTask);
  }

  /**
   * Checks every day (at 02:01) if there are memberships from persons which have
   * not the membership group assigned
   */
  @Scheduled(cron = "0 1 2 * * ?")
  @Transactional
  public void updateMembershipGroup() {
    this.multiTenancyExecuter.execute(this.updateMembershipGroupTask);
  }

}
