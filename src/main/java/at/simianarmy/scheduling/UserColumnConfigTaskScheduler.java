package at.simianarmy.scheduling;

import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.scheduling.task.UserColumnConfigGeneratorTask;
import javax.inject.Inject;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class UserColumnConfigTaskScheduler implements ApplicationListener<ApplicationReadyEvent> {

  @Inject
  private UserColumnConfigGeneratorTask userColumnConfigGeneratorTask;

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;


  /**
   * Generates all missing ColumnConfigs for all Users at ApplicationReadyEvent
   * @param applicationReadyEvent
   */
  @Override
  public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
    try {
      //Wait for 20 Seconds before filling the ColumnConfigs!
      Thread.sleep(20000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    this.multiTenancyExecuter.execute(this.userColumnConfigGeneratorTask);
  }

}
