package at.simianarmy.scheduling;
import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.scheduling.task.CheckPersonRelevanceTask;
import at.simianarmy.scheduling.task.DeleteNonRelevantPersonsTask;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class PersonRelevanceScheduler {


  @Inject
  private CheckPersonRelevanceTask checkPersonRelevanceTask;

  @Inject
  private DeleteNonRelevantPersonsTask deleteNonRelevantPersonsTask;

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  /**
   * Checks every 5 minutes for person to mark as not relevant or unmarks them
   */
  @Scheduled(fixedRate = 300000)
  public void schedulePersonRelevance() {
    multiTenancyExecuter.execute(checkPersonRelevanceTask);
  }

  /**
   * Deletes non relevant persons every day once
   */
  @Scheduled(cron = "0 0 3 * * ?")
  public void deleteNotRelevantPersons() {
     multiTenancyExecuter.execute(deleteNonRelevantPersonsTask);
  }



}
