package at.simianarmy.scheduling;

import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.scheduling.task.EmailVerificationLinkInvalidatorTask;
import javax.inject.Inject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EmailVerificationLinkTaskScheduler {

  @Inject
  private EmailVerificationLinkInvalidatorTask emailVerificationLinkTask;

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  /**
   * Scheduled Task runs every minute and starts first after 1 Minutes
   */
  @Scheduled(fixedRate = 60000, initialDelay = 60000)
  public void invalidateEmailVerificationLinks() {
    multiTenancyExecuter.execute(this.emailVerificationLinkTask);
  }


}
