package at.simianarmy.scheduling.task;

import at.simianarmy.service.NotificationRuleService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateNotificationsTask implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(CreateNotificationsTask.class);

  @Inject
  private NotificationRuleService notificationRuleService;

  @Override
  public void run() {
    this.notificationRuleService.createNotifications();
  }
}
