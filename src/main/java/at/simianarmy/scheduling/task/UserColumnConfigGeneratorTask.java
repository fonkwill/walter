package at.simianarmy.scheduling.task;

import at.simianarmy.domain.User;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.service.UserColumnConfigService;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserColumnConfigGeneratorTask implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(UserColumnConfigGeneratorTask.class);

  @Inject
  private UserRepository userRepository;

  @Inject
  private UserColumnConfigService userColumnConfigService;

  @Override
  public void run() {

    this.log.info("Checking and Generating missing UserColumnConfigs");

    List<User> users = this.userRepository.findAll();

    for (User user : users) {
      this.userColumnConfigService.generateColumnConfigsForUser(user);
    }

  }
}
