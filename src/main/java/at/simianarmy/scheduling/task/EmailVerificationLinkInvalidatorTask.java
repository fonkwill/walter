package at.simianarmy.scheduling.task;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.service.CustomizationService;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class EmailVerificationLinkInvalidatorTask implements Runnable {

  private static final Long HOURS_TO_INVALIDATE = new Long(48);

  private static final Logger log = LoggerFactory.getLogger(EmailVerificationLinkInvalidatorTask.class);

  @Inject
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Inject
  private CustomizationService customizationService;

  @Override
  public void run() {

    log.debug("Checking EmailVerificationLinks to invalidate!");

    Long hours = customizationService.getHoursToInvalidateVerificationLink();
    if (hours == null || hours <= 0) {
      hours = HOURS_TO_INVALIDATE;
    }

    ZonedDateTime referenceDate = ZonedDateTime.now().minusHours(hours);

    List<EmailVerificationLink> toInvalidate = emailVerificationLinkRepository.findAllToInvalidate(referenceDate);

    for (EmailVerificationLink link : toInvalidate) {
      log.debug("Invalidating EmailVerificationLink with ID: " + link.getId());
      link.setInvalid(true);
      emailVerificationLinkRepository.save(link);
      emailVerificationLinkRepository.flush();
    }
  }
}
