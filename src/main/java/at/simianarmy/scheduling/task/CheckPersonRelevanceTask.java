package at.simianarmy.scheduling.task;

import at.simianarmy.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.LocalDate;

@Component
public class CheckPersonRelevanceTask implements  Runnable {

  private static final Logger log = LoggerFactory.getLogger(CheckPersonRelevanceTask.class);

  @Inject
  private PersonService personService;

  @Override
  public void run() {

    LocalDate now = LocalDate.now();
    log.debug("starting to mark non-relevant persons");
    personService.markNotRelevantPersons(now);

    log.debug("starting to unmark relevant persons");
    personService.unmarkRelevantPersons(now);

  }
}
