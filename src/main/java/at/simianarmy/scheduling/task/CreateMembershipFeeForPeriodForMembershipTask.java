package at.simianarmy.scheduling.task;

import at.simianarmy.service.MembershipService;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CreateMembershipFeeForPeriodForMembershipTask implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(CreateMembershipFeeForPeriodForMembershipTask.class);

  @Inject
  private MembershipService membershipService;

  @Override
  public void run() {
    this.membershipService.createMembershipFeeForPeriodForMembership();
  }
}
