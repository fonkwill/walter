package at.simianarmy.scheduling.task;

import at.simianarmy.domain.Membership;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.service.MembershipService;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UpdateMembershipGroupTask implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(UpdateMembershipGroupTask.class);

  @Inject
  private MembershipRepository membershipRepository;

  @Inject
  private MembershipService membershipService;

  @Override
  public void run() {
    List<Membership> memberships = membershipRepository.findByDueDate(LocalDate.now());
    for (Membership m : memberships) {
      if (m.getPerson() != null) {
        this.membershipService.updateMembershipGroup(m.getPerson().getId());
      }
    }
  }
}
