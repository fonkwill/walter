package at.simianarmy.scheduling.task;

import at.simianarmy.domain.MailLog;
import at.simianarmy.repository.MailLogRepository;
import at.simianarmy.service.CustomizationService;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MailLogDeletionTask implements Runnable {

  private static final Long DAYS_TO_DELETE = new Long(7);

  private static final Logger log = LoggerFactory.getLogger(MailLogDeletionTask.class);

  @Inject
  private MailLogRepository mailLogRepository;

  @Inject
  private CustomizationService customizationService;

  @Override
  public void run() {

    log.debug("Cleaning up MailLog!");

    Long days = customizationService.getDaysToDeleteMailLog();
    if (days == null || days <= 0) {
      days = DAYS_TO_DELETE;
    }

    ZonedDateTime referenceDate = ZonedDateTime.now().minusDays(days);
    List<MailLog> toDelete = this.mailLogRepository.findByDateBefore(referenceDate);

    for (MailLog log : toDelete) {
      this.mailLogRepository.delete(log);
    }

  }
}
