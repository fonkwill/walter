package at.simianarmy.scheduling.task;

import at.simianarmy.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.LocalDate;

@Component
public class DeleteNonRelevantPersonsTask implements  Runnable {

  private static final Logger log = LoggerFactory.getLogger(DeleteNonRelevantPersonsTask.class);

  @Inject
  private PersonService personService;

  @Override
  public void run() {
    LocalDate now = LocalDate.now();
    log.debug("starting to delete not relevant persons");
    personService.deletePersonsWhichAreNotRelevant(now);

  }
}
