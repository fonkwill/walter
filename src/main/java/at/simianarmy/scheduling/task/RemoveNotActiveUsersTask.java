package at.simianarmy.scheduling.task;

import at.simianarmy.domain.User;
import at.simianarmy.repository.UserRepository;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoveNotActiveUsersTask implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(RemoveNotActiveUsersTask.class);

  @Inject
  private UserRepository userRepository;

  @Override
  public void run() {
    ZonedDateTime now = ZonedDateTime.now();
    List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3).toInstant());
    for (User user : users) {
      log.debug("Deleting not activated user {}", user.getLogin());
      userRepository.delete(user);
    }
  }
}
