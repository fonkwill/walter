package at.simianarmy.scheduling;

import at.simianarmy.domain.User;
import at.simianarmy.multitenancy.MultiTenancyExecuter;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.scheduling.task.RemoveNotActiveUsersTask;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UserTaskScheduler {

  private static final Logger log = LoggerFactory.getLogger(UserTaskScheduler.class);

  @Inject
  private MultiTenancyExecuter multiTenancyExecuter;

  @Inject
  private RemoveNotActiveUsersTask removeNotActiveUsersTask;

  /**
   * Not activated users should be automatically deleted after 3 days.
   * <p>
   * This is scheduled to get fired everyday, at 01:00 (am).
   * </p>
   */
  @Scheduled(cron = "0 0 1 * * ?")
  public void removeNotActivatedUsers() {
    this.multiTenancyExecuter.execute(this.removeNotActiveUsersTask);
  }

}
