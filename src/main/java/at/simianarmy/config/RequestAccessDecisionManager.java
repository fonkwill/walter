package at.simianarmy.config;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.Request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.stereotype.Component;

import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.repository.AuthConfigRepository;

@Component
public class RequestAccessDecisionManager implements AccessDecisionManager {

	private static Logger logger = LoggerFactory.getLogger(RequestAccessDecisionManager.class);
	
	private static final String anonym = "ANONYM";
	
	@Inject
	private AuthConfigRepository authConfigRepository;
	
	@Override
	public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
			throws AccessDeniedException, InsufficientAuthenticationException {
			
			FilterInvocation filterInvocation = (FilterInvocation) object;
			boolean granted = false;
			
//			check which auth is needed
			List<String> requiredAuth = authConfigRepository.getAllAuthoritiesForRequest(filterInvocation.getHttpRequest().getRequestURI());
			if (requiredAuth.size() == 1 && requiredAuth.get(0).equals(anonym)) {
				granted = true;
				return;
			}
			
			String httpMethod = filterInvocation.getRequest().getMethod();
			
//			check if user has one of the needed auth
			for (GrantedAuthority grAuth : authentication.getAuthorities()) {
				Authority auth = Authority.getFromGrantedAuthRepr(grAuth.getAuthority());
				if (auth == null)  {
					continue;
				}
				if (requiredAuth.contains(auth.getName())) {
					if (HttpMethod.GET.matches(httpMethod) || auth.getWrite().equals(true)) {
						granted = true;
						break;
					} 			
				}
				
			}
			
			if (!granted) {
				throw new AccessDeniedException("Access denied");
			}	

	}

	@Override
	public boolean supports(ConfigAttribute attribute) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		if(FilterInvocation.class.isAssignableFrom(clazz)) {
			return true;
		}
		return false;
	}

}
