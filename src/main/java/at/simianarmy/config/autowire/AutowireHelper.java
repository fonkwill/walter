package at.simianarmy.config.autowire;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Hilfsklasse, welche AutoWiring untersützen soll
 * {@link org .springframework.context.ApplicationContext}.
 */
public final class AutowireHelper implements ApplicationContextAware {

	private static final AutowireHelper INSTANCE = new AutowireHelper();
	private static ApplicationContext applicationContext;

	private AutowireHelper() {
	}

	/**
	 * Versucht eine Instanz zu erstellen und diese zu wiren, falls sie null ist.
	 *
	 * @param classToAutowire
	 *            die Instanz der Klasse, welche eine Verbindung mit @Autowire
	 *            Annotation beinhaltet
	 * @param beansToAutowireInClass
	 *            die Bean, welche die @Autowire Annotation in der "classToAutowire"
	 *            hat
	 */
	public static void autowire(Object classToAutowire, Object... beansToAutowireInClass) {
		for (Object bean : beansToAutowireInClass) {
			if (bean == null) {
				applicationContext.getAutowireCapableBeanFactory().autowireBean(classToAutowire);
				return;
			}
		}
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		AutowireHelper.applicationContext = applicationContext;
	}

	/**
	 * @return den Singleton.
	 */
	public static AutowireHelper getInstance() {
		return INSTANCE;
	}

}
