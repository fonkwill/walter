package at.simianarmy.config.filestore;

public enum FileStoreType {
  LOCAL, DROPBOX
}
