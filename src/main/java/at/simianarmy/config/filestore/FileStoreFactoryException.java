package at.simianarmy.config.filestore;

public class FileStoreFactoryException extends RuntimeException {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public FileStoreFactoryException(String message) {
    super(message);
  }

  public FileStoreFactoryException(String message, Exception cause) {
    super(message, cause);
  }
}
