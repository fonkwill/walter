package at.simianarmy.config.filestore;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.impl.dropbox.DropboxFileStore;
import at.simianarmy.filestore.impl.local.LocalFileStore;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class FileStoreFactory {

  @Inject
  private ApplicationContext context;

  @Inject
  private FileStoreProperties fileStoreProperties;

  private final Logger log = LoggerFactory.getLogger(FileStoreFactory.class);

  public FileStore createFileStore() {
    FileStoreType type = this.fileStoreProperties.getType();

    log.debug("Creating FileStore for Type {}", type);

    FileStore fileStore;

    if (type.equals(FileStoreType.LOCAL)) {
      fileStore = this.context.getBean("LocalFileStore", LocalFileStore.class);
    } else if (type.equals(FileStoreType.DROPBOX)) {
      fileStore = this.context.getBean("DropboxFileStore", DropboxFileStore.class);
    } else {
      log.error("FileStoreType {} is not implemented!", type);
      throw new FileStoreFactoryException("FileStoreType is not implemented!");
    }

    try {
      fileStore.initStorageLocations();
    } catch (FileStoreException e) {
      throw new FileStoreFactoryException("Error on initializing Storage Locations!", e);
    }

    return fileStore;
  }

}
