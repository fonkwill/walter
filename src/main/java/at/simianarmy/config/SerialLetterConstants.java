package at.simianarmy.config;

import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;

public class SerialLetterConstants {

	public static final String generatedSerialLettersFile = "Serienbrief.pdf";

	public static final Rectangle defaultPageSize = PageSize.A4;

	public static final Float defaultMarginBottom = 75F;
	public static final Float defaultMarginTop = 75F;
	public static final Float defaultMarginLeft = 75F;
	public static final Float defaultMarginRight = 75F;
	// margin which is used at the first page of a letter (with address block)
	public static final Float defaultMarginTopLetter = 270F;
	// bottom margin if a payment check is printed
	public static final Float defaultMarginBottomPaymentCheck = 300F;

	public static final Float marginTopAddress = 155F;
	public static final Float marginBottomAddress = 240F;
	public static final Float marginTopAddressWithRecipient = 135F;
	public static final Float marginLeftAddress = 75F;
	public static final Float marginRightAddress = 450F;

	public static final String maleAddress = "Herrn";
	public static final String femaleAddress = "Frau";

	public static final String membershipFeeText = "Mitgliedsbeitrag";
	public static final String vzweckInfo = "Bei Online-Überweisung geben Sie bitte ins Feld Zahlungsreferenz folgendes ein:";

	public static final String referenceVariable = "%REFERENZ%";
	public static final String amountVariable = "%BETRAG%";

	public static final String emailVerificationLink = "%LINK%";
  public static final String emailVerificationQRLink = "%QR_LINK%";
	public static final String emailVerificationLinkSecret = "%GEHEIMCODE%";

	public static final String previousPeriods = "Vorperioden";

	public static final String companyAddress = "Firma";

	public static final String recipientPrefix = "z.Hd.";

}
