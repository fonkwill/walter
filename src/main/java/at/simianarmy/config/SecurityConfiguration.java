package at.simianarmy.config;

import javax.inject.Inject;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;

import at.simianarmy.security.Http401UnauthorizedEntryPoint;
import at.simianarmy.security.jwt.JWTConfigurer;
import at.simianarmy.security.jwt.TokenProvider;
import at.simianarmy.web.filter.MultiTenancySecurityConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Inject
	private Http401UnauthorizedEntryPoint authenticationEntryPoint;

	@Inject
	private UserDetailsService userDetailsService;

	@Inject
	private TokenProvider tokenProvider;

	@Inject
	private Environment env;
	
	@Inject
	private RequestAccessDecisionManager decisionManager;

	@Inject
	private WalterProperties walterProperties;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Inject
	public void configureGlobal(AuthenticationManagerBuilder auth) {
		try {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		} catch (Exception exception) {
			throw new BeanInitializationException("Security configuration failed", exception);
		}
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**").antMatchers("/app/**/*.{js,html}")
				.antMatchers("/bower_components/**").antMatchers("/i18n/**").antMatchers("/content/**")
				.antMatchers("/swagger-ui/index.html").antMatchers("/test/**").antMatchers("/h2-console/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).and().csrf().disable().headers()
				.frameOptions().disable().and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
				.antMatchers("/api/register").permitAll().antMatchers("/api/activate").permitAll()
				.antMatchers("/api/authenticate").permitAll().antMatchers("/api/receipts/file/**").permitAll()
				.antMatchers("/api/templates/file/**").permitAll().antMatchers("/api/appointments/ical").permitAll()
				.antMatchers("/api/account/reset_password/init").permitAll()
        .antMatchers("/api/email-verification/**").permitAll()
        .antMatchers("/api/email-verification").permitAll()
				.antMatchers("/api/account/reset_password/finish").permitAll().antMatchers("/api/profile-info")
				.permitAll().antMatchers("/api/**").authenticated().
        antMatchers("/api/documents/directory/**").permitAll().
        antMatchers("/api/documents/directory").permitAll()
//				.antMatchers("/websocket/tracker").hasAuthority(AuthoritiesConstants.ADMIN)
				.antMatchers("/websocket/**").permitAll()
//				.antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN).antMatchers("/v2/api-docs/**").permitAll()
				.antMatchers("/swagger-resources/configuration/ui").permitAll().and()
//				.antMatchers("/swagger-ui/index.html").hasAuthority(AuthoritiesConstants.ADMIN).and()
				.apply(securityConfigurerAdapter()).and()
				.apply(tenantFilterAdapter());

	
		String requireSecure = env.getProperty("server.requireSecure");
		if (requireSecure != null) {
			if (requireSecure.equals("true")) {
				http.requiresChannel().anyRequest().requiresSecure();
			}

		}
		
		http.authorizeRequests().accessDecisionManager(decisionManager);

	}

	private MultiTenancySecurityConfigurer tenantFilterAdapter() {
		return new MultiTenancySecurityConfigurer(walterProperties);
	}

	private JWTConfigurer securityConfigurerAdapter() {
		return new JWTConfigurer(tokenProvider);
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
		return new SecurityEvaluationContextExtension();
	}
}
