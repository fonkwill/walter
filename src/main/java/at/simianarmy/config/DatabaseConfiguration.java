package at.simianarmy.config;

import at.simianarmy.config.liquibase.AsyncSpringLiquibase;
import at.simianarmy.config.liquibase.MultipleAsyncSpringLiquibase;
import at.simianarmy.service.util.BackupUtil;
import at.simianarmy.service.util.RestoreUtil;

import java.sql.SQLException;
import javax.inject.Inject;
import javax.sql.DataSource;

import liquibase.integration.spring.SpringLiquibase;

import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories({ "at.simianarmy.repository", "at.simianarmy.roles.repository", "at.simianarmy.multitenancy.repository", "at.simianarmy.filestore.repository" })
@EnableJpaAuditing(auditorAwareRef = "springSecurityAuditorAware")
@EnableTransactionManagement
public class DatabaseConfiguration {

	private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

	@Inject
	private Environment env;

	@Value("${walter.db.type:none}")
	String database;

	@Value("${walter.multitenancy.enabled:false}")
	Boolean multitenancy;

	/**
	 * Open the TCP port for the H2 database, so it is available remotely.
	 *
	 * @return the H2 database TCP server
	 * @throws SQLException if the server failed to start
	 */
	@Bean(initMethod = "start", destroyMethod = "stop")
	@Profile(Constants.SPRING_PROFILE_DEVELOPMENT)
	public Server h2TCPServer() throws SQLException {
		return Server.createTcpServer("-tcp", "-tcpAllowOthers");
	}

	@Bean
	public SpringLiquibase liquibase(DataSource dataSource, LiquibaseProperties liquibaseProperties) {

		// Use liquibase.integration.spring.SpringLiquibase if you don't want Liquibase
		// to start
		// asynchronously
		SpringLiquibase liquibase = new AsyncSpringLiquibase();
		liquibase.setDataSource(dataSource);
		liquibase.setChangeLog("classpath:config/liquibase/master.xml");
		liquibase.setContexts(liquibaseProperties.getContexts());
		liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
		liquibase.setDropFirst(liquibaseProperties.isDropFirst());
		if (env.acceptsProfiles(Constants.SPRING_PROFILE_NO_LIQUIBASE)) {
			liquibase.setShouldRun(false);
		} else {
			liquibase.setShouldRun(liquibaseProperties.isEnabled());
			log.debug("Configuring Liquibase");
		}

		return liquibase;

	}

	@Bean
	@ConditionalOnProperty(name = "walter.multitenancy.enabled", havingValue = "true", matchIfMissing = false)
	public MultipleAsyncSpringLiquibase multipleLiquibase() {

		MultipleAsyncSpringLiquibase liquibase = new MultipleAsyncSpringLiquibase();
		return liquibase;
	}

	@Bean
	public BackupUtil getBackup() {

		return BackupUtil.getInstance(database);
	}

	@Bean
	public RestoreUtil getRestore() {

		return RestoreUtil.getInstance(database);
	}
	//
	//
	//
	// @Bean
	// public Hibernate4Module hibernate4Module() {
	// return new Hibernate4Module();
	// }
}
