package at.simianarmy.config;

import at.simianarmy.filestore.FileStore;
import at.simianarmy.config.filestore.FileStoreFactory;
import javax.inject.Inject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FileStoreConfiguration {

  @Inject
  private FileStoreFactory fileStoreFactory;

  @Bean
  public FileStore fileStore() {
    return this.fileStoreFactory.createFileStore();
  }

}
