package at.simianarmy.config;

import at.simianarmy.config.filestore.FileStoreType;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 *
 * <p>
 * Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "filestore")
public class FileStoreProperties {

  private FileStoreType type;

  private String root = "/";

  private String accessToken;

  private String templatesLocation = "templates/";

  private String receiptsLocation = "receipts/";

  private String documentsLocation = "documents/";

  public FileStoreType getType() {
    return type;
  }

  public void setType(FileStoreType type) {
    this.type = type;
  }

  public String getRoot() {
    return root;
  }

  public void setRoot(String root) {
    this.root = root;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getTemplatesLocation() {
    return templatesLocation;
  }

  public void setTemplatesLocation(String templatesLocation) {
    this.templatesLocation = templatesLocation;
  }

  public String getReceiptsLocation() {
    return receiptsLocation;
  }

  public void setReceiptsLocation(String receiptsLocation) {
    this.receiptsLocation = receiptsLocation;
  }

  public String getDocumentsLocation() {
    return documentsLocation;
  }

  public void setDocumentsLocation(String documentsLocation) {
    this.documentsLocation = documentsLocation;
  }
}
