package at.simianarmy.config.liquibase;

import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import at.simianarmy.config.Constants;
import at.simianarmy.multitenancy.TenantConnectionProvider;
import at.simianarmy.multitenancy.TenantDataSourceBuilder;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationRepository;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;

public class MultipleAsyncSpringLiquibase {

	private static final Logger logger = LoggerFactory.getLogger(MultipleAsyncSpringLiquibase.class);

	@Inject
	private TenantConfigurationRepository tenantConfigurationRepository;

	@Inject
	private LiquibaseProperties liquibaseProperties;

	@Inject
	private Environment env;

	@Inject
	private ResourceLoader resourceLoader;

	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	@Autowired
	private TenantConnectionProvider connProvider;

	@PostConstruct
	public void postProcessBeanFactory() throws BeansException {
		if (tenantConfigurationRepository == null) {
			return;
		}

		for (Entry<String, TenantConfiguration> config : tenantConfigurationRepository.getAllTenantConfigurations()
				.entrySet()) {
			DataSource ds = TenantDataSourceBuilder.createDataSource(config.getValue().getDbConfiguration());
			if (ds == null) {
				logger.error("Couldn't get datasource for tenant {}", config.getKey());
				continue;
			}
			TenantConfiguration tConfig = config.getValue();
			
			SpringLiquibase liquibase = runLiquibase(ds, tConfig, false);
			
		}

	}

	public SpringLiquibase runLiquibase(DataSource ds, TenantConfiguration tConfig, boolean forceLiquibase) {
		SpringLiquibase liquibase = new AsyncSpringLiquibase(true);
		liquibase.setDataSource(ds);
		liquibase.setChangeLog("classpath:config/liquibase/master.xml");
		liquibase.setContexts(liquibaseProperties.getContexts());
		liquibase.setDefaultSchema(tConfig.getSchema());
		liquibase.setDropFirst(liquibaseProperties.isDropFirst());
		liquibase.setResourceLoader(this.resourceLoader);
		if (env.acceptsProfiles(Constants.SPRING_PROFILE_NO_LIQUIBASE)) {
			liquibase.setShouldRun(false);
		} else {
			if (forceLiquibase) {
				liquibase.setShouldRun(true);
			} else {
				liquibase.setShouldRun(liquibaseProperties.isEnabled());
			}
			logger.debug("Configuring Liquibase");
		}
		logger.debug("Starting Liquibase for tenant: {}", tConfig.getTenantId());

		beanFactory.autowireBean(liquibase);
		try {
			liquibase.afterPropertiesSet();
		} catch (LiquibaseException e) {
			logger.error("Could not start Liquibase", e);
		}
		return liquibase;
	}

}
