package at.simianarmy.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 *
 * <p>
 * Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "walter")
public class WalterProperties {

	private String emailVerificationUrl = "";

	private String emailVerificationHost = "";

  private MultitenancyProperties multitenancy;

  public String getEmailVerificationUrl() {
    return emailVerificationUrl;
  }

  public void setEmailVerificationUrl(String emailVerificationUrl) {
    this.emailVerificationUrl = emailVerificationUrl;
  }

  public String getEmailVerificationHost() {
    return emailVerificationHost;
  }

  public void setEmailVerificationHost(String emailVerificationHost) {
    this.emailVerificationHost = emailVerificationHost;
  }

  public MultitenancyProperties getMultitenancy() {
    return multitenancy;
  }

  public void setMultitenancy(MultitenancyProperties multitenancy) {
    this.multitenancy = multitenancy;
  }

  public static class MultitenancyProperties {

    private boolean enabled = false;

    private String tenantConfig = "classpath:config/tenantConfig.json";

    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }

    public String getTenantConfig() {
      return tenantConfig;
    }

    public void setTenantConfig(String tenantConfig) {
      this.tenantConfig = tenantConfig;
    }


  }
}
