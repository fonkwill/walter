package at.simianarmy.config;

import at.simianarmy.config.autowire.AutowireHelper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutowireHelperConfiguration {

	@Bean
	public AutowireHelper autowireHelper() {
		return AutowireHelper.getInstance();
	}

}
