package at.simianarmy.export.akm;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.Composition;
import at.simianarmy.export.akm.xml.AkmMeldung;
import at.simianarmy.export.akm.xml.AkmReport;
import at.simianarmy.export.akm.xml.Allgemeines;
import at.simianarmy.export.akm.xml.Musikstueck;
import at.simianarmy.export.akm.xml.Veranstaltung;
import at.simianarmy.service.CustomizationService;

import java.io.File;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class AkmReportFactory {

	@Inject
	private AllgemeinesFactory allgemeinesFactory;

	@Inject
	private VeranstaltungFactory veranstaltungFactory;

	@Inject
	protected MusikstueckFactory musikstueckFactory;
	
	@Inject
	private CustomizationService customizationService;
	// FIXME musikstueckFactory should be private

	/**
	 * Creates an AkmMeldung. The appointments should be OFFICIAL appointments -
	 * isOfficial should be true, and there should be an associated
	 * OfficialAppointment. Appointments not meeting these requirements are ignored.
	 *
	 * @param vereinsdaten
	 *            File containing the vereinsdaten XML
	 * @param appointments
	 *            list of appointments the report should be generated from.
	 * @throws IncompleteAppointmentException
	 *             if a provided Appointment is an official appointment, but is
	 *             missing either the address or the organizer's name/address.
	 * @throws InvalidAssociationDataException
	 *             if the vereinsdaten file is missing or incomplete
	 */
	public AkmMeldung createAkmMeldung(Set<Appointment> appointments)
			throws InvalidAssociationDataException, IncompleteAppointmentException {



		AkmMeldung akmMeldung = new AkmMeldung();
		
		Allgemeines allgemeines = customizationService.getAKMData();

		akmMeldung.setAllgemeines(allgemeinesFactory.createAllgemeines(allgemeines));

		Map<String, Musikstueck> musikstuecke = new TreeMap<>();

		// veranstaltungen
		for (Appointment appointment : appointments) {
			try {
				Veranstaltung veranstaltung = veranstaltungFactory.createVeranstaltung(appointment);
				akmMeldung.addVeranstaltung(veranstaltung);

				// musikstuecke
				for (Composition composition : appointment.getOfficialAppointment().getCompositions()) {
					String key = veranstaltung.getAkmJahr() + "_" + composition.getId();
					Musikstueck musikstueck = musikstuecke.get(key);
					if (musikstueck == null) {
						musikstueck = musikstueckFactory.createMusikstueck(composition);
						musikstuecke.put(key, musikstueck);
					}
					if (musikstueck.getJahr() == null) {
						musikstueck.setJahr(veranstaltung.getAkmJahr());
					} else if (veranstaltung.getAkmJahr() != musikstueck.getJahr().intValue()) {
						throw new AssertionError(
								String.format("Wrong year %d of Veranstaltung %s for Musikstueck %s (year %d)",
										veranstaltung.getAkmJahr(), veranstaltung.getName(), musikstueck.getTitel(),
										musikstueck.getJahr()));
					}
					musikstueck.incAnz();
				}
			} catch (NotAnOfficialAppointmentException exc) {
				// AppointmentDTOs that don't represent an official appointment are just ignored
				continue;
			}
		}
		akmMeldung.setMusikstuecke(new LinkedList<>(musikstuecke.values()));

		return akmMeldung;
	}

	public AkmReport createAkmReport(Set<Appointment> appointments)
			throws InvalidAssociationDataException, IncompleteAppointmentException {
		AkmReport akmReport = new AkmReport();
		akmReport.setAkmMeldung(createAkmMeldung(appointments));
		return akmReport;

	}

}
