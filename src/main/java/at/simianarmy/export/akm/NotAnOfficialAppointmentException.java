package at.simianarmy.export.akm;

public class NotAnOfficialAppointmentException extends Exception {
	private static final long serialVersionUID = 1L;

	public NotAnOfficialAppointmentException(String message) {
		super(message);
	}

	public NotAnOfficialAppointmentException(String message, Exception cause) {
		super(message, cause);
	}

}
