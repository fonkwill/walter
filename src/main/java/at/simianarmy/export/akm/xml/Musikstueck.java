package at.simianarmy.export.akm.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "musikstueck")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "werknummer", "titel", "komponist", "arrangeur", "anz", "jahr" })
public class Musikstueck {

	@XmlElement(name = "aks_id")
	private long id;

	@XmlElement(name = "aks_werknummer")
	private int werknummer;

	@XmlElement(name = "aks_titel")
	private String titel;

	@XmlElement(name = "aks_komponist")
	private String komponist;

	@XmlElement(name = "aks_arrangeur")
	private String arrangeur;

	@XmlElement(name = "aks_anz_auffuehrungen")
	private int anz = 0;

	@XmlElement(name = "aks_jahr")
	private Integer jahr;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getWerknummer() {
		return werknummer;
	}

	public void setWerknummer(int werknummer) {
		this.werknummer = werknummer;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getKomponist() {
		return komponist;
	}

	public void setKomponist(String komponist) {
		this.komponist = komponist;
	}

	public String getArrangeur() {
		return arrangeur;
	}

	public void setArrangeur(String arrangeur) {
		this.arrangeur = arrangeur;
	}

	public int getAnz() {
		return anz;
	}

	public void setAnz(int anz) {
		this.anz = anz;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	public int incAnz() {
		return ++anz;
	}
}
