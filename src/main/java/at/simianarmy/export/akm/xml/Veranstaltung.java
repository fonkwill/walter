package at.simianarmy.export.akm.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "veranstaltung")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "name", "datum", "von", "bis", "ort", "veranstalter", "veranstalterAdresse", "kopfquote" })
public class Veranstaltung {
	// TODO zu melden sind nur Veranstaltungen in Österreich!
	// FIXME was passiert, wenn startDate und endDate an verschiedenen Tagen sind?

	@XmlElement(name = "akv_name")
	private String name;

	@XmlElement(name = "akv_datum")
	private String datum;

	@XmlElement(name = "akv_von")
	private String von;

	@XmlElement(name = "akv_bis")
	private String bis;

	@XmlElement(name = "akv_ort")
	private String ort;

	@XmlElement(name = "akv_veranstalter")
	private String veranstalter;

	@XmlElement(name = "akv_veranstalter_adresse")
	private String veranstalterAdresse;

	@XmlElement(name = "akv_kopfquote")
	private int kopfquote;

	@XmlTransient
	private int akmJahr;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getVon() {
		return von;
	}

	public void setVon(String von) {
		this.von = von;
	}

	public String getBis() {
		return bis;
	}

	public void setBis(String bis) {
		this.bis = bis;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getVeranstalter() {
		return veranstalter;
	}

	public void setVeranstalter(String veranstalter) {
		this.veranstalter = veranstalter;
	}

	public String getVeranstalterAdresse() {
		return veranstalterAdresse;
	}

	public void setVeranstalterAdresse(String veranstalterAdresse) {
		this.veranstalterAdresse = veranstalterAdresse;
	}

	public int getKopfquote() {
		return kopfquote;
	}

	public void setKopfquote(int kopfquote) {
		this.kopfquote = kopfquote;
	}

	public int getAkmJahr() {
		return akmJahr;
	}

	public void setAkmJahr(int akmJahr) {
		this.akmJahr = akmJahr;
	}

}
