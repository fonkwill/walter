package at.simianarmy.export.akm.xml;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "akm_meldung")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "allgemeines", "musikstuecke", "veranstaltungen" })

public class AkmMeldung {

	@XmlAttribute(name = "ver_id")
	private String vereinsID;

	@XmlElement(name = "allgemeines")
	private Allgemeines allgemeines;

	@XmlElementWrapper
	@XmlElement(name = "musikstueck")
	private List<Musikstueck> musikstuecke = new LinkedList<>();

	private class BeginDateComparator implements Comparator<Veranstaltung> {
		@Override
		public int compare(Veranstaltung v1, Veranstaltung v2) {
			int result = v1.getDatum().compareTo(v2.getDatum());
			if (result == 0) {
				result = v1.getVon().compareTo(v2.getVon());
			}
			return result;
		}
	}

	@XmlElementWrapper
	@XmlElement(name = "veranstaltung")
	private SortedSet<Veranstaltung> veranstaltungen = new TreeSet<>(new BeginDateComparator());

	public String getVereinsID() {
		return vereinsID;
	}

	public void setVereinsID(String vereinsID) {
		this.vereinsID = vereinsID;
	}

	public Allgemeines getAllgemeines() {
		return allgemeines;
	}

	public void setAllgemeines(Allgemeines allgemeines) {
		this.allgemeines = allgemeines;
		this.vereinsID = allgemeines.getVerId();
	}

	public List<Musikstueck> getMusikstuecke() {
		return musikstuecke;
	}

	public void setMusikstuecke(List<Musikstueck> musikstuecke) {
		this.musikstuecke = musikstuecke;
	}

	public boolean addMusikstueck(Musikstueck musikstueck) {
		return musikstuecke.add(musikstueck);
	}

	public SortedSet<Veranstaltung> getVeranstaltungen() {
		return veranstaltungen;
	}

	public void setVeranstaltungen(SortedSet<Veranstaltung> veranstaltungen) {
		this.veranstaltungen = veranstaltungen;
	}

	public boolean addVeranstaltung(Veranstaltung veranstaltung) {
		return veranstaltungen.add(veranstaltung);
	}

}
