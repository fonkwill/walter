package at.simianarmy.export.akm.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "allgemeines")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "verId", "verName", "adrStrasse", "adrPlz", "adrOrt", "ausstellerName", "ausstellerAdresse" })
public class Allgemeines {

	@XmlElement(name = "ver_id")
	private String verId;

	@XmlElement(name = "ver_name")
	private String verName;

	@XmlElement(name = "adr_strasse")
	private String adrStrasse;

	@XmlElement(name = "adr_plz")
	private String adrPlz;

	@XmlElement(name = "adr_ort")
	private String adrOrt;

	@XmlElement(name = "aussteller_name")
	private String ausstellerName;

	@XmlElement(name = "aussteller_adresse")
	private String ausstellerAdresse;

	public String getVerId() {
		return verId;
	}

	public void setVerId(String verId) {
		this.verId = verId;
	}

	public String getVerName() {
		return verName;
	}

	public void setVerName(String verName) {
		this.verName = verName;
	}

	public String getAdrStrasse() {
		return adrStrasse;
	}

	public void setAdrStrasse(String adrStrasse) {
		this.adrStrasse = adrStrasse;
	}

	public String getAdrPlz() {
		return adrPlz;
	}

	public void setAdrPlz(String adrPlz) {
		this.adrPlz = adrPlz;
	}

	public String getAdrOrt() {
		return adrOrt;
	}

	public void setAdrOrt(String adrOrt) {
		this.adrOrt = adrOrt;
	}

	public String getAusstellerName() {
		return ausstellerName;
	}

	public void setAusstellerName(String ausstellerName) {
		this.ausstellerName = ausstellerName;
	}

	public String getAusstellerAdresse() {
		return ausstellerAdresse;
	}

	public void setAusstellerAdresse(String ausstellerAdresse) {
		this.ausstellerAdresse = ausstellerAdresse;
	}
}
