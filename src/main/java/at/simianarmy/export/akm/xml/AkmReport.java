package at.simianarmy.export.akm.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * An AkmReport containing a single AkmMeldung
 */
@XmlRootElement(name = "akm_meldungen")
@XmlAccessorType(XmlAccessType.FIELD)
public class AkmReport {

	@XmlElement(name = "akm_meldung")
	private AkmMeldung akmMeldung;

	public AkmMeldung getAkmMeldung() {
		return akmMeldung;
	}

	public void setAkmMeldung(AkmMeldung akmMeldung) {
		this.akmMeldung = akmMeldung;
	}
}
