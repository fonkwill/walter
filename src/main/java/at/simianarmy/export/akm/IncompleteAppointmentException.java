package at.simianarmy.export.akm;

public class IncompleteAppointmentException extends Exception {
	private static final long serialVersionUID = 1L;

	public IncompleteAppointmentException(String message) {
		super(message);
	}

	public IncompleteAppointmentException(String message, Exception cause) {
		super(message, cause);
	}

}
