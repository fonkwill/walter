package at.simianarmy.export.akm;

import at.simianarmy.export.akm.xml.Allgemeines;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class AllgemeinesFactory {

	/**
	 * Creates a new Allgemeines section from the specified XML file containing
	 * vereinsdaten.
	 *
	 * @param vereinsdaten
	 *            File containing the vereinsdaten XML
	 * @return the Allgemeines instance
	 * @throws InvalidAssociationDataException
	 *             if the vereinsdaten file is missing or incomplete
	 */
	public Allgemeines createAllgemeines(Allgemeines allgemeines) throws InvalidAssociationDataException {

		
		if (allgemeines == null) {
			throw new InvalidAssociationDataException("Vereinsdaten fehlen");
		}

		if (allgemeines.getVerId() == null) {
			throw new InvalidAssociationDataException("Vereins-ID fehlt");
		}
		if (allgemeines.getVerName() == null) {
			throw new InvalidAssociationDataException("Vereinsname fehlt");
		}
		if (allgemeines.getAdrStrasse() == null) {
			throw new InvalidAssociationDataException("Anschrift des Vereins: Straße fehlt");
		}
		if (allgemeines.getAdrPlz() == null) {
			throw new InvalidAssociationDataException("Anschrift des Vereins: Postleitzahl fehlt");
		}
		if (allgemeines.getAdrOrt() == null) {
			throw new InvalidAssociationDataException("Anschrift des Vereins: Ort fehlt");
		}
		if (allgemeines.getAusstellerName() == null) {
			allgemeines.setAusstellerName("");
		}
		if (allgemeines.getAusstellerAdresse() == null) {
			allgemeines.setAusstellerAdresse("");
		}
		return allgemeines;
	}
}
