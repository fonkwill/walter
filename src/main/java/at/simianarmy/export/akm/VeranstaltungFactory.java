package at.simianarmy.export.akm;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.export.akm.xml.Veranstaltung;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class VeranstaltungFactory {

	/**
	 * Creates a single String representation from adresse, plz and ort (all
	 * optional).
	 *
	 * @param adresse
	 *            Adresse
	 * @param plz
	 *            Postleitzahl
	 * @param ort
	 *            Ort
	 * @return the resulting String
	 */
	private String createOrt(String adresse, String plz, String ort) {
		adresse = adresse == null ? "" : adresse.trim();
		plz = plz == null ? "" : plz.trim();
		ort = ort == null ? "" : ort.trim();

		ort = (plz + " " + ort).trim();
		if (adresse.length() > 0) {
			if (ort.length() > 0) {
				return adresse + ", " + ort;
			} else {
				return adresse;
			}
		} else {
			return ort;
		}
	}

	/**
	 * Returns the AKM billing year for this begin date of an Appointment. According
	 * to AKM, Appointments until November 30th are billed in the current year,
	 * later appointments are billed in the next year.
	 *
	 * @return the billing year the appointment is assigned to
	 */
	private int createAKMJahr(ZonedDateTime beginDate) {
		if (beginDate.getMonthValue() < 12) {
			return beginDate.getYear();
		} else {
			return beginDate.getYear() + 1;
		}
	}

	/**
	 * Creates a veranstaltung, which is an XML representation of an official
	 * Appointment for the AKM Export.
	 *
	 * @param appointment
	 *            the Appointment to represent
	 * @return the created Veranstaltung
	 * @throws NotAnOfficialAppointmentException
	 *             if the appointment is not an official appointment
	 * @throws IncompleteAppointmentException
	 *             if mandatory fields are missing
	 */
	public Veranstaltung createVeranstaltung(Appointment appointment)
			throws NotAnOfficialAppointmentException, IncompleteAppointmentException {

		OfficialAppointment officialAppointment = appointment.getOfficialAppointment();
		if (officialAppointment == null) {
			throw new NotAnOfficialAppointmentException("not an official appointment: " + appointment);
		}
		if (appointment.getStreetAddress() == null && appointment.getCity() == null) {
			throw new IncompleteAppointmentException("Ort fehlt: " + appointment);
		}
		if (officialAppointment.getOrganizerName() == null) {
			throw new IncompleteAppointmentException("Kein Veranstalter: " + appointment);
		}
		if (officialAppointment.getOrganizerAddress() == null) {
			throw new IncompleteAppointmentException("Keine Adresse des Veranstalters: " + appointment);
		}

		Veranstaltung veranstaltung = new Veranstaltung();

		veranstaltung.setName(appointment.getName());

		DateTimeFormatter formatter;
		formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		veranstaltung.setDatum(appointment.getBeginDate().format(formatter));
		formatter = DateTimeFormatter.ofPattern("HH:mm");
		veranstaltung.setVon(appointment.getBeginDate().format(formatter));
		veranstaltung.setBis(appointment.getEndDate().format(formatter));

		veranstaltung
				.setOrt(createOrt(appointment.getStreetAddress(), appointment.getPostalCode(), appointment.getCity()));

		veranstaltung.setVeranstalter(officialAppointment.getOrganizerName());
		veranstaltung.setVeranstalterAdresse(officialAppointment.getOrganizerAddress());

		// this avoids checking for != null
		if (Boolean.TRUE.equals(officialAppointment.isIsHeadQuota())) {
			veranstaltung.setKopfquote(1);
		} else {
			veranstaltung.setKopfquote(0);
		}

		veranstaltung.setAkmJahr(createAKMJahr(appointment.getBeginDate()));

		return veranstaltung;
	}
}
