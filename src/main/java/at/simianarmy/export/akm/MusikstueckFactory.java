package at.simianarmy.export.akm;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.export.akm.xml.Musikstueck;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.repository.ComposerRepository;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Scope("singleton")
@Component
public class MusikstueckFactory {

	@Inject
	private ComposerRepository composerRepository;

	@Inject
	private ArrangerRepository arrangerRepository;

	/**
	 * Creates a Musikstueck from a Composition. Note: Year is uninitialized and has
	 * to be set when the Musikstueck is played at an Auffuehrung
	 *
	 * @param composition
	 *            the Composition to convert to a Musikstueck
	 * @return the created Musikstueck
	 */
	public Musikstueck createMusikstueck(Composition composition) {
		Musikstueck musikstueck = new Musikstueck();

		musikstueck.setId(composition.getId());
		musikstueck.setWerknummer(composition.getAkmCompositionNr());
		musikstueck.setTitel(composition.getTitle());

		List<Composer> composers = composerRepository.findByComposition(composition.getId());
		List<String> composerNames = composers.stream().map(composer -> composer.getName())
				.collect(Collectors.toList());
		musikstueck.setKomponist(String.join(", ", composerNames));

		List<Arranger> arrangers = arrangerRepository.findByComposition(composition.getId());
		List<String> arrangerNames = arrangers.stream().map(arranger -> arranger.getName())
				.collect(Collectors.toList());
		musikstueck.setArrangeur(String.join(", ", arrangerNames));

		return musikstueck;
	}

}
