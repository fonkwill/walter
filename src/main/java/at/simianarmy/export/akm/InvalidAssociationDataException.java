package at.simianarmy.export.akm;

public class InvalidAssociationDataException extends Exception {
	private static final long serialVersionUID = 1L;

	public InvalidAssociationDataException(String message) {
		super(message);
	}

	public InvalidAssociationDataException(String message, Exception cause) {
		super(message, cause);
	}

}
