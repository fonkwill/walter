package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A MusicBook.
 */
@Entity
@Table(name = "music_book")
public class MusicBook implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "musicBook")
	@JsonIgnore
	private Set<Composition> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public MusicBook name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public MusicBook description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Composition> getCompositions() {
		return compositions;
	}

	public MusicBook compositions(Set<Composition> compositions) {
		this.compositions = compositions;
		return this;
	}

	public MusicBook addCompositions(Composition composition) {
		compositions.add(composition);
		composition.setMusicBook(this);
		return this;
	}

	public MusicBook removeCompositions(Composition composition) {
		compositions.remove(composition);
		composition.setMusicBook(null);
		return this;
	}

	public void setCompositions(Set<Composition> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		MusicBook musicBook = (MusicBook) obj;
		if (musicBook.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, musicBook.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MusicBook{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'" + '}';
	}
}
