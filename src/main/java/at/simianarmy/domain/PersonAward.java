package at.simianarmy.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A PersonAward.
 */
@Entity
@Table(name = "person_award")
public class PersonAward implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date", nullable = false)
	private LocalDate date;

	@ManyToOne
	private Award award;

	@ManyToOne
	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public PersonAward date(LocalDate date) {
		this.date = date;
		return this;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Award getAward() {
		return award;
	}

	public PersonAward award(Award award) {
		this.award = award;
		return this;
	}

	public void setAward(Award award) {
		this.award = award;
	}

	public Person getPerson() {
		return person;
	}

	public PersonAward person(Person person) {
		this.person = person;
		return this;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		PersonAward personAward = (PersonAward) obj;
		if (personAward.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, personAward.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonAward{" + "id=" + id + ", date='" + date + "'" + '}';
	}
}
