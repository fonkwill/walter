package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A InstrumentService.
 */
@Entity
@Table(name = "instrument_service")
public class InstrumentService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date", nullable = false)
	private LocalDate date;

	@Column(name = "note")
	private String note;

	@OneToOne
	@JoinColumn(unique = true)
	private CashbookEntry cashbookEntry;

	@ManyToOne
	@NotNull
	private Instrument instrument;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public InstrumentService date(LocalDate date) {
		this.date = date;
		return this;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public InstrumentService note(String note) {
		this.note = note;
		return this;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public CashbookEntry getCashbookEntry() {
		return cashbookEntry;
	}

	public InstrumentService cashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
		return this;
	}

	public void setCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public InstrumentService instrument(Instrument instrument) {
		this.instrument = instrument;
		return this;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		InstrumentService instrumentService = (InstrumentService) o;
		if (instrumentService.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, instrumentService.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "InstrumentService{" + "id=" + id + ", date='" + date + "'" + ", note='" + note + "'" + '}';
	}
}
