package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PersonClothing.
 */
@Entity
@Table(name = "person_clothing")
public class PersonClothing implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "begin_date", nullable = false)
	private LocalDate beginDate;

	@Column(name = "end_date")
	private LocalDate endDate;

	@ManyToOne
	private Clothing clothing;

	@ManyToOne
	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public PersonClothing beginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public PersonClothing endDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Clothing getClothing() {
		return clothing;
	}

	public PersonClothing clothing(Clothing clothing) {
		this.clothing = clothing;
		return this;
	}

	public void setClothing(Clothing clothing) {
		this.clothing = clothing;
	}

	public Person getPerson() {
		return person;
	}

	public PersonClothing person(Person person) {
		this.person = person;
		return this;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PersonClothing personClothing = (PersonClothing) o;
		if (personClothing.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, personClothing.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonClothing{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'" + '}';
	}
}
