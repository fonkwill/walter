package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.notification.NotificationEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import at.simianarmy.domain.enumeration.Gender;

/**
 * A Person.
 */
@Entity
@Table(name = "person")
public class Person implements Serializable, NotificationEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "first_name", nullable = false)
	private String firstName;

	@NotNull
	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "birth_date")
	private LocalDate birthDate;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "email_type", nullable = false)
  private EmailType emailType = EmailType.MANUALLY;

	@Column(name = "email")
	private String email;

	@Column(name = "telephone_number")
	private String telephoneNumber;

	@NotNull
	@Column(name = "street_address", nullable = false)
	private String streetAddress;

	@NotNull
	@Column(name = "postal_code", nullable = false)
	private String postalCode;

	@NotNull
	@Column(name = "city", nullable = false)
	private String city;

	@NotNull
	@Column(name = "country", nullable = false)
	private String country;

	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
	private Gender gender;

	@NotNull
  @Column(name = "has_direct_debit", nullable = false)
	private Boolean hasDirectDebit;



  @Column(name = "date_lost_relevance")
  private LocalDate dateLostRelevance;

	@OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<PersonClothing> personClothings = new HashSet<>();

	@OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<PersonInstrument> personInstruments = new HashSet<>();

	@OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<PersonHonor> personHonors = new HashSet<>();

	@OneToMany(mappedBy = "person", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<PersonAward> personAwards = new HashSet<>();

	@OneToMany(mappedBy = "person", cascade = {CascadeType.DETACH, CascadeType.REMOVE}, orphanRemoval = true)
	@JsonIgnore
	private Set<Membership> memberships = new HashSet<>();

	@ManyToMany
	@JoinTable(name = "person_person_groups", joinColumns = @JoinColumn(name = "people_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "person_groups_id", referencedColumnName = "ID"))
	private Set<PersonGroup> personGroups = new HashSet<>();

	@ManyToMany
  @JoinTable(name = "appointment_persons", joinColumns = @JoinColumn(name = "persons_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "appointments_id", referencedColumnName = "ID"))
	@JsonIgnore
	private Set<Appointment> appointments = new HashSet<>();

	@OneToMany(mappedBy = "person", cascade = {CascadeType.DETACH, CascadeType.REMOVE}, orphanRemoval = true)
	private Set<EmailVerificationLink> emailVerificationLinks = new HashSet<>();

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public Person firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Person lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public Person birthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

  public EmailType getEmailType() {
    return emailType;
  }

  public Person emailType(EmailType emailType) {
	  this.emailType = emailType;
	  return this;
  }

  public void setEmailType(EmailType emailType) {
    this.emailType = emailType;
  }

  public String getEmail() {
		return email;
	}

	public Person email(String email) {
		this.email = email;
		return this;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public Person telephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
		return this;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public Person streetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
		return this;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public Person postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public Person city(String city) {
		this.city = city;
		return this;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public Person country(String country) {
		this.country = country;
		return this;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Gender getGender() {
		return gender;
	}

	public Person gender(Gender gender) {
		this.gender = gender;
		return this;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Boolean getHasDirectDebit() { return hasDirectDebit; }

	public Person hasDirectDebit(Boolean hasDirectDebit) {
	  this.hasDirectDebit = hasDirectDebit;
	  return this;
  }

  public void setHasDirectDebit(Boolean hasDirectDebit) { this.hasDirectDebit = hasDirectDebit; }

	public Set<PersonClothing> getPersonClothings() {
		return personClothings;
	}

	public Person personClothings(Set<PersonClothing> personClothings) {
		this.personClothings = personClothings;
		return this;
	}

	public Person addPersonClothings(PersonClothing personClothing) {
		personClothings.add(personClothing);
		personClothing.setPerson(this);
		return this;
	}

	public Person removePersonClothings(PersonClothing personClothing) {
		personClothings.remove(personClothing);
		personClothing.setPerson(null);
		return this;
	}

	public void setPersonClothings(Set<PersonClothing> personClothings) {
		this.personClothings = personClothings;
	}

	public Set<PersonInstrument> getPersonInstruments() {
		return personInstruments;
	}

	public Person personInstruments(Set<PersonInstrument> personInstruments) {
		this.personInstruments = personInstruments;
		return this;
	}

	public Person addPersonInstruments(PersonInstrument personInstrument) {
		personInstruments.add(personInstrument);
		personInstrument.setPerson(this);
		return this;
	}

	public Person removePersonInstruments(PersonInstrument personInstrument) {
		personInstruments.remove(personInstrument);
		personInstrument.setPerson(null);
		return this;
	}

	public void setPersonInstruments(Set<PersonInstrument> personInstruments) {
		this.personInstruments = personInstruments;
	}

	public Set<PersonHonor> getPersonHonors() {
		return personHonors;
	}

	public Person personHonors(Set<PersonHonor> personHonors) {
		this.personHonors = personHonors;
		return this;
	}

	public Person addPersonHonors(PersonHonor personHonor) {
		personHonors.add(personHonor);
		personHonor.setPerson(this);
		return this;
	}

	public Person removePersonHonors(PersonHonor personHonor) {
		personHonors.remove(personHonor);
		personHonor.setPerson(null);
		return this;
	}

	public void setPersonHonors(Set<PersonHonor> personHonors) {
		this.personHonors = personHonors;
	}

	public Set<PersonAward> getPersonAwards() {
		return personAwards;
	}

	public Person personAwards(Set<PersonAward> personAwards) {
		this.personAwards = personAwards;
		return this;
	}

	public Person addPersonAwards(PersonAward personAward) {
		personAwards.add(personAward);
		personAward.setPerson(this);
		return this;
	}

	public Person removePersonAwards(PersonAward personAward) {
		personAwards.remove(personAward);
		personAward.setPerson(null);
		return this;
	}

	public void setPersonAwards(Set<PersonAward> personAwards) {
		this.personAwards = personAwards;
	}

	public Set<Membership> getMemberships() {
		return memberships;
	}

	public Person memberships(Set<Membership> memberships) {
		this.memberships = memberships;
		return this;
	}

	public Person addMemberships(Membership membership) {
		memberships.add(membership);
		membership.setPerson(this);
		return this;
	}

	public Person removeMemberships(Membership membership) {
		memberships.remove(membership);
		membership.setPerson(null);
		return this;
	}

	public void setMemberships(Set<Membership> memberships) {
		this.memberships = memberships;
	}

	public Set<PersonGroup> getPersonGroups() {
		return personGroups;
	}

	public Person personGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
		return this;
	}

	public Person addPersonGroups(PersonGroup personGroup) {
		personGroups.add(personGroup);
		personGroup.getPersons().add(this);
		return this;
	}

	public Person removePersonGroups(PersonGroup personGroup) {
		personGroups.remove(personGroup);
		personGroup.getPersons().remove(this);
		return this;
	}

	public void setPersonGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
	}


	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public Person appointments(Set<Appointment> appointments) {
		this.appointments = appointments;
		return this;
	}

	public Person addAppointments(Appointment appointment) {
		appointments.add(appointment);
		appointment.getPersons().add(this);
		return this;
	}

	public Person removeAppointments(Appointment appointment) {
		appointments.remove(appointment);
		appointment.getPersons().remove(this);
		return this;
	}

	public Set<EmailVerificationLink> getEmailVerificationLinks() {
	  return this.emailVerificationLinks;
  }

  public Person addEmailVerificationLink(EmailVerificationLink emailVerificationLink) {
	  this.emailVerificationLinks.add(emailVerificationLink);
	  return this;
  }

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}


  public LocalDate getDateLostRelevance() {
    return dateLostRelevance;
  }

  public void setDateLostRelevance(LocalDate dateLostRelevance) {
    this.dateLostRelevance = dateLostRelevance;
  }
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Person person = (Person) o;
		if (person.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, person.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Person{" + "id=" + id + ", firstName='" + firstName + "'" + ", lastName='" + lastName + "'"
				+ ", birthDate='" + birthDate + "'" + ", email='" + email + "'" + ", telephoneNumber='"
				+ telephoneNumber + "'" + ", streetAddress='" + streetAddress + "'" + ", postalCode='" + postalCode
				+ "'" + ", city='" + city + "'" + ", country='" + country + "'" + ", gender='" + gender + "'" + '}';
	}
}
