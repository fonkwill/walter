package at.simianarmy.domain;

import at.simianarmy.domain.listener.CompositionListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * A Composition.
 */
@Entity
@Table(name = "composition")
@EntityListeners({ CompositionListener.class })
public class Composition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "nr", nullable = false)
	private Integer nr;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "note")
	private String note;

	@NotNull
	@Column(name = "akm_composition_nr", nullable = false)
	private Integer akmCompositionNr;

	@ManyToMany
	@JoinTable(name = "composition_appointments", joinColumns = @JoinColumn(name = "compositions_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "appointments_id", referencedColumnName = "ID"))
	private Set<OfficialAppointment> appointments = new HashSet<>();

	@ManyToOne
	private CashbookEntry cashbookEntry;

	@ManyToOne
	private Company orderingCompany;

	@ManyToOne
	private Company publisher;

	@ManyToOne
	private Genre genre;

	@ManyToOne
	private MusicBook musicBook;

	@ManyToMany(mappedBy = "compositions")
	@JsonIgnore
	private Set<Arranger> arrangers = new HashSet<>();

	@ManyToMany(mappedBy = "compositions")
	@JsonIgnore
	private Set<Composer> composers = new HashSet<>();

	@ManyToOne
	@NotNull
	private ColorCode colorCode;

	@Transient
	private ColorCode oldColorCode;

	@Transient
	private Integer oldNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNr() {
		return nr;
	}

	public Composition nr(Integer nr) {
		this.oldNumber = this.nr;
		this.nr = nr;
		return this;
	}

	public void setNr(Integer nr) {
		this.oldNumber = this.nr;
		this.nr = nr;
	}

	public String getTitle() {
		return title;
	}

	public Composition title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public Composition note(String note) {
		this.note = note;
		return this;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getAkmCompositionNr() {
		return akmCompositionNr;
	}

	public Composition akmCompositionNr(Integer akmCompositionNr) {
		this.akmCompositionNr = akmCompositionNr;
		return this;
	}

	public void setAkmCompositionNr(Integer akmCompositionNr) {
		this.akmCompositionNr = akmCompositionNr;
	}

	public Set<OfficialAppointment> getAppointments() {
		return appointments;
	}

	public Composition appointments(Set<OfficialAppointment> officialAppointments) {
		this.appointments = officialAppointments;
		return this;
	}

	public Composition addAppointments(OfficialAppointment officialAppointment) {
		appointments.add(officialAppointment);
		officialAppointment.getCompositions().add(this);
		return this;
	}

	public Composition removeAppointments(OfficialAppointment officialAppointment) {
		appointments.remove(officialAppointment);
		officialAppointment.getCompositions().remove(this);
		return this;
	}

	public void setAppointments(Set<OfficialAppointment> officialAppointments) {
		this.appointments = officialAppointments;
	}

	public CashbookEntry getCashbookEntry() {
		return cashbookEntry;
	}

	public Composition cashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
		return this;
	}

	public void setCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
	}

	public Company getOrderingCompany() {
		return orderingCompany;
	}

	public Composition orderingCompany(Company company) {
		this.orderingCompany = company;
		return this;
	}

	public void setOrderingCompany(Company company) {
		this.orderingCompany = company;
	}

	public Company getPublisher() {
		return publisher;
	}

	public Composition publisher(Company company) {
		this.publisher = company;
		return this;
	}

	public void setPublisher(Company company) {
		this.publisher = company;
	}

	public Genre getGenre() {
		return genre;
	}

	public Composition genre(Genre genre) {
		this.genre = genre;
		return this;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public MusicBook getMusicBook() {
		return musicBook;
	}

	public Composition musicBook(MusicBook musicBook) {
		this.musicBook = musicBook;
		return this;
	}

	public void setMusicBook(MusicBook musicBook) {
		this.musicBook = musicBook;
	}

	public Set<Arranger> getArrangers() {
		return arrangers;
	}

	public Composition arrangers(Set<Arranger> arrangers) {
		this.arrangers = arrangers;
		return this;
	}

	public Composition addArrangers(Arranger arranger) {
		arrangers.add(arranger);
		arranger.getCompositions().add(this);
		return this;
	}

	public Composition removeArrangers(Arranger arranger) {
		arrangers.remove(arranger);
		arranger.getCompositions().remove(this);
		return this;
	}

	public void setArrangers(Set<Arranger> arrangers) {
		this.arrangers = arrangers;
	}

	public Set<Composer> getComposers() {
		return composers;
	}

	public Composition composers(Set<Composer> composers) {
		this.composers = composers;
		return this;
	}

	public Composition addComposers(Composer composer) {
		composers.add(composer);
		composer.getCompositions().add(this);
		return this;
	}

	public Composition removeComposers(Composer composer) {
		composers.remove(composer);
		composer.getCompositions().remove(this);
		return this;
	}

	public void setComposers(Set<Composer> composers) {
		this.composers = composers;
	}

	public ColorCode getColorCode() {
		return colorCode;
	}

	public Composition colorCode(ColorCode colorCode) {
		this.oldColorCode = this.colorCode;
		this.colorCode = colorCode;
		return this;
	}

	public void setColorCode(ColorCode colorCode) {
		this.oldColorCode = this.colorCode;
		this.colorCode = colorCode;
	}

	public void setOldColorCode(ColorCode oldColorCode) {
		this.oldColorCode = oldColorCode;
	}

	public ColorCode getOldColorCode() {
		return this.oldColorCode;
	}

	public void setOldNumber(Integer number) {
		this.oldNumber = number;
	}

	public Integer getOldNumber() {
		return this.oldNumber;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Composition composition = (Composition) obj;
		if (composition.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, composition.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Composition{" + "id=" + id + ", nr='" + nr + "'" + ", title='" + title + "'" + ", note='" + note + "'"
				+ ", akmCompositionNr='" + akmCompositionNr + "'" + '}';
	}
}
