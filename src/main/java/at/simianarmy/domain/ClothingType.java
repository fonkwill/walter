package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A ClothingType.
 */
@Entity
@Table(name = "clothing_type")
public class ClothingType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "type")
	@JsonIgnore
	private Set<Clothing> clothings = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public ClothingType name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Clothing> getClothings() {
		return clothings;
	}

	public ClothingType clothings(Set<Clothing> clothing) {
		this.clothings = clothing;
		return this;
	}

	public ClothingType addClothings(Clothing clothing) {
		clothings.add(clothing);
		clothing.setType(this);
		return this;
	}

	public ClothingType removeClothings(Clothing clothing) {
		clothings.remove(clothing);
		clothing.setType(null);
		return this;
	}

	public void setClothings(Set<Clothing> clothing) {
		this.clothings = clothing;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		ClothingType clothingType = (ClothingType) obj;
		if (clothingType.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, clothingType.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ClothingType{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
