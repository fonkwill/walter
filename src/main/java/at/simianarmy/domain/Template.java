package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Template.
 */
@Entity
@Table(name = "template")
public class Template implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "file")
	private String file;

	@Column(name = "margin_left")
	private Float marginLeft;

	@Column(name = "margin_right")
	private Float marginRight;

	@Column(name = "margin_top")
	private Float marginTop;

	@Column(name = "margin_bottom")
	private Float marginBottom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public Template title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFile() {
		return file;
	}

	public Template file(String file) {
		this.file = file;
		return this;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Float getMarginLeft() {
		return marginLeft;
	}

	public Template marginLeft(Float marginLeft) {
		this.marginLeft = marginLeft;
		return this;
	}

	public void setMarginLeft(Float marginLeft) {
		this.marginLeft = marginLeft;
	}

	public Float getMarginRight() {
		return marginRight;
	}

	public Template marginRight(Float marginRight) {
		this.marginRight = marginRight;
		return this;
	}

	public void setMarginRight(Float marginRight) {
		this.marginRight = marginRight;
	}

	public Float getMarginTop() {
		return marginTop;
	}

	public Template marginTop(Float marginTop) {
		this.marginTop = marginTop;
		return this;
	}

	public void setMarginTop(Float marginTop) {
		this.marginTop = marginTop;
	}

	public Float getMarginBottom() {
		return marginBottom;
	}

	public Template marginBottom(Float marginBottom) {
		this.marginBottom = marginBottom;
		return this;
	}

	public void setMarginBottom(Float marginBottom) {
		this.marginBottom = marginBottom;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Template template = (Template) o;
		if (template.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, template.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Template{" + "id=" + id + ", title='" + title + "'" + ", file='" + file + "'" + ", marginLeft='"
				+ marginLeft + "'" + ", marginRight='" + marginRight + "'" + ", marginTop='" + marginTop + "'"
				+ ", marginBottom='" + marginBottom + "'" + '}';
	}
}
