package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Report.
 */
@Entity
@Table(name = "report")
public class Report implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "generation_date", nullable = false)
	private ZonedDateTime generationDate;

	@NotNull
	@Column(name = "association_id", nullable = false)
	private String associationId;

	@NotNull
	@Column(name = "association_name", nullable = false)
	private String associationName;

	@NotNull
	@Column(name = "street_address", nullable = false)
	private String streetAddress;

	@NotNull
	@Column(name = "postal_code", nullable = false)
	private String postalCode;

	@NotNull
	@Column(name = "city", nullable = false)
	private String city;

	@OneToMany(mappedBy = "report")
	@JsonIgnore
	private Set<OfficialAppointment> appointments = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getGenerationDate() {
		return generationDate;
	}

	public Report generationDate(ZonedDateTime generationDate) {
		this.generationDate = generationDate;
		return this;
	}

	public void setGenerationDate(ZonedDateTime generationDate) {
		this.generationDate = generationDate;
	}

	public String getAssociationId() {
		return associationId;
	}

	public Report associationId(String associationId) {
		this.associationId = associationId;
		return this;
	}

	public void setAssociationId(String associationId) {
		this.associationId = associationId;
	}

	public String getAssociationName() {
		return associationName;
	}

	public Report associationName(String associationName) {
		this.associationName = associationName;
		return this;
	}

	public void setAssociationName(String associationName) {
		this.associationName = associationName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public Report streetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
		return this;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public Report postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public Report city(String city) {
		this.city = city;
		return this;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Set<OfficialAppointment> getAppointments() {
		return appointments;
	}

	public Report appointments(Set<OfficialAppointment> officialAppointments) {
		this.appointments = officialAppointments;
		return this;
	}

	public Report addAppointments(OfficialAppointment officialAppointment) {
		appointments.add(officialAppointment);
		officialAppointment.setReport(this);
		return this;
	}

	public Report removeAppointments(OfficialAppointment officialAppointment) {
		appointments.remove(officialAppointment);
		officialAppointment.setReport(null);
		return this;
	}

	public void setAppointments(Set<OfficialAppointment> officialAppointments) {
		this.appointments = officialAppointments;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Report report = (Report) obj;
		if (report.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, report.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Report{" + "id=" + id + ", generationDate='" + generationDate + "'" + ", associationId='"
				+ associationId + "'" + ", associationName='" + associationName + "'" + ", streetAddress='"
				+ streetAddress + "'" + ", postalCode='" + postalCode + "'" + ", city='" + city + "'" + '}';
	}
}
