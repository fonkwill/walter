package at.simianarmy.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A PersonHonor.
 */
@Entity
@Table(name = "person_honor")
public class PersonHonor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date", nullable = false)
	private LocalDate date;

	@ManyToOne
	private Honor honor;

	@ManyToOne
	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public PersonHonor date(LocalDate date) {
		this.date = date;
		return this;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Honor getHonor() {
		return honor;
	}

	public PersonHonor honor(Honor honor) {
		this.honor = honor;
		return this;
	}

	public void setHonor(Honor honor) {
		this.honor = honor;
	}

	public Person getPerson() {
		return person;
	}

	public PersonHonor person(Person person) {
		this.person = person;
		return this;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		PersonHonor personHonor = (PersonHonor) obj;
		if (personHonor.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, personHonor.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonHonor{" + "id=" + id + ", date='" + date + "'" + '}';
	}
}
