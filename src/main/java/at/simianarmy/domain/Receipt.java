package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Receipt.
 */
@Entity
@Table(name = "receipt")
public class Receipt implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date", nullable = false)
	private LocalDate date;

	@Column(name = "note")
	private String note;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@Column(name = "file")
	private String file;

	@Column(name = "identifier", nullable = false, unique = true)
	private String identifier;

	@Column(name = "overwrite_identifier", nullable = false)
	private Boolean overwriteIdentifier;

	@ManyToOne
  private CashbookEntry initialCashbookEntry;

	@OneToMany(mappedBy = "receipt")
	@JsonIgnore
	private Set<CashbookEntry> entries = new HashSet<>();

	@ManyToOne
	private Company company;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public Receipt date(LocalDate date) {
		this.date = date;
		return this;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public Receipt note(String note) {
		this.note = note;
		return this;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getTitle() {
		return title;
	}

	public Receipt title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFile() {
		return file;
	}

	public Receipt file(String file) {
		this.file = file;
		return this;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getIdentifier() {
		return identifier;
	}

	public Receipt identifier(String identifier) {
		this.identifier = identifier;
		return this;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Set<CashbookEntry> getEntries() {
		return entries;
	}

	public Receipt entries(Set<CashbookEntry> cashbookEntries) {
		this.entries = cashbookEntries;
		return this;
	}

	public Receipt addEntries(CashbookEntry cashbookEntry) {
		entries.add(cashbookEntry);
		cashbookEntry.setReceipt(this);
		return this;
	}

	public Receipt removeEntries(CashbookEntry cashbookEntry) {
		entries.remove(cashbookEntry);
		cashbookEntry.setReceipt(null);
		return this;
	}

	public void setEntries(Set<CashbookEntry> cashbookEntries) {
		this.entries = cashbookEntries;
	}

	public Company getCompany() {
		return company;
	}

	public Receipt company(Company company) {
		this.company = company;
		return this;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

  public Boolean isOverwriteIdentifier() {
    return overwriteIdentifier;
  }

  public void setOverwriteIdentifier(Boolean overwriteIdentifier) {
    this.overwriteIdentifier = overwriteIdentifier;
  }

  public Receipt overwriteIdentifier(Boolean overwriteIdentifier) {
	  this.overwriteIdentifier = overwriteIdentifier;
	  return this;
  }

  public CashbookEntry getInitialCashbookEntry() {
    return initialCashbookEntry;
  }

  public void setInitialCashbookEntry(CashbookEntry initialCashbookEntry) {
    this.initialCashbookEntry = initialCashbookEntry;
  }

  public Receipt initialCashbookEntry(CashbookEntry initialCashbookEntry) {
	  this.initialCashbookEntry = initialCashbookEntry;
	  return this;
  }

  @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Receipt receipt = (Receipt) obj;
		if (receipt.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, receipt.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Receipt{" + "id=" + id + ", date='" + date + "'" + ", note='" + note + "'" + ", title='" + title + "'"
				+ ", file='" + file + "'" + ", identifier='" + identifier + "'" + '}';
	}
}
