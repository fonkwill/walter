package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A UserColumnConfig.
 */
@Entity
@Table(name = "user_column_config")
public class UserColumnConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "visible", nullable = false)
    private Boolean visible;

    @NotNull
    @Min(value = 1)
    @Column(name = "position", nullable = false)
    private Integer position;

    @ManyToOne(optional = false)
    @NotNull
    private ColumnConfig columnConfig;

    @ManyToOne(optional = false)
    @NotNull
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isVisible() {
        return visible;
    }

    public UserColumnConfig visible(Boolean visible) {
        this.visible = visible;
        return this;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getPosition() {
        return position;
    }

    public UserColumnConfig position(Integer position) {
        this.position = position;
        return this;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public ColumnConfig getColumnConfig() {
        return columnConfig;
    }

    public UserColumnConfig columnConfig(ColumnConfig columnConfig) {
        this.columnConfig = columnConfig;
        return this;
    }

    public void setColumnConfig(ColumnConfig columnConfig) {
        this.columnConfig = columnConfig;
    }

    public User getUser() {
        return user;
    }

    public UserColumnConfig user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserColumnConfig userColumnConfig = (UserColumnConfig) o;
        if (userColumnConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userColumnConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserColumnConfig{" +
            "id=" + getId() +
            ", visible='" + isVisible() + "'" +
            ", position=" + getPosition() +
            ", user=" + getUser() +
            ", columnConfig=" + getColumnConfig() +
            "}";
    }
}
