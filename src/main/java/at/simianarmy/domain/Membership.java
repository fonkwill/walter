package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Membership.
 */
@Entity
@Table(name = "membership")
public class Membership implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "begin_date", nullable = false)
	private LocalDate beginDate;

	@Column(name = "end_date")
	private LocalDate endDate;

	@OneToMany(mappedBy = "membership", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private Set<MembershipFeeForPeriod> membershipFeeForPeriods = new HashSet<>();

	@ManyToOne
	private Person person;

	@ManyToOne
	private MembershipFee membershipFee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public Membership beginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public Membership endDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Set<MembershipFeeForPeriod> getMembershipFeeForPeriods() {
		return membershipFeeForPeriods;
	}

	public Membership membershipFeeForPeriods(Set<MembershipFeeForPeriod> membershipFeeForPeriods) {
		this.membershipFeeForPeriods = membershipFeeForPeriods;
		return this;
	}

	public Membership addMembershipFeeForPeriods(MembershipFeeForPeriod membershipFeeForPeriod) {
		membershipFeeForPeriods.add(membershipFeeForPeriod);
		membershipFeeForPeriod.setMembership(this);
		return this;
	}

	public Membership removeMembershipFeeForPeriods(MembershipFeeForPeriod membershipFeeForPeriod) {
		membershipFeeForPeriods.remove(membershipFeeForPeriod);
		membershipFeeForPeriod.setMembership(null);
		return this;
	}

	public void setMembershipFeeForPeriods(Set<MembershipFeeForPeriod> membershipFeeForPeriods) {
		this.membershipFeeForPeriods = membershipFeeForPeriods;
	}

	public Person getPerson() {
		return person;
	}

	public Membership person(Person person) {
		this.person = person;
		return this;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public MembershipFee getMembershipFee() {
		return membershipFee;
	}

	public Membership membershipFee(MembershipFee membershipFee) {
		this.membershipFee = membershipFee;
		return this;
	}

	public void setMembershipFee(MembershipFee membershipFee) {
		this.membershipFee = membershipFee;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Membership membership = (Membership) o;
		if (membership.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, membership.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Membership{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'" + '}';
	}
}
