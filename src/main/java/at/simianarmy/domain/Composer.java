package at.simianarmy.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Composer.
 */
@Entity
@Table(name = "composer")
public class Composer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@ManyToMany
	@JoinTable(name = "composer_compositions", joinColumns = @JoinColumn(name = "composers_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "compositions_id", referencedColumnName = "ID"))
	private Set<Composition> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Composer name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Composition> getCompositions() {
		return compositions;
	}

	public Composer compositions(Set<Composition> compositions) {
		this.compositions = compositions;
		return this;
	}

	public Composer addCompositions(Composition composition) {
		compositions.add(composition);
		composition.getComposers().add(this);
		return this;
	}

	public Composer removeCompositions(Composition composition) {
		compositions.remove(composition);
		composition.getComposers().remove(this);
		return this;
	}

	public void setCompositions(Set<Composition> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Composer composer = (Composer) obj;
		if (composer.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, composer.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Composer{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
