package at.simianarmy.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A MembershipFeeAmount.
 */
@Entity
@Table(name = "membership_fee_amount")
public class MembershipFeeAmount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "begin_date", nullable = false)
	private LocalDate beginDate;

	@Column(name = "end_date")
	private LocalDate endDate;

	@NotNull
	@Column(name = "amount", precision = 10, scale = 2, nullable = false)
	private BigDecimal amount;

	@ManyToOne
	private MembershipFee membershipFee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public MembershipFeeAmount beginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public MembershipFeeAmount endDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public MembershipFeeAmount amount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public MembershipFee getMembershipFee() {
		return membershipFee;
	}

	public MembershipFeeAmount membershipFee(MembershipFee membershipFee) {
		this.membershipFee = membershipFee;
		return this;
	}

	public void setMembershipFee(MembershipFee membershipFee) {
		this.membershipFee = membershipFee;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		MembershipFeeAmount membershipFeeAmount = (MembershipFeeAmount) obj;
		if (membershipFeeAmount.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, membershipFeeAmount.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MembershipFeeAmount{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'"
				+ ", amount='" + amount + "'" + '}';
	}
}
