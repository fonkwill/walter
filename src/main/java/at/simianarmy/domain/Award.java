package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Award.
 */
@Entity
@Table(name = "award")
public class Award implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "descripton")
	private String descripton;

	@OneToMany(mappedBy = "award")
	@JsonIgnore
	private Set<PersonAward> personAwards = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Award name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripton() {
		return descripton;
	}

	public Award descripton(String descripton) {
		this.descripton = descripton;
		return this;
	}

	public void setDescripton(String descripton) {
		this.descripton = descripton;
	}

	public Set<PersonAward> getPersonAwards() {
		return personAwards;
	}

	public Award personAwards(Set<PersonAward> personAwards) {
		this.personAwards = personAwards;
		return this;
	}

	public Award addPersonAwards(PersonAward personAward) {
		personAwards.add(personAward);
		personAward.setAward(this);
		return this;
	}

	public Award removePersonAwards(PersonAward personAward) {
		personAwards.remove(personAward);
		personAward.setAward(null);
		return this;
	}

	public void setPersonAwards(Set<PersonAward> personAwards) {
		this.personAwards = personAwards;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Award award = (Award) obj;
		if (award.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, award.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Award{" + "id=" + id + ", name='" + name + "'" + ", descripton='" + descripton + "'" + '}';
	}
}
