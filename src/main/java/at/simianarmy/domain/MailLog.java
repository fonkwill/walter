package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.MailLogStatus;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * MailLogs.
 */
@Entity
@Table(name = "mail_log")
public class MailLog {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "date", nullable = false)
  private ZonedDateTime date;

  @NotNull
  @Column(name = "email", nullable = false)
  private String email;

  @NotNull
  @Column(name = "subject", nullable = false)
  private String subject;

  @Enumerated(EnumType.STRING)
  @NotNull
  @Column(name = "mail_log_status", nullable = false)
  private MailLogStatus mailLogStatus = MailLogStatus.WAITING;

  @Column(name = "error_message")
  private String errorMessage;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ZonedDateTime getDate() {
    return date;
  }

  public void setDate(ZonedDateTime date) {
    this.date = date;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public MailLogStatus getMailLogStatus() {
    return mailLogStatus;
  }

  public void setMailLogStatus(MailLogStatus mailLogStatus) {
    this.mailLogStatus = mailLogStatus;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MailLog mailLog = (MailLog) o;
    if (mailLog.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), mailLog.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "MailLog{" +
      "id=" + id +
      ", date=" + date +
      ", email='" + email + '\'' +
      ", subject='" + subject + '\'' +
      ", mailLogStatus=" + mailLogStatus +
      ", errorMessage='" + errorMessage + '\'' +
      '}';
  }
}
