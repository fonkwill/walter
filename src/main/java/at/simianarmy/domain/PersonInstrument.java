package at.simianarmy.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A PersonInstrument.
 */
@Entity
@Table(name = "person_instrument")
public class PersonInstrument implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "begin_date", nullable = false)
	private LocalDate beginDate;

	@Column(name = "end_date")
	private LocalDate endDate;

	@ManyToOne
	private Instrument instrument;

	@ManyToOne
	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getBeginDate() {
		return beginDate;
	}

	public PersonInstrument beginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public PersonInstrument endDate(LocalDate endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public PersonInstrument instrument(Instrument instrument) {
		this.instrument = instrument;
		return this;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public Person getPerson() {
		return person;
	}

	public PersonInstrument person(Person person) {
		this.person = person;
		return this;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		PersonInstrument personInstrument = (PersonInstrument) obj;
		if (personInstrument.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, personInstrument.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "PersonInstrument{" + "id=" + id + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate + "'"
				+ '}';
	}
}
