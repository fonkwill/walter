package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A InstrumentType.
 */
@Entity
@Table(name = "instrument_type")
public class InstrumentType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "type")
	@JsonIgnore
	private Set<Instrument> instruments = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public InstrumentType name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public InstrumentType instruments(Set<Instrument> instruments) {
		this.instruments = instruments;
		return this;
	}

	public InstrumentType addInstruments(Instrument instrument) {
		instruments.add(instrument);
		instrument.setType(this);
		return this;
	}

	public InstrumentType removeInstruments(Instrument instrument) {
		instruments.remove(instrument);
		instrument.setType(null);
		return this;
	}

	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		InstrumentType instrumentType = (InstrumentType) obj;
		if (instrumentType.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, instrumentType.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "InstrumentType{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
