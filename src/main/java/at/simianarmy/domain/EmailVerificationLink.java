package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A EmailVerificationLink.
 */
@Entity
@Table(name = "email_verification_link")
public class EmailVerificationLink implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "secret_key", nullable = false)
    private String secretKey;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private ZonedDateTime creationDate;

    @Column(name = "validation_date")
    private ZonedDateTime validationDate;

    @NotNull
    @Column(name = "invalid", nullable = false)
    private Boolean invalid;

    @ManyToOne(optional = false)
    @NotNull
    private Person person;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public EmailVerificationLink secretKey(String secretKey) {
        this.secretKey = secretKey;
        return this;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public EmailVerificationLink creationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getValidationDate() {
        return validationDate;
    }

    public EmailVerificationLink validationDate(ZonedDateTime validationDate) {
        this.validationDate = validationDate;
        return this;
    }

    public void setValidationDate(ZonedDateTime validationDate) {
        this.validationDate = validationDate;
    }

    public Boolean isInvalid() {
        return invalid;
    }

    public EmailVerificationLink invalid(Boolean invalid) {
        this.invalid = invalid;
        return this;
    }

    public void setInvalid(Boolean invalid) {
        this.invalid = invalid;
    }

    public Person getPerson() {
        return person;
    }

    public EmailVerificationLink person(Person person) {
        this.person = person;
        this.person.addEmailVerificationLink(this);
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
        this.person.addEmailVerificationLink(this);
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EmailVerificationLink emailVerificationLink = (EmailVerificationLink) o;
        if (emailVerificationLink.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), emailVerificationLink.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmailVerificationLink{" +
            "id=" + getId() +
            ", secretKey='" + getSecretKey() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", validationDate='" + getValidationDate() + "'" +
            ", invalid='" + isInvalid() + "'" +
            "}";
    }
}
