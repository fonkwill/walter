package at.simianarmy.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ColumnConfig.
 */
@Entity
@Table(name = "column_config")
public class ColumnConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "entity", nullable = false)
    private String entity;

    @NotNull
    @Column(name = "column_name", nullable = false)
    private String columnName;

    @NotNull
    @Column(name = "default_visible", nullable = false)
    private Boolean defaultVisible;

    @NotNull
    @Min(value = 1)
    @Column(name = "default_position", nullable = false)
    private Integer defaultPosition;

    @NotNull
    @Column(name = "column_display_name", nullable = false)
    private String columnDisplayName;

    @NotNull
    @Column(name = "sort_by_name", nullable = false)
    private String sortByName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntity() {
        return entity;
    }

    public ColumnConfig entity(String entity) {
        this.entity = entity;
        return this;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getColumnName() {
        return columnName;
    }

    public ColumnConfig columnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Boolean isDefaultVisible() {
        return defaultVisible;
    }

    public ColumnConfig defaultVisible(Boolean defaultVisible) {
        this.defaultVisible = defaultVisible;
        return this;
    }

    public void setDefaultVisible(Boolean defaultVisible) {
        this.defaultVisible = defaultVisible;
    }

    public Integer getDefaultPosition() {
        return defaultPosition;
    }

    public ColumnConfig defaultPosition(Integer defaultPosition) {
        this.defaultPosition = defaultPosition;
        return this;
    }

    public void setDefaultPosition(Integer defaultPosition) {
        this.defaultPosition = defaultPosition;
    }

    public String getColumnDisplayName() {
        return columnDisplayName;
    }

    public ColumnConfig columnDisplayName(String columnDisplayName) {
        this.columnDisplayName = columnDisplayName;
        return this;
    }

    public void setColumnDisplayName(String columnDisplayName) {
        this.columnDisplayName = columnDisplayName;
    }

    public String getSortByName() {
        return sortByName;
    }

    public ColumnConfig sortByName(String sortByName) {
        this.sortByName = sortByName;
        return this;
    }

    public void setSortByName(String sortByName) {
        this.sortByName = sortByName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ColumnConfig columnConfig = (ColumnConfig) o;
        if (columnConfig.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), columnConfig.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ColumnConfig{" +
            "id=" + getId() +
            ", entity='" + getEntity() + "'" +
            ", columnName='" + getColumnName() + "'" +
            ", defaultVisible='" + isDefaultVisible() + "'" +
            ", defaultPosition=" + getDefaultPosition() +
            ", columnDisplayName='" + getColumnDisplayName() + "'" +
            ", sortByName='" + getSortByName() + "'" +
            "}";
    }
}
