package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.BankImporterType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A BankImportData.
 */
@Entity
@Table(name = "bank_import_data")
public class BankImportData implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "entry_date", nullable = false)
	private LocalDate entryDate;

	@NotNull
	@Column(name = "entry_value", precision = 10, scale = 2, nullable = false)
	private BigDecimal entryValue;

	@Column(name = "entry_text")
	private String entryText;

	@Column(name = "partner_name")
	private String partnerName;

	@NotNull
	@Column(name = "entry_reference", nullable = false)
	private byte[] entryReference;

	@NotNull
  @Column(name = "is_transfer", nullable = false)
  private Boolean isTransfer = false;

	@ManyToOne
  @JoinColumn(name = "cashbook_account_id_from")
  private CashbookAccount cashbookAccountFrom;

	@ManyToOne
  @JoinColumn(name = "cashbook_account_id_to")
  private CashbookAccount cashbookAccountTo;

	@NotNull
	@ManyToOne
	private CashbookAccount cashbookAccount;

  @ManyToOne
  private CashbookCategory cashbookCategory;

	@OneToOne
	@JoinColumn(unique = true)
	private CashbookEntry cashbookEntry;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getEntryDate() {
		return entryDate;
	}

	public BankImportData entryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
		return this;
	}

	public void setEntryDate(LocalDate entryDate) {
		this.entryDate = entryDate;
	}

	public BigDecimal getEntryValue() {
		return entryValue;
	}

	public BankImportData entryValue(BigDecimal entryValue) {
		this.entryValue = entryValue;
		return this;
	}

	public void setEntryValue(BigDecimal entryValue) {
		this.entryValue = entryValue;
	}

	public String getEntryText() {
		return entryText;
	}

	public BankImportData entryText(String entryText) {
		this.entryText = entryText;
		return this;
	}

	public void setEntryText(String entryText) {
		this.entryText = entryText;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public BankImportData partnerName(String partnerName) {
		this.partnerName = partnerName;
		return this;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public byte[] getEntryReference() {
		return entryReference;
	}

	public BankImportData entryReference(byte[] entryReference) {
		this.entryReference = entryReference;
		return this;
	}

	public void setEntryReference(byte[] entryReference) {
		this.entryReference = entryReference;
	}

	public BankImportData cashbookAccount(CashbookAccount cashbookAccount) {
	  this.cashbookAccount = cashbookAccount;
	  return this;
  }

  public CashbookAccount getCashbookAccount() {
    return cashbookAccount;
  }

  public void setCashbookAccount(CashbookAccount cashbookAccount) {
    this.cashbookAccount = cashbookAccount;
  }

  public CashbookEntry getCashbookEntry() {
		return cashbookEntry;
	}

	public BankImportData cashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
		return this;
	}

	public void setCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
	}

  public CashbookCategory getCashbookCategory() {
    return cashbookCategory;
  }

  public void setCashbookCategory(CashbookCategory cashbookCategory) {
    this.cashbookCategory = cashbookCategory;
  }

  public BankImportData cashbookCategory(CashbookCategory cashbookCategory) {
	  this.cashbookCategory = cashbookCategory;
	  return this;
  }

  public Boolean getTransfer() {
    return isTransfer;
  }

  public void setTransfer(Boolean transfer) {
    isTransfer = transfer;
  }

  public CashbookAccount getCashbookAccountFrom() {
    return cashbookAccountFrom;
  }

  public void setCashbookAccountFrom(CashbookAccount cashbookAccountFrom) {
    this.cashbookAccountFrom = cashbookAccountFrom;
  }

  public CashbookAccount getCashbookAccountTo() {
    return cashbookAccountTo;
  }

  public void setCashbookAccountTo(CashbookAccount cashbookAccountTo) {
    this.cashbookAccountTo = cashbookAccountTo;
  }

  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BankImportData bankImportData = (BankImportData) o;
		if (bankImportData.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, bankImportData.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "BankImportData{" + "id=" + id + ", entryDate='" + entryDate + "'" + ", entryValue='" + entryValue + "'"
				+ ", entryText='" + entryText + "'" + ", partnerName='" + partnerName + "'" + ", entryReference='"
				+ entryReference + "'" + ", cashbookAccount='" + cashbookAccount + "'" + ", isTransfer='" + isTransfer + "'" + '}';
	}
}
