package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * An OfficialAppointment.
 */
@Entity
@Table(name = "official_appointment")
public class OfficialAppointment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "organizer_name", nullable = false)
	private String organizerName;

	@NotNull
	@Column(name = "organizer_address", nullable = false)
	private String organizerAddress;

	@NotNull
	@Column(name = "is_head_quota", nullable = false)
	private Boolean isHeadQuota;

	@OneToOne(mappedBy = "officialAppointment")
	@JsonIgnore
	private Appointment appointment;

	@ManyToOne
	private Report report;

	@ManyToMany(mappedBy = "appointments")
	@JsonIgnore
	private Set<Composition> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrganizerName() {
		return organizerName;
	}

	public OfficialAppointment organizerName(String organizerName) {
		this.organizerName = organizerName;
		return this;
	}

	public void setOrganizerName(String organizerName) {
		this.organizerName = organizerName;
	}

	public String getOrganizerAddress() {
		return organizerAddress;
	}

	public OfficialAppointment organizerAddress(String organizerAddress) {
		this.organizerAddress = organizerAddress;
		return this;
	}

	public void setOrganizerAddress(String organizerAddress) {
		this.organizerAddress = organizerAddress;
	}

	public Boolean isIsHeadQuota() {
		return isHeadQuota;
	}

	public OfficialAppointment isHeadQuota(Boolean isHeadQuota) {
		this.isHeadQuota = isHeadQuota;
		return this;
	}

	public void setIsHeadQuota(Boolean isHeadQuota) {
		this.isHeadQuota = isHeadQuota;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public OfficialAppointment appointment(Appointment appointment) {
		this.appointment = appointment;
		return this;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public Report getReport() {
		return report;
	}

	public OfficialAppointment report(Report report) {
		this.report = report;
		return this;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public Set<Composition> getCompositions() {
		return compositions;
	}

	public OfficialAppointment compositions(Set<Composition> compositions) {
		this.compositions = compositions;
		return this;
	}

	public OfficialAppointment addCompositions(Composition composition) {
		compositions.add(composition);
		composition.getAppointments().add(this);
		return this;
	}

	public OfficialAppointment removeCompositions(Composition composition) {
		compositions.remove(composition);
		composition.getAppointments().remove(this);
		return this;
	}

	public void setCompositions(Set<Composition> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		OfficialAppointment officialAppointment = (OfficialAppointment) obj;
		if (officialAppointment.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, officialAppointment.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "OfficialAppointment{" + "id=" + id + ", organizerName='" + organizerName + "'" + ", organizerAddress='"
				+ organizerAddress + "'" + ", isHeadQuota='" + isHeadQuota + "'" + '}';
	}
}
