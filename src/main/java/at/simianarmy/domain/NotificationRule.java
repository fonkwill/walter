package at.simianarmy.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import at.simianarmy.roles.domain.Role;

/**
 * A NotificationRule.
 */
@Entity
@Table(name = "notification_rule")
public class NotificationRule implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@NotNull
	@Column(name = "entity", nullable = false)
	private String entity;

	@Lob
	@NotNull
	@Column(name = "conditions", nullable = false)
	private String conditions;

	@NotNull
	@Column(name = "active", nullable = false)
	private Boolean active;

	@NotNull
	@Column(name = "message_group", nullable = false)
	private String messageGroup;

	@NotNull
	@Column(name = "message_single", nullable = false)
	private String messageSingle;

	@ManyToMany
	@JoinTable(name = "notification_rule_target_audiences", joinColumns = @JoinColumn(name = "notification_rules_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "target_role_id", referencedColumnName = "ID"))
	private Set<Role> targetAudiences = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public NotificationRule title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEntity() {
		return entity;
	}

	public NotificationRule entity(String entity) {
		this.entity = entity;
		return this;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getConditions() {
		return conditions;
	}

	public NotificationRule conditions(String conditions) {
		this.conditions = conditions;
		return this;
	}

	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	public Boolean isActive() {
		return active;
	}

	public NotificationRule active(Boolean active) {
		this.active = active;
		return this;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getMessageGroup() {
		return messageGroup;
	}

	public NotificationRule messageGroup(String messageGroup) {
		this.messageGroup = messageGroup;
		return this;
	}

	public void setMessageGroup(String messageGroup) {
		this.messageGroup = messageGroup;
	}

	public String getMessageSingle() {
		return messageSingle;
	}

	public NotificationRule messageSingle(String messageSingle) {
		this.messageSingle = messageSingle;
		return this;
	}

	public void setMessageSingle(String messageSingle) {
		this.messageSingle = messageSingle;
	}

	public Set<Role> getTargetAudiences() {
		return targetAudiences;
	}

	public NotificationRule targetAudiences(Set<Role> authorities) {
		this.targetAudiences = authorities;
		return this;
	}

	public NotificationRule addTargetAudiences(Role authority) {
		targetAudiences.add(authority);
		return this;
	}

	public NotificationRule removeTargetAudiences(Role authority) {
		targetAudiences.remove(authority);
		return this;
	}

	public void setTargetAudiences(Set<Role> authorities) {
		this.targetAudiences = authorities;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		NotificationRule notificationRule = (NotificationRule) obj;
		if (notificationRule.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, notificationRule.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "NotificationRule{" + "id=" + id + ", title='" + title + "'" + ", entity='" + entity + "'" + ", active='"
				+ active + "'" + '}';
	}
}
