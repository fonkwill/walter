package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A AppointmentType.
 */
@Entity
@Table(name = "appointment_type")
public class AppointmentType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@NotNull
	@Column(name = "is_official", nullable = false)
	private Boolean isOfficial;

	@OneToMany(mappedBy = "type")
	@JsonIgnore
	private Set<Appointment> appointments = new HashSet<>();

	@ManyToOne
	private PersonGroup personGroup;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public AppointmentType name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isIsOfficial() {
		return isOfficial;
	}

	public AppointmentType isOfficial(Boolean isOfficial) {
		this.isOfficial = isOfficial;
		return this;
	}

	public void setIsOfficial(Boolean isOfficial) {
		this.isOfficial = isOfficial;
	}

	public Set<Appointment> getAppointments() {
		return appointments;
	}

	public AppointmentType appointments(Set<Appointment> appointments) {
		this.appointments = appointments;
		return this;
	}

	public AppointmentType addAppointments(Appointment appointment) {
		appointments.add(appointment);
		appointment.setType(this);
		return this;
	}

	public AppointmentType removeAppointments(Appointment appointment) {
		appointments.remove(appointment);
		appointment.setType(null);
		return this;
	}

	public void setAppointments(Set<Appointment> appointments) {
		this.appointments = appointments;
	}

	public PersonGroup getPersonGroup() {
		return personGroup;
	}

	public AppointmentType personGroup(PersonGroup personGroup) {
		this.personGroup = personGroup;
		return this;
	}

	public void setPersonGroup(PersonGroup personGroup) {
		this.personGroup = personGroup;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		AppointmentType appointmentType = (AppointmentType) obj;
		if (appointmentType.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, appointmentType.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "AppointmentType{" + "id=" + id + ", name='" + name + "'" + ", isOfficial='" + isOfficial + "'" + '}';
	}
}
