package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.LetterType;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.validation.constraints.NotNull;

/**
 * A Letter.
 */
@Entity
@Table(name = "letter")
public class Letter implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

  @Enumerated(EnumType.STRING)
	@Column(name = "letter_type")
  @NotNull
	private LetterType letterType;

	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "text")
	private String text;

	@Column(name = "payment")
	private Boolean payment;

	@Column(name = "current_period")
	private Boolean currentPeriod;

	@Column(name = "previous_period")
	private Boolean previousPeriod;

	@ManyToOne
	private Template template;

	@ManyToMany
	@JoinTable(name = "letter_person_group", joinColumns = @JoinColumn(name = "letters_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "person_groups_id", referencedColumnName = "ID"))
	private Set<PersonGroup> personGroups = new HashSet<>();

	@Column(name = "subject")
  private String subject;

	@Column(name = "mail_text")
  private String mailText;

	@Column(name = "attachment_file_name")
	private String attachmentFileName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public Letter title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LetterType getLetterType() { return this.letterType; }

	public Letter letterType(LetterType letterType) {
	  this.letterType = letterType;
	  return this;
  }

  public void setLetterType(LetterType letterType) {
	  this.letterType = letterType;
  }

	public String getText() {
		return text;
	}

	public Letter text(String text) {
		this.text = text;
		return this;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean isPayment() {
		return payment;
	}

	public Letter payment(Boolean payment) {
		this.payment = payment;
		return this;
	}

	public void setPayment(Boolean payment) {
		this.payment = payment;
	}

	public Boolean isCurrentPeriod() {
		return currentPeriod;
	}

	public Letter currentPeriod(Boolean currentPeriod) {
		this.currentPeriod = currentPeriod;
		return this;
	}

	public void setCurrentPeriod(Boolean currentPeriod) {
		this.currentPeriod = currentPeriod;
	}

	public Boolean isPreviousPeriod() {
		return previousPeriod;
	}

	public Letter previousPeriod(Boolean previousPeriod) {
		this.previousPeriod = previousPeriod;
		return this;
	}

	public void setPreviousPeriod(Boolean previousPeriod) {
		this.previousPeriod = previousPeriod;
	}

	public Template getTemplate() {
		return template;
	}

	public Letter template(Template template) {
		this.template = template;
		return this;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public Set<PersonGroup> getPersonGroups() {
		return personGroups;
	}

	public Letter personGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
		return this;
	}

	public Letter addPersonGroup(PersonGroup personGroup) {
		personGroups.add(personGroup);
		return this;
	}

	public Letter removePersonGroup(PersonGroup personGroup) {
		personGroups.remove(personGroup);
		return this;
	}

	public void setPersonGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
	}

  public Boolean getPayment() {
    return payment;
  }

  public Boolean getCurrentPeriod() {
    return currentPeriod;
  }

  public Boolean getPreviousPeriod() {
    return previousPeriod;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getMailText() {
    return mailText;
  }

  public void setMailText(String mailText) {
    this.mailText = mailText;
  }

  public String getAttachmentFileName() {
    return attachmentFileName;
  }

  public void setAttachmentFileName(String attachmentFileName) {
    this.attachmentFileName = attachmentFileName;
  }

  @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Letter letter = (Letter) o;
		if (letter.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, letter.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

  @Override
  public String toString() {
    return "Letter{" +
      "id=" + id +
      ", letterType=" + letterType +
      ", title='" + title + '\'' +
      ", text='" + text + '\'' +
      ", payment=" + payment +
      ", currentPeriod=" + currentPeriod +
      ", previousPeriod=" + previousPeriod +
      ", template=" + template +
      ", personGroups=" + personGroups +
      ", subject='" + subject + '\'' +
      ", mailText='" + mailText + '\'' +
      ", attachmentFileName='" + attachmentFileName + '\'' +
      '}';
  }
}
