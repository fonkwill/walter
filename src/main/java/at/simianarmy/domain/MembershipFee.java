package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import at.simianarmy.domain.enumeration.MembershipFeePeriod;

/**
 * A MembershipFee.
 */
@Entity
@Table(name = "membership_fee")
public class MembershipFee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "description", nullable = false)
	private String description;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "period", nullable = false)
	private MembershipFeePeriod period;

	@Min(value = 1)
	@Max(value = 12)
	@Column(name = "period_time_fraction")
	private Integer periodTimeFraction;

	@OneToMany(mappedBy = "membershipFee")
	@JsonIgnore
	private Set<MembershipFeeAmount> membershipFeeAmounts = new HashSet<>();

	@OneToMany(mappedBy = "membershipFee")
	@JsonIgnore
	private Set<Membership> memberships = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public MembershipFee description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MembershipFeePeriod getPeriod() {
		return period;
	}

	public MembershipFee period(MembershipFeePeriod period) {
		this.period = period;
		return this;
	}

	public void setPeriod(MembershipFeePeriod period) {
		this.period = period;
	}

	public Integer getPeriodTimeFraction() {
		return periodTimeFraction;
	}

	public MembershipFee periodTimeFraction(Integer periodTimeFraction) {
		this.periodTimeFraction = periodTimeFraction;
		return this;
	}

	public void setPeriodTimeFraction(Integer periodTimeFraction) {
		this.periodTimeFraction = periodTimeFraction;
	}

	public Set<MembershipFeeAmount> getMembershipFeeAmounts() {
		return membershipFeeAmounts;
	}

	public MembershipFee membershipFeeAmounts(Set<MembershipFeeAmount> membershipFeeAmounts) {
		this.membershipFeeAmounts = membershipFeeAmounts;
		return this;
	}

	public MembershipFee addMembershipFeeAmounts(MembershipFeeAmount membershipFeeAmount) {
		membershipFeeAmounts.add(membershipFeeAmount);
		membershipFeeAmount.setMembershipFee(this);
		return this;
	}

	public MembershipFee removeMembershipFeeAmounts(MembershipFeeAmount membershipFeeAmount) {
		membershipFeeAmounts.remove(membershipFeeAmount);
		membershipFeeAmount.setMembershipFee(null);
		return this;
	}

	public void setMembershipFeeAmounts(Set<MembershipFeeAmount> membershipFeeAmounts) {
		this.membershipFeeAmounts = membershipFeeAmounts;
	}

	public Set<Membership> getMemberships() {
		return memberships;
	}

	public MembershipFee memberships(Set<Membership> memberships) {
		this.memberships = memberships;
		return this;
	}

	public MembershipFee addMembership(Membership membership) {
		memberships.add(membership);
		membership.setMembershipFee(this);
		return this;
	}

	public MembershipFee removeMembership(Membership membership) {
		memberships.remove(membership);
		membership.setMembershipFee(null);
		return this;
	}

	public void setMemberships(Set<Membership> memberships) {
		this.memberships = memberships;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MembershipFee membershipFee = (MembershipFee) o;
		if (membershipFee.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, membershipFee.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "MembershipFee{" + "id=" + id + ", description='" + description + "'" + ", period='" + period + "'"
				+ ", periodTimeFraction='" + periodTimeFraction + "'" + '}';
	}
}
