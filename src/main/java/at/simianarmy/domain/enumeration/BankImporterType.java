package at.simianarmy.domain.enumeration;

/**
 * The BankImporterType enumeration.
 */
public enum BankImporterType {
	GEORGE_IMPORTER, BACA_IMPORTER, RAIKA_IMPORTER, EASYBANK_IMPORTER
}
