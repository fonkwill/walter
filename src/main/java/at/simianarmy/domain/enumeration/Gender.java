package at.simianarmy.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
	MALE, FEMALE
}
