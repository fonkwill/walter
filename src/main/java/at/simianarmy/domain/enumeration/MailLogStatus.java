package at.simianarmy.domain.enumeration;

public enum MailLogStatus {
  WAITING, SUCCESSFUL, UNSUCCESSFUL
}
