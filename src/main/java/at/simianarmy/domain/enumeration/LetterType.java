package at.simianarmy.domain.enumeration;

public enum LetterType {
  SERIAL_LETTER, DEMAND_LETTER, EMAIL_VERIFICATION_LETTER
}
