package at.simianarmy.domain.enumeration;

public enum EmailType {
  MANUALLY, VERIFIED
}
