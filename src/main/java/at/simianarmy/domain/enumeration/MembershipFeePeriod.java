package at.simianarmy.domain.enumeration;

/**
 * The MembershipFeePeriod enumeration.
 */
public enum MembershipFeePeriod {
	MONTHLY, ANNUALLY, BIANNUAL, QUARTERLY
}
