package at.simianarmy.domain.enumeration;

/**
 * The CashbookEntryType enumeration.
 */
public enum CashbookEntryType {
	INCOME, SPENDING
}
