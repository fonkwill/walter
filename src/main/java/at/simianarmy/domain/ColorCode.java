package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ColorCode.
 */
@Entity
public class ColorCode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false, unique = true)
	private String name;

	@OneToMany(mappedBy = "colorCode")
	@JsonIgnore
	private Set<Composition> compositions = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public ColorCode name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Composition> getCompositions() {
		return compositions;
	}

	public ColorCode compositions(Set<Composition> compositions) {
		this.compositions = compositions;
		return this;
	}

	public ColorCode addCompositions(Composition composition) {
		compositions.add(composition);
		composition.setColorCode(this);
		return this;
	}

	public ColorCode removeCompositions(Composition composition) {
		compositions.remove(composition);
		composition.setColorCode(null);
		return this;
	}

	public void setCompositions(Set<Composition> compositions) {
		this.compositions = compositions;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ColorCode colorCode = (ColorCode) o;
		if (colorCode.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, colorCode.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "ColorCode{" + "id=" + id + ", name='" + name + "'" + '}';
	}
}
