package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Instrument.
 */
@Entity
@Table(name = "instrument")
public class Instrument implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@NotNull
	@Column(name = "purchase_date")
	private LocalDate purchaseDate;

	@Column(name = "private_instrument")
	private Boolean privateInstrument;

	@DecimalMin(value = "0")
	@Column(name = "price", precision = 10, scale = 2)
	private BigDecimal price;

	@OneToMany(mappedBy = "instrument")
	@JsonIgnore
	private Set<InstrumentService> instrumentServices = new HashSet<>();

	@OneToMany(mappedBy = "instrument")
	@JsonIgnore
	private Set<PersonInstrument> personInstruments = new HashSet<>();

	@ManyToOne
	@NotNull
	private InstrumentType type;

	@OneToOne
	@JoinColumn(unique = true)
	private CashbookEntry cashbookEntry;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Instrument name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public Instrument purchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
		return this;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Boolean isPrivateInstrument() {
		return privateInstrument;
	}

	public Instrument privateInstrument(Boolean privateInstrument) {
		this.privateInstrument = privateInstrument;
		return this;
	}

	public void setPrivateInstrument(Boolean privateInstrument) {
		this.privateInstrument = privateInstrument;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Instrument price(BigDecimal price) {
		this.price = price;
		return this;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Set<InstrumentService> getInstrumentServices() {
		return instrumentServices;
	}

	public Instrument instrumentServices(Set<InstrumentService> instrumentServices) {
		this.instrumentServices = instrumentServices;
		return this;
	}

	public Instrument addInstrumentServices(InstrumentService instrumentService) {
		instrumentServices.add(instrumentService);
		instrumentService.setInstrument(this);
		return this;
	}

	public Instrument removeInstrumentServices(InstrumentService instrumentService) {
		instrumentServices.remove(instrumentService);
		instrumentService.setInstrument(null);
		return this;
	}

	public void setInstrumentServices(Set<InstrumentService> instrumentServices) {
		this.instrumentServices = instrumentServices;
	}

	public Set<PersonInstrument> getPersonInstruments() {
		return personInstruments;
	}

	public Instrument personInstruments(Set<PersonInstrument> personInstruments) {
		this.personInstruments = personInstruments;
		return this;
	}

	public Instrument addPersonInstruments(PersonInstrument personInstrument) {
		personInstruments.add(personInstrument);
		personInstrument.setInstrument(this);
		return this;
	}

	public Instrument removePersonInstruments(PersonInstrument personInstrument) {
		personInstruments.remove(personInstrument);
		personInstrument.setInstrument(null);
		return this;
	}

	public void setPersonInstruments(Set<PersonInstrument> personInstruments) {
		this.personInstruments = personInstruments;
	}

	public InstrumentType getType() {
		return type;
	}

	public Instrument type(InstrumentType instrumentType) {
		this.type = instrumentType;
		return this;
	}

	public void setType(InstrumentType instrumentType) {
		this.type = instrumentType;
	}

	public CashbookEntry getCashbookEntry() {
		return cashbookEntry;
	}

	public Instrument cashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
		return this;
	}

	public void setCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Instrument instrument = (Instrument) o;
		if (instrument.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, instrument.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Instrument{" + "id=" + id + ", name='" + name + "'" + ", purchaseDate='" + purchaseDate + "'"
				+ ", privateInstrument='" + privateInstrument + "'" + ", price='" + price + "'" + '}';
	}
}
