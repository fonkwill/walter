package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import at.simianarmy.domain.enumeration.CustomizationName;

/**
 * A Customization.
 */
@Entity
@Table(name = "customization")
public class Customization implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "name", nullable = false, unique = true)
	private CustomizationName name;

	@NotNull
	@Column(name = "data", nullable = false)
	private String data;

	@Column(name = "text")
	private String text;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CustomizationName getName() {
		return name;
	}

	public Customization name(CustomizationName name) {
		this.name = name;
		return this;
	}

	public void setName(CustomizationName name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public Customization data(String data) {
		this.data = data;
		return this;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getText() {
		return text;
	}

	public Customization text(String text) {
		this.text = text;
		return this;
	}

	public void setText(String text) {
		this.text = text;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Customization customization = (Customization) o;
		if (customization.getName() == null || getName() == null) {
			return false;
		}
		return Objects.equals(getName(), customization.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getName());
	}

	@Override
	public String toString() {
		return "Customization{" + "id=" + getId() + ", name='" + getName() + "'" + ", data='" + getData() + "'"
				+ ", text='" + getText() + "'" + "}";
	}
}
