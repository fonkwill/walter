package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * A CashbookCategory.
 */
@Entity
@Table(name = "cashbook_category")
public class CashbookCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@NotNull
	@Pattern(regexp = "^#[A-Fa-f0-9]{6}$")
	@Column(name = "color", nullable = false)
	private String color;

	@OneToMany(mappedBy = "parent")
	@JsonIgnore
	private Set<CashbookCategory> children = new HashSet<>();

	@OneToMany(mappedBy = "cashbookCategory")
	@JsonIgnore
	private Set<CashbookEntry> cashbookEntries = new HashSet<>();

	@ManyToOne
	private CashbookCategory parent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public CashbookCategory name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public CashbookCategory description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public CashbookCategory color(String color) {
		this.color = color;
		return this;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Set<CashbookCategory> getChildren() {
		return children;
	}

	public CashbookCategory children(Set<CashbookCategory> cashbookCategories) {
		this.children = cashbookCategories;
		return this;
	}

	public CashbookCategory addChildren(CashbookCategory cashbookCategory) {
		children.add(cashbookCategory);
		cashbookCategory.setParent(this);
		return this;
	}

	public CashbookCategory removeChildren(CashbookCategory cashbookCategory) {
		children.remove(cashbookCategory);
		cashbookCategory.setParent(null);
		return this;
	}

	public void setChildren(Set<CashbookCategory> cashbookCategories) {
		this.children = cashbookCategories;
	}

	public Set<CashbookEntry> getCashbookEntries() {
		return cashbookEntries;
	}

	public CashbookCategory cashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
		return this;
	}

	public CashbookCategory addCashbookEntries(CashbookEntry cashbookEntry) {
		cashbookEntries.add(cashbookEntry);
		cashbookEntry.setCashbookCategory(this);
		return this;
	}

	public CashbookCategory removeCashbookEntries(CashbookEntry cashbookEntry) {
		cashbookEntries.remove(cashbookEntry);
		cashbookEntry.setCashbookCategory(null);
		return this;
	}

	public void setCashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
	}

	public CashbookCategory getParent() {
		return parent;
	}

	public CashbookCategory parent(CashbookCategory cashbookCategory) {
		this.parent = cashbookCategory;
		return this;
	}

	public void setParent(CashbookCategory cashbookCategory) {
		this.parent = cashbookCategory;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		CashbookCategory cashbookCategory = (CashbookCategory) obj;
		if (cashbookCategory.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, cashbookCategory.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "CashbookCategory{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'"
				+ ", color='" + color + "'" + '}';
	}
}
