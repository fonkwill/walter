package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A PersonGroup.
 */
@Entity
@Table(name = "person_group")
public class PersonGroup implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;


  @NotNull
  @Column(name = "has_person_relevance", nullable = false )
  private Boolean hasPersonRelevance = false;

	@OneToMany(mappedBy = "parent")
	@JsonIgnore
	private Set<PersonGroup> children = new HashSet<>();

	@ManyToOne
	private PersonGroup parent;

	@ManyToMany(mappedBy = "personGroups")
	@JsonIgnore
	private Set<Person> persons = new HashSet<>();

	@ManyToMany(mappedBy = "personGroups")
	@JsonIgnore
	private Set<Company> companies = new HashSet<>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public PersonGroup name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<PersonGroup> getChildren() {
		return children;
	}

	public PersonGroup children(Set<PersonGroup> personGroups) {
		this.children = personGroups;
		return this;
	}

	public PersonGroup addChildren(PersonGroup personGroup) {
		this.children.add(personGroup);
		personGroup.setParent(this);
		return this;
	}

	public PersonGroup removeChildren(PersonGroup personGroup) {
		this.children.remove(personGroup);
		personGroup.setParent(null);
		return this;
	}

	public void setChildren(Set<PersonGroup> personGroups) {
		this.children = personGroups;
	}

	public PersonGroup getParent() {
		return parent;
	}

	public PersonGroup parent(PersonGroup personGroup) {
		this.parent = personGroup;
		return this;
	}

	public void setParent(PersonGroup personGroup) {
		this.parent = personGroup;
	}

	public Set<Person> getPersons() {
		return persons;
	}

	public PersonGroup persons(Set<Person> people) {
		this.persons = people;
		return this;
	}

	public PersonGroup addPersons(Person person) {
		this.persons.add(person);
		person.getPersonGroups().add(this);
		return this;
	}

	public PersonGroup removePersons(Person person) {
		this.persons.remove(person);
		person.getPersonGroups().remove(this);
		return this;
	}

	public void setPersons(Set<Person> people) {
		this.persons = people;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public PersonGroup companies(Set<Company> companies) {
		this.companies = companies;
		return this;
	}

	public PersonGroup addCompanies(Company company) {
		this.companies.add(company);
		company.getPersonGroups().add(this);
		return this;
	}

	public PersonGroup removeCompanies(Company company) {
		this.companies.remove(company);
		company.getPersonGroups().remove(this);
		return this;
	}

  public Boolean getHasPersonRelevance() {
    return hasPersonRelevance;
  }

  public void setHasPersonRelevance(Boolean personRelevance) {
    this.hasPersonRelevance = personRelevance;
  }


  public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PersonGroup personGroup = (PersonGroup) o;
		if (personGroup.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), personGroup.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "PersonGroup{" + "id=" + getId() + ", name='" + getName() + "'" + "}";
	}
}
