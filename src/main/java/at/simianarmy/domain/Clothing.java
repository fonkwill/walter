package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Clothing.
 */
@Entity
@Table(name = "clothing")
public class Clothing implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "size")
	private String size;

	@Column(name = "purchase_date")
	private LocalDate purchaseDate;

	@Column(name = "not_available")
	private Boolean notAvailable = false;

	@OneToMany(mappedBy = "clothing")
	@JsonIgnore
	private Set<PersonClothing> personClothings = new HashSet<>();

	@ManyToOne
	private ClothingType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public Clothing size(String size) {
		this.size = size;
		return this;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public Clothing purchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
		return this;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Boolean isNotAvailable() {
		return notAvailable;
	}

	public Clothing notAvailable(Boolean notAvailable) {
		this.notAvailable = notAvailable;
		return this;
	}

	public void setNotAvailable(Boolean notAvailable) {
		this.notAvailable = notAvailable;
	}

	public Set<PersonClothing> getPersonClothings() {
		return personClothings;
	}

	public Clothing personClothings(Set<PersonClothing> personClothings) {
		this.personClothings = personClothings;
		return this;
	}

	public Clothing addPersonClothings(PersonClothing personClothing) {
		personClothings.add(personClothing);
		personClothing.setClothing(this);
		return this;
	}

	public Clothing removePersonClothings(PersonClothing personClothing) {
		personClothings.remove(personClothing);
		personClothing.setClothing(null);
		return this;
	}

	public void setPersonClothings(Set<PersonClothing> personClothings) {
		this.personClothings = personClothings;
	}

	public ClothingType getType() {
		return type;
	}

	public Clothing type(ClothingType clothingType) {
		this.type = clothingType;
		return this;
	}

	public void setType(ClothingType clothingType) {
		this.type = clothingType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Clothing clothing = (Clothing) o;
		if (clothing.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, clothing.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Clothing{" + "id=" + id + ", size='" + size + "'" + ", purchaseDate='" + purchaseDate + "'"
				+ ", notAvailable='" + notAvailable + "'" + '}';
	}
}
