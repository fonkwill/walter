package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Honor.
 */
@Entity
@Table(name = "honor")
public class Honor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@OneToMany(mappedBy = "honor")
	@JsonIgnore
	private Set<PersonHonor> personHonors = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Honor name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public Honor description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<PersonHonor> getPersonHonors() {
		return personHonors;
	}

	public Honor personHonors(Set<PersonHonor> personHonors) {
		this.personHonors = personHonors;
		return this;
	}

	public Honor addPersonHonors(PersonHonor personHonor) {
		personHonors.add(personHonor);
		personHonor.setHonor(this);
		return this;
	}

	public Honor removePersonHonors(PersonHonor personHonor) {
		personHonors.remove(personHonor);
		personHonor.setHonor(null);
		return this;
	}

	public void setPersonHonors(Set<PersonHonor> personHonors) {
		this.personHonors = personHonors;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Honor honor = (Honor) obj;
		if (honor.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, honor.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Honor{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'" + '}';
	}
}
