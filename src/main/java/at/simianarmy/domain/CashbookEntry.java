package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.CashbookEntryType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * A CashbookEntry.
 */
@Entity
@Table(name = "cashbook_entry")
public class CashbookEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "title", nullable = false)
	private String title;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	private CashbookEntryType type;

	@NotNull
	@Column(name = "date", nullable = false)
	private LocalDate date;

	@NotNull
	@DecimalMin(value = "0")
	@Column(name = "amount", precision = 10, scale = 2, nullable = false)
	private BigDecimal amount;

	@OneToMany(mappedBy = "cashbookEntry")
	@JsonIgnore
	private Set<Composition> compostions = new HashSet<>();

	@OneToOne(mappedBy = "cashbookEntry")
	@JsonIgnore
	private InstrumentService instrumentService;

  @OneToOne(mappedBy = "cashbookEntry", fetch = FetchType.LAZY)
  @JoinColumn(unique = true)
  @JsonIgnore
  private BankImportData bankImportData;

	@OneToOne(mappedBy = "cashbookEntry")
	@JsonIgnore
	private MembershipFeeForPeriod membershipFeeForPeriod;

	@ManyToOne
	private Appointment appointment;

	@ManyToOne
	private CashbookCategory cashbookCategory;

	@ManyToOne
	private Receipt receipt;

	@ManyToOne
  @NotNull
	private CashbookAccount cashbookAccount;

	@OneToOne(mappedBy = "cashbookEntry")
	@JsonIgnore
	private Instrument instrument;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public CashbookEntry title(String title) {
		this.title = title;
		return this;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public CashbookEntryType getType() {
		return type;
	}

	public CashbookEntry type(CashbookEntryType type) {
		this.type = type;
		return this;
	}

	public void setType(CashbookEntryType type) {
		this.type = type;
	}

	public LocalDate getDate() {
		return date;
	}

	public CashbookEntry date(LocalDate date) {
		this.date = date;
		return this;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public CashbookEntry amount(BigDecimal amount) {
		this.amount = amount;
		return this;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Set<Composition> getCompostions() {
		return compostions;
	}

	public CashbookEntry compostions(Set<Composition> compositions) {
		this.compostions = compositions;
		return this;
	}

	public CashbookEntry addCompostion(Composition composition) {
		compostions.add(composition);
		composition.setCashbookEntry(this);
		return this;
	}

	public CashbookEntry removeCompostion(Composition composition) {
		compostions.remove(composition);
		composition.setCashbookEntry(null);
		return this;
	}

	public void setCompostions(Set<Composition> compositions) {
		this.compostions = compositions;
	}

	public Instrument getInstrument() {
		return instrument;
	}

	public CashbookEntry instrument(Instrument instrument) {
		this.instrument = instrument;
		return this;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public InstrumentService getInstrumentService() {
		return instrumentService;
	}

	public CashbookEntry instrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
		return this;
	}

	public void setInstrumentService(InstrumentService instrumentService) {
		this.instrumentService = instrumentService;
	}

	public MembershipFeeForPeriod getMembershipFeeForPeriod() {
		return membershipFeeForPeriod;
	}

	public CashbookEntry membershipFeeForPeriod(MembershipFeeForPeriod membershipFeeForPeriod) {
		this.membershipFeeForPeriod = membershipFeeForPeriod;
		return this;
	}

	public void setMembershipFeeForPeriod(MembershipFeeForPeriod membershipFeeForPeriod) {
		this.membershipFeeForPeriod = membershipFeeForPeriod;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public CashbookEntry appointment(Appointment appointment) {
		this.appointment = appointment;
		return this;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public CashbookCategory getCashbookCategory() {
		return cashbookCategory;
	}

	public CashbookEntry cashbookCategory(CashbookCategory cashbookCategory) {
		this.cashbookCategory = cashbookCategory;
		return this;
	}

	public void setCashbookCategory(CashbookCategory cashbookCategory) {
		this.cashbookCategory = cashbookCategory;
	}

	public Receipt getReceipt() {
		return receipt;
	}

	public CashbookEntry receipt(Receipt receipt) {
		this.receipt = receipt;
		return this;
	}

	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}

	public CashbookAccount getCashbookAccount() {
		return cashbookAccount;
	}

	public void setCashbookAccount(CashbookAccount cashbookAccount) {
		this.cashbookAccount = cashbookAccount;
	}

	public CashbookEntry cashbookAccount(CashbookAccount cashbookAccount) {
	  this.cashbookAccount = cashbookAccount;
	  return this;
  }

  public BankImportData getBankImportData() {
    return bankImportData;
  }

  public void setBankImportData(BankImportData bankImportData) {
    this.bankImportData = bankImportData;
  }

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		CashbookEntry cashbookEntry = (CashbookEntry) obj;
		if (cashbookEntry.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, cashbookEntry.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "CashbookEntry{" + "id=" + id + ", title='" + title + "'" + ", type='" + type + "'" + ", date='" + date
				+ "'" + ", amount='" + amount + "'" + '}';
	}
}
