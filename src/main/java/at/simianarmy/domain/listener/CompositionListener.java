package at.simianarmy.domain.listener;

import at.simianarmy.config.autowire.AutowireHelper;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.CompositionRepository;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CompositionListener {

	private final Logger log = LoggerFactory.getLogger(CompositionListener.class);

	@Autowired
	private CompositionRepository compositionRepository;

	@PostLoad
	public void postLoad(Composition composition) {
		composition.setOldColorCode(composition.getColorCode());
		composition.setOldNumber(composition.getNr());
	}

	/**
	 * Weist die neue Nummer laut ColorCode der Composition zu.
	 * 
	 * @param composition
	 *            anzulegende Composition
	 */
	@PrePersist
	public void prePersist(Composition composition) {
		AutowireHelper.autowire(this, this.compositionRepository);
		log.info("Assigning new Number to Composition");

		if (composition.getColorCode() != null) {
			log.debug("ColorCode: " + composition.getColorCode().getId());
			Integer nextNumber = this.compositionRepository.getNextNrForColorCode(composition.getColorCode());
			log.debug("New Number is: " + nextNumber);
			composition.setNr(nextNumber);
		}
	}

	@PreUpdate
	public void preUpdate(Composition composition) {
		AutowireHelper.autowire(this, this.compositionRepository);
		log.info("PreUpdate event");

		if (composition.getColorCode() != composition.getOldColorCode()) {
			log.info("ColorCode for composition changed! Assigning new ColorCode!");

			log.debug("ColorCode: " + composition.getColorCode().getId());
			Integer nextNumber = this.compositionRepository.getNextNrForColorCode(composition.getColorCode());
			log.debug("New Number is: " + nextNumber);
			composition.setNr(nextNumber);
		}
	}
}
