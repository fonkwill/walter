package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A MembershipFeeForPeriod.
 */
@Entity
@Table(name = "membership_fee_for_period")
public class MembershipFeeForPeriod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "nr", nullable = false)
	private Integer nr;

	@NotNull
	@Column(name = "jhi_year", nullable = false)
	private Integer year;

	@Column(name = "reference_code")
	private String referenceCode;

	@Column(name = "paid")
	private Boolean paid;

	@Column(name = "note")
	private String note;

	@OneToOne
	@JoinColumn(unique = true)
	private CashbookEntry cashbookEntry;

	@ManyToOne
	private Membership membership;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNr() {
		return nr;
	}

	public MembershipFeeForPeriod nr(Integer nr) {
		this.nr = nr;
		return this;
	}

	public void setNr(Integer nr) {
		this.nr = nr;
	}

	public Integer getYear() {
		return year;
	}

	public MembershipFeeForPeriod year(Integer year) {
		this.year = year;
		return this;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public MembershipFeeForPeriod referenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
		return this;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean isPaid() {
		return paid;
	}

	public MembershipFeeForPeriod paid(Boolean paid) {
		this.paid = paid;
		return this;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public String getNote() {
		return note;
	}

	public MembershipFeeForPeriod note(String note) {
		this.note = note;
		return this;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public CashbookEntry getCashbookEntry() {
		return cashbookEntry;
	}

	public MembershipFeeForPeriod cashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
		return this;
	}

	public void setCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntry = cashbookEntry;
	}

	public Membership getMembership() {
		return membership;
	}

	public MembershipFeeForPeriod membership(Membership membership) {
		this.membership = membership;
		return this;
	}

	public void setMembership(Membership membership) {
		this.membership = membership;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MembershipFeeForPeriod membershipFeeForPeriod = (MembershipFeeForPeriod) o;
		if (membershipFeeForPeriod.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), membershipFeeForPeriod.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "MembershipFeeForPeriod{" + "id=" + getId() + ", nr='" + getNr() + "'" + ", year='" + getYear() + "'"
				+ ", referenceCode='" + getReferenceCode() + "'" + ", paid='" + isPaid() + "'" + ", note='" + getNote()
				+ "'" + "}";
	}
}
