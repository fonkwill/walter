package at.simianarmy.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Notification.
 */
@Entity
@Table(name = "notification")
public class Notification implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "entity_id")
	private Long entityId;

	@NotNull
	@Column(name = "created_at", nullable = false)
	private ZonedDateTime createdAt;

	@ManyToOne
	private User readByUser;

	@ManyToOne
	private NotificationRule notificationRule;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEntityId() {
		return entityId;
	}

	public Notification entityId(Long entityId) {
		this.entityId = entityId;
		return this;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public ZonedDateTime getCreatedAt() {
		return createdAt;
	}

	public Notification createdAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public void setCreatedAt(ZonedDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public User getReadByUser() {
		return readByUser;
	}

	public Notification readByUser(User user) {
		this.readByUser = user;
		return this;
	}

	public void setReadByUser(User user) {
		this.readByUser = user;
	}

	public NotificationRule getNotificationRule() {
		return notificationRule;
	}

	public Notification notificationRule(NotificationRule notificationRule) {
		this.notificationRule = notificationRule;
		return this;
	}

	public void setNotificationRule(NotificationRule notificationRule) {
		this.notificationRule = notificationRule;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Notification notification = (Notification) obj;
		if (notification.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, notification.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Notification{" + "id=" + id + ", entityId='" + entityId + "'" + ", createdAt='" + createdAt + "'" + '}';
	}
}
