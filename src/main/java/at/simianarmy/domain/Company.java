package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Company.
 */
@Entity
@Table(name = "company")
public class Company implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "street_address")
	private String streetAddress;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "recipient")
	private String recipient;

	@OneToMany(mappedBy = "orderingCompany")
	@JsonIgnore
	private Set<Composition> orderedCompositions = new HashSet<>();

	@OneToMany(mappedBy = "publisher")
	@JsonIgnore
	private Set<Composition> publishedCompositions = new HashSet<>();

	@OneToMany(mappedBy = "company")
	@JsonIgnore
	private Set<Receipt> receipts = new HashSet<>();

	@ManyToMany
	@JoinTable(name = "company_person_groups", joinColumns = @JoinColumn(name = "companies_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "person_groups_id", referencedColumnName = "id"))
	private Set<PersonGroup> personGroups = new HashSet<>();

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Company name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public Company streetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
		return this;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public Company postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public Company city(String city) {
		this.city = city;
		return this;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public Company country(String country) {
		this.country = country;
		return this;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRecipient() {
		return recipient;
	}

	public Company recipient(String recipient) {
		this.recipient = recipient;
		return this;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public Set<Composition> getOrderedCompositions() {
		return orderedCompositions;
	}

	public Company orderedCompositions(Set<Composition> compositions) {
		this.orderedCompositions = compositions;
		return this;
	}

	public Company addOrderedCompositions(Composition composition) {
		this.orderedCompositions.add(composition);
		composition.setOrderingCompany(this);
		return this;
	}

	public Company removeOrderedCompositions(Composition composition) {
		this.orderedCompositions.remove(composition);
		composition.setOrderingCompany(null);
		return this;
	}

	public void setOrderedCompositions(Set<Composition> compositions) {
		this.orderedCompositions = compositions;
	}

	public Set<Composition> getPublishedCompositions() {
		return publishedCompositions;
	}

	public Company publishedCompositions(Set<Composition> compositions) {
		this.publishedCompositions = compositions;
		return this;
	}

	public Company addPublishedCompositions(Composition composition) {
		this.publishedCompositions.add(composition);
		composition.setPublisher(this);
		return this;
	}

	public Company removePublishedCompositions(Composition composition) {
		this.publishedCompositions.remove(composition);
		composition.setPublisher(null);
		return this;
	}

	public void setPublishedCompositions(Set<Composition> compositions) {
		this.publishedCompositions = compositions;
	}

	public Set<Receipt> getReceipts() {
		return receipts;
	}

	public Company receipts(Set<Receipt> receipts) {
		this.receipts = receipts;
		return this;
	}

	public Company addReceipts(Receipt receipt) {
		this.receipts.add(receipt);
		receipt.setCompany(this);
		return this;
	}

	public Company removeReceipts(Receipt receipt) {
		this.receipts.remove(receipt);
		receipt.setCompany(null);
		return this;
	}

	public void setReceipts(Set<Receipt> receipts) {
		this.receipts = receipts;
	}

	public Set<PersonGroup> getPersonGroups() {
		return personGroups;
	}

	public Company personGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
		return this;
	}

	public Company addPersonGroups(PersonGroup personGroup) {
		this.personGroups.add(personGroup);
		personGroup.getCompanies().add(this);
		return this;
	}

	public Company removePersonGroups(PersonGroup personGroup) {
		this.personGroups.remove(personGroup);
		personGroup.getCompanies().remove(this);
		return this;
	}

	public void setPersonGroups(Set<PersonGroup> personGroups) {
		this.personGroups = personGroups;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Company company = (Company) o;
		if (company.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), company.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Company{" + "id=" + getId() + ", name='" + getName() + "'" + ", streetAddress='" + getStreetAddress()
				+ "'" + ", postalCode='" + getPostalCode() + "'" + ", city='" + getCity() + "'" + ", country='"
				+ getCountry() + "'" + ", recipient='" + getRecipient() + "'" + "}";
	}
}
