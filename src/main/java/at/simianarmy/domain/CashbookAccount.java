package at.simianarmy.domain;

import at.simianarmy.domain.enumeration.BankImporterType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A CashbookAccount.
 */
@Entity
@Table(name = "cashbook_account")
public class CashbookAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "bank_account")
	private Boolean bankAccount;

  @Enumerated(EnumType.STRING)
	@Column(name = "bank_importer_type")
	private BankImporterType bankImporterType;

  @NotNull
  @Column(name = "receipt_code", nullable = false, unique = true)
  private String receiptCode;

  @NotNull
  @Column(name = "current_number", nullable = false)
  private Integer currentNumber;

	@OneToMany(mappedBy = "cashbookAccount")
	@JsonIgnore
	private Set<CashbookEntry> cashbookEntries = new HashSet<>();

	@Column(name = "text_for_auto_transfer_from")
  private String textForAutoTransferFrom;

	@Column(name = "text_for_auto_transfer_to")
  private String textForAutoTransferTo;

	@NotNull
	@Column(name = "default_for_cashbook", nullable = false)
	private Boolean defaultForCashbook;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public CashbookAccount name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isBankAccount() {
		return bankAccount;
	}

	public CashbookAccount bankAccount(Boolean bankAccount) {
		this.bankAccount = bankAccount;
		return this;
	}

	public void setBankAccount(Boolean bankAccount) {
		this.bankAccount = bankAccount;
	}

	public BankImporterType getBankImporterType() {
		return bankImporterType;
	}

	public CashbookAccount bankImporterType(BankImporterType bankImporterType) {
		this.bankImporterType = bankImporterType;
		return this;
	}

	public void setBankImporterType(BankImporterType bankImporterType) {
		this.bankImporterType = bankImporterType;
	}

	public Set<CashbookEntry> getCashbookEntries() {
		return cashbookEntries;
	}

	public CashbookAccount cashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
		return this;
	}

	public CashbookAccount addCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntries.add(cashbookEntry);
		cashbookEntry.setCashbookAccount(this);
		return this;
	}

	public CashbookAccount removeCashbookEntry(CashbookEntry cashbookEntry) {
		this.cashbookEntries.remove(cashbookEntry);
		cashbookEntry.setCashbookAccount(null);
		return this;
	}

	public void setCashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
	}

  public String getReceiptCode() {
    return receiptCode;
  }

  public void setReceiptCode(String receiptCode) {
    this.receiptCode = receiptCode;
  }

  public CashbookAccount receiptCode(String receiptCode) {
    this.receiptCode = receiptCode;
    return this;
  }

  public Integer getCurrentNumber() {
    return currentNumber;
  }

  public void setCurrentNumber(Integer currentNumber) {
    this.currentNumber = currentNumber;
  }

  public CashbookAccount currentNumber(Integer currentNumber) {
    this.currentNumber = currentNumber;
    return this;
  }

  public String getTextForAutoTransferFrom() {
    return textForAutoTransferFrom;
  }

  public void setTextForAutoTransferFrom(String textForAutoTransferFrom) {
    this.textForAutoTransferFrom = textForAutoTransferFrom;
  }

  public CashbookAccount textForAutoTransferFrom(String textForAutoTransferFrom) {
	  this.textForAutoTransferFrom = textForAutoTransferFrom;
	  return this;
  }

  public String getTextForAutoTransferTo() {
    return textForAutoTransferTo;
  }

  public void setTextForAutoTransferTo(String textForAutoTransferTo) {
    this.textForAutoTransferTo = textForAutoTransferTo;
  }

  public CashbookAccount textForAutoTransferTo(String textForAutoTransferTo) {
	  this.textForAutoTransferTo = textForAutoTransferTo;
	  return this;
  }

  public Boolean isDefaultForCashbook() {
    return defaultForCashbook;
  }

  public void setDefaultForCashbook(Boolean defaultForCashbook) {
    this.defaultForCashbook = defaultForCashbook;
  }

  public CashbookAccount defaultForCashbook(Boolean defaultForCashbook) {
	  this.defaultForCashbook = defaultForCashbook;
	  return this;
  }

  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
	// setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CashbookAccount cashbookAccount = (CashbookAccount) o;
		if (cashbookAccount.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), cashbookAccount.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "CashbookAccount{" + "id=" + getId() + ", name='" + getName() + "'" + ", bankAccount='" + isBankAccount()
				+ "'" + ", bankImporterType='" + getBankImporterType() + "'" + "}";
	}
}
