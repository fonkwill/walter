package at.simianarmy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A Appointment.
 */
@Entity
@Table(name = "appointment")
public class Appointment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "street_address")
	private String streetAddress;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@NotNull
	@Column(name = "begin_date", nullable = false)
	private ZonedDateTime beginDate;

	@NotNull
	@Column(name = "end_date", nullable = false)
	private ZonedDateTime endDate;

	@OneToOne
	@JoinColumn(unique = true)
	private OfficialAppointment officialAppointment;

	@OneToMany(mappedBy = "appointment")
	@JsonIgnore
	private Set<CashbookEntry> cashbookEntries = new HashSet<>();

	@ManyToMany(mappedBy = "appointments")
	//@JoinTable(name = "appointment_persons", joinColumns = @JoinColumn(name = "appointments_id", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "persons_id", referencedColumnName = "ID"))
	private Set<Person> persons = new HashSet<>();

	@ManyToOne
	private AppointmentType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public Appointment name(String name) {
		this.name = name;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public Appointment description(String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public Appointment streetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
		return this;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public Appointment postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public Appointment city(String city) {
		this.city = city;
		return this;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public Appointment country(String country) {
		this.country = country;
		return this;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ZonedDateTime getBeginDate() {
		return beginDate;
	}

	public Appointment beginDate(ZonedDateTime beginDate) {
		this.beginDate = beginDate;
		return this;
	}

	public void setBeginDate(ZonedDateTime beginDate) {
		this.beginDate = beginDate;
	}

	public ZonedDateTime getEndDate() {
		return endDate;
	}

	public Appointment endDate(ZonedDateTime endDate) {
		this.endDate = endDate;
		return this;
	}

	public void setEndDate(ZonedDateTime endDate) {
		this.endDate = endDate;
	}

	public OfficialAppointment getOfficialAppointment() {
		return officialAppointment;
	}

	public Appointment officialAppointment(OfficialAppointment officialAppointment) {
		this.officialAppointment = officialAppointment;
		return this;
	}

	public void setOfficialAppointment(OfficialAppointment officialAppointment) {
		this.officialAppointment = officialAppointment;
	}

	public Set<CashbookEntry> getCashbookEntries() {
		return cashbookEntries;
	}

	public Appointment cashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
		return this;
	}

	public Appointment addCashbookEntries(CashbookEntry cashbookEntry) {
		cashbookEntries.add(cashbookEntry);
		cashbookEntry.setAppointment(this);
		return this;
	}

	public Appointment removeCashbookEntries(CashbookEntry cashbookEntry) {
		cashbookEntries.remove(cashbookEntry);
		cashbookEntry.setAppointment(null);
		return this;
	}

	public void setCashbookEntries(Set<CashbookEntry> cashbookEntries) {
		this.cashbookEntries = cashbookEntries;
	}

	public Set<Person> getPersons() {
		return persons;
	}

	public Appointment persons(Set<Person> people) {
		this.persons = people;
		return this;
	}

	public Appointment addPersons(Person person) {
		persons.add(person);
		person.getAppointments().add(this);
		return this;
	}

	public Appointment removePersons(Person person) {
		persons.remove(person);
		person.getAppointments().remove(this);
		return this;
	}

	public void setPersons(Set<Person> people) {
		this.persons = people;
	}

	public AppointmentType getType() {
		return type;
	}

	public Appointment type(AppointmentType appointmentType) {
		this.type = appointmentType;
		return this;
	}

	public void setType(AppointmentType appointmentType) {
		this.type = appointmentType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Appointment appointment = (Appointment) obj;
		if (appointment.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, appointment.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Appointment{" + "id=" + id + ", name='" + name + "'" + ", description='" + description + "'"
				+ ", streetAddress='" + streetAddress + "'" + ", postalCode='" + postalCode + "'" + ", city='" + city
				+ "'" + ", country='" + country + "'" + ", beginDate='" + beginDate + "'" + ", endDate='" + endDate
				+ "'" + '}';
	}
}
