package at.simianarmy.notification;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class NotificationEntityUtility {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * extracts the classname from a notification rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return the fully qualified classname
	 */
	public String getFullyQualifiedClassNameFromRule(NotificationRule rule) {
		return "at.simianarmy.domain." + rule.getEntity();
	}

	/**
	 * gets the notification entity from a notification rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return a notification entity
	 * @throws NotificationException
	 *             when the class could not be found or another error occured.
	 */
	public NotificationEntity getEntityClassFromRule(NotificationRule rule) throws NotificationException {
		String className = this.getFullyQualifiedClassNameFromRule(rule);

		try {
			@SuppressWarnings("rawtypes")
			Class clazz = Class.forName(className);
			Object entity = clazz.newInstance();

			if (!(entity instanceof NotificationEntity)) {
				throw new NotificationException(
						"Class " + className + " does not implement NotificationEntity interface.");
			}

			return (NotificationEntity) entity;
		} catch (ClassNotFoundException exception) {
			throw new NotificationException("Class " + className + " not found.");
		} catch (Exception exception) {
			throw new NotificationException("Failed to instanciate class " + className + ".");
		}
	}

	/**
	 * get all notification entities for a given rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return a list of notification entities
	 * @throws NotificationException
	 *             when the class could not be found or another error occured.
	 */
	public List<? extends NotificationEntity> getResultsFromRule(NotificationRule rule) throws NotificationException {
		NotificationEntity entity = this.getEntityClassFromRule(rule);
		try {
			return entityManager.createQuery(rule.getConditions(), entity.getClass()).getResultList();
		} catch (Exception exception) {
			throw new NotificationException("Failed to create query for rule id " + rule.getId());
		}
	}

	/**
	 * get a list of notification objects of entities for a given rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return a list of notifications
	 * @throws NotificationException
	 *             when the class could not be found or another error occured.
	 */
	public List<Notification> getNotificationsFromRule(NotificationRule rule) throws NotificationException {
		List<? extends NotificationEntity> results = this.getResultsFromRule(rule);

		List<Notification> notifications = new ArrayList<Notification>();
		for (NotificationEntity notificationEntity : results) {
			Notification notification = new Notification();
			notification.setEntityId(notificationEntity.getId());
			notification.setNotificationRule(rule);
			notification.setCreatedAt(ZonedDateTime.now());

			notifications.add(notification);
		}

		return notifications;
	}

}
