package at.simianarmy.notification;

public interface NotificationEntity {

	/**
	 * get the id of an entity.
	 * 
	 * @return the id of the entity.
	 */
	public Long getId();
}
