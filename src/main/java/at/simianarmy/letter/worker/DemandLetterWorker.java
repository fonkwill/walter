package at.simianarmy.letter.worker;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Person;
import at.simianarmy.letter.PaymentCheckWriter;
import at.simianarmy.letter.dataprovider.DemandLetterDataProvider;
import at.simianarmy.letter.pageeventhandler.DemandLetterPageEventHandler;
import at.simianarmy.letter.pageeventhandler.SerialLetterPageEventHandler;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfWriter;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

@Component("DemandLetterWorker")
public class DemandLetterWorker extends SerialLetterWorker {

  @Inject
  private DemandLetterDataProvider demandLetterDataProvider;

  public boolean write_letter(Document document, PdfWriter writer, SerialLetterPageEventHandler pageEventHandler,
    String html, boolean firstPersonGenerated, Object p) {

    PaymentCheckWriter paymentCheckWriter = new PaymentCheckWriter(document.getPageSize(), writer);

    String personHtml = html;

    if (firstPersonGenerated) {
      // not at the very first page
      pageEventHandler.setNextPageIsFirstPage(true);
      document.newPage();
    }
    firstPersonGenerated = true;

    if (p instanceof Person) {
      writeAddressBlock((Person) p, document, writer);
    } else if (p instanceof Company) {
      writeAddressBlock((Company) p, document, writer);
    }

    String referenceCode = null;
    String amount = null;

    if ((letter.isPreviousPeriod() != null && letter.isPreviousPeriod())
      || (letter.isCurrentPeriod() != null && letter.isCurrentPeriod() && p instanceof Person)) {

      if (letter.isPreviousPeriod() == null) {
        letter.setPreviousPeriod(false);
      }

      if (letter.isCurrentPeriod() == null) {
        letter.setCurrentPeriod(false);
      }

      demandLetterDataProvider.setLetter(letter);
      referenceCode = demandLetterDataProvider.getReferenceCode((Person) p);
      amount = demandLetterDataProvider.getAmount((Person) p);

      personHtml = replaceVariables(SerialLetterConstants.amountVariable, personHtml, amount);
      personHtml = replaceVariables(SerialLetterConstants.referenceVariable, personHtml, referenceCode);

    }

    if (letter.isPayment() && p instanceof Person) {
      // payment letter is only possible if current or previous period is set
      // therefore referenceCode and amount can not be null
      String sender = ((Person) p).getFirstName() + " " + ((Person) p).getLastName();
      String currentPeriod = demandLetterDataProvider.getCurrentPeriod();
      String vzweck1 = SerialLetterConstants.membershipFeeText + " " + currentPeriod;
      String vzweck2 = SerialLetterConstants.vzweckInfo + " " + referenceCode;

      paymentCheckWriter.writeZahlungsreferenz(referenceCode);
      paymentCheckWriter.writeSender(sender);
      paymentCheckWriter.writeVerwendungszweck(vzweck1);
      paymentCheckWriter.writeVerwendungszweckInfo(vzweck2);

    }

    if (personHtml != null) {
      writeHtml(personHtml, document, writer);
    }
    return firstPersonGenerated;
  }

  protected SerialLetterPageEventHandler instantiateSerialLetterPageEventHandler(PdfImportedPage page, Float marginBottom, Float marginTop,
    Float marginLeft, Float marginRight) {
    // Create new PageEventHandler
    SerialLetterPageEventHandler eventHandler = new DemandLetterPageEventHandler(page, marginBottom,
      marginTop, marginLeft, marginRight, this.letter);

    return eventHandler;
  }
}
