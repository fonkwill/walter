package at.simianarmy.letter.worker;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.Template;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.letter.pageeventhandler.SerialLetterPageEventHandler;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Utilities;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.tidy.Tidy;

/**
 * This class contains the whole logic for generating a serial letter Based on
 * the information from the serial letter dto it produces the concerning
 * letters. It writes a PDF to a PipedoutputStream which will be closed by it.
 *
 */
@Component("SerialLetterWorker")
public class SerialLetterWorker implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(SerialLetterWorker.class);

	protected Resource templatePageResource;

	public void setTemplatePageResource(Resource templatePageResource) {
		this.templatePageResource = templatePageResource;
	}

	protected Letter letter;

	protected Document document;

	protected PdfWriter writer;

	private OutputStream out;

	private List<Person> persons;

	private List<Company> companies;

	private String tenant;

	public Letter getLetter() {
		return letter;
	}

	public void setLetter(Letter letter) {
		this.letter = letter;
	}

	public OutputStream getOut() {
		return out;
	}

	public void setOut(OutputStream out) {
		this.out = out;
	}

  @Override
  public void run() {
		if (out != null && letter != null) {

		  if (tenant != null && !tenant.equals(TenantIdentiferResolver.MASTER_TENANT)) {
        TenantContext.setCurrentTenant(tenant);
      }

			this.document = new Document(SerialLetterConstants.defaultPageSize, 0, 0, 0, 0);

			this.writer = null;
			try {
				writer = PdfWriter.getInstance(document, out);
			} catch (DocumentException e) {
				logger.error("PDF-Writer could not be opened", e);
				throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNotGeneratable, null);
			}

			// The pageEventHandler makes sure that the correct margins and the
			// template is used
			SerialLetterPageEventHandler pageEventHandler = createPageEventHandler(letter.getTemplate(), writer);

			writer.setPageEvent(pageEventHandler);

			// set the margins for the very first page
			document.setMargins(pageEventHandler.getMarginLeft(), pageEventHandler.getMarginRight(),
					pageEventHandler.getMarginTopFirstPageOfLetter(),
					pageEventHandler.getMarginBottomFirstPageOfLetter());

			pageEventHandler.setNextPageIsFirstPage(true);
			document.open();

			// tidy up html
			String html = tidyHTML(letter.getText());

			boolean firstPersonGenerated = false;

			for (Person p : persons) {

			  System.out.println("writing: " + p.getFirstName());
				firstPersonGenerated = write_letter(document, writer, pageEventHandler, html, firstPersonGenerated, p);
				System.out.println("writing finsished: " + p.getFirstName());

			}

			for (Company c : companies) {

				firstPersonGenerated = write_letter(document, writer, pageEventHandler, html, firstPersonGenerated, c);

			}
			if (!firstPersonGenerated) {
				throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNoPersonsSelected, null);
			}

			document.close();

			try {

				out.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public boolean write_letter(Document document, PdfWriter writer, SerialLetterPageEventHandler pageEventHandler,
			String html, boolean firstPersonGenerated, Object p)  {
		String personHtml = html;

		if (firstPersonGenerated) {
			// not at the very first page
			pageEventHandler.setNextPageIsFirstPage(true);
			document.newPage();
		}
		firstPersonGenerated = true;

		if (p instanceof Person) {
			writeAddressBlock((Person) p, document, writer);
		} else if (p instanceof Company) {
			writeAddressBlock((Company) p, document, writer);
		}

		if (personHtml != null) {
			writeHtml(personHtml, document, writer);
		}
		return firstPersonGenerated;
	}

	protected String replaceVariables(String regex, String html, String amount) {
		if (html != null) {
			html = html.replaceAll(regex, amount);
		}
		return html;
	}

	private String tidyHTML(String text) {
		String tidyHtml = null;

		if (text != null) {
			InputStream is = IOUtils.toInputStream(text, StandardCharsets.UTF_8);
			ByteArrayOutputStream os = new ByteArrayOutputStream();

			Tidy tidy = new Tidy();
			tidy.setXHTML(true);
			tidy.setInputEncoding(StandardCharsets.UTF_8.toString());
			tidy.setOutputEncoding(StandardCharsets.UTF_8.toString());
			tidy.parse(is, os);

			try {
				tidyHtml = os.toString(StandardCharsets.UTF_8.toString());
			} catch (UnsupportedEncodingException e) {
				// not possible
				logger.error("Could not tidy up html");
				// e.printStackTrace();
			}

			try {
				is.close();
				os.close();
			} catch (IOException e) {
				// ignore
			}

		}

		return tidyHtml;
	}

	protected void writeHtml(String html, Document document, PdfWriter writer) {

		HTMLWorker worker = new HTMLWorker(document);
		if (html != null) {
			// tidy up html

			try {
				worker.parse(new StringReader(html));
			} catch (Exception e) {
				logger.error("Could not parse given html", e);
				throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNotParseable, null);
			}

		}

	}

	protected void writeAddressBlock(Person p, Document document, PdfWriter writer) {

		Rectangle pageSize = document.getPageSize();

		String genderAddress = null;
		if (p.getGender() != null) {
			if (p.getGender() == Gender.MALE) {
				genderAddress = SerialLetterConstants.maleAddress;
			} else {
				genderAddress = SerialLetterConstants.femaleAddress;
			}
		}

		ColumnText ct = new ColumnText(writer.getDirectContent());
		ct.setSimpleColumn(SerialLetterConstants.marginLeftAddress,
				pageSize.getTop() - SerialLetterConstants.marginBottomAddress, SerialLetterConstants.marginRightAddress,
				pageSize.getTop() - SerialLetterConstants.marginTopAddress);
		ct.addElement(new Paragraph(genderAddress == null ? "" : genderAddress));
		ct.addElement(new Paragraph(p.getFirstName() + " " + p.getLastName()));
		ct.addElement(new Paragraph(p.getStreetAddress()));
		ct.addElement(new Paragraph(p.getPostalCode() + " " + p.getCity()));
		try {
			ct.go();
		} catch (DocumentException e) {
			logger.error("Could not parse given html", e);
			throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNotGeneratable, null);
		}

	}

	protected void writeAddressBlock(Company p, Document document, PdfWriter writer) {
		float marginTop = SerialLetterConstants.marginTopAddress;
		Rectangle pageSize = document.getPageSize();

		if (p.getRecipient() != null && !p.getRecipient().isEmpty()) {
			marginTop = SerialLetterConstants.marginTopAddressWithRecipient;
		}

		ColumnText ct = new ColumnText(writer.getDirectContent());
		ct.setSimpleColumn(SerialLetterConstants.marginLeftAddress,
				pageSize.getTop() - SerialLetterConstants.marginBottomAddress, SerialLetterConstants.marginRightAddress,
				pageSize.getTop() - marginTop);
		ct.addElement(new Paragraph(SerialLetterConstants.companyAddress));
		ct.addElement(new Paragraph(p.getName()));
		if (p.getRecipient() != null && !p.getRecipient().isEmpty()) {
			ct.addElement(new Paragraph(SerialLetterConstants.recipientPrefix + " " + p.getRecipient()));
		}
		ct.addElement(new Paragraph(p.getStreetAddress()));
		ct.addElement(new Paragraph(p.getPostalCode() + " " + p.getCity()));
		try {
			ct.go();
		} catch (DocumentException e) {
			logger.error("Could not parse given html", e);
			throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNotGeneratable, null);
		}

	}

	/**
	 * Sets the margins and the template in the custom PDFEventHandler
	 * 
	 * @param template
	 *            The concerning template to this letter
	 * @param writer
	 *            The writer for the PDF
	 * @return A PageEventHandler for this letter
	 */
	protected SerialLetterPageEventHandler createPageEventHandler(Template template, PdfWriter writer) {
		Float marginBottom = SerialLetterConstants.defaultMarginBottom;
		Float marginTop = SerialLetterConstants.defaultMarginTop;
		Float marginLeft = SerialLetterConstants.defaultMarginLeft;
		Float marginRight = SerialLetterConstants.defaultMarginRight;
		PdfImportedPage page = null;

		// read attributes from the template
		if (template != null) {
			//

			if (template.getMarginBottom() != null) {
				marginBottom = Utilities.millimetersToPoints(template.getMarginBottom() * 10);
			}
			if (template.getMarginTop() != null) {
				marginTop = Utilities.millimetersToPoints(template.getMarginTop() * 10);
			}
			if (template.getMarginLeft() != null) {
				marginLeft = Utilities.millimetersToPoints(template.getMarginLeft() * 10);
			}
			if (template.getMarginRight() != null) {
				marginRight = Utilities.millimetersToPoints(template.getMarginRight() * 10);
			}

			// Get the template as PDF page
			PdfReader reader = null;
			try {
				if (letter.getTemplate().getFile() != null && templatePageResource != null) {
					reader = new PdfReader(templatePageResource.getInputStream());
					page = writer.getImportedPage(reader, 1);
				}
			} catch (Exception e) {
				logger.error("Could not read the template for serialLetter {}", letter, e);
				throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterTemplateNotReadable, null);
			}

		}

		return this.instantiateSerialLetterPageEventHandler(page, marginBottom,
      marginTop, marginLeft, marginRight);
	}

	protected SerialLetterPageEventHandler instantiateSerialLetterPageEventHandler(PdfImportedPage page, Float marginBottom, Float marginTop,
    Float marginLeft, Float marginRight) {
    // Create new PageEventHandler
    SerialLetterPageEventHandler eventHandler = new SerialLetterPageEventHandler(page, marginBottom,
      marginTop, marginLeft, marginRight);

    return eventHandler;
  }

	public void setPersons(List<Person> persons) {
		this.persons = persons;

	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;

	}

  public void setTenant(String tenant) {
    this.tenant = tenant;
  }

  public String getTenant() {
    return tenant;
  }
}
