package at.simianarmy.letter.worker;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Person;
import at.simianarmy.letter.dataprovider.EmailVerificationLetterDataProvider;
import at.simianarmy.letter.pageeventhandler.SerialLetterPageEventHandler;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

@Component("EmailVerificationLetterWorker")
public class EmailVerificationLetterWorker extends SerialLetterWorker {

  @Inject
  private EmailVerificationLetterDataProvider emailVerificationLetterDataProvider;

  @Override
  public void run() {
    this.letter.setText(
      this.letter.getText() + "<br/><br/><table><tr><td><img id='barcode' "
        + "src='https://api.qrserver.com/v1/create-qr-code/?data=%QR_LINK%&amp;size=100x100' "
        + "alt='%LINK%' "
        + "title='Link in Barcode-Form' "
        + "width='100' "
        + "height='100' /></td></tr></table>"
    );

    super.run();
  }

  public boolean write_letter(Document document, PdfWriter writer, SerialLetterPageEventHandler pageEventHandler,
    String html, boolean firstPersonGenerated, Object p) {

    if (!(p instanceof Person)) {
      throw new SerialLetterGenerationException("EmailVerificationLetter is only available for Persons!", null);
    }

    Person person = (Person) p;

    String personHtml = html;

    try {

      String link = this.emailVerificationLetterDataProvider
        .getQualifiedEmailVerificationLinkForPerson(person);
      String qrLink = URLEncoder.encode(link, "UTF-8");
      String secretKey = this.emailVerificationLetterDataProvider
        .getEmailVerificationLinkSecretForPerson(person);

      personHtml = replaceVariables(SerialLetterConstants.emailVerificationLink, personHtml, link);
      personHtml = replaceVariables(SerialLetterConstants.emailVerificationQRLink, personHtml,
        qrLink);
      personHtml = replaceVariables(SerialLetterConstants.emailVerificationLinkSecret, personHtml,
        secretKey);

    } catch (UnsupportedEncodingException e) {
      throw new SerialLetterGenerationException(e.getMessage(), null);
    }

    if (firstPersonGenerated) {
      // not at the very first page
      pageEventHandler.setNextPageIsFirstPage(true);
      document.newPage();
    }
    firstPersonGenerated = true;

    writeAddressBlock(person, document, writer);

    if (personHtml != null) {
      writeHtml(personHtml, document, writer);
    }
    return firstPersonGenerated;
  }
}
