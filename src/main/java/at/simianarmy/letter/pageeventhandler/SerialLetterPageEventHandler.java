package at.simianarmy.letter.pageeventhandler;

import at.simianarmy.domain.Letter;
import java.awt.Color;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfWriter;

import at.simianarmy.config.SerialLetterConstants;

/**
 * This class handles the different events for the PDF. It makes sure that the
 * template is set on every page and the margins are set correct. That means
 * that on the first page of every letter the top-margin has to be the defined
 * value from the constants, at the following pages the top-margin should be the
 * value from the template
 *
 */
public class SerialLetterPageEventHandler implements PdfPageEvent {

	private PdfImportedPage page;

	private Float marginBottom;
	private Float marginTop;
	private Float marginLeft;
	private Float marginRight;
	private Float marginTopFirstPageOfLetter;
	protected Float marginBottomFirstPageOfLetter;

	protected Rectangle whiteBox;

	protected boolean nextPageIsFirstPage = false;

	public SerialLetterPageEventHandler(PdfImportedPage page, Float marginBottom, Float marginTop,
			Float marginLeft, Float marginRight) {
		this.page = page;
		this.marginBottom = marginBottom;
		this.marginTop = marginTop;
		this.marginLeft = marginLeft;
		this.marginRight = marginRight;
		this.marginTopFirstPageOfLetter = SerialLetterConstants.defaultMarginTopLetter;
		this.marginBottomFirstPageOfLetter = marginBottom;

		// make a rectangle to overwrite the footer from the template by using payment
		// checks

		Rectangle rect = new Rectangle(0, 0, PageSize.A4.getRight(),
				SerialLetterConstants.defaultMarginBottomPaymentCheck);
		rect.setBorder(Rectangle.BOX);
		rect.setBorderWidth(0);
		rect.setBackgroundColor(Color.WHITE);
		this.whiteBox = rect;

	}

	public Float getMarginBottom() {
		return marginBottom;
	}

	public void setMarginBottom(Float marginBottom) {
		this.marginBottom = marginBottom;
	}

	public Float getMarginTop() {
		return marginTop;
	}

	public void setMarginTop(Float marginTop) {
		this.marginTop = marginTop;
	}

	public Float getMarginLeft() {
		return marginLeft;
	}

	public void setMarginLeft(Float marginLeft) {
		this.marginLeft = marginLeft;
	}

	public Float getMarginRight() {
		return marginRight;
	}

	public void setMarginRight(Float marginRight) {
		this.marginRight = marginRight;
	}

	public Float getMarginTopFirstPageOfLetter() {
		return marginTopFirstPageOfLetter;
	}

	@Override
	public void onOpenDocument(PdfWriter writer, Document document) {

	}

	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		if (page != null) {
			writer.getDirectContentUnder().addTemplate(page, 0, 0);
		}
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		if (nextPageIsFirstPage) {
			document.setMargins(marginLeft, marginRight, marginTopFirstPageOfLetter, marginBottomFirstPageOfLetter);
		} else {
			document.setMargins(marginLeft, marginRight, marginTop, marginBottom);
		}

	}

	@Override
	public void onCloseDocument(PdfWriter writer, Document document) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onChapterEnd(PdfWriter writer, Document document, float paragraphPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSectionEnd(PdfWriter writer, Document document, float paragraphPosition) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGenericTag(PdfWriter writer, Document document, Rectangle rect, String text) {
		// TODO Auto-generated method stub

	}

	public void setNextPageIsFirstPage(boolean nextPageIsFirstPage) {
		this.nextPageIsFirstPage = nextPageIsFirstPage;
	}

	public Float getMarginBottomFirstPageOfLetter() {
		return marginBottomFirstPageOfLetter;
	}
}
