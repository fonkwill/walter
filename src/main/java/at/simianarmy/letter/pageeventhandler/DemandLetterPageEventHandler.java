package at.simianarmy.letter.pageeventhandler;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Letter;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfWriter;

public class DemandLetterPageEventHandler extends SerialLetterPageEventHandler {

  private Boolean payment;

  public DemandLetterPageEventHandler(PdfImportedPage page,
    Float marginBottom, Float marginTop, Float marginLeft,
    Float marginRight, Letter letter) {
    super(page, marginBottom, marginTop, marginLeft, marginRight);
    this.payment = letter.isPayment();
    this.marginBottomFirstPageOfLetter = this.payment == true
      ?SerialLetterConstants.defaultMarginBottomPaymentCheck
      :marginBottom;
  }

  @Override
  public void onStartPage(PdfWriter writer, Document document) {
    super.onStartPage(writer, document);
    if (nextPageIsFirstPage) {
      // this page is the first page of a letter
      if (this.payment) {
        writer.getDirectContent().rectangle(whiteBox);
      }
      nextPageIsFirstPage = false;
    }
  }
}
