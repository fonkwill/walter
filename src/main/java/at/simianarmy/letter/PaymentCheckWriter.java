package at.simianarmy.letter;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Utilities;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.SerialLetterGenerationException;

/**
 * This class writes the concerning information on a payment check
 * (Zahlungsanweisung) It is important, that the data to be printed, is on the
 * right position. To get the right position the payment check is build like a
 * grid with rows and columns.
 * 
 * The specification can be found at
 * https://www.stuzza.at/de/download/zahlungsbelege/107-techspecs-zahlungsanweisung-pdf/file.html
 * and
 * https://www.stuzza.at/de/download/zahlungsbelege/114-hinweise-produktion/file.html
 *
 */
public class PaymentCheckWriter {

	/**
	 * The line height in the grid is 1/6 inch
	 */
	private static final Float lineHeight = Utilities.inchesToPoints(1F / 6);

	/**
	 * The column width in the grid is 1/10 inch
	 */
	private static final Float columnWidth = Utilities.inchesToPoints(1F / 10);

	private Float rlx;

	private Float rly = 0F;

	private Font clearReadFont;

	private PdfWriter writer;

	public PaymentCheckWriter(Rectangle pageSize, PdfWriter writer) {
		if (pageSize == null) {
			throw new IllegalArgumentException("pageSize must not be null");
		}
		if (writer == null) {
			throw new IllegalArgumentException("writer must not be null");
		}
		this.writer = writer;

		// we need the coordinates of the right corner on the bottom of the page
		rlx = pageSize.getRight();

		FontFactory.register("ClearReadMono_2.ttf");
		clearReadFont = FontFactory.getFont("ClearRead Mono", BaseFont.IDENTITY_H);
		if (clearReadFont == null) {
			throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterNotGeneratable, null);
		}

	}

	public void writeZahlungsreferenz(String text) {
		int plusColumn = 3;
		int plusLine = 15;
		int codeColumn = 57;
		int codeLine = 15;

		writeText(plusLine, plusColumn, "+");
		writeText(codeLine, codeColumn, text);

	}

	public PdfWriter getWriter() {
		return writer;
	}

	public void setWriter(PdfWriter writer) {
		this.writer = writer;
	}

	public void writeVerwendungszweck(String text) {
		// Verwendungszeck Line 1 has max 55 characters
		text = text.substring(0, Math.min(text.length(), 40));

		int vzweckColumn = 57;
		int vzweckLine = 13;

		writeText(vzweckLine, vzweckColumn, text);

	}

	public void writeVerwendungszweckInfo(String text) {
		// VerwendungszweckInfo (the second line of Verwendungszwekc)
		// can be writed over 2 lines with smaller font
		int vzweckColumn = 57;
		int vzweckLine1 = 11;
		int vzweckLine2 = 10;

		// max 50 characters each line
		String textLine1 = text.substring(0, Math.min(text.length(), 50));
		writeText(vzweckLine1, vzweckColumn, textLine1);
		if (text.length() > 50) {
			String textLine2 = text.substring(50, Math.min(text.length(), 100));
			writeText(vzweckLine2, vzweckColumn, textLine2);
		}

	}

	public void writeSender(String text) {
		// VerwendungszweckInfo (the second line of Verwendungszwekc)
		// can be writed over 2 lines with smaller font
		int vzweckColumn = 57;
		int vzweckLine = 7;
		// As the Verwendungszweck must not have more than 140 characters, for
		// the Verwendungszweck info only 85 characters are allowed
		writeText(vzweckLine, vzweckColumn, text);

	}

	private void writeText(int line, int column, String text) {
		if (text != null) {
			Float x = getXForColumn(column);
			Float y = getYForLine(line);

			PdfContentByte cb = writer.getDirectContent();

			cb.setFontAndSize(clearReadFont.getBaseFont(), 10F);
			cb.beginText();
			cb.setTextMatrix(x, y);
			cb.showText(text);
			cb.endText();
		}
	}

	private Float getYForLine(int line) {
		// get the y coordinate beginning from the bottom
		return rly + ((line - 1) * lineHeight);

	}

	private Float getXForColumn(int column) {
		// get the x coordinate beginning from right
		return rlx - (column * columnWidth);
	}

}
