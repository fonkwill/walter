package at.simianarmy.letter;

public class SerialLetterFactoryException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public SerialLetterFactoryException (String message) {
    super(message);
  }

  public SerialLetterFactoryException (String message, Exception cause) {
    super(message, cause);
  }

}
