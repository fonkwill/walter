package at.simianarmy.letter.dataprovider;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.service.mapper.qualifier.VerificationLinkQualifier;
import javax.inject.Inject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class EmailVerificationLetterDataProvider {

  @Inject
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Inject
  private VerificationLinkQualifier verificationLinkQualifier;

  public String getQualifiedEmailVerificationLinkForPerson(Person person) {
    return this.verificationLinkQualifier.verificationLinkForPersonId(person.getId());
  }

  public String getEmailVerificationLinkSecretForPerson(Person person) {
    EmailVerificationLink link = this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person);
    return link.getSecretKey();
  }

}
