package at.simianarmy.letter.dataprovider;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.service.PersonService;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import java.time.LocalDate;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class DemandLetterDataProvider {

	private final Logger logger = LoggerFactory.getLogger(DemandLetterDataProvider.class);

	@Inject
	private PersonService personService;

	private Letter letter;

	public Letter getLetter() {
		return letter;
	}

	public void setLetter(Letter letter) {
		this.letter = letter;
	}

	public String getReferenceCode(Person p) {
		if (this.letter == null) {

			throw new IllegalStateException("letter must not be null");
		}
		MembershipFeeForPeriod mfpMaxYear = null;
		for (MembershipFeeForPeriod mfp : personService.getOpenMembershipFeeForPeriods(p, letter.isCurrentPeriod(),
				letter.isPreviousPeriod())) {
			if (mfpMaxYear == null || mfpMaxYear.getYear() < mfp.getYear()) {
				mfpMaxYear = mfp;
			}
		}
		if (mfpMaxYear == null) {
			return null;
		}
		logger.debug("Got Reference code for person {}, {}", p.getId(), mfpMaxYear.getReferenceCode());
		return mfpMaxYear.getReferenceCode();

	}

	public String getAmount(Person p) {
		if (this.letter == null) {
			throw new IllegalStateException("letter must not be null");
		}
		return personService.getOpenMembershipAmount(p, letter.isCurrentPeriod(), letter.isPreviousPeriod()).toString();

	}

	public String getCurrentPeriod() {
		if (letter.isCurrentPeriod() != true && letter.isPreviousPeriod() != true) {
			throw new SerialLetterGenerationException("no period chosen", null);
		}
		if (letter.isCurrentPeriod()) {
			return "" + LocalDate.now().getYear();
		}
		if (letter.isPreviousPeriod()) {
			return SerialLetterConstants.previousPeriods;
		}
		return null;
	}

}
