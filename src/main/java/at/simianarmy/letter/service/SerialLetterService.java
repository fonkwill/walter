package at.simianarmy.letter.service;

import at.simianarmy.domain.Company;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.Template;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.service.TemplateFileStoreService;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.LetterService;
import at.simianarmy.service.PersonService;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import at.simianarmy.service.specification.CompanySpecifications;
import at.simianarmy.service.specification.PersonSpecifications;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service implementation for generating Serial Letters.
 */
@Service("SerialLetterService")
public class SerialLetterService {

	@Inject
	PersonService personService;

	@Inject
	private TemplateFileStoreService fileStore;

	@Inject
	private TemplateRepository templateRepository;

	@Inject
	protected PersonRepository personRepository;

	@Inject
	private CompanyRepository companyRepository;

	private final Logger logger = LoggerFactory.getLogger(LetterService.class);

	@Transactional
	public Resource getTemplatePage(Letter letter) {
		if (letter == null) {
			logger.error("letter not set");
			throw new SerialLetterGenerationException(ServiceErrorConstants.letterNotSet, null);
		}
		Resource result = null;
		// load template from database
		if (letter.getTemplate() != null && letter.getTemplate().getId() != null) {
			Template template = templateRepository.findById(letter.getTemplate().getId()).orElse(null);
			letter.setTemplate(template);

			try {
				result = fileStore.getTemplate(UUID.fromString(letter.getTemplate().getFile())).getResource();
			} catch (FileStoreException exc) {
				logger.error("Could not read template");
				throw new SerialLetterGenerationException(ServiceErrorConstants.serialLetterTemplateNotReadable, null);
			}

		}
		return result;
	}

	@Transactional
	public List<Company> getRelevantCompanies(Letter letter) {
		if (letter == null) {
			logger.error("letter not set");
			throw new SerialLetterGenerationException(ServiceErrorConstants.letterNotSet, null);
		}
		List<Long> allPersonGroups = this.getPersonGroupIdsFromLetter(letter);

		Specification<Company> specs = CompanySpecifications.isInPersonGroup(allPersonGroups);
		return companyRepository.findAll(specs);

	}

	@Transactional
	public List<Person> getRelevantPersons(Letter letter) {
		if (letter == null) {
			logger.error("letter not set");
			throw new SerialLetterGenerationException(ServiceErrorConstants.letterNotSet, null);
		}
		return this.computePersonList(letter);
	}

  @Transactional
  public List<Person> getRelevantPersonsForMailLetter(Letter letter) {
    List<Person> relevantPersonsForLetter = this.getRelevantPersons(letter);

    List<Long> personGroupIds = this.getPersonGroupIdsFromLetter(letter);

    if (personGroupIds.size() == 0) {
      personGroupIds = null;
    }

    List<Person> personsWithActiveEmail = this.personRepository
      .findAllByActiveEmailAndPersonGroups(personGroupIds);

    return this.intersectPersonLists(relevantPersonsForLetter, personsWithActiveEmail);
  }

  @Transactional
  public List<Person> getPersonsWithInactiveMail(Letter letter) {
    List<Person> relevantPersonsForLetter = this.getRelevantPersons(letter);

    List<Long> personGroupIds = this.getPersonGroupIdsFromLetter(letter);

    if (personGroupIds.size() == 0) {
      personGroupIds = null;
    }

    List<Person> personsWithInActiveEmail = this.personRepository
      .findAllByInActiveEmailAndPersonGroups(personGroupIds);

    return this.intersectPersonLists(relevantPersonsForLetter, personsWithInActiveEmail);
  }

	protected List<Person> computePersonList(Letter letter) {

	  List<Long> allPersonGroups = this.getPersonGroupIdsFromLetter(letter);

    Specification<Person> specs = PersonSpecifications.isInPersonGroup(allPersonGroups);
    return personRepository.findAll(specs);
  }

  protected List<Person> intersectPersonLists(List<Person> listA, List<Person> listB) {
	  listA.retainAll(listB);
	  return listA;
  }

  protected List<Long> getPersonGroupIdsFromLetter(Letter letter) {
    List<Long> allPersonGroups = new ArrayList<>();

    if (letter.getPersonGroups() != null) {

      for (PersonGroup pg : letter.getPersonGroups()) {
        personService.getPersonGroupIdsForPersonGroup(pg, allPersonGroups);
      }
    }

    return allPersonGroups;
  }

}
