package at.simianarmy.letter.service;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.enumeration.LetterType;
import javax.inject.Inject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class SerialLetterServiceFactory {

  @Inject
  ApplicationContext context;

  public SerialLetterService createForLetter(Letter letter)
    throws SerialLetterServiceFactoryException {

    if (letter.getLetterType().equals(LetterType.SERIAL_LETTER)) {
      return this.context.getBean("SerialLetterService", SerialLetterService.class);
    } else if (letter.getLetterType().equals(LetterType.DEMAND_LETTER)) {
      return this.context.getBean("DemandLetterService", DemandLetterService.class);
    } else if (letter.getLetterType().equals(LetterType.EMAIL_VERIFICATION_LETTER)) {
      return this.context.getBean("EmailVerificationLetterService", EmailVerificationLetterService.class);
    } else {
      throw new SerialLetterServiceFactoryException("This LetterType is not implemented!");
    }
  }

}
