package at.simianarmy.letter.service;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("DemandLetterService")
public class DemandLetterService extends SerialLetterService {

  private final Logger logger = LoggerFactory.getLogger(DemandLetterService.class);

  protected List<Person> computePersonList(Letter letter) {
    logger.debug("Create payment letter with attributes {}", letter);
    LocalDate now = LocalDate.now();

    List<Person> personList = new ArrayList<>();

    if (letter.isCurrentPeriod() != null && letter.isCurrentPeriod()) {
      // add current not if both options are set
      if (letter.isPreviousPeriod() == null || !letter.isPreviousPeriod()) {
        personList.addAll(personRepository.findAllWithUnpaidFeesAndNoDirectDebit(now.getYear(), now.getYear()));
      }
    }
    if (letter.isPreviousPeriod() != null && letter.isPreviousPeriod()) {
      personList.addAll(personRepository.findAllWithUnpaidFeesAndNoDirectDebit(1900, now.getYear() - 1));
    }
    // make distinct with set
    personList = new ArrayList<Person>(new HashSet<>(personList));

    return personList;
  }

}
