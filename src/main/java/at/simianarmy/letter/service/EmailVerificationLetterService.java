package at.simianarmy.letter.service;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.service.specification.PersonSpecifications;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service("EmailVerificationLetterService")
public class EmailVerificationLetterService extends SerialLetterService {

  private final Logger logger = LoggerFactory.getLogger(EmailVerificationLetterService.class);

  protected List<Person> computePersonList(Letter letter) {
    logger.debug("Create email verification letter with attributes {}", letter);
    List<Long> allPersonGroups = new ArrayList<>();

    if (letter.getPersonGroups() != null) {

      for (PersonGroup pg : letter.getPersonGroups()) {
        personService.getPersonGroupIdsForPersonGroup(pg, allPersonGroups);
      }
    }

    if (allPersonGroups.size() == 0) {
      allPersonGroups = null;
    }

    return personRepository.findAllByPersonGroupsAndEmailVerificationLinksToVerify(allPersonGroups);
  }

}
