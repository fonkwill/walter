package at.simianarmy.letter.service;

public class SerialLetterServiceFactoryException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public SerialLetterServiceFactoryException (String message) {
    super(message);
  }

  public SerialLetterServiceFactoryException (String message, Exception cause) {
    super(message, cause);
  }

}
