package at.simianarmy.letter;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.letter.worker.DemandLetterWorker;
import at.simianarmy.letter.worker.EmailVerificationLetterWorker;
import at.simianarmy.letter.worker.SerialLetterWorker;
import at.simianarmy.service.ServiceErrorConstants;
import javax.inject.Inject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class SerialLetterFactory {

  @Inject
  ApplicationContext context;

  public SerialLetterWorker createForLetter(Letter letter)
    throws SerialLetterFactoryException {

    SerialLetterWorker serialLetterWorker = null;

    if (letter.getLetterType().equals(LetterType.SERIAL_LETTER)) {
      serialLetterWorker = this.context.getBean("SerialLetterWorker", SerialLetterWorker.class);
    } else if (letter.getLetterType().equals(LetterType.DEMAND_LETTER)) {
      if (letter.isCurrentPeriod() != true && letter.isPreviousPeriod() != true) {
        throw new SerialLetterFactoryException(ServiceErrorConstants.serialLetterNoPeriodSelected);
      }
      serialLetterWorker = this.context.getBean("DemandLetterWorker", DemandLetterWorker.class);
    } else if (letter.getLetterType().equals(LetterType.EMAIL_VERIFICATION_LETTER)) {
      serialLetterWorker = this.context.getBean("EmailVerificationLetterWorker",
        EmailVerificationLetterWorker.class);
    } else {
      throw new SerialLetterFactoryException("This LetterType is not implemented!");
    }

    serialLetterWorker.setLetter(letter);

    return serialLetterWorker;
  }

}
