package at.simianarmy.imports.data.csv.mapper.qualifier;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.repository.MembershipFeeRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Named;

@Component
public class PeopleCSVOQualifier {


  private MembershipFeeRepository membershipFeeRepository;

  public PeopleCSVOQualifier(MembershipFeeRepository membershipFeeRepository) {
    this.membershipFeeRepository = membershipFeeRepository;
  }

  @Named("emailTypeForEmail")
  public EmailType emailTypeForEmail(String email) {
    if (StringUtils.hasText(email)) {
      return EmailType.MANUALLY;
    }
    return EmailType.VERIFIED;
  }

  @Named("membershipFeeIdForMembershipDescription")
  public Long membershipFeeIdForMembershipDescription(String description) {
    MembershipFee membershipFee =  membershipFeeRepository.findFirstByDescription(description);
    if (membershipFee != null) {
      return membershipFee.getId();
    }
    return  null;

  }

}
