package at.simianarmy.imports.data.csv.mapper;

import at.simianarmy.imports.data.csv.mapper.qualifier.PeopleCSVOQualifier;
import at.simianarmy.imports.data.csv.csvo.PeopleCSVO;
import at.simianarmy.service.dto.MemberDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = PeopleCSVOQualifier.class)
public interface PeopleCSVOMapper {

  @Mapping(source = "gender", target = "person.gender")
  @Mapping(source = "lastName", target = "person.lastName")
  @Mapping(source = "firstName", target = "person.firstName")
  @Mapping(source = "birthDate", target = "person.birthDate")
  @Mapping(source = "email", target = "person.emailType", qualifiedByName = "emailTypeForEmail")
  @Mapping(source = "email", target = "person.email")
  @Mapping(source = "telephoneNumber", target = "person.telephoneNumber")
  @Mapping(source = "streetAddress", target = "person.streetAddress")
  @Mapping(source = "postalCode", target = "person.postalCode")
  @Mapping(source = "city", target = "person.city")
  @Mapping(source = "country", target = "person.country")
  @Mapping(source = "directDebit", target =  "person.hasDirectDebit")
  @Mapping(source = "beginDate", target = "membership.beginDate")
  @Mapping(source = "endDate", target = "membership.endDate")
  @Mapping(source = "membershipFeeName", target = "membership.membershipFeeId", qualifiedByName = "membershipFeeIdForMembershipDescription")
  MemberDTO peopleCSVOToMemberDTO(PeopleCSVO peopleCSVO);

}
