package at.simianarmy.imports.data.csv.csvo;

import at.simianarmy.domain.enumeration.Gender;

import java.time.LocalDate;

public class PeopleCSVO {

  private Gender gender;

  private String lastName;

  private String firstName;

  private LocalDate birthDate;

  private String email;

  private String telephoneNumber;

  private String streetAddress;

  private String postalCode;

  private String city;

  private String country;

  private LocalDate beginDate;

  private LocalDate endDate;

  private String membershipFeeName;


  private Boolean directDebit;

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTelephoneNumber() {
    return telephoneNumber;
  }

  public void setTelephoneNumber(String telephoneNumber) {
    this.telephoneNumber = telephoneNumber;
  }

  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public LocalDate getBeginDate() {
    return beginDate;
  }

  public void setBeginDate(LocalDate beginDate) {
    this.beginDate = beginDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public String getMembershipFeeName() {
    return membershipFeeName;
  }

  public void setMembershipFeeName(String membershipFeeName) {
    this.membershipFeeName = membershipFeeName;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public Boolean getDirectDebit() {
    return directDebit;
  }

  public void setDirectDebit(Boolean directDebit) {
    this.directDebit = directDebit;
  }



}
