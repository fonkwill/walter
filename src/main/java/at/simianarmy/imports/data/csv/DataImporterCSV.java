package at.simianarmy.imports.data.csv;

import at.simianarmy.imports.data.DataImporterException;
import at.simianarmy.service.ServiceErrorConstants;
import org.simpleflatmapper.csv.CsvMapper;
import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.map.MapperBuildingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class DataImporterCSV<T, U> {

  private static final Logger logger = LoggerFactory.getLogger(DataImporterCSV.class);

  private static final char separator = ';';

  public List<T> readData(MultipartFile file) throws DataImporterException {

    try {
      logger.debug("Going to read in csv-file for data import {]", file.getOriginalFilename());
      Reader in = new InputStreamReader(file.getInputStream(), this.getCharset());

      CsvMapper<U> csvMapper = this.getCsvMapper();

     List<T> result = CsvParser.separator(separator).mapWith(csvMapper).stream(in).map(
        csvObject -> this.mapToDTO(csvObject)).collect(Collectors.toList());

      return result;

    } catch (IOException e) {
        throw new DataImporterException(ServiceErrorConstants.csvImportFileCouldNotBeRead);
    } catch (MapperBuildingException ex) {
       throw new DataImporterException(ServiceErrorConstants.csvImportColumnNameNotMapped);
    } catch (DateTimeParseException dex) {
       throw new DataImporterException(ServiceErrorConstants.dateFormatNotParseable);
    }
  }

  protected abstract T mapToDTO(U csvObject);

  protected abstract CsvMapper<U> getCsvMapper();

  protected abstract String getCharset();

}
