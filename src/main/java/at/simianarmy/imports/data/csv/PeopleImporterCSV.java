package at.simianarmy.imports.data.csv;

import at.simianarmy.imports.data.csv.mapper.PeopleCSVOMapper;
import at.simianarmy.imports.data.csv.csvo.PeopleCSVO;
import at.simianarmy.service.dto.MemberDTO;
import org.simpleflatmapper.csv.CsvMapper;
import org.simpleflatmapper.csv.CsvMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class PeopleImporterCSV extends DataImporterCSV<MemberDTO, PeopleCSVO> {

  public static final String CHARSET = "UTF-8";

  private PeopleCSVOMapper peopleCSVOMapper;

  public PeopleImporterCSV(PeopleCSVOMapper peopleCSVOMapper) {
    this.peopleCSVOMapper = peopleCSVOMapper;
  }

  @Override
  protected MemberDTO mapToDTO(PeopleCSVO csvObject) {
    return peopleCSVOMapper.peopleCSVOToMemberDTO(csvObject);
  }

  @Override
  protected CsvMapper<PeopleCSVO> getCsvMapper() {
    CsvMapper<PeopleCSVO> mapper =
      CsvMapperFactory
        .newInstance()
        .defaultDateFormat("dd.MM.yyyy")
        .newMapper(PeopleCSVO.class);
    return mapper;
  }

  @Override
  public String getCharset() {
    return CHARSET;
  }
}
