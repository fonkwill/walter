package at.simianarmy.imports.data.service;

import at.simianarmy.domain.Membership;
import at.simianarmy.domain.enumeration.MigrationImporterType;
import at.simianarmy.imports.data.DataImporterException;
import at.simianarmy.imports.data.DataImporterFactory;
import at.simianarmy.imports.data.csv.DataImporterCSV;
import at.simianarmy.imports.data.csv.csvo.PeopleCSVO;
import at.simianarmy.service.dto.MemberDTO;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.dto.PersonCollectionDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import org.simpleflatmapper.csv.CsvMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataImporterService {

  /**
   * Imports person data from a a csv file and returns a PersonCollectionDTO with members and persons
   * @param file The file for the import
   * @return A DTO with pembers and persons
   */
  public PersonCollectionDTO importPersonsFromFile(MultipartFile file) {
    try {

      @SuppressWarnings("unchecked")
      DataImporterCSV<MemberDTO, PeopleCSVO> importer = DataImporterFactory.createMigrationImporterCSV(MigrationImporterType.PEOPLE_IMPORTER);


      List<MemberDTO> readInMembers = importer.readData(file);

      List<MemberDTO> members = new ArrayList<>();
      List<PersonDTO> persons = new ArrayList<>();

      for (MemberDTO member : readInMembers) {
        MembershipDTO membership =  member.getMembership();
        if (membership.getBeginDate() == null) {
            persons.add(member.getPerson());
        } else {
            members.add(member);
        }

      }

      PersonCollectionDTO personCollectionDTO = new PersonCollectionDTO();
      personCollectionDTO.setMembers(members);
      personCollectionDTO.setPersons(persons);
      return personCollectionDTO;

    } catch (DataImporterException e) {
      throw new ServiceValidationException(e.getMessage(), new String[]{} );
    }

  }

}
