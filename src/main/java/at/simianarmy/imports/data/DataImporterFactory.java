package at.simianarmy.imports.data;

import at.simianarmy.domain.enumeration.MigrationImporterType;
import at.simianarmy.imports.data.DataImporterException;
import at.simianarmy.imports.data.csv.DataImporterCSV;
import at.simianarmy.imports.data.csv.PeopleImporterCSV;
import at.simianarmy.imports.data.csv.csvo.PeopleCSVO;
import at.simianarmy.service.dto.MemberDTO;
import at.simianarmy.service.util.BeanUtil;


public class DataImporterFactory {

  public static DataImporterCSV createMigrationImporterCSV(MigrationImporterType type) throws DataImporterException {
    switch (type) {
      case PEOPLE_IMPORTER:
        return BeanUtil.getBean(PeopleImporterCSV.class);
      default: throw new DataImporterException("This MigrationImporterType is unknown or not implemented ");
    }
  }

}
