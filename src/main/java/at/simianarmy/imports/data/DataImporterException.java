package at.simianarmy.imports.data;

public class DataImporterException extends Exception {

  public DataImporterException(String message) {
    super(message);
  }

  public DataImporterException(String message, Exception cause) {
    super(message, cause);
  }
}
