package at.simianarmy.imports.accounting;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.BankImportDataRepository;

import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CustomizationService;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.transaction.annotation.Transactional;

public abstract class BankImporterGeneral {

	@Inject
	private BankImportDataRepository bankImportDataRepository;

	@Inject
  private CashbookAccountRepository cashbookAccountRepository;

	@Inject
  private CustomizationService customizationService;

	/**
	 * Persists the BankImportData with Specified Type into DB if necessary.
	 * 
	 * @param bankImportData
	 *            The Data to persistFile
	 * @param cashbookAccount
	 *            The CashbookAccount to use
	 * @return The persisted Data
	 */
	@Transactional
	protected List<BankImportData> persistNotImportedBankImportData(List<BankImportData> bankImportData,
			CashbookAccount cashbookAccount) {
		List<BankImportData> result = new ArrayList<BankImportData>();

		for (BankImportData data : bankImportData) {
			if (this.bankImportDataRepository
					.findByCashbookAccountAndReference(cashbookAccount.getId(), data.getEntryReference()).size() == 0) {
			  data = this.createCashbookAccountTransferIfNecessery(data);
				result.add(this.bankImportDataRepository.save(data));
			} else {
				this.bankImportDataRepository.delete(data);
			}
		}

		this.bankImportDataRepository.flush();

		return result;
	}

	protected BankImportData createCashbookAccountTransferIfNecessery(BankImportData bankImportData) {
	  String text = bankImportData.getEntryText();
    CashbookCategory cashbookCategory = this.customizationService.getDefaultCashbookCategoryForTransfers();

    List<CashbookAccount> cashbookAccountsForTransferFrom = this.cashbookAccountRepository.findAllByTextForAutoTransferFromIsNotNullAndNotEmpty();

    for (CashbookAccount account : cashbookAccountsForTransferFrom) {
      if (text.contains(account.getTextForAutoTransferFrom())) {
        bankImportData.setTransfer(true);
        bankImportData.setCashbookAccountFrom(account);
        bankImportData.setCashbookAccountTo(bankImportData.getCashbookAccount());
        bankImportData.setCashbookCategory(cashbookCategory);
        return bankImportData;
      }
    }

    List<CashbookAccount> cashbookAccountsForTransferTo = this.cashbookAccountRepository.findAllByTextForAutoTransferToIsNotNullAndNotEmpty();

    for (CashbookAccount account: cashbookAccountsForTransferTo) {
      if (text.contains(account.getTextForAutoTransferTo())) {
        bankImportData.setTransfer(true);
        bankImportData.setCashbookAccountFrom(bankImportData.getCashbookAccount());
        bankImportData.setCashbookAccountTo(account);
        bankImportData.setCashbookCategory(cashbookCategory);
        return bankImportData;
      }
    }

    return bankImportData;
  }

	protected String cutInputString(String input) {
		if (input.length() > 255) {
			input = input.substring(0, 252);
			input = input + "...";
		}

		return input;
	}
}
