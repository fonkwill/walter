package at.simianarmy.imports.accounting;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.api.BankImporterAPI;
import at.simianarmy.imports.accounting.api.BankImporterGeorgeAPI;
import at.simianarmy.imports.accounting.csv.BankImporterBACACSV;
import at.simianarmy.imports.accounting.csv.BankImporterCSV;
import at.simianarmy.imports.accounting.csv.BankImporterEasyBankCSV;
import at.simianarmy.imports.accounting.csv.BankImporterGeorgeCSV;

import at.simianarmy.imports.accounting.csv.BankImporterRAIKACSV;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.ServiceValidationException;
import javax.inject.Inject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class BankImporterFactory {

	@Inject
	ApplicationContext context;

	/**
	 * Creates a new CSVImporter.
	 * 
	 * @param cashbookAccount
	 *            The cashbookAccount
	 * @return The CSVImporter
	 * @throws BankImporterException
	 *             If the Type is unknown
	 */
	public BankImporterCSV createCSVImporter(CashbookAccount cashbookAccount) {
		if (cashbookAccount.getBankImporterType().equals(BankImporterType.GEORGE_IMPORTER)) {
			return this.context.getBean(BankImporterGeorgeCSV.class);
		} else if (cashbookAccount.getBankImporterType().equals(BankImporterType.BACA_IMPORTER)) {
		  return this.context.getBean(BankImporterBACACSV.class);
    } else if (cashbookAccount.getBankImporterType().equals(BankImporterType.RAIKA_IMPORTER)) {
		  return this.context.getBean(BankImporterRAIKACSV.class);
    } else if (cashbookAccount.getBankImporterType().equals(BankImporterType.EASYBANK_IMPORTER)) {
		  return this.context.getBean(BankImporterEasyBankCSV.class);
    } else {
			throw new ServiceValidationException(ServiceErrorConstants.unimplementedImporterType, null);
		}
	}

	/**
	 * Creates a new APIImporter.
	 * 
	 * @param cashbookAccount
	 *            The cashbookAccount
	 * @return The APIImporter
	 * @throws BankImporterException
	 *             If the Type is unknown
	 */
	public BankImporterAPI createAPIImporter(CashbookAccount cashbookAccount) {
		if (cashbookAccount.getBankImporterType().equals(BankImporterType.GEORGE_IMPORTER)) {
			return this.context.getBean(BankImporterGeorgeAPI.class);
		} else {
      throw new ServiceValidationException(ServiceErrorConstants.unimplementedImporterType, null);
		}
	}
}
