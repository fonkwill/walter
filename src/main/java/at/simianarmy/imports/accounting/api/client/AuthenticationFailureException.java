package at.simianarmy.imports.accounting.api.client;

public class AuthenticationFailureException extends APIClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticationFailureException(String message) {
		super(message);
	}

}
