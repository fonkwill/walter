package at.simianarmy.imports.accounting.api.client.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeorgeAmount {

	@JsonProperty(value = "value")
	private int value;
	@JsonProperty(value = "precision")
	private int precision;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	@Override
	public String toString() {
		String returnString = "value: " + this.value;
		returnString += ", precision: " + this.precision;
		return returnString;
	}

}
