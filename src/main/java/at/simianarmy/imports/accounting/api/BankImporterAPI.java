package at.simianarmy.imports.accounting.api;

import at.simianarmy.aop.logging.annotation.NoLogging;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterGeneral;

import java.util.Date;
import java.util.List;

public abstract class BankImporterAPI extends BankImporterGeneral {

	public abstract List<BankImportData> readData(@NoLogging String username, @NoLogging String password, Date from,
			Date to, CashbookAccount cashbookAccount, CashbookCategory cashbookCategory) throws BankImporterException;
}
