package at.simianarmy.imports.accounting.api.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;

import java.util.HashMap;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;

public class APIClient {

	private Client client = new Client();
	private HashMap<String, Cookie> cookies = new HashMap<String, Cookie>();

	public APIClient() {
		this.client.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS, false);
		this.client.addFilter(new ClientFilter() {

			@Override
			public ClientResponse handle(ClientRequest request) throws ClientHandlerException {

				ClientResponse response = getNext().handle(request);

				if (response.getCookies() != null) {
					for (Cookie cookie : response.getCookies()) {
						cookies.put(cookie.getName(), cookie);
					}
				}

				return response;
			}
		});
	}

	private WebResource.Builder getWebResourceBuilder(String url, HashMap<String, String> headers) {
		WebResource.Builder builder = client.resource(url).getRequestBuilder();

		for (Cookie cookie : this.cookies.values()) {
			builder.cookie(cookie);
		}

		if (headers != null) {
			for (String key : headers.keySet()) {
				builder.header(key, headers.get(key));
			}
		}

		return builder;
	}

	protected ClientResponse doGetRequest(String url) {
		return this.getWebResourceBuilder(url, null).get(ClientResponse.class);
	}

	protected ClientResponse doGetRequest(String url, HashMap<String, String> headers) {
		return this.getWebResourceBuilder(url, headers).get(ClientResponse.class);
	}

	protected ClientResponse doPostRequest(String url, MultivaluedMap<String, String> parameters) {
		return this.getWebResourceBuilder(url, null).post(ClientResponse.class, parameters);
	}

	protected ClientResponse doPostRequest(String url, MultivaluedMap<String, String> parameters,
			HashMap<String, String> headers) {
		return this.getWebResourceBuilder(url, headers).post(ClientResponse.class, parameters);
	}
}
