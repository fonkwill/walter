package at.simianarmy.imports.accounting.api.client;

import at.simianarmy.aop.logging.annotation.NoLogging;
import at.simianarmy.imports.accounting.api.client.json.GeorgeResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.MultivaluedMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class GeorgeAPIClient extends APIClient {

	private static final String AUTH_URL = "https://login.sparkasse.at/sts/oauth/authorize?client_id=georgeclient&response_type=token";
	private static final String TRANS_URL = "https://api.sparkasse.at/proxy/g/api/my/transactions/export.json?";

	private String username;
	private String password;
	private String authenticationToken;
	private String tokenType;

	private void doAuthentication() throws APIClientException {

		this.doGetRequest(AUTH_URL);

		MultivaluedMap<String, String> map = new MultivaluedMapImpl();

		List<String> javaScriptParameter = new ArrayList<String>();
		javaScriptParameter.add("jsOK");

		map.put("javaScript", javaScriptParameter);

		ClientResponse response = this.doPostRequest(AUTH_URL, map);

		String html = response.getEntity(String.class);

		String exponent = "";
		String modulus = "";
		String saltCode = "";

		Document doc = Jsoup.parse(html);
		Element body = doc.body();
		Elements inputs = body.getElementsByTag("input");

		for (Element input : inputs) {
			if (input.attr("name").equals("exponent")) {
				exponent = input.val();
			}
			if (input.attr("name").equals("modulus")) {
				modulus = input.val();
			}
			if (input.attr("name").equals("saltCode")) {
				saltCode = input.val();
			}
		}

		StringWriter sw = new StringWriter();

		String jsCode = "" + "var navigator = {appName:'Netscape'};" + "var window = {};"
				+ "load('https://login.sparkasse.at/sts/scripts/prng4.js');"
				+ "load('https://login.sparkasse.at/sts/scripts/jsbn.js');"
				+ "load('https://login.sparkasse.at/sts/scripts/rng.js');"
				+ "load('https://login.sparkasse.at/sts/scripts/rsa.js');" + "var random = '" + saltCode + "';"
				+ "var plainText = '" + saltCode + "' + '\t' + '" + password + "';" + "var rsa = new RSAKey();"
				+ "var modulus = '" + modulus + "';" + "var exponent = '" + exponent + "';"
				+ "rsa.setPublic(modulus, exponent);" + "print(rsa.encrypt(plainText));";

		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		engine.getContext().setWriter(sw);
		try {
			engine.eval(jsCode);
		} catch (ScriptException ex) {
			throw new APIClientException("Could not execute Authentication Algorithm!", ex);
		}

		String rsaEncrypted = sw.toString().trim();

		List<String> rsaEncryptedParameter = new ArrayList<String>();
		rsaEncryptedParameter.add(rsaEncrypted);

		List<String> exponentParameter = new ArrayList<String>();
		exponentParameter.add(exponent);

		List<String> usernameParameter = new ArrayList<String>();
		usernameParameter.add(username);

		List<String> passwordParameter = new ArrayList<String>();
		passwordParameter.add(password);

		List<String> modulusParameter = new ArrayList<String>();
		modulusParameter.add(modulus);

		List<String> saltCodeParameter = new ArrayList<String>();
		saltCodeParameter.add(saltCode);

		map.put("exponent", exponentParameter);
		map.put("j_username", usernameParameter);
		map.put("j_password", passwordParameter);
		map.put("modulus", modulusParameter);
		map.put("rsaEncrypted", rsaEncryptedParameter);
		map.put("saltCode", saltCodeParameter);

		response = this.doPostRequest(AUTH_URL, map);

		if (!response.getHeaders().containsKey("Location")) {
			throw new AuthenticationFailureException("Authentication failed!");
		}

		String locationHeader = response.getHeaders().getFirst("Location");

		Pattern pattern = Pattern.compile("access_token=([^&]+)");
		Matcher matcher = pattern.matcher(locationHeader);
		matcher.find();
		this.authenticationToken = matcher.group(1);

		pattern = Pattern.compile("token_type=([^&]+)");
		matcher = pattern.matcher(locationHeader);
		matcher.find();
		this.tokenType = matcher.group(1);
	}

	public List<GeorgeResponse> getTransactions(Date from, Date to) throws APIClientException {
		this.doAuthentication();

		List<GeorgeResponse> result = null;

		try {
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("Authorization", this.tokenType + " " + this.authenticationToken);

			ClientResponse response = this.doPostRequest(TRANS_URL + "from=" + this.formatDateForRequest(from) + "&"
					+ "to=" + this.formatDateForRequest(to) + "&"
					+ "lang=de&fields=booking,receiver,amount,reference,referenceNumber" + "&sort=BOOKING_DATE_ASC",
					null, headers);

			String stringResponse = response.getEntity(String.class);

			ObjectMapper mapper = new ObjectMapper();

			GeorgeResponse[] responseObjects = mapper.readValue(stringResponse, GeorgeResponse[].class);

			result = new ArrayList<GeorgeResponse>(Arrays.asList(responseObjects));
		} catch (JsonParseException ex) {
			throw new APIClientException("Error while Parsing JSON from George API!", ex);
		} catch (JsonMappingException ex) {
			throw new APIClientException("Error while Mapping JSON from George API to Application Object!", ex);
		} catch (IOException ex) {
			throw new APIClientException("Input/Output Error while Reading Data from George API!", ex);
		}
		return result;
	}

	private String formatDateForRequest(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return format.format(date);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(@NoLogging String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(@NoLogging String password) {
		this.password = password;
	}
}
