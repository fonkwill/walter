package at.simianarmy.imports.accounting.api.client;

public class APIClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public APIClientException(String message) {
		super(message);
	}

	public APIClientException(String message, Exception cause) {
		super(message, cause);
	}

}
