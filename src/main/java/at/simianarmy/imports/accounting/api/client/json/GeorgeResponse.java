package at.simianarmy.imports.accounting.api.client.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeorgeResponse {

	@JsonProperty(value = "booking")
	private Date booking;
	@JsonProperty(value = "partnerName")
	private String partnerName;
	@JsonProperty(value = "amount")
	private GeorgeAmount amount;
	@JsonProperty(value = "reference")
	private String reference;
	@JsonProperty(value = "referenceNumber")
	private String referenceNumber;

	public Date getBooking() {
		return booking;
	}

	public void setBooking(Date booking) {
		this.booking = booking;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public GeorgeAmount getAmount() {
		return this.amount;
	}

	public void setAmount(GeorgeAmount amount) {
		this.amount = amount;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@Override
	public String toString() {
		String returnString = "booking: " + this.booking;
		returnString += ", partnerName: " + this.partnerName;
		returnString += ", " + this.amount.toString();
		returnString += ", reference: " + this.reference;
		returnString += ", referenceNumber: " + this.referenceNumber;
		return returnString;
	}

}
