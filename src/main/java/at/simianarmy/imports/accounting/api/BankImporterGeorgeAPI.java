package at.simianarmy.imports.accounting.api;

import at.simianarmy.aop.logging.annotation.NoLogging;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.api.client.APIClientException;
import at.simianarmy.imports.accounting.api.client.GeorgeAPIClient;
import at.simianarmy.imports.accounting.api.client.json.GeorgeResponse;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

@Component
public class BankImporterGeorgeAPI extends BankImporterAPI {

	@Inject
	GeorgeAPIClient client;

	@Override
	public List<BankImportData> readData(@NoLogging String username, @NoLogging String password, Date from, Date to, CashbookAccount cashbookAccount, CashbookCategory cashbookCategory)
			throws BankImporterException {

		this.client.setUsername(username);
		this.client.setPassword(password);

		List<GeorgeResponse> responseData;
		try {
			responseData = this.client.getTransactions(from, to);
		} catch (APIClientException ex) {
			throw new BankImporterException("Failed to get Transactions from API!", ex);
		}
		List<BankImportData> result = new ArrayList<BankImportData>();

		for (GeorgeResponse response : responseData) {
			result.add(this.mapReponseToBankImportData(response, cashbookAccount, cashbookCategory));
		}

		result = this.persistNotImportedBankImportData(result, cashbookAccount);

		return result;
	}

	private BankImportData mapReponseToBankImportData(GeorgeResponse response, CashbookAccount cashbookAccount, CashbookCategory cashbookCategory) {

		BankImportData data = new BankImportData();
		data.setCashbookAccount(cashbookAccount);
		data.setEntryReference(response.getReferenceNumber().getBytes());
		data.setEntryText(this.cutInputString(response.getReference()));
		data.setPartnerName(response.getPartnerName());

		LocalDate entryDate = response.getBooking().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		BigInteger value = BigInteger.valueOf(response.getAmount().getValue());
		int precision = response.getAmount().getPrecision();
		BigDecimal entryValue = new BigDecimal(value, precision);

		data.setEntryDate(entryDate);
		data.setEntryValue(entryValue);
		data.setCashbookCategory(cashbookCategory);

		return data;
	}
}
