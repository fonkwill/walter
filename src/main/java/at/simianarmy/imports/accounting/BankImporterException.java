package at.simianarmy.imports.accounting;

public class BankImporterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BankImporterException(String message) {
		super(message);
	}

	public BankImporterException(String message, Exception cause) {
		super(message, cause);
	}

}
