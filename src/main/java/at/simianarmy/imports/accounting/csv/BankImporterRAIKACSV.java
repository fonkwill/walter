package at.simianarmy.imports.accounting.csv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

@Component
public class BankImporterRAIKACSV extends BankImporterCSVWithoutHeader {

  public static final Integer FIELD_ENTRY_DATE = 0;
  public static final Integer FIELD_ENTRY_VALUE = 3;
  public static final Integer FIELD_ENTRY_TEXT = 1;
  public static final Integer FIELD_TIMESTAMP = 5;
  public static final String CHARSET = "UTF-8";

  @Override
  protected String getCharset() {
    return CHARSET;
  }

  @Override
  protected List<Integer> getFieldsForEntryReference() {
    return new ArrayList<>(Arrays.asList(
      FIELD_TIMESTAMP, FIELD_ENTRY_TEXT, FIELD_ENTRY_VALUE
    ));
  }

  @Override
  protected Boolean isEntryReferenceHashed() {
    return true;
  }

  @Override
  protected Integer getEntryDatePosition() {
    return FIELD_ENTRY_DATE;
  }

  @Override
  protected Integer getEntryValuePosition() {
    return FIELD_ENTRY_VALUE;
  }

  @Override
  protected Integer getEntryTextPosition() {
    return FIELD_ENTRY_TEXT;
  }

  @Override
  protected Integer getPartnerNamePosition() {
    return null;
  }

  @Override
  protected String getEntryPartnerName(CSVRecord record) {
    return null;
  }

  @Override
  protected CSVFormat getFormat() {
    return super.getFormat().withDelimiter(';');
  }
}
