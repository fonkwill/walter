package at.simianarmy.imports.accounting.csv;

import at.simianarmy.domain.enumeration.BankImporterType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.springframework.stereotype.Component;

@Component
public class BankImporterGeorgeCSV extends BankImporterCSVWithHeader {

	public static final String FIELD_ENTRY_DATE = "Buchungsdatum";
	public static final String FIELD_ENTRY_VALUE = "Betrag";
	public static final String FIELD_ENTRY_TEXT = "Buchungstext";
	public static final String FIELD_PARTNER_NAME = "Partnername";
	public static final String FIELD_ENTRY_REFERENCE = "Ersterfassungsreferenz";
	public static final String CHARSET = "UTF-16";

	@Override
	protected String getEntryDateName() {
		return FIELD_ENTRY_DATE;
	}

	@Override
	protected String getEntryValueName() {
		return FIELD_ENTRY_VALUE;
	}

	@Override
	protected String getEntryTextName() {
		return FIELD_ENTRY_TEXT;
	}

	@Override
	protected String getPartnerNameName() {
		return FIELD_PARTNER_NAME;
	}

	@Override
	protected String getCharset() {
		return CHARSET;
	}

  @Override
  protected List<String> getFieldsForEntryReference() {
    return new ArrayList<>(Arrays.asList(FIELD_ENTRY_REFERENCE));
  }

  @Override
  protected Boolean isEntryReferenceHashed() {
    return false;
  }
}
