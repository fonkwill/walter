package at.simianarmy.imports.accounting.csv;

import at.simianarmy.imports.accounting.BankImporterException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public abstract class BankImporterCSVWithoutHeader extends BankImporterCSV {

  protected LocalDate getEntryDate(CSVRecord record) {

    String date = record.get(this.getEntryDatePosition());

    if (date.length() > 10) {
      date = record.get(this.getEntryDatePosition()).substring(1);
    }

    return LocalDate.parse(date,
      DateTimeFormatter.ofPattern("dd.MM.yyyy"));
  }

  protected BigDecimal getEntryValue(CSVRecord record) {
    return new BigDecimal(record.get(this.getEntryValuePosition()).replace(".", "").replace(',', '.'));
  }

  protected String getEntryText(CSVRecord record) {
    return record.get(this.getEntryTextPosition());
  }

  protected String getEntryPartnerName(CSVRecord record) {
    return record.get(this.getPartnerNamePosition());
  }

  protected byte[] getEntryReference(CSVRecord record) throws BankImporterException {

    String entryReference = "";

    for (Integer field : this.getFieldsForEntryReference()) {
      entryReference += record.get(field);
    }

    return this.calculateEntryReference(entryReference);
  }

  protected abstract Integer getEntryDatePosition();

  protected abstract Integer getEntryValuePosition();

  protected abstract Integer getEntryTextPosition();

  protected abstract Integer getPartnerNamePosition();

  protected abstract List<Integer> getFieldsForEntryReference();

  @Override
  protected CSVFormat getFormat() {
    return CSVFormat.DEFAULT;
  }

}
