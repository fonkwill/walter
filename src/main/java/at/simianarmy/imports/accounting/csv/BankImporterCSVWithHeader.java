package at.simianarmy.imports.accounting.csv;

import at.simianarmy.imports.accounting.BankImporterException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public abstract class BankImporterCSVWithHeader extends BankImporterCSV {

  protected LocalDate getEntryDate(CSVRecord record) {
    return LocalDate.parse(record.get(this.getEntryDateName()),
      DateTimeFormatter.ofPattern("dd.MM.yyyy"));
  }

  protected BigDecimal getEntryValue(CSVRecord record) {
    return new BigDecimal(record.get(this.getEntryValueName()).replace(".", "").replace(',', '.'));
  }

  protected String getEntryText(CSVRecord record) {
    return record.get(this.getEntryTextName());
  }

  protected String getEntryPartnerName(CSVRecord record) {
    return record.get(this.getPartnerNameName());
  }

  protected byte[] getEntryReference(CSVRecord record) throws BankImporterException {

    String entryReference = "";

    for (String field : this.getFieldsForEntryReference()) {
      entryReference += record.get(field);
    }

    return this.calculateEntryReference(entryReference);
  }

  @Override
  protected CSVFormat getFormat() {
    return CSVFormat.DEFAULT.withHeader();
  }

  protected abstract String getEntryDateName();

  protected abstract String getEntryValueName();

  protected abstract String getEntryTextName();

  protected abstract String getPartnerNameName();

  protected abstract List<String> getFieldsForEntryReference();


}
