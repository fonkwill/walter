package at.simianarmy.imports.accounting.csv;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterGeneral;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

public abstract class BankImporterCSV extends BankImporterGeneral {

	/**
	 * Reads Data from CSV-File and Persists Data to DB.
	 * 
	 * @return The List of persisted BankImportData
	 * @throws BankImporterException
	 *             If CSV-File is not set or specific Exception occurs
	 */
	public List<BankImportData> readData(MultipartFile csvFile, CashbookAccount cashbookAccount, CashbookCategory cashbookCategory) throws BankImporterException {

		CSVParser csvFileParser = null;

		try {
			Reader in = new InputStreamReader(csvFile.getInputStream(), this.getCharset());
			csvFileParser = new CSVParser(in, this.getFormat());

			List<BankImportData> data = new ArrayList<BankImportData>();
			List<CSVRecord> csvRecords = csvFileParser.getRecords();

			for (CSVRecord record : csvRecords) {
				BankImportData importData = new BankImportData();

				try {
					LocalDate entryDate = this.getEntryDate(record);
					BigDecimal entryValue = this.getEntryValue(record);
					String entryText = this.getEntryText(record);
					byte[] entryReference = this.getEntryReference(record);
					String partnerName = this.getEntryPartnerName(record);

					importData.setEntryDate(entryDate);
					importData.setEntryValue(entryValue);
					importData.setEntryText(this.cutInputString(entryText));
					importData.setPartnerName(partnerName);
					importData.setEntryReference(entryReference);
					importData.setCashbookAccount(cashbookAccount);
					importData.setCashbookCategory(cashbookCategory);

				} catch (IllegalArgumentException ex) {
					throw new BankImporterException("CSV-Format is invalid!", ex);
				}

				data.add(importData);
			}

			csvFileParser.close();

			data = this.persistNotImportedBankImportData(data, cashbookAccount);

			return data;
		} catch (FileNotFoundException ex) {
			throw new BankImporterException("The file could not be found!", ex);
		} catch (IOException ex) {
			throw new BankImporterException("The CSV-File could not be parsed!", ex);
		} finally {
			try {
				if (csvFileParser != null) {
					csvFileParser.close();
				}
			} catch (IOException ex) {
				throw new BankImporterException("The CSV-FileParser could not be closed!", ex);
			}
		}
	}

	protected byte[] calculateEntryReference(String entryReference) throws BankImporterException {
	  if (this.isEntryReferenceHashed()) {
      try {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(entryReference.getBytes());
        byte[] digest = md.digest();
        return digest;
      } catch (NoSuchAlgorithmException e) {
        throw new BankImporterException("Could not Hash the EntryReference!", e);
      }
    }

    return entryReference.getBytes();
  }

	protected abstract LocalDate getEntryDate(CSVRecord record);

	protected abstract BigDecimal getEntryValue(CSVRecord record);

	protected abstract String getEntryText(CSVRecord record);

	protected abstract String getEntryPartnerName(CSVRecord record);

	protected abstract byte[] getEntryReference(CSVRecord record) throws BankImporterException;

	protected abstract CSVFormat getFormat();

	protected abstract String getCharset();

  protected abstract Boolean isEntryReferenceHashed();
}
