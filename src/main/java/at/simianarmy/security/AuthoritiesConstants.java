package at.simianarmy.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

	public static final String ADMIN = "ROLE_ADMIN";

	public static final String USER = "ROLE_USER";

	public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	public static final String BOARD_MEMBER = "ROLE_BOARD_MEMBER";

	public static final String MEMBER_ADMIN = "ROLE_MEMBER_ADMIN";

	public static final String INSTRUMENT_ADMIN = "ROLE_INSTRUMENT_ADMIN";

	public static final String CLOTHING_ADMIN = "ROLE_CLOTHING_ADMIN";

	public static final String AWARD_ADMIN = "ROLE_AWARD_ADMIN";

	public static final String HONOR_ADMIN = "ROLE_HONOR_ADMIN";

	public static final String COMPOSITION_ADMIN = "ROLE_COMPOSITION_ADMIN";

	public static final String ACCOUNTING_ADMIN = "ROLE_ACCOUNTING_ADMIN";

	public static final String ACCOUNTING_CONTROLLER = "ROLE_MEMBER_CONTROLLER";

	private AuthoritiesConstants() {
	}
}
