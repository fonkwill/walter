package at.simianarmy.security.jwt;

import at.simianarmy.config.JHipsterProperties;
import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class TokenProvider {

	private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

	private static final String AUTHORITIES_KEY = "auth";

	private String secretKey;

	private long tokenValidityInSeconds;

	private long tokenValidityInSecondsForRememberMe;

	@Inject
	private JHipsterProperties jHipsterProperties;
	
	@Inject
	private WalterProperties walterProperties;
	
	@Inject
	private TenantConfigurationRepository tenantConfigurationRepository;
	
	@Inject
	private TenantIdentiferResolver tenantIdentiferResolver;

	@PostConstruct
	public void init() {
		this.secretKey = jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();

		this.tokenValidityInSeconds = 1000
				* jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSeconds();
		this.tokenValidityInSecondsForRememberMe = 1000 * jHipsterProperties.getSecurity().getAuthentication().getJwt()
				.getTokenValidityInSecondsForRememberMe();
	}

	public String createToken(Authentication authentication, Boolean rememberMe) {
		String authorities = authentication.getAuthorities().stream().map(authority -> authority.getAuthority())
				.collect(Collectors.joining(","));

		String individualSecretKey = getTenantSecretKey();
		
		long now = (new Date()).getTime();
		Date validity;
		if (rememberMe) {
			validity = new Date(now + this.tokenValidityInSecondsForRememberMe);
		} else {
			validity = new Date(now + this.tokenValidityInSeconds);
		}

		return Jwts.builder().setSubject(authentication.getName()).claim(AUTHORITIES_KEY, authorities)
				.signWith(SignatureAlgorithm.HS512, individualSecretKey).setExpiration(validity).compact();
	}

	private String getTenantSecretKey() {
		if (walterProperties.getMultitenancy().isEnabled()) {
			String tenantId = tenantIdentiferResolver.resolveCurrentTenantIdentifier();
			if (tenantId.equals(TenantIdentiferResolver.MASTER_TENANT)) {
				return this.secretKey;
			}
			TenantConfiguration tenantConfig = tenantConfigurationRepository.getTenantConfigurationForTenant(tenantIdentiferResolver.resolveCurrentTenantIdentifier());
			if (tenantConfig != null) {
				return tenantConfig.getJwtSecret();
			}
			return null;
		} else {
			return this.secretKey;
		}
	}

	public Authentication getAuthentication(String token) {
		String individualSecretKey = getTenantSecretKey();
		
		Claims claims = Jwts.parser().setSigningKey(individualSecretKey).parseClaimsJws(token).getBody();

		Collection<? extends GrantedAuthority> authorities = Arrays
				.asList(claims.get(AUTHORITIES_KEY).toString().split(",")).stream()
				.map(authority -> new SimpleGrantedAuthority(authority)).collect(Collectors.toList());

		User principal = new User(claims.getSubject(), "", authorities);

		return new UsernamePasswordAuthenticationToken(principal, "", authorities);
	}

	public boolean validateToken(String authToken) {
		String individualSecretKey = getTenantSecretKey();
		try {
			Jwts.parser().setSigningKey(individualSecretKey).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException exception) {
			log.info("Invalid JWT signature: " + exception.getMessage());
			return false;
		}
	}
}
