package at.simianarmy.security;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.domain.User;
import at.simianarmy.multitenancy.TenantContext;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.service.CustomizationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

	@Inject
	private UserRepository userRepository;
	
	@Inject
	private CustomizationService customizationService;
	
	@Inject
	private AuthConfigRepository authConfigRepository;

	@Inject
  private WalterProperties walterProperties;

	@Inject
  private TenantIdentiferResolver tenantIdentiferResolver;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String login) {
		log.debug("Authenticating {}", login);
		String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
		Optional<User> userFromDatabase = userRepository.findOneByLogin(lowercaseLogin);
		return userFromDatabase.map(user -> {
			if (!user.getActivated()) {
				throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
			}
			List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
			
			
			List<String> deactivatedFeatureGroups = customizationService.getDeactivatedFeatureGroups();
			
			List<String> deactivatedFeatures = authConfigRepository.getAllAuthoritiesForFeatureGroups(deactivatedFeatureGroups);

      if(!walterProperties.getMultitenancy().isEnabled() || !tenantIdentiferResolver.resolveCurrentTenantIdentifier().equals(TenantIdentiferResolver.MASTER_TENANT)) {
        deactivatedFeatures.add("TENANT_CONFIGURATION");
      }

			for (Role role : user.getRoles()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
								
				grantedAuthorities.addAll(
						role.getAuthorities().stream().
						filter(authority -> !deactivatedFeatures.contains(authority.getName())).
						map(authority -> new SimpleGrantedAuthority(authority.getGrantedAuthRepr())).
						collect(Collectors.toList())
						);
			}
			

			return new org.springframework.security.core.userdetails.User(lowercaseLogin, user.getPassword(),
					grantedAuthorities);
		}).orElseThrow(
				() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " + "database"));
	}
}
