package at.simianarmy.repository;

import at.simianarmy.domain.MembershipFeeAmount;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the MembershipFeeAmount entity.
 */
@SuppressWarnings("unused")
public interface MembershipFeeAmountRepository extends JpaRepository<MembershipFeeAmount, Long> {

	@Query("SELECT membershipFeeAmount FROM MembershipFeeAmount membershipFeeAmount "
			+ "WHERE membershipFeeAmount.membershipFee.id = :membershipFeeId")
	Page<MembershipFeeAmount> findByMembershipFee(@Param("membershipFeeId") Long membershipFeeId, Pageable pageable);

	@Query("SELECT membershipFeeAmount FROM MembershipFeeAmount membershipFeeAmount "
			+ "WHERE membershipFeeAmount.membershipFee.id = :membershipFeeId AND ("
			+ "(YEAR(membershipFeeAmount.beginDate) <= :year AND " + "YEAR(membershipFeeAmount.endDate) >= :year) "
			+ "OR " + "(YEAR(membershipFeeAmount.beginDate) <= :year AND " + "membershipFeeAmount.endDate IS NULL))")
	MembershipFeeAmount findByMembershipFeeAndYear(@Param("membershipFeeId") Long membershipFeeId,
			@Param("year") Integer year);

}
