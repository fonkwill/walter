package at.simianarmy.repository;

import at.simianarmy.domain.MembershipFee;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the MembershipFee entity.
 */
@SuppressWarnings("unused")
public interface MembershipFeeRepository extends JpaRepository<MembershipFee, Long> {

  MembershipFee findFirstByDescription(String description);

}
