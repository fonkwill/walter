package at.simianarmy.repository;

import at.simianarmy.domain.Membership;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Membership entity.
 */
@SuppressWarnings("unused")
public interface MembershipRepository extends JpaRepository<Membership, Long>, JpaSpecificationExecutor<Membership> {

	@Query("SELECT membership FROM Membership membership " + "WHERE membership.person.id = :personId")
	Page<Membership> findByPerson(@Param("personId") Long personId, Pageable pageable);

	@Query("SELECT membership FROM Membership membership " + "WHERE membership.person.id = :personId "
			+ "AND :year >= YEAR(membership.beginDate)" + "AND (:year <= YEAR(membership.endDate) "
			+ "OR membership.endDate IS NULL)")
	List<Membership> findByPersonAndYear(@Param("personId") Long personId, @Param("year") int year);

	/**
	 * Finds the memberships for a person at a given date
	 * 
	 * @param personId
	 *            The person
	 * @param dueDate
	 *            Date to check the membership at
	 * @return List of memberships of this person at the given date.
	 */
	@Query("SELECT membership FROM Membership membership " + "WHERE membership.person.id = :personId "
			+ "AND :dueDate >= membership.beginDate " + "AND (:dueDate <= membership.endDate "
			+ "OR membership.endDate IS NULL)")
	List<Membership> findByPersonAndDueDate(@Param("personId") Long personId, @Param("dueDate") LocalDate dueDate);

	/**
	 * Finds all memberships for at a given date
	 * 
	 * @param dueDate
	 *            Date to check the membership at
	 * @return List of memberships of all persons at the given date.
	 */
	@Query("SELECT membership FROM Membership membership " + "WHERE :dueDate >= membership.beginDate "
			+ "AND (:dueDate <= membership.endDate " + "OR membership.endDate IS NULL)")
	List<Membership> findByDueDate(@Param("dueDate") LocalDate dueDate);

	@Query("SELECT membership FROM Membership membership " + "WHERE membership.person.id = :personId")
	List<Membership> findByPerson(@Param("personId") Long personId);

}
