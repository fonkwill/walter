package at.simianarmy.repository;

import at.simianarmy.domain.CashbookCategory;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the CashbookCategory entity.
 */
@SuppressWarnings("unused")
public interface CashbookCategoryRepository extends JpaRepository<CashbookCategory, Long> {

}
