package at.simianarmy.repository;

import at.simianarmy.domain.ColorCode;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the ColorCode entity.
 */
@SuppressWarnings("unused")
public interface ColorCodeRepository extends JpaRepository<ColorCode, Long> {

}
