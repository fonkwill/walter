package at.simianarmy.repository;

import at.simianarmy.domain.Clothing;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Clothing entity.
 */
@SuppressWarnings("unused")
public interface ClothingRepository extends JpaRepository<Clothing, Long>, JpaSpecificationExecutor<Clothing> {

	@Query("select distinct c.size, ct.id from Clothing c LEFT JOIN c.type ct")
	public List<Object[]> getAllDistinctValues();
}
