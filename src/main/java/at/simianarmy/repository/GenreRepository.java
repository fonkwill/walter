package at.simianarmy.repository;

import at.simianarmy.domain.Genre;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Genre entity.
 */
@SuppressWarnings("unused")
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
