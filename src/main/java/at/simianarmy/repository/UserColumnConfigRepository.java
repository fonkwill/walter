package at.simianarmy.repository;

import at.simianarmy.domain.ColumnConfig;
import at.simianarmy.domain.User;
import at.simianarmy.domain.UserColumnConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the UserColumnConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserColumnConfigRepository extends JpaRepository<UserColumnConfig, Long> {

    @Query("select user_column_config from UserColumnConfig user_column_config where user_column_config.user.login = ?#{principal.username}")
    List<UserColumnConfig> findByUserIsCurrentUser();

    @Query("select user_column_config from UserColumnConfig user_column_config "
      + "where user_column_config.user.login = ?#{principal.username} "
      + "AND user_column_config.columnConfig.entity = :entity "
      + "ORDER BY user_column_config.position ASC")
    List<UserColumnConfig> findByEntityAndUserIsCurrentUser(@Param("entity") String entity);

    @Query("select user_column_config from UserColumnConfig user_column_config "
      + "where user_column_config.user.login = ?#{principal.username} "
      + "AND user_column_config.columnConfig.entity = :entity AND user_column_config.visible = true "
      + "ORDER BY user_column_config.position ASC")
    List<UserColumnConfig> findByEntityAndVisibleAndUserIsCurrentUser(@Param("entity") String entity);

    List<UserColumnConfig> findByUser(User user);

    Boolean existsByColumnConfigAndUser(ColumnConfig columnConfig, User user);

}
