package at.simianarmy.repository;

import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composition;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the Composition entity.
 */
public interface CompositionRepository extends JpaRepository<Composition, Long>, JpaSpecificationExecutor<Composition> {

	@Query("select distinct composition from Composition composition " + "left join fetch composition.appointments")
	List<Composition> findAllWithEagerRelationships();

	@Query("select composition from Composition composition "
			+ "left join fetch composition.appointments where composition.id =:id")
	Composition findOneWithEagerRelationships(@Param("id") Long id);

	@Query("select composition from Composition composition "
			+ "left join composition.appointments appointment WHERE appointment.id = " + ":officialAppointmentId")
	Page<Composition> findByOfficialAppointment(@Param("officialAppointmentId") Long appointmentId, Pageable pageable);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Query("select (CASE WHEN max(nr) IS NOT NULL THEN max(nr) ELSE 0 END) + " + "1 from Composition composition "
			+ "where composition.colorCode = :colorCode")
	Integer getNextNrForColorCode(@Param("colorCode") ColorCode colorCode);

}
