package at.simianarmy.repository;

import at.simianarmy.domain.PersonClothing;
import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the PersonClothing entity.
 */
@SuppressWarnings("unused")
public interface PersonClothingRepository
		extends JpaRepository<PersonClothing, Long>, JpaSpecificationExecutor<PersonClothing> {

}
