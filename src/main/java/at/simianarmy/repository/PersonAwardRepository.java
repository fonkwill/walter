package at.simianarmy.repository;

import at.simianarmy.domain.PersonAward;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PersonAward entity.
 */
@SuppressWarnings("unused")
public interface PersonAwardRepository extends JpaRepository<PersonAward, Long>, JpaSpecificationExecutor<PersonAward> {

	@Query("SELECT personAward FROM PersonAward personAward " + "WHERE personAward.award.id = :awardId")
	Page<PersonAward> findByAward(@Param("awardId") Long awardId, Pageable pageable);
}
