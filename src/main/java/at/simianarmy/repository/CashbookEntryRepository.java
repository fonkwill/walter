package at.simianarmy.repository;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the CashbookEntry entity.
 */
public interface CashbookEntryRepository
		extends JpaRepository<CashbookEntry, Long>, JpaSpecificationExecutor<CashbookEntry> {

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date < :maxDate AND "
    + "(c.cashbookAccount.id IN :cashbookAccountIds OR :cashbookAccountIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
	BigDecimal getIncomeSumForMaxDateAndCashbookAccountId(@Param("maxDate") LocalDate date, @Param("cashbookAccountIds") List<Long> cashbookAccountIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date < :maxDate AND "
    + "(c.cashbookAccount.id IN :cashbookAccountIds OR :cashbookAccountIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForMaxDateAndCashbookAccountId(@Param("maxDate") LocalDate date, @Param("cashbookAccountIds") List<Long> cashbookAccountIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookAccount.id IN :cashbookAccountIds OR :cashbookAccountIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
  BigDecimal getIncomeSumForMinAndMaxDateAndCashbookAccountId(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate, @Param("cashbookAccountIds") List<Long> cashbookAccountIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookAccount.id IN :cashbookAccountIds OR :cashbookAccountIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForMinAndMaxDateAndCashbookAccountId(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate, @Param("cashbookAccountIds") List<Long> cashbookAccountIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookCategory.id IN :cashbookCategoryIds OR :cashbookCategoryIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
  BigDecimal getIncomeSumForMinAndMaxDateAndCashbookCategoryId(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate, @Param("cashbookCategoryIds") List<Long> cashbookCategoryIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookCategory.id IN :cashbookCategoryIds OR :cashbookCategoryIds IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForMinAndMaxDateAndCashbookCategoryId(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate, @Param("cashbookCategoryIds") List<Long> cashbookCategoryIds);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookCategory.id IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
  BigDecimal getIncomeSumForMinAndMaxDateWithoutCategory(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate);

  @Query("select sum(c.amount) from CashbookEntry c where "
    + "c.date >= :minDate AND "
    + "c.date < :maxDate AND "
    + "(c.cashbookCategory.id IS NULL) AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForMinAndMaxDateWithoutCategory(@Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate);

  @Query("select distinct cat FROM CashbookEntry c JOIN c.cashbookCategory cat WHERE c.appointment = :appointment ORDER BY cat.name")
  List<CashbookCategory> getCashbookCategoriesForAppointment(@Param("appointment") Appointment appointment);

  @Query("select sum(c.amount) FROM CashbookEntry c "
    + "WHERE c.appointment = :appointment AND "
    + "c.cashbookCategory = :cashbookCategory AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
  BigDecimal getIncomeSumForAppointmentAndCashbookCategory(@Param("appointment") Appointment appointment, @Param("cashbookCategory") CashbookCategory cashbookCategory);

  @Query("select sum(c.amount) FROM CashbookEntry c "
    + "WHERE c.appointment = :appointment AND "
    + "c.cashbookCategory = :cashbookCategory AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForAppointmentAndCashbookCategory(@Param("appointment") Appointment appointment, @Param("cashbookCategory") CashbookCategory cashbookCategory);

  @Query("select sum(c.amount) FROM CashbookEntry c "
    + "WHERE c.appointment = :appointment AND "
    + "c.cashbookCategory.id is null AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.INCOME")
  BigDecimal getIncomeSumForAppointmentWithNoCashbookCategory(@Param("appointment") Appointment appointment);

  @Query("select sum(c.amount) FROM CashbookEntry c "
    + "WHERE c.appointment = :appointment AND "
    + "c.cashbookCategory.id is null AND "
    + "c.type is at.simianarmy.domain.enumeration.CashbookEntryType.SPENDING")
  BigDecimal getSpendingSumForAppointmentWithNoCashbookCategory(@Param("appointment") Appointment appointment);

}
