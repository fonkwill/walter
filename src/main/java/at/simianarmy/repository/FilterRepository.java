package at.simianarmy.repository;

import at.simianarmy.domain.Filter;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Filter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FilterRepository extends JpaRepository<Filter, Long> {

  Boolean existsByListNameAndName(String listName, String name);

  List<Filter> findByListName(String listName);

}
