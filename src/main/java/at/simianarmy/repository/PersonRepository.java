package at.simianarmy.repository;

import at.simianarmy.domain.Person;

import java.time.LocalDate;
import java.util.List;

import org.springframework.cglib.core.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Person entity.
 */
@SuppressWarnings("unused")
public interface PersonRepository extends JpaRepository<Person, Long>, JpaSpecificationExecutor<Person> {

	@Query("select distinct person from Person person left join fetch person.personGroups")
	List<Person> findAllWithEagerPersonGroupRelations();

	@Query("select person from Person person " + "left join fetch person.personGroups where person.id =:id")
	Person findOneWithEagerPersonGroupRelations(@Param("id") Long id);

	@Query("select person from Person person "
			+ "left join person.personGroups personGroup where personGroup.id in :personGroupIds")
	Page<Person> findByPersonGroups(@Param("personGroupIds") List<Long> personGroupIds, Pageable pageable);

	@Query("select person from Person person "
			+ "left join person.appointments appointment WHERE appointment.id = :appointmentId")
	Page<Person> findByAppointment(@Param("appointmentId") Long appointmentId, Pageable pageable);

	@Query("select distinct person from Person person " + "join person.memberships membership "
			+ "join membership.membershipFeeForPeriods membershipFeeForPeriod "
			+ "where membershipFeeForPeriod.referenceCode is not null"
			+ "  and membershipFeeForPeriod.year = year(current_date)")
	Page<Person> findAllWithCurrentFees(Pageable pageable);

	@Query("select distinct person from Person person " + "join person.memberships membership "
			+ "join membership.membershipFeeForPeriods membershipFeeForPeriod "
			+ "where membershipFeeForPeriod.referenceCode is not null "
			+ "  and (membershipFeeForPeriod.paid = false or membershipFeeForPeriod.paid is null) "
			+ "  and membershipFeeForPeriod.year between :yearFrom and :yearTo "
			+ " and (membership.endDate >= current_date or membership.endDate is null) "
      + " and person.hasDirectDebit = false")
	List<Person> findAllWithUnpaidFeesAndNoDirectDebit(@Param("yearFrom") int yearFrom, @Param("yearTo") int yearTo);

	@Query("select distinct person from Person person " + "join person.memberships membership "
			+ "join membership.membershipFeeForPeriods membershipFeeForPeriod "
			+ "where membershipFeeForPeriod.referenceCode is not null "
			+ "  and (membershipFeeForPeriod.paid = false or membershipFeeForPeriod.paid is null) "
			+ "  and membershipFeeForPeriod.year between :yearFrom and :yearTo"
			+ " and (membership.endDate >= current_date or membership.endDate is null)")
	Page<Person> findAllWithUnpaidFeesAsPage(@Param("yearFrom") int yearFrom, @Param("yearTo") int yearTo,
			Pageable pageable);

	@Query("select distinct person from Person person JOIN person.emailVerificationLinks emailVerificationLink "
    + " LEFT JOIN person.personGroups personGroup "
    + " WHERE emailVerificationLink.validationDate IS NULL AND emailVerificationLink.invalid = false AND (personGroup.id IN :personGroupIds OR :personGroupIds IS NULL)")
	List<Person> findAllByPersonGroupsAndEmailVerificationLinksToVerify(@Param("personGroupIds") List<Long> personGroupIds);

  @Query(
    "SELECT distinct p FROM Person p LEFT JOIN p.personGroups g "
      + "WHERE (g.id IN :personGroupIds OR :personGroupIds IS NULL) "
      + "AND p.emailType = 'VERIFIED' AND p.email IS NOT NULL AND p.email <> '' "
      + "AND EXISTS "
      + "(SELECT l FROM EmailVerificationLink l JOIN l.person p1 WHERE p1 = p AND l.invalid = false AND l.validationDate IS NOT NULL AND l.creationDate = ("
      + "SELECT max(l1.creationDate) FROM EmailVerificationLink l1 JOIN l1.person p2 WHERE p2 = p1"
      + ")) ORDER BY p.email"
  )
  List<Person> findAllByActiveEmailAndPersonGroups(@Param("personGroupIds") List<Long> personGroupIds);

  @Query(
    "SELECT distinct p FROM Person p LEFT JOIN p.personGroups g "
      + "WHERE (g.id IN :personGroupIds OR :personGroupIds IS NULL) "
      + "AND p.emailType = 'MANUALLY' "
      + "OR (p.emailType = 'VERIFIED' "
      + "AND NOT EXISTS "
      + "(SELECT l FROM EmailVerificationLink l JOIN l.person p1 WHERE p1 = p AND l.invalid = false AND l.validationDate IS NOT NULL AND l.creationDate = ("
      + "SELECT max(l1.creationDate) FROM EmailVerificationLink l1 JOIN l1.person p2 WHERE p2 = p1"
      + "))) ORDER BY p.email"
  )
  List<Person> findAllByInActiveEmailAndPersonGroups(@Param("personGroupIds") List<Long> personGroupIds);


  /**
   * Returns all persons which are currently not marked as "not relevant" but don't have an active membership or at least
   * one of the given persongroups
   * @param dueDate The date for checking the active memberships
   * @param relevantPersonGroupIds List of person-group ids for the check
   * @return A list of persons for which the given constraints are fulfilled
   */
	@Query(
	  "select p from Person p " +
    "left join fetch p.memberships ms " +
   " where p.dateLostRelevance is null " +
    " and p.id not in " +
    //active memberships or given persongroups
    "(" +
      " select pe.id from Person pe " +
      " left join  pe.personGroups persongroup " +
      " left join  pe.memberships membership " +
        " where ( " +
          " ( membership.beginDate is not null and membership.beginDate <= :dueDate and (membership.endDate is NULL or membership.endDate >= :dueDate ) ) " +
            " or " +
          " ( persongroup.id in :relevantPersonGroupIds )" +
        " ) " +
    ")"
  )
	List<Person> findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(@Param("dueDate") LocalDate dueDate, @Param("relevantPersonGroupIds") List<Long> relevantPersonGroupIds);

  /**
   * Returns all persons which are currently marked as "not relevant" but have an active membership or at least
   * one of the given persongroups
   * @param dueDate The date for checking the active memberships
   * @param relevantPersonGroupIds List of person-group ids for the check
   * @return A list of persons for which the given constraints are fulfilled
   */
  @Query(
    "select distinct p from Person p " +
      " where p.dateLostRelevance is not null " +
      " and p.id in " +
      //active memberships or given persongroups
      "(" +
        " select pe.id from Person pe " +
        " left join  pe.personGroups persongroup " +
        " left join  pe.memberships membership " +
          " where ( " +
            " ( membership.beginDate is not null and membership.beginDate <= :dueDate and ( membership.endDate is NULL or membership.endDate >= :dueDate ) ) " +
            " or " +
            " ( persongroup.id in :relevantPersonGroupIds )" +
          " )" +
      ")"
  )
  List<Person> findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(@Param("dueDate") LocalDate dueDate, @Param("relevantPersonGroupIds") List<Long> relevantPersonGroupIds);


  List<Person> findAllByDateLostRelevanceIsBefore(LocalDate deleteDate);

  @Query(
    "select person from Person person " +
    "left join fetch person.personGroups  " +
    "left join fetch person.personInstruments pi " +
      "left join fetch pi.instrument " +
    "left join fetch person.personClothings pc " +
      "left join fetch pc.clothing " +
    "left join fetch person.personAwards pa "+
      "left join fetch pa.award " +
    "left join fetch person.personHonors ph " +
      "left join fetch ph.honor " +
    "left join fetch person.appointments ap " +
      "left join fetch ap.type " +
    "left join fetch person.memberships m " +
      "left join fetch m.membershipFeeForPeriods mfp " +
        "left join fetch mfp.cashbookEntry mfpCp " +
          "left join fetch mfpCp.bankImportData " +
    "where person.id =:id ")
  Person findOneWithAllEagerRelations(@Param("id") Long id);
}
