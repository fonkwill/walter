package at.simianarmy.repository;

import at.simianarmy.domain.Appointment;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Appointment entity.
 */
public interface AppointmentRepository extends JpaRepository<Appointment, Long>, JpaSpecificationExecutor<Appointment> {

	@Query("SELECT DISTINCT appointment " + "FROM Appointment appointment LEFT JOIN FETCH appointment.persons")
	List<Appointment> findAllWithEagerRelationships();

	@Query("SELECT appointment " + "FROM Appointment appointment LEFT JOIN FETCH appointment.persons "
			+ "WHERE appointment.id =:id")
	Appointment findOneWithEagerRelationships(@Param("id") Long id);

	@Query("SELECT a FROM Appointment a WHERE "
    + "a.type.id = :appointmentTypeId AND "
    + "cast(a.beginDate as LocalDate) >= :minDate AND "
    + "cast(a.endDate as LocalDate) <= :maxDate ORDER BY a.beginDate ASC")
	List<Appointment> findByAppointmentTypeAndMinAndMaxDate(@Param("appointmentTypeId") Long appointmentTypeId, @Param("minDate") LocalDate minDate, @Param("maxDate") LocalDate maxDate);

}
