package at.simianarmy.repository;

import at.simianarmy.domain.PersonInstrument;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PersonInstrument entity.
 */
@SuppressWarnings("unused")
public interface PersonInstrumentRepository
		extends JpaRepository<PersonInstrument, Long>, JpaSpecificationExecutor<PersonInstrument> {

	@Query("SELECT personInstrument FROM PersonInstrument personInstrument "
			+ "WHERE personInstrument.instrument.id = :instrumentId")
	Page<PersonInstrument> findByInstrument(@Param("instrumentId") Long instrumentId, Pageable pageable);

}
