package at.simianarmy.repository;

import at.simianarmy.domain.MusicBook;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the MusicBook entity.
 */
@SuppressWarnings("unused")
public interface MusicBookRepository extends JpaRepository<MusicBook, Long> {

}
