package at.simianarmy.repository;

import at.simianarmy.domain.NotificationRule;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the NotificationRule entity.
 */
@SuppressWarnings("unused")
public interface NotificationRuleRepository extends JpaRepository<NotificationRule, Long> {

	@Query("select distinct notificationRule from NotificationRule notificationRule "
			+ "left join fetch notificationRule.targetAudiences")
	List<NotificationRule> findAllWithEagerRelationships();

	@Query("select notificationRule from NotificationRule notificationRule "
			+ "left join fetch notificationRule.targetAudiences where notificationRule.id =:id")
	NotificationRule findOneWithEagerRelationships(@Param("id") Long id);

	@Query("select distinct notificationRule from NotificationRule notificationRule " + "where active = true")
	List<NotificationRule> findAllActive();

}
