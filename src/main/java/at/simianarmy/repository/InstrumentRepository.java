package at.simianarmy.repository;

import at.simianarmy.domain.Instrument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Instrument entity.
 */
@SuppressWarnings("unused")
public interface InstrumentRepository extends JpaRepository<Instrument, Long>, JpaSpecificationExecutor<Instrument> {

	@Query("SELECT instrument FROM Instrument instrument " + "WHERE instrument.type.id = :typeId")
	Page<Instrument> findByType(@Param("typeId") Long typeId, Pageable pageable);
}
