package at.simianarmy.repository;

import at.simianarmy.domain.Award;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Award entity.
 */
@SuppressWarnings("unused")
public interface AwardRepository extends JpaRepository<Award, Long> {

}
