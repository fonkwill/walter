package at.simianarmy.repository;

import at.simianarmy.domain.Template;

import org.springframework.data.jpa.repository.*;

/**
 * Spring Data JPA repository for the Template entity.
 */
@SuppressWarnings("unused")
public interface TemplateRepository extends JpaRepository<Template, Long> {

}
