package at.simianarmy.repository;

import at.simianarmy.domain.Composer;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Composer entity.
 */
public interface ComposerRepository extends JpaRepository<Composer, Long> {

	@Query("select distinct composer from Composer composer " + "left join fetch composer.compositions")
	List<Composer> findAllWithEagerRelationships();

	@Query("select composer from Composer composer " + "left join fetch composer.compositions where composer.id =:id")
	Composer findOneWithEagerRelationships(@Param("id") Long id);

	@Query("SELECT composer from Composer composer "
			+ "LEFT JOIN composer.compositions composition WHERE composition.id = :compositionId")
	Page<Composer> findByComposition(@Param("compositionId") Long compositionId, Pageable pageable);

	@Query("SELECT composer from Composer composer "
			+ "LEFT JOIN composer.compositions composition WHERE composition.id = :compositionId")
	List<Composer> findByComposition(@Param("compositionId") Long compositionId);
}
