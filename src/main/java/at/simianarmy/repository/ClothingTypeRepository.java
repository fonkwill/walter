package at.simianarmy.repository;

import at.simianarmy.domain.ClothingType;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the ClothingType entity.
 */
@SuppressWarnings("unused")
public interface ClothingTypeRepository extends JpaRepository<ClothingType, Long> {

}
