package at.simianarmy.repository;

import at.simianarmy.domain.Report;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Report entity.
 */
@SuppressWarnings("unused")
public interface ReportRepository extends JpaRepository<Report, Long> {

}
