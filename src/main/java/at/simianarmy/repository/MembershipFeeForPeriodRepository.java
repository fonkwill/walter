package at.simianarmy.repository;

import at.simianarmy.domain.MembershipFeeForPeriod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the MembershipFeeForPeriod entity.
 */
@SuppressWarnings("unused")
public interface MembershipFeeForPeriodRepository extends JpaRepository<MembershipFeeForPeriod, Long> {

	@Query("SELECT membershipFeeForPeriod FROM MembershipFeeForPeriod membershipFeeForPeriod WHERE "
			+ "membershipFeeForPeriod.referenceCode = :referenceCode")
	public MembershipFeeForPeriod findByReferenceCode(@Param("referenceCode") String referenceCode);

}
