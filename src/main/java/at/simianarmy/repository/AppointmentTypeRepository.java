package at.simianarmy.repository;

import at.simianarmy.domain.AppointmentType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the AppointmentType entity.
 */
@SuppressWarnings("unused")
public interface AppointmentTypeRepository extends JpaRepository<AppointmentType, Long> {

}
