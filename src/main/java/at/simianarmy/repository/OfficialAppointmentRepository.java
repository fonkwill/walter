package at.simianarmy.repository;

import at.simianarmy.domain.OfficialAppointment;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the OfficialAppointment entity.
 */
@SuppressWarnings("unused")
public interface OfficialAppointmentRepository extends JpaRepository<OfficialAppointment, Long> {

}
