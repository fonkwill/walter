package at.simianarmy.repository;

import at.simianarmy.domain.Honor;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Honor entity.
 */
@SuppressWarnings("unused")
public interface HonorRepository extends JpaRepository<Honor, Long> {

}
