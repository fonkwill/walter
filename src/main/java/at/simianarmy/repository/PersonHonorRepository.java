package at.simianarmy.repository;

import at.simianarmy.domain.PersonHonor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PersonHonor entity.
 */
@SuppressWarnings("unused")
public interface PersonHonorRepository extends JpaRepository<PersonHonor, Long>, JpaSpecificationExecutor<PersonHonor> {

	@Query("SELECT personHonor FROM PersonHonor personHonor " + "WHERE personHonor.honor.id = :honorId")
	Page<PersonHonor> findByHonor(@Param("honorId") Long honorId, Pageable pageable);
}
