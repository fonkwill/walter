package at.simianarmy.repository;

import at.simianarmy.domain.CashbookAccount;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the CashbookAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CashbookAccountRepository extends JpaRepository<CashbookAccount, Long> {

  Optional<CashbookAccount> findOneByReceiptCode(String receiptCode);

  Page<CashbookAccount> findByBankAccount(Boolean bankAccount, Pageable pageable);

  Boolean existsByTextForAutoTransferFrom(String textForAutoTransferFrom);

  Boolean existsByTextForAutoTransferTo(String textForAutoTransferTo);

  @Query("SELECT c FROM CashbookAccount c WHERE c.textForAutoTransferFrom IS NOT NULL AND c.textForAutoTransferFrom <> ''")
  List<CashbookAccount> findAllByTextForAutoTransferFromIsNotNullAndNotEmpty();

  @Query("SELECT c FROM CashbookAccount c WHERE c.textForAutoTransferTo IS NOT NULL AND c.textForAutoTransferTo <> ''")
  List<CashbookAccount> findAllByTextForAutoTransferToIsNotNullAndNotEmpty();

  CashbookAccount findFirstByDefaultForCashbookIsTrue();

}
