package at.simianarmy.repository;

import at.simianarmy.domain.ColumnConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ColumnConfig entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ColumnConfigRepository extends JpaRepository<ColumnConfig, Long> {

}
