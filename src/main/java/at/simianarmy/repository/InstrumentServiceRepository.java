package at.simianarmy.repository;

import at.simianarmy.domain.InstrumentService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the InstrumentService entity.
 */
@SuppressWarnings("unused")
public interface InstrumentServiceRepository extends JpaRepository<InstrumentService, Long> {

	@Query("SELECT instrumentService FROM InstrumentService instrumentService "
			+ "WHERE instrumentService.instrument.id = :instrumentId")
	Page<InstrumentService> findByInstrument(@Param("instrumentId") Long instrumentId, Pageable pageable);

}
