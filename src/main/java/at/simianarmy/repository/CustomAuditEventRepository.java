package at.simianarmy.repository;

import at.simianarmy.config.audit.AuditEventConverter;
import at.simianarmy.domain.PersistentAuditEvent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import javax.inject.Inject;

import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * An implementation of Spring Boot's AuditEventRepository.
 */
@Repository
public class CustomAuditEventRepository implements AuditEventRepository {

	private static final String AUTHORIZATION_FAILURE = "AUTHORIZATION_FAILURE";

	private static final String ANONYMOUS_USER = "anonymoususer";

	@Inject
	private PersistenceAuditEventRepository persistenceAuditEventRepository;

	@Inject
	private AuditEventConverter auditEventConverter;

	public List<AuditEvent> find(Instant after) {
		Iterable<PersistentAuditEvent> persistentAuditEvents = persistenceAuditEventRepository
				.findByAuditEventDateAfter(LocalDateTime.from(after));
		return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
	}

	public List<AuditEvent> find(String principal, Instant after) {
		Iterable<PersistentAuditEvent> persistentAuditEvents;
		if (principal == null && after == null) {
			persistentAuditEvents = persistenceAuditEventRepository.findAll();
		} else if (after == null) {
			persistentAuditEvents = persistenceAuditEventRepository.findByPrincipal(principal);
		} else {
			persistentAuditEvents = persistenceAuditEventRepository.findByPrincipalAndAuditEventDateAfter(principal,
					LocalDateTime.from(after));
		}
		return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
	}

	@Override
	public List<AuditEvent> find(String principal, Instant after, String type) {
		Iterable<PersistentAuditEvent> persistentAuditEvents = persistenceAuditEventRepository
				.findByPrincipalAndAuditEventDateAfterAndAuditEventType(principal, LocalDateTime.from(after), type);
		return auditEventConverter.convertToAuditEvent(persistentAuditEvents);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void add(AuditEvent event) {
		if (!AUTHORIZATION_FAILURE.equals(event.getType()) && !ANONYMOUS_USER.equals(event.getPrincipal().toString())) {

			PersistentAuditEvent persistentAuditEvent = new PersistentAuditEvent();
			persistentAuditEvent.setPrincipal(event.getPrincipal());
			persistentAuditEvent.setAuditEventType(event.getType());
			Instant instant = event.getTimestamp();
			persistentAuditEvent.setAuditEventDate(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()));
			persistentAuditEvent.setData(auditEventConverter.convertDataToStrings(event.getData()));
			persistenceAuditEventRepository.save(persistentAuditEvent);
		}
	}
}
