package at.simianarmy.repository;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import java.time.ZonedDateTime;
import java.util.List;
import javax.validation.constraints.Email;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the EmailVerificationLink entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmailVerificationLinkRepository extends JpaRepository<EmailVerificationLink, Long> {

  @Query("select e from EmailVerificationLink e where "
    + "e.invalid = false AND e.validationDate is null AND e.creationDate <= :referenceDate")
  List<EmailVerificationLink> findAllToInvalidate(@Param("referenceDate") ZonedDateTime referenceDate);

  Boolean existsByPersonAndInvalidFalseAndValidationDateIsNull(Person person);

  EmailVerificationLink findFirstByPersonAndInvalidFalseAndValidationDateIsNull(Person person);

  EmailVerificationLink findFirstByPersonOrderByCreationDateDesc(Person person);
}
