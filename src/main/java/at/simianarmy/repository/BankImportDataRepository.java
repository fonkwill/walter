package at.simianarmy.repository;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.enumeration.BankImporterType;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the BankImportData entity.
 */
@SuppressWarnings("unused")
public interface BankImportDataRepository extends JpaRepository<BankImportData, Long> {

	@Query("SELECT bankImportData FROM BankImportData bankImportData "
			+ "WHERE bankImportData.cashbookEntry IS NULL ORDER BY bankImportData.entryDate DESC")
	List<BankImportData> findAllNotImported();

	@Query("SELECT bankImportData FROM BankImportData bankImportData "
			+ "WHERE bankImportData.cashbookAccount.id = :cashbookAccountId "
			+ "AND bankImportData.entryReference = :entryReference")
	List<BankImportData> findByCashbookAccountAndReference(
			@Param("cashbookAccountId") Long cashbookAccountId,
			@Param("entryReference") byte[] entryReference);

	@Query("SELECT bankImportData FROM BankImportData bankImportData "
			+ "WHERE bankImportData.cashbookEntry.id = :cashbookEntryId")
	BankImportData findByCashbookEntry(@Param("cashbookEntryId") Long cashbookEntryId);
}
