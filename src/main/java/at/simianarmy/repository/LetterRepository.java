package at.simianarmy.repository;

import at.simianarmy.domain.Letter;

import at.simianarmy.domain.enumeration.LetterType;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Letter entity.
 */
@SuppressWarnings("unused")
public interface LetterRepository extends JpaRepository<Letter, Long>, JpaSpecificationExecutor<Letter> {

	@Query("select distinct letter from Letter letter left join fetch letter.personGroups")
	List<Letter> findAllWithEagerRelationships();

	@Query("select letter" + " from Letter letter left join fetch letter.personGroups where letter.id =:id")
	Letter findOneWithEagerRelationships(@Param("id") Long id);

	Page<Letter> findByLetterType(LetterType letterType, Pageable pageable);
}
