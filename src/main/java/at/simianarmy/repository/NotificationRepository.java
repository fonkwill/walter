package at.simianarmy.repository;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Notification entity.
 */
@SuppressWarnings("unused")
public interface NotificationRepository extends JpaRepository<Notification, Long> {

	@Query("select notification from Notification notification "
			+ "where notification.readByUser.login = ?#{principal.username}")
	List<Notification> findByReadByUserIsCurrentUser();

	@Query("select notification from Notification notification "
			+ "where notification.notificationRule = :notificationRule")
	List<Notification> findByNotificationRule(@Param("notificationRule") NotificationRule notificationRule);

	@Query("select notification from Notification notification "
			+ "where notification.notificationRule = :notificationRule " + "and notification.entityId = :entityId")
	List<Notification> findByNotificationRuleAndEntityId(@Param("notificationRule") NotificationRule notificationRule,
			@Param("entityId") Long entityId);

	@Query("select distinct notification from Notification notification "
			+ "join notification.notificationRule rule join rule.targetAudiences audience "
			+ "where audience.name in (:authorities)")
	List<Notification> findAllForAuthorities(@Param("authorities") List<String> authorities);

	@Query("select count(notification) from Notification notification " + "where notification.readByUser is null")
	Long countUnread();

	@Query("select distinct count(notification) from Notification notification "
			+ "join notification.notificationRule rule join rule.targetAudiences audience "
			+ "where notification.readByUser is null and audience.name in (:authorities)")
	Long countUnreadForAuthorities(@Param("authorities") List<String> authorities);

	@Query("select count(notification) from Notification notification")
	Long countTotal();

	@Query("select distinct count(notification) from Notification notification "
			+ "join notification.notificationRule rule join rule.targetAudiences audience "
			+ "where audience.name in (:authorities)")
	Long countTotalForAuthorities(@Param("authorities") List<String> authorities);

}
