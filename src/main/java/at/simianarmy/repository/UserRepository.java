package at.simianarmy.repository;

import at.simianarmy.domain.User;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findOneByActivationKey(String activationKey);

	List<User> findAllByActivatedIsFalseAndCreatedDateBefore(Instant dateTime);

	Optional<User> findOneByResetKey(String resetKey);

	Optional<User> findOneByEmail(String email);

	Optional<User> findOneByLogin(String login);

	Optional<User> findOneById(Long userId);

	@Query(value = "select distinct user from User user left join fetch user.roles", countQuery = "select count(user) from User user")
	Page<User> findAllWithAuthorities(Pageable pageable);

	@Query(value = "select distinct user from User user where user.login = ?#{principal.username}")
	User findCurrentUser();

	@Override
	void delete(User user);

}
