package at.simianarmy.repository;

import at.simianarmy.domain.InstrumentType;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the InstrumentType entity.
 */
@SuppressWarnings("unused")
public interface InstrumentTypeRepository extends JpaRepository<InstrumentType, Long> {

}
