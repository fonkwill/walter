package at.simianarmy.repository;

import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.enumeration.MailLogStatus;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the Letter entity.
 */
@SuppressWarnings("unused")
public interface MailLogRepository extends JpaRepository<MailLog, Long>, JpaSpecificationExecutor<MailLog> {

  @Query("SELECT m FROM MailLog m WHERE CAST(m.date AS date) = CAST(:date AS date) AND (m.mailLogStatus IN :mailLogStatus OR :mailLogStatus IS NULL) ORDER BY m.date DESC")
  Page<MailLog> findByDateAndMailLogStatus(@Param("date") LocalDate date, @Param("mailLogStatus") List<MailLogStatus> mailLogStatus, Pageable pageable);

  List<MailLog> findByDateBefore(ZonedDateTime date);

}
