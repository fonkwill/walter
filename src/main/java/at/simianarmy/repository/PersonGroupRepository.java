package at.simianarmy.repository;

import at.simianarmy.domain.PersonGroup;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonGroup entity.
 */
@SuppressWarnings("unused")
public interface PersonGroupRepository extends JpaRepository<PersonGroup, Long> {


  List<PersonGroup> findByHasPersonRelevanceIsTrue();

}
