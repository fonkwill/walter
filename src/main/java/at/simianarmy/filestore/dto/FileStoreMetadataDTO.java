package at.simianarmy.filestore.dto;

import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;
import javax.validation.constraints.NotNull;

public class FileStoreMetadataDTO implements Serializable {

  private UUID id;

  private String extension;

  private String originalFile;

  @NotNull
  private FileStoreMetadataType type;

  private UUID parentId;

  @NotNull
  private ZonedDateTime createdAt;

  private Long size;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getOriginalFile() {
    return originalFile;
  }

  public void setOriginalFile(String originalFile) {
    this.originalFile = originalFile;
  }

  public FileStoreMetadataType getType() {
    return type;
  }

  public void setType(FileStoreMetadataType type) {
    this.type = type;
  }

  public UUID getParentId() {
    return parentId;
  }

  public void setParentId(UUID parentId) {
    this.parentId = parentId;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileStoreMetadataDTO that = (FileStoreMetadataDTO) o;
    return Objects.equals(id, that.id) &&
      Objects.equals(extension, that.extension) &&
      Objects.equals(originalFile, that.originalFile) &&
      type == that.type &&
      Objects.equals(parentId, that.parentId) &&
      Objects.equals(createdAt, that.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, extension, originalFile, type, parentId, createdAt);
  }

  @Override
  public String toString() {
    return "FileStoreMetadataDTO{" +
      "id=" + id +
      ", extension='" + extension + '\'' +
      ", originalFile='" + originalFile + '\'' +
      ", type=" + type +
      ", parentId=" + parentId +
      ", createdAt=" + createdAt +
      ", size=" + size +
      '}';
  }
}
