package at.simianarmy.filestore.dto;

import org.springframework.core.io.Resource;

public class FileResourceDTO {

  private Resource resource;

  private String fileName;

  public FileResourceDTO(Resource resource, String fileName) {
    this.resource = resource;
    this.fileName = fileName;
  }

  public Resource getResource() {
    return resource;
  }

  public String getFileName() {
    return fileName;
  }

}
