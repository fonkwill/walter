package at.simianarmy.filestore.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

public class DirectoryMetadataDTO implements Serializable {

  private LinkedHashMap<UUID, String> breadCrumbs = new LinkedHashMap<UUID, String>();

  private List<FileStoreMetadataDTO> content;

  private String name;

  public LinkedHashMap<UUID, String> getBreadCrumbs() {
    return breadCrumbs;
  }

  public void addBreadCrumb(UUID uuid, String text) {
    this.breadCrumbs.put(uuid, text);
  }

  public List<FileStoreMetadataDTO> getContent() {
    return content;
  }

  public void setContent(List<FileStoreMetadataDTO> content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
