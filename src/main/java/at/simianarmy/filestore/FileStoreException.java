package at.simianarmy.filestore;

public class FileStoreException extends Exception {

  public FileStoreException(String message) {
    super(message);
  }

  public FileStoreException(String message, Exception cause) {
    super(message, cause);
  }

}
