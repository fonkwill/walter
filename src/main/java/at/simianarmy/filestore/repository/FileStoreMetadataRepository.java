package at.simianarmy.filestore.repository;

import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the FileStoreMetadata entity.
 */
@Repository
public interface FileStoreMetadataRepository extends JpaRepository<FileStoreMetadata, UUID> {

  List<FileStoreMetadata> findByParentAndStorage(FileStoreMetadata parent, StorageType storage, Pageable pageable);

  Boolean existsByParentAndStorageAndOriginalFileEquals(FileStoreMetadata parent, StorageType storage, String originalFile);

}
