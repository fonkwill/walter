package at.simianarmy.filestore.impl.local;

import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.filestore.service.FileStoreLocationService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component("LocalFileStore")
public class LocalFileStore extends FileStore {

  private final Logger log = LoggerFactory.getLogger(LocalFileStore.class);

  private Path storageLocation;

  @Autowired
  public LocalFileStore(FileStoreLocationService fileStoreLocationService,
    FileStoreMetadataRepository fileStoreMetadataRepository) {
    super(fileStoreLocationService, fileStoreMetadataRepository);
  }

  @Override
  protected void persistFile(MultipartFile file, String fullPath) throws FileStoreException {
    try {
      log.debug("Path for " + file.getOriginalFilename() + ": " + fullPath);
      Files.copy(file.getInputStream(), this.storageLocation.resolve(fullPath));
    } catch (IOException exception) {
      throw new FileStoreException("Failed to store file " + file.getOriginalFilename(), exception);
    }
  }

  @Override
  protected Boolean existsPhysically(String path) {
    return Files.exists(this.storageLocation.resolve(path));
  }

  @Override
  protected Resource getFilePhysically(String path) throws FileStoreException {
    log.debug("trying to retrieve file " + path);
    try {
      Path file = this.storageLocation.resolve(path);
      Resource resource = new UrlResource(file.toUri());
      return resource;
    } catch (MalformedURLException exception) {
      throw new FileStoreException("Could not read file: " + path, exception);
    }
  }

  @Override
  protected void deleteFilePhysically(String path) throws FileStoreException {
    log.debug("trying to remove file " + path);

    try {
      Files.delete(this.storageLocation.resolve(path));
    } catch (IOException exception) {
      throw new FileStoreException("Failed to remove file: " + path, exception);
    }
  }

  @Override
  public void initStorageLocations() throws FileStoreException {
    String homeDirectory = System.getProperty("user.home");
    if (homeDirectory.charAt(homeDirectory.length() - 1) == '\\') {
      homeDirectory = homeDirectory.substring(0, homeDirectory.length() - 2);
    }

    String storageLocationPath = this.fileStoreLocationService.getRoot().replaceFirst("^~", homeDirectory);

    this.storageLocation = this.createFolder(storageLocationPath);

    for (String location : this.fileStoreLocationService.getLocationsToInit()) {
      this.createFolder(storageLocationPath + location);
    }
  }

  private Path createFolderInStorageLocation(String path) throws FileStoreException {
    return this.createFolder(Paths.get(this.storageLocation.toUri()) + "/" + path);
  }

  private Path createFolder(String path) throws FileStoreException {
    Path location = Paths.get(path);

    log.debug("Createing directory: {}", path);

    if (Files.exists(location) == false) {
      try {
        Files.createDirectory(location);
      } catch (IOException exception) {
        log.error("Failed to create storage directory {}", path);
        throw new FileStoreException("Failed to create storage directory: " + path, exception);
      }
    }

    if (Files.isWritable(location) == false) {
      log.error("Storage directory is not writable {}", path);
      throw new FileStoreException("Storage directory is not writable: " + path);
    }

    return location;
  }
}
