package at.simianarmy.filestore.impl.dropbox;

import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.filestore.service.FileStoreLocationService;
import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.GetMetadataErrorException;
import com.dropbox.core.v2.users.FullAccount;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component("DropboxFileStore")
public class DropboxFileStore extends FileStore {

  private final Logger log = LoggerFactory.getLogger(DropboxFileStore.class);

  private DbxClientV2 dropBoxClient;

  @Autowired
  public DropboxFileStore(FileStoreLocationService fileStoreLocationService,
    FileStoreMetadataRepository fileStoreMetadataRepository) {
    super(fileStoreLocationService, fileStoreMetadataRepository);
  }

  @Override
  protected void persistFile(MultipartFile file, String fullPath)
    throws FileStoreException {

    String realPath = this.buildFullPath(fullPath);

    try {

      log.debug("Path for " + file.getOriginalFilename() + ": " + realPath);

      this.dropBoxClient.files().uploadBuilder(realPath).uploadAndFinish(file.getInputStream());
    } catch (DbxException e) {
      throw new FileStoreException("Could not upload file to Dropbox!", e);
    } catch (IOException e) {
      throw new FileStoreException("Could not read file for Upload!", e);
    }
  }

  @Override
  protected Boolean existsPhysically(String path) throws FileStoreException {
    String realPath = this.buildFullPath(path);

    try {
      log.debug("Checking if path " + realPath + " exists!");
      this.dropBoxClient.files().getMetadata(realPath);
      return true;
    } catch (GetMetadataErrorException e) {
      if (e.errorValue.isPath() && e.errorValue.getPathValue().isNotFound()) {
        return false;
      } else {
        throw new FileStoreException("Error on Accessing Metadata for path " + path, e);
      }
    } catch (DbxException e) {
      throw new FileStoreException("Could not check existence of path " + path, e);
    }
  }

  @Override
  protected Resource getFilePhysically(String path) throws FileStoreException {
    String realPath = this.buildFullPath(path);

    try {
      DbxDownloader<FileMetadata> download = this.dropBoxClient.files().download(realPath);

      return new InputStreamResource(download.getInputStream());

    } catch (DbxException e) {
      throw new FileStoreException("Could not download File from Dopbox!", e);
    }
  }

  @Override
  protected void deleteFilePhysically(String path) throws FileStoreException {
    String realPath = this.buildFullPath(path);

    try {
      this.dropBoxClient.files().deleteV2(realPath);
    } catch (DbxException e) {
      throw new FileStoreException("Could not delete File from Dropbox!", e);
    }
  }

  @Override
  public void initStorageLocations() throws FileStoreException {
    DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/WalterApp")
      .withUserLocale("de_AT").build();
    this.dropBoxClient = new DbxClientV2(config, this.fileStoreLocationService.getAccessToken());

    FullAccount account = null;
    try {
      account = this.dropBoxClient.users().getCurrentAccount();
      log.debug("Connected to Dropbox via Account {}", account.getName().getDisplayName());
    } catch (DbxException e) {
      log.error("Unable to connected to Dropbox Account!", e);
    }


    for (String location : this.fileStoreLocationService.getLocationsToInit()) {
      this.createDirectoryPhysically(location);
    }
  }

  private String buildFullPath(String path) {
    String fullPath = this.fileStoreLocationService.getRoot() + path;

    if (fullPath.endsWith("/")) {
      fullPath = fullPath.substring(0, path.length());
    }

    return fullPath;
  }


  private void createDirectoryPhysically(String fullPath) throws FileStoreException {
    if (!this.existsPhysically(fullPath)) {
      String realPath = this.buildFullPath(fullPath);
      log.debug("Creating directory: " + realPath);
      try {
        this.dropBoxClient.files().createFolderV2(realPath);
      } catch (DbxException e) {
        throw new FileStoreException("Could not create directory: " + realPath);
      }
    }
  }
}
