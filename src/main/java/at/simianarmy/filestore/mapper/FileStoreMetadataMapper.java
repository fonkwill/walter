package at.simianarmy.filestore.mapper;

import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.dto.FileStoreMetadataDTO;
import java.util.List;
import java.util.UUID;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FileStoreMetadataMapper {

  @Mapping(source = "parent.id", target = "parentId")
  FileStoreMetadataDTO fileStoreMetadataToFileStoreMetadataDTO(FileStoreMetadata fileStoreMetadata);

  @Mapping(source = "parentId", target = "parent")
  @Mapping(target = "children", ignore = true)
  @Mapping(target = "storage", ignore = true)
  FileStoreMetadata fileStoreMetadataDTOToFileStoreMetadata(FileStoreMetadataDTO fileStoreMetadataDTO);

  List<FileStoreMetadataDTO> fileStoreMetadatasToFileStoreMetadataDTOs(List<FileStoreMetadata> fileStoreMetadatas);

  default FileStoreMetadata fileStoreMetadataFromId(UUID id) {
    if (id == null) {
      return null;
    }
    FileStoreMetadata fileStoreMetadata = new FileStoreMetadata();
    fileStoreMetadata.setId(id);
    return fileStoreMetadata;
  }
}
