package at.simianarmy.filestore.domain;

import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class FileStoreMetadata implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  @Column(name = "extension")
  private String extension;

  @Column(name = "original_file", nullable = false)
  private String originalFile;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "type", nullable = false)
  private FileStoreMetadataType type;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(name = "storage", nullable = false)
  private StorageType storage;

  @ManyToOne
  private FileStoreMetadata parent;

  @OneToMany(mappedBy = "parent")
  private Set<FileStoreMetadata> children = new HashSet<>();

  @NotNull
  @Column(name = "created_at", nullable = false)
  private ZonedDateTime createdAt;

  @Column(name = "size")
  private Long size;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getOriginalFile() {
    return originalFile;
  }

  public void setOriginalFile(String originalFile) {
    this.originalFile = originalFile;
  }

  public FileStoreMetadataType getType() {
    return type;
  }

  public void setType(FileStoreMetadataType type) {
    this.type = type;
  }

  public FileStoreMetadata getParent() {
    return parent;
  }

  public void setParent(FileStoreMetadata parent) {
    this.parent = parent;
  }

  public Set<FileStoreMetadata> getChildren() {
    return children;
  }

  public void addChild(FileStoreMetadata metadata) {
    metadata.setParent(this);
    this.children.add(metadata);
  }

  public void removeChild(FileStoreMetadata metadata) {
    metadata.setParent(null);
    this.children.remove(metadata);
  }

  public void setChildren(Set<FileStoreMetadata> children) {
    this.children = children;
  }

  public ZonedDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(ZonedDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public StorageType getStorage() {
    return storage;
  }

  public void setStorage(StorageType storage) {
    this.storage = storage;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileStoreMetadata metadata = (FileStoreMetadata) o;
    return Objects.equals(id, metadata.id) &&
      Objects.equals(extension, metadata.extension) &&
      Objects.equals(originalFile, metadata.originalFile) &&
      type == metadata.type &&
      Objects.equals(parent, metadata.parent) &&
      Objects.equals(createdAt, metadata.createdAt);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, extension, originalFile, type, parent, createdAt);
  }

  @Override
  public String toString() {
    return "FileStoreMetadata{" +
      "id=" + id +
      ", extension='" + extension + '\'' +
      ", originalFile='" + originalFile + '\'' +
      ", type=" + type +
      ", parent=" + parent +
      ", createdAt=" + createdAt +
      ", size=" + size +
      '}';
  }
}
