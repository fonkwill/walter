package at.simianarmy.filestore.domain.enumeration;

public enum StorageType {
  TEMPLATE, RECEIPT, DOCUMENT
}
