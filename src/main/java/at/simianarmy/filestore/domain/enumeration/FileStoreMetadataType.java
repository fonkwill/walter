package at.simianarmy.filestore.domain.enumeration;

public enum FileStoreMetadataType {
  FILE, DIRECTORY
}
