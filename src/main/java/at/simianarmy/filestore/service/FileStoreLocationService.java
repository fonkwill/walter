package at.simianarmy.filestore.service;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.config.WalterProperties;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.multitenancy.repository.TenantConfigurationJsonRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.springframework.stereotype.Component;

@Component
public class FileStoreLocationService {

  @Inject
  private FileStoreProperties fileStoreProperties;

  @Inject
  private WalterProperties walterProperties;

  @Inject
  private TenantConfigurationJsonRepository tenantConfigurationJsonRepository;

  @Inject
  private TenantIdentiferResolver tenantIdentiferResolver;

  public String getReceiptsLocation() {
    return this.resolveCurrentTenantDirectory() + this.fileStoreProperties.getReceiptsLocation();
  }

  public String getTemplatesLocation() {
    return this.resolveCurrentTenantDirectory() + this.fileStoreProperties.getTemplatesLocation();
  }

  public String getDocumentsLocation() {
    return this.resolveCurrentTenantDirectory() + this.fileStoreProperties.getDocumentsLocation();
  }

  public String getAccessToken() {
    return this.fileStoreProperties.getAccessToken();
  }

  public String getRoot() {
    return this.fileStoreProperties.getRoot();
  }

  public List<String> getLocationsToInit() {
    List<String> result = new ArrayList<>();

    Set<String> tenants = this.tenantConfigurationJsonRepository.getAllTenantIDs();

    if (!this.walterProperties.getMultitenancy().isEnabled()) {

      result.add(this.fileStoreProperties.getTemplatesLocation());
      result.add(this.fileStoreProperties.getDocumentsLocation());
      result.add(this.fileStoreProperties.getReceiptsLocation());

    } else {
      for (String tenant : tenants) {
        result.add(tenant);
        result.add(tenant + "/" + this.fileStoreProperties.getReceiptsLocation());
        result.add(tenant + "/" + this.fileStoreProperties.getDocumentsLocation());
        result.add(tenant + "/" + this.fileStoreProperties.getTemplatesLocation());
      }
    }

    return result;
  }

  private String resolveCurrentTenantDirectory() {
    if (this.tenantIdentiferResolver.resolveCurrentTenantIdentifier() != TenantIdentiferResolver.MASTER_TENANT) {
      return this.tenantIdentiferResolver.resolveCurrentTenantIdentifier() + "/";
    }
    return "";
  }


}
