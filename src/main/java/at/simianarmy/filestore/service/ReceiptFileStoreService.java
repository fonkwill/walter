package at.simianarmy.filestore.service;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.FileResourceDTO;
import java.util.UUID;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ReceiptFileStoreService {

  private final StorageType TYPE = StorageType.RECEIPT;

  @Inject
  private FileStoreLocationService fileStoreLocationService;

  @Inject
  private FileStore fileStore;

  public String saveReceipt(MultipartFile file) throws FileStoreException {
    return this.fileStore.saveFile(file, this.fileStoreLocationService.getReceiptsLocation(), TYPE).getId().toString();
  }

  public FileResourceDTO getReceipt(UUID uuid) throws FileStoreException {
    return this.fileStore.getFile(uuid, this.fileStoreLocationService.getReceiptsLocation());
  }

  public void deleteReceipt(UUID uuid) throws FileStoreException {
    this.fileStore.deleteFile(uuid, this.fileStoreLocationService.getReceiptsLocation());
  }

}
