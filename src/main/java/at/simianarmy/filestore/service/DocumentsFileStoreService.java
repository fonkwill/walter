package at.simianarmy.filestore.service;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.DirectoryMetadataDTO;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.dto.FileStoreMetadataDTO;
import at.simianarmy.filestore.mapper.FileStoreMetadataMapper;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.service.ServiceErrorConstants;
import at.simianarmy.service.exception.ServiceValidationException;
import com.google.common.io.Files;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class DocumentsFileStoreService {

  private final Logger log = LoggerFactory.getLogger(DocumentsFileStoreService.class);
  private final StorageType TYPE = StorageType.DOCUMENT;

  @Inject
  private FileStoreLocationService fileStoreLocationService;

  @Inject
  private FileStore fileStore;

  @Inject
  private FileStoreMetadataMapper fileStoreMetadataMapper;

  @Inject
  private FileStoreMetadataRepository fileStoreMetadataRepository;

  @Transactional
  public FileStoreMetadataDTO saveDocument(MultipartFile file, UUID parentUuid) {

    FileStoreMetadata parent = null;

    if (parentUuid != null) {

      Optional<FileStoreMetadata> metadataOptional = this.fileStoreMetadataRepository
        .findById(parentUuid);

      if (metadataOptional.isPresent()) {
        parent = metadataOptional.get();
      }
    }

    try {
      return this.fileStoreMetadataMapper.fileStoreMetadataToFileStoreMetadataDTO(this.fileStore
        .saveFile(file, parentUuid, this.fileStoreLocationService.getDocumentsLocation(),
          this.getUniqueName(file.getOriginalFilename(), parent, TYPE,false), TYPE));
    } catch(FileStoreException e) {
      log.error("Could not store file {}", e);
      throw new ServiceValidationException(ServiceErrorConstants.documentsFileNotCreated, null);
    }
  }

  @Transactional(readOnly = true)
  public FileResourceDTO getDocument(UUID uuid) {
    if (!this.fileStore.exists(uuid)) {
      return null;
    }

    try {
      return this.fileStore.getFile(uuid, this.fileStoreLocationService.getDocumentsLocation());
    } catch(FileStoreException e) {
      throw new ServiceValidationException(ServiceErrorConstants.documentsOverallError, null);
    }
  }

  @Transactional
  public void deleteDocument(UUID uuid) {
    try {
      this.fileStore.deleteFile(uuid, this.fileStoreLocationService.getDocumentsLocation());
    } catch(FileStoreException e) {
      throw new ServiceValidationException(ServiceErrorConstants.documentsFileNotDeleted, null);
    }
  }

  @Transactional
  public FileStoreMetadataDTO createDirectory(FileStoreMetadataDTO fileStoreMetadataDTO) {
    FileStoreMetadata metadata = this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(fileStoreMetadataDTO);
    UUID parentId = null;

    if (metadata.getParent() != null) {
      parentId = metadata.getParent().getId();
    }

    try {
      metadata = this.fileStore
        .createDirectory(this.getUniqueName(metadata.getOriginalFile(), metadata.getParent(), TYPE, true), parentId,
          this.fileStoreLocationService.getDocumentsLocation(), TYPE);

      return this.fileStoreMetadataMapper.fileStoreMetadataToFileStoreMetadataDTO(metadata);
    } catch(FileStoreException e) {
      log.error("Could not create Directory!", e);
      throw new ServiceValidationException(ServiceErrorConstants.documentsDirectoryNotCreated, null);
    }
  }

  @Transactional(readOnly = true)
  public DirectoryMetadataDTO getDirectory(UUID uuid, Pageable pageable) {

    if (uuid != null && !this.fileStore.exists(uuid)) {
      return null;
    }

    try {
      List<FileStoreMetadataDTO> content = this.fileStoreMetadataMapper.fileStoreMetadatasToFileStoreMetadataDTOs(this.fileStore.getDirectory(uuid, TYPE, pageable));
      DirectoryMetadataDTO directory = new DirectoryMetadataDTO();
      directory.setContent(content);
      if (uuid != null) {
        this.generateBreadCrumbs(directory, uuid);
      }
      return directory;
    } catch(FileStoreException e) {
      log.error("Could not get Directory!", e);
      throw new ServiceValidationException(ServiceErrorConstants.documentsOverallError, null);
    }
  }

  @Transactional
  public void deleteDirectory(UUID uuid) {
    try {
      this.fileStore.deleteDirectory(uuid, this.fileStoreLocationService.getDocumentsLocation());
    } catch (FileStoreException e) {
      log.error("Could not delete Directory!", e);
      throw new ServiceValidationException(ServiceErrorConstants.documentsDirectoryNotDeleted, null);
    }
  }

  @Transactional
  public FileStoreMetadataDTO updateMetadata(FileStoreMetadataDTO fileStoreMetadataDTO) {
    FileStoreMetadata metadata = this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(fileStoreMetadataDTO);
    FileStoreMetadata metadataFromDb = this.fileStoreMetadataRepository.getOne(metadata.getId());

    if ((metadata.getStorage() != null && !metadata.getStorage().equals(metadataFromDb.getStorage())) ||
      (metadata.getParent() != null && metadata.getParent().getId().equals(metadata.getId())) ||
        !metadata.getType().equals(metadataFromDb.getType())) {
      throw new ServiceValidationException(ServiceErrorConstants.documentsFileUnallowedManipulation, null);
    }

    if (!metadata.getOriginalFile().equals(metadataFromDb.getOriginalFile())) {
      if (metadataFromDb.getType().equals(FileStoreMetadataType.FILE)) {
        metadataFromDb.setOriginalFile(this
          .getUniqueName(metadata.getOriginalFile(), metadataFromDb.getParent(),
            metadataFromDb.getStorage(), false));
      } else {
        metadataFromDb.setOriginalFile(this
          .getUniqueName(metadata.getOriginalFile(), metadataFromDb.getParent(),
            metadataFromDb.getStorage(), true));
      }
      this.fileStoreMetadataRepository.save(metadataFromDb);
    }

    metadataFromDb.setParent(metadata.getParent());

    return this.fileStoreMetadataMapper.fileStoreMetadataToFileStoreMetadataDTO(metadataFromDb);

  }

  private String getUniqueName(String originalName, FileStoreMetadata parent, StorageType storage, boolean isFolder) {
    String name = originalName;

    int counter = 1;

    while (this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(parent, storage, name)) {
      if (isFolder) {
        name = originalName + "(" + counter + ")";
      } else {
        String extension = Files.getFileExtension(originalName);
        String fileNameWithoutExtension = originalName.replace("." + extension, "");
        name = fileNameWithoutExtension + "(" + counter + ")." + extension;
      }
      counter++;
    }

    return name;
  }

  private void generateBreadCrumbs(DirectoryMetadataDTO dto, UUID uuid) {

    FileStoreMetadata directory = this.fileStoreMetadataRepository.getOne(uuid);
    dto.setName(directory.getOriginalFile());
    FileStoreMetadata parent = directory.getParent();

    while (parent != null) {
      dto.addBreadCrumb(parent.getId(), parent.getOriginalFile());
      parent = parent.getParent();
    }
  }



}
