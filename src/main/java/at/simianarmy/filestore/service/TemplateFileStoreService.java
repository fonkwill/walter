package at.simianarmy.filestore.service;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.FileResourceDTO;
import java.util.UUID;
import javax.inject.Inject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TemplateFileStoreService {

  private final StorageType TYPE = StorageType.TEMPLATE;

  @Inject
  private FileStoreLocationService fileStoreLocationService;

  @Inject
  private FileStore fileStore;

  public String saveTemplate(MultipartFile file) throws FileStoreException {
    return this.fileStore.saveFile(file, this.fileStoreLocationService.getTemplatesLocation(), TYPE).getId().toString();
  }

  public FileResourceDTO getTemplate(UUID uuid) throws FileStoreException {
    return this.fileStore.getFile(uuid, this.fileStoreLocationService.getTemplatesLocation());
  }

  public void deleteTemplate(UUID uuid) throws FileStoreException {
    this.fileStore.deleteFile(uuid, this.fileStoreLocationService.getTemplatesLocation());
  }

}
