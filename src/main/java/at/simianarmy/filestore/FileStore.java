package at.simianarmy.filestore;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.filestore.service.FileStoreLocationService;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public abstract class FileStore {

  private final Logger log = LoggerFactory.getLogger(FileStore.class);

  protected FileStoreMetadataRepository fileStoreMetadataRepository;

  protected FileStoreLocationService fileStoreLocationService;

  @Autowired
  public FileStore(FileStoreLocationService fileStoreLocationService, FileStoreMetadataRepository fileStoreMetadataRepository) {
    this.fileStoreMetadataRepository = fileStoreMetadataRepository;
    this.fileStoreLocationService = fileStoreLocationService;
  }

  public boolean exists(UUID uuid) {
    return this.fileStoreMetadataRepository.existsById(uuid);
  }

  public FileStoreMetadata saveFile(MultipartFile file, String path, StorageType storage) throws FileStoreException {
    return this.saveFile(file, null, path, storage);
  }

  public FileStoreMetadata saveFile(MultipartFile file, UUID parentUuid, String path, StorageType storage) throws FileStoreException {
    return this.saveFile(file, parentUuid, path, null, storage);
  }

  public FileStoreMetadata saveFile(MultipartFile file, UUID parentUuid, String path, String fileName, StorageType storage) throws FileStoreException {
    if (file.isEmpty()) {
      throw new FileStoreException("Failed to store empty file " + file.getOriginalFilename());
    }

    FileStoreMetadata parent = this.getMetadataFromUuid(parentUuid);

    log.debug("trying to store file " + file.getOriginalFilename());

    String extension = FilenameUtils.getExtension(file.getOriginalFilename());
    FileStoreMetadata fileStoreMetadata = new FileStoreMetadata();
    fileStoreMetadata.setExtension(extension);

    if (fileName != null) {
      fileStoreMetadata.setOriginalFile(fileName);
    } else {
      fileStoreMetadata.setOriginalFile(file.getOriginalFilename());
    }
    fileStoreMetadata.setType(FileStoreMetadataType.FILE);
    fileStoreMetadata.setCreatedAt(ZonedDateTime.now());
    fileStoreMetadata.setSize(file.getSize());
    fileStoreMetadata.setStorage(storage);
    fileStoreMetadata = this.fileStoreMetadataRepository.save(fileStoreMetadata);

    if (parent != null) {
      parent.addChild(fileStoreMetadata);
      this.fileStoreMetadataRepository.save(parent);
    }

    String fullPath = path + this.getPathFromMetadata(fileStoreMetadata);

    this.persistFile(file, fullPath);

    return fileStoreMetadata;
  }

  public FileStoreMetadata createDirectory(String name, UUID parentUuid, String path, StorageType storage) throws FileStoreException {
    if (name == null || name.equals("")) {
      throw new FileStoreException("Cannot create a folder with empty name!");
    }

    FileStoreMetadata parent = this.getMetadataFromUuid(parentUuid);

    log.debug("trying to create folder " + name);

    FileStoreMetadata fileStoreMetadata = new FileStoreMetadata();
    fileStoreMetadata.setOriginalFile(name);
    fileStoreMetadata.setType(FileStoreMetadataType.DIRECTORY);
    fileStoreMetadata.setCreatedAt(ZonedDateTime.now());
    fileStoreMetadata.setStorage(storage);
    fileStoreMetadata = this.fileStoreMetadataRepository.save(fileStoreMetadata);

    if (parent != null) {
      parent.addChild(fileStoreMetadata);
      this.fileStoreMetadataRepository.save(parent);
    }

    String fullPath = path + this.getPathFromMetadata(fileStoreMetadata);

    return fileStoreMetadata;
  }

  public FileResourceDTO getFile(UUID uuid, String basePath) throws FileStoreException {
    if (!this.exists(uuid)) {
      throw new FileStoreException("File not found: " + uuid);
    }

    FileStoreMetadata metadata = this.getMetadataFromUuid(uuid);

    if (!this.isFile(metadata)) {
      throw new FileStoreException("UUID " + uuid.toString() + " is not a file!");
    }

    String fullPath = basePath + this.getPathFromMetadata(metadata);

    return new FileResourceDTO(this.getFilePhysically(fullPath), metadata.getOriginalFile());
  }

  public List<FileStoreMetadata> getDirectory(UUID uuid, StorageType storage, Pageable pageable) throws FileStoreException {

    if (uuid == null) {
      return this.fileStoreMetadataRepository.findByParentAndStorage(null, storage, pageable);
    }

    if (!this.exists(uuid)) {
      throw new FileStoreException("Directory not found: " + uuid);
    }

    FileStoreMetadata metadata = this.getMetadataFromUuid(uuid);

    if (!this.isDirectory(metadata)) {
      throw new FileStoreException("UUID " + uuid.toString() + " is not a directory!");
    }

    return this.fileStoreMetadataRepository.findByParentAndStorage(metadata, storage, pageable);
  }

  public void deleteFile(UUID uuid, String basePath) throws FileStoreException {
    if (this.exists(uuid)) {
      FileStoreMetadata metadata = this.getMetadataFromUuid(uuid);

      if (!this.isFile(metadata)) {
        throw new FileStoreException("UUID " + uuid.toString() + " is not a file!");
      }

      String fullPath = basePath + this.getPathFromMetadata(metadata);

      this.deleteFilePhysically(fullPath);
      this.fileStoreMetadataRepository.delete(metadata);
    }
  }

  public void deleteDirectory(UUID uuid, String basePath) throws FileStoreException {
    if (this.exists(uuid)) {
      FileStoreMetadata metadata = this.getMetadataFromUuid(uuid);

      if (!this.isDirectory(metadata)) {
        throw new FileStoreException("UUID " + uuid.toString() + " is not a directory!");
      }

      Set<FileStoreMetadata> children = metadata.getChildren();

      for (FileStoreMetadata child : children) {
        if (child.getType().equals(FileStoreMetadataType.FILE)) {
          this.deleteFile(child.getId(), basePath);
        } else {
          this.deleteDirectory(child.getId(), basePath);
        }
        metadata.removeChild(child);
      }

      this.fileStoreMetadataRepository.delete(metadata);
    }
  }

  private boolean isFile(FileStoreMetadata metadata) {
    return metadata.getType().equals(FileStoreMetadataType.FILE);
  }

  private boolean isDirectory(FileStoreMetadata metadata) {
    return metadata.getType().equals(FileStoreMetadataType.DIRECTORY);
  }

  private FileStoreMetadata getMetadataFromUuid(UUID uuid) throws FileStoreException {
    if (uuid != null) {

      if (!this.exists(uuid)) {
        throw new FileStoreException("Failed to store file, because parent element does not exist!");
      }

      return this.fileStoreMetadataRepository.findById(uuid).get();

    }

    return null;
  }

  private String getPathFromMetadata(FileStoreMetadata metadata) {
    String current = null;

    if(metadata.getType().equals(FileStoreMetadataType.DIRECTORY)) {
      current = metadata.getId().toString() + "/";
    }
    else {
      current = metadata.getId().toString() + "." + metadata.getExtension();
    }

    return current;
  }

  protected abstract void persistFile(MultipartFile file, String fullPath) throws FileStoreException;
  public abstract void initStorageLocations() throws FileStoreException;
  protected abstract Boolean existsPhysically(String path) throws FileStoreException;
  protected abstract Resource getFilePhysically(String path) throws FileStoreException;
  protected abstract void deleteFilePhysically(String path) throws FileStoreException;

}
