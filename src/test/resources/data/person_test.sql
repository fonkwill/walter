--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

-- Started on 2019-02-08 12:26:04

--
-- TOC entry 3804 (class 0 OID 86559)
-- Dependencies: 341
-- Data for Name: address; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



-- DELETE everything from person_group

DELETE FROM person_group;

--
-- TOC entry 3790 (class 0 OID 86477)
-- Dependencies: 327
-- Data for Name: person_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_group VALUES (1, 'Vorstand', NULL, FALSE );
INSERT INTO person_group VALUES (2, 'Musiker', NULL, FALSE);
INSERT INTO person_group VALUES (3, 'Mitglieder', NULL, FALSE);


--
-- TOC entry 3845 (class 0 OID 86759)
-- Dependencies: 382
-- Data for Name: appointment_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO appointment_type VALUES (1, 'Sitzung', false, 1);
INSERT INTO appointment_type VALUES (2, 'Probe', false, 2);


--
-- TOC entry 3862 (class 0 OID 86836)
-- Dependencies: 399
-- Data for Name: report; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3864 (class 0 OID 86847)
-- Dependencies: 401
-- Data for Name: official_appointment; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3842 (class 0 OID 86741)
-- Dependencies: 379
-- Data for Name: appointment; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO appointment VALUES (1, 'Probe', 'Generalprobe für das Konzert am 30.01', 'Untere Oberstrasse', '3483', 'Oberndorf', 'Austria', '2019-01-02 18:00:00', '2019-02-02 20:00:00', NULL, 2);
INSERT INTO appointment VALUES (2, 'Vorstands-Sitzung', 'Tagesordnung-
- Bericht des Obmannes
- Diskussion um Ausbau des Klublokasl', 'Wirthaus Hansi', NULL, NULL, NULL, '2019-01-31 20:00:00', '2019-01-31 22:00:00', NULL, 1);


--
-- TOC entry 3787 (class 0 OID 86461)
-- Dependencies: 324
-- Data for Name: person; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person VALUES (3, 'Franzi', 'Mayer', NULL, NULL, NULL, 'Hauptplatz 3', '3647', 'Hauptplazi-Ort', 'Austria', 'MALE', 'MANUALLY', true, NULL);
INSERT INTO person VALUES (1, 'Hansi', 'Huber', '1989-02-06', 'hansili.huber@gmx.at', NULL, 'Untere Hauptstrasse 22', '2256', 'Oberndorf', 'Austria', 'MALE', 'VERIFIED', false, NULL);
INSERT INTO person VALUES (2, 'Brigitte', 'Maier', NULL, 'maier.gitti@gmx.at', NULL, 'Obere Hauptstrasse 55', '2658', 'Unterndorf', 'Austria', 'FEMALE', 'MANUALLY', false, NULL);


--
-- TOC entry 3843 (class 0 OID 86752)
-- Dependencies: 380
-- Data for Name: appointment_persons; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO appointment_persons VALUES (1, 2);
INSERT INTO appointment_persons VALUES (2, 2);
INSERT INTO appointment_persons VALUES (1, 1);


--
-- TOC entry 3859 (class 0 OID 86823)
-- Dependencies: 396
-- Data for Name: arranger; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3882 (class 0 OID 86941)
-- Dependencies: 419
-- Data for Name: cashbook_account; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO cashbook_account VALUES (1, 'Kassabuch', false, null, 'KA', 0, null, null, true);
INSERT INTO cashbook_account VALUES (2, 'George', true, 'GEORGE_IMPORTER', 'GEO', 0, null, null, false);

--
-- TOC entry 3835 (class 0 OID 86703)
-- Dependencies: 372
-- Data for Name: cashbook_category; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3833 (class 0 OID 86692)
-- Dependencies: 370
-- Data for Name: cashbook_entry; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO cashbook_entry VALUES (1, '01060012017', 'INCOME', '2017-05-03', 50.00, NULL, NULL, NULL, 1);
INSERT INTO cashbook_entry VALUES (2, 'Instrumentenankauf: ''Trompete''', 'SPENDING', '2012-02-15', 300.00, NULL, NULL, NULL, 1);
INSERT INTO cashbook_entry VALUES (3, '00001012019', 'INCOME', '2019-02-01', 50.00, NULL, NULL, NULL, 1);


--
-- TOC entry 3850 (class 0 OID 86783)
-- Dependencies: 387
-- Data for Name: color_code; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3839 (class 0 OID 86725)
-- Dependencies: 376
-- Data for Name: company; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--
INSERT INTO company (id, name) values (1, 'Verein');


--
-- TOC entry 3854 (class 0 OID 86802)
-- Dependencies: 391
-- Data for Name: genre; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3852 (class 0 OID 86791)
-- Dependencies: 389
-- Data for Name: music_book; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3847 (class 0 OID 86767)
-- Dependencies: 384
-- Data for Name: composition; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3860 (class 0 OID 86829)
-- Dependencies: 397
-- Data for Name: arranger_compositions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3822 (class 0 OID 86641)
-- Dependencies: 359
-- Data for Name: award; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO award VALUES (1, 'Goldenes Abzeichen', 'Das goldene Abzeichen');
INSERT INTO award VALUES (2, 'Silbernes Abzeichen', 'Das silberne Abzeichen');


--
-- TOC entry 3868 (class 0 OID 86866)
-- Dependencies: 405
-- Data for Name: bank_import_data; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO bank_import_data VALUES (2, '2017-05-03', 50.00, '01060012017', 'Fabsich Stefan', STRINGTOUTF8('12000168945021665875476345234424'), 1, NULL, 2, NULL, NULL, false);
INSERT INTO bank_import_data VALUES (4, '2019-02-01', 50.00, '00001012019', 'Huber Hansi', STRINGTOUTF8('12000168945021665875476345231234'), 3, NULL, 2, NULL, NULL, false);


--
-- TOC entry 3830 (class 0 OID 86676)
-- Dependencies: 367
-- Data for Name: cashbook; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3831 (class 0 OID 86685)
-- Dependencies: 368
-- Data for Name: cashbook_cashbook_entries; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3818 (class 0 OID 86622)
-- Dependencies: 355
-- Data for Name: clothing_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO clothing_type VALUES (1, 'Jacke');
INSERT INTO clothing_type VALUES (2, 'Hose');


--
-- TOC entry 3816 (class 0 OID 86614)
-- Dependencies: 353
-- Data for Name: clothing; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO clothing VALUES (1, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (2, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (3, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (4, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (5, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (6, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (7, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (8, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (9, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (10, 'L', NULL, false, 1);
INSERT INTO clothing VALUES (11, 'M', NULL, false, 1);
INSERT INTO clothing VALUES (12, 'M', NULL, false, 1);
INSERT INTO clothing VALUES (13, 'M', NULL, false, 1);
INSERT INTO clothing VALUES (14, 'M', NULL, false, 1);
INSERT INTO clothing VALUES (15, 'M', NULL, false, 1);
INSERT INTO clothing VALUES (16, '54', NULL, false, 2);
INSERT INTO clothing VALUES (17, '54', NULL, false, 2);
INSERT INTO clothing VALUES (18, '54', NULL, false, 2);
INSERT INTO clothing VALUES (19, '54', NULL, false, 2);
INSERT INTO clothing VALUES (20, '54', NULL, false, 2);
INSERT INTO clothing VALUES (21, '54', NULL, false, 2);
INSERT INTO clothing VALUES (22, '54', NULL, false, 2);
INSERT INTO clothing VALUES (23, '54', NULL, false, 2);
INSERT INTO clothing VALUES (24, '54', NULL, false, 2);
INSERT INTO clothing VALUES (25, '54', NULL, false, 2);
INSERT INTO clothing VALUES (26, '54', NULL, false, 2);
INSERT INTO clothing VALUES (27, '54', NULL, false, 2);
INSERT INTO clothing VALUES (28, '54', NULL, false, 2);
INSERT INTO clothing VALUES (29, '54', NULL, false, 2);
INSERT INTO clothing VALUES (30, '54', NULL, false, 2);
INSERT INTO clothing VALUES (31, '54', NULL, false, 2);
INSERT INTO clothing VALUES (32, '54', NULL, false, 2);
INSERT INTO clothing VALUES (33, '54', NULL, false, 2);
INSERT INTO clothing VALUES (34, '54', NULL, false, 2);
INSERT INTO clothing VALUES (35, '54', NULL, false, 2);
INSERT INTO clothing VALUES (36, '52', NULL, false, 2);
INSERT INTO clothing VALUES (37, '52', NULL, false, 2);
INSERT INTO clothing VALUES (38, '52', NULL, false, 2);
INSERT INTO clothing VALUES (39, '52', NULL, false, 2);
INSERT INTO clothing VALUES (40, '52', NULL, false, 2);


--
-- TOC entry 3866 (class 0 OID 86858)
-- Dependencies: 403
-- Data for Name: clothing_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--





--
-- TOC entry 3840 (class 0 OID 86734)
-- Dependencies: 377
-- Data for Name: company_person_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3856 (class 0 OID 86810)
-- Dependencies: 393
-- Data for Name: composer; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3857 (class 0 OID 86816)
-- Dependencies: 394
-- Data for Name: composer_compositions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3848 (class 0 OID 86776)
-- Dependencies: 385
-- Data for Name: composition_appointments; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3880 (class 0 OID 86930)
-- Dependencies: 417
-- Data for Name: customization; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--
update customization set data = '1' where name = 'ORGANIZATION_COMPANY';


--
-- TOC entry 3777 (class 0 OID 86396)
-- Dependencies: 314
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3776 (class 0 OID 86391)
-- Dependencies: 313
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--


--
-- TOC entry 3885 (class 0 OID 87328)
-- Dependencies: 422
-- Data for Name: email_verification_link; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO email_verification_link VALUES (1, '9dkdxKko', '2019-02-04 19:07:03.543', '2019-02-04 19:08:04.641', false, 1);
INSERT INTO email_verification_link VALUES (2, 'YFHPrbcJ', '2019-02-08 10:16:19.215', '2019-02-08 10:16:39.97', false, 1);


--
-- TOC entry 3887 (class 0 OID 87349)
-- Dependencies: 424
-- Data for Name: filter; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3820 (class 0 OID 86630)
-- Dependencies: 357
-- Data for Name: honor; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO honor VALUES (1, '10 Jahre Mtiglied', 'Mitglied seit 10 Jahren');
INSERT INTO honor VALUES (2, '5 Jahre Vorstand', 'Im Vorstand seit 5 Jahren');
INSERT INTO honor VALUES (3, '20 Jahre Mitglied', 'Mitglied seit 20 Jahren');


--
-- TOC entry 3810 (class 0 OID 86588)
-- Dependencies: 347
-- Data for Name: instrument_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO instrument_type VALUES (2, 'Blasinstrument');
INSERT INTO instrument_type VALUES (1, 'Schlaginstrument');


--
-- TOC entry 3808 (class 0 OID 86578)
-- Dependencies: 345
-- Data for Name: instrument; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO instrument VALUES (1, 'Trompete', '2012-02-15', false, 300.00, 2, 2);
INSERT INTO instrument VALUES (2, 'Schlagzeug', '2012-09-01', true, NULL, 1, NULL);
INSERT INTO instrument VALUES (3, 'Saxophon', '2016-05-01', true, 250.00, 2, NULL);


--
-- TOC entry 3812 (class 0 OID 86596)
-- Dependencies: 349
-- Data for Name: instrument_service; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3781 (class 0 OID 86419)
-- Dependencies: 318
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO jhi_authority VALUES ('AWARD', 3, 7, true);
INSERT INTO jhi_authority VALUES ('LETTER', 3, 8, true);
INSERT INTO jhi_authority VALUES ('MUSIC_BOOK', 3, 9, true);
INSERT INTO jhi_authority VALUES ('DEMAND_LETTER', 3, 10, true);
INSERT INTO jhi_authority VALUES ('CASHBOOK', 3, 11, true);
INSERT INTO jhi_authority VALUES ('CLOTHING_TYPE', 3, 12, true);
INSERT INTO jhi_authority VALUES ('PERSON_AWARD', 3, 13, true);
INSERT INTO jhi_authority VALUES ('PERSON_INSTRUMENT', 3, 14, true);
INSERT INTO jhi_authority VALUES ('INSTRUMENT_SERVICE', 3, 15, true);
INSERT INTO jhi_authority VALUES ('COMPOSER', 3, 16, true);
INSERT INTO jhi_authority VALUES ('LETTER_TEMPLATE', 3, 17, true);
INSERT INTO jhi_authority VALUES ('CLOTHING', 3, 18, true);
INSERT INTO jhi_authority VALUES ('CUSTOMIZING', 3, 19, true);
INSERT INTO jhi_authority VALUES ('HONOR', 3, 20, true);
INSERT INTO jhi_authority VALUES ('MEMBERSHIP', 3, 21, true);
INSERT INTO jhi_authority VALUES ('APPOINTMENT_TYPE', 3, 22, true);
INSERT INTO jhi_authority VALUES ('APPOINTMENT', 3, 23, true);
INSERT INTO jhi_authority VALUES ('ARRANGER', 3, 24, true);
INSERT INTO jhi_authority VALUES ('COMPOSITION', 3, 25, true);
INSERT INTO jhi_authority VALUES ('PERSON_HONOR', 3, 26, true);
INSERT INTO jhi_authority VALUES ('PERSON_GROUP', 3, 27, true);
INSERT INTO jhi_authority VALUES ('APPOINTMENT_PEOPLE', 3, 28, true);
INSERT INTO jhi_authority VALUES ('MEMBERSHIP_CATEGORY', 3, 29, true);
INSERT INTO jhi_authority VALUES ('NOTIFICATION_RULE', 3, 30, true);
INSERT INTO jhi_authority VALUES ('INSTRUMENT_TYPE', 3, 31, true);
INSERT INTO jhi_authority VALUES ('PERSON', 3, 32, true);
INSERT INTO jhi_authority VALUES ('AKM', 3, 33, true);
INSERT INTO jhi_authority VALUES ('CASHBOOK-IMPORT', 3, 34, true);
INSERT INTO jhi_authority VALUES ('ICAL', 3, 35, true);
INSERT INTO jhi_authority VALUES ('PERSON_CLOTHING', 3, 36, true);
INSERT INTO jhi_authority VALUES ('GENRE', 3, 37, true);
INSERT INTO jhi_authority VALUES ('EMAIL_VERIFICATION_LETTER', 3, 38, true);
INSERT INTO jhi_authority VALUES ('USER', 3, 39, true);
INSERT INTO jhi_authority VALUES ('COLOR_CODE', 3, 40, true);
INSERT INTO jhi_authority VALUES ('COMPANY', 3, 41, true);
INSERT INTO jhi_authority VALUES ('INSTRUMENT', 3, 42, true);
INSERT INTO jhi_authority VALUES ('NOTIFICATION', 3, 43, true);
INSERT INTO jhi_authority VALUES ('APPOINTMENT_COMPOSITION', 3, 44, true);
INSERT INTO jhi_authority VALUES ('ROLES', 3, 45, true);
INSERT INTO jhi_authority VALUES ('DATA_IMPORT', 3, 46, true);


--
-- TOC entry 3784 (class 0 OID 86441)
-- Dependencies: 321
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3785 (class 0 OID 86447)
-- Dependencies: 322
-- Data for Name: jhi_persistent_audit_evt_data; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3780 (class 0 OID 86406)
-- Dependencies: 317
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3792 (class 0 OID 86498)
-- Dependencies: 329
-- Data for Name: role; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--


--
-- TOC entry 3782 (class 0 OID 86424)
-- Dependencies: 319
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3870 (class 0 OID 86879)
-- Dependencies: 407
-- Data for Name: template; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3872 (class 0 OID 86890)
-- Dependencies: 409
-- Data for Name: letter; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3873 (class 0 OID 86899)
-- Dependencies: 410
-- Data for Name: letter_person_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3798 (class 0 OID 86527)
-- Dependencies: 335
-- Data for Name: membership_fee; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO membership_fee VALUES (1, 'Ermässigt', 'ANNUALLY', 1);
INSERT INTO membership_fee VALUES (2, 'Vollzahler', 'ANNUALLY', 3);


--
-- TOC entry 3796 (class 0 OID 86519)
-- Dependencies: 333
-- Data for Name: membership; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO membership VALUES (1, '2018-01-01', NULL, 1, 2);
INSERT INTO membership VALUES (2, '2018-01-01', NULL, 2, 1);
INSERT INTO membership VALUES (3, '2018-01-01', NULL, 3, 2);


--
-- TOC entry 3800 (class 0 OID 86538)
-- Dependencies: 337
-- Data for Name: membership_fee_amount; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO membership_fee_amount VALUES (1, '2018-01-01', NULL, 25.00, 1);
INSERT INTO membership_fee_amount VALUES (2, '2018-01-01', NULL, 50.00, 2);


--
-- TOC entry 3802 (class 0 OID 86546)
-- Dependencies: 339
-- Data for Name: membership_fee_for_period; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO membership_fee_for_period VALUES (2, 1, 2019, '00002012019', false, NULL, NULL, 2);
INSERT INTO membership_fee_for_period VALUES (3, 1, 2018, '00001012018', true, NULL, NULL, 1);
INSERT INTO membership_fee_for_period VALUES (4, 1, 2018, '00002012018', false, NULL, NULL, 2);
INSERT INTO membership_fee_for_period VALUES (5, 1, 2019, '00003012019', false, NULL, NULL, 3);
INSERT INTO membership_fee_for_period VALUES (1, 1, 2019, '00001012019', true, NULL, 3, 1);


--
-- TOC entry 3875 (class 0 OID 86906)
-- Dependencies: 412
-- Data for Name: notification_rule; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3878 (class 0 OID 86922)
-- Dependencies: 415
-- Data for Name: notification; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3876 (class 0 OID 86915)
-- Dependencies: 413
-- Data for Name: notification_rule_target_audiences; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3828 (class 0 OID 86668)
-- Dependencies: 365
-- Data for Name: person_address; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3826 (class 0 OID 86660)
-- Dependencies: 363
-- Data for Name: person_award; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_award VALUES (1, '2018-09-04', 1, 1);
INSERT INTO person_award VALUES (2, '2018-04-01', 2, 1);


--
-- TOC entry 3814 (class 0 OID 86606)
-- Dependencies: 351
-- Data for Name: person_clothing; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_clothing VALUES (1, '2019-01-01', NULL, 1, 1);
INSERT INTO person_clothing VALUES (2, '2018-11-12', NULL, 36, 1);
INSERT INTO person_clothing VALUES (3, '2018-10-01', NULL, 16, 2);


--
-- TOC entry 3824 (class 0 OID 86652)
-- Dependencies: 361
-- Data for Name: person_honor; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_honor VALUES (1, '2018-11-01', 1, 1);
INSERT INTO person_honor VALUES (2, '2019-02-11', 2, 1);
INSERT INTO person_honor VALUES (3, '2019-02-06', 3, 2);


--
-- TOC entry 3794 (class 0 OID 86511)
-- Dependencies: 331
-- Data for Name: person_info; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3806 (class 0 OID 86570)
-- Dependencies: 343
-- Data for Name: person_instrument; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_instrument VALUES (1, '2018-01-01', '2018-10-31', 1, 1);
INSERT INTO person_instrument VALUES (2, '2018-09-01', NULL, 3, 1);
INSERT INTO person_instrument VALUES (3, '2018-11-01', NULL, 2, 2);


--
-- TOC entry 3788 (class 0 OID 86470)
-- Dependencies: 325
-- Data for Name: person_person_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

INSERT INTO person_person_groups VALUES (1, 1);
INSERT INTO person_person_groups VALUES (1, 2);
INSERT INTO person_person_groups VALUES (3, 1);
INSERT INTO person_person_groups VALUES (3, 2);
INSERT INTO person_person_groups VALUES (3, 3);
INSERT INTO person_person_groups VALUES (2, 1);
INSERT INTO person_person_groups VALUES (2, 3);


--
-- TOC entry 3837 (class 0 OID 86714)
-- Dependencies: 374
-- Data for Name: receipt; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--



--
-- TOC entry 3891 (class 0 OID 87371)
-- Dependencies: 428
-- Data for Name: user_column_config; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--


