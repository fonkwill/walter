package at.simianarmy.service;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.repository.PersonRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonServiceRelevanceTest {

  @Mock
  private PersonGroupService personGroupService;

  @Mock
  private CustomizationService customizationService;

  @Mock
  private PersonRepository personRepository;

  @InjectMocks
  private PersonService sut;


  private Person person1;

  private Person person2;

  private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.of(1960, 1, 1);
  private static final String DEFAULT_FIRSTNAME = "Vorname";
  private static final String DEFAULT_LASTNAME = "Nachname";
  private static final String DEFAULT_STREET = "Straße";
  private static final String DEFAULT_PLZ = "12345";
  private static final String DEFAULT_CITY = "Ort";
  private static final String DEFAULT_COUNTRY = "AT";

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    person1 = new Person();
    person1.setFirstName(DEFAULT_FIRSTNAME + "1");
    person1.setLastName(DEFAULT_LASTNAME + "1");
    person1.setBirthDate(DEFAULT_BIRTHDATE);
    person1.setStreetAddress(DEFAULT_STREET + " 1");
    person1.setPostalCode(DEFAULT_PLZ);
    person1.setCity(DEFAULT_CITY);
    person1.setCountry(DEFAULT_COUNTRY);
    person1.setGender(Gender.FEMALE);
    person1.setHasDirectDebit(false);
    person1.setDateLostRelevance(null);

    person2 = new Person();
    person2.setFirstName(DEFAULT_FIRSTNAME + "2");
    person2.setLastName(DEFAULT_LASTNAME + "2");
    person2.setBirthDate(DEFAULT_BIRTHDATE);
    person2.setStreetAddress(DEFAULT_STREET + " 2");
    person2.setPostalCode(DEFAULT_PLZ);
    person2.setCity(DEFAULT_CITY);
    person2.setCountry(DEFAULT_COUNTRY);
    person2.setGender(Gender.FEMALE);
    person2.setHasDirectDebit(false);
    person2.setDateLostRelevance(null);
  }

  @Test
  public void testMarkNotRelevant_MarkNotRelevant() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());


    List<Person> markNotRelevant = new ArrayList<>();
    person1.setId(1L);
    person2.setId(2L);
    markNotRelevant.add(person1);
    markNotRelevant.add(person2);

    Membership m1 = new Membership();
    m1.setBeginDate(LocalDate.of(2017, 01, 01));
    m1.setEndDate(LocalDate.of(2018, 12, 31));
    Membership m2 = new Membership();
    m2.setBeginDate(LocalDate.of(2018, 1, 1));
    LocalDate lastEndDate = LocalDate.of(2019, 1, 15);
    m2.setEndDate(lastEndDate);

    person1.addMemberships(m1);
    person1.addMemberships(m2);

    Mockito.when(personRepository.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(Mockito.any(), Mockito.any())).thenReturn(markNotRelevant);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    Mockito.when(personRepository.saveAll(ArgumentMatchers.anyList())).thenReturn(new ArrayList<>());


    LocalDate due = LocalDate.of(2019, 2, 5);

    sut.markNotRelevantPersons(due);


    Mockito.verify(personRepository).saveAll(persons.capture());
    List<Person> res = persons.getValue();

    assertThat(res, hasSize(2));

    Person p1result = null;
    Person p2result = null;
    for (Person p : res) {
      if (p.getId().equals(1L)) {
        p1result = p;
      }
      if (p.getId().equals(2L)) {
        p2result = p;
      }
    }

    assertThat(p2result, hasProperty("dateLostRelevance", equalTo(due)));
    assertThat(p1result, hasProperty("dateLostRelevance", equalTo(lastEndDate)));

  }

  @Test
  public void testMarkNotRelevantNotNeeded_markNone() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());


    List<Person> markNotRelevant = new ArrayList<>();

    Mockito.when(personRepository.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(Mockito.any(), Mockito.any())).thenReturn(markNotRelevant);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    Mockito.when(personRepository.saveAll(ArgumentMatchers.anyList())).thenReturn(new ArrayList<>());


    LocalDate due = LocalDate.of(2019, 2, 5);

    sut.markNotRelevantPersons(due);

    Mockito.verify(personRepository, Mockito.times(0)).saveAll(Mockito.any());

  }

  @Test
  public void testUnmarkRelevant_UnmarkRelevant() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());

    LocalDate lostRelevance = LocalDate.of(2018, 2, 5);

    person1.setDateLostRelevance(lostRelevance);
    person2.setDateLostRelevance(lostRelevance);

    List<Person> unmarkRelevant = new ArrayList<>();
    unmarkRelevant.add(person1);
    unmarkRelevant.add(person2);

    Mockito.when(personRepository.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(Mockito.any(), Mockito.any())).thenReturn(unmarkRelevant);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    Mockito.when(personRepository.saveAll(ArgumentMatchers.anyList())).thenReturn(new ArrayList<>());


    LocalDate due = LocalDate.of(2019, 2, 5);

    sut.unmarkRelevantPersons(due);

    Mockito.verify(personRepository).saveAll(persons.capture());
    List<Person> res = persons.getValue();

    assertThat(res, hasSize(2));

    assertThat(res.get(0), hasProperty("dateLostRelevance", nullValue()));
    assertThat(res.get(1), hasProperty("dateLostRelevance", nullValue()));

  }

  @Test
  public void testUnmarkRelevantNotNeeden_UnmarkNone() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());

    LocalDate lostRelevance = LocalDate.of(2018, 2, 5);

    person1.setDateLostRelevance(lostRelevance);
    person2.setDateLostRelevance(lostRelevance);

    List<Person> unmarkRelevant = new ArrayList<>();

    Mockito.when(personRepository.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(Mockito.any(), Mockito.any())).thenReturn(unmarkRelevant);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    Mockito.when(personRepository.saveAll(ArgumentMatchers.anyList())).thenReturn(new ArrayList<>());

    LocalDate due = LocalDate.of(2019, 2, 5);

    sut.unmarkRelevantPersons(due);

    Mockito.verify(personRepository, Mockito.times(0)).saveAll(Mockito.any());

  }


  @Test
  public void deleteBothPeriodOver_DeleteBoth() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());
    Mockito.when(customizationService.getMonthsToDeletePersonData()).thenReturn(2L);

    LocalDate lostRelevance = LocalDate.of(2018, 12, 31);

    person1.setDateLostRelevance(lostRelevance);
    person2.setDateLostRelevance(lostRelevance);

    List<Person> toDelete = new ArrayList<>();
    toDelete.add(person1);
    toDelete.add(person2);

    Mockito.when(personRepository.findAllByDateLostRelevanceIsBefore(LocalDate.of(2019, 1, 1))).thenReturn(toDelete);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    LocalDate due = LocalDate.of(2019, 3, 1);

    sut.deletePersonsWhichAreNotRelevant(due);

    Mockito.verify(personRepository).deleteAll(persons.capture());
    List<Person> res = persons.getValue();

    assertThat(res, hasSize(2));

    assertThat(res, hasItem(person1));
    assertThat(res, hasItem(person2));

  }

  @Test
  public void deleteOnePeriodOverOneNot_DeleteOne() {

    Mockito.when(personGroupService.getAllPersonRelevantPersonGroups()).thenReturn(new ArrayList<>());
    Mockito.when(customizationService.getMonthsToDeletePersonData()).thenReturn(2L);

    person1.setDateLostRelevance(LocalDate.of(2018, 12, 31));
    person2.setDateLostRelevance(LocalDate.of(2019, 1, 1));

    List<Person> toDelete = new ArrayList<>();
    toDelete.add(person1);

    Mockito.when(personRepository.findAllByDateLostRelevanceIsBefore(LocalDate.of(2019, 1, 1))).thenReturn(toDelete);

    ArgumentCaptor<List<Person>> persons = ArgumentCaptor.forClass(List.class);

    LocalDate due = LocalDate.of(2019, 3, 1);

    sut.deletePersonsWhichAreNotRelevant(due);

    Mockito.verify(personRepository).deleteAll(persons.capture());
    List<Person> res = persons.getValue();

    assertThat(res, hasSize(1));
    assertThat(res, hasItem(person1));


  }




}
