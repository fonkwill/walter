package at.simianarmy.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.service.dto.CashbookCategoryDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookCategoryMapper;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookCategoryServiceTest {

  private static final Long id1 = new Long(1);
  private static final Long id2 = new Long(2);
  private static final Long id3 = new Long(3);

  private static final String name1 = "name1";
  private static final String name2 = "name2";
  private static final String name3 = "name3";

  private CashbookCategory cc1;
  private CashbookCategory cc2;
  private CashbookCategory cc3;

  @Mock		
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  @Spy
  private CashbookCategoryMapper cashbookCategoryMapper;
  
  @InjectMocks
  private CashbookCategoryService cashbookCategoryService;

 

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    cc1 = new CashbookCategory();
    cc1.setId(id1);
    cc1.setName(name1);
    cc2 = new CashbookCategory();
    cc2.setId(id2);
    cc2.setName(name2);
    cc3 = new CashbookCategory();
    cc3.setId(id3);
    cc3.setName(name3);

    cc1.addChildren(cc2);
    cc2.addChildren(cc3);

    when(cashbookCategoryRepository.findById(id1)).thenReturn(Optional.of(cc1));
    when(cashbookCategoryRepository.findById(id2)).thenReturn(Optional.of(cc2));
    when(cashbookCategoryRepository.findById(id3)).thenReturn(Optional.of(cc2));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDeleteParentShouldBeNull() {
    cashbookCategoryService.delete(cc1.getId());
    assertThat(cc2.getParent(), nullValue());
    assertThat(cc3.getParent(), equalTo(cc2));
  }

  @Test
  public void testDeleteParentShouldBeParentParent() {
    cashbookCategoryService.delete(cc2.getId());
    assertThat(cc3.getParent(), equalTo(cc1));
    assertThat(cc1.getParent(), nullValue());

  }

  @Test(expected = ServiceValidationException.class)
  public void testCircularReference() {
    cc3.addChildren(cc1);
    // cc3->cc1->cc2->cc3
    // Must not be saveable
    CashbookCategoryDTO cc1dto = cashbookCategoryMapper.cashbookCategoryToCashbookCategoryDTO(cc1);
    cashbookCategoryService.save(cc1dto);
  }

  @Test
  public void testDelete() {
    this.cashbookCategoryRepository.deleteById(id1);

    Mockito.verify(this.cashbookCategoryRepository, Mockito.times(1)).deleteById(id1);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShouldThrowException() {
    this.cc1.addCashbookEntries(Mockito.mock(CashbookEntry.class));

    this.cashbookCategoryService.delete(id1);
  }

}
