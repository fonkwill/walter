package at.simianarmy.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.EmailVerificationLinkDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.EmailVerificationLinkMapper;
import at.simianarmy.service.mapper.PersonMapper;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmailVerificationLinkServiceTest {

  private static final Long DEFAULT_PERSON_ID = new Long(1);
  private static final String DEFAULT_SECRET_KEY = "ABCD1234";
  private static final String DEFAULT_EMAIL = "person@walter.com";

  @Mock
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Mock
  private PersonRepository personRepository;

  @Mock
  private EmailVerificationLinkMapper emailVerificationLinkMapper;

  @Mock
  private PersonMapper personMapper;

  @InjectMocks
  private EmailVerificationLinkService sut;

  @Test
  public void testCreateEmailVerificationLinkForPerson() {

    Person person = this.newPersonMock(DEFAULT_PERSON_ID);
    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(false);

    this.sut.createEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);

    ArgumentCaptor<EmailVerificationLink> linkCaptor = ArgumentCaptor.forClass(EmailVerificationLink.class);
    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).save(linkCaptor.capture());
    EmailVerificationLink capturedLink = linkCaptor.getValue();
    assertEquals(person, capturedLink.getPerson());
    assertNotNull(capturedLink.getCreationDate());
    assertNull(capturedLink.getValidationDate());
    assertFalse(capturedLink.isInvalid());
    assertNotNull(capturedLink.getSecretKey());
  }

  @Test(expected = ServiceValidationException.class)
  public void testCreateEmailVerificationLinkForPersonWithActiveLink() {

    Person person = this.newPersonMock(DEFAULT_PERSON_ID);
    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(true);

    this.sut.createEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testCreateEmailVerificationLinkForNonExistingPerson() {
    this.sut.createEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);
  }

  @Test
  public void testGetEmailVerificationLinkForPerson() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);
    EmailVerificationLink emailVerificationLink = Mockito.mock(EmailVerificationLink.class);
    EmailVerificationLinkDTO emailVerificationLinkDTO = Mockito.mock(EmailVerificationLinkDTO.class);
    Mockito.when(this.emailVerificationLinkMapper.toDto(emailVerificationLink)).thenReturn(emailVerificationLinkDTO);

    Mockito.when(this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(emailVerificationLink);

    EmailVerificationLinkDTO result = this.sut.getActiveEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);

    assertEquals(emailVerificationLinkDTO, result);
  }

  @Test
  public void testGetEmailVerificationLinkForPersonNoneActive() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);

    Mockito.when(this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(null);

    EmailVerificationLinkDTO result = this.sut.getActiveEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);

    Mockito.verify(this.emailVerificationLinkMapper, Mockito.times(1)).toDto(
      (EmailVerificationLink) null);
    assertNull(result);
  }

  @Test
  public void testInvalidateEmailVerificationLinkForPerson() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);
    EmailVerificationLink link = Mockito.mock(EmailVerificationLink.class);

    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(true);
    Mockito.when(this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(link);

    this.sut.invalidateEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);

    Mockito.verify(link, Mockito.times(1)).setInvalid(true);
    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).save(link);
  }

  @Test(expected = ServiceValidationException.class)
  public void testInvalidateEmailVerificationLinkForPersonNoneActive() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);

    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(false);

    this.sut.invalidateEmailVerificationLinkForPerson(DEFAULT_PERSON_ID);
  }

  @Test
  public void testVerifyEmailVerificationLink() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);
    EmailVerificationLink link = Mockito.mock(EmailVerificationLink.class);
    Mockito.when(link.getSecretKey()).thenReturn(DEFAULT_SECRET_KEY);

    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(true);
    Mockito.when(this.emailVerificationLinkRepository.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(link);

    this.sut.verifyEmailVerificationLinkForPersonWithEmailAndSecretKey(DEFAULT_PERSON_ID, DEFAULT_EMAIL, DEFAULT_SECRET_KEY);

    Mockito.verify(person, Mockito.times(1)).setEmail(DEFAULT_EMAIL);
    Mockito.verify(this.personRepository, Mockito.times(1)).save(person);
    Mockito.verify(link, Mockito.times(1)).setValidationDate(Mockito.any(ZonedDateTime.class));
    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).save(link);
  }

  @Test(expected = ServiceValidationException.class)
  public void testVerifyEmailVerificationLinkNoneActive() {
    Person person = this.newPersonMock(DEFAULT_PERSON_ID);

    Mockito.when(this.emailVerificationLinkRepository.existsByPersonAndInvalidFalseAndValidationDateIsNull(person)).thenReturn(false);

    this.sut.verifyEmailVerificationLinkForPersonWithEmailAndSecretKey(DEFAULT_PERSON_ID, DEFAULT_EMAIL, DEFAULT_SECRET_KEY);
  }

  private Person newPersonMock(Long id) {
    Person person = Mockito.mock(Person.class);
    Mockito.when(this.personRepository.findById(id)).thenReturn(Optional.ofNullable(person));

    return person;
  }


}
