package at.simianarmy.service;

import at.simianarmy.domain.Composition;
import at.simianarmy.domain.Genre;
import at.simianarmy.repository.GenreRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GenreServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private GenreRepository genreRepository;

  @InjectMocks
  private GenreService genreService;

  private Genre genre;

  @Before
  public void setUp() {
    this.genre = Mockito.mock(Genre.class);
    Mockito.when(this.genreRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.genre));
  }

  @Test
  public void testDelete() {
    this.genreService.delete(DEFAULT_ID);
    Mockito.verify(this.genreRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShoulThrowException() {
    Mockito.when(this.genre.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.genreService.delete(DEFAULT_ID);
  }

}