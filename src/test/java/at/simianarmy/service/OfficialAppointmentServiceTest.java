package at.simianarmy.service;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.Composition;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.repository.OfficialAppointmentRepository;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.dto.OfficialAppointmentDTO;
import at.simianarmy.service.mapper.CompositionMapper;
import at.simianarmy.service.mapper.OfficialAppointmentMapper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class OfficialAppointmentServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private OfficialAppointmentRepository officialAppointmentRepository;

  @Mock
  private CompositionRepository compositionRepository;

  @Mock
  private OfficialAppointmentMapper officialAppointmentMapper;

  @Mock
  private CompositionMapper compositionMapper;

  @InjectMocks
  private OfficialAppointmentService officialAppointmentService;

  @Test
  public void testAddComposition() {
    OfficialAppointment officialAppointment = Mockito.mock(OfficialAppointment.class);
    Composition composition = Mockito.mock(Composition.class);
    OfficialAppointmentDTO officialAppointmentDTO = Mockito.mock(OfficialAppointmentDTO.class);
    CompositionDTO compositionDTO = Mockito.mock(CompositionDTO.class);

    Mockito.when(this.officialAppointmentRepository.findById(DEFAULT_ID))
        .thenReturn(Optional.ofNullable(officialAppointment));
    Mockito.when(this.compositionMapper.compositionDTOToComposition(compositionDTO))
        .thenReturn(composition);
    Mockito.when(officialAppointment.getCompositions()).thenReturn(new HashSet<Composition>());
    Mockito
        .when(officialAppointmentMapper
            .officialAppointmentToOfficialAppointmentDTO(officialAppointment))
        .thenReturn(officialAppointmentDTO);

    assertEquals(this.officialAppointmentService.addComposition(DEFAULT_ID, compositionDTO),
        officialAppointmentDTO);

    Mockito.verify(this.officialAppointmentRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionDTOToComposition(compositionDTO);
    Mockito.verify(composition, Mockito.times(1)).addAppointments(officialAppointment);
    Mockito.verify(this.compositionRepository, Mockito.times(1))
        .saveAndFlush(composition);
  }

  @Test
  public void testAddCompositionAlreadyExisting() {
    OfficialAppointment officialAppointment = Mockito.mock(OfficialAppointment.class);
    Composition composition = Mockito.mock(Composition.class);
    OfficialAppointmentDTO officialAppointmentDTO = Mockito.mock(OfficialAppointmentDTO.class);
    CompositionDTO compositionDTO = Mockito.mock(CompositionDTO.class);

    Mockito.when(this.officialAppointmentRepository.findById(DEFAULT_ID))
        .thenReturn(Optional.ofNullable(officialAppointment));
    Mockito.when(this.compositionMapper.compositionDTOToComposition(compositionDTO))
        .thenReturn(composition);
    Mockito.when(officialAppointment.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(composition)));
    Mockito
        .when(officialAppointmentMapper
            .officialAppointmentToOfficialAppointmentDTO(officialAppointment))
        .thenReturn(officialAppointmentDTO);

    assertEquals(this.officialAppointmentService.addComposition(DEFAULT_ID, compositionDTO),
        officialAppointmentDTO);

    Mockito.verify(this.officialAppointmentRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionDTOToComposition(compositionDTO);
    Mockito.verify(composition, Mockito.never()).addAppointments(ArgumentMatchers.any());
    Mockito.verify(this.compositionRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
  }

  @Test
  public void testRemoveComposition() {
    OfficialAppointment officialAppointment = Mockito.mock(OfficialAppointment.class);
    Composition composition = Mockito.mock(Composition.class);

    Mockito.when(this.officialAppointmentRepository.findById(DEFAULT_ID))
        .thenReturn(Optional.ofNullable(officialAppointment));
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));

    this.officialAppointmentService.removeComposition(DEFAULT_ID, DEFAULT_ID);

    Mockito.verify(composition, Mockito.times(1)).removeAppointments(officialAppointment);
    Mockito.verify(this.compositionRepository, Mockito.times(1))
        .saveAndFlush(composition);
  }

  @Test
  public void testRemoveCompositionNotExisting() {
    OfficialAppointment officialAppointment = Mockito.mock(OfficialAppointment.class);
    
    Mockito.when(this.officialAppointmentRepository.findById(DEFAULT_ID))
        .thenReturn(Optional.ofNullable(officialAppointment));
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.empty());

    this.officialAppointmentService.removeComposition(DEFAULT_ID, DEFAULT_ID);

    Mockito.verify(this.compositionRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
  }

}
