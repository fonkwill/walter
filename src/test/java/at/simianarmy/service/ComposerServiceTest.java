package at.simianarmy.service;

import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ComposerServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private ComposerRepository composerRepository;

  @InjectMocks
  private ComposerService composerService;

  private Composer composer;

  @Before
  public void setUp() {
    this.composer = Mockito.mock(Composer.class);
    Mockito.when(this.composerRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.composer));
  }

  @Test
  public void testDelete() {
    this.composerService.delete(DEFAULT_ID);
    Mockito.verify(this.composerRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShoulThrowException() {
    Mockito.when(this.composer.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.composerService.delete(DEFAULT_ID);
  }

}