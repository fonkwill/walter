package at.simianarmy.service;

import static org.junit.Assert.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonClothing;
import at.simianarmy.repository.PersonClothingRepository;
import at.simianarmy.service.dto.PersonClothingDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonClothingMapper;

import java.time.LocalDate;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonClothingServiceTest {

	private PersonClothing pi1;
	private PersonClothingDTO piDto1;


	@Mock
	private PersonClothingRepository personClothingRepository;
	
	@Spy
	@Inject
	private PersonClothingMapper personClothingMapper;
	
	@InjectMocks
	private PersonClothingService personClothingService;

	


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		pi1 = new PersonClothing();

		Mockito.when(personClothingRepository.save(ArgumentMatchers.any(PersonClothing.class)))
				.then(AdditionalAnswers.returnsFirstArg());
	}

	@After
	public void tearDown() throws Exception {
		pi1 = null;

	}

	@Test(expected = ServiceValidationException.class)
	public void testBeginAfterEnd() {
		pi1.setBeginDate(LocalDate.of(2016, 10, 15));
		pi1.setEndDate(LocalDate.of(2016, 9, 15));
		piDto1 = personClothingMapper.personClothingToPersonClothingDTO(pi1);

		personClothingService.save(piDto1);

	}

	@Test
	public void testBeginBeforeEnd() {
		PersonClothingDTO piDtol;

		pi1.setBeginDate(LocalDate.of(2016, 9, 15));
		pi1.setEndDate(LocalDate.of(2016, 10, 15));

		piDto1 = personClothingMapper.personClothingToPersonClothingDTO(pi1);

		piDtol = personClothingService.save(piDto1);

		assertThat(piDtol, Matchers.equalTo(piDto1));
	}

	@Test
	public void testBeginEqualsEnd() {
		PersonClothingDTO piDtol;

		pi1.setBeginDate(LocalDate.of(2016, 9, 15));
		pi1.setEndDate(LocalDate.of(2016, 9, 15));

		piDto1 = personClothingMapper.personClothingToPersonClothingDTO(pi1);

		piDtol = personClothingService.save(piDto1);

		assertThat(piDtol, Matchers.equalTo(piDto1));
	}

	@Test
	public void testEndisNull() {
		PersonClothingDTO piDtol;

		pi1.setBeginDate(LocalDate.of(2016, 9, 15));
		pi1.setEndDate(null);

		piDto1 = personClothingMapper.personClothingToPersonClothingDTO(pi1);

		piDtol = personClothingService.save(piDto1);

		assertThat(piDtol, Matchers.equalTo(piDto1));

	}

}
