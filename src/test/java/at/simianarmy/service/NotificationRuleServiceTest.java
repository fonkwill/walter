package at.simianarmy.service;

import at.simianarmy.WalterApp;
import at.simianarmy.service.dto.NotificationRuleDTO;
import at.simianarmy.service.exception.ServiceValidationException;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class NotificationRuleServiceTest {

  @Inject
  private NotificationRuleService notificationRuleService;

  @Test(expected = ServiceValidationException.class)
  public void testEntityClassNotFound() {
    NotificationRuleDTO ruleDTO = new NotificationRuleDTO();
    ruleDTO.setConditions("select person from Person person");
    ruleDTO.setEntity("InvalidClazz"); // invalid class name
    ruleDTO.setMessageGroup("message for group.");
    ruleDTO.setMessageSingle("message for group.");
    ruleDTO.setTitle("Test Rule");
    ruleDTO.setActive(true);

    notificationRuleService.save(ruleDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUnkownEntityClass() {
    NotificationRuleDTO ruleDTO = new NotificationRuleDTO();
    ruleDTO.setConditions("select person from Person person");
    ruleDTO.setEntity("NotificationRule"); // valid but does not implement NotificationEntity
                                           // Interface
    ruleDTO.setMessageGroup("message for group.");
    ruleDTO.setMessageSingle("message for group.");
    ruleDTO.setTitle("Test Rule");
    ruleDTO.setActive(true);

    notificationRuleService.save(ruleDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testInvalidQuerySyntax() {
    NotificationRuleDTO ruleDTO = new NotificationRuleDTO();
    ruleDTO.setConditions("selecte person from Xxblubb person");
    ruleDTO.setEntity("Person");
    ruleDTO.setMessageGroup("message for group.");
    ruleDTO.setMessageSingle("message for group.");
    ruleDTO.setTitle("Test Rule");
    ruleDTO.setActive(true);

    notificationRuleService.save(ruleDTO);
  }

}
