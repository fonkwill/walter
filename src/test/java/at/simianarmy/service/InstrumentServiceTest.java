package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.repository.InstrumentRepository;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.mapper.InstrumentMapper;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentServiceTest {

	private static final Long id1 = new Long(1);
	private static final Long id2 = new Long(2);
	private static final Long id3 = new Long(3);
	private static final Long id4 = new Long(4);
	
	private static final Long typeId = new Long(10);
	
	private static final String name1 = "name1";
	private static final String name2 = "name2";
	private static final String name3 = "name3";
	private static final String name4 = "name4";

	private static final LocalDate badDate = LocalDate.now().plus(2, ChronoUnit.DAYS);
	
	private static final InstrumentType it1 = new InstrumentType();
	
	private static final at.simianarmy.domain.InstrumentService is1 = new at.simianarmy.domain.InstrumentService();
	private static final Set<at.simianarmy.domain.InstrumentService> is = new HashSet<at.simianarmy.domain.InstrumentService>();
	
	private static final PersonInstrument pi1 = new PersonInstrument();
	
	private Instrument instrument1;
	private Instrument instrument2;
	private Instrument instrument3;
	private Instrument instrument4;
	
	@Mock
	private InstrumentRepository InstrumentRepository;
	
	@Inject
	@Spy
	private InstrumentMapper InstrumentMapper;

	@Inject
  private CustomizationService customizationService;

	@InjectMocks
	private InstrumentService InstrumentService;

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		instrument1 = new Instrument();
		instrument1.setId(id1);
		instrument1.setName(name1);
		instrument2 = new Instrument();
		instrument2.setId(id2);
		instrument2.setName(name2);
		instrument3 = new Instrument();
		instrument3.setId(id3);
		instrument3.setName(name3);
		instrument4 = new Instrument();
    instrument4.setId(id4);
    instrument4.setName(name4);
		
		instrument1.addInstrumentServices(is1); //must not be deleted
		instrument2.setInstrumentServices(is); //must be deleted
		
		instrument4.addPersonInstruments(pi1); //must not be deleted
		
		it1.setId(typeId);
		
		instrument1.setType(it1);
		instrument2.setType(it1);
		//instrument3 must not be saved
		instrument4.setType(it1);
		
		when(InstrumentRepository.findById(id1)).thenReturn(Optional.ofNullable(instrument1));
		when(InstrumentRepository.findById(id2)).thenReturn(Optional.ofNullable(instrument2));
		when(InstrumentRepository.findById(id3)).thenReturn(Optional.ofNullable(instrument3));
		when(InstrumentRepository.findById(id4)).thenReturn(Optional.ofNullable(instrument4));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithInstrumentService() {
	  InstrumentService.delete(instrument1.getId());
	}
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveWithBadDate() {
    instrument2.setPurchaseDate(badDate);
    InstrumentDTO instrument2dto = InstrumentMapper.instrumentToInstrumentDTO(instrument2);
    InstrumentService.save(instrument2dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testDeleteWithPersonInstrument() {
    InstrumentService.delete(instrument4.getId());
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveWithoutType() {
    InstrumentDTO instrument3dto = InstrumentMapper.instrumentToInstrumentDTO(instrument3);
    InstrumentService.save(instrument3dto);
  }
}
