package at.simianarmy.service;

import static org.junit.Assert.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Customization;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.repository.CustomizationRepository;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MembershipMapper;
import at.simianarmy.service.mapper.PersonMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class MembershipServiceTest {

	private Membership pi1;
	private MembershipDTO piDto1;
	private Person person;
	private Customization customization;
	private PersonGroup membershipGroup;
	private List<Membership> membershipsToday;

	@Mock
	private MembershipRepository membershipRepository;

	@Mock
	private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;

	@Inject
	@Spy
	private MembershipMapper membershipMapper;

	@Mock
	private PersonRepository personRepository;

	@Mock
	private CustomizationRepository customizationRepository;
	
	@Mock
	private PersonGroupRepository personGroupRepository;
	
	@Mock
	private CustomizationService customizationService;
	
	@Inject
	@Spy
	private PersonMapper personMapper;
	
	@InjectMocks
	private MembershipService membershipService;

	@InjectMocks
	private PersonService personService;
	

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		membershipsToday = new ArrayList<>();
		MockitoAnnotations.initMocks(membershipRepository);
		MockitoAnnotations.initMocks(membershipFeeForPeriodRepository);
		MockitoAnnotations.initMocks(personRepository);
		MockitoAnnotations.initMocks(customizationRepository);
		MockitoAnnotations.initMocks(personGroupRepository);
		customization = new Customization();
		membershipGroup = new PersonGroup();
		person = new Person();
		person.setId(Long.valueOf(1));
		
		pi1 = new Membership();

		person.addMemberships(pi1);
		pi1.setPerson(person);

		
		customization.setId(Long.valueOf(2));
		customization.setData("2");
		customization.setName(CustomizationName.MEMBER_PERSONGROUP);

	    membershipGroup.setId(Long.valueOf(2));
	    membershipGroup.setName("Membership");
	
		
		
		List<Membership> list = new ArrayList<>();
		list.add(pi1);
		Mockito.when(membershipRepository.save(ArgumentMatchers.any(Membership.class)))
				.then(AdditionalAnswers.returnsFirstArg());
		Mockito.when(membershipRepository.findAll(ArgumentMatchers.any(Specification.class))).thenReturn(list);

		Mockito.when(membershipFeeForPeriodRepository.save(ArgumentMatchers.any(MembershipFeeForPeriod.class)))
				.then(i -> i.getArgument(0));

		Mockito.when(personRepository.findById(Long.valueOf(1))).thenReturn(Optional.ofNullable(person));
		
		Mockito.when(customizationRepository.findOne(ArgumentMatchers.any(Example.class))).thenReturn(Optional.ofNullable(customization));
		
		Mockito.when(membershipRepository.findByPersonAndDueDate(ArgumentMatchers.anyLong(), ArgumentMatchers.any())).thenReturn(membershipsToday);
		
		Mockito.when(personGroupRepository.findById(Long.valueOf(2))).thenReturn(Optional.ofNullable(membershipGroup));
		Mockito.when(customizationService.getMemberPersonGroup()).thenReturn(new Long(2));
	
	}

	@After
	public void tearDown() throws Exception {
		pi1 = null;

	}

	@Test(expected = ServiceValidationException.class)
	public void testBeginAfterEnd() {
		pi1.setBeginDate(LocalDate.of(2016, 10, 15));
		pi1.setEndDate(LocalDate.of(2016, 9, 15));
		piDto1 = membershipMapper.membershipToMembershipDTO(pi1);

		membershipService.save(piDto1);

	}

	@Test
	public void testReferenceCode() {
		pi1.setId(new Long(1));
		pi1.setBeginDate(LocalDate.of(2016, 01, 01));

		membershipService.createMembershipFeeForPeriodForMembership();

		assertThat(pi1.getMembershipFeeForPeriods(), Matchers.not(Matchers.empty()));
		for (MembershipFeeForPeriod mfp : pi1.getMembershipFeeForPeriods()) {
			assertThat(mfp.getReferenceCode(), Matchers.equalTo("0000101" + LocalDate.now().getYear()));
			assertThat(mfp.getNr(), Matchers.equalTo(1));
			assertThat(mfp.getYear(), Matchers.equalTo(LocalDate.now().getYear()));
			assertThat(mfp.getMembership(), Matchers.equalTo(pi1));
		}
		;

	}
	
	@Test
	public void testUpdateMembers_HasMembership_HasMembershipGroup() {
		person.addPersonGroups(membershipGroup);
		membershipsToday.add(pi1);
		
		PersonGroup pg2 = new PersonGroup();
		pg2.setId(Long.valueOf(4));
		pg2.setName("abc");
		person.addPersonGroups(pg2);
		
		Person p = membershipService.getUpdatedPersonWithMembershipGroup(Long.valueOf(1));
		assertThat(p, Matchers.nullValue());
	}
	
	@Test
	public void testUpdateMembers_HasMembership_HasNotMembershipGroup() {
		membershipsToday.add(pi1);
		
		PersonGroup pg2 = new PersonGroup();
		pg2.setId(Long.valueOf(4));
		pg2.setName("abc");
		person.addPersonGroups(pg2);
		
		Person p = membershipService.getUpdatedPersonWithMembershipGroup(Long.valueOf(1));
		assertThat(p.getPersonGroups(), Matchers.containsInAnyOrder(membershipGroup, pg2));
	}
	
	@Test
	public void testUpdateMembers_HasNoMembership_HasNotMembershipGroup() {
		
		PersonGroup pg2 = new PersonGroup();
		pg2.setId(Long.valueOf(4));
		pg2.setName("abc");
		person.addPersonGroups(pg2);
		
		Person p = membershipService.getUpdatedPersonWithMembershipGroup(Long.valueOf(1));
		assertThat(p, Matchers.nullValue());
	}
	
	@Test
	public void testUpdateMembers_HasNoMembership_HasMembershipGroup() {
		person.addPersonGroups(membershipGroup);
		
		PersonGroup pg2 = new PersonGroup();
		pg2.setId(Long.valueOf(4));
		pg2.setName("abc");
		person.addPersonGroups(pg2);
		
		Person p = membershipService.getUpdatedPersonWithMembershipGroup(Long.valueOf(1));
		assertThat(p.getPersonGroups(), Matchers.containsInAnyOrder(pg2));
	}
	
	


}
