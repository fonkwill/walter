package at.simianarmy.service;

import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ColorCodeServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private ColorCodeRepository colorCodeRepository;

  @InjectMocks
  private ColorCodeService colorCodeService;

  private ColorCode colorCode;

  @Before
  public void setUp() {
    this.colorCode = Mockito.mock(ColorCode.class);
    Mockito.when(this.colorCodeRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(this.colorCode));
  }

  @Test
  public void testDelete() {
    this.colorCodeService.delete(DEFAULT_ID);
    Mockito.verify(this.colorCodeRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShoulThrowException() {
    Mockito.when(this.colorCode.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.colorCodeService.delete(DEFAULT_ID);
  }

}