package at.simianarmy.service;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.dto.CashbookAccountDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookAccountMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class CashbookAccountServiceTest {

  private final static Long DEFAULT_ID = Long.valueOf(1);
  private final static String DEFAULT_NAME = "NAME";
  private final static Boolean DEFAULT_BANK_ACCOUNT = false;
  private final static String DEFAULT_RECEIPT_CODE = "N";
  private final static String DEFAULT_RECEIPT_CODE2 = "O";
  private final static String DEFAULT_TEXT_TO = "TO";
  private final static String DEFAULT_TEXT_FROM = "FROM";
  private final static BankImporterType DEFAULT_BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;

  @Mock
  private CashbookAccountRepository cashbookAccountRepository;

  @Mock
  private CashbookAccountMapper cashbookAccountMapper;

  @InjectMocks
  private CashbookAccountService cashbookAccountService;

  private CashbookAccount newCashbookAccount;

  private CashbookAccountDTO newCashbookAccountDTO;

  private CashbookAccount cashbookAccount;

  private CashbookAccountDTO cashbookAccountDTO;

  @Before
  public void setUp() {
    this.newCashbookAccountDTO = Mockito.mock(CashbookAccountDTO.class);
    this.newCashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccountMapper.toEntity(this.newCashbookAccountDTO)).thenReturn(this.newCashbookAccount);

    this.cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);
    this.cashbookAccountDTO = Mockito.mock(CashbookAccountDTO.class);
    Mockito.when(this.cashbookAccountRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.cashbookAccount));
    Mockito.when(this.cashbookAccountDTO.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(this.cashbookAccountDTO.getName()).thenReturn(DEFAULT_NAME);
    Mockito.when(this.cashbookAccountDTO.getBankImporterType()).thenReturn(DEFAULT_BANK_IMPORTER_TYPE);
    Mockito.when(this.cashbookAccountDTO.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);
    Mockito.when(this.cashbookAccountDTO.isBankAccount()).thenReturn(DEFAULT_BANK_ACCOUNT);
  }

  @Test
  public void testSave() {
    Mockito.when(this.newCashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);

    this.cashbookAccountService.save(this.newCashbookAccountDTO);

    Mockito.verify(this.newCashbookAccount, Mockito.times(1)).setCurrentNumber(
      CashbookAccountService.START_CURRENT_NUMBER
    );
    Mockito.verify(this.cashbookAccountRepository, Mockito.times(1)).save(this.newCashbookAccount);
  }

  @Test
  public void testUpdate() {
    this.cashbookAccountService.update(this.cashbookAccountDTO);

    Mockito.verify(this.cashbookAccount, Mockito.times(1)).setName(this.cashbookAccountDTO.getName());
    Mockito.verify(this.cashbookAccount, Mockito.times(1)).setBankImporterType(this.cashbookAccountDTO.getBankImporterType());
    Mockito.verify(this.cashbookAccount, Mockito.times(1)).setReceiptCode(this.cashbookAccountDTO.getReceiptCode());
    Mockito.verify(this.cashbookAccount, Mockito.times(1)).setBankAccount(this.cashbookAccountDTO.isBankAccount());
    Mockito.verify(this.cashbookAccountRepository, Mockito.times(1)).save(this.cashbookAccount);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveNewReceiptCodeAlreadyInUse() {
    Mockito.when(this.cashbookAccountRepository.findOneByReceiptCode(DEFAULT_RECEIPT_CODE)).thenReturn(Optional.ofNullable(
      Mockito.mock(CashbookAccount.class)
    ));

    Mockito.when(this.newCashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);

    this.cashbookAccountService.save(this.newCashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveExistingReceiptCodeUpdateNotAllowed() {
    Mockito.when(this.cashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE2);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveNewReceiptCodeOnlyLetters() {
    Mockito.when(this.newCashbookAccount.getReceiptCode()).thenReturn("A1");

    this.cashbookAccountService.save(this.newCashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveInvalidBankAccount() {
    Mockito.when(this.newCashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);
    Mockito.when(this.newCashbookAccount.isBankAccount()).thenReturn(true);
    Mockito.when(this.newCashbookAccount.getBankImporterType()).thenReturn(null);

    this.cashbookAccountService.save(this.newCashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveInvalidBankAccount2() {
    Mockito.when(this.newCashbookAccount.getReceiptCode()).thenReturn(DEFAULT_RECEIPT_CODE);
    Mockito.when(this.newCashbookAccount.isBankAccount()).thenReturn(false);
    Mockito.when(this.newCashbookAccount.getBankImporterType()).thenReturn(DEFAULT_BANK_IMPORTER_TYPE);

    this.cashbookAccountService.save(this.newCashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveExistingInvalidBankAccount() {
    Mockito.when(this.cashbookAccount.isBankAccount()).thenReturn(true);
    Mockito.when(this.cashbookAccount.getBankImporterType()).thenReturn(null);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveExistingInvalidBankAccount2() {
    Mockito.when(this.cashbookAccount.isBankAccount()).thenReturn(false);
    Mockito.when(this.cashbookAccount.getBankImporterType()).thenReturn(DEFAULT_BANK_IMPORTER_TYPE);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveWithSameTextForAutoTransfer() {
    Mockito.when(this.cashbookAccountDTO.getTextForAutoTransferTo()).thenReturn(DEFAULT_TEXT_TO);
    Mockito.when(this.cashbookAccountDTO.getTextForAutoTransferFrom()).thenReturn(DEFAULT_TEXT_TO);
    Mockito.when(this.cashbookAccount.getTextForAutoTransferTo()).thenReturn(DEFAULT_TEXT_TO);
    Mockito.when(this.cashbookAccount.getTextForAutoTransferFrom()).thenReturn(DEFAULT_TEXT_TO);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveWithAutoTransferToNotUnique() {
    Mockito.when(this.cashbookAccountDTO.getTextForAutoTransferTo()).thenReturn(DEFAULT_TEXT_TO + "1");
    Mockito.when(this.cashbookAccount.getTextForAutoTransferTo()).thenReturn(DEFAULT_TEXT_TO);
    Mockito.when(this.cashbookAccountRepository.existsByTextForAutoTransferTo(DEFAULT_TEXT_TO + "1")).thenReturn(true);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveWithAutoTransferFromNotUnique() {
    Mockito.when(this.cashbookAccountDTO.getTextForAutoTransferFrom()).thenReturn(DEFAULT_TEXT_FROM + "1");
    Mockito.when(this.cashbookAccount.getTextForAutoTransferFrom()).thenReturn(DEFAULT_TEXT_FROM);
    Mockito.when(this.cashbookAccountRepository.existsByTextForAutoTransferTo(DEFAULT_TEXT_FROM + "1")).thenReturn(true);

    this.cashbookAccountService.update(this.cashbookAccountDTO);
  }
}
