package at.simianarmy.service;

import static org.junit.Assert.*;

import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import java.util.Set;
import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Letter;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.service.dto.LetterDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class LetterServiceTest {

	
	@Inject
	private LetterService letterService;
	
	@Inject
	private LetterRepository letterRepository;
	
	private static final String title1 = "Title1";
	private static final String title2 = "Title2";
	private static final String text1 = "Text1";
	private static final String text2 = "Text2";
	
	private LetterDTO letter1;
	
	
	@Before
	public void setUp() throws Exception {
		Letter letterSave;
		
		letter1 = new LetterDTO();
		
		letter1.setTitle(title1);
		letter1.setText(text1);
		letter1.setLetterType(LetterType.SERIAL_LETTER);
		
		
		letterSave = new Letter();
		letterSave.setTitle(letter1.getTitle());
		letterSave.setLetterType(LetterType.SERIAL_LETTER);
		letterSave.setText(letter1.getText());
		
		letterRepository.saveAndFlush(letterSave);
		
	}

	@After
	public void tearDown() throws Exception {
		letterRepository.deleteAll();
	
	}

	@Test
	public void testWithExistingTitle_ShouldOverwrite() {
		LetterDTO letter = new LetterDTO();
		
		letter.setTitle(title1);
		letter.setLetterType(LetterType.SERIAL_LETTER);
		letter.setText(text2);
		
		//Letter1 should be overwritten
		letterService.save(letter);
		
		List<Letter> result = letterRepository.findAll();
		
		assertThat(result, Matchers.hasSize(1));
		
		for (Letter l : result) {
			if (l.getTitle() == title1) {
				assertThat(l.getText(), Matchers.equalTo(text2));
			}			
		}
		
	}
	
	@Test
	public void testWithNonExistingTitle_ShouldBeSaved() {
		LetterDTO letter = new LetterDTO();
		
		letter.setTitle(title2);
		letter.setLetterType(LetterType.SERIAL_LETTER);
		letter.setText(text2);
		
//		Letter should be saved as new 
		letterService.save(letter);
		
		List<Letter> result = letterRepository.findAll();
		
		assertThat(result, Matchers.hasSize(2));

		for (Letter l : result) {
			if (l.getTitle() == title2) {
				assertThat(l.getText(), Matchers.equalTo(text2));
			}
					
					
		}
		
		
	}

	@Test(expected = ServiceValidationException.class)
  public void testGenerateWithPersonGroupsAndPersonsSet() {
	  LetterDTO letter = new LetterDTO();
	  Set<PersonGroupDTO> personGroups = new HashSet<>();
	  personGroups.add(new PersonGroupDTO());
	  List<PersonDTO> persons = new ArrayList<>(Arrays.asList(new PersonDTO()));

    letter.setTitle(title2);
    letter.setLetterType(LetterType.SERIAL_LETTER);
    letter.setText(text2);
    letter.setPersonGroups(personGroups);
    letter.setPersons(persons);

    this.letterService.generate(letter);
  }

}
