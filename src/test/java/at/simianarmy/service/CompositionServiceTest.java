package at.simianarmy.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CompositionMapper;
import at.simianarmy.service.specification.CompositionSpecification;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CompositionServiceTest {

  private static final Long DEFAULT_ID = new Long(1);
  private static final Integer DEFAULT_NUMBER = 1234;
  private static final String DEFAULT_STRING = "THIS_IS_A_STRING";
  private static final BigDecimal AMOUNT_DEFAULT = new BigDecimal(5);
  private static final BigDecimal AMOUNT_ZERO = new BigDecimal(0);

  @Mock
  private CompositionRepository compositionRepository;

  @Mock
  private CashbookEntryRepository cashbookEntryRepository;

  @Mock
  private CompositionMapper compositionMapper;

  @Mock
  private CompositionSpecification compositionSpecification;

  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private CompositionService compositionService;

  private CashbookAccount cashbookAccount;

  @Before
  public void init() {
    this.cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.customizationService.getDefaultCashbookAccount()).thenReturn(cashbookAccount);
  }

  @Test
  public void testSaveNewEntryWithCasbookEntry() {

    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Mockito.when(dto.getId()).thenReturn(null);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(result.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(result.getCashbookEntryId()).thenReturn(DEFAULT_ID);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(ArgumentMatchers.any()))
        .thenReturn(cashbookEntry);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.never()).findById(ArgumentMatchers.anyLong());
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(dto, Mockito.times(3)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(ArgumentMatchers.any());
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(cashbookEntry);

    assertNotNull(result.getCashbookEntryId());
    assertEquals(result.getCashbookEntryAmount(), AMOUNT_DEFAULT);
  }

  @Test
  public void testSaveNewEntryWithCasbookEntryZero() {

    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);

    Mockito.when(dto.getId()).thenReturn(null);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(AMOUNT_ZERO);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.never()).findById(ArgumentMatchers.anyLong());
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(dto, Mockito.times(2)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(null);

    assertEquals(result.getCashbookEntryAmount(), null);
  }

  @Test
  public void testSaveNewEntryWithoutCasbookEntry() {

    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Mockito.when(dto.getId()).thenReturn(null);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(null);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(result.getCashbookEntryId()).thenReturn(null);
    Mockito.when(result.getCashbookEntryAmount()).thenReturn(null);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(ArgumentMatchers.any()))
        .thenReturn(cashbookEntry);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.never()).findById(ArgumentMatchers.anyLong());
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(dto, Mockito.times(1)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(null);

    assertNull(result.getCashbookEntryId());
    assertEquals(result.getCashbookEntryAmount(), null);
  }

  @Test
  public void testSaveUpdateEntryWithNewCashbookEntry() {
    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Mockito.when(dto.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(composition.getCashbookEntry()).thenReturn(null);
    Mockito.when(result.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(result.getCashbookEntryId()).thenReturn(DEFAULT_ID);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(ArgumentMatchers.any()))
        .thenReturn(cashbookEntry);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(composition, Mockito.times(1)).getCashbookEntry();
    Mockito.verify(dto, Mockito.times(3)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(ArgumentMatchers.any());
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(cashbookEntry);

    assertNotNull(result.getCashbookEntryId());
    assertEquals(result.getCashbookEntryAmount(), AMOUNT_DEFAULT);
  }

  @Test
  public void testSaveUpdateEntryWithNewCashbookEntryZero() {
    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Mockito.when(dto.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(AMOUNT_ZERO);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(AMOUNT_ZERO);
    Mockito.when(composition.getCashbookEntry()).thenReturn(null);
    Mockito.when(result.getCashbookEntryId()).thenReturn(null);
    Mockito.when(result.getCashbookEntryAmount()).thenReturn(null);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(ArgumentMatchers.any()))
        .thenReturn(cashbookEntry);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(composition, Mockito.times(1)).getCashbookEntry();
    Mockito.verify(dto, Mockito.times(2)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(null);

    assertNull(result.getCashbookEntryId());
    assertEquals(result.getCashbookEntryAmount(), null);
  }

  @Test
  public void testSaveUpdateEntryWithExistingCashbookEntry() {
    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Mockito.when(dto.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(composition.getCashbookEntry()).thenReturn(cashbookEntry);
    Mockito.when(result.getCashbookEntryAmount()).thenReturn(AMOUNT_DEFAULT);
    Mockito.when(result.getCashbookEntryId()).thenReturn(DEFAULT_ID);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(cashbookEntry))
        .thenReturn(cashbookEntry);

    assertEquals(result, this.compositionService.save(dto));

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(this.compositionMapper, Mockito.times(1)).compositionDTOToComposition(dto);
    Mockito.verify(this.compositionRepository, Mockito.times(1)).save(composition);
    Mockito.verify(this.compositionMapper, Mockito.times(1))
        .compositionToCompositionDTO(composition);
    Mockito.verify(composition, Mockito.times(1)).getCashbookEntry();
    Mockito.verify(dto, Mockito.times(2)).getCashbookEntryAmount();
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(cashbookEntry);
    Mockito.verify(composition, Mockito.times(1)).setCashbookEntry(cashbookEntry);

    assertNotNull(result.getCashbookEntryId());
    assertEquals(result.getCashbookEntryAmount(), AMOUNT_DEFAULT);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveUpdateEntryWithNullCashbookEntryShouldThrowException() {
    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);
    BigDecimal amount = null;

    Mockito.when(dto.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(dto.getCashbookEntryAmount()).thenReturn(amount);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(cashbookEntry.getAmount()).thenReturn(amount);
    Mockito.when(composition.getCashbookEntry()).thenReturn(cashbookEntry);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);
    Mockito.when(this.cashbookEntryRepository.saveAndFlush(cashbookEntry))
        .thenReturn(cashbookEntry);

    this.compositionService.save(dto);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveUpdateEntryShouldThrowException() {
    CompositionDTO dto = Mockito.mock(CompositionDTO.class);
    CompositionDTO result = Mockito.mock(CompositionDTO.class);
    Composition composition = Mockito.mock(Composition.class);
    Mockito.when(dto.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(dto.getNr()).thenReturn(DEFAULT_NUMBER + 1);
    Mockito.when(composition.getNr()).thenReturn(DEFAULT_NUMBER);
    Mockito.when(this.compositionMapper.compositionDTOToComposition(dto)).thenReturn(composition);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));
    Mockito.when(this.compositionRepository.save(composition)).thenReturn(composition);
    Mockito.when(this.compositionMapper.compositionToCompositionDTO(composition))
        .thenReturn(result);

    this.compositionService.save(dto);
  }

  @Test
  public void testDelete() {
    Composition composition = Mockito.mock(Composition.class);

    Composer composer1 = Mockito.mock(Composer.class);
    Composer composer2 = Mockito.mock(Composer.class);
    Composer composer3 = Mockito.mock(Composer.class);

    Arranger arranger1 = Mockito.mock(Arranger.class);
    Arranger arranger2 = Mockito.mock(Arranger.class);

    CashbookEntry cashbookEntry = Mockito.mock(CashbookEntry.class);

    Set<Composer> composers = new HashSet<>();
    composers.add(composer1);
    composers.add(composer2);
    composers.add(composer3);

    Set<Arranger> arrangers = new HashSet<>();
    arrangers.add(arranger1);
    arrangers.add(arranger2);

    Mockito.when(composition.getComposers()).thenReturn(composers);
    Mockito.when(composition.getArrangers()).thenReturn(arrangers);
    Mockito.when(composition.getCashbookEntry()).thenReturn(cashbookEntry);
    Mockito.when(cashbookEntry.getId()).thenReturn(DEFAULT_ID);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));

    this.compositionService.delete(DEFAULT_ID);

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(composition, Mockito.times(1)).getCashbookEntry();
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).deleteById(DEFAULT_ID);

    for (Composer composer : composers) {
      Mockito.verify(composer, Mockito.times(1)).removeCompositions(composition);
    }

    for (Arranger arranger : arrangers) {
      Mockito.verify(arranger, Mockito.times(1)).removeCompositions(composition);
    }

    Mockito.verify(this.compositionRepository, Mockito.times(1)).deleteById(DEFAULT_ID);

  }

  @Test
  public void testDeleteWithoutCashbookEntry() {
    Composition composition = Mockito.mock(Composition.class);

    Composer composer1 = Mockito.mock(Composer.class);
    Composer composer2 = Mockito.mock(Composer.class);
    Composer composer3 = Mockito.mock(Composer.class);

    Arranger arranger1 = Mockito.mock(Arranger.class);
    Arranger arranger2 = Mockito.mock(Arranger.class);

    CashbookEntry cashbookEntry = null;

    Set<Composer> composers = new HashSet<>();
    composers.add(composer1);
    composers.add(composer2);
    composers.add(composer3);

    Set<Arranger> arrangers = new HashSet<>();
    arrangers.add(arranger1);
    arrangers.add(arranger2);

    Mockito.when(composition.getComposers()).thenReturn(composers);
    Mockito.when(composition.getArrangers()).thenReturn(arrangers);
    Mockito.when(composition.getCashbookEntry()).thenReturn(cashbookEntry);
    Mockito.when(this.compositionRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(composition));

    this.compositionService.delete(DEFAULT_ID);

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findById(DEFAULT_ID);
    Mockito.verify(composition, Mockito.times(1)).getCashbookEntry();
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(0)).deleteById(DEFAULT_ID);

    for (Composer composer : composers) {
      Mockito.verify(composer, Mockito.times(1)).removeCompositions(composition);
    }

    for (Arranger arranger : arrangers) {
      Mockito.verify(arranger, Mockito.times(1)).removeCompositions(composition);
    }

    Mockito.verify(this.compositionRepository, Mockito.times(1)).deleteById(DEFAULT_ID);

  }

  @Test
  @SuppressWarnings("unchecked")
  public void testFindAll() {
    
	Page<Composition> page =  Mockito.mock(Page.class);
    Pageable pageable = Mockito.mock(Pageable.class);
    Specification<Composition> specifications = Mockito.mock(Specification.class);

    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification).init();
    Mockito.doReturn(specifications).when(this.compositionSpecification).build();
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasArranger(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasComposer(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasTitle(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasGenre(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasPublisher(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasOrderingCompany(DEFAULT_STRING);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasNr(DEFAULT_NUMBER);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasMusicBook(DEFAULT_ID);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
        .hasColorCode(DEFAULT_ID);
    Mockito.doReturn(this.compositionSpecification).when(this.compositionSpecification)
    .isActive(true);

    Mockito.when(this.compositionRepository.findAll(specifications, pageable)).thenReturn(page);

    this.compositionService.findAll(DEFAULT_NUMBER, DEFAULT_ID, DEFAULT_STRING, DEFAULT_STRING,
        DEFAULT_STRING, DEFAULT_STRING, DEFAULT_STRING, DEFAULT_STRING, DEFAULT_ID, true, pageable);

    Mockito.verify(this.compositionSpecification, Mockito.times(1)).init();
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasNr(DEFAULT_NUMBER);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasTitle(DEFAULT_STRING);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasColorCode(DEFAULT_ID);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasMusicBook(DEFAULT_ID);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasPublisher(DEFAULT_STRING);
    Mockito.verify(this.compositionSpecification, Mockito.times(1))
        .hasOrderingCompany(DEFAULT_STRING);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasComposer(DEFAULT_STRING);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).hasArranger(DEFAULT_STRING);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).isActive(true);
    Mockito.verify(this.compositionSpecification, Mockito.times(1)).build();

    Mockito.verify(this.compositionRepository, Mockito.times(1)).findAll(specifications, pageable);
    Mockito.verify(page, Mockito.times(1)).map(ArgumentMatchers.any());
  }
}
