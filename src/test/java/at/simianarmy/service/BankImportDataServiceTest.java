package at.simianarmy.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Receipt;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterFactory;
import at.simianarmy.imports.accounting.api.BankImporterGeorgeAPI;
import at.simianarmy.imports.accounting.csv.BankImporterGeorgeCSV;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.service.dto.BankImportDataDTO;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.dto.ReceiptDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.BankImportDataMapper;
import at.simianarmy.service.mapper.CashbookEntryMapper;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.Locale;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringRunner.class)
public class BankImportDataServiceTest {

  private final DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

  private static final String FILENAME = "test.csv";
  private static final String USERNAME = "USER";
  private static final String PASSWORD = "PASSWORD";
  private static final Long CATEGORY_ID = new Long(1);
  private static final String CATEGORY_NAME = "CAT";
  private static final Long ACCOUNT_ID = new Long(1);
  private static final String AUTO_GEN_STRING = "AUTO_GENERATED";

  private BankImportDataDTO dto;
  private BankImportData data;
  private CashbookEntryDTO entryDTO;
  private CashbookEntry entry;
  private CashbookCategory cashbookCategory;
  private List<BankImportDataDTO> imports;

  @Mock
  private BankImportDataRepository bankImportDataRepository;

  @Mock
  private BankImportDataMapper bankImportDataMapper;

  @Mock
  private CashbookEntryRepository cashbookEntryRepository;

  @Mock
  private CashbookEntryMapper cashbookEntryMapper;

  @Mock
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Mock
  private MembershipFeeForPeriodRepository memberShipFeeForPeriodRepository;

  @Mock
  private MembershipFeeAmountRepository memberShipFeeAmountRepository;

  @Mock
  private BankImporterFactory bankImporterFactory;

  @Mock
  private CashbookAccountRepository cashbookAccountRepository;

  @Mock
  private ReceiptService receiptService;

  @Mock
  private MessageSource messageSource;

  @InjectMocks
  private BankImportDataService sut;

  private MultipartFile multipartFile;

  private BankImporterGeorgeCSV georgeCSVImporter;

  private BankImporterGeorgeAPI georgeAPIImporter;

  private CashbookAccount cashbookAccount;

  @Before
  public void init() throws IOException {
    this.multipartFile = Mockito.mock(MultipartFile.class);
    this.georgeCSVImporter = Mockito.mock(BankImporterGeorgeCSV.class);
    this.georgeAPIImporter = Mockito.mock(BankImporterGeorgeAPI.class);
    this.cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccount.getBankImporterType()).thenReturn(BankImporterType.GEORGE_IMPORTER);
    Mockito.when(this.cashbookAccountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.ofNullable(this.cashbookAccount));

    this.cashbookCategory = Mockito.mock(CashbookCategory.class);
    Mockito.when(this.cashbookCategory.getId()).thenReturn(CATEGORY_ID);
    Mockito.when(this.cashbookCategory.getName()).thenReturn(CATEGORY_NAME);
    Mockito.when(this.cashbookCategoryRepository.findById(CATEGORY_ID)).thenReturn(
      Optional.ofNullable(this.cashbookCategory));

    Mockito.when(this.multipartFile.getOriginalFilename()).thenReturn(FILENAME);
    Mockito.when(this.multipartFile.getInputStream()).thenReturn(Mockito.mock(InputStream.class));
    Mockito.when(this.multipartFile.getContentType())
      .thenReturn(BankImportDataService.CONTENT_TYPE_CSV);
    Mockito
      .when(this.bankImporterFactory
        .createCSVImporter(ArgumentMatchers.eq(this.cashbookAccount)))
      .thenReturn(this.georgeCSVImporter);
    Mockito
      .when(this.bankImporterFactory
        .createAPIImporter(ArgumentMatchers.eq(this.cashbookAccount)))
      .thenReturn(this.georgeAPIImporter);

    Mockito.when(this.memberShipFeeForPeriodRepository.findByReferenceCode(ArgumentMatchers.anyString()))
      .thenReturn(null);
    Mockito.when(this.memberShipFeeAmountRepository.findByMembershipFeeAndYear(ArgumentMatchers.anyLong(),
      ArgumentMatchers.anyInt())).thenReturn(null);

    Mockito.when(this.messageSource.getMessage("bankImport.receiptAutoGenerated", null,
      Locale.getDefault())).thenReturn(AUTO_GEN_STRING);
  }

  @Test
  public void testFindAll() {
    this.sut.findAll();
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1)).findAllNotImported();
  }

  @Test
  public void testImportBankImportDataIncome() {

    this.generateEntitiesForImport(new BigDecimal("2.4"), "IMPORT", CashbookEntryType.INCOME, false, null, null, null);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());
  }

  @Test
  public void testImportBankImportDataOneToDelete() {
    this.generateEntitiesForImport(new BigDecimal("2.4"), "IMPORT", CashbookEntryType.INCOME, false, null, null, null);

    BankImportData imp1 = new BankImportData();
    imp1.setCashbookAccount(this.cashbookAccount);

    List<BankImportDataDTO> imports = new ArrayList<BankImportDataDTO>(Arrays.asList(dto));

    Mockito.when(this.bankImportDataMapper.bankImportDataDTOsToBankImportData(imports))
      .thenReturn(new ArrayList<BankImportData>(Arrays.asList(data)));
    Mockito
      .when(this.cashbookEntryMapper
        .cashbookEntriesToCashbookEntryDTOs(new ArrayList<CashbookEntry>(Arrays.asList(entry))))
      .thenReturn(new ArrayList<CashbookEntryDTO>(Arrays.asList(entryDTO)));
    Mockito.when(this.bankImportDataRepository.findAllNotImported())
      .thenReturn(new ArrayList<BankImportData>(Arrays.asList(imp1)));

    this.sut.importBankImportData(imports, false);

    Mockito.verify(this.bankImportDataRepository, Mockito.times(1)).delete(imp1);
  }

  @Test
  public void testImportBankImportDataSpending() {
    this.generateEntitiesForImport(new BigDecimal("-2.4"), "IMPORT", CashbookEntryType.SPENDING, false, null, null, null);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());
  }

  @Test
  public void testImportBankImportDataAutoTransfer() {

    CashbookAccount accountFrom = Mockito.mock(CashbookAccount.class);
    CashbookAccount accountTo = this.cashbookAccount;
    CashbookCategory categoryForTransers = Mockito.mock(CashbookCategory.class);

    this.generateEntitiesForImport(new BigDecimal("2.4"), "Einzahlung", CashbookEntryType.INCOME, true, accountFrom, accountTo, categoryForTransers);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(2)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    List<CashbookEntry> capturedArguments = argument.getAllValues();

    CashbookEntry entry1 = capturedArguments.get(0);
    CashbookEntry entry2 = capturedArguments.get(1);

    assertEquals(entry1.getDate(), entry.getDate());
    assertEquals(entry1.getAmount(), entry.getAmount());
    assertEquals(entry1.getTitle(), entry.getTitle());
    assertEquals(entry1.getType(), CashbookEntryType.INCOME);
    assertEquals(entry1.getCashbookCategory(), categoryForTransers);
    assertEquals(entry1.getCashbookAccount(), accountTo);

    assertEquals(entry2.getDate(), entry.getDate());
    assertEquals(entry2.getAmount(), entry.getAmount());
    assertEquals(entry2.getTitle(), entry.getTitle());
    assertEquals(entry2.getType(), CashbookEntryType.SPENDING);
    assertEquals(entry2.getCashbookCategory(), categoryForTransers);
    assertEquals(entry2.getCashbookAccount(), accountFrom);
  }

  @Test
  public void testImportBankImportDataWithMembershipFee() {
    this.generateEntitiesForImport(new BigDecimal("99.99"), "00001012017 Mitgliedsbeitragszahlung",
      CashbookEntryType.INCOME, false, null, null, null);

    MembershipFeeForPeriod feeForPeriod = Mockito.mock(MembershipFeeForPeriod.class);
    Membership membership = Mockito.mock(Membership.class);
    MembershipFeeAmount amount = Mockito.mock(MembershipFeeAmount.class);
    MembershipFee fee = Mockito.mock(MembershipFee.class);
    long membershipFeeId = 1;
    long feeForPeriodId = 1;
    Mockito.when(feeForPeriod.getMembership()).thenReturn(membership);
    Mockito.when(feeForPeriod.getYear()).thenReturn(2017);
    Mockito.when(feeForPeriod.getId()).thenReturn(feeForPeriodId);
    Mockito.when(membership.getMembershipFee()).thenReturn(fee);
    Mockito.when(fee.getId()).thenReturn(membershipFeeId);
    Mockito.when(amount.getAmount()).thenReturn(new BigDecimal("99.99"));
    Mockito.when(this.memberShipFeeForPeriodRepository.findByReferenceCode("00001012017"))
      .thenReturn(feeForPeriod);
    Mockito
      .when(this.memberShipFeeAmountRepository.findByMembershipFeeAndYear(membershipFeeId, 2017))
      .thenReturn(amount);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());

    Mockito.verify(feeForPeriod, Mockito.times(1)).setCashbookEntry(argument.capture());
    ArgumentCaptor<MembershipFeeForPeriod> argumentFee = ArgumentCaptor
      .forClass(MembershipFeeForPeriod.class);
    Mockito.verify(this.memberShipFeeForPeriodRepository, Mockito.times(1))
      .saveAndFlush(argumentFee.capture());

    assertEquals(argumentFee.getValue().getMembership(), feeForPeriod.getMembership());
    assertEquals(argumentFee.getValue().getYear(), feeForPeriod.getYear());
    assertEquals(argumentFee.getValue().getId(), feeForPeriod.getId());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());

    Mockito.verify(feeForPeriod, Mockito.times(1)).setPaid(true);
  }

  @Test
  public void testImportBankImportDataWithMembershipFeePaidMore() {
    this.generateEntitiesForImport(new BigDecimal("100.99"), "00001012017",
      CashbookEntryType.INCOME, false, null, null, null);

    MembershipFeeForPeriod feeForPeriod = Mockito.mock(MembershipFeeForPeriod.class);
    Membership membership = Mockito.mock(Membership.class);
    MembershipFeeAmount amount = Mockito.mock(MembershipFeeAmount.class);
    MembershipFee fee = Mockito.mock(MembershipFee.class);
    long membershipFeeId = 1;
    long feeForPeriodId = 1;
    Mockito.when(feeForPeriod.getMembership()).thenReturn(membership);
    Mockito.when(feeForPeriod.getYear()).thenReturn(2017);
    Mockito.when(feeForPeriod.getId()).thenReturn(feeForPeriodId);
    Mockito.when(membership.getMembershipFee()).thenReturn(fee);
    Mockito.when(fee.getId()).thenReturn(membershipFeeId);
    Mockito.when(amount.getAmount()).thenReturn(new BigDecimal("99.99"));
    Mockito.when(this.memberShipFeeForPeriodRepository.findByReferenceCode("00001012017"))
      .thenReturn(feeForPeriod);
    Mockito
      .when(this.memberShipFeeAmountRepository.findByMembershipFeeAndYear(membershipFeeId, 2017))
      .thenReturn(amount);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());

    Mockito.verify(feeForPeriod, Mockito.times(1)).setCashbookEntry(argument.capture());
    ArgumentCaptor<MembershipFeeForPeriod> argumentFee = ArgumentCaptor
      .forClass(MembershipFeeForPeriod.class);
    Mockito.verify(this.memberShipFeeForPeriodRepository, Mockito.times(1))
      .saveAndFlush(argumentFee.capture());

    assertEquals(argumentFee.getValue().getMembership(), feeForPeriod.getMembership());
    assertEquals(argumentFee.getValue().getYear(), feeForPeriod.getYear());
    assertEquals(argumentFee.getValue().getId(), feeForPeriod.getId());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());

    Mockito.verify(feeForPeriod, Mockito.times(1)).setPaid(true);
  }

  @Test
  public void testImportBankImportDataWithMembershipFeePaidLess() {
    this.generateEntitiesForImport(new BigDecimal("0.1"), "00001012017 Zahlung zu wenig",
      CashbookEntryType.INCOME, false, null, null, null);

    MembershipFeeForPeriod feeForPeriod = Mockito.mock(MembershipFeeForPeriod.class);
    Membership membership = Mockito.mock(Membership.class);
    MembershipFeeAmount amount = Mockito.mock(MembershipFeeAmount.class);
    MembershipFee fee = Mockito.mock(MembershipFee.class);
    long membershipFeeId = 1;
    long feeForPeriodId = 1;
    Mockito.when(feeForPeriod.getMembership()).thenReturn(membership);
    Mockito.when(feeForPeriod.getYear()).thenReturn(2017);
    Mockito.when(feeForPeriod.getId()).thenReturn(feeForPeriodId);
    Mockito.when(membership.getMembershipFee()).thenReturn(fee);
    Mockito.when(fee.getId()).thenReturn(membershipFeeId);
    Mockito.when(amount.getAmount()).thenReturn(new BigDecimal("99.99"));
    Mockito.when(this.memberShipFeeForPeriodRepository.findByReferenceCode("00001012017"))
      .thenReturn(feeForPeriod);
    Mockito
      .when(this.memberShipFeeAmountRepository.findByMembershipFeeAndYear(membershipFeeId, 2017))
      .thenReturn(amount);

    this.sut.importBankImportData(imports, false);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());

    Mockito.verify(feeForPeriod, Mockito.times(1)).setCashbookEntry(argument.capture());
    ArgumentCaptor<MembershipFeeForPeriod> argumentFee = ArgumentCaptor
      .forClass(MembershipFeeForPeriod.class);
    Mockito.verify(this.memberShipFeeForPeriodRepository, Mockito.times(1))
      .saveAndFlush(argumentFee.capture());

    assertEquals(argumentFee.getValue().getMembership(), feeForPeriod.getMembership());
    assertEquals(argumentFee.getValue().getYear(), feeForPeriod.getYear());
    assertEquals(argumentFee.getValue().getId(), feeForPeriod.getId());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());

    Mockito.verify(feeForPeriod, Mockito.never()).setPaid(true);
  }

  @Test
  public void testUploadCSVFileShouldBeDeleted() {
    this.sut.uploadCSVFile(this.multipartFile, ACCOUNT_ID, CATEGORY_ID);

    File file = new File("test.csv");

    assertFalse(file.exists());
  }

  @Test(expected = ServiceValidationException.class)
  public void testUploadCSVFileShouldThrowException() throws BankImporterException {

    Mockito.when(this.georgeCSVImporter.readData(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
      .thenThrow(BankImporterException.class);

    this.sut.uploadCSVFile(this.multipartFile, ACCOUNT_ID, CATEGORY_ID);

  }

  @Test(expected = ServiceValidationException.class)
  public void testUploadCSVFileWrongBankImporterType() {
    this.sut.uploadCSVFile(this.multipartFile, new Long(3), CATEGORY_ID);
  }

  //  TODO: check csv file - adjust test
//  @Test(expected = ServiceValidationException.class)
  public void testUploadCSVFileWrongFileType() {
    Mockito.when(this.multipartFile.getContentType()).thenReturn("application/pdf");
    this.sut.uploadCSVFile(multipartFile, ACCOUNT_ID, CATEGORY_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testImportFromAPIWrongBankImporterType() throws ParseException {
    this.sut.importFromAPI(USERNAME, PASSWORD, this.dateFormatter.parse("01.01.2017"),
      this.dateFormatter.parse("01.02.2017"), new Long(3), CATEGORY_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testImportAPIDataShouldThrowException() throws BankImporterException, ParseException {

    Mockito.when(
      this.georgeAPIImporter.readData(ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any(), ArgumentMatchers.any()))
      .thenThrow(BankImporterException.class);

    this.sut.importFromAPI(USERNAME, PASSWORD, this.dateFormatter.parse("01.01.2017"),
      this.dateFormatter.parse("01.02.2017"), ACCOUNT_ID, CATEGORY_ID);

  }

  @Test(expected = ServiceValidationException.class)
  public void testImportAPIFromBeforeTo() throws BankImporterException, ParseException {
    this.sut.importFromAPI(USERNAME, PASSWORD, this.dateFormatter.parse("01.01.2017"),
      this.dateFormatter.parse("01.02.2016"), ACCOUNT_ID, CATEGORY_ID);
  }

  @Test
  public void testImportBankImportDataAndCreateReceipt() {

    this.generateEntitiesForImport(new BigDecimal("2.4"), "IMPORT", CashbookEntryType.INCOME, false, null, null, null);

    this.sut.importBankImportData(imports, true);

    ArgumentCaptor<CashbookEntry> argument = ArgumentCaptor.forClass(CashbookEntry.class);
    ArgumentCaptor<BankImportData> argumentData = ArgumentCaptor.forClass(BankImportData.class);
    ArgumentCaptor<ReceiptDTO> receiptArgument = ArgumentCaptor.forClass(ReceiptDTO.class);
    Mockito.verify(this.cashbookEntryRepository, Mockito.times(1)).saveAndFlush(argument.capture());
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1))
      .saveAndFlush(argumentData.capture());
    Mockito.verify(this.receiptService, Mockito.times(1)).save(receiptArgument.capture());

    assertEquals(argument.getValue().getDate(), entry.getDate());
    assertEquals(argument.getValue().getAmount(), entry.getAmount());
    assertEquals(argument.getValue().getTitle(), entry.getTitle());
    assertEquals(argument.getValue().getType(), entry.getType());
    assertEquals(argument.getValue().getCashbookCategory(), entry.getCashbookCategory());
    assertEquals(argument.getValue().getCashbookAccount(), entry.getCashbookAccount());
    assertEquals(argumentData.getValue().getCashbookEntry(), argument.getValue());

    assertEquals(receiptArgument.getValue().getTitle(), entry.getTitle());
    assertEquals(receiptArgument.getValue().getDate(), entry.getDate());
    assertEquals(receiptArgument.getValue().getNote(), AUTO_GEN_STRING);
    assertFalse(receiptArgument.getValue().isOverwriteIdentifier());
    assertEquals(receiptArgument.getValue().getCashbookEntryId(), entry.getId());
  }

  private void generateEntitiesForImport(BigDecimal value, String entryText,
    CashbookEntryType entryType, Boolean isTransfer, CashbookAccount accountFrom, CashbookAccount accountTo, CashbookCategory categoryForTransfer) {
    this.dto = new BankImportDataDTO();
    dto.setEntryDate(LocalDate.of(2016, 1, 1));
    dto.setEntryValue(value);
    dto.setEntryText(entryText);
    dto.setCashbookCategoryId(CATEGORY_ID);
    dto.setCashbookCategoryName(CATEGORY_NAME);

    this.data = new BankImportData();
    data.setEntryDate(LocalDate.of(2016, 1, 1));
    data.setEntryValue(value);
    data.setEntryText(entryText);
    data.setCashbookCategory(this.cashbookCategory);
    data.setCashbookAccount(this.cashbookAccount);

    if (isTransfer) {
      data.setTransfer(true);
      data.setCashbookAccountFrom(accountFrom);
      data.setCashbookAccountTo(accountTo);
      data.setCashbookCategory(categoryForTransfer);
    }

    this.entryDTO = new CashbookEntryDTO();
    entryDTO.setDate(dto.getEntryDate());
    entryDTO.setAmount(value.abs());
    entryDTO.setTitle(dto.getEntryText());
    entryDTO.setType(entryType);

    this.entry = new CashbookEntry();
    entry.setDate(entryDTO.getDate());
    entry.setAmount(entryDTO.getAmount());
    entry.setTitle(entryDTO.getTitle());
    entry.setType(entryDTO.getType());
    entry.setCashbookCategory(this.cashbookCategory);
    entry.setCashbookAccount(this.cashbookAccount);

    this.imports = new ArrayList<BankImportDataDTO>(Arrays.asList(dto));

    Mockito.when(this.bankImportDataMapper.bankImportDataDTOsToBankImportData(imports))
      .thenReturn(new ArrayList<BankImportData>(Arrays.asList(data)));
    Mockito
      .when(this.cashbookEntryMapper
        .cashbookEntriesToCashbookEntryDTOs(new ArrayList<CashbookEntry>(Arrays.asList(entry))))
      .thenReturn(new ArrayList<CashbookEntryDTO>(Arrays.asList(entryDTO)));
  }

}
