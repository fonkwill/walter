package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentService;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.repository.InstrumentRepository;
import at.simianarmy.repository.InstrumentServiceRepository;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.dto.InstrumentServiceDTO;
import at.simianarmy.service.mapper.InstrumentMapper;
import at.simianarmy.service.mapper.InstrumentServiceMapper;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentServiceServiceTest {

	private static final Long id1 = new Long(1);
	private static final Long id2 = new Long(2);
	private static final Long id3 = new Long(3);
	
	private static final String name1 = "name1";
	private static final InstrumentType it1 = new InstrumentType();
	
	private static final Long instrumentId = new Long(10);
	private static final Long entryId = new Long(11);
	private static final Long typeId = new Long(12);

	private static final LocalDate instrumentDate = LocalDate.now().minusDays(5);
	private static final LocalDate futureDate = LocalDate.now().plus(2, ChronoUnit.DAYS);
	private static final LocalDate beforeInstrumentDate = LocalDate.now().minusDays(10);
	
	private Instrument instrument = new Instrument();
	
	private CashbookEntry entry = new CashbookEntry();
	
	private InstrumentService service1;
	private InstrumentService service2;
	private InstrumentService service3;
	
	@Mock
	private InstrumentServiceRepository InstrumentServiceRepository;
	
	@Mock
  private InstrumentRepository InstrumentRepository;
	
	@Inject
	@Spy
	private InstrumentServiceMapper InstrumentServiceMapper;
	
	@Inject
	@Spy
  private InstrumentMapper InstrumentMapper;

	@Inject
  private CustomizationService customizationService;
	
	@InjectMocks
	private InstrumentServiceService InstrumentServiceService;
	
	@InjectMocks
  private at.simianarmy.service.InstrumentService serviceForInstrument;
	
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		service1 = new InstrumentService();
		service1.setId(id1);
		service2 = new InstrumentService();
		service2.setId(id2);
		service3 = new InstrumentService();
    service3.setId(id3);

    it1.setId(typeId);
    
		instrument.setId(instrumentId);
		instrument.setPurchaseDate(instrumentDate);
		instrument.setName(name1);
		instrument.setType(it1);
		
		InstrumentDTO instrumentdto = InstrumentMapper.instrumentToInstrumentDTO(instrument);
		serviceForInstrument.save(instrumentdto);
	
	// service1 must not be saved
		service2.setInstrument(instrument);
		service3.setInstrument(instrument);

		
//		service2.setDate(futureDate); //must not be saved
//		service3.setDate(beforeInstrumentDate); //must not be saved
		entry.setId(entryId);
		BigDecimal amount = BigDecimal.TEN;
    entry.setAmount(amount);
		service1.setCashbookEntry(entry); //must not be deleted
		
		when(InstrumentServiceRepository.findById(id1)).thenReturn(Optional.ofNullable(service1));
		when(InstrumentServiceRepository.findById(id2)).thenReturn(Optional.ofNullable(service2));
		when(InstrumentServiceRepository.findById(id3)).thenReturn(Optional.ofNullable(service3));
    when(InstrumentRepository.findById(instrumentId)).thenReturn(Optional.ofNullable(instrument));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testSaveWithoutInstrument() {
	  InstrumentServiceDTO service1dto = InstrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(service1);
    InstrumentServiceService.save(service1dto);
	}
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveWithFutureDate() {
    service2.setDate(futureDate);
    InstrumentServiceDTO service2dto = InstrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(service2);
    InstrumentServiceService.save(service2dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveWithBeforeInstrumentDate() {
    service3.setDate(beforeInstrumentDate);
    System.out.println(service3);
    System.out.println(service3.getInstrument());
    InstrumentServiceDTO service3dto = InstrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(service3);
    InstrumentServiceService.save(service3dto);
  }
}
