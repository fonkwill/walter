package at.simianarmy.service;

import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ArrangerServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private ArrangerRepository arrangerRepository;

  @InjectMocks
  private ArrangerService arrangerService;

  private Arranger arranger;

  @Before
  public void setUp() {
    this.arranger = Mockito.mock(Arranger.class);
    Mockito.when(this.arrangerRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.arranger));
  }

  @Test
  public void testDelete() {
    this.arrangerService.delete(DEFAULT_ID);
    Mockito.verify(this.arrangerRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShoulThrowException() {
    Mockito.when(this.arranger.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.arrangerService.delete(DEFAULT_ID);
  }

}