package at.simianarmy.service;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipFeeRepository;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;
import at.simianarmy.service.dto.MembershipFeeDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.MembershipFeeAmountMapper;
import at.simianarmy.service.mapper.MembershipFeeMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeAmountServiceTest {

	private static final Long id1 = new Long(1);
	private static final Long id2 = new Long(2);
	private static final Long id3 = new Long(3);
	
	private static final BigDecimal amount = new BigDecimal(10);

	private static final LocalDate beginDate = LocalDate.now().minusDays(5);
	private static final LocalDate enddateRight = LocalDate.now().plus(2, ChronoUnit.DAYS);
	private static final LocalDate enddateBad = LocalDate.now().minusDays(10);
	
	private MembershipFeeAmount amount1;
	private MembershipFeeAmount amount2;
	private MembershipFeeAmount amount3;
	
	private MembershipFee fee = new MembershipFee();;
	private static final Long feeId = new Long(10);
	private static final String description = "description";
	
	
	@Inject
	@Spy
	private MembershipFeeAmountMapper MembershipFeeAmountMapper;
	
	@Inject
	@Spy
  private MembershipFeeMapper MembershipFeeMapper;
	
	@Mock
	private MembershipFeeAmountRepository MembershipFeeAmountRepository;
	
	@Mock
  private MembershipFeeRepository MembershipFeeRepository;
	
	@InjectMocks
	private MembershipFeeAmountService MembershipFeeAmountService;
	
	@InjectMocks
  private MembershipFeeService MembershipFeeService;
	

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		fee.setId(feeId);
		fee.setDescription(description);
		MembershipFeeDTO feeDTO = MembershipFeeMapper.membershipFeeToMembershipFeeDTO(fee);
		MembershipFeeService.save(feeDTO);
		
		amount1 = new MembershipFeeAmount();
		amount1.setId(id1);
		amount1.setAmount(amount);
		amount1.setMembershipFee(fee);
		amount2 = new MembershipFeeAmount();
		amount2.setId(id2);
		amount2.setAmount(amount);
		amount2.setMembershipFee(fee);
		
    amount1.setBeginDate(beginDate);
    amount1.setEndDate(enddateRight); //must be saved
    amount2.setBeginDate(beginDate);
    amount2.setEndDate(enddateBad); //must not be saved
		
		when(MembershipFeeAmountRepository.findById(id1)).thenReturn(Optional.ofNullable(amount1));
		when(MembershipFeeAmountRepository.findById(id2)).thenReturn(Optional.ofNullable(amount2));
		when(MembershipFeeAmountRepository.findById(id3)).thenReturn(Optional.ofNullable(amount3));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testSaveWithRightDates() {
	  MembershipFeeAmountDTO amount1dto = MembershipFeeAmountMapper.membershipFeeAmountToMembershipFeeAmountDTO(amount1);
    MembershipFeeAmountService.save(amount1dto);
    
    List<MembershipFeeAmount> result = MembershipFeeAmountRepository.findAll();
    
    for (MembershipFeeAmount a : result) {
      assertTrue(a.getAmount().equals(amount));
      assertTrue(a.getBeginDate().equals(beginDate));
      assertTrue(a.getEndDate().equals(enddateRight));
    }
	}
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveWithEndDateBeforeBeginDate() {
    MembershipFeeAmountDTO amount2dto = MembershipFeeAmountMapper.membershipFeeAmountToMembershipFeeAmountDTO(amount2);
    MembershipFeeAmountService.save(amount2dto);
  }
}
