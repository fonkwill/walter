package at.simianarmy.service;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.dto.CashbookAccountTransferDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CashbookAccountTransferServiceTest {

  private static final Long ACCOUNT_TO_ID = new Long(1);
  private static final String ACCOUNT_TO_NAME = "Kassabuch";
  private static final Long ACCOUNT_FROM_ID = new Long(2);
  private static final String ACCOUNT_FROM_NAME = "Bank Austria";
  private static final BigDecimal AMOUNT = BigDecimal.valueOf(3.33);


  @Mock
  private CashbookAccountRepository cashbookAccountRepository;

  @Mock
  private CashbookEntryRepository cashbookEntryRepository;

  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private CashbookAccountTransferService sut;

  private CashbookAccount accountTo;

  private CashbookAccount accountFrom;

  @Before
  public void init() {
    this.accountTo = newCashbookAccountMock(ACCOUNT_TO_ID, ACCOUNT_TO_NAME);
    this.accountFrom = newCashbookAccountMock(ACCOUNT_FROM_ID, ACCOUNT_FROM_NAME);
  }

  @Test(expected = ServiceValidationException.class)
  public void testAccountFromNotFound() {
    CashbookAccountTransferDTO dto = this.newCashbookAccountTransferDTOMock(new Long(4), ACCOUNT_TO_ID, AMOUNT);
    this.sut.save(dto);
  }

  @Test(expected = ServiceValidationException.class)
  public void testAccountToNotFound() {
    CashbookAccountTransferDTO dto = this.newCashbookAccountTransferDTOMock(ACCOUNT_FROM_ID, new Long(4), AMOUNT);
    this.sut.save(dto);
  }

  @Test(expected = ServiceValidationException.class)
  public void testAccountFromAndToEqual() {
    CashbookAccountTransferDTO dto = this.newCashbookAccountTransferDTOMock(ACCOUNT_FROM_ID, ACCOUNT_FROM_ID, AMOUNT);
    this.sut.save(dto);
  }

  @Test
  public void testTransferCreated() {
    CashbookAccountTransferDTO dto = this.newCashbookAccountTransferDTOMock(ACCOUNT_FROM_ID, ACCOUNT_TO_ID, AMOUNT);

    ArgumentCaptor<CashbookEntry> argumentEntry = ArgumentCaptor.forClass(CashbookEntry.class);

    this.sut.save(dto);

    Mockito.verify(this.cashbookEntryRepository, Mockito.times(2)).save(argumentEntry.capture());

    List<CashbookEntry> capturedEntries = argumentEntry.getAllValues();

    CashbookEntry cashbookEntryFrom = capturedEntries.get(0);
    CashbookEntry cashbookEntryTo = capturedEntries.get(1);

    assertEquals(cashbookEntryFrom.getCashbookAccount(), this.accountFrom);
    assertEquals(cashbookEntryFrom.getType(), CashbookEntryType.SPENDING);
    assertEquals(cashbookEntryFrom.getAmount(), AMOUNT);
    assertEquals(cashbookEntryFrom.getTitle(), "Kontenübertrag nach " + ACCOUNT_TO_NAME);

    assertEquals(cashbookEntryTo.getCashbookAccount(), this.accountTo);
    assertEquals(cashbookEntryTo.getType(), CashbookEntryType.INCOME);
    assertEquals(cashbookEntryTo.getAmount(), AMOUNT);
    assertEquals(cashbookEntryTo.getTitle(), "Kontenübertrag von " + ACCOUNT_FROM_NAME);
  }

  @Test
  public void testTransferCreatedWithDefaultCategory() {
    CashbookCategory cashbookCategory = Mockito.mock(CashbookCategory.class);
    Mockito.when(this.customizationService.getDefaultCashbookCategoryForTransfers()).thenReturn(cashbookCategory);
    CashbookAccountTransferDTO dto = this.newCashbookAccountTransferDTOMock(ACCOUNT_FROM_ID, ACCOUNT_TO_ID, AMOUNT);

    ArgumentCaptor<CashbookEntry> argumentEntry = ArgumentCaptor.forClass(CashbookEntry.class);

    this.sut.save(dto);

    Mockito.verify(this.cashbookEntryRepository, Mockito.times(2)).save(argumentEntry.capture());

    List<CashbookEntry> capturedEntries = argumentEntry.getAllValues();

    CashbookEntry cashbookEntryFrom = capturedEntries.get(0);
    CashbookEntry cashbookEntryTo = capturedEntries.get(1);

    assertEquals(cashbookEntryFrom.getCashbookAccount(), this.accountFrom);
    assertEquals(cashbookEntryFrom.getType(), CashbookEntryType.SPENDING);
    assertEquals(cashbookEntryFrom.getAmount(), AMOUNT);
    assertEquals(cashbookEntryFrom.getTitle(), "Kontenübertrag nach " + ACCOUNT_TO_NAME);
    assertEquals(cashbookEntryFrom.getCashbookCategory(), cashbookCategory);

    assertEquals(cashbookEntryTo.getCashbookAccount(), this.accountTo);
    assertEquals(cashbookEntryTo.getType(), CashbookEntryType.INCOME);
    assertEquals(cashbookEntryTo.getAmount(), AMOUNT);
    assertEquals(cashbookEntryTo.getTitle(), "Kontenübertrag von " + ACCOUNT_FROM_NAME);
    assertEquals(cashbookEntryTo.getCashbookCategory(), cashbookCategory);
  }

  private CashbookAccount newCashbookAccountMock(Long id, String name) {
    CashbookAccount cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(cashbookAccount.getId()).thenReturn(id);
    Mockito.when(cashbookAccount.getName()).thenReturn(name);
    Mockito.when(this.cashbookAccountRepository.findById(id)).thenReturn(Optional.ofNullable(cashbookAccount));
    return cashbookAccount;
  }

  private CashbookAccountTransferDTO newCashbookAccountTransferDTOMock(Long accountFrom, Long accountTo, BigDecimal amount) {
    CashbookAccountTransferDTO dto = Mockito.mock(CashbookAccountTransferDTO.class);
    Mockito.when(dto.getAmount()).thenReturn(amount);
    Mockito.when(dto.getCashbookAccountFromId()).thenReturn(accountFrom);
    Mockito.when(dto.getCashbookAccountToId()).thenReturn(accountTo);
    return dto;
  }

}
