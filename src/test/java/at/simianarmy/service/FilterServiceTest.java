package at.simianarmy.service;

import at.simianarmy.domain.Filter;
import at.simianarmy.repository.FilterRepository;
import at.simianarmy.service.dto.FilterDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.FilterMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilterServiceTest {

  private final String LIST_NAME = "PERSON";
  private final String NAME = "PERSON_FILTER";
  private final String FILTER = "{name='pf'}";

  @Mock
  private FilterRepository filterRepository;

  @Mock
  private FilterMapper filterMapper;

  @InjectMocks
  private FilterService sut;

  @Test(expected = ServiceValidationException.class)
  public void testSaveWithAlreadyExistentName() {
    FilterDTO dtoToSave = Mockito.mock(FilterDTO.class);
    Filter toSave = Mockito.mock(Filter.class);
    Mockito.when(toSave.getListName()).thenReturn(LIST_NAME);
    Mockito.when(toSave.getName()).thenReturn(NAME);
    Mockito.when(toSave.getFilter()).thenReturn(FILTER);

    Mockito.when(this.filterMapper.toEntity(dtoToSave)).thenReturn(toSave);
    Mockito.when(this.filterRepository.existsByListNameAndName(LIST_NAME, NAME)).thenReturn(true);

    this.sut.save(dtoToSave);
  }

  @Test
  public void testSave() {
    FilterDTO dtoToSave = Mockito.mock(FilterDTO.class);
    Filter toSave = Mockito.mock(Filter.class);
    Mockito.when(toSave.getListName()).thenReturn(LIST_NAME);
    Mockito.when(toSave.getName()).thenReturn(NAME);
    Mockito.when(toSave.getFilter()).thenReturn(FILTER);

    Mockito.when(this.filterMapper.toEntity(dtoToSave)).thenReturn(toSave);
    Mockito.when(this.filterRepository.existsByListNameAndName(LIST_NAME, NAME)).thenReturn(false);

    this.sut.save(dtoToSave);

    Mockito.verify(this.filterRepository, Mockito.times(1)).save(toSave);
  }


}
