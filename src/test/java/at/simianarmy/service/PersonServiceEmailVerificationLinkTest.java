package at.simianarmy.service;

import at.simianarmy.domain.Person;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.EmailVerificationLinkDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.mapper.PersonMapper;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PersonServiceEmailVerificationLinkTest {

  private final Long DEFAULT_ID = new Long(1);

  @Mock
  private PersonRepository personRepository;

  @Mock
  private EmailVerificationLinkService emailVerificationLinkService;

  @Mock
  private PersonMapper personMapper;

  @InjectMocks
  private PersonService sut;

  @Test
  public void testCreateNewPersonWithoutEmailVerificationLink() {
    PersonDTO personDTO = this.createPersonDTOMock(null, EmailType.MANUALLY);

    this.sut.save(personDTO);

    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).invalidateEmailVerificationLinkForPerson(Mockito.anyLong());
    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).createEmailVerificationLinkForPerson(Mockito.anyLong());
  }

  @Test
  public void testCreateNewPersonWithEmailVerificationLink() {
    PersonDTO personDTO = this.createPersonDTOMock(null, EmailType.VERIFIED);
    Person person = this.createPersonMock(DEFAULT_ID, EmailType.VERIFIED);

    Mockito.when(this.personMapper.personDTOToPerson(personDTO)).thenReturn(person);
    Mockito.when(this.personRepository.saveAndFlush(person)).thenReturn(person);

    this.sut.save(personDTO);

    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).invalidateEmailVerificationLinkForPerson(Mockito.anyLong());
    Mockito.verify(this.emailVerificationLinkService, Mockito.times(1)).createEmailVerificationLinkForPerson(DEFAULT_ID);
  }

  @Test
  public void updateAndCreateNewEmailVerificationLink() {
    PersonDTO dtoToUpdate = this.createPersonDTOMock(DEFAULT_ID, EmailType.VERIFIED);
    Person personToUpdate = this.createPersonMock(DEFAULT_ID, EmailType.VERIFIED);
    Person existingPerson = this.createPersonMock(DEFAULT_ID, EmailType.MANUALLY);

    Mockito.when(this.personMapper.personDTOToPerson(dtoToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.personRepository.saveAndFlush(personToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(DEFAULT_ID)).thenReturn(null);
    Mockito.when(this.personRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(existingPerson));

    this.sut.save(dtoToUpdate);

    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).invalidateEmailVerificationLinkForPerson(Mockito.any());
    Mockito.verify(this.emailVerificationLinkService, Mockito.times(1)).createEmailVerificationLinkForPerson(DEFAULT_ID);
  }

  @Test
  public void updateAndInvalidateEmailVerificationLink() {
    PersonDTO dtoToUpdate = this.createPersonDTOMock(DEFAULT_ID, EmailType.MANUALLY);
    Person personToUpdate = this.createPersonMock(DEFAULT_ID, EmailType.MANUALLY);
    Person existingPerson = this.createPersonMock(DEFAULT_ID, EmailType.VERIFIED);
    EmailVerificationLinkDTO emailVerificationLinkDTO = this.createEmailVerificationLinkDTOMock();

    Mockito.when(this.personMapper.personDTOToPerson(dtoToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.personRepository.saveAndFlush(personToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(DEFAULT_ID)).thenReturn(emailVerificationLinkDTO);
    Mockito.when(this.personRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(existingPerson));

    this.sut.save(dtoToUpdate);

    Mockito.verify(this.emailVerificationLinkService, Mockito.times(1)).invalidateEmailVerificationLinkForPerson(DEFAULT_ID);
    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).createEmailVerificationLinkForPerson(DEFAULT_ID);
  }

  @Test
  public void updateNoChangesWithVerified() {
    PersonDTO dtoToUpdate = this.createPersonDTOMock(DEFAULT_ID, EmailType.VERIFIED);
    Person personToUpdate = this.createPersonMock(DEFAULT_ID, EmailType.VERIFIED);
    Person existingPerson = this.createPersonMock(DEFAULT_ID, EmailType.VERIFIED);
    EmailVerificationLinkDTO emailVerificationLinkDTO = this.createEmailVerificationLinkDTOMock();

    Mockito.when(this.personMapper.personDTOToPerson(dtoToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.personRepository.saveAndFlush(personToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(DEFAULT_ID)).thenReturn(emailVerificationLinkDTO);
    Mockito.when(this.personRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(existingPerson));

    this.sut.save(dtoToUpdate);

    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).invalidateEmailVerificationLinkForPerson(DEFAULT_ID);
    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).createEmailVerificationLinkForPerson(DEFAULT_ID);
  }

  @Test
  public void updateNoChangesWithManually() {
    PersonDTO dtoToUpdate = this.createPersonDTOMock(DEFAULT_ID, EmailType.MANUALLY);
    Person personToUpdate = this.createPersonMock(DEFAULT_ID, EmailType.MANUALLY);
    Person existingPerson = this.createPersonMock(DEFAULT_ID, EmailType.MANUALLY);

    Mockito.when(this.personMapper.personDTOToPerson(dtoToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.personRepository.saveAndFlush(personToUpdate)).thenReturn(personToUpdate);
    Mockito.when(this.emailVerificationLinkService.getActiveEmailVerificationLinkForPerson(DEFAULT_ID)).thenReturn(null);
    Mockito.when(this.personRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(existingPerson));

    this.sut.save(dtoToUpdate);

    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).invalidateEmailVerificationLinkForPerson(DEFAULT_ID);
    Mockito.verify(this.emailVerificationLinkService, Mockito.never()).createEmailVerificationLinkForPerson(DEFAULT_ID);
  }

  private PersonDTO createPersonDTOMock(Long id, EmailType emailType) {
    PersonDTO personDTO = Mockito.mock(PersonDTO.class);
    Mockito.when(personDTO.getId()).thenReturn(id);
    Mockito.when(personDTO.getEmailType()).thenReturn(emailType);
    return personDTO;
  }

  private Person createPersonMock(Long id, EmailType emailType) {
    Person person = Mockito.mock(Person.class);
    Mockito.when(person.getId()).thenReturn(id);
    Mockito.when(person.getEmailType()).thenReturn(emailType);
    return person;
  }

  private EmailVerificationLinkDTO createEmailVerificationLinkDTOMock() {
    return Mockito.mock(EmailVerificationLinkDTO.class);
  }

}
