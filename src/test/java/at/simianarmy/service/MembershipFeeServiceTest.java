package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.util.Optional;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.repository.MembershipFeeRepository;
import at.simianarmy.service.mapper.MembershipFeeMapper;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeServiceTest {

	private static final Long id1 = new Long(1);
	
	
	private static final String description1 = "description1";
	
	private static final Membership membership = new Membership();
	
	private MembershipFee fee1;
	
	@Inject
	@Spy
	private MembershipFeeMapper MembershipFeeMapper;
	
	
	@Mock
	private MembershipFeeRepository MembershipFeeRepository;
	
	@InjectMocks
	private MembershipFeeService MembershipFeeService;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		fee1 = new MembershipFee();
		fee1.setId(id1);
		fee1.setDescription(description1);

		fee1.addMembership(membership);
		

		
		when(MembershipFeeRepository.findById(id1)).thenReturn(Optional.ofNullable(fee1));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithMembership() {
	  MembershipFeeService.delete(fee1.getId());
	}
}
