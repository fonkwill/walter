package at.simianarmy.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Receipt;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.ReceiptRepository;
import at.simianarmy.service.dto.ReceiptDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.ReceiptMapper;

import java.time.LocalDate;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ReceiptServiceTest {

  private Receipt receipt1;

  private CashbookEntry cashbookEntry1;
  private CashbookEntry cashbookEntry2;

  private static final Long RECEIPT_ID1 = new Long(1);
  private static final Long RECEIPT_ID2 = new Long(2);

  private static final Long CASHBOOKENTRY_ID1 = new Long(1);
  private static final Long CASHBOOKENTRY_ID2 = new Long(2);

  private static final String DEFAULT_IDENTIFIER1 = "A1";
  private static final String DEFAULT_IDENTIFIER2 = "A2";

  private static final String DEFAULT_RECEIPT_CODE = "A";

  @Mock
  private ReceiptRepository receiptRepository;

  @Mock
  private CashbookEntryRepository cashbookEntryRepository;

  @Mock
  private CashbookAccountRepository cashbookAccountRepository;

  @InjectMocks
  private ReceiptService receiptService;

  @Inject
  @Spy
  private ReceiptMapper receiptMapper;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    receipt1 = new Receipt();
    receipt1.setId(RECEIPT_ID1);
    receipt1.setIdentifier(DEFAULT_IDENTIFIER1);
    receipt1.setOverwriteIdentifier(false);

    cashbookEntry1 = new CashbookEntry();
    cashbookEntry1.setId(CASHBOOKENTRY_ID1);
    cashbookEntry1.setReceipt(receipt1);

    cashbookEntry2 = new CashbookEntry();
    cashbookEntry2.setId(CASHBOOKENTRY_ID2);
    cashbookEntry2.setReceipt(null);

    when(receiptRepository.findById(RECEIPT_ID1)).thenReturn(Optional.ofNullable(receipt1));

    when(cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry1));
    when(cashbookEntryRepository.findById(CASHBOOKENTRY_ID2)).thenReturn(Optional.ofNullable(cashbookEntry2));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = ServiceValidationException.class)
  public void testDateInFuture() {
    Receipt receipt = new Receipt();
    receipt.setTitle("Test-Title");
    receipt.setFile("file-1");
    receipt.setDate(LocalDate.MAX);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);

    this.receiptService.save(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testLinkToCashbookEntryWithNoIds() {
    Receipt receipt = new Receipt();

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testLinkToCashbookEntryWithWrongReceiptId() {
    Receipt receipt = new Receipt();
    receipt.setId(new Long(99));

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(new Long(1));

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testLinkToCashbookEntryWithWrongCashbookEntryId() {
    Receipt receipt = new Receipt();
    receipt.setId(RECEIPT_ID1);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(new Long(99));

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testLinkToCashbookEntryWithAlreadyLinkedCashbookEntry() {
    Receipt receipt = new Receipt();
    receipt.setId(RECEIPT_ID1);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUnlinkCashbookEntryWithNoIds() {
    Receipt receipt = new Receipt();

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUnlinkCashbookEntryWithWrongReceiptId() {
    Receipt receipt = new Receipt();
    receipt.setId(new Long(99));

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(new Long(1));

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUnlinkCashbookEntryWithWrongCashbookEntryId() {
    Receipt receipt = new Receipt();
    receipt.setId(RECEIPT_ID1);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(new Long(99));

    this.receiptService.linkToCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUnlinkCashbookEntryWithNoLinkedCashbookEntry() {
    Receipt receipt = new Receipt();
    receipt.setId(RECEIPT_ID1);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);
    receiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID2);

    this.receiptService.unlinkCashbookEntry(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveNewReceiptWithIdentifierSetButNoOverwriting() {
    Receipt receipt = new Receipt();
    receipt.setId(null);
    receipt.setIdentifier(DEFAULT_IDENTIFIER1);
    receipt.setOverwriteIdentifier(false);
    receipt.setDate(LocalDate.MIN);

    ReceiptDTO receiptDTO = this.receiptMapper.receiptToReceiptDTO(receipt);

    this.receiptService.save(receiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveExistingReceiptWithAlteredIdentifierSetButNoOverwriting() {
    Receipt existingReceipt = new Receipt();
    existingReceipt.setIdentifier(DEFAULT_IDENTIFIER1);
    existingReceipt.setOverwriteIdentifier(false);
    existingReceipt.setId(RECEIPT_ID2);

    Mockito.when(this.receiptRepository.findById(RECEIPT_ID2)).thenReturn(Optional.ofNullable(existingReceipt));

    Receipt updatedReceipt = new Receipt();
    updatedReceipt.setId(RECEIPT_ID2);
    updatedReceipt.setIdentifier(DEFAULT_IDENTIFIER2);
    updatedReceipt.setOverwriteIdentifier(false);
    updatedReceipt.setDate(LocalDate.MIN);

    ReceiptDTO updatedReceiptDTO = this.receiptMapper.receiptToReceiptDTO(updatedReceipt);

    this.receiptService.save(updatedReceiptDTO);
  }

  @Test
  public void testAutomaticIdentifierForNewReceipt() {
    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setOverwriteIdentifier(false);
    newReceiptDTO.setDate(LocalDate.MIN);

    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(CASHBOOKENTRY_ID1);

    CashbookAccount cashbookAccount = new CashbookAccount();
    cashbookAccount.setReceiptCode("A");
    cashbookAccount.setCurrentNumber(10);
    cashbookEntry.setCashbookAccount(cashbookAccount);

    newReceiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    Mockito.when(this.cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry));
    Mockito.when(this.cashbookAccountRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(cashbookAccount));

    ArgumentCaptor<Receipt> receiptCaptor = ArgumentCaptor.forClass(Receipt.class);

    this.receiptService.save(newReceiptDTO);

    Mockito.verify(this.receiptRepository, Mockito.times(1)).save(receiptCaptor.capture());
    Receipt savedReceipt = receiptCaptor.getValue();
    assertFalse(savedReceipt.isOverwriteIdentifier());
    assertEquals("A11", savedReceipt.getIdentifier());
    assertEquals(new Integer(11), cashbookAccount.getCurrentNumber());
    assertEquals(cashbookEntry, savedReceipt.getInitialCashbookEntry());
    Mockito.verify(this.cashbookAccountRepository, Mockito.times(1)).save(cashbookAccount);
  }

  @Test
  public void testOverwriteIdentifierForNewReceipt() {
    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setOverwriteIdentifier(true);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER1);
    newReceiptDTO.setDate(LocalDate.MIN);

    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(CASHBOOKENTRY_ID1);

    newReceiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    Mockito.when(this.cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry));

    ArgumentCaptor<Receipt> receiptCaptor = ArgumentCaptor.forClass(Receipt.class);

    this.receiptService.save(newReceiptDTO);

    Mockito.verify(this.receiptRepository, Mockito.times(1)).save(receiptCaptor.capture());
    Receipt savedReceipt = receiptCaptor.getValue();
    assertTrue(savedReceipt.isOverwriteIdentifier());
    assertEquals(DEFAULT_IDENTIFIER1, savedReceipt.getIdentifier());
    assertEquals(cashbookEntry, savedReceipt.getInitialCashbookEntry());
  }

  @Test
  public void testAutomaticIdentifierForExistingReceipt() {
    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(CASHBOOKENTRY_ID1);

    CashbookAccount cashbookAccount = new CashbookAccount();
    cashbookAccount.setReceiptCode("A");
    cashbookAccount.setCurrentNumber(10);
    cashbookEntry.setCashbookAccount(cashbookAccount);

    Receipt existingReceipt = new Receipt();
    existingReceipt.setId(RECEIPT_ID1);
    existingReceipt.setOverwriteIdentifier(true);
    existingReceipt.setIdentifier(DEFAULT_IDENTIFIER1);
    existingReceipt.setInitialCashbookEntry(cashbookEntry);

    Mockito.when(this.receiptRepository.findById(RECEIPT_ID1)).thenReturn(Optional.ofNullable(existingReceipt));

    ReceiptDTO receiptDTO = new ReceiptDTO();
    receiptDTO.setId(RECEIPT_ID1);
    receiptDTO.setOverwriteIdentifier(false);
    receiptDTO.setDate(LocalDate.MIN);

    receiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    Mockito.when(this.cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry));

    ArgumentCaptor<Receipt> receiptCaptor = ArgumentCaptor.forClass(Receipt.class);

    this.receiptService.save(receiptDTO);

    Mockito.verify(this.receiptRepository, Mockito.times(1)).save(receiptCaptor.capture());
    Receipt savedReceipt = receiptCaptor.getValue();
    assertEquals(RECEIPT_ID1, savedReceipt.getId());
    assertFalse(savedReceipt.isOverwriteIdentifier());
    assertEquals("A11", savedReceipt.getIdentifier());
    assertEquals(new Integer(11), cashbookAccount.getCurrentNumber());
    Mockito.verify(this.cashbookAccountRepository, Mockito.times(1)).save(cashbookAccount);
  }

  @Test
  public void testOverwriteIdentifierForExistingReceipt() {
    Receipt existingReceipt = new Receipt();
    existingReceipt.setId(RECEIPT_ID1);
    existingReceipt.setOverwriteIdentifier(true);
    existingReceipt.setIdentifier(DEFAULT_IDENTIFIER1);

    Mockito.when(this.receiptRepository.findById(RECEIPT_ID1)).thenReturn(Optional.ofNullable(existingReceipt));
    Mockito.when(this.receiptRepository.findOneByIdentifier(DEFAULT_IDENTIFIER1)).thenReturn(Optional.ofNullable(null));

    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setId(RECEIPT_ID1);
    newReceiptDTO.setOverwriteIdentifier(true);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER2);
    newReceiptDTO.setDate(LocalDate.MIN);

    ArgumentCaptor<Receipt> receiptCaptor = ArgumentCaptor.forClass(Receipt.class);

    this.receiptService.save(newReceiptDTO);

    Mockito.verify(this.receiptRepository, Mockito.times(1)).save(receiptCaptor.capture());
    Receipt savedReceipt = receiptCaptor.getValue();
    assertTrue(savedReceipt.isOverwriteIdentifier());
    assertEquals(DEFAULT_IDENTIFIER2, savedReceipt.getIdentifier());
  }

  @Test
  public void testSaveExistingReceiptWithAutomaticIdentifier() {
    Receipt existingReceipt = new Receipt();
    existingReceipt.setId(RECEIPT_ID1);
    existingReceipt.setOverwriteIdentifier(false);
    existingReceipt.setIdentifier(DEFAULT_IDENTIFIER1);

    Mockito.when(this.receiptRepository.findById(RECEIPT_ID1)).thenReturn(Optional.ofNullable(existingReceipt));

    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setId(RECEIPT_ID1);
    newReceiptDTO.setOverwriteIdentifier(false);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER1);
    newReceiptDTO.setDate(LocalDate.MIN);

    ArgumentCaptor<Receipt> receiptCaptor = ArgumentCaptor.forClass(Receipt.class);

    this.receiptService.save(newReceiptDTO);

    Mockito.verify(this.receiptRepository, Mockito.times(1)).save(receiptCaptor.capture());
    Receipt savedReceipt = receiptCaptor.getValue();
    assertFalse(savedReceipt.isOverwriteIdentifier());
    assertEquals(DEFAULT_IDENTIFIER1, savedReceipt.getIdentifier());
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveExistingReceiptIdentifierAlreadyUsed() {
    Receipt existingReceipt = new Receipt();
    existingReceipt.setId(RECEIPT_ID1);
    existingReceipt.setOverwriteIdentifier(true);
    existingReceipt.setIdentifier(DEFAULT_IDENTIFIER1);

    Mockito.when(this.receiptRepository.findById(RECEIPT_ID1)).thenReturn(Optional.ofNullable(existingReceipt));
    Mockito.when(this.receiptRepository.findOneByIdentifier(DEFAULT_IDENTIFIER2)).thenReturn(Optional.ofNullable(receipt1));

    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setId(RECEIPT_ID1);
    newReceiptDTO.setOverwriteIdentifier(true);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER2);
    newReceiptDTO.setDate(LocalDate.MIN);

    this.receiptService.save(newReceiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveNewReceiptIdentifierAlreadyUsed() {
    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setOverwriteIdentifier(true);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER1);
    newReceiptDTO.setDate(LocalDate.MIN);

    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(CASHBOOKENTRY_ID1);

    newReceiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    Mockito.when(this.cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry));
    Mockito.when(this.receiptRepository.findOneByIdentifier(DEFAULT_IDENTIFIER1)).thenReturn(Optional.ofNullable(receipt1));

    this.receiptService.save(newReceiptDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveAutoReceiptIdentifier() {
    ReceiptDTO newReceiptDTO = new ReceiptDTO();
    newReceiptDTO.setOverwriteIdentifier(true);
    newReceiptDTO.setIdentifier(DEFAULT_IDENTIFIER1);
    newReceiptDTO.setDate(LocalDate.MIN);

    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(CASHBOOKENTRY_ID1);

    newReceiptDTO.setCashbookEntryId(CASHBOOKENTRY_ID1);

    Mockito.when(this.cashbookEntryRepository.findById(CASHBOOKENTRY_ID1)).thenReturn(Optional.ofNullable(cashbookEntry));
    Mockito.when(this.cashbookAccountRepository.findOneByReceiptCode(DEFAULT_RECEIPT_CODE)).thenReturn(Optional.ofNullable(
      Mockito.mock(CashbookAccount.class)
    ));

    this.receiptService.save(newReceiptDTO);
  }
}
