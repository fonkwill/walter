package at.simianarmy.service;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.repository.AppointmentRepository;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonGroupMapper;
import at.simianarmy.service.mapper.PersonMapper;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonServiceTest {

  private Person p1;
  private Person p2;
  private Person p3;

  private PersonDTO p1dto;
  private PersonDTO p2dto;
  private PersonDTO p3dto;

  private PersonGroup pg1;
  private PersonGroup pg2;
  private PersonGroup pg3;
  private PersonGroup pg4;

  private Appointment appointment;

  private static final String fn1 = "Hans";
  private static final String fn2 = "Franz";
  private static final String fn3 = "Hans-Joachim";

  private static final String ln1 = "Huber";
  private static final String ln2 = "Hubertans";
  private static final String ln3 = "Franziskus";

  private static final String pgName1 = "pg1";
  private static final String pgName2 = "pg2";
  private static final String pgName3 = "pg3";
  private static final String pgName4 = "pg4";
  private static final String pgName5 = "pg5";
  private static final Long unknownPersonGroupId = new Long(9999);
  private static final String appointmentName = "appointment";

  private static final LocalDate bd1 = LocalDate.of(1950, 05, 01);
  private static final LocalDate bd2 = LocalDate.of(1980, 06, 15);
  private static final LocalDate bd3 = LocalDate.of(1960, 10, 20);

  // pageable like the rest interface creates
  private static final Pageable page = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
  @Inject
  private PersonRepository personRepository;

  @Inject
  private PersonGroupRepository personGroupRepository;

  @Inject
  private AppointmentRepository appointmentRepository;

  @Inject
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Inject
  private PersonService personService;

  @Inject
  private PersonMapper personMapper;

  @Inject
  private PersonGroupMapper personGroupMapper;

  @Before
  public void setUp() throws Exception {
    p1 = new Person();
    p1.setFirstName(fn1);
    p1.setLastName(ln1);
    p1.setStreetAddress("street 1");
    p1.setCity("city 1");
    p1.setCountry("country 1");
    p1.setPostalCode("1010");
    p1.setBirthDate(bd1);
    p1.setHasDirectDebit(false);

    p2 = new Person();
    p2.setFirstName(fn2);
    p2.setLastName(ln2);
    p2.setStreetAddress("street 2");
    p2.setCity("city 2");
    p2.setCountry("country 2");
    p2.setPostalCode("1020");
    p2.setBirthDate(bd2);
    p2.setHasDirectDebit(false);

    p3 = new Person();
    p3.setFirstName(fn3);
    p3.setLastName(ln3);
    p3.setStreetAddress("street 3");
    p3.setCity("city 3");
    p3.setCountry("country 3");
    p3.setPostalCode("1030");
    p3.setBirthDate(bd3);
    p3.setHasDirectDebit(false);

    p1 = personRepository.saveAndFlush(p1);
    p2 = personRepository.saveAndFlush(p2);
    p3 = personRepository.saveAndFlush(p3);

    p1dto = personMapper.personToPersonDTO(p1);
    p2dto = personMapper.personToPersonDTO(p2);
    p3dto = personMapper.personToPersonDTO(p3);

    this.pg1 = new PersonGroup();
    this.pg1.setName(pgName1);
    this.pg1.addPersons(this.p1);
    this.personGroupRepository.saveAndFlush(this.pg1);

    this.pg2 = new PersonGroup();
    this.pg2.setName(pgName2);
    this.pg2.addPersons(this.p2);
    this.personGroupRepository.saveAndFlush(this.pg2);

    this.pg3 = new PersonGroup();
    this.pg3.setName(pgName3);
    this.pg3.addPersons(this.p3);
    this.personGroupRepository.saveAndFlush(this.pg3);

    this.pg4 = new PersonGroup();
    this.pg4.setName(pgName4);
    this.personGroupRepository.saveAndFlush(this.pg4);

    this.pg1.addChildren(this.pg2);
    this.pg2.addChildren(this.pg3);

    this.personGroupRepository.saveAndFlush(this.pg1);
    this.personGroupRepository.saveAndFlush(this.pg2);

    this.appointment = new Appointment();
    this.appointment.setName(appointmentName);
    this.appointment.setBeginDate(ZonedDateTime.now());
    this.appointment.setEndDate(ZonedDateTime.now().plusDays(1));
    this.appointment.addPersons(p1);
    this.appointment.addPersons(p2);
    this.appointmentRepository.saveAndFlush(this.appointment);

  }

  @After
  public void tearDown() throws Exception {
    p1 = null;
    p2 = null;
    p3 = null;

    pg1 = null;
    pg2 = null;
    pg3 = null;


  }

  @Test
  public void testWithCompleteFirstName() {
    Page<PersonDTO> result = personService.findAll(fn1, null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p1dto, p3dto));

  }

  @Test
  public void testWithNullParameters() {
    Page<PersonDTO> result = personService.findAll(null, null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));

  }

  @Test
  public void testWithCompleteLastName() {
    Page<PersonDTO> result = personService.findAll(ln1, null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto));
  }

  @Test
  public void testWithPartOnlyFirstName() {
    Page<PersonDTO> result = personService.findAll("oach", null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testWithPartOnlyLasttName() {
    Page<PersonDTO> result = personService.findAll("zisku", null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testWithPartFirstAndLastName() {
    Page<PersonDTO> result = personService.findAll("ans", null, null, null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));

  }

  @Test
  public void testBetweenDates() {
    Page<PersonDTO> result = personService.findAll(null, LocalDate.of(1955, 01, 01),
        LocalDate.of(1970, 01, 01), null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testBeforeDate() {
    Page<PersonDTO> result = personService.findAll(null, null, LocalDate.of(1970, 01, 01), null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p1dto, p3dto));

  }

  @Test
  public void testAfterDate() {
    Page<PersonDTO> result = personService.findAll(null, LocalDate.of(1955, 01, 01), null, null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p2dto, p3dto));

  }

  @Test
  public void testAfterDateInclusive() {
    Page<PersonDTO> result = personService.findAll(null, LocalDate.of(1950, 05, 01), null, null, null,
        page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }

  @Test
  public void testBeforeDateInclusive() {
    Page<PersonDTO> result = personService.findAll(null, null, LocalDate.of(1980, 06, 15), null, null,
        page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }

  @Test
  public void testBetweenDateInclusive() {
    Page<PersonDTO> result = personService.findAll(null, LocalDate.of(1950, 05, 01),
        LocalDate.of(1980, 06, 15), null, null, page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }

  @Test
  public void testBeginEqualEnd() {
    Page<PersonDTO> result = personService.findAll(null, LocalDate.of(1960, 10, 20),
        LocalDate.of(1960, 10, 20), null, null, page);
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));
  }

  @Test
  public void testWithinPersonGroups() {
    List<Long> personGroupIds = new ArrayList<Long>(Arrays.asList(this.pg3.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));
  }

  @Test
  public void testWithinPersonGroups2() {
    this.pg3.addPersons(this.p2);
    this.personGroupRepository.saveAndFlush(this.pg3);

    List<Long> personGroupIds = new ArrayList<Long>(Arrays.asList(this.pg3.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p2dto, p3dto));
  }

  @Test
  public void testWithinPersonGroupsNoResult() {
    List<Long> personGroupIds = new ArrayList<Long>(Arrays.asList(this.pg4.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(0));
  }

  @Test
  public void testWithinPersonGroupsEmptyList() {
    List<Long> personGroupIds = new ArrayList<Long>();
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }
  
  @Test
  public void testWithinPersonGroupsAll() {
    List<Long> personGroupIds = new ArrayList<Long>(
        Arrays.asList(this.pg1.getId(), this.pg2.getId(), this.pg3.getId(), this.pg4.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }
  
  @Test
  public void testWithinPersonGroupsRecursive() {
    
    PersonGroup pg5 = new PersonGroup();
    pg5.setName(pgName5);
    pg5.addChildren(pg3);
    this.personGroupRepository.saveAndFlush(pg5);
    
    List<Long> personGroupIds = new ArrayList<Long>(Arrays.asList(pg5.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));
  }
  
  @Test
  public void testWithinPersonGroupsRecursive2() {
    
    PersonGroup pg5 = new PersonGroup();
    pg5.setName(pgName5);
    pg5.addChildren(pg1);
    pg1.addChildren(pg2);
    pg2.addChildren(pg3);
    this.personGroupRepository.saveAndFlush(pg5);
    this.personGroupRepository.saveAndFlush(pg1);
    this.personGroupRepository.saveAndFlush(pg2);
    
    List<Long> personGroupIds = new ArrayList<Long>(Arrays.asList(pg5.getId()));
    Page<PersonDTO> result = personService.findAll(null, null, null, null, personGroupIds, page);
    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }

  @Test
  public void testWithPartFirstAndLastNameBeforeDate() {
    Page<PersonDTO> result = personService.findAll("ans", null, LocalDate.of(1970, 01, 01), null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p1dto, p3dto));

  }

  @Test
  public void testWithPartFirstAndLastNameAfterDate() {
    Page<PersonDTO> result = personService.findAll("ans", LocalDate.of(1955, 01, 01), null, null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p2dto, p3dto));

  }

  @Test
  public void testWithPartFirstAndLastNameBetweenDate() {
    Page<PersonDTO> result = personService.findAll("ans", LocalDate.of(1955, 01, 01),
        LocalDate.of(1970, 01, 01), null, null, page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testWithCompleteNameBeforeDate() {
    Page<PersonDTO> result = personService.findAll(ln3, null, LocalDate.of(1970, 01, 01), null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testWithCompleteNameAfterDate() {
    Page<PersonDTO> result = personService.findAll(ln3, LocalDate.of(1955, 01, 01), null, null, null,
        page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));

  }

  @Test
  public void testFindByPersonGroup1() {
    Page<PersonDTO> result = this.personService.findByPersonGroup(this.pg1.getId(), page);

    assertThat(result.getContent().size(), Matchers.equalTo(3));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto, p3dto));
  }

  @Test
  public void testFindByPersonGroup2() {
    Page<PersonDTO> result = this.personService.findByPersonGroup(this.pg2.getId(), page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p2dto, p3dto));
  }

  @Test
  public void testFindByPersonGroup3() {
    Page<PersonDTO> result = this.personService.findByPersonGroup(this.pg3.getId(), page);

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(p3dto));
  }

  @Test
  public void testFindByPersonGroup4() {
    Page<PersonDTO> result = this.personService.findByPersonGroup(this.pg4.getId(), page);

    assertThat(result.getContent().size(), Matchers.equalTo(0));
  }

  @Test
  public void testFindByAppointment() {
    Page<PersonDTO> result = this.personService.findByAppointment(this.appointment.getId(), page);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(p1dto, p2dto));
  }

  @Test(expected = ServiceValidationException.class)
  public void testBeginAferEnd() {
    personService.findAll(null, LocalDate.of(1955, 01, 01), LocalDate.of(1950, 01, 01), null, null, page);
  }

  @Test(expected = ServiceValidationException.class)
  public void testFindByPersonGroupShouldThrowException() {
    this.personService.findByPersonGroup(unknownPersonGroupId, page);
  }
}
