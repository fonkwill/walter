package at.simianarmy.service;

import static org.junit.Assert.assertEquals;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColumnConfig;
import at.simianarmy.domain.User;
import at.simianarmy.domain.UserColumnConfig;
import at.simianarmy.repository.ColumnConfigRepository;
import at.simianarmy.repository.UserColumnConfigRepository;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.service.dto.UserColumnConfigDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.UserColumnConfigMapper;
import java.util.ArrayList;
import java.util.Arrays;
import javax.persistence.Column;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class UserColumnConfigServiceTest {

  @Mock
  private UserColumnConfigRepository userColumnConfigRepository;

  @Mock
  private ColumnConfigRepository columnConfigRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private UserColumnConfigMapper userColumnConfigMapper;

  @InjectMocks
  private UserColumnConfigService sut;

  @Test
  public void testDeleteColumnConfigsForUser() {
   User user = Mockito.mock(User.class);
   UserColumnConfig userColumnConfig = Mockito.mock(UserColumnConfig.class);

   Mockito.when(this.userColumnConfigRepository.findByUser(user)).thenReturn(new ArrayList<UserColumnConfig>(
     Arrays.asList(userColumnConfig)));

   this.sut.deleteColumnConfigsForUser(user);

   Mockito.verify(this.userColumnConfigRepository, Mockito.times(1)).delete(userColumnConfig);
  }

  @Test
  public void testCreateColumnConfigsForUser() {
    User user = Mockito.mock(User.class);
    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.isDefaultVisible()).thenReturn(true);
    Mockito.when(columnConfig.getDefaultPosition()).thenReturn(1);

    Mockito.when(this.columnConfigRepository.findAll()).thenReturn(new ArrayList<ColumnConfig>(Arrays.asList(columnConfig)));
    Mockito.when(this.userColumnConfigRepository.existsByColumnConfigAndUser(columnConfig, user)).thenReturn(false);

    ArgumentCaptor<UserColumnConfig> argument = ArgumentCaptor.forClass(UserColumnConfig.class);

    this.sut.generateColumnConfigsForUser(user);

    Mockito.verify(this.userColumnConfigRepository, Mockito.times(1)).saveAndFlush(argument.capture());

    UserColumnConfig result = argument.getValue();

    assertEquals(user, result.getUser());
    assertEquals(columnConfig, result.getColumnConfig());
    assertEquals(true, result.isVisible());
    assertEquals(1, result.getPosition().longValue());
  }

  @Test
  public void testCreateColumnConfigsForUserNothingToDo() {
    User user = Mockito.mock(User.class);
    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.isDefaultVisible()).thenReturn(true);

    Mockito.when(this.columnConfigRepository.findAll()).thenReturn(new ArrayList<ColumnConfig>(Arrays.asList(columnConfig)));
    Mockito.when(this.userColumnConfigRepository.existsByColumnConfigAndUser(columnConfig, user)).thenReturn(true);
    this.sut.generateColumnConfigsForUser(user);

    Mockito.verify(this.userColumnConfigRepository, Mockito.never()).saveAndFlush(Mockito.any(UserColumnConfig.class));
  }

  @Test(expected = ServiceValidationException.class)
  @WithMockUser(username = "SOME_USER", password = "pwd", roles = "USER")
  public void testUpdateNotAllowed() {
    User user = Mockito.mock(User.class);
    Mockito.when(user.getId()).thenReturn(new Long(1));
    Mockito.when(this.userRepository.getOne(new Long(1))).thenReturn(user);
    Mockito.when(user.getLogin()).thenReturn("OTHER_USER");

    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.getId()).thenReturn(new Long(1));
    Mockito.when(this.columnConfigRepository.getOne(new Long(1))).thenReturn(columnConfig);
    Mockito.when(columnConfig.getEntity()).thenReturn("ENTITY");

    UserColumnConfigDTO dtoToUpdate = Mockito.mock(UserColumnConfigDTO.class);
    UserColumnConfig toUpdate = Mockito.mock(UserColumnConfig.class);
    Mockito.when(toUpdate.getUser()).thenReturn(user);
    Mockito.when(toUpdate.getColumnConfig()).thenReturn(columnConfig);
    Mockito.when(toUpdate.isVisible()).thenReturn(true);
    ArrayList<UserColumnConfigDTO> dtosToUpdate = new ArrayList<UserColumnConfigDTO>(Arrays.asList(dtoToUpdate));
    ArrayList<UserColumnConfig> updates = new ArrayList<UserColumnConfig>(Arrays.asList(toUpdate));
    Mockito.when(this.userColumnConfigMapper.toEntity(dtosToUpdate)).thenReturn(updates);

    this.sut.update(dtosToUpdate);
  }

  @Test
  @WithMockUser(username = "SOME_USER", password = "pwd", roles = "USER")
  public void testUpdateAllowed() {
    User user = Mockito.mock(User.class);
    Mockito.when(user.getId()).thenReturn(new Long(1));
    Mockito.when(this.userRepository.getOne(new Long(1))).thenReturn(user);
    Mockito.when(user.getLogin()).thenReturn("SOME_USER");

    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.getId()).thenReturn(new Long(1));
    Mockito.when(this.columnConfigRepository.getOne(new Long(1))).thenReturn(columnConfig);
    Mockito.when(columnConfig.getEntity()).thenReturn("ENTITY");

    UserColumnConfigDTO dtoToUpdate = Mockito.mock(UserColumnConfigDTO.class);
    UserColumnConfig toUpdate = Mockito.mock(UserColumnConfig.class);
    Mockito.when(toUpdate.getUser()).thenReturn(user);
    Mockito.when(toUpdate.getColumnConfig()).thenReturn(columnConfig);
    Mockito.when(toUpdate.isVisible()).thenReturn(true);
    ArrayList<UserColumnConfigDTO> dtosToUpdate = new ArrayList<UserColumnConfigDTO>(Arrays.asList(dtoToUpdate));
    ArrayList<UserColumnConfig> updates = new ArrayList<UserColumnConfig>(Arrays.asList(toUpdate));
    Mockito.when(this.userColumnConfigMapper.toEntity(dtosToUpdate)).thenReturn(updates);

    this.sut.update(dtosToUpdate);

    Mockito.verify(this.userColumnConfigRepository, Mockito.times(1)).saveAndFlush(toUpdate);
  }

  @Test(expected = ServiceValidationException.class)
  @WithMockUser(username = "SOME_USER", password = "pwd", roles = "USER")
  public void testNoneSelected() {
    User user = Mockito.mock(User.class);
    Mockito.when(user.getId()).thenReturn(new Long(1));
    Mockito.when(this.userRepository.getOne(new Long(1))).thenReturn(user);
    Mockito.when(user.getLogin()).thenReturn("SOME_USER");

    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.getId()).thenReturn(new Long(1));
    Mockito.when(this.columnConfigRepository.getOne(new Long(1))).thenReturn(columnConfig);
    Mockito.when(columnConfig.getEntity()).thenReturn("ENTITY");

    UserColumnConfigDTO dtoToUpdate = Mockito.mock(UserColumnConfigDTO.class);
    UserColumnConfig toUpdate = Mockito.mock(UserColumnConfig.class);
    Mockito.when(toUpdate.getUser()).thenReturn(user);
    Mockito.when(toUpdate.getColumnConfig()).thenReturn(columnConfig);
    Mockito.when(toUpdate.isVisible()).thenReturn(false);
    ArrayList<UserColumnConfigDTO> dtosToUpdate = new ArrayList<UserColumnConfigDTO>(Arrays.asList(dtoToUpdate));
    ArrayList<UserColumnConfig> updates = new ArrayList<UserColumnConfig>(Arrays.asList(toUpdate));
    Mockito.when(this.userColumnConfigMapper.toEntity(dtosToUpdate)).thenReturn(updates);

    this.sut.update(dtosToUpdate);
  }

  @Test(expected = ServiceValidationException.class)
  @WithMockUser(username = "SOME_USER", password = "pwd", roles = "USER")
  public void testSeveralEntities() {
    User user = Mockito.mock(User.class);
    Mockito.when(user.getId()).thenReturn(new Long(1));
    Mockito.when(this.userRepository.getOne(new Long(1))).thenReturn(user);
    Mockito.when(user.getLogin()).thenReturn("SOME_USER");

    ColumnConfig columnConfig = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig.getId()).thenReturn(new Long(1));
    Mockito.when(this.columnConfigRepository.getOne(new Long(1))).thenReturn(columnConfig);
    Mockito.when(columnConfig.getEntity()).thenReturn("ENTITY");

    ColumnConfig columnConfig2 = Mockito.mock(ColumnConfig.class);
    Mockito.when(columnConfig2.getId()).thenReturn(new Long(2));
    Mockito.when(this.columnConfigRepository.getOne(new Long(2))).thenReturn(columnConfig2);
    Mockito.when(columnConfig2.getEntity()).thenReturn("ENTITY2");

    UserColumnConfigDTO dtoToUpdate = Mockito.mock(UserColumnConfigDTO.class);
    UserColumnConfigDTO dtoToUpdate2 = Mockito.mock(UserColumnConfigDTO.class);
    UserColumnConfig toUpdate = Mockito.mock(UserColumnConfig.class);
    Mockito.when(toUpdate.getUser()).thenReturn(user);
    Mockito.when(toUpdate.getColumnConfig()).thenReturn(columnConfig);
    Mockito.when(toUpdate.isVisible()).thenReturn(true);
    UserColumnConfig toUpdate2 = Mockito.mock(UserColumnConfig.class);
    Mockito.when(toUpdate2.getUser()).thenReturn(user);
    Mockito.when(toUpdate2.getColumnConfig()).thenReturn(columnConfig2);
    Mockito.when(toUpdate2.isVisible()).thenReturn(true);
    ArrayList<UserColumnConfigDTO> dtosToUpdate = new ArrayList<UserColumnConfigDTO>(Arrays.asList(dtoToUpdate, dtoToUpdate2));
    ArrayList<UserColumnConfig> updates = new ArrayList<UserColumnConfig>(Arrays.asList(toUpdate, toUpdate2));
    Mockito.when(this.userColumnConfigMapper.toEntity(dtosToUpdate)).thenReturn(updates);

    this.sut.update(dtosToUpdate);
  }

}
