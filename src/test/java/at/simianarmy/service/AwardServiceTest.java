package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Award;
import at.simianarmy.domain.PersonAward;
import at.simianarmy.repository.AwardRepository;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AwardServiceTest {

	private static final Long id1 = new Long(1);
	
	private static final Long personAwardId = new Long(10);
	
	private static final String name1 = "name1";
	
	private static final PersonAward personAward = new PersonAward();
	
	private Award award1;
	
	@Mock
	private AwardRepository AwardRepository;
	
	@InjectMocks
	private AwardService AwardService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		award1 = new Award();
		award1.setId(id1);
		award1.setName(name1);
		
		personAward.setId(personAwardId);;
		
		award1.addPersonAwards(personAward); //must not be deleted
		
		when(AwardRepository.findById(id1)).thenReturn(Optional.ofNullable(award1));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithPersonAward() {
	  AwardService.delete(award1.getId());
	}
}
