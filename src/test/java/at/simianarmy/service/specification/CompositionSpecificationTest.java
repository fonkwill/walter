package at.simianarmy.service.specification;

import static org.assertj.core.api.Assertions.assertThat;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.Genre;
import at.simianarmy.domain.MusicBook;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.repository.GenreRepository;
import at.simianarmy.repository.MusicBookRepository;

import java.util.HashSet;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompositionSpecificationTest {

  private static final Integer DEFAULT_NUMBER = 1;
  private static final Integer DEFAULT_NUMBER2 = 2;
  private static final String DEFAULT_TITLE = "TITLE";
  private static final String DEFAULT_TITLE2 = "BEZEICHNUNG";
  private static final String DEFAULT_COMPANY1 = "COMPANY1";
  private static final String DEFAULT_COMPANY2 = "COMPANY2";
  private static final String DEFAULT_COLORCODE = "COLOR1";
  private static final String DEFAULT_COLORCODE2 = "COLOR2";
  private static final String DEFAULT_COMPOSER = "COMPOSER";
  private static final String DEFAULT_ARRANGER = "ARRANGER";
  private static final String DEFAULT_GENRE = "GENRE";
  private static final String DEFAULT_MUSICBOOK = "MUSICBOOK";

  @Inject
  private CompositionRepository compositionRepository;

  @Inject
  private ColorCodeRepository colorCodeRepository;

  @Inject
  private CompanyRepository companyRepository;

  @Inject
  private ComposerRepository composerRepository;

  @Inject
  private ArrangerRepository arrangerRepository;

  @Inject
  private GenreRepository genreRepository;

  @Inject
  private MusicBookRepository musicBookRepository;

  @Inject
  private CompositionSpecification compositionSpecification;

  private Pageable pageable;

  private Composition composition;

  private Composition composition2;

  private ColorCode colorCode;

  private ColorCode colorCode2;

  private Company orderingCompany;

  private Company publisher;

  private Composer composer;

  private Arranger arranger;

  private Genre genre;

  private MusicBook musicBook;

  @Before
  public void setUp() {
    this.pageable = PageRequest.of(0, 9999);

    this.colorCode = new ColorCode();
    colorCode.setName(DEFAULT_COLORCODE);
    this.colorCodeRepository.saveAndFlush(colorCode);

    this.orderingCompany = new Company();
    this.orderingCompany.setName(DEFAULT_COMPANY1);
    this.companyRepository.saveAndFlush(this.orderingCompany);

    this.publisher = new Company();
    this.publisher.setName(DEFAULT_COMPANY2);
    this.companyRepository.saveAndFlush(this.publisher);

    this.composer = new Composer();
    this.composer.setName(DEFAULT_COMPOSER);
    this.composerRepository.saveAndFlush(this.composer);

    this.arranger = new Arranger();
    this.arranger.setName(DEFAULT_ARRANGER);
    this.arrangerRepository.saveAndFlush(this.arranger);

    this.genre = new Genre();
    this.genre.setName(DEFAULT_GENRE);
    this.genreRepository.saveAndFlush(this.genre);

    this.musicBook = new MusicBook();
    this.musicBook.setName(DEFAULT_MUSICBOOK);
    this.musicBookRepository.saveAndFlush(this.musicBook);

    this.composition = new Composition();
    this.composition.setTitle(DEFAULT_TITLE);
    this.composition.setColorCode(colorCode);
    this.composition.setAkmCompositionNr(DEFAULT_NUMBER);
    this.composition.setOrderingCompany(orderingCompany);
    this.composition.setPublisher(publisher);
    this.composition.setGenre(this.genre);
    this.composition.setMusicBook(this.musicBook);

    this.compositionRepository.saveAndFlush(this.composition);

    this.composer.addCompositions(this.composition);
    this.composerRepository.saveAndFlush(this.composer);

    this.arranger.addCompositions(this.composition);
    this.arrangerRepository.saveAndFlush(this.arranger);

    this.colorCode2 = new ColorCode();
    this.colorCode2.setName(DEFAULT_COLORCODE2);
    this.colorCodeRepository.saveAndFlush(this.colorCode2);

    this.composition2 = new Composition();
    this.composition2.setTitle(DEFAULT_TITLE2);
    this.composition2.setColorCode(colorCode2);
    this.composition2.setAkmCompositionNr(DEFAULT_NUMBER2);
    this.compositionRepository.saveAndFlush(this.composition2);
  }

  @After
  public void tearDown() {
    for (Composer composer : this.composerRepository.findAll()) {
      composer.compositions(new HashSet<Composition>());
      this.composerRepository.saveAndFlush(composer);
    }

    for (Arranger arranger : this.arrangerRepository.findAll()) {
      arranger.compositions(new HashSet<Composition>());
      this.arrangerRepository.saveAndFlush(arranger);
    }

    this.composerRepository.deleteAll();
    this.arrangerRepository.deleteAll();
    this.compositionRepository.deleteAll();
    this.companyRepository.deleteAll();
    this.musicBookRepository.deleteAll();
    this.genreRepository.deleteAll();
  }

  @Test
  public void testHasNr() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasNr(DEFAULT_NUMBER).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasNrNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasNr(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasColorCode() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasColorCode(this.colorCode.getId()).build(),
        this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasColorCodeNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasColorCode(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasTitle() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasTitle(DEFAULT_TITLE).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasTitleNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasTitle(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasOrderingCompany() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasOrderingCompany(DEFAULT_COMPANY1).build(),
        this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasOrderingCompanyNull() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasOrderingCompany(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasPublisher() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasPublisher(DEFAULT_COMPANY2).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasPublisherNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasPublisher(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasComposer() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasComposer(DEFAULT_COMPOSER).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasComposerNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasComposer(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasArranger() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasArranger(DEFAULT_ARRANGER).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasArrangerNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasArranger(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasGenre() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasGenre(DEFAULT_GENRE).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasGenreNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasGenre(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testHasMusicBook() {
    Page<Composition> page = this.compositionRepository.findAll(
        this.compositionSpecification.init().hasMusicBook(this.musicBook.getId()).build(),
        this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testHasMusicBookNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasMusicBook(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }
  
  @Test
  public void testIsActiveTrue() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().isActive(true).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }
  
  @Test
  public void testIsActiveFalse() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().isActive(false).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition2);
  }
  
  @Test
  public void testIsActiveNull() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().isActive(null).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testFindAll() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testFindComplex() {
    Page<Composition> page = this.compositionRepository.findAll(this.compositionSpecification.init()
        .hasColorCode(this.colorCode.getId()).hasTitle(DEFAULT_TITLE).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testFindComplex2() {
    Page<Composition> page = this.compositionRepository.findAll(this.compositionSpecification.init()
        .hasColorCode(this.colorCode2.getId()).hasTitle(DEFAULT_TITLE).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(0);
  }

  @Test
  public void testFindComplex3() {
    Page<Composition> page = this.compositionRepository.findAll(this.compositionSpecification.init()
        .hasTitle("I").hasOrderingCompany(DEFAULT_COMPANY1).build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testFindComplex4() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasTitle("I").build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(2);
    assertThat(page.getContent()).contains(this.composition);
    assertThat(page.getContent()).contains(this.composition2);
  }

  @Test
  public void testFindComplex5() {
    Page<Composition> page = this.compositionRepository.findAll(this.compositionSpecification.init()
        .hasTitle("I").hasPublisher(DEFAULT_COMPANY2).hasColorCode(this.colorCode.getId()).build(),
        this.pageable);

    assertThat(page.getContent().size()).isEqualTo(1);
    assertThat(page.getContent()).contains(this.composition);
  }

  @Test
  public void testFindNothing() {
    Page<Composition> page = this.compositionRepository
        .findAll(this.compositionSpecification.init().hasGenre("XYZ").build(), this.pageable);

    assertThat(page.getContent().size()).isEqualTo(0);
  }
}
