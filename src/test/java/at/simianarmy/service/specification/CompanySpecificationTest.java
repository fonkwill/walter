package at.simianarmy.service.specification;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.PersonRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class CompanySpecificationTest {
	


	private static final String firstName = "hans";
	private static final String lastName = "huber";
	private static final String address = "abc1";
	private static final String plz = "1010";
	private static final String city = "Wien";
	private static final String country = "Österreich";
	@Inject
	private PersonRepository personRepository;
	
	@Inject
	private PersonGroupRepository personGroupRepository;
	
	private Person p1;
	private Person p2;
	
	
	private PersonGroup pg1;
	private PersonGroup pg2;
	
	
	@Before
	public void setUp() throws Exception {
	    p1 = new Person();
	    p1.setGender(Gender.MALE);
	    p1.setFirstName(firstName);
	    p1.setLastName(lastName);
	    p1.setStreetAddress(address);
	    p1.setPostalCode(plz);
	    p1.setCity(city);
	    p1.setCountry(country);
      p1.setHasDirectDebit(false);

	    p1 = personRepository.save(p1);
	    
	    p2 = new Person();
	    p2.setGender(Gender.MALE);
	    p2.setFirstName(firstName);
	    p2.setLastName(lastName);
	    p2.setStreetAddress(address);
	    p2.setPostalCode(plz);
	    p2.setCity(city);
	    p2.setCountry(country);
      p2.setHasDirectDebit(false);

	    p2 = personRepository.save(p2);
	    
	    
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_isInPersongroups_oneGroup() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg1.addPersons(p1);
		pg1.addPersons(p2);
		
		pg1 = personGroupRepository.save(pg1);
		
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		
		Specification<Person> specs = PersonSpecifications.isInPersonGroup(pgs);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(p1, p2));
		
	}
	
	@Test
	public void test_isInPersongroups_twoGroup_hierarchy() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg2 = new PersonGroup();
		pg2.setName("gr2");
		pg1.addChildren(pg2);
		pg2.setParent(pg1);
		
		pg1.addPersons(p1);
		pg2.addPersons(p2);
		
		pg1 = personGroupRepository.save(pg1);
		pg2 = personGroupRepository.save(pg2);
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		pgs.add(pg2.getId());
		
		Specification<Person> specs = PersonSpecifications.isInPersonGroup(pgs);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(p1, p2));
		
	}
	

	
	@Test
	public void test_isInPersongroups_twoGroup_hierarchy_multiple() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg2 = new PersonGroup();
		pg2.setName("gr2");
		pg1.addChildren(pg2);
		pg2.setParent(pg1);
		
		pg1.addPersons(p1);
		pg2.addPersons(p2);
		pg2.addPersons(p1);
		
		pg1 = personGroupRepository.save(pg1);
		pg2 = personGroupRepository.save(pg2);
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		pgs.add(pg2.getId());
		
		Specification<Person> specs = PersonSpecifications.isInPersonGroup(pgs);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.iterableWithSize(2));
		assertThat(persons, Matchers.containsInAnyOrder(p1, p2));
		
	}
	
	@Test
	public void testGender_None() {
		Specification<Person> specs = PersonSpecifications.hasGender(null);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(p1, p2));
	}
	
	@Test
	public void testGender_male() {
		Specification<Person> specs = PersonSpecifications.hasGender(Gender.MALE);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(p1, p2));
	}
	
	@Test
	public void testGender_female() {
		Specification<Person> specs = PersonSpecifications.hasGender(Gender.FEMALE);
		
		List<Person> persons = personRepository.findAll(specs);
		assertThat(persons, Matchers.hasSize(0));
	}
	

	
}
