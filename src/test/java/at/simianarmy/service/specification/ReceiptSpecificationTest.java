package at.simianarmy.service.specification;

import static org.junit.Assert.assertThat;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Receipt;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.ReceiptRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class ReceiptSpecificationTest {

  private Company company1;

  private Receipt receipt1;
  private Receipt receipt2;
  private Receipt receipt3;

  private CashbookEntry cashbookEntry1;
  private CashbookEntry cashbookEntry2;
  private CashbookEntry cashbookEntry3;

  private CashbookAccount cashbookAccount;

  private static final String COMPANY_NAME1 = "Vorwärts A";

  private static final String RECEIPT_TITLE1 = "Beleg Vorführung A";
  private static final String RECEIPT_TITLE2 = "Beleg Fest A";
  private static final String RECEIPT_TITLE3 = "Beleg Fest B";

  private static final String RECEIPT_ID1 = "A1";
  private static final String RECEIPT_ID2 = "A2";
  private static final String RECEIPT_ID3 = "A3";

  private static final String DEFAULT_NAME = "Account";
  private static final String DEFAULT_CODE = "A";

  private static final Pageable PAGEABLE = PageRequest.of(0, 20, Sort.Direction.ASC, "id");

  @Inject
  private ReceiptRepository receiptRepository;

  @Inject
  private CompanyRepository companyRepository;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Before
  public void setUp() throws Exception {
    company1 = new Company();
    company1.setName(COMPANY_NAME1);
    company1.setCity("Vienna");
    company1.setCountry("Austria");
    company1.setPostalCode("1000");
    company1.setStreetAddress("somewhere 47/3/2b");

    companyRepository.save(company1);

    this.cashbookAccount = new CashbookAccount();
    this.cashbookAccount.setName(DEFAULT_NAME);
    this.cashbookAccount.setCurrentNumber(0);
    this.cashbookAccount.setBankAccount(false);
    this.cashbookAccount.setReceiptCode(DEFAULT_CODE);
    this.cashbookAccount.setDefaultForCashbook(false);
    this.cashbookAccountRepository.save(this.cashbookAccount);

    cashbookEntry1 = new CashbookEntry()
      .title("A")
      .amount(new BigDecimal(10.0))
      .date(LocalDate.MIN)
      .type(CashbookEntryType.INCOME)
      .cashbookAccount(this.cashbookAccount);
    this.cashbookEntryRepository.save(cashbookEntry1);

    receipt1 = new Receipt();
    receipt1.setTitle(RECEIPT_TITLE1);
    receipt1.setFile("file-1");
    receipt1.setDate(LocalDate.now());
    receipt1.setIdentifier(RECEIPT_ID1);
    receipt1.setOverwriteIdentifier(true);
    receipt1.setInitialCashbookEntry(cashbookEntry1);

    cashbookEntry2 = new CashbookEntry()
      .title("B")
      .amount(new BigDecimal(10.0))
      .date(LocalDate.MIN)
      .type(CashbookEntryType.INCOME)
      .cashbookAccount(this.cashbookAccount);
    this.cashbookEntryRepository.save(cashbookEntry2);

    receipt2 = new Receipt();
    receipt2.setTitle(RECEIPT_TITLE2);
    receipt2.setFile("file-2");
    receipt2.setDate(LocalDate.now());
    receipt2.setIdentifier(RECEIPT_ID2);
    receipt2.setOverwriteIdentifier(true);
    receipt2.setInitialCashbookEntry(cashbookEntry2);

    cashbookEntry3 = new CashbookEntry()
      .title("C")
      .amount(new BigDecimal(10.0))
      .date(LocalDate.MIN)
      .type(CashbookEntryType.INCOME)
      .cashbookAccount(this.cashbookAccount);
    this.cashbookEntryRepository.save(cashbookEntry3);

    receipt3 = new Receipt();
    receipt3.setTitle(RECEIPT_TITLE3);
    receipt3.setFile("file-3");
    receipt3.setDate(LocalDate.now());
    receipt3.setCompany(company1);
    receipt3.setIdentifier(RECEIPT_ID3);
    receipt3.setOverwriteIdentifier(true);
    receipt3.setInitialCashbookEntry(cashbookEntry3);

    // receiptRepository.deleteAll();

    receipt1 = receiptRepository.save(receipt1);
    receipt2 = receiptRepository.save(receipt2);
    receipt3 = receiptRepository.save(receipt3);

    System.out.println(receiptRepository.findAll());
  }

  @After
  public void tearDown() throws Exception {
    receiptRepository.delete(receipt1);
    receiptRepository.delete(receipt2);
    receiptRepository.delete(receipt3);

    companyRepository.delete(company1);

    cashbookEntryRepository.delete(cashbookEntry1);
    cashbookEntryRepository.delete(cashbookEntry2);
    cashbookEntryRepository.delete(cashbookEntry3);

    cashbookAccountRepository.delete(this.cashbookAccount);
  }

  @Test
  public void testWithCompleteSearchWord() {
    ReceiptSpecification specification = new ReceiptSpecification();

    Page<Receipt> result = receiptRepository
        .findAll(specification.hasTitleOrCompany(RECEIPT_TITLE2).build(), PAGEABLE);

    System.out.println(result.getContent());

    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(receipt2));
  }

  @Test
  public void testWithPartialSearchWord() {
    ReceiptSpecification specification = new ReceiptSpecification();

    Page<Receipt> result = receiptRepository
        .findAll(specification.hasTitleOrCompany("fest").build(), PAGEABLE);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(receipt2, receipt3));
  }

  @Test
  public void testWithSearchWordInTitleAndCompany() {
    ReceiptSpecification specification = new ReceiptSpecification();

    Page<Receipt> result = receiptRepository.findAll(specification.hasTitleOrCompany("vor").build(),
        PAGEABLE);

    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(receipt1, receipt3));
  }

  @Test
  public void testWithNothing() {
    ReceiptSpecification specification = new ReceiptSpecification();

    Page<Receipt> result = receiptRepository.findAll(specification.build(), PAGEABLE);

    assertThat(result.getContent().size(), Matchers.equalTo(3));
  }

  @Test
  public void testWithNull() {
    ReceiptSpecification specification = new ReceiptSpecification();

    Page<Receipt> result = receiptRepository.findAll(specification.hasTitleOrCompany(null).build(),
        PAGEABLE);

    assertThat(result.getContent().size(), Matchers.equalTo(3));
  }

}
