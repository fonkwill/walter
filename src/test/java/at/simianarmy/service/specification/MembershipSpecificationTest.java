package at.simianarmy.service.specification;


import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;



import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.enumeration.MembershipFeePeriod;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.repository.MembershipFeeRepository;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.repository.PersonRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class MembershipSpecificationTest {
	private static final Pageable PAGEABLE = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
	
	
	private static final String firstName = "hans";
	private static final String lastName = "huber";
	private static final String address = "abc1";
	private static final String plz = "1010";
	private static final String city = "Wien";
	private static final String country = "Österreich";

	private final int mfp1year = 2016;
	private final int mfp1nr = 1;
	
	
	private final int mfp3year = 2016;
	private final int mfp3nr = 1;
	
	private final int mfp2year = 2017;
	private final int mfp2nr = 1;
	
	private final int mfp4year = 2017;
	private final int mfp4nr = 1;
	
	private final LocalDate m1begin = LocalDate.of(2012, 01, 01); 
	
	private final LocalDate m3begin = LocalDate.of(2012, 01,01);
	private final LocalDate m3end = LocalDate.of(2016, 12, 31);
	
	
	private MembershipFeeForPeriod mfp1;
	private MembershipFeeForPeriod mfp2;
	private MembershipFeeForPeriod mfp3;
	private MembershipFeeForPeriod mfp4;
	
	private Person p1;
	private Person p2;
	
	private Membership m1;
	private Membership m2;
	private Membership m3;
	private Membership m4;
	
	private MembershipFee membershipFee;
	
	private MembershipFeeAmount membershipFeeAmount;
	
	@Inject
	private MembershipRepository membershipRepository;
	
	@Inject
	private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;
	
	@Inject
	private MembershipFeeRepository membershipFeeRepository;
	
	@Inject
	private MembershipFeeAmountRepository membershipFeeAmountRepository;

	@Inject
	private PersonRepository personRepository;
	
	@Before
	public void setUp() {
		membershipFeeAmount = new MembershipFeeAmount();
		membershipFeeAmount.setAmount(new BigDecimal("120.50"));
		membershipFeeAmount.beginDate(m1begin);
		membershipFeeAmount = membershipFeeAmountRepository.save(membershipFeeAmount);
		
		membershipFee = new MembershipFee();
		membershipFee.setDescription("adfa");
		membershipFee.addMembershipFeeAmounts(membershipFeeAmount);
		membershipFee.setPeriod(MembershipFeePeriod.ANNUALLY);
		membershipFee = membershipFeeRepository.save(membershipFee);
			
		
		p1 = new Person();
		p1.setFirstName(firstName);
		p1.setLastName(lastName);
		p1.setStreetAddress(address);
		p1.setPostalCode(plz);
		p1.setCity(city);
		p1.setCountry(country);
    p1.setHasDirectDebit(false);
		
		p2 = new Person();
		p2.setFirstName(firstName);
		p2.setLastName(lastName);
		p2.setStreetAddress(address);
		p2.setPostalCode(plz);
		p2.setCity(city);
		p2.setCountry(country);
    p2.setHasDirectDebit(false);
		
		p1 = personRepository.save(p1);
		p2 = personRepository.save(p2);
		

		mfp1 = new MembershipFeeForPeriod();
		mfp1.setYear(mfp1year);
		mfp1.setNr(mfp1nr);

		mfp2 = new MembershipFeeForPeriod();
		mfp2.setYear(mfp2year);
		mfp2.setNr(mfp2nr);
		
		mfp3 = new MembershipFeeForPeriod();
		mfp3.setYear(mfp3year);
		mfp3.setNr(mfp3nr);
		
		mfp4 = new MembershipFeeForPeriod();
		mfp4.setYear(mfp4year);
		mfp4.setNr(mfp4nr);
		
		m1 = new Membership();
		m1.setBeginDate(m1begin);
		m1.setMembershipFee(membershipFee);
		
		m2 = new Membership();
		m2.setBeginDate(m1begin);
		m2.setMembershipFee(membershipFee);
		
		m3 = new Membership();
		m3.setBeginDate(m3begin);
		m3.setEndDate(m3end);
		m3.setMembershipFee(membershipFee);
		
		m4 = new Membership();
		m4.setBeginDate(m1begin);
		
		m1.setPerson(p1);
		m2.setPerson(p2);
		
		
		mfp1 = membershipFeeForPeriodRepository.save(mfp1);
		mfp2 = membershipFeeForPeriodRepository.save(mfp2);
		mfp3 = membershipFeeForPeriodRepository.save(mfp3);
		mfp4 = membershipFeeForPeriodRepository.save(mfp4);
		m1.addMembershipFeeForPeriods(mfp1);
		m1.addMembershipFeeForPeriods(mfp2);
		m2.addMembershipFeeForPeriods(mfp3);
		m4.addMembershipFeeForPeriods(mfp4);
		
		m1 = membershipRepository.save(m1);
		m2 = membershipRepository.save(m2);
		m3 = membershipRepository.save(m3);
		m4 = membershipRepository.save(m4);
		
		membershipFeeAmountRepository.flush();
		membershipFeeRepository.flush();
		membershipFeeForPeriodRepository.flush();
		membershipRepository.flush();

	}
	
	
	@Test
	public void testWithNoneAvailableMembershipFeeForPeriod() {
		Specification<Membership> specs = MembershipSpecification.hasNoMembershipFeeForPeriodIn(2017);
		
		Page<Membership> page = membershipRepository.findAll(specs, PAGEABLE);
		
		List<Membership> list = page.getContent();
		assertThat(list, Matchers.contains(m2));
		assertThat(list, Matchers.not(Matchers.contains(m3)));
		//m4 has no MembershipFee
		assertThat(list, Matchers.not(Matchers.contains(m4)));
	}
	
	@Test
	public void testWithAvailableMembershipFeeForPeriod() {
	Specification<Membership> specs = MembershipSpecification.hasNoMembershipFeeForPeriodIn(2016);
		
		Page<Membership> page = membershipRepository.findAll(specs, PAGEABLE);
	
		List<Membership> list = page.getContent();
		assertThat(list, Matchers.not(Matchers.contains(m1)));
		assertThat(list, Matchers.not(Matchers.contains(m2)));
		//m4 has no MembershipFee
		assertThat(list, Matchers.not(Matchers.contains(m4)));
		assertThat(list, Matchers.contains(m3));
		
		
	}
	
	
	@Test
	public void test_notAvailableMembershipFeeForPeriod_perPerson() {
		m1.removeMembershipFeeForPeriods(mfp2);
		membershipRepository.save(m1);
		membershipRepository.flush();
		Specification<Membership> specs = MembershipSpecification.hasNoMembershipFeeForPeriodIn(2017);
		Specification<Membership> specp = PersonSpecifications.hasPersonIdFromMembership(p1.getId());
		
		Specification<Membership> specm = Specification.where(specs).and(specp);
		
//		for 2017 it should the result should only be p1
		List<Membership> result = membershipRepository.findAll(specm);
		assertThat(result, Matchers.not(Matchers.contains(m2)));
		assertThat(result, Matchers.not(Matchers.contains(m3)));
		//m4 has no MembershipFee
		assertThat(result, Matchers.not(Matchers.contains(m4)));
		assertThat(result, Matchers.contains(m1));
		
		
	}
	
	@After
	public void tearDown() {
		
	}
}
