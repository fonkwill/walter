package at.simianarmy.service.specification;

import static org.junit.Assert.assertThat;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class CashbookEntrySpecificationTest {
  
  private CashbookCategory category1;
  private CashbookCategory category2;

  private CashbookEntry entry1;
  private CashbookEntry entry2;
  private CashbookEntry entry3;

  private CashbookAccount cashbookAccount;
  
  private static final String ENTRY_TITLE1 = "Vorführung";
  private static final String ENTRY_TITLE2 = "Fest A";
  private static final String ENTRY_TITLE3 = "Fest B";

  private static final String DEFAULT_NAME = "Account";
  private static final String DEFAULT_CODE = "A";
  
  private static final Pageable PAGEABLE = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
  
  @Inject
  private CashbookEntryRepository cashbookEntryRepository;
  
  @Inject
  private BankImportDataRepository bankImportDataRepository;
  
  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Before
  public void setUp() throws Exception {
    bankImportDataRepository.deleteAll();
    bankImportDataRepository.flush();
    
    cashbookEntryRepository.deleteAll();
    cashbookEntryRepository.flush();
    
    category1 = new CashbookCategory();
    category1.setName("category1");
    category1.setColor("#ff00ff");
    
    category2 = new CashbookCategory();
    category2.setName("category2");
    category2.setColor("#444444");

    this.cashbookAccount = new CashbookAccount();
    this.cashbookAccount.setName(DEFAULT_NAME);
    this.cashbookAccount.setCurrentNumber(0);
    this.cashbookAccount.setBankAccount(false);
    this.cashbookAccount.setReceiptCode(DEFAULT_CODE);
    this.cashbookAccount.setDefaultForCashbook(false);
    this.cashbookAccountRepository.save(this.cashbookAccount);
    
    entry1 = new CashbookEntry();
    entry1.setTitle(ENTRY_TITLE1);
    entry1.setDate(LocalDate.of(2016, 10, 20));
    entry1.setAmount(BigDecimal.TEN);
    entry1.setType(CashbookEntryType.INCOME);
    entry1.setCashbookCategory(category1);
    entry1.setCashbookAccount(this.cashbookAccount);
    
    entry2 = new CashbookEntry();
    entry2.setTitle(ENTRY_TITLE2);
    entry2.setDate(LocalDate.of(2016, 11, 20));
    entry2.setAmount(BigDecimal.ONE);
    entry2.setType(CashbookEntryType.SPENDING);
    entry2.setCashbookCategory(category2);
    entry2.setCashbookAccount(this.cashbookAccount);
    
    entry3 = new CashbookEntry();
    entry3.setTitle(ENTRY_TITLE3);
    entry3.setDate(LocalDate.of(2016, 12, 20));
    entry3.setAmount(BigDecimal.ZERO);
    entry3.setType(CashbookEntryType.SPENDING);
    entry3.setCashbookAccount(this.cashbookAccount);
    
    cashbookEntryRepository.deleteAll();
    
    category1 = cashbookCategoryRepository.save(category1);
    category2 = cashbookCategoryRepository.save(category2);
    
    entry1 = cashbookEntryRepository.save(entry1);
    entry2 = cashbookEntryRepository.save(entry2);
    entry3 = cashbookEntryRepository.save(entry3);
  }
  
  @After
  public void tearDown() throws Exception {    
    cashbookEntryRepository.delete(entry1);
    cashbookEntryRepository.delete(entry2);
    cashbookEntryRepository.delete(entry3);

    cashbookCategoryRepository.delete(category1);
    cashbookCategoryRepository.delete(category2);

    cashbookAccountRepository.delete(cashbookAccount);
  }
  
  @Test
  public void testWithCompleteTitle() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasTitle(ENTRY_TITLE2)
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(entry2));
  }
  
  @Test
  public void testWithPartialTitle() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasTitle("fest")
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(entry2, entry3));
  }
  
  @Test
  public void testWithCategory() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasCategory(category1.getId())
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(entry1));
  }
  
  @Test
  public void testWithType() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasType(CashbookEntryType.SPENDING)
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(entry2, entry3));
  }
  
  @Test
  public void testWithLowerBoundDate() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasDate(LocalDate.of(2016, 10, 31), null)
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(entry2, entry3));
  }
  
  @Test
  public void testWithUpperBoundDate() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasDate(null, LocalDate.of(2016, 11, 30))
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(2));
    assertThat(result.getContent(), Matchers.contains(entry1, entry2));
  }
  
  @Test
  public void testWithLowerBoundAndUpperBoundDate() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasDate(LocalDate.of(2016, 10, 1), LocalDate.of(2016, 10, 31))
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(1));
    assertThat(result.getContent(), Matchers.contains(entry1));
  }
  
  @Test
  public void testWithNothing() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification.build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(3));
  }
  
  @Test
  public void testWithNull() {
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .hasTitle(null)
        .hasCategory(null)
        .hasType(null)
        .hasDate(null, null)
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(3));
  }
  
  public void testWithCategoryIdList() {
    List<Long> categoryIds = new ArrayList<Long>();
    categoryIds.add(category1.getId());
    
    CashbookEntrySpecification specification = new CashbookEntrySpecification();
    
    Page<CashbookEntry> result = cashbookEntryRepository.findAll(specification
        .belongsToCategory(categoryIds)
        .build(), PAGEABLE);
    
    assertThat(result.getContent().size(), Matchers.equalTo(1));
  }
  
}
