package at.simianarmy.service.specification;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


import at.simianarmy.WalterApp;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.PersonGroupRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonSpecificationTest {
	


	private static final String firstName = "hans";
	
	@Inject
	private CompanyRepository companyRepository;
	
	@Inject
	private PersonGroupRepository personGroupRepository;
	
	private Company c1;
	private Company c2;
	
	
	private PersonGroup pg1;
	private PersonGroup pg2;
	
	
	@Before
	public void setUp() throws Exception {
	    c1 = new Company();
	    c1.setName(firstName);

	    c1 = companyRepository.save(c1);
	    
	    c2 = new Company();
	    c2.setName(firstName);
	    c2 = companyRepository.save(c2);
	    
	    
	
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_isInPersongroups_oneGroup() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg1.addCompanies(c1);
		pg1.addCompanies(c2);
		
		pg1 = personGroupRepository.save(pg1);
		
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		
		Specification<Company> specs = CompanySpecifications.isInPersonGroup(pgs);
		
		List<Company> persons = companyRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(c1, c2));
		
	}
	
	@Test
	public void test_isInPersongroups_twoGroup_hierarchy() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg2 = new PersonGroup();
		pg2.setName("gr2");
		pg1.addChildren(pg2);
		pg2.setParent(pg1);
		
		pg1.addCompanies(c1);
		pg2.addCompanies(c2);
		
		pg1 = personGroupRepository.save(pg1);
		pg2 = personGroupRepository.save(pg2);
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		pgs.add(pg2.getId());
		
		Specification<Company> specs = CompanySpecifications.isInPersonGroup(pgs);
		
		List<Company> persons = companyRepository.findAll(specs);
		assertThat(persons, Matchers.containsInAnyOrder(c1, c2));
		
	}
	

	
	@Test
	public void test_isInPersongroups_twoGroup_hierarchy_multiple() {
		pg1 = new PersonGroup();
		pg1.setName("gr1");
		pg2 = new PersonGroup();
		pg2.setName("gr2");
		pg1.addChildren(pg2);
		pg2.setParent(pg1);
		
		pg1.addCompanies(c1);
		pg2.addCompanies(c2);
//		pg2.addCompanies(c3);
		
		pg1 = personGroupRepository.save(pg1);
		pg2 = personGroupRepository.save(pg2);
		
		List<Long> pgs = new ArrayList<>();
		
		pgs.add(pg1.getId());
		pgs.add(pg2.getId());
		
		Specification<Company> specs = CompanySpecifications.isInPersonGroup(pgs);
		
		List<Company> persons = companyRepository.findAll(specs);
		assertThat(persons, Matchers.iterableWithSize(2));
		assertThat(persons, Matchers.containsInAnyOrder(c1, c2));
		
	}
	

	
}
