package at.simianarmy.service;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationRepository;
import at.simianarmy.multitenancy.service.TenantConfigurationService;
import at.simianarmy.multitenancy.service.dto.TenantConfigurationDTO;
import at.simianarmy.multitenancy.service.mapper.TenantConfigurationMapper;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.util.BeanUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class TenantConfigurationServiceTest {
	
	@Mock
	private TenantConfigurationRepository repo;
	
	@Spy
	private TenantConfigurationMapper mapper;
	

	@InjectMocks
	private TenantConfigurationService sut;
	


	@Before
	public void setUp() throws Exception {
		mapper = BeanUtil.getBean(TenantConfigurationMapper.class);
		
		MockitoAnnotations.initMocks(this);

		Map<String, TenantConfiguration> tenantConfiguration = new HashMap<>();

		DBConfiguration dbConfig = new DBConfiguration();
		dbConfig.setUrl("www.url.net");
		TenantConfiguration tenant1 = new TenantConfiguration();
		tenant1.setDbConfiguration(dbConfig);
		tenant1.setSchema("tenant1S");
		tenant1.setTenantId("tenant1");
		tenantConfiguration.put("tenant1", tenant1);

		TenantConfiguration tenant2 = new TenantConfiguration();
		tenant2.setDbConfiguration(dbConfig);
		tenant2.setSchema("tenant2S");
		tenant2.setTenantId("tenant2");
		tenantConfiguration.put("tenant2", tenant2);

		Mockito.when(repo.getAllTenantConfigurations()).thenReturn(tenantConfiguration);
		Mockito.when(repo.save(ArgumentMatchers.any())).thenAnswer(i -> i.getArgument(0));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSave_Valid() {
		DBConfiguration dbConfig = new DBConfiguration();
		dbConfig.setUrl("www.url.net");
		TenantConfiguration tenant3 = new TenantConfiguration();
		tenant3.setDbConfiguration(dbConfig);
		tenant3.setSchema("tenant3S");
		tenant3.setTenantId("tenant3");
		TenantConfigurationDTO tenant3s = sut.save(mapper.tenantConfigurationToTenantConfigurationDTO(tenant3));
		assertThat(tenant3s.getTenantId(), equalTo("tenant3"));
	}

	@Test(expected = ServiceValidationException.class)
	public void testSave_NotValid() {
		DBConfiguration dbConfig = new DBConfiguration();
		dbConfig.setUrl("www.url.net");
		TenantConfiguration tenant3 = new TenantConfiguration();
		tenant3.setDbConfiguration(dbConfig);
		tenant3.setSchema("tenant1S");
		tenant3.setTenantId("tenant3");
		
		sut.save(mapper.tenantConfigurationToTenantConfigurationDTO(tenant3));

	}

}
