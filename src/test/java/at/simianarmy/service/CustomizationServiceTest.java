package at.simianarmy.service;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.Company;
import at.simianarmy.domain.Customization;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.export.akm.xml.Allgemeines;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.repository.CustomizationRepository;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.roles.domain.AuthConfig;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.service.dto.CustomizationDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CustomizationMapper;
import at.simianarmy.service.util.BeanUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CustomizationServiceTest {

  private CustomizationRepository customizationRepository;
  private PersonGroupRepository personGroupRepository;
  private CompanyRepository companyRepository;
  private CashbookAccountRepository cashbookAccountRepository;
  private CashbookCategoryRepository cashbookCategoryRepository;

  private CustomizationService sut;


  @Before
  public void setUp() {
    customizationRepository = Mockito.mock(CustomizationRepository.class);
    personGroupRepository = Mockito.mock(PersonGroupRepository.class);
    companyRepository = Mockito.mock(CompanyRepository.class);
    cashbookAccountRepository = Mockito.mock(CashbookAccountRepository.class);
    cashbookCategoryRepository = Mockito.mock(CashbookCategoryRepository.class);

    CustomizationMapper customizationMapper = BeanUtil.getBean(CustomizationMapper.class);
    AuthConfigRepository authConfigRepository = Mockito.mock(AuthConfigRepository.class);

    sut = new CustomizationService(customizationRepository, customizationMapper, personGroupRepository, authConfigRepository, companyRepository, cashbookAccountRepository, cashbookCategoryRepository);

    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getMemberPersonGroup_ReturnMemberPersonGroup() {
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.MEMBER_PERSONGROUP);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));
    PersonGroup pg3 = new PersonGroup();
    pg3.setId(3L);
    Mockito.when(personGroupRepository.findById(3L)).thenReturn(Optional.of(pg3));

    Long i = sut.getMemberPersonGroup();

    assertThat(i, equalTo(3L));

  }

  @Test
  public void getOrganizationId_ReturnCompanyId() {
    Customization customization = new Customization();
    customization.setData("5");
    customization.setName(CustomizationName.ORGANIZATION_COMPANY);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.ORGANIZATION_COMPANY);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));
    Company c = new Company();
    c.setId(5L);
    Mockito.when(companyRepository.findById(5L)).thenReturn(Optional.of(c));

    Long i = sut.getOrganizationId();

    assertThat(i, equalTo(5L));

  }

  @Test
  public void getMonthtsToDeletePersonData_ReturnMonthsToDelete() {
    Customization customization = new Customization();
    customization.setData("24");
    customization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));


    Long i = sut.getMonthsToDeletePersonData();

    assertThat(i, equalTo(24L));

  }


  @Test
  public void getHoursToInvalidateVerificationLink_ReturnHoursToInvalidate() {

    Customization customization = new Customization();
    customization.setData("24");
    customization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));

    Long i = sut.getHoursToInvalidateVerificationLink();

    assertThat(i, equalTo(24L));
  }

  @Test
  public void getDeactivatedFeatureGroups_returnDeactivatedFeatureGroups() {
    List<String> deactivatedFeatureGroups = new ArrayList<>();
    deactivatedFeatureGroups.add("FEATURE1");
    deactivatedFeatureGroups.add("FEATURE2");
    deactivatedFeatureGroups.add("FEATURE3");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, deactivatedFeatureGroups);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));

    List<String> list = sut.getDeactivatedFeatureGroups();

    assertThat(list, hasItems( equalTo("FEATURE1"), equalTo("FEATURE2"), equalTo("FEATURE3")));


  }
  @Test
  public void getAKMData_ReturnAKMData() {

    Allgemeines akm = new Allgemeines();
    akm.setAusstellerName("Aussteller1");
    akm.setVerName("Veranstalter1");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, akm);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.AKM_DATA);

    Customization exampleCustomization = new Customization();
    exampleCustomization.setName(CustomizationName.AKM_DATA);

    Mockito.when(customizationRepository.findOne(Example.of(exampleCustomization))).thenReturn(Optional.of(customization));

    Allgemeines akmR = sut.getAKMData();

    assertThat(akmR.getVerName(), equalTo("Veranstalter1"));
    assertThat(akmR.getAusstellerName(), equalTo("Aussteller1"));

  }

  @Test
  public void validationPersonGroup_IsValid() {
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationPersonGroup_IsInvalid() {
    Customization customization = new Customization();
    customization.setData("-5");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationPersonGroup_IsInvalidWrongType() {
    Customization customization = new Customization();
    customization.setData("Hansi");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }
  @Test
  public void validationOrganizationId_IsValid() {
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.ORGANIZATION_COMPANY);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationOrganizationId_IsInvalid() {
    Customization customization = new Customization();
    customization.setData("-5");
    customization.setName(CustomizationName.ORGANIZATION_COMPANY);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationOrganizationId_IsInvalidWrongType() {
    Customization customization = new Customization();
    customization.setData("Hansi");
    customization.setName(CustomizationName.ORGANIZATION_COMPANY);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationMonthsToDelete_IsValid() {
    Customization customization = new Customization();
    customization.setData("25");
    customization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationMonthsToDelete_IsInvalid() {
    Customization customization = new Customization();
    customization.setData("-5");
    customization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationMonthsToDelete_IsInvalidWrongType() {
    Customization customization = new Customization();
    customization.setData("Hansi");
    customization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationHours_IsValid() {
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationHours_IsInvalid() {
    Customization customization = new Customization();
    customization.setData("-5");
    customization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationHours_IsInvalidWrongType() {
    Customization customization = new Customization();
    customization.setData("Hansi");
    customization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationDeactivatedFeatureGroups_IsValid() {
    List<String> deactivatedFeatureGroups = new ArrayList<>();
    deactivatedFeatureGroups.add("FEATURE1");
    deactivatedFeatureGroups.add("FEATURE2");
    deactivatedFeatureGroups.add("FEATURE3");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, deactivatedFeatureGroups);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    try {
      sut.validate(customization);
    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }
  @Test
  public void validationDeactivatedFeatureGroups_IsInValid() {
    List<String> deactivatedFeatureGroups = new ArrayList<>();
    deactivatedFeatureGroups.add("FEATURE1");
    deactivatedFeatureGroups.add("ADMIN");
    deactivatedFeatureGroups.add("FEATURE3");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, deactivatedFeatureGroups);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {
    }
  }
  @Test
  public void validationDeactivatedFeatureGroups_IsInValidWrongType() {
   Allgemeines akm = new Allgemeines();
   akm.setVerName("Veranstalter1");
   akm.setAusstellerName("Ausssteller");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, akm);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationAKM_IsValid() {
    Allgemeines akm = new Allgemeines();
    akm.setVerName("Veranstalter1");
    akm.setAusstellerName("Ausssteller");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, akm);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.AKM_DATA);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationAKM_IsInValidWrongType() {
    List<String> deactivatedFeatureGroups = new ArrayList<>();
    deactivatedFeatureGroups.add("FEATURE1");
    deactivatedFeatureGroups.add("FEATURE2");
    deactivatedFeatureGroups.add("FEATURE3");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, deactivatedFeatureGroups);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.AKM_DATA);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void setTextHours_SetText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), equalTo("3"));

  }

  @Test
  public void setTextMonthsToDelete_SetText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.MONTHS_TO_DELETE_PERSON_DATA);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), equalTo("3"));

  }

  @Test
  public void validationDefaultCashbookAccount_IsValid() {
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);

    try {
      sut.validate(customization);

    }
    catch(ServiceValidationException ex) {
      fail();
    }
  }

  @Test
  public void validationDefaultCashbookAccount_IsInvalid() {
    Customization customization = new Customization();
    customization.setData("-5");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void validationDefaultCashbookAccount_IsInvalidWrongType() {
    Customization customization = new Customization();
    customization.setData("Hansi");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);

    try {
      sut.validate(customization);
      fail();
    }
    catch(ServiceValidationException ex) {

    }
  }

  @Test
  public void setTextPersonGroup_SetText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    PersonGroup pg = new PersonGroup();
    pg.setName("Mitglied");
    Mockito.when(personGroupRepository.findById(3L)).thenReturn(Optional.of(pg));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), equalTo("Mitglied"));

  }

  @Test
  public void getOne_ReturnCustomizationWithText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.MEMBER_PERSONGROUP);


    Customization example = new Customization();
    example.setName(CustomizationName.MEMBER_PERSONGROUP);

    Mockito.when(customizationRepository.findOne(Example.of(example))).thenReturn(Optional.of(customization));

    PersonGroup pg = new PersonGroup();
    pg.setName("Mitglied");
    Mockito.when(personGroupRepository.findById(3L)).thenReturn(Optional.of(pg));

    Customization cust = sut.getOne(CustomizationName.MEMBER_PERSONGROUP);
    assertThat( cust.getText(), equalTo("Mitglied"));

  }


  @Test
  public void setTextOrganizationCompany_SetText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.ORGANIZATION_COMPANY);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    Company company = new Company();
    company.setName("Verein");
    Mockito.when(companyRepository.findById(3L)).thenReturn(Optional.of(company));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), equalTo("Verein"));

  }

  @Test
  public void setTextFeatureGroups_SetText(){

    List<String> deactivatedFeatureGroups = new ArrayList<>();
    deactivatedFeatureGroups.add("FEATURE1");
    deactivatedFeatureGroups.add("FEATURE2");
    deactivatedFeatureGroups.add("FEATURE3");

    ObjectMapper mapper = new ObjectMapper();
    StringWriter writer = new StringWriter();
    try {
      mapper.writeValue(writer, deactivatedFeatureGroups);
    } catch (IOException e) {
      e.printStackTrace();
      fail();
    }
    String data = writer.toString();

    Customization customization = new Customization();
    customization.setData(data);
    customization.setName(CustomizationName.DEACTIVATED_FEATUREGROUPS);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), allOf( containsString("FEATURE1"), containsString("FEATURE2"), containsString("FEATURE3")));

  }

  @Test
  public void setTextDefaultCashbookAccount_SetText(){
    Customization customization = new Customization();
    customization.setData("3");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);

    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.of(customization));

    CashbookAccount cashbookAccount = new CashbookAccount();
    cashbookAccount.setName("Kassabuch");
    Mockito.when(cashbookAccountRepository.findById(3L)).thenReturn(Optional.of(cashbookAccount));

    CustomizationDTO cust = sut.findOne(1L);
    assertThat( cust.getText(), equalTo("Kassabuch"));

  }

  @Test(expected = ServiceValidationException.class)
  public void testGetDefaultCashbookAccount_throwsServiceValidationException() {
    this.sut.getDefaultCashbookAccount();
  }

  @Test
  public void testGetDefaultCashbookEntry() {

    CashbookAccount wanted = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccountRepository.findById(1L)).thenReturn(Optional.ofNullable(wanted));

    Customization customization = new Customization();
    customization.setData("1");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);
    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.ofNullable(customization));

    Customization example = new Customization();
    example.setName(CustomizationName.DEFAULT_CASHBOOK_ACCOUNT);

    Mockito.when(customizationRepository.findOne(Example.of(example))).thenReturn(Optional.of(customization));

    CashbookAccount cashbookAccount = this.sut.getDefaultCashbookAccount();
    assertThat(cashbookAccount, equalTo(wanted));
  }

  @Test
  public void testGetDefaultCashbookCategoryForTransfer() {

    CashbookCategory wanted = Mockito.mock(CashbookCategory.class);
    Mockito.when(this.cashbookCategoryRepository.findById(1L)).thenReturn(Optional.ofNullable(wanted));

    Customization customization = new Customization();
    customization.setData("1");
    customization.setName(CustomizationName.DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER);
    Mockito.when(customizationRepository.findById(1L)).thenReturn(Optional.ofNullable(customization));

    Customization example = new Customization();
    example.setName(CustomizationName.DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER);

    Mockito.when(customizationRepository.findOne(Example.of(example))).thenReturn(Optional.of(customization));

    CashbookCategory cashbookCategory = this.sut.getDefaultCashbookCategoryForTransfers();
    assertThat(cashbookCategory, equalTo(wanted));
  }
}
