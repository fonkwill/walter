package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ClothingTypeServiceTest {

	private static final Long id1 = new Long(1);
	
	private static final Long clothingId = new Long(10);
	
	private static final String name1 = "name1";
	
	private static final Clothing clothing = new Clothing();
	
	private ClothingType clothingType1;
	
	@Mock
	private ClothingTypeRepository ClothingTypeRepository;
	
	@InjectMocks
	private ClothingTypeService ClothingTypeService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		clothingType1 = new ClothingType();
		clothingType1.setId(id1);
		clothingType1.setName(name1);
		
		clothing.setId(clothingId);;
		
		clothingType1.addClothings(clothing); //must not be deleted
		
		when(ClothingTypeRepository.findById(id1)).thenReturn(Optional.ofNullable(clothingType1));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithPersonClothingType() {
	  ClothingTypeService.delete(clothingType1.getId());
	}
}
