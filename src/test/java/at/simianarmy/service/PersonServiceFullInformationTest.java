package at.simianarmy.service;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.*;
import at.simianarmy.domain.enumeration.CustomizationName;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.service.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.xnio.IoUtils;

import javax.inject.Inject;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonServiceFullInformationTest {


  @Inject
  private PersonService sut;


  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void fullInformation_ShouldReturnAllInformation() {

    LocalDateTime before = LocalDateTime.now();


    FullPersonDTO p = sut.findOneWithFullInformation(1L);
    LocalDateTime after = LocalDateTime.now();

    verifyPersonInfo(p);


    assertThat(p.getCreatedAt(), allOf( greaterThanOrEqualTo(before), lessThanOrEqualTo(after) )  );
    assertThat(p.getOrganizationName(), equalTo("Verein"));


  }


  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void fullInformation_ShouldReturnAllInformationAsJson() {
    Resource res = sut.findOneWithFullInformationAsJson(1L);

    if (res instanceof InputStreamResource) {
      try {
        InputStream inputStream = res.getInputStream();

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        FullPersonDTO result = mapper.readValue(inputStream, FullPersonDTO.class);

        verifyPersonInfo(result);

      } catch (IOException e) {
        e.printStackTrace();
        fail();
      }
    } else {
      fail();
    }
  }

  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void fullInformation_ShouldReturnAllInformationAsJsonLeaveEmptyOut() {
    Resource res = sut.findOneWithFullInformationAsJson(3L);

    if (res instanceof InputStreamResource) {
      try {
        InputStream inputStream = res.getInputStream();

        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        String result = writer.toString();

        assertThat(result, containsString("firstName"));
        assertThat(result, not(containsString("appointments")));

      } catch (IOException e) {
        e.printStackTrace();
        fail();
      }
    } else {
      fail();
    }
  }

  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void fullInformation_ShouldReturnAllInformationAsPDF() {
    Resource res = sut.findOneWithFullInformationAsPDF(1L);

    if (res instanceof InputStreamResource) {
      try {

        PDDocument  pdfDocument = PDDocument.load(res.getInputStream());

        String text = new PDFTextStripper().getText(pdfDocument);
        assertThat(text, containsString("Hansi"));
        assertThat(text, containsString("Huber"));
        assertThat(text, containsString("2256"));
        assertThat(text, containsString("Untere Hauptstrasse 22"));
        assertThat(text, containsString("hansili.huber@gmx.at"));
        assertThat(text, containsString("Austria"));
        assertThat(text, containsString("Vorstand"));
        assertThat(text, containsString("Trompete"));
        assertThat(text, containsString("Saxophon"));
        assertThat(text, containsString("52"));
        assertThat(text, containsString("L"));
        assertThat(text, containsString("Jacke"));
        assertThat(text, containsString("Hose"));
        assertThat(text, containsString("Goldenes Abzeichen"));
        assertThat(text, containsString("Silbernes Abzeichen"));
        assertThat(text, containsString("10 Jahre Mtiglied"));
        assertThat(text, containsString("5 Jahre Vorstand"));
        assertThat(text, containsString("Probe"));
        assertThat(text, containsString("Wirthaus Hansi"));
        assertThat(text, containsString("Untere Oberstrasse"));
        assertThat(text, containsString("Vollzahler"));
        assertThat(text, containsString("2018"));
        assertThat(text, containsString("2019"));
        assertThat(text, containsString("Sitzung"));
        assertThat(text, containsString("Verein"));



      } catch (IOException e) {
        e.printStackTrace();
        fail();
      }
    } else {
      fail();
    }
  }

  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void fullInformation_ShouldReturnAllInformationAsJsonRemovedOtherPersons() {
    Resource res = sut.findOneWithFullInformationAsJson(1L);

    if (res instanceof InputStreamResource) {
      try {
        InputStream inputStream = res.getInputStream();

        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        String result = writer.toString();

        assertThat(result, containsString("firstName"));
        assertThat(result, not(containsString("Brigitte")));

      } catch (IOException e) {
        e.printStackTrace();
        fail();
      }
    } else {
      fail();
    }
  }


  private void verifyPersonInfo(FullPersonDTO p) {
    Person exp = new Person();
    exp.setFirstName("Hansi");
    exp.setLastName("Huber");
    exp.setBirthDate(LocalDate.of(1989, 02, 06));
    exp.setPostalCode("2256");
    exp.setStreetAddress("Untere Hauptstrasse 22");
    exp.setEmail("hansili.huber@gmx.at");
    exp.setCity("Oberndorf");
    exp.setCountry("Austria");
    exp.setEmailType(EmailType.VERIFIED);
    exp.setGender(Gender.MALE);
    exp.setHasDirectDebit(false);

    PersonGroup expPg1 = new PersonGroup();
    expPg1.setId(1L);
    expPg1.setName("Vorstand");

    PersonGroup expPg2= new PersonGroup();
    expPg1.setId(2L);
    expPg1.setName("Musiker");

    exp.addPersonGroups(expPg1);
    exp.addPersonGroups(expPg2);


    assertEquals(exp.getFirstName(), p.getFirstName());
    assertEquals(exp.getLastName(), p.getLastName());
    assertEquals(exp.getBirthDate(), p.getBirthDate());
    assertEquals(exp.getEmail(), p.getEmail());
    assertEquals(exp.getStreetAddress(), p.getStreetAddress());
    assertEquals(exp.getPostalCode(), p.getPostalCode());
    assertEquals(exp.getCity(), p.getCity());
    assertEquals(exp.getCountry(), p.getCountry());
    assertEquals(exp.getGender(), p.getGender());
    assertEquals(exp.getEmailType(), p.getEmailType());
    assertEquals(exp.getHasDirectDebit(), p.getHasDirectDebit());

    //persongroups
    Set<PersonGroupDTO> pg = p.getPersonGroups();
    assertThat(pg, hasItems(
      hasProperty("id", equalTo(expPg1.getId())),
      hasProperty("id", equalTo(expPg1.getId())),
      hasProperty("name", equalTo(expPg1.getName())),
      hasProperty("name", equalTo(expPg1.getName()))
      )
    );

    //instruments
    Set<PersonInstrumentDTO> pin = p.getPersonInstruments();
    assertThat(pin, hasSize(2));
    assertThat(pin, hasItems(
      hasProperty("beginDate", equalTo(LocalDate.of(2018, 1, 1))),
      hasProperty("beginDate", equalTo(LocalDate.of(2018, 1, 1))),
      hasProperty("endDate", equalTo(LocalDate.of(2018, 10, 31))),
      hasProperty("instrumentName", equalTo("Trompete")),
      hasProperty("instrumentName", equalTo("Saxophon"))

    ));

    //clothings
    Set<PersonClothingDTO> pcl = p.getPersonClothings();
    assertThat(pcl, hasSize(2));
    assertThat(pcl, hasItems(
      hasProperty("beginDate", equalTo(LocalDate.of(2019, 1, 1))),
      hasProperty("beginDate", equalTo(LocalDate.of(2018, 11, 12))),
      hasProperty("clothingSize", equalTo("L")),
      hasProperty("clothingSize", equalTo("52")),
      hasProperty("clothingTypeName", equalTo("Jacke")),
      hasProperty("clothingTypeName", equalTo("Hose"))
    ));


    //awards
    Set<PersonAwardDTO> paw = p.getPersonAwards();
    assertThat(paw, hasSize(2));
    assertThat(paw, hasItems(
      hasProperty("date", equalTo(LocalDate.of(2018, 9, 4))),
      hasProperty("date", equalTo(LocalDate.of(2018, 4, 1))),
      hasProperty("awardName", equalTo("Goldenes Abzeichen")),
      hasProperty("awardName", equalTo("Silbernes Abzeichen"))
    ));


    //honor
    Set<PersonHonorDTO> pho = p.getPersonHonors();
    assertThat(pho, hasSize(2));
    assertThat(pho, hasItems(
      hasProperty("date", equalTo(LocalDate.of(2018, 11, 1))),
      hasProperty("date", equalTo(LocalDate.of(2019, 02, 11))),
      hasProperty( "honorName", equalTo("10 Jahre Mtiglied")),
      hasProperty("honorName", equalTo("5 Jahre Vorstand"))
    ));


    //appointment
    Set<AppointmentDTO> ap = p.getAppointments();
    assertThat(ap, hasSize(2));
    assertThat(ap, hasItems(
      hasProperty("name", equalTo("Probe")),
      hasProperty("name", equalTo("Vorstands-Sitzung")),
      hasProperty("streetAddress", equalTo("Wirthaus Hansi")),
      hasProperty("streetAddress", equalTo("Untere Oberstrasse")),
      hasProperty("typeName", equalTo("Probe")),
      hasProperty("typeName", equalTo("Sitzung"))
    ));


    //membership
    Set<MembershipDTO> mem = p.getMemberships();
    assertThat(mem, hasSize(1));
    assertThat(mem, hasItems(
      hasProperty("beginDate", equalTo(LocalDate.of(2018,01,01))),
      hasProperty("membershipFeeDescription", equalTo("Vollzahler"))
    ));

    Set<MembershipFeeForPeriodDTO> memFeePeriod = p.getMembershipFeeForPeriods();
    assertThat(memFeePeriod, hasSize(2));
    assertThat(memFeePeriod, hasItems(
      //hasProperty("paid", equalTo(true)),
      hasProperty("year", equalTo(2019)),
      hasProperty("year", equalTo(2018))
    ));


    //cashbookEntry
    Set<CashbookEntryDTO> cbe = p.getCashbookEntries();
    assertThat(cbe, hasSize(1));
    assertThat(cbe, hasItems(
      hasProperty("title", equalTo("00001012019"))
    ));


    //bankImportData
    Set<BankImportDataDTO> bid =  p.getBankImportData();
    assertThat(bid, hasSize(1));
    assertThat(bid, hasItems(
      hasProperty("partnerName", equalTo("Huber Hansi"))
    ));
  }



}
