package at.simianarmy.service;


import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColumnConfig;
import at.simianarmy.domain.User;
import at.simianarmy.domain.UserColumnConfig;
import at.simianarmy.repository.ColumnConfigRepository;
import at.simianarmy.repository.UserColumnConfigRepository;
import at.simianarmy.repository.UserRepository;
import at.simianarmy.service.util.RandomUtil;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserService
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class UserServiceIntTest {

  @MockBean
  UserColumnConfigService userColumnConfigService;

  @Inject
  private UserRepository userRepository;

  @Inject
  private UserService userService;

  @Test
  public void assertThatUserMustExistToResetPassword() {
    Optional<User> maybeUser = userService.requestPasswordReset("john.doe@localhost");
    assertFalse(maybeUser.isPresent());

    maybeUser = userService.requestPasswordReset("admin@localhost");
    assertTrue(maybeUser.isPresent());

    assertThat(maybeUser.get().getEmail(), equalTo("admin@localhost"));
    assertNotNull(maybeUser.get().getResetDate());
    assertNotNull(maybeUser.get().getResetKey());
  }

  @Test
  public void assertThatOnlyActivatedUserCanRequestPasswordReset() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
        "en-US");
    Optional<User> maybeUser = userService.requestPasswordReset("john.doe@localhost");
    assertFalse(maybeUser.isPresent());
    userRepository.delete(user);
  }

  @Test
  public void assertThatResetKeyMustNotBeOlderThan24Hours() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
        "en-US");

    ZonedDateTime daysAgo = ZonedDateTime.now().minusHours(25);
     
    String resetKey = RandomUtil.generateResetKey();
    user.setActivated(true);
    user.setResetDate(daysAgo.toInstant());
    user.setResetKey(resetKey);

    userRepository.save(user);

    Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());

    assertFalse(maybeUser.isPresent());

    userRepository.delete(user);
  }

  @Test
  public void assertThatResetKeyMustBeValid() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
        "en-US");

    ZonedDateTime daysAgo = ZonedDateTime.now().minusHours(25);
    user.setActivated(true);
    user.setResetDate(daysAgo.toInstant());
    user.setResetKey("1234");
    userRepository.save(user);
    Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());
    assertFalse(maybeUser.isPresent());
    userRepository.delete(user);
  }

  @Test
  public void assertThatUserCanResetPassword() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
        "en-US");
    String oldPassword = user.getPassword();
    ZonedDateTime daysAgo = ZonedDateTime.now().minusHours(2);
    String resetKey = RandomUtil.generateResetKey();
    user.setActivated(true);
    user.setResetDate(daysAgo.toInstant());
    user.setResetKey(resetKey);
    userRepository.save(user);
    Optional<User> maybeUser = userService.completePasswordReset("johndoe2", user.getResetKey());
    assertTrue(maybeUser.isPresent());
    assertNull(maybeUser.orElse(null).getResetDate());
    assertNull(maybeUser.orElse(null).getResetKey());
    assertThat(maybeUser.orElse(null).getPassword(), not(equalTo(oldPassword)));

    userRepository.delete(user);
  }

  @Test
  public void testCreateUserColumnConfigs() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
      "en-US");

    Mockito.verify(this.userColumnConfigService, Mockito.times(1)).generateColumnConfigsForUser(user);

    userRepository.delete(user);
  }

  @Test
  public void testDeleteUserColumnConfigs() {
    User user = userService.createUser("johndoe", "johndoe", "John", "Doe", "john.doe@localhost",
      "en-US");

    userService.deleteUser("johndoe");

    Mockito.verify(this.userColumnConfigService, Mockito.times(1)).deleteColumnConfigsForUser(user);
  }
}
