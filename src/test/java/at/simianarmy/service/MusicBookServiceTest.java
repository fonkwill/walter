package at.simianarmy.service;

import at.simianarmy.domain.Composition;
import at.simianarmy.domain.MusicBook;
import at.simianarmy.repository.MusicBookRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MusicBookServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private MusicBookRepository musicBookRepository;

  @InjectMocks
  private MusicBookService musicBookService;

  private MusicBook musicBook;

  @Before
  public void setUp() {
    this.musicBook = Mockito.mock(MusicBook.class);
    Mockito.when(this.musicBookRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.musicBook));
  }

  @Test
  public void testDelete() {
    this.musicBookService.delete(DEFAULT_ID);
    Mockito.verify(this.musicBookRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShoulThrowException() {
    Mockito.when(this.musicBook.getCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.musicBookService.delete(DEFAULT_ID);
  }

}