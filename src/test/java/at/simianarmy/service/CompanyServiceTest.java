package at.simianarmy.service;

import at.simianarmy.domain.Company;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.Receipt;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.service.exception.ServiceValidationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyServiceTest {

  private static final Long DEFAULT_ID = new Long(1);

  @Mock
  private CompanyRepository companyRepository;

  @InjectMocks
  private CompanyService companyService;

  private Company company;

  @Before
  public void setUp() {
    this.company = Mockito.mock(Company.class);
    Mockito.when(this.companyRepository.findById(DEFAULT_ID)).thenReturn(Optional.ofNullable(this.company));
  }

  @Test
  public void testDelete() {
    this.companyService.delete(DEFAULT_ID);
    Mockito.verify(this.companyRepository, Mockito.times(1)).deleteById(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShouldThrowException() {
    Mockito.when(this.company.getPublishedCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.companyService.delete(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShouldThrowException2() {
    Mockito.when(this.company.getOrderedCompositions())
        .thenReturn(new HashSet<Composition>(Arrays.asList(Mockito.mock(Composition.class))));
    this.companyService.delete(DEFAULT_ID);
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteShouldThrowException3() {
    Mockito.when(this.company.getReceipts())
        .thenReturn(new HashSet<Receipt>(Arrays.asList(Mockito.mock(Receipt.class))));
    this.companyService.delete(DEFAULT_ID);
  }

}