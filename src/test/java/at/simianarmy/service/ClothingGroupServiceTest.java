package at.simianarmy.service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonClothing;
import at.simianarmy.repository.ClothingRepository;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.repository.PersonClothingRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.ClothingGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.specification.ClothingSpecification;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class ClothingGroupServiceTest {

//	PersonData
	private static final String firstName = "hans";
	private static final String lastName = "huber";
	private static final String address = "abc1";
	private static final String plz = "1010";
	private static final String city = "Wien";
	private static final String country = "Österreich";
	
//	ClothingData
	private static final String size1 = "52";
	private static final String size2 = "54";
	
//	ClothingTypeData
	private static final String name1 = "T-Shirt";
	private static final String name2 = "Hose";
	
	private Clothing c1;
	private Clothing c2;
	private Clothing c3;
	private Clothing c4;
	private Clothing c5;
	private Clothing c6;
	private Clothing c7;

	
	private ClothingType ct1;
	private ClothingType ct2;
	
	private Person p1;
	private Person p2;
	
	private PersonClothing pc1;
	private PersonClothing pc2;
	private PersonClothing pc3;
	private PersonClothing pc4;
	private PersonClothing pc5;
	
	private ClothingGroupDTO cgDTO1;
	private ClothingGroupDTO cgDTO2;
	
	
	
	
	@Inject
	ClothingRepository clothingRepository;
	
	@Inject
	PersonRepository personRepository;
	
	@Inject
	PersonClothingRepository personClothingRepository;
	
	@Inject
	ClothingTypeRepository ClothingTypeRepository;

	@Inject
	ClothingGroupService clothingGroupService;
	
	private static final Pageable page = PageRequest.of(0, 20, Sort.Direction.ASC, "id");
	
	@Before
	public void setUp() throws Exception {
		p1 = new Person();
		p1.setFirstName(firstName);
		p1.setLastName(lastName);
		p1.setStreetAddress(address);
		p1.setPostalCode(plz);
		p1.setCity(city);
		p1.setCountry(country);
		p1.setHasDirectDebit(false);
		
		
		p2 = new Person();
		p2.setFirstName(firstName);
		p2.setLastName(lastName);
		p2.setStreetAddress(address);
		p2.setPostalCode(plz);
		p2.setCity(city);
		p2.setCountry(country);
		p2.setHasDirectDebit(false);
		
		personRepository.save(p2);
		personRepository.save(p1);
		
		ct1 = new ClothingType();
		ct1.setName(name1);
		
		
		ct2 = new ClothingType();
		ct2.setName(name2);
		
		ClothingTypeRepository.save(ct1);
		ClothingTypeRepository.save(ct2);
		
		c1 = new Clothing();
		c1.setType(ct1);
		c1.setSize(size1);
		
		c2 = new Clothing();
		c2.setType(ct2);
		c2.setSize(size2);
		
		c3 = new Clothing();
		c3.setType(ct1);
		c3.setSize(size2);
		c3.setNotAvailable(true);
		
		c4 = new Clothing();
		c4.setType(ct2);
		c4.setSize(size1);
		
		c5 = new Clothing();
		c5.setType(ct1);
		
		c6 = new Clothing();
		c6.setType(ct2);
		c6.setNotAvailable(true);
		
		c7 = new Clothing();
		c7.setType(ct1);
		
		
		
		pc1 = new PersonClothing();
		pc1.setBeginDate(LocalDate.of(2016, 5, 1));
		pc1.setEndDate(LocalDate.of(2016, 9, 15));
		pc1.setPerson(p1);
		p1.addPersonClothings(pc1);
		pc1.setClothing(c1);
		c1.addPersonClothings(pc1);
		pc1 = personClothingRepository.save(pc1);
		
		pc2 = new PersonClothing();
		pc2.setBeginDate(LocalDate.of(2016, 7, 10));
		pc2.setPerson(p2);
		p2.addPersonClothings(pc2);
		pc2.setClothing(c2);
		c2.addPersonClothings(pc2);
		pc2 = personClothingRepository.save(pc2);
		
		pc3 = new PersonClothing();
		pc3.setClothing(c3);
		c3.addPersonClothings(pc3);
		pc3.setPerson(p1);
		p1.addPersonClothings(pc3);
		pc3.setBeginDate(LocalDate.of(2016, 5, 1));
		pc3.setEndDate(LocalDate.of(2016, 9, 15));
		pc3 = personClothingRepository.save(pc3);

		pc4 = new PersonClothing();
		pc4.setClothing(c4);
		c4.addPersonClothings(pc4);
		pc4.setPerson(p2);
		p2.addPersonClothings(pc4);
		pc4.setBeginDate(LocalDate.of(2016, 5, 10));
		pc4.setEndDate(LocalDate.of(2016, 9, 10));
		pc4 = personClothingRepository.save(pc4);

		pc5 = new PersonClothing();
		pc5.setClothing(c5);
		c5.addPersonClothings(pc5);
		pc5.setPerson(p2);
		p2.addPersonClothings(pc5);
		pc5.setBeginDate(LocalDate.of(2016, 12, 05));
		pc5 = personClothingRepository.save(pc5);
		c7 = clothingRepository.save(c7);
		c6 = clothingRepository.save(c6);
		c5 = clothingRepository.save(c5);
		c4 = clothingRepository.save(c4);
		c3 = clothingRepository.save(c3);
		c2 = clothingRepository.save(c2);
		c1 = clothingRepository.save(c1);

		personRepository.flush();
		clothingRepository.flush();
		ClothingTypeRepository.flush();
		personClothingRepository.flush();

		
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testSave() {
		cgDTO1 = new ClothingGroupDTO();
		cgDTO1.setQuantity(10);
		cgDTO1.setCsize(size1);
		cgDTO1.setTypeId(ct1.getId());
		
		cgDTO1 = clothingGroupService.save(cgDTO1);
		
//		set to 10 with save
		assertThat(cgDTO1.getQuantity(), Matchers.equalTo(10));
		assertThat(cgDTO1.getCsize(), Matchers.equalTo(size1));
		assertThat(cgDTO1.getTypeId(), Matchers.equalTo(ct1.getId()));
		
		
		
		Specification<Clothing> spec = ClothingSpecification.allWith(ct1, cgDTO1.getCsize());
		
		List<Clothing> clothings = clothingRepository.findAll(spec);
		
		assertThat(clothings.size(), Matchers.greaterThanOrEqualTo(10));
		
		
	}
	
	@Test
	public void testGetAll() {
		Page<ClothingGroupDTO> res = clothingGroupService.findAll(page);
		
		assertThat(res.getContent(), Matchers.hasSize(6));
		
	}
	
	@Test
	public void testDelete() {
		cgDTO1 = new ClothingGroupDTO();
		cgDTO1.setQuantity(10);
		cgDTO1.setCsize(size1);
		cgDTO1.setTypeId(ct1.getId());
		
		cgDTO1 = clothingGroupService.save(cgDTO1);
		
		cgDTO2 = new ClothingGroupDTO();
		cgDTO2.setQuantity(4);
		cgDTO2.setCsize(size1);
		cgDTO2.setTypeId(ct1.getId());
		
		clothingGroupService.delete(cgDTO2);
		
		cgDTO2 = clothingGroupService.findOne(cgDTO2.getCsize(), cgDTO2.getTypeId());
		
		//set to 10 with save and - 4 = 6
		
		assertThat(cgDTO2.getQuantity(), Matchers.equalTo(6));
		assertThat(cgDTO2.getCsize(), Matchers.equalTo(size1));
		assertThat(cgDTO2.getTypeId(), Matchers.equalTo(ct1.getId()));
		
		Specification<Clothing> spec = ClothingSpecification.allWith(ct1, cgDTO2.getCsize());
		
		List<Clothing> clothings = clothingRepository.findAll(spec);
		
		assertThat(clothings.size(), Matchers.greaterThanOrEqualTo(6));
		
	}

	@Test(expected = ServiceValidationException.class)
	public void testDeleteMoreThanAvailable() {
		cgDTO1 = new ClothingGroupDTO();
		cgDTO1.setQuantity(10);
		cgDTO1.setCsize(size1);
		cgDTO1.setTypeId(ct1.getId());
		
		cgDTO1 = clothingGroupService.save(cgDTO1);
		
		cgDTO2 = new ClothingGroupDTO();
		cgDTO2.setQuantity(11);
		cgDTO2.setCsize(size1);
		cgDTO2.setTypeId(ct2.getId());
		
		cgDTO2 = clothingGroupService.delete(cgDTO2);
		
		
	}
	
	

}
