package at.simianarmy.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import at.simianarmy.WalterApp;
import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Template;
import at.simianarmy.filestore.service.TemplateFileStoreService;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.dto.TemplateDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.TemplateMapper;
import biweekly.util.IOUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class TemplateServiceTest {
  
  private static final String testResources = "src/test/resources/template";

	private static final Long id1 = new Long(1);
	private static final Long id2 = new Long(2);
	private static final Long id3 = new Long(3);
	
	
	private static final String title1 = "name1";
	private static final String title2 = "name2";
	private static final String title3 = "name3";
	
	private Template template1;
	private Template template2;
	private Template template3;
	
	
	@Inject
	@Spy
	private TemplateMapper templateMapper;
	
	@Mock
	private TemplateFileStoreService fileStore;
	
	@Mock
	private TemplateRepository templateRepository;
	
	@Mock
  private LetterRepository letterRepository;
	
	@InjectMocks
	private TemplateService templateService;
	
	

	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		template1 = new Template();
		template1.setId(id1);
		template1.setTitle(title1);
		template2 = new Template();
		template2.setId(id2);
		template2.setTitle(title2);
		template3 = new Template();
    template3.setId(id3);
    template3.setTitle(title3);
		
		template1.setMarginLeft(0.5F); //must not be saved (lower bound)
		
		template2.setMarginRight(11F); //must not be saved (upper bound)
		
		template3.setMarginLeft(3F);
		//template3 margins = default value
		
		when(templateRepository.findById(id1)).thenReturn(Optional.ofNullable(template1));
		when(templateRepository.findById(id2)).thenReturn(Optional.ofNullable(template2));
		when(templateRepository.findById(id3)).thenReturn(Optional.ofNullable(template3));
		
		when(fileStore.saveTemplate(Mockito.any())).thenReturn(String.valueOf("TemplateUUID"));
	}

	@After
	public void tearDown() throws Exception {
	}
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveMarginLowerBound() {
    TemplateDTO template1dto = templateMapper.templateToTemplateDTO(template1);
    templateService.save(template1dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveMarginUpperBound() {
    TemplateDTO template2dto = templateMapper.templateToTemplateDTO(template2);
    templateService.save(template2dto);
  }
  
  @Test
  public void testSaveMarginDefault() {
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    templateService.save(template3dto);
    
    List<Template> result = templateRepository.findAll();
    
    for (Template t : result) {
      assertTrue(t.getMarginLeft().equals(3F));
      assertTrue(t.getMarginRight().equals((com.lowagie.text.Utilities.pointsToMillimeters(SerialLetterConstants.defaultMarginRight))/10));
      assertTrue(t.getMarginTop().equals((com.lowagie.text.Utilities.pointsToMillimeters(SerialLetterConstants.defaultMarginTop))/10));
      assertTrue(t.getMarginBottom().equals((com.lowagie.text.Utilities.pointsToMillimeters(SerialLetterConstants.defaultMarginBottom))/10));
    }
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveFileNoA4() throws IOException {
    File file = new File(testResources + "/" + "notA4.pdf");
    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile("file",
            file.getName(), "application/pdf", IOUtils.toByteArray(input));
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    template3dto.setUploadedFile(multipartFile);
    templateService.save(template3dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveFileMoreThanOnePage() throws IOException {
    File file = new File(testResources + "/" + "moreThanOnePage.pdf");
    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile("file",
            file.getName(), "application/pdf", IOUtils.toByteArray(input));
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    template3dto.setUploadedFile(multipartFile);
    templateService.save(template3dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveFileNoPdf() throws IOException {
    File file = new File(testResources + "/" + "noPdf.docx");
    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile("file",
            file.getName(), "application/pdf", IOUtils.toByteArray(input));
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    template3dto.setUploadedFile(multipartFile);
    templateService.save(template3dto);
  }
  
  @Test(expected = ServiceValidationException.class)
  public void testSaveFileLandscapeOrientation() throws IOException {
    File file = new File(testResources + "/" + "landscapeOrientation.pdf");
    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile("file",
            file.getName(), "application/pdf", IOUtils.toByteArray(input));
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    template3dto.setUploadedFile(multipartFile);
    templateService.save(template3dto);
  }
  
  @Test
  public void testSaveFilePortraitOrientation() throws IOException {
    File file = new File(testResources + "/" + "portraitOrientation.pdf");
    FileInputStream input = new FileInputStream(file);
    MultipartFile multipartFile = new MockMultipartFile("file",
            file.getName(), "application/pdf", IOUtils.toByteArray(input));
    TemplateDTO template3dto = templateMapper.templateToTemplateDTO(template3);
    template3dto.setUploadedFile(multipartFile);
    templateService.save(template3dto);
    
    List<Template> result = templateRepository.findAll();
    
    for (Template t : result) {
      TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(t);
      assertTrue(templateDTO.getUploadedFile().equals(multipartFile));
    }
  }
}
