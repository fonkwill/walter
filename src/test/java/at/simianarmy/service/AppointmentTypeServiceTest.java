package at.simianarmy.service;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.AppointmentType;
import at.simianarmy.repository.AppointmentTypeRepository;
import at.simianarmy.service.exception.ServiceValidationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AppointmentTypeServiceTest {

  private static long id1 = 1;
  private Appointment appointment = new Appointment();
  private AppointmentType testAppointmentType;

  @Mock
  private AppointmentTypeRepository appointmentTypeRepository;

  @InjectMocks
  private AppointmentTypeService appointmentTypeService;

  private AppointmentType createAppointmentType(Long id, String name, Boolean isOfficial) {
    AppointmentType appointmentType = new AppointmentType();
    appointmentType.setId(id);
    appointmentType.setName(name);
    appointmentType.setIsOfficial(isOfficial);
    return appointmentType;
  }


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    testAppointmentType = createAppointmentType(id1, "TestAppointmentType", false);
    appointmentTypeRepository.saveAndFlush(testAppointmentType);

    appointment.setId(id1);
    appointment.setName("TestAppointment");
    appointment.setBeginDate(ZonedDateTime.now());
    appointment.setEndDate(ZonedDateTime.now());

    when(appointmentTypeRepository.findById(id1)).thenReturn(Optional.of(testAppointmentType));
 }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = ServiceValidationException.class)
  public void testDeleteWithAppointment() throws Exception {
    // deleting when an AppointmentType has Appointments should fail
    testAppointmentType.addAppointments(appointment);
    appointmentTypeService.delete(id1);
  }

  @Test
  public void testDelete() throws Exception {
    // deleting when an AppointmentType has no Appointments should work
    testAppointmentType.removeAppointments(appointment);
    appointmentTypeService.delete(id1);
  }

}
