package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.repository.InstrumentTypeRepository;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentTypeServiceTest {

	private static final Long id1 = new Long(1);
	
	private static final Long instrumentId = new Long(10);
	
	private static final String name1 = "name1";
	
	private static final Instrument instrument = new Instrument();
	
	private InstrumentType type1;
	
	@Mock
	private InstrumentTypeRepository InstrumentTypeRepository;
	
	@InjectMocks
	private InstrumentTypeService InstrumentTypeService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		type1 = new InstrumentType();
		type1.setId(id1);
		type1.setName(name1);
		
		instrument.setId(instrumentId);
		
		type1.addInstruments(instrument); //must not be deleted
		
		when(InstrumentTypeRepository.findById(id1)).thenReturn(Optional.ofNullable(type1));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithInstrument() {
	  InstrumentTypeService.delete(type1.getId());
	}
}
