package at.simianarmy.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.Template;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.domain.enumeration.MailLogStatus;
import at.simianarmy.letter.SerialLetterFactory;
import at.simianarmy.letter.SerialLetterFactoryException;
import at.simianarmy.letter.service.SerialLetterService;
import at.simianarmy.letter.service.SerialLetterServiceFactory;
import at.simianarmy.letter.service.SerialLetterServiceFactoryException;
import at.simianarmy.letter.worker.SerialLetterWorker;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.repository.MailLogRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.dto.MailLetterDTO;
import at.simianarmy.service.exception.SerialLetterGenerationException;
import at.simianarmy.service.mapper.MailLetterMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.activation.DataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class MailLetterServiceTest {

  private static final LetterType DEFAULT_LETTER_TYPE = LetterType.SERIAL_LETTER;
  private static final String DEFAULT_MAIL1 = "test@test.com";
  private static final String DEFAULT_MAIL2 = "test2@test.com";
  private static final String DEFAULT_SUBJECT = "SUBJECT";
  private static final String DEFAULT_MAILTEXT = "MAILTEXT";
  private static final String DEFAULT_ATTACHMENT_NAME = "ATTACHMENT";
  private static final Long DEFAULT_PG_ID = new Long(1);

  @Mock
  private MailLetterMapper mailLetterMapper;

  @Mock
  private SerialLetterServiceFactory serialLetterServiceFactory;

  @Mock
  private SerialLetterFactory serialLetterFactory;

  @Mock
  private MailService mailService;

  @Mock
  private TemplateRepository templateRepository;

  @Mock
  private LetterRepository letterRepository;

  @Mock
  private MailLogRepository mailLogRepository;

  @InjectMocks
  private MailLetterService sut;

  @Test
  public void testSendToTwoPersons()
    throws SerialLetterServiceFactoryException, SerialLetterFactoryException, IOException {
    Template template = Mockito.mock(Template.class);
    Set<PersonGroup> personGroups = new HashSet<>();
    Letter letter = this.newLetterMock(template, personGroups);
    MailLetterDTO dto = this.newMailLetterDTOMock(letter);

    Person p1 = this.newPersonMock(DEFAULT_MAIL1);
    Person p2 = this.newPersonMock(DEFAULT_MAIL2);

    SerialLetterService serialLetterService = this.newSerialLetterServiceMock(letter);

    Mockito.when(serialLetterService.getRelevantPersonsForMailLetter(letter)).thenReturn(
      new ArrayList<>(Arrays.asList(p1, p2))
    );

    SerialLetterWorker worker = this.newSerialLetterWorkerMock(letter);

    this.sut.send(dto);

    Mockito.verify(worker, Mockito.times(1)).setCompanies(new ArrayList<>());
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p1)));
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p2)));
    Mockito.verify(worker, Mockito.times(2)).run();

    ArgumentCaptor<MailLog> mailLogCaptor = ArgumentCaptor.forClass(MailLog.class);
    MailLog capturedMailLog = null;
    Mockito.verify(this.mailService, Mockito.times(1)).sendEmailWithAttachment(
      eq(DEFAULT_MAIL1),
      eq(DEFAULT_SUBJECT),
      eq(DEFAULT_MAILTEXT),
      eq(true),
      eq(true),
      any(DataSource.class),
      eq(DEFAULT_ATTACHMENT_NAME),
      mailLogCaptor.capture()
    );

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL1, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    Mockito.verify(this.mailLogRepository, Mockito.times(1)).saveAndFlush(capturedMailLog);

    Mockito.verify(this.mailService, Mockito.times(1)).sendEmailWithAttachment(
      eq(DEFAULT_MAIL2),
      eq(DEFAULT_SUBJECT),
      eq(DEFAULT_MAILTEXT),
      eq(true),
      eq(true),
      any(DataSource.class),
      eq(DEFAULT_ATTACHMENT_NAME),
      mailLogCaptor.capture()
    );

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL2, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    Mockito.verify(this.mailLogRepository, Mockito.times(1)).saveAndFlush(capturedMailLog);

  }

  @Test
  public void testSendToOnePerson()
    throws SerialLetterServiceFactoryException, SerialLetterFactoryException {
    Template template = Mockito.mock(Template.class);
    Set<PersonGroup> personGroups = new HashSet<>();
    Letter letter = this.newLetterMock(template, personGroups);
    MailLetterDTO dto = this.newMailLetterDTOMock(letter);

    Person p = this.newPersonMock(DEFAULT_MAIL1);

    SerialLetterService serialLetterService = this.newSerialLetterServiceMock(letter);

    Mockito.when(serialLetterService.getRelevantPersonsForMailLetter(letter)).thenReturn(
      new ArrayList<>(Arrays.asList(p))
    );

    SerialLetterWorker worker = this.newSerialLetterWorkerMock(letter);

    this.sut.send(dto);

    Mockito.verify(worker, Mockito.times(1)).setCompanies(new ArrayList<>());
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p)));
    Mockito.verify(worker, Mockito.times(1)).run();

    ArgumentCaptor<MailLog> mailLogCaptor = ArgumentCaptor.forClass(MailLog.class);
    MailLog capturedMailLog = null;
    Mockito.verify(this.mailService, Mockito.times(1)).sendEmailWithAttachment(
      eq(DEFAULT_MAIL1),
      eq(DEFAULT_SUBJECT),
      eq(DEFAULT_MAILTEXT),
      eq(true),
      eq(true),
      any(DataSource.class),
      eq(DEFAULT_ATTACHMENT_NAME),
      mailLogCaptor.capture()
    );

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL1, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    Mockito.verify(this.mailLogRepository, Mockito.times(1)).saveAndFlush(capturedMailLog);

  }

  @Test
  public void testSendToOnePersonWithPersonGroup()
    throws SerialLetterServiceFactoryException, SerialLetterFactoryException {
    Template template = Mockito.mock(Template.class);
    Set<PersonGroup> personGroups = new HashSet<>();
    PersonGroup personGroup = this.newPersonGroupMock(DEFAULT_PG_ID);
    personGroups.add(personGroup);
    List<Long> personGroupIds = new ArrayList<>(Arrays.asList(DEFAULT_PG_ID));
    Letter letter = this.newLetterMock(template, personGroups);
    MailLetterDTO dto = this.newMailLetterDTOMock(letter);

    Person p = this.newPersonMock(DEFAULT_MAIL1);

    SerialLetterService serialLetterService = this.newSerialLetterServiceMock(letter);

    Mockito.when(serialLetterService.getRelevantPersonsForMailLetter(letter))
      .thenReturn(
        new ArrayList<>(Arrays.asList(p))
      );

    SerialLetterWorker worker = this.newSerialLetterWorkerMock(letter);

    this.sut.send(dto);

    Mockito.verify(worker, Mockito.times(1)).setCompanies(new ArrayList<>());
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p)));
    Mockito.verify(worker, Mockito.times(1)).run();

    ArgumentCaptor<MailLog> mailLogCaptor = ArgumentCaptor.forClass(MailLog.class);
    MailLog capturedMailLog = null;
    Mockito.verify(this.mailService, Mockito.times(1)).sendEmailWithAttachment(
      eq(DEFAULT_MAIL1),
      eq(DEFAULT_SUBJECT),
      eq(DEFAULT_MAILTEXT),
      eq(true),
      eq(true),
      any(DataSource.class),
      eq(DEFAULT_ATTACHMENT_NAME),
      mailLogCaptor.capture()
    );

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL1, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    Mockito.verify(this.mailLogRepository, Mockito.times(1)).saveAndFlush(capturedMailLog);
  }

  @Test
  public void testSendAndSaveMailLetterData()
    throws SerialLetterServiceFactoryException, SerialLetterFactoryException {
    Template template = Mockito.mock(Template.class);
    Set<PersonGroup> personGroups = new HashSet<>();
    PersonGroup personGroup = this.newPersonGroupMock(DEFAULT_PG_ID);
    personGroups.add(personGroup);
    List<Long> personGroupIds = new ArrayList<>(Arrays.asList(DEFAULT_PG_ID));
    Letter letter = this.newLetterMock(template, personGroups);
    MailLetterDTO dto = this.newMailLetterDTOMock(letter);
    Mockito.when(dto.isSaveInLetter()).thenReturn(true);

    Person p = this.newPersonMock(DEFAULT_MAIL1);

    SerialLetterService serialLetterService = this.newSerialLetterServiceMock(letter);

    Mockito.when(serialLetterService.getRelevantPersonsForMailLetter(letter))
      .thenReturn(
        new ArrayList<>(Arrays.asList(p))
      );

    SerialLetterWorker worker = this.newSerialLetterWorkerMock(letter);

    this.sut.send(dto);

    Mockito.verify(worker, Mockito.times(1)).setCompanies(new ArrayList<>());
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p)));
    Mockito.verify(worker, Mockito.times(1)).run();

    ArgumentCaptor<MailLog> mailLogCaptor = ArgumentCaptor.forClass(MailLog.class);
    MailLog capturedMailLog = null;
    Mockito.verify(this.mailService, Mockito.times(1)).sendEmailWithAttachment(
      eq(DEFAULT_MAIL1),
      eq(DEFAULT_SUBJECT),
      eq(DEFAULT_MAILTEXT),
      eq(true),
      eq(true),
      any(DataSource.class),
      eq(DEFAULT_ATTACHMENT_NAME),
      mailLogCaptor.capture()
    );

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL1, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    Mockito.verify(this.mailLogRepository, Mockito.times(1)).saveAndFlush(capturedMailLog);

    Mockito.verify(this.letterRepository, Mockito.times(1)).saveAndFlush(letter);

  }

  @Test
  public void testSendToOnePersonWithSerialLetterError()
    throws SerialLetterServiceFactoryException, SerialLetterFactoryException {
    Template template = Mockito.mock(Template.class);
    Set<PersonGroup> personGroups = new HashSet<>();
    Letter letter = this.newLetterMock(template, personGroups);
    MailLetterDTO dto = this.newMailLetterDTOMock(letter);

    Person p = this.newPersonMock(DEFAULT_MAIL1);

    SerialLetterService serialLetterService = this.newSerialLetterServiceMock(letter);

    Mockito.when(serialLetterService.getRelevantPersonsForMailLetter(letter)).thenReturn(
      new ArrayList<>(Arrays.asList(p))
    );

    SerialLetterWorker worker = this.newSerialLetterWorkerMock(letter);
    Mockito.doThrow(new SerialLetterGenerationException("ERROR!", null)).when(worker).run();

    this.sut.send(dto);

    Mockito.verify(worker, Mockito.times(1)).setCompanies(new ArrayList<>());
    Mockito.verify(worker, Mockito.times(1)).setPersons(new ArrayList<>(Arrays.asList(p)));
    Mockito.verify(worker, Mockito.times(1)).run();

    ArgumentCaptor<MailLog> mailLogCaptor = ArgumentCaptor.forClass(MailLog.class);
    MailLog capturedMailLog = null;
    Mockito.verify(this.mailLogRepository, Mockito.times(2)).saveAndFlush(mailLogCaptor.capture());

    capturedMailLog = mailLogCaptor.getValue();
    assertEquals(DEFAULT_MAIL1, capturedMailLog.getEmail());
    assertEquals(DEFAULT_SUBJECT, capturedMailLog.getSubject());
    assertEquals(MailLogStatus.UNSUCCESSFUL, capturedMailLog.getMailLogStatus());
    assertEquals(ServiceErrorConstants.serialLetterNotGeneratable, capturedMailLog.getErrorMessage());

  }

  private MailLetterDTO newMailLetterDTOMock(Letter letter) {
    MailLetterDTO dto = Mockito.mock(MailLetterDTO.class);
    Mockito.when(this.mailLetterMapper.mailLetterDTOToLetter(dto)).thenReturn(letter);
    Mockito.when(dto.getSubject()).thenReturn(DEFAULT_SUBJECT);
    Mockito.when(dto.getMailText()).thenReturn(DEFAULT_MAILTEXT);
    Mockito.when(dto.getAttachmentFileName()).thenReturn(DEFAULT_ATTACHMENT_NAME);
    Mockito.when(letter.getSubject()).thenReturn(DEFAULT_SUBJECT);
    Mockito.when(letter.getMailText()).thenReturn(DEFAULT_MAILTEXT);
    Mockito.when(letter.getAttachmentFileName()).thenReturn(DEFAULT_ATTACHMENT_NAME);
    return dto;
  }

  private Letter newLetterMock(Template template, Set<PersonGroup> personGroups) {
    Letter letter = Mockito.mock(Letter.class);
    Mockito.when(letter.getLetterType()).thenReturn(DEFAULT_LETTER_TYPE);
    Mockito.when(letter.getTemplate()).thenReturn(template);
    Mockito.when(letter.getPersonGroups()).thenReturn(personGroups);
    return letter;
  }

  private Person newPersonMock(String email) {
    Person person = Mockito.mock(Person.class);
    Mockito.when(person.getEmail()).thenReturn(email);
    return person;
  }

  private PersonGroup newPersonGroupMock(Long id) {
    PersonGroup pg = Mockito.mock(PersonGroup.class);
    Mockito.when(pg.getId()).thenReturn(id);
    return pg;
  }

  private SerialLetterService newSerialLetterServiceMock(Letter letter)
    throws SerialLetterServiceFactoryException {
    SerialLetterService service = Mockito.mock(SerialLetterService.class);
    Mockito.when(this.serialLetterServiceFactory.createForLetter(letter)).thenReturn(service);
    return service;
  }

  private SerialLetterWorker newSerialLetterWorkerMock(Letter letter)
    throws SerialLetterFactoryException {
    SerialLetterWorker worker = Mockito.mock(SerialLetterWorker.class);
    Mockito.when(this.serialLetterFactory.createForLetter(letter)).thenReturn(worker);
    Mockito.when(worker.getLetter()).thenReturn(letter);
    return worker;
  }

}
