package at.simianarmy.service;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentService;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.CashbookEntryMapper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookEntryServiceTest {

  private static final Long id1 = new Long(1);
  private static final Long id2 = new Long(2);
  private static final Long id3 = new Long(3);
  private static final Long id4 = new Long(4);
  private static final Long id5 = new Long(5);

  private static final Long idCategory1 = new Long(1);
  private static final Long idCategory2 = new Long(2);
  private static final Long idCategory3 = new Long(3);
  private static final Long idCategory4 = new Long(4);
  
  private CashbookCategory cat1;
  private CashbookCategory cat2;
  private CashbookCategory cat3;
  private CashbookCategory cat4;

  
  
  
  private CashbookEntry entry1;
  private CashbookEntry entry2;
  private CashbookEntry entry3;
  private CashbookEntry entry4;
  private CashbookEntry entry5;

  private BankImportData bankImportData;

  
  
  @Mock
  private BankImportDataRepository bankImportDataRepository;

  @Mock
  private CashbookEntryRepository cashbookEntryRepository;
  
  @Mock
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  @Spy
  private CashbookEntryMapper cashbookEntryMapper;

  @InjectMocks
  private CashbookEntryService cashbookEntryService;


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    entry1 = new CashbookEntry();
    entry1.setId(id1);
    entry1.setAmount(BigDecimal.TEN);
    entry1.setType(CashbookEntryType.INCOME);
    entry1.setTitle("entry 1");
    entry1.setDate(LocalDate.of(2016, 10, 10));

    InstrumentService is1 = new InstrumentService();
    is1.setId(new Long(1));
    entry1.setInstrumentService(is1);

    entry2 = new CashbookEntry();
    entry2.setId(id2);
    entry2.setAmount(BigDecimal.TEN);
    entry2.setType(CashbookEntryType.INCOME);
    entry2.setTitle("entry 2");
    entry2.setDate(LocalDate.of(2016, 10, 10));

    MembershipFeeForPeriod membershipFeeForPeriod = new MembershipFeeForPeriod();
    entry2.setMembershipFeeForPeriod(membershipFeeForPeriod);

    entry3 = new CashbookEntry();
    entry3.setId(id3);
    entry3.setAmount(BigDecimal.TEN);
    entry3.setType(CashbookEntryType.INCOME);
    entry3.setTitle("entry 3");
    entry3.setDate(LocalDate.of(2016, 10, 10));

    entry3.addCompostion(new Composition());

    entry4 = new CashbookEntry();
    entry4.setId(id4);
    entry4.setAmount(BigDecimal.TEN);
    entry4.setType(CashbookEntryType.INCOME);
    entry4.setTitle("entry 4");
    entry4.setDate(LocalDate.of(2016, 10, 10));
    
    entry5 = new CashbookEntry();
    entry5.setId(id5);
    entry5.setAmount(BigDecimal.TEN);
    entry5.setType(CashbookEntryType.INCOME);
    entry5.setTitle("entry 5");
    entry5.setDate(LocalDate.of(2016, 10, 10));
    entry5.setInstrument(new Instrument());

    when(cashbookEntryRepository.findById(id1)).thenReturn(Optional.ofNullable(entry1));
    when(cashbookEntryRepository.findById(id2)).thenReturn(Optional.ofNullable(entry2));
    when(cashbookEntryRepository.findById(id3)).thenReturn(Optional.ofNullable(entry3));
    when(cashbookEntryRepository.findById(id5)).thenReturn(Optional.ofNullable(entry5));
    
    cat1 = new CashbookCategory();
    cat1.setId(idCategory1);
    cat1.setName("a");
    
    cat2 = new CashbookCategory();
    cat2.setId(idCategory2);
    cat2.setName("b");
    
    cat3 = new CashbookCategory();
    cat3.setId(idCategory3);
    cat3.setName("c");
    
    cat4 = new CashbookCategory();
    cat4.setId(idCategory4);
    cat4.setName("d");
    
    when(cashbookCategoryRepository.findById(idCategory1)).thenReturn(Optional.ofNullable(cat1));
    when(cashbookCategoryRepository.findById(idCategory2)).thenReturn(Optional.ofNullable(cat2));
    when(cashbookCategoryRepository.findById(idCategory3)).thenReturn(Optional.ofNullable(cat3));
    when(cashbookCategoryRepository.findById(idCategory4)).thenReturn(Optional.ofNullable(cat4));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDeleteWithBankImportData() {
    this.bankImportData = Mockito.mock(BankImportData.class);
    when(bankImportDataRepository.findByCashbookEntry(entry4.getId()))
        .thenReturn(this.bankImportData);
    
    this.cashbookEntryService.delete(id4);
    
    assertNull(this.cashbookEntryRepository.findById(id4).orElse(null));
    Mockito.verify(this.bankImportDataRepository, Mockito.times(1)).delete(this.bankImportData);
  }
  
  @Test
  public void testDeleteWithoutBankImportData() {
    this.cashbookEntryService.delete(id4);
    
    assertNull(this.cashbookEntryRepository.findById(id4).orElse(null));
    Mockito.verify(this.bankImportDataRepository, Mockito.never()).deleteById(ArgumentMatchers.anyLong());
  }

  @Test(expected = ServiceValidationException.class)
  public void testEditOfProtectedFieldInstrumentService() {
    CashbookEntry cashbookEntry = new CashbookEntry();
    cashbookEntry.setId(entry1.getId());
    cashbookEntry.setAmount(entry1.getAmount());
    cashbookEntry.setType(entry1.getType());
    cashbookEntry.setInstrumentService(entry1.getInstrumentService());
    cashbookEntry.setDate(entry1.getDate());
    cashbookEntry.setTitle("asd");

    CashbookEntryDTO cashbookEntryDTO = this.cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    this.cashbookEntryService.save(cashbookEntryDTO);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUndeleteableWhenInstrumentServiceExists() {
    this.cashbookEntryService.delete(id1);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUndeleteableWhenMembershipFeeExists() {
    this.cashbookEntryService.delete(id2);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUndeleteableWhenCompositionExists() {
    this.cashbookEntryService.delete(id3);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUndeleteableWhenInstrumentExists() {
    this.cashbookEntryService.delete(id5);
  }
  
  @Test
  public void test_addCategoryGroups_with_Parent() {
	  java.util.List<Long> ids = new ArrayList<>();
	  
	  cat2.setParent(cat1);
	  cat1.setChildren(new HashSet<>(Arrays.asList(cat2)));
	  
	  
	  this.cashbookEntryService.addCategoryIdsForCategory(cat1, ids);
	  Assert.assertThat(ids, Matchers.hasSize(2));
	  Assert.assertThat(ids, Matchers.containsInAnyOrder(1L, 2L));
	  
	  
	  
  }
  
  @Test
  public void test_addCategoryGroups_with_parent_2_children() {
	  java.util.List<Long> ids = new ArrayList<>();
	  
	  cat2.setParent(cat1);
	  cat3.setParent(cat1);
	  cat1.setChildren(new HashSet<>(Arrays.asList(cat2, cat3)));
	  
	  
	  this.cashbookEntryService.addCategoryIdsForCategory(cat1, ids);
	  Assert.assertThat(ids, Matchers.hasSize(3));
	  Assert.assertThat(ids, Matchers.containsInAnyOrder(1L, 2L, 3L));
	  
	  
	  
  }
  
  @Test
  public void test_addCategoryGroups_with_parent_2_children_nonEmptyList() {
	  java.util.List<Long> ids = new ArrayList<>();
	  ids.add(5L);
	  cat2.setParent(cat1);
	  cat3.setParent(cat1);
	  cat1.setChildren(new HashSet<>(Arrays.asList(cat2, cat3)));
	  
	  
	  this.cashbookEntryService.addCategoryIdsForCategory(cat1, ids);
	  Assert.assertThat(ids, Matchers.hasSize(4));
	  Assert.assertThat(ids, Matchers.containsInAnyOrder(1L, 2L, 3L, 5L));
	  
  }
  
  @Test
  public void test_addCategoryGroups_with_parent_parent() {
	  java.util.List<Long> ids = new ArrayList<>();
	  
	  cat2.setParent(cat1);
	  cat1.setChildren(new HashSet<>(Arrays.asList(cat2)));
	  
	  cat3.setParent(cat2);
	  cat2.setChildren(new HashSet<>(Arrays.asList(cat3)));
	  
	  this.cashbookEntryService.addCategoryIdsForCategory(cat1, ids);
	  Assert.assertThat(ids, Matchers.hasSize(3));
	  Assert.assertThat(ids, Matchers.containsInAnyOrder(1L, 2L, 3L));
	  
	  
	  
  }
  
}
