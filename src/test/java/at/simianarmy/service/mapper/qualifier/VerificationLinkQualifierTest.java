package at.simianarmy.service.mapper.qualifier;

import static org.junit.Assert.assertEquals;

import at.simianarmy.config.WalterProperties;
import at.simianarmy.config.WalterProperties.MultitenancyProperties;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import java.net.InetAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class VerificationLinkQualifierTest {

  private final String HOST = "localhost";
  private final Integer PORT = 8080;
  private final Integer PORT_SSL = 443;
  private final String URL = "email-verification";
  private final String TENANT = "test_tenant";
  private final Long PERSONID = new Long(1);

  @Mock
  private ServerProperties serverProperties;

  @Mock
  private TenantIdentiferResolver tenantIdentiferResolver;

  @Mock
  private WalterProperties walterProperties;

  @InjectMocks
  private VerificationLinkQualifier sut;

  private MultitenancyProperties multitenancyProperties;

  private InetAddress inetAddress;

  @Before
  public void init() {
    this.inetAddress = Mockito.mock(InetAddress.class);
    this.multitenancyProperties = new MultitenancyProperties();
    Mockito.when(this.walterProperties.getEmailVerificationHost()).thenReturn(HOST);
    Mockito.when(this.walterProperties.getEmailVerificationUrl()).thenReturn(URL);
    Mockito.when(this.serverProperties.getAddress()).thenReturn(this.inetAddress);
    Mockito.when(this.serverProperties.getPort()).thenReturn(PORT);
  }

  @Test
  public void testVerificationLinkForPersonId() {
    this.multitenancyProperties.setEnabled(false);
    Mockito.when(this.walterProperties.getMultitenancy()).thenReturn(this.multitenancyProperties);
    String url = this.sut.verificationLinkForPersonId(PERSONID);

    this.assertIsCorrectVerificationLink(url, PERSONID, false, false);
  }

  @Test
  public void testVerificationLinkForPersonIdWithSSL() {
    this.multitenancyProperties.setEnabled(false);
    Mockito.when(this.walterProperties.getMultitenancy()).thenReturn(this.multitenancyProperties);
    Mockito.when(this.serverProperties.getPort()).thenReturn(PORT_SSL);
    String url = this.sut.verificationLinkForPersonId(PERSONID);

    this.assertIsCorrectVerificationLink(url, PERSONID, true, false);
  }

  @Test
  public void testVerificationLinkForPersonIdWithTenant() {
    this.multitenancyProperties.setEnabled(true);
    Mockito.when(this.walterProperties.getMultitenancy()).thenReturn(this.multitenancyProperties);
    Mockito.when(this.tenantIdentiferResolver.resolveCurrentTenantIdentifier()).thenReturn(TENANT);
    String url = this.sut.verificationLinkForPersonId(PERSONID);

    this.assertIsCorrectVerificationLink(url, PERSONID, false, true);
  }

  private void assertIsCorrectVerificationLink(String link, Long personid, Boolean isSsl, Boolean withTenant) {
    String protocol = "http";
    Integer port = PORT;
    String tenant = "";

    if (isSsl) {
      protocol += "s";
      port = PORT_SSL;
    }

    if (withTenant) {
      tenant = "&tenant=" + TENANT;
    }

    String expectedLink = protocol + "://" + HOST + "/" + URL + "?id=" + personid + tenant;

    assertEquals(expectedLink, link);
  }

}
