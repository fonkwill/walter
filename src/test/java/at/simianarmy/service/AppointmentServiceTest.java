package at.simianarmy.service;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.Person;
import at.simianarmy.repository.AppointmentRepository;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.dto.AppointmentDTO;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.AppointmentMapper;
import at.simianarmy.service.mapper.PersonMapper;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class AppointmentServiceTest {

  private final static Long DEFAULT_ID = new Long(1);

  @Mock
  private AppointmentRepository appointmentRepository;

  @Mock
  private PersonRepository personRepository;

  @Mock
  private AppointmentMapper appointmentMapper;

  @Mock
  private PersonMapper personMapper;

  @InjectMocks
  private AppointmentService appointmentService;

  @Test
  public void testSave() {
    AppointmentDTO appointmentDTO = Mockito.mock(AppointmentDTO.class);
    Appointment appointment = Mockito.mock(Appointment.class);

    Mockito.when(this.appointmentMapper.appointmentDTOToAppointment(appointmentDTO))
        .thenReturn(appointment);
    Mockito.when(appointment.getBeginDate()).thenReturn(ZonedDateTime.now());
    Mockito.when(appointment.getEndDate()).thenReturn(ZonedDateTime.now().plusDays(1));
    Mockito.when(this.appointmentRepository.saveAndFlush(appointment)).thenReturn(appointment);
    Mockito.when(this.appointmentMapper.appointmentToAppointmentDTO(appointment))
        .thenReturn(appointmentDTO);

    AppointmentDTO result = this.appointmentService.save(appointmentDTO);
    assertEquals(result, appointmentDTO);

    Mockito.verify(this.appointmentRepository, Mockito.times(1)).saveAndFlush(appointment);
  }

  @Test(expected = ServiceValidationException.class)
  public void testSaveShouldThrowException() {
    AppointmentDTO appointmentDTO = Mockito.mock(AppointmentDTO.class);
    Appointment appointment = Mockito.mock(Appointment.class);

    Mockito.when(this.appointmentMapper.appointmentDTOToAppointment(appointmentDTO))
        .thenReturn(appointment);
    Mockito.when(appointment.getBeginDate()).thenReturn(ZonedDateTime.now());
    Mockito.when(appointment.getEndDate()).thenReturn(ZonedDateTime.now().minusDays(1));

    this.appointmentService.save(appointmentDTO);
  }

  @Test
  public void testAddPerson() {
    PersonDTO personDTO = Mockito.mock(PersonDTO.class);
    Person person = Mockito.mock(Person.class);
    Appointment appointment = Mockito.mock(Appointment.class);

    Mockito.when(this.appointmentRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(appointment));
    Mockito.when(this.personMapper.personDTOToPerson(personDTO)).thenReturn(person);
    Mockito.when(appointment.getPersons()).thenReturn(new HashSet<Person>());

    this.appointmentService.addPerson(DEFAULT_ID, personDTO);

    Mockito.verify(person, Mockito.times(1)).addAppointments(appointment);
    Mockito.verify(this.personRepository, Mockito.times(1)).saveAndFlush(person);
  }

  @Test
  public void testAddPersonAlreadyExisting() {
    PersonDTO personDTO = Mockito.mock(PersonDTO.class);
    Person person = Mockito.mock(Person.class);
    Appointment appointment = Mockito.mock(Appointment.class);

    Mockito.when(this.appointmentRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(appointment));
    Mockito.when(this.personMapper.personDTOToPerson(personDTO)).thenReturn(person);
    Mockito.when(appointment.getPersons()).thenReturn(new HashSet<Person>(Arrays.asList(person)));

    this.appointmentService.addPerson(DEFAULT_ID, personDTO);

    Mockito.verify(person, Mockito.never()).addAppointments(appointment);
    Mockito.verify(this.personRepository, Mockito.never()).saveAndFlush(person);
  }

  @Test
  public void testRemovePerson() {
    Appointment appointment = Mockito.mock(Appointment.class);
    Person person = Mockito.mock(Person.class);

    Mockito.when(this.appointmentRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(appointment));
    Mockito.when(this.personRepository.findById(DEFAULT_ID + 1)).thenReturn(Optional.of(person));

    this.appointmentService.removePerson(DEFAULT_ID, DEFAULT_ID + 1);

    Mockito.verify(person, Mockito.times(1)).removeAppointments(appointment);
    Mockito.verify(this.personRepository, Mockito.times(1)).saveAndFlush(person);
  }

  @Test
  public void testRemovePersonNotExisting() {
    Appointment appointment = Mockito.mock(Appointment.class);

    Mockito.when(this.appointmentRepository.findById(DEFAULT_ID)).thenReturn(Optional.of(appointment));
    Mockito.when(this.personRepository.findById(DEFAULT_ID + 1)).thenReturn(Optional.empty());

    this.appointmentService.removePerson(DEFAULT_ID, DEFAULT_ID + 1);

    Mockito.verify(this.personRepository, Mockito.never()).saveAndFlush(ArgumentMatchers.any());
  }
}
