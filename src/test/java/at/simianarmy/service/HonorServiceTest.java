package at.simianarmy.service;

import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Honor;
import at.simianarmy.domain.PersonHonor;
import at.simianarmy.repository.HonorRepository;
import at.simianarmy.service.exception.ServiceValidationException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class HonorServiceTest {

	private static final Long id1 = new Long(1);
	
	private static final Long personHonorId = new Long(10);
	
	private static final String name1 = "name1";
	
	private static final PersonHonor personHonor = new PersonHonor();
	
	private Honor honor1;
	
	@Mock
	private HonorRepository HonorRepository;
	
	@InjectMocks
	private HonorService HonorService;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		honor1 = new Honor();
		honor1.setId(id1);
		honor1.setName(name1);
		
		personHonor.setId(personHonorId);;
		
		honor1.addPersonHonors(personHonor); //must not be deleted
		
		when(HonorRepository.findById(id1)).thenReturn(Optional.ofNullable(honor1));
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test(expected = ServiceValidationException.class)
	public void testDeleteWithPersonHonor() {
	  HonorService.delete(honor1.getId());
	}
}
