package at.simianarmy.service;

import static org.junit.Assert.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.repository.PersonInstrumentRepository;
import at.simianarmy.service.dto.PersonInstrumentDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonInstrumentMapper;

import java.time.LocalDate;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonInstrumentServiceTest {

  private PersonInstrument pi1;
  private PersonInstrumentDTO piDto1;



  @Mock
  private PersonInstrumentRepository personInstrumentRepository;
  
  @Spy
  @Inject
  private PersonInstrumentMapper personInstrumentMapper;
  
  @InjectMocks
  private PersonInstrumentService personInstrumentService;

  
  


  @Before
  public void setUp() throws Exception {
	 MockitoAnnotations.initMocks(this);
	
	 pi1 = new PersonInstrument();
	
	 	
	 Mockito.when(personInstrumentRepository.save(ArgumentMatchers.any(PersonInstrument.class))).then(AdditionalAnswers.returnsFirstArg());
  }

  @After
  public void tearDown() throws Exception {
    pi1 = null;

  }

  @Test(expected=ServiceValidationException.class)
  public void testBeginAfterEnd(){
	  pi1.setBeginDate(LocalDate.of(2016, 10, 15));
	  pi1.setEndDate(LocalDate.of(2016, 9, 15));
	  piDto1 = personInstrumentMapper.personInstrumentToPersonInstrumentDTO(pi1);
	  
	  personInstrumentService.save(piDto1);
	  
  }
  
  @Test
  public void testBeginBeforeEnd() {
	  PersonInstrumentDTO piDtol;
	  
	  pi1.setBeginDate(LocalDate.of(2016, 9, 15));
	  pi1.setEndDate(LocalDate.of(2016, 10, 15));
	  
	  piDto1 = personInstrumentMapper.personInstrumentToPersonInstrumentDTO(pi1);
	  
	  
	  piDtol = personInstrumentService.save(piDto1);
	  
	  assertThat(piDtol, Matchers.equalTo(piDto1));
  }
  
  @Test
  public void testBeginEqualsEnd() {
	  PersonInstrumentDTO piDtol;
	  
	  pi1.setBeginDate(LocalDate.of(2016, 9, 15));
	  pi1.setEndDate(LocalDate.of(2016, 9, 15));
	  
	  piDto1 = personInstrumentMapper.personInstrumentToPersonInstrumentDTO(pi1); 
	  
	  piDtol = personInstrumentService.save(piDto1);
	  
	  assertThat(piDtol, Matchers.equalTo(piDto1));
  }
  
  @Test
  public void testEndisNull() {
	  PersonInstrumentDTO piDtol;
	  
	  pi1.setBeginDate(LocalDate.of(2016, 9, 15));
	  pi1.setEndDate(null);
	  
	  piDto1 = personInstrumentMapper.personInstrumentToPersonInstrumentDTO(pi1); 
	  
	  piDtol = personInstrumentService.save(piDto1);
	  
	  assertThat(piDtol, Matchers.equalTo(piDto1));
	  
  }
  
 
}
