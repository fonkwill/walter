package at.simianarmy.service;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.PersonGroupMapper;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonGroupServiceTest {

  private static final Long id1 = new Long(1);
  private static final Long id2 = new Long(2);
  private static final Long id3 = new Long(3);

  private static final String name1 = "name1";
  private static final String name2 = "name2";
  private static final String name3 = "name3";

  private PersonGroup pg1;
  private PersonGroup pg2;
  private PersonGroup pg3;

  @Mock
  private PersonGroupRepository personGroupRepository;

  @Inject
  @Spy
  private PersonGroupMapper personGroupMapper;
  
  @InjectMocks
  private PersonGroupService personGroupService;



  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    pg1 = new PersonGroup();
    pg1.setId(id1);
    pg1.setName(name1);
    pg2 = new PersonGroup(); 
    pg2.setId(id2);
    pg2.setName(name2);
    pg3 = new PersonGroup();
    pg3.setId(id3);
    pg3.setName(name3);

    pg1.addChildren(pg2);
    pg2.addChildren(pg3);


    when(personGroupRepository.findById(id1)).thenReturn(Optional.ofNullable(pg1));
    when(personGroupRepository.findById(id2)).thenReturn(Optional.ofNullable(pg2));
    when(personGroupRepository.findById(id3)).thenReturn(Optional.ofNullable(pg3));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDeleteParentShouldBeNull() {
    personGroupService.delete(pg1.getId());
    assertThat(pg2.getParent(), nullValue());
    assertThat(pg3.getParent(), equalTo(pg2));
  }


  @Test
  public void testDelteParentShouldBeParentParent() {
    personGroupService.delete(pg2.getId());
    assertThat(pg3.getParent(), equalTo(pg1));
    assertThat(pg1.getParent(), nullValue());

  }

  @Test(expected = ServiceValidationException.class)
  public void testCircularReference() {
    pg3.addChildren(pg1);
//  pg3->pg1->pg2->pg3
//  Must not be saveable
    PersonGroupDTO pg1dto = personGroupMapper.personGroupToPersonGroupDTO(pg1);
    personGroupService.save(pg1dto);


  }

  @Test
  public void getAllPersonRelevantPersonGroupsWithParentRelevant_ReturnAll() {
    PersonGroup parent = new PersonGroup();
    parent.setName("Parent");
    parent.setHasPersonRelevance(true);

    PersonGroup child1 = new PersonGroup();
    child1.setName("Child1");
    child1.setHasPersonRelevance(false);
    child1.setParent(parent);
    parent.addChildren(child1);

    PersonGroup child2 = new PersonGroup();
    child2.setName("Child2");
    child2.setHasPersonRelevance(false);
    child2.setParent(parent);
    parent.addChildren(child2);

    PersonGroup child1child = new PersonGroup();
    child1child.setName("Child1Child");
    child1child.setHasPersonRelevance(false);
    child1child.setParent(child1);
    child1.addChildren(child1child);

    PersonGroup child2child = new PersonGroup();
    child2child.setName("Child2Child");
    child2child.setHasPersonRelevance(false);
    child2child.setParent(child2);
    child2.addChildren(child2child);

    List<PersonGroup> personRelevace = new ArrayList<>();
    personRelevace.add(parent);
    when(personGroupRepository.findByHasPersonRelevanceIsTrue()).thenReturn(personRelevace);

    List<PersonGroup> result = personGroupService.getAllPersonRelevantPersonGroups();

    assertThat(result, hasSize(5));
    assertThat(result, allOf( hasItem(parent), hasItem(child1), hasItem(child2), hasItem(child1child), hasItem(child2child) ));

  }

  @Test
  public void getAllPersonRelevantPersonGroupsWithChildRelevant_ReturnAll() {
    PersonGroup parent = new PersonGroup();
    parent.setName("Parent");
    parent.setHasPersonRelevance(false);

    PersonGroup child1 = new PersonGroup();
    child1.setName("Child1");
    child1.setHasPersonRelevance(true);
    child1.setParent(parent);
    parent.addChildren(child1);

    PersonGroup child2 = new PersonGroup();
    child2.setName("Child2");
    child2.setHasPersonRelevance(false);
    child2.setParent(parent);
    parent.addChildren(child2);

    PersonGroup child1child = new PersonGroup();
    child1child.setName("Child1Child");
    child1child.setHasPersonRelevance(false);
    child1child.setParent(child1);
    child1.addChildren(child1child);

    PersonGroup child2child = new PersonGroup();
    child2child.setName("Child2Child");
    child2child.setHasPersonRelevance(true);
    child2child.setParent(child2);
    child2.addChildren(child2child);

    List<PersonGroup> personRelevace = new ArrayList<>();
    personRelevace.add(child1);
    personRelevace.add(child2child);
    when(personGroupRepository.findByHasPersonRelevanceIsTrue()).thenReturn(personRelevace);

    List<PersonGroup> result = personGroupService.getAllPersonRelevantPersonGroups();

    assertThat(result, hasSize(3));
    assertThat(result, allOf(  hasItem(child1),  hasItem(child1child), hasItem(child2child) ));

  }

}
