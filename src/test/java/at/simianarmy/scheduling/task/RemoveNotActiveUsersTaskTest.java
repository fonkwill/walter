package at.simianarmy.scheduling.task;

import at.simianarmy.domain.User;
import at.simianarmy.repository.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class RemoveNotActiveUsersTaskTest {

  @Mock
  private UserRepository userRepository;

  @InjectMocks
  private RemoveNotActiveUsersTask sut;

  @Test
  public void testFindNotActivatedUsersByCreationDateBefore() {

    User userA = Mockito.mock(User.class);
    User userB = Mockito.mock(User.class);

    Mockito.when(this.userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(Mockito.any())).thenReturn(new ArrayList<User>(
      Arrays.asList(userA, userB)));

    this.sut.run();

    Mockito.verify(this.userRepository, Mockito.times(1)).delete(userA);
    Mockito.verify(this.userRepository, Mockito.times(1)).delete(userB);
  }

}
