package at.simianarmy.scheduling.task;

import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.repository.EmailVerificationLinkRepository;
import at.simianarmy.service.CustomizationService;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmailVerificationLinkTaskTest {

  @Mock
  private EmailVerificationLinkRepository emailVerificationLinkRepository;
  
  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private EmailVerificationLinkInvalidatorTask sut;
  
  @Before
  public void setup() {
	  MockitoAnnotations.initMocks(this);
	  Mockito.when(customizationService.getHoursToInvalidateVerificationLink()).thenReturn(new Long(48));
  }

  @Test
  public void testInvalidateVerificationLinks() {
    EmailVerificationLink link = Mockito.mock(EmailVerificationLink.class);
    Mockito.when(this.emailVerificationLinkRepository.findAllToInvalidate(Mockito.any())).thenReturn(new ArrayList<EmailVerificationLink>(
      Arrays.asList(link)));

    this.sut.run();

    Mockito.verify(link, Mockito.times(1)).setInvalid(true);
    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).save(link);
    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).flush();
  }

  @Test
  public void testNothingToInvalidate() {
    Mockito.when(this.emailVerificationLinkRepository.findAllToInvalidate(Mockito.any())).thenReturn(new ArrayList<EmailVerificationLink>());

    this.sut.run();

    Mockito.verify(this.emailVerificationLinkRepository, Mockito.times(1)).findAllToInvalidate(Mockito.any());
    Mockito.verifyNoMoreInteractions(this.emailVerificationLinkRepository);
  }

}
