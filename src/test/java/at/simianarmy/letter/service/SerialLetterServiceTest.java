package at.simianarmy.letter.service;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doCallRealMethod;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.PersonRepository;
import at.simianarmy.service.PersonService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SerialLetterServiceTest {

  @Mock
  private PersonService personService;

  @Mock
  private PersonRepository personRepository;

  @InjectMocks
  private SerialLetterService sut;

  @Before
  public void init() {
    doCallRealMethod().when(this.personService).getPersonGroupIdsForPersonGroup(Mockito.any(PersonGroup.class), Mockito.anyList());
  }

  @Test
  public void testGetRelevantPersons() {
    Letter letter = this.newLetterMock();
    Person personA = this.newPersonMock();
    Person personB = this.newPersonMock();

    Mockito.when(this.personRepository.findAll(Mockito.any(Specification.class)))
      .thenReturn(new ArrayList<>(
      Arrays.asList(personA, personB)
    ));

    List<Person> result = this.sut.getRelevantPersons(letter);

    assertThat(result.size(), Matchers.equalTo(2));
    assertThat(result, Matchers.containsInAnyOrder(personA, personB));
  }

  @Test
  public void testGetRelevantPersonsForMailLetter() {
    Letter letter = this.newLetterMock();
    Person personA = this.newPersonMock();
    Person personB = this.newPersonMock();

    Mockito.when(this.personRepository.findAll(Mockito.any(Specification.class)))
      .thenReturn(new ArrayList<>(
        Arrays.asList(personA, personB)
      ));

    Mockito.when(this.personRepository.findAllByActiveEmailAndPersonGroups(Mockito.any())).thenReturn(
      new ArrayList<>(
        Arrays.asList(personA)
      )
    );

    List<Person> result = this.sut.getRelevantPersonsForMailLetter(letter);

    assertThat(result.size(), Matchers.equalTo(1));
    assertThat(result, Matchers.containsInAnyOrder(personA));
  }

  @Test
  public void testPersonsWithInactiveMail() {
    Letter letter = this.newLetterMock();
    Person personA = this.newPersonMock();
    Person personB = this.newPersonMock();

    Mockito.when(this.personRepository.findAll(Mockito.any(Specification.class)))
      .thenReturn(new ArrayList<>(
        Arrays.asList(personA, personB)
      ));

    Mockito.when(this.personRepository.findAllByInActiveEmailAndPersonGroups(Mockito.any())).thenReturn(
      new ArrayList<>(
        Arrays.asList(personB)
      )
    );

    List<Person> result = this.sut.getPersonsWithInactiveMail(letter);

    assertThat(result.size(), Matchers.equalTo(1));
    assertThat(result, Matchers.containsInAnyOrder(personB));
  }

  private Letter newLetterMock() {
    Letter letter = Mockito.mock(Letter.class);

    return letter;
  }

  private Person newPersonMock() {
    Person person = Mockito.mock(Person.class);

    return person;
  }
}
