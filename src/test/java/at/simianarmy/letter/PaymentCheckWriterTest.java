package at.simianarmy.letter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class PaymentCheckWriterTest {

	private static final String testResources = "src/test/resources/serialLetters";
	private static final String testTemplates = "src/test/resources/letterTemplates";
	
	private PaymentCheckWriter paymentCheckWriter;
	
	@Before
	public void setup() {
		
	}

	
	@Test
	public void testWriteZahlungsreferenz() {
		// Create output PDF
		Document document = new Document(PageSize.A4, 0, 0, 0, 0);
		PdfWriter writer = null;
		
		String filePath = testResources + "/" + "Zahlungsreferenz.pdf";

		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
		} catch (FileNotFoundException | DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		document.open();
		
		//set the payment check as background
		PdfReader reader = null;
		try {
			reader = new PdfReader(testTemplates + "/" + "sepa.pdf");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfImportedPage check = writer.getImportedPage(reader, 1);
		writer.getDirectContent().addTemplate(check, 0, 0);
		
		paymentCheckWriter = new PaymentCheckWriter(PageSize.A4, writer);
		paymentCheckWriter.writeZahlungsreferenz("0123456789");
		paymentCheckWriter.writeVerwendungszweck("testZweckmit weniger Zeichen als die 80, genau sind es 57");
		paymentCheckWriter.writeVerwendungszweckInfo("testZeile2");
		paymentCheckWriter.writeSender("Hans Müller");
		document.close();
	}
	
	@Test
	public void testLongVerwendungszwekcInfo() {
		// Create output PDF
				Document document = new Document(PageSize.A4, 0, 0, 0, 0);
				PdfWriter writer = null;
				
				String filePath = testResources + "/" + "PaymentCheck_LongVerwendungszweckInfo.pdf";

				try {
					writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
				} catch (FileNotFoundException | DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				document.open();
				
				//set the payment check as background
				PdfReader reader = null;
				try {
					reader = new PdfReader(testTemplates + "/" + "sepa.pdf");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				PdfImportedPage check = writer.getImportedPage(reader, 1);
				writer.getDirectContent().addTemplate(check, 0, 0);
				
				paymentCheckWriter = new PaymentCheckWriter(PageSize.A4, writer);
				paymentCheckWriter.writeZahlungsreferenz("0123456789");
				paymentCheckWriter.writeVerwendungszweck("Mitgliedsbeitrag");
				paymentCheckWriter.writeVerwendungszweckInfo("Das sind genau 100 Zeichen, daher sollte alles sichtbar sein. Also genauer 100 Zeichen. Nicht mehr.");
				document.close();
	}
	
	@Test
	public void testTooLongVerwendungszwekcInfo() {
		// Create output PDF
				Document document = new Document(PageSize.A4, 0, 0, 0, 0);
				PdfWriter writer = null;
				
				String filePath = testResources + "/" + "PaymentCheck_TooLongVerwendungszweckInfo.pdf";

				try {
					writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
				} catch (FileNotFoundException | DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				document.open();
				
				//set the payment check as background
				PdfReader reader = null;
				try {
					reader = new PdfReader(testTemplates + "/" + "sepa.pdf");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				PdfImportedPage check = writer.getImportedPage(reader, 1);
				writer.getDirectContent().addTemplate(check, 0, 0);
				
				paymentCheckWriter = new PaymentCheckWriter(PageSize.A4, writer);
				paymentCheckWriter.writeZahlungsreferenz("0123456789");
				paymentCheckWriter.writeVerwendungszweck("Mitgliedsbeitrag");
				paymentCheckWriter.writeVerwendungszweckInfo("Das sind mehr als 100 Zeichen, daher sollte nicht alles sichtbar sein. Also genauer 100 Zeichen. Nicht mehr.");
				document.close();
	}


}


