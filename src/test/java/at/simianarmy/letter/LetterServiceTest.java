package at.simianarmy.letter;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Letter;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.Template;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.service.TemplateFileStoreService;
import at.simianarmy.letter.service.SerialLetterService;
import at.simianarmy.letter.service.SerialLetterServiceFactory;
import at.simianarmy.multitenancy.TenantIdentiferResolver;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.LetterService;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.exception.ServiceValidationException;
import at.simianarmy.service.mapper.LetterMapper;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class LetterServiceTest {

  private static final String testResources = "src/test/resources/serialLetters";

  private static final String testTemplate = "testTemplate.pdf";

  private static final String testTemplateHeader = "KOPFZEILE TEST";
  private static final String testTemplateFooter = "FUSSZEILE TEST";


  private static final UUID nonExistingPdf = UUID.randomUUID();

  private PDDocument tester;

  @Mock
  private TemplateFileStoreService fileStore;

  @Mock
  private TemplateRepository templateRepository;

  @Spy
  private TaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();

  @Mock
  private PersonGroupRepository personGroupRepository;

  @Mock
  private SerialLetterServiceFactory serialLetterServiceFactory;

  @Mock
  private SerialLetterService serialLetterService;

  @Spy
  @Inject
  private LetterMapper letterMapper;

  @Spy
  @Inject
  private SerialLetterFactory serialLetterFactory;

  @Spy
  @Inject
  private TenantIdentiferResolver tenantIdentiferResolver;

  @InjectMocks
  private LetterService letterService;

  private Letter l1;
  private Letter l2;
  private Letter l3;
  private Letter l4;

  private PersonGroup pg1;
  private PersonGroup pg2;
  private PersonGroup pg3;

  private Person p1;
  private Person p2;

  private String firstName1 = "Hans";
  private String lastName1 = "Müller";
  private String streetAddress1 = "Landstraßer Hauptstraße 16";
  private String postalCode1 = "1030";
  private String city1 = "Wien";
  private String country1 = "Österreich";

  private String firstName2 = "Maria";
  private String lastName2 = "Koller-Huber";
  private String streetAddress2 = "Untere Gasse die am See ist und ziemlich lang ist 1500";
  private String postalCode2 = "7110";
  private String city2 = "Neusiedl am Neusiedler See";
  private String country2 = "Österreich";

  private String nonHTMLText =
    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt "
      + "ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. "
      + "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. " +
      "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.";


  private String htmlTextDirty =
    "<h2></h2><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor "
      + "	invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. "
      + "quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   </p><p><br/></p><p><br/></p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   </p><p><br/></p><p><br/></p><p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. "
      + "Lorem ipsum dolor sit amet, consetetur</p><p><a href=\"https://github.com/fraywing/textAngular\"></a> </p>";

  private String htmlTextDirtyLittle = "<h2></h2><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor ";

  private String htmlTextDirtySeveralPages =
    "<h2></h2><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor "
      + "	invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. "
      + "Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.   </p><p><br/></p><p><br/></p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer "
      + "adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.   </p><p><br/></p><p><br/></p><p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto "
      + "odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.   </p><p><br/></p><p><br/></p><p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, "
      + "quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.   </p><p><br/></p><p><br/></p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.   </p><p><br/></p><p><br/></p><p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. "
      + "Lorem ipsum dolor sit amet, consetetur</p><p><a href=\"https://github.com/fraywing/textAngular\"></a> </p>";

  private String htmlTextClean = "<!DOCTYPE html>" +
    "<html>" +
    "<head>" +
    "	<title></title>" +
    "</head>" +
    "<body>" +
    "	<h2></h2>" +
    "	<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>"
    +
    "	<p><br></p>" +
    "	<p><br></p>" +
    "	<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis.</p>"
    +
    "	<p><br></p>" +
    "	<p><br></p>" +
    "	<p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur</p>"
    +
    "	<p><a href=\"https://github.com/fraywing/textAngular\"></a></p>" +
    "</body>" +
    "</html>";


  private Resource generatedTestTemplate = getTestTemplate();

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    p1 = new Person();
    p1.setId(new Long(1));
    p1.setFirstName(firstName1);
    p1.setLastName(lastName1);
    p1.setStreetAddress(streetAddress1);
    p1.setPostalCode(postalCode1);
    p1.setCity(city1);
    p1.setCountry(country1);
    p1.setGender(Gender.MALE);

    p2 = new Person();
    p2.setId(new Long(2));
    p2.setFirstName(firstName2);
    p2.setLastName(lastName2);
    p2.setStreetAddress(streetAddress2);
    p2.setPostalCode(postalCode2);
    p2.setCity(city2);
    p2.setCountry(country2);
    p2.setGender(Gender.FEMALE);

    pg1 = new PersonGroup();
    pg2 = new PersonGroup();
    pg3 = new PersonGroup();

    Long id1 = new Long(1);
    Long id2 = new Long(2);
    Long id3 = new Long(3);

    pg1.setId(id1);
    pg2.setId(id2);
    pg3.setId(id3);

    pg1.addPersons(p1);

    pg2.addPersons(p1);
    pg2.addPersons(p2);

    l1 = new Letter();
    l1.setId(1L);
    l1.setLetterType(LetterType.SERIAL_LETTER);
    l2 = new Letter();
    l2.setLetterType(LetterType.SERIAL_LETTER);
    l2.setId(2L);
    l3 = new Letter();
    l3.setLetterType(LetterType.SERIAL_LETTER);
    l3.setId(3L);
    l4 = new Letter();
    l4.setLetterType(LetterType.SERIAL_LETTER);
    l4.setId(4L);

    l1.addPersonGroup(pg1);
    l2.addPersonGroup(pg2);
    l3.addPersonGroup(pg1);
    l3.addPersonGroup(pg2);
    l4.addPersonGroup(pg3);

    Mockito.when(this.serialLetterServiceFactory.createForLetter(Mockito.any(Letter.class))).thenReturn(serialLetterService);

    Mockito.when(serialLetterService.getRelevantPersons(l1))
      .thenReturn(new ArrayList<>(pg1.getPersons()));
    Mockito.when(serialLetterService.getRelevantPersons(l2))
      .thenReturn(new ArrayList<>(pg2.getPersons()));
    List<Person> pg1l = new ArrayList<>(pg1.getPersons());
    pg1l.addAll(pg2.getPersons());
    //serialDataProvider makes distinct lists
    pg1l = new ArrayList<>(new HashSet<>(pg1l));
    Mockito.when(serialLetterService.getRelevantPersons(l3)).thenReturn(pg1l);
    Mockito.when(serialLetterService.getRelevantPersons(l4))
      .thenReturn(new ArrayList<>(pg3.getPersons()));

    Mockito.when(serialLetterService.getTemplatePage(ArgumentMatchers.any()))
      .thenReturn(generatedTestTemplate);

    Mockito.when(fileStore.getTemplate(ArgumentMatchers.any()))
      .thenReturn(new FileResourceDTO(generatedTestTemplate, testTemplate));
    Mockito.when(fileStore.getTemplate(nonExistingPdf))
      .thenThrow(new FileStoreException("Exception"));

    Mockito.when(personGroupRepository.getOne(id1)).thenReturn(pg1);
    Mockito.when(personGroupRepository.getOne(id2)).thenReturn(pg2);
    Mockito.when(personGroupRepository.getOne(id3)).thenReturn(pg3);

    Mockito.doCallRealMethod().when(taskExecutor).execute(ArgumentMatchers.any(Runnable.class));

  }

  @After
  public void tearDown() throws Exception {
    if (tester != null) {
      tester.close();
    }
  }

  @Test
  public void testWithCleanHTML_OnePage() throws FileNotFoundException {

    File f = makeTestFile("OnePage_clean.pdf");

    Letter letter = l1;
    letter.setText(htmlTextClean);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(letter));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
//		tester.close();

  }

  @Test
  public void testWithDirtyHTML_OnePage() throws FileNotFoundException {
    File f = makeTestFile("OnePage_dirty.pdf");

    Letter letter = l1;
    letter.setText(htmlTextDirty);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l1));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  @Test
  public void testWithText_OnePage() throws FileNotFoundException {
    File f = makeTestFile("OnePage_text.pdf");

    Letter letter = l1;
    letter.setText(nonHTMLText);

    LetterDTO letterDTO = letterMapper.letterToLetterDTO(letter);

    Resource is = letterService.generate(letterDTO);

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {
      assertThat(tester.getNumberOfPages(), Matchers.equalTo(1));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));

    } else {
      fail();
    }

  }

  @Test
  public void testWithDirtyHTML_SeveralPages() throws FileNotFoundException {
    File f = makeTestFile("SeveralPages_dirty.pdf");

    Letter letter = l1;
    letter.setText(htmlTextDirtySeveralPages);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l1));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }
    if (tester != null && text != null) {
      assertThat(tester.getNumberOfPages(), Matchers.equalTo(2));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
    } else {
      fail();
    }


  }

  @Test
  public void testWithDirtyHTML_OnePageSeveralRecipients() throws FileNotFoundException {
    File f = makeTestFile("OnePageSeveralRecipients_dirty.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirty);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(2));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }


  }

  @Test
  public void testWithDirtyHTML_OneSmallPageSeveralRecipients() throws FileNotFoundException {
    File f = makeTestFile("OneSmallPageSeveralRecipients_dirty.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirtyLittle);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(2));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }


  }

  @Test
  public void testWithDirtyHTML_SeveralPagesAndSeveralRecipients() throws FileNotFoundException {
    File f = makeTestFile("SeveralPagesSeveralRecipients_dirty.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirtySeveralPages);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(4));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }

  }

  @Test
  public void testWithDirtyHTML_SeveralPagesAndSeveralPersonGroupsWithTheSamePerson()
    throws FileNotFoundException {
    File f = makeTestFile("SeveralPagesSeveralPersonGroups_dirty.pdf");

    Letter letter = l3;
    letter.setText(htmlTextDirtySeveralPages);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l3));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(4));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }
  }

  @Test(expected = ServiceValidationException.class)
  public void testWithNoPersons() {
    Letter letter = l4;
    letter.addPersonGroup(pg3);

    letterService.generate(letterMapper.letterToLetterDTO(l4));

  }


  private PDDocument writeTestFile(InputStream is, File f) {
    FileOutputStream fos = null;
    PDDocument pdfDocument = null;
    try {
      fos = new FileOutputStream(f);
      IOUtils.copy(is, fos);
      pdfDocument = PDDocument.load(f);


    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    try {
      is.close();
      fos.close();
    } catch (Exception e) {
      // ignore
    }

    return pdfDocument;

  }

  @Test
  public void testWithTemplate_OnePage() {
    File f = makeTestFile("OnePageSeveralRecipients_withTemplate.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirty);

    Template template = new Template();
    template.setId(1L);
    template.setFile("abc.pdf");
    letter.setTemplate(template);

    Mockito.when(templateRepository.findById(template.getId()))
      .thenReturn(Optional.ofNullable(template));
    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(2));
      assertThat(text, Matchers.containsString(testTemplateHeader));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }

  }

  @Test
  public void testWithBackgroundTemplate() {

  }

  @Test
  public void testWithTemplate_SeveralPagesAndSeveralRecipients() throws FileNotFoundException {
    File f = makeTestFile("SeveralPagesSeveralRecipients_withTemplate.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirtySeveralPages);

    Template template = new Template();
    template.setId(1L);
    template.setFile("abc.pdf");
    letter.setTemplate(template);
    Mockito.when(templateRepository.findById(template.getId()))
      .thenReturn(Optional.ofNullable(template));
    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(4));
      assertThat(text, Matchers.containsString(testTemplateHeader));
      assertThat(text, Matchers.containsString(testTemplateFooter));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));

    } else {
      fail();
    }

  }

  @Test
  public void testWithTemplateMargins_OnePage() throws FileNotFoundException {
    File f = makeTestFile("OnePage_withTemplateMargins.pdf");

    Letter letter = l1;
    letter.setText(htmlTextDirtyLittle);

    Template template = new Template();
    template.setId(1L);
    template.setMarginBottom(new Float(10));
    template.setMarginTop(new Float(10));
    template.setMarginLeft(new Float(10));
    template.setMarginRight(new Float(10));

    letter.setTemplate(template);
    Mockito.when(templateRepository.findById(template.getId()))
      .thenReturn(Optional.ofNullable(template));
    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l1));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }

    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      //check manually if margins are very large
      assertThat(tester.getNumberOfPages(), Matchers.equalTo(6));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));

    } else {
      fail();
    }

  }

  @Test
  public void testReplacingVariables() throws FileNotFoundException {
    File f = makeTestFile("testReplacingVariables.pdf");

    Letter letter = l1;
    String text = "Sie müssen %BETRAG% einzahlen";

    letter.setText(text);
    letter.setLetterType(LetterType.DEMAND_LETTER);
    letter.setPayment(true);
    letter.setCurrentPeriod(true);

    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l1));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String texta = null;
    try {
      texta = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && texta != null) {

      assertThat(tester.getNumberOfPages(), Matchers.equalTo(1));
      assertThat(texta, Matchers.not(Matchers.containsString("%BETRAG%")));
      assertThat(texta, Matchers.not(Matchers.containsString("%REFERENZ%")));

      assertThat(texta, Matchers.containsString(streetAddress1));
      assertThat(texta, Matchers.containsString(postalCode1));
      assertThat(texta, Matchers.containsString(city1));

    } else {
      fail();
    }

  }

  @Test(expected = ServiceValidationException.class)
  public void testNonePeriodSelected() {
    Letter letter = new Letter();
    letter.setLetterType(LetterType.DEMAND_LETTER);
    letter.setPayment(true);

    letterService.generate(letterMapper.letterToLetterDTO(letter));


  }

  @Test
  public void testWithTemplateAndPayment_SeveralPagesAndSeveralRecipients()
    throws FileNotFoundException {
    File f = makeTestFile("SeveralPagesSeveralRecipientsAndPayment_withTemplate.pdf");

    Letter letter = l2;
    letter.setText(htmlTextDirtySeveralPages);

    Template template = new Template();
    template.setId(1L);
    template.setFile("abc.pdf");
    letter.setTemplate(template);
    letter.setLetterType(LetterType.DEMAND_LETTER);
    letter.setPayment(true);
    letter.setCurrentPeriod(true);
    Mockito.when(templateRepository.findById(template.getId()))
      .thenReturn(Optional.ofNullable(template));
    Resource is = letterService.generate(letterMapper.letterToLetterDTO(l2));

    try {
      tester = writeTestFile(((InputStreamResource) is).getInputStream(), f);
    } catch (Exception e) {
      e.printStackTrace();
    }
    String text = null;
    try {
      text = new PDFTextStripper().getText(tester);
    } catch (IOException e) {

    }

    if (tester != null && text != null) {

      //check manually if margins are very large
      assertThat(tester.getNumberOfPages(), Matchers.equalTo(4));
      assertThat(text, Matchers.containsString(testTemplateHeader));
      assertThat(text, Matchers.containsString(testTemplateFooter));
      assertThat(text, Matchers.containsString(firstName1));
      assertThat(text, Matchers.containsString(lastName1));
      assertThat(text, Matchers.containsString(streetAddress1));
      assertThat(text, Matchers.containsString(postalCode1));
      assertThat(text, Matchers.containsString(city1));
      assertThat(text, Matchers.containsString(firstName2));
      assertThat(text, Matchers.containsString(lastName2));
      assertThat(text, Matchers.containsString(streetAddress2));
      assertThat(text, Matchers.containsString(postalCode2));
      assertThat(text, Matchers.containsString(city2));


    } else {
      fail();
    }

  }


  private File makeTestFile(String string) {
    String path = testResources + "/" + string;
    File f = new File(path);
    f.getParentFile().mkdirs();
    try {
      f.createNewFile();
    } catch (Exception e) {
      fail();
      e.printStackTrace();
    }
    return f;
  }

  private Resource getTestTemplate() {
    String templatePath = testResources + "/" + testTemplate;
    FileOutputStream fo = null;
    try {
      fo = new FileOutputStream(templatePath);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    Document document = new Document(PageSize.A4, 0, 0, 0, 0);
    PdfWriter writer = null;
    try {
      writer = PdfWriter.getInstance(document, fo);
    } catch (DocumentException e1) {
      e1.printStackTrace();
    }
    document.open();
    PdfContentByte cb = writer.getDirectContent();

    cb.beginText();
    try {
      cb.setFontAndSize(BaseFont.createFont(BaseFont.TIMES_BOLD, // Font
        // name
        BaseFont.CP1257, // Font encoding
        BaseFont.EMBEDDED // Font embedded
      ), 12);
    } catch (DocumentException | IOException e) {
      e.printStackTrace();
    }

    cb.setTextMatrix(200, 700);
    cb.showText(testTemplateHeader);

    cb.setTextMatrix(200, 100);
    cb.showText(testTemplateFooter);

    cb.endText();

//		draw background
    cb.setRGBColorFill(255, 64, 64);
    cb.rectangle(200, 50, 400, 800);
    cb.fill();

    document.close();
    try {
      fo.close();
    } catch (IOException e) {
      // ignore
    }

    File f = new File(templatePath);

    Resource resource = null;
    try {
      resource = new UrlResource(f.toPath().toUri());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
    return resource;

  }

}
