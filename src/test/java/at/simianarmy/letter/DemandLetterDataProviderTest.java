package at.simianarmy.letter;

import static org.junit.Assert.*;

import at.simianarmy.letter.dataprovider.DemandLetterDataProvider;
import org.hamcrest.Matchers;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.simianarmy.config.SerialLetterConstants;
import at.simianarmy.domain.Letter;
import at.simianarmy.service.exception.SerialLetterGenerationException;

public class DemandLetterDataProviderTest {
	
	private Letter l1;
	
	private DemandLetterDataProvider demandLetterDataProvider;
	
	@Before
	public void setUp() throws Exception {
		demandLetterDataProvider = new DemandLetterDataProvider();
		l1 = new Letter();
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test(expected=SerialLetterGenerationException.class)
	public void testGetCurrentPeriod_current() {
		
		l1.setPreviousPeriod(false);
		l1.setCurrentPeriod(false);
		
		demandLetterDataProvider.setLetter(l1);
		demandLetterDataProvider.getCurrentPeriod();
		
	}
	
	@Test
	public void testGetCurrentPeriod_previous() {
		l1.setPreviousPeriod(true);
		l1.setCurrentPeriod(false);
		
		demandLetterDataProvider.setLetter(l1);
		assertThat(demandLetterDataProvider.getCurrentPeriod(), Matchers.equalTo(SerialLetterConstants.previousPeriods));
	}
	
	@Test
	public void testGetCurrentPeriod_currentAndPrevious() {
		l1.setPreviousPeriod(false);
		l1.setCurrentPeriod(true);
		
		LocalDate now = LocalDate.now();
		
		demandLetterDataProvider.setLetter(l1);
		assertThat(demandLetterDataProvider.getCurrentPeriod(), Matchers.equalTo(""+now.getYear()));
		
	}
	
	@Test
	public void testGetCurrentPeriod_None() {
		l1.setPreviousPeriod(true);
		l1.setCurrentPeriod(true);
		
		LocalDate now = LocalDate.now();
		
		demandLetterDataProvider.setLetter(l1);
		assertThat(demandLetterDataProvider.getCurrentPeriod(), Matchers.equalTo(""+now.getYear()));
		
	}

}
