package at.simianarmy.filestore.impl.local;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreTest;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public class LocalFileStoreTest extends FileStoreTest {

  private final static String LOCATION = "src/test/resources/filestore/test_files/";

  @Mock
  private FileStoreMetadataRepository fileStoreMetadataRepository;

  @InjectMocks
  private LocalFileStore sut;

  @Before
  public void init() throws Exception {
    Files.createDirectory(Paths.get(LOCATION));
    this.sut = new LocalFileStore(this.fileStoreLocationService, this.fileStoreMetadataRepository);
    Mockito.when(this.fileStoreLocationService.getRoot()).thenReturn(LOCATION);
    super.init();
  }

  @After
  public void tearDown() throws IOException {
    FileSystemUtils.deleteRecursively(Paths.get(LOCATION));
  }

  @Test
  public void testInitStorageLocations() {
    this.assertExists(this.fileStoreLocationService.getReceiptsLocation());
    this.assertExists(this.fileStoreLocationService.getTemplatesLocation());
    this.assertExists(this.fileStoreLocationService.getDocumentsLocation());
  }

  @Override
  protected FileStore getSut() {
    return this.sut;
  }

  @Override
  protected void assertExists(String path) {
    assertTrue(Files.exists(Paths.get(LOCATION + "/" + path)));
  }

  @Override
  protected void assertNotExists(String path) {
    assertFalse(Files.exists(Paths.get(LOCATION + "/" + path)));
  }

  @Override
  protected void assertFileEquals(MultipartFile expectedFile, String actualFilePath)
    throws IOException {
    String expectedContent = new String(expectedFile.getBytes());
    String actualContent = new String(Files.readAllBytes(Paths.get(LOCATION + "/" + actualFilePath)));

    assertEquals(expectedContent, actualContent);
  }

  @Override
  protected FileStoreMetadataRepository getFileStoreMetadataRepository() {
    return this.fileStoreMetadataRepository;
  }

  @Override
  protected void createTestFile(MultipartFile testFile, String path, String fileName) throws IOException {
    Files.copy(testFile.getInputStream(), Paths.get(this.fileStoreLocationService.getRoot() + "/" + path + "/" + fileName));
  }

  protected void createTestDirectory(String path) throws IOException {
    Files.createDirectory(Paths.get(this.fileStoreLocationService.getRoot() + "/" + path));
  }
}
