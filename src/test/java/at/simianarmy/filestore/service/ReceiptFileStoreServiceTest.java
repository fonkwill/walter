package at.simianarmy.filestore.service;

import static org.mockito.Mockito.verify;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReceiptFileStoreServiceTest {

  private static final String DEFAULT_LOC = "receipts/";
  private static final UUID DEFAULT_UUID = UUID.randomUUID();

  @Mock
  private MultipartFile file;

  @Mock
  private FileStore fileStore;

  @Mock
  private FileStoreLocationService fileStoreLocationService;

  @InjectMocks
  private ReceiptFileStoreService sut;


  @Before
  public void init() {
    Mockito.when(this.fileStoreLocationService.getReceiptsLocation()).thenReturn(DEFAULT_LOC);
  }

  @Test
  public void testGetReceipt() throws FileStoreException {
    this.sut.getReceipt(DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).getFile(DEFAULT_UUID, DEFAULT_LOC);
  }

  @Test
  public void testSaveReceipt() throws FileStoreException {
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStore.saveFile(this.file, DEFAULT_LOC, StorageType.RECEIPT)).thenReturn(metadata);
    this.sut.saveReceipt(this.file);

    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, DEFAULT_LOC, StorageType.RECEIPT);
  }

  @Test
  public void testDeleteReceipt() throws FileStoreException {
    this.sut.deleteReceipt(DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).deleteFile(DEFAULT_UUID, DEFAULT_LOC);
  }

}
