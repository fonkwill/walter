package at.simianarmy.filestore.service;

import static org.mockito.Mockito.verify;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public class TemplateFileStoreServiceTest {

  private static final String DEFAULT_LOC = "templates/";
  private static final UUID DEFAULT_UUID = UUID.randomUUID();

  @Mock
  private MultipartFile file;

  @Mock
  private FileStore fileStore;

  @Mock
  private FileStoreLocationService fileStoreLocationService;

  @InjectMocks
  private TemplateFileStoreService sut;


  @Before
  public void init() {
    Mockito.when(this.fileStoreLocationService.getTemplatesLocation()).thenReturn(DEFAULT_LOC);
  }

  @Test
  public void testGetTemplate() throws FileStoreException {
    this.sut.getTemplate(DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).getFile(DEFAULT_UUID, DEFAULT_LOC);
  }

  @Test
  public void testSaveReceipt() throws FileStoreException {
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStore.saveFile(this.file, DEFAULT_LOC, StorageType.TEMPLATE)).thenReturn(metadata);
    this.sut.saveTemplate(this.file);

    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, DEFAULT_LOC, StorageType.TEMPLATE);
  }

  @Test
  public void testDeleteReceipt() throws FileStoreException {
    this.sut.deleteTemplate(DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).deleteFile(DEFAULT_UUID, DEFAULT_LOC);
  }

}
