package at.simianarmy.filestore.service;

import static org.mockito.Mockito.verify;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.FileStoreMetadataDTO;
import at.simianarmy.filestore.mapper.FileStoreMetadataMapper;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.service.exception.ServiceValidationException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public class DocumentsFileStoreServiceTest {

  private static final UUID DEFAULT_UUID = UUID.randomUUID();
  private static final UUID DEFAULT_UUID2 = UUID.randomUUID();
  private static final String DEFAULT_FILENAME = "test.pdf";
  private static final String DEFAULT_RENAMED_FILENAME = "test(1).pdf";
  private static final String DEFAULT_RERENAMED_FILENAME = "test(2).pdf";
  private static final String DEFAULT_FOLDERNAME = "folder";
  private static final String DEFAULT_RENAMED_FOLDERNAME = "folder(1)";
  private static final String DEFAULT_RERENAMED_FOLDERNAME = "folder(2)";
  private static final String DEFAULT_LOC = "documents/";
  private static final ZonedDateTime DEFAULT_TIME = ZonedDateTime.now();
  private static final FileStoreMetadata DEFAULT_PARENT = Mockito.mock(FileStoreMetadata.class);

  @Mock
  private FileStoreLocationService fileStoreLocationService;

  @Mock
  private FileStore fileStore;

  @Mock
  private FileStoreMetadataRepository fileStoreMetadataRepository;

  @Mock
  private FileStoreMetadataMapper fileStoreMetadataMapper;

  @Mock
  private MultipartFile file;

  @InjectMocks
  private DocumentsFileStoreService sut;

  @Before
  public void init() {
    this.file = Mockito.mock(MultipartFile.class);
    Mockito.when(this.file.getOriginalFilename()).thenReturn(DEFAULT_FILENAME);
    Mockito.when(this.fileStoreLocationService.getDocumentsLocation()).thenReturn(DEFAULT_LOC);
  }

  @Test
  public void testSaveDocument() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FILENAME)).thenReturn(false);
    this.sut.saveDocument(this.file, null);
    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, null, DEFAULT_LOC, DEFAULT_FILENAME, StorageType.DOCUMENT);
  }

  @Test
  public void testSaveDocumentAndRename() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FILENAME)).thenReturn(true);
    this.sut.saveDocument(this.file, null);
    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, null, DEFAULT_LOC, DEFAULT_RENAMED_FILENAME, StorageType.DOCUMENT);
  }

  @Test
  public void testSaveDocumentAndRenameTwice() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FILENAME)).thenReturn(true);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_RENAMED_FILENAME)).thenReturn(true);
    this.sut.saveDocument(this.file, null);
    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, null, DEFAULT_LOC, DEFAULT_RERENAMED_FILENAME, StorageType.DOCUMENT);
  }

  @Test
  public void testSaveDocumentInSubDir() throws FileStoreException {
    FileStoreMetadataDTO parentDTO = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata parent = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(parent.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStoreMetadataRepository.findById(DEFAULT_UUID)).thenReturn(Optional.ofNullable(parent));
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(parentDTO)).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(parent, StorageType.DOCUMENT, DEFAULT_FILENAME)).thenReturn(false);

    this.sut.saveDocument(this.file, DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, DEFAULT_UUID, DEFAULT_LOC, DEFAULT_FILENAME, StorageType.DOCUMENT);
  }

  @Test
  public void testSaveDocumentAndRenameInSubDir() throws FileStoreException {
    FileStoreMetadataDTO parentDTO = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata parent = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(parent.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStoreMetadataRepository.findById(DEFAULT_UUID)).thenReturn(Optional.ofNullable(parent));
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(parentDTO)).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(parent, StorageType.DOCUMENT, DEFAULT_FILENAME)).thenReturn(true);

    this.sut.saveDocument(this.file, DEFAULT_UUID);

    verify(this.fileStore, Mockito.times(1)).saveFile(this.file, DEFAULT_UUID, DEFAULT_LOC, DEFAULT_RENAMED_FILENAME, StorageType.DOCUMENT);
  }

  @Test
  public void testCreateDirectory() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FOLDERNAME)).thenReturn(false);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getOriginalFile()).thenReturn(DEFAULT_FOLDERNAME);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadata);
    this.sut.createDirectory(dto);
    verify(this.fileStore, Mockito.times(1)).createDirectory(DEFAULT_FOLDERNAME, null, DEFAULT_LOC, StorageType.DOCUMENT);
  }

  @Test
  public void testCreateDirectoryAndRename() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FOLDERNAME)).thenReturn(true);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_RENAMED_FOLDERNAME)).thenReturn(false);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getOriginalFile()).thenReturn(DEFAULT_FOLDERNAME);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadata);
    this.sut.createDirectory(dto);
    verify(this.fileStore, Mockito.times(1)).createDirectory(DEFAULT_RENAMED_FOLDERNAME, null, DEFAULT_LOC, StorageType.DOCUMENT);
  }

  @Test
  public void testCreateDirectoryAndRenameTwice() throws FileStoreException {
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_FOLDERNAME)).thenReturn(true);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(null, StorageType.DOCUMENT, DEFAULT_RENAMED_FOLDERNAME)).thenReturn(true);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getOriginalFile()).thenReturn(DEFAULT_FOLDERNAME);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadata);
    this.sut.createDirectory(dto);
    verify(this.fileStore, Mockito.times(1)).createDirectory(DEFAULT_RERENAMED_FOLDERNAME, null, DEFAULT_LOC, StorageType.DOCUMENT);
  }

  @Test
  public void testCreateDirectoryInSubDir() throws FileStoreException {
    FileStoreMetadataDTO parentDTO = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata parent = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(parent.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(parentDTO)).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(parent, StorageType.DOCUMENT, DEFAULT_FOLDERNAME)).thenReturn(false);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getOriginalFile()).thenReturn(DEFAULT_FOLDERNAME);
    Mockito.when(metadata.getParent()).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadata);

    this.sut.createDirectory(dto);

    verify(this.fileStore, Mockito.times(1)).createDirectory(DEFAULT_FOLDERNAME, DEFAULT_UUID, DEFAULT_LOC, StorageType.DOCUMENT);
  }

  @Test
  public void testCreateDirectoryAndRenameInSubDir() throws FileStoreException {
    FileStoreMetadataDTO parentDTO = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata parent = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(parent.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(parentDTO)).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(parent, StorageType.DOCUMENT, DEFAULT_FOLDERNAME)).thenReturn(true);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getOriginalFile()).thenReturn(DEFAULT_FOLDERNAME);
    Mockito.when(metadata.getParent()).thenReturn(parent);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadata);

    this.sut.createDirectory(dto);

    verify(this.fileStore, Mockito.times(1)).createDirectory(DEFAULT_RENAMED_FOLDERNAME, DEFAULT_UUID, DEFAULT_LOC, StorageType.DOCUMENT);
  }

  @Test
  public void testRenameFile() {
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadataParameter = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataParameter.getOriginalFile()).thenReturn("New_File.pdf");
    Mockito.when(metadataParameter.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataParameter.getExtension()).thenReturn("pdf");
    Mockito.when(metadataParameter.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataParameter.getParent()).thenReturn(null);
    Mockito.when(metadataParameter.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataParameter.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadataParameter);

    FileStoreMetadata metadataToUpdate = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataToUpdate.getOriginalFile()).thenReturn("Old_File.txt");
    Mockito.when(metadataToUpdate.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataToUpdate.getExtension()).thenReturn("txt");
    Mockito.when(metadataToUpdate.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataToUpdate.getParent()).thenReturn(null);
    Mockito.when(metadataToUpdate.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataToUpdate.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataRepository.getOne(DEFAULT_UUID)).thenReturn(metadataToUpdate);

    this.sut.updateMetadata(dto);

    verify(metadataToUpdate, Mockito.times(1)).setOriginalFile("New_File.pdf");
  }

  @Test
  public void testRenameDirectory() {
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadataParameter = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataParameter.getOriginalFile()).thenReturn("New_Dir");
    Mockito.when(metadataParameter.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataParameter.getExtension()).thenReturn(null);
    Mockito.when(metadataParameter.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataParameter.getParent()).thenReturn(null);
    Mockito.when(metadataParameter.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataParameter.getType()).thenReturn(FileStoreMetadataType.DIRECTORY);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadataParameter);

    FileStoreMetadata metadataToUpdate = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataToUpdate.getOriginalFile()).thenReturn("Old_Dir");
    Mockito.when(metadataToUpdate.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataToUpdate.getExtension()).thenReturn(null);
    Mockito.when(metadataToUpdate.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataToUpdate.getParent()).thenReturn(null);
    Mockito.when(metadataToUpdate.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataToUpdate.getType()).thenReturn(FileStoreMetadataType.DIRECTORY);
    Mockito.when(this.fileStoreMetadataRepository.getOne(DEFAULT_UUID)).thenReturn(metadataToUpdate);

    this.sut.updateMetadata(dto);

    verify(metadataToUpdate, Mockito.times(1)).setOriginalFile("New_Dir");
    verify(metadataToUpdate, Mockito.never()).setExtension(Mockito.any());

  }

  @Test
  public void testRenameFileAndRenameNewName() {
    Mockito.when(DEFAULT_PARENT.getId()).thenReturn(DEFAULT_UUID2);
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadataParameter = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataParameter.getOriginalFile()).thenReturn("New_File.pdf");
    Mockito.when(metadataParameter.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataParameter.getExtension()).thenReturn("pdf");
    Mockito.when(metadataParameter.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataParameter.getParent()).thenReturn(DEFAULT_PARENT);
    Mockito.when(metadataParameter.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataParameter.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadataParameter);

    FileStoreMetadata metadataToUpdate = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataToUpdate.getOriginalFile()).thenReturn("Old_File.txt");
    Mockito.when(metadataToUpdate.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataToUpdate.getExtension()).thenReturn("txt");
    Mockito.when(metadataToUpdate.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataToUpdate.getParent()).thenReturn(DEFAULT_PARENT);
    Mockito.when(metadataToUpdate.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataToUpdate.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataRepository.getOne(DEFAULT_UUID)).thenReturn(metadataToUpdate);

    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(DEFAULT_PARENT, StorageType.DOCUMENT, "New_File.pdf")).thenReturn(true);

    this.sut.updateMetadata(dto);

    verify(metadataToUpdate, Mockito.times(1)).setOriginalFile("New_File(1).pdf");
  }

  @Test(expected = ServiceValidationException.class)
  public void testUpdateMetadataUnallowedManipulation1() {
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadataParameter = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataParameter.getOriginalFile()).thenReturn("New_File.pdf");
    Mockito.when(metadataParameter.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataParameter.getExtension()).thenReturn("pdf");
    Mockito.when(metadataParameter.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataParameter.getParent()).thenReturn(DEFAULT_PARENT);
    Mockito.when(metadataParameter.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataParameter.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadataParameter);

    FileStoreMetadata metadataToUpdate = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataToUpdate.getOriginalFile()).thenReturn("Old_File.txt");
    Mockito.when(metadataToUpdate.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataToUpdate.getExtension()).thenReturn("txt");
    Mockito.when(metadataToUpdate.getStorage()).thenReturn(StorageType.RECEIPT);
    Mockito.when(metadataToUpdate.getParent()).thenReturn(DEFAULT_PARENT);
    Mockito.when(metadataToUpdate.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataToUpdate.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataRepository.getOne(DEFAULT_UUID)).thenReturn(metadataToUpdate);

    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(DEFAULT_PARENT, StorageType.DOCUMENT, "New_File.pdf")).thenReturn(true);

    this.sut.updateMetadata(dto);
  }

  @Test(expected = ServiceValidationException.class)
  public void testUpdateMetadataUnallowedManipulation2() {
    FileStoreMetadataDTO dto = Mockito.mock(FileStoreMetadataDTO.class);
    FileStoreMetadata metadataParameter = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataParameter.getOriginalFile()).thenReturn("New_File.pdf");
    Mockito.when(metadataParameter.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataParameter.getExtension()).thenReturn("pdf");
    Mockito.when(metadataParameter.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataParameter.getParent()).thenReturn(metadataParameter);
    Mockito.when(metadataParameter.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataParameter.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataMapper.fileStoreMetadataDTOToFileStoreMetadata(dto)).thenReturn(metadataParameter);

    FileStoreMetadata metadataToUpdate = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadataToUpdate.getOriginalFile()).thenReturn("Old_File.txt");
    Mockito.when(metadataToUpdate.getId()).thenReturn(DEFAULT_UUID);
    Mockito.when(metadataToUpdate.getExtension()).thenReturn("txt");
    Mockito.when(metadataToUpdate.getStorage()).thenReturn(StorageType.DOCUMENT);
    Mockito.when(metadataToUpdate.getParent()).thenReturn(DEFAULT_PARENT);
    Mockito.when(metadataToUpdate.getCreatedAt()).thenReturn(DEFAULT_TIME);
    Mockito.when(metadataToUpdate.getType()).thenReturn(FileStoreMetadataType.FILE);
    Mockito.when(this.fileStoreMetadataRepository.getOne(DEFAULT_UUID)).thenReturn(metadataToUpdate);

    Mockito.when(this.fileStoreMetadataRepository.existsByParentAndStorageAndOriginalFileEquals(DEFAULT_PARENT, StorageType.DOCUMENT, "New_File.pdf")).thenReturn(true);

    this.sut.updateMetadata(dto);
  }

}
