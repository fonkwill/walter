package at.simianarmy.filestore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.verify;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import at.simianarmy.filestore.dto.FileResourceDTO;
import at.simianarmy.filestore.repository.FileStoreMetadataRepository;
import at.simianarmy.filestore.service.FileStoreLocationService;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.swing.text.html.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public abstract class FileStoreTest {

  protected final static String TEMPLATE_LOC = "templates/";
  protected final static String RECEIPT_LOC = "receipts/";
  protected final static String DOCS_LOC = "documents/";
  protected final static String TYPE = "text/plain";
  protected final static String FILE1 = "src/test/resources/filestore/file1.txt";
  protected final static UUID FIlE1_UUID = UUID.randomUUID();
  protected final static String FILE1_ORIG_NAME = "file1.txt";
  protected final static String FILE1_EXT = "txt";
  protected final static String FILE1_NAME = FIlE1_UUID + "." + FILE1_EXT;
  protected final static String TEMPLATE_SUB1_LOC = "templates_sub1";
  protected final static UUID TEMPLATE_SUB1_UUID = UUID.randomUUID();
  protected final static String TEMPLATE_SUB2_LOC = "templates_sub2";
  protected final static UUID TEMPLATE_SUB2_UUID = UUID.randomUUID();
  protected final static Pageable DEFAULT_PAGEABLE = Pageable.unpaged();

  protected MultipartFile file1;

  @Mock
  protected FileStoreLocationService fileStoreLocationService = Mockito.mock(FileStoreLocationService.class);

  @Before
  public void init() throws Exception {
    Mockito.when(this.fileStoreLocationService.getTemplatesLocation()).thenReturn(TEMPLATE_LOC);
    Mockito.when(this.fileStoreLocationService.getReceiptsLocation()).thenReturn(RECEIPT_LOC);
    Mockito.when(this.fileStoreLocationService.getDocumentsLocation()).thenReturn(DOCS_LOC);
    Mockito.when(this.fileStoreLocationService.getLocationsToInit()).thenReturn(Arrays.asList(TEMPLATE_LOC, RECEIPT_LOC, DOCS_LOC));

    this.getSut().initStorageLocations();

    this.file1 = this.mockFile(FILE1);
  }

  @Test
  public void testSave() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, null);
    Mockito.when(this.getFileStoreMetadataRepository().save(Mockito.any())).thenReturn(metadata);
    this.getSut().saveFile(this.file1, this.fileStoreLocationService.getTemplatesLocation(), StorageType.TEMPLATE);

    this.assertExists(TEMPLATE_LOC + "/" + FILE1_NAME);
    this.assertFileEquals(this.file1, TEMPLATE_LOC + "/" + FILE1_NAME);

    ArgumentCaptor<FileStoreMetadata> metadataArgumentCaptor = ArgumentCaptor.forClass(FileStoreMetadata.class);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).save(metadataArgumentCaptor.capture());
    FileStoreMetadata argument = metadataArgumentCaptor.getValue();
    assertEquals(metadata.getExtension(), argument.getExtension());
    assertEquals(metadata.getOriginalFile(), argument.getOriginalFile());
    assertEquals(metadata.getType(), argument.getType());
    assertEquals(metadata.getParent(), argument.getParent());
  }

  @Test(expected = FileStoreException.class)
  public void testGetAsDirectory() throws FileStoreException {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.DIRECTORY, null);
    Mockito.when(this.getFileStoreMetadataRepository().findById(FIlE1_UUID)).thenReturn(
      Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(FIlE1_UUID)).thenReturn(true);

    this.getSut().getFile(FIlE1_UUID, this.fileStoreLocationService.getTemplatesLocation());
  }

  @Test(expected = FileStoreException.class)
  public void testGetNonExistentFile() throws FileStoreException {
    this.getSut().getFile(UUID.randomUUID(), this.fileStoreLocationService.getTemplatesLocation());
  }

  @Test
  public void testGetFile() throws Exception {
    this.createTestFile(this.file1, TEMPLATE_LOC, FILE1_NAME);

    FileStoreMetadata metadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, null);
    Mockito.when(this.getFileStoreMetadataRepository().findById(FIlE1_UUID)).thenReturn(
      Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(FIlE1_UUID)).thenReturn(true);

    FileResourceDTO dto = this.getSut().getFile(FIlE1_UUID, this.fileStoreLocationService.getTemplatesLocation());
    assertTrue(dto.getResource().exists());
  }

  @Test
  public void testDeleteFile() throws Exception {

    this.createTestFile(this.file1, TEMPLATE_LOC, FILE1_NAME);

    FileStoreMetadata metadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, null);
    Mockito.when(this.getFileStoreMetadataRepository().findById(FIlE1_UUID)).thenReturn(
      Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(FIlE1_UUID)).thenReturn(true);

    this.getSut().deleteFile(FIlE1_UUID, this.fileStoreLocationService.getTemplatesLocation());

    this.assertNotExists(TEMPLATE_LOC + "/" + FILE1_NAME);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).delete(metadata);
  }

  @Test
  public void testCreateDirectory() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    Mockito.when(this.getFileStoreMetadataRepository().save(Mockito.any())).thenReturn(metadata);
    this.getSut().createDirectory(TEMPLATE_SUB1_LOC, null, this.fileStoreLocationService.getTemplatesLocation(), StorageType.TEMPLATE);

    ArgumentCaptor<FileStoreMetadata> metadataArgumentCaptor = ArgumentCaptor.forClass(FileStoreMetadata.class);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).save(metadataArgumentCaptor.capture());
    FileStoreMetadata argument = metadataArgumentCaptor.getValue();
    assertEquals(metadata.getExtension(), argument.getExtension());
    assertEquals(metadata.getOriginalFile(), argument.getOriginalFile());
    assertEquals(metadata.getType(), argument.getType());
    assertEquals(metadata.getParent(), argument.getParent());
  }

  @Test
  public void testDeleteDirectory() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));

    this.getSut().deleteDirectory(TEMPLATE_SUB1_UUID, this.fileStoreLocationService.getTemplatesLocation());

    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).delete(metadata);
  }

  @Test
  public void testDeleteDirectoryRecursive() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata metadata2 = this.mockFileStoreMetadata(TEMPLATE_SUB2_UUID, null, TEMPLATE_SUB2_LOC, FileStoreMetadataType.DIRECTORY, metadata);
    Mockito.when(metadata.getChildren()).thenReturn(Sets.newSet(metadata2));

    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB2_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB2_UUID)).thenReturn(Optional.ofNullable(metadata2));

    this.getSut().deleteDirectory(TEMPLATE_SUB1_UUID, this.fileStoreLocationService.getTemplatesLocation());

    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).delete(metadata);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).delete(metadata2);
  }

  @Test
  public void testCreate2SubDirectories() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata metadata2 = this.mockFileStoreMetadata(TEMPLATE_SUB2_UUID, null, TEMPLATE_SUB2_LOC, FileStoreMetadataType.DIRECTORY, metadata);
    Mockito.when(this.getFileStoreMetadataRepository().save(Mockito.any())).thenReturn(metadata2);
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));

    this.getSut().createDirectory(TEMPLATE_SUB2_LOC, TEMPLATE_SUB1_UUID, this.fileStoreLocationService.getTemplatesLocation(), StorageType.TEMPLATE);

    ArgumentCaptor<FileStoreMetadata> metadataArgumentCaptor = ArgumentCaptor.forClass(FileStoreMetadata.class);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(2)).save(metadataArgumentCaptor.capture());

    FileStoreMetadata argument = metadataArgumentCaptor.getAllValues().get(0);
    assertEquals(metadata2.getExtension(), argument.getExtension());
    assertEquals(metadata2.getOriginalFile(), argument.getOriginalFile());
    assertEquals(metadata2.getType(), argument.getType());

    verify(metadata, Mockito.times(1)).addChild(metadata2);
  }

  @Test
  public void testCreateFileInSubDirectory() throws Exception {

    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata fileMetadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, metadata);
    Mockito.when(this.getFileStoreMetadataRepository().save(Mockito.any())).thenReturn(fileMetadata);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);

    this.getSut().saveFile(this.file1, TEMPLATE_SUB1_UUID, TEMPLATE_LOC, StorageType.TEMPLATE);

    this.assertExists(TEMPLATE_LOC + "/"  + FILE1_NAME);

    ArgumentCaptor<FileStoreMetadata> metadataArgumentCaptor = ArgumentCaptor.forClass(FileStoreMetadata.class);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(2)).save(metadataArgumentCaptor.capture());
    FileStoreMetadata argument = metadataArgumentCaptor.getAllValues().get(0);
    assertEquals(fileMetadata.getExtension(), argument.getExtension());
    assertEquals(fileMetadata.getOriginalFile(), argument.getOriginalFile());
    assertEquals(fileMetadata.getType(), argument.getType());

    verify(metadata, Mockito.times(1)).addChild(fileMetadata);
  }

  @Test
  public void testGetFileInSubDirectory() throws Exception {
    this.createTestFile(this.file1, TEMPLATE_LOC, FILE1_NAME);

    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata fileMetadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, metadata);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(FIlE1_UUID)).thenReturn(
      Optional.ofNullable(fileMetadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(FIlE1_UUID)).thenReturn(true);

    FileResourceDTO dto = this.getSut().getFile(FIlE1_UUID, this.fileStoreLocationService.getTemplatesLocation());
    assertTrue(dto.getResource().exists());
  }

  @Test
  public void testDeleteFileInSubDirectory() throws Exception {
    this.createTestFile(this.file1, TEMPLATE_LOC, FILE1_NAME);

    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata fileMetadata = this.mockFileStoreMetadata(FIlE1_UUID, FILE1_EXT, FILE1_ORIG_NAME, FileStoreMetadataType.FILE, metadata);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(FIlE1_UUID)).thenReturn(
      Optional.ofNullable(fileMetadata));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(FIlE1_UUID)).thenReturn(true);

    this.getSut().deleteFile(FIlE1_UUID, this.fileStoreLocationService.getTemplatesLocation());

    this.assertNotExists(TEMPLATE_LOC + FILE1_NAME);
    verify(this.getFileStoreMetadataRepository(), Mockito.times(1)).delete(fileMetadata);
  }

  @Test
  public void testGetDirectory() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    FileStoreMetadata metadata2 = this.mockFileStoreMetadata(TEMPLATE_SUB2_UUID, null, TEMPLATE_SUB2_LOC, FileStoreMetadataType.DIRECTORY, metadata);
    Mockito.when(this.getFileStoreMetadataRepository().findByParentAndStorage(metadata, StorageType.TEMPLATE, DEFAULT_PAGEABLE)).thenReturn(Arrays.asList(metadata2));
    Mockito.when(this.getFileStoreMetadataRepository().existsById(TEMPLATE_SUB1_UUID)).thenReturn(true);
    Mockito.when(this.getFileStoreMetadataRepository().findById(TEMPLATE_SUB1_UUID)).thenReturn(Optional.ofNullable(metadata));

    List<FileStoreMetadata> result = this.getSut().getDirectory(TEMPLATE_SUB1_UUID, StorageType.TEMPLATE, DEFAULT_PAGEABLE);

    assertTrue(result.contains(metadata2));
  }

  @Test
  public void testGetRoot() throws Exception {
    FileStoreMetadata metadata = this.mockFileStoreMetadata(TEMPLATE_SUB1_UUID, null, TEMPLATE_SUB1_LOC, FileStoreMetadataType.DIRECTORY, null);
    Mockito.when(this.getFileStoreMetadataRepository().findByParentAndStorage(null, StorageType.TEMPLATE, DEFAULT_PAGEABLE)).thenReturn(Arrays.asList(metadata));

    List<FileStoreMetadata> result = this.getSut().getDirectory(null, StorageType.TEMPLATE, DEFAULT_PAGEABLE);

    assertTrue(result.contains(metadata));
  }

  private MultipartFile mockFile(String pathToFile) throws Exception {
    Path path = Paths.get(pathToFile);
    String name = path.getFileName().toString();

    byte[] content = Files.readAllBytes(path);

    return new MockMultipartFile(name,
      name, TYPE, content);
  }

  private FileStoreMetadata mockFileStoreMetadata(UUID uuid, String extension, String originalFile, FileStoreMetadataType type, FileStoreMetadata parent) {
    FileStoreMetadata metadata = Mockito.mock(FileStoreMetadata.class);
    Mockito.when(metadata.getId()).thenReturn(uuid);
    Mockito.when(metadata.getExtension()).thenReturn(extension);
    Mockito.when(metadata.getOriginalFile()).thenReturn(originalFile);
    Mockito.when(metadata.getType()).thenReturn(type);
    Mockito.when(metadata.getParent()).thenReturn(parent);

    return metadata;
  }

  @Test
  protected abstract void testInitStorageLocations();

  protected abstract FileStore getSut();

  protected abstract void assertExists(String path);

  protected abstract void assertNotExists(String path);

  protected abstract void assertFileEquals(MultipartFile expectedFile, String actualFilePath)
    throws Exception;

  protected abstract void createTestFile(MultipartFile testFile, String path, String fileName) throws Exception;

  protected abstract void createTestDirectory(String path) throws Exception;

  protected abstract FileStoreMetadataRepository getFileStoreMetadataRepository();

}
