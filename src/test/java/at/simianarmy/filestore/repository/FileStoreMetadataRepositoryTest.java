package at.simianarmy.filestore.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.filestore.domain.FileStoreMetadata;
import at.simianarmy.filestore.domain.enumeration.FileStoreMetadataType;
import at.simianarmy.filestore.domain.enumeration.StorageType;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class FileStoreMetadataRepositoryTest {

  private static final String DEFAULT_FILENAME = "file1.txt";
  private static final String DEFAULT_EXTENSION = "txt";
  private static final String DEFAULT_FILENAME2 = "file2.txt";
  private static final String DEFAULT_FOLDER = "folder";
  private static final StorageType DEFAULT_TYPE = StorageType.DOCUMENT;
  private static final StorageType DEFAULT_TYPE_OTHER = StorageType.TEMPLATE;
  private static final ZonedDateTime DEFAULT_TIME = ZonedDateTime.now();

  @Inject
  private FileStoreMetadataRepository sut;

  private FileStoreMetadata metadata;

  private FileStoreMetadata metadataWithParent;

  private FileStoreMetadata directoryMetadata;

  private FileStoreMetadata metadataInDirectory;

  @Before
  public void init() {
    this.metadata = new FileStoreMetadata();
    this.metadata.setOriginalFile(DEFAULT_FILENAME);
    this.metadata.setType(FileStoreMetadataType.FILE);
    this.metadata.setExtension(DEFAULT_EXTENSION);
    this.metadata.setCreatedAt(DEFAULT_TIME);
    this.metadata.setStorage(DEFAULT_TYPE);

    this.sut.save(this.metadata);

    this.metadataWithParent = new FileStoreMetadata();
    this.metadataWithParent.setOriginalFile(DEFAULT_FILENAME);
    this.metadataWithParent.setType(FileStoreMetadataType.FILE);
    this.metadataWithParent.setExtension(DEFAULT_EXTENSION);
    this.metadataWithParent.setCreatedAt(DEFAULT_TIME);
    this.metadataWithParent.setParent(this.metadata);
    this.metadataWithParent.setStorage(DEFAULT_TYPE);

    this.sut.save(this.metadataWithParent);

    this.directoryMetadata = new FileStoreMetadata();
    this.directoryMetadata.setOriginalFile(DEFAULT_FOLDER);
    this.directoryMetadata.setType(FileStoreMetadataType.DIRECTORY);
    this.directoryMetadata.setCreatedAt(DEFAULT_TIME);
    this.directoryMetadata.setStorage(DEFAULT_TYPE);

    this.sut.save(this.directoryMetadata);

    this.metadataInDirectory = new FileStoreMetadata();
    this.metadataInDirectory.setOriginalFile(DEFAULT_FILENAME2);
    this.metadataInDirectory.setExtension(DEFAULT_EXTENSION);
    this.metadataInDirectory.setType(FileStoreMetadataType.FILE);
    this.metadataInDirectory.setParent(this.directoryMetadata);
    this.metadataInDirectory.setCreatedAt(DEFAULT_TIME);
    this.metadataInDirectory.setStorage(DEFAULT_TYPE);

    this.sut.save(this.metadataInDirectory);
  }

  @After
  public void tearDown() {
    this.sut.delete(this.metadataWithParent);
    this.sut.delete(this.metadata);
    this.sut.delete(this.directoryMetadata);
    this.sut.delete(this.metadataInDirectory);
  }

  @Test
  public void testGetByParentIsNull() {
    List<FileStoreMetadata> result = this.sut.findByParentAndStorage(null, DEFAULT_TYPE, Pageable.unpaged());
    assertEquals(2, result.size());
    assertTrue(result.contains(this.metadata));
    assertTrue(result.contains(this.directoryMetadata));
  }

  @Test
  public void testGetByParentIsNullOtherType() {
    List<FileStoreMetadata> result = this.sut.findByParentAndStorage(null, DEFAULT_TYPE_OTHER, Pageable.unpaged());
    assertEquals(0, result.size());
  }

  @Test
  public void testExistsByOriginalFileEquals() {
    assertTrue(this.sut.existsByParentAndStorageAndOriginalFileEquals(null, DEFAULT_TYPE, DEFAULT_FILENAME));
  }

  @Test
  public void testExistsByOriginalFileEquals2() {
    assertFalse(this.sut.existsByParentAndStorageAndOriginalFileEquals(null, DEFAULT_TYPE, "test.pdf"));
  }

  @Test
  public void testExistsByParentAndOriginalFileEquals() {
    assertTrue(this.sut.existsByParentAndStorageAndOriginalFileEquals(this.directoryMetadata, DEFAULT_TYPE, DEFAULT_FILENAME2));
  }

  @Test
  public void testExistsByParentAndOriginalFileEquals2() {
    assertFalse(this.sut.existsByParentAndStorageAndOriginalFileEquals(this.directoryMetadata, DEFAULT_TYPE, DEFAULT_FILENAME));
  }

  @Test
  public void testExistsByOriginalFileEqualsInOtherStorage() {
    assertFalse(this.sut.existsByParentAndStorageAndOriginalFileEquals(null, DEFAULT_TYPE_OTHER, DEFAULT_FILENAME));
  }

  @Test
  public void testExsistsByParentAndOriginalFileInOtherStorage() {
    assertFalse(this.sut.existsByParentAndStorageAndOriginalFileEquals(this.directoryMetadata, DEFAULT_TYPE_OTHER, DEFAULT_FILENAME2));
  }


}
