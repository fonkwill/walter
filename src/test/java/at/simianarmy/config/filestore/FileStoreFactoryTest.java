package at.simianarmy.config.filestore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

import at.simianarmy.config.FileStoreProperties;
import at.simianarmy.filestore.FileStore;
import at.simianarmy.filestore.FileStoreException;
import at.simianarmy.filestore.impl.dropbox.DropboxFileStore;
import at.simianarmy.filestore.impl.local.LocalFileStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class FileStoreFactoryTest {

  @Mock
  private FileStoreProperties fileStoreProperties;

  @Mock
  private ApplicationContext applicationContext;

  @Mock
  private LocalFileStore localFileStore;

  @Mock
  private DropboxFileStore dropboxFileStore;

  @InjectMocks
  private FileStoreFactory sut;

  @Before
  public void setUp() {
    Mockito.when(this.applicationContext.getBean("LocalFileStore", LocalFileStore.class)).thenReturn(this.localFileStore);
    Mockito.when(this.applicationContext.getBean("DropboxFileStore", DropboxFileStore.class)).thenReturn(this.dropboxFileStore);
  }

  @Test
  public void testCreateLocalFileStore() throws FileStoreException {
    Mockito.when(this.fileStoreProperties.getType()).thenReturn(FileStoreType.LOCAL);
    FileStore fileStore = this.sut.createFileStore();
    assertTrue(fileStore instanceof LocalFileStore);
    assertNotNull(fileStore);
    assertEquals(this.localFileStore, fileStore);

    verify(this.localFileStore, Mockito.times(1)).initStorageLocations();
  }

  @Test
  public void testCreateDropboxFileStore() throws FileStoreException {
    Mockito.when(this.fileStoreProperties.getType()).thenReturn(FileStoreType.DROPBOX);
    FileStore fileStore = this.sut.createFileStore();
    assertTrue(fileStore instanceof DropboxFileStore);
    assertNotNull(fileStore);
    assertEquals(this.dropboxFileStore, fileStore);

    verify(this.dropboxFileStore, Mockito.times(1)).initStorageLocations();
  }

}
