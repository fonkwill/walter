package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.Clothing;
import at.simianarmy.repository.ClothingRepository;
import at.simianarmy.service.ClothingService;
import at.simianarmy.service.dto.ClothingDTO;
import at.simianarmy.service.mapper.ClothingMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClothingResource REST controller.
 *
 * @see ClothingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ClothingResourceIntTest {

    private static final String DEFAULT_SIZE = "AAAAA";
    private static final String UPDATED_SIZE = "BBBBB";

    private static final LocalDate DEFAULT_PURCHASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PURCHASE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_NOT_AVAILABLE = false;
    private static final Boolean UPDATED_NOT_AVAILABLE = true;

    @Inject
    private ClothingRepository clothingRepository;

    @Inject
    private ClothingMapper clothingMapper;

    @Inject
    private ClothingService clothingService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restClothingMockMvc;

    private Clothing clothing;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ClothingResource clothingResource = new ClothingResource();
        ReflectionTestUtils.setField(clothingResource, "clothingService", clothingService);
        this.restClothingMockMvc = MockMvcBuilders.standaloneSetup(clothingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Clothing createEntity(EntityManager em) {
        Clothing clothing = new Clothing()
                .size(DEFAULT_SIZE)
                .purchaseDate(DEFAULT_PURCHASE_DATE)
                .notAvailable(DEFAULT_NOT_AVAILABLE);
        return clothing;
    }

    @Before
    public void initTest() {
        clothing = createEntity(em);
    }

    @Test
    @Transactional
    public void createClothing() throws Exception {
        int databaseSizeBeforeCreate = clothingRepository.findAll().size();

        // Create the Clothing
        ClothingDTO clothingDTO = clothingMapper.clothingToClothingDTO(clothing);

        restClothingMockMvc.perform(post("/api/clothing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clothingDTO)))
                .andExpect(status().isCreated());

        // Validate the Clothing in the database
        List<Clothing> clothing = clothingRepository.findAll();
        assertThat(clothing).hasSize(databaseSizeBeforeCreate + 1);
        Clothing testClothing = clothing.get(clothing.size() - 1);
        assertThat(testClothing.getSize()).isEqualTo(DEFAULT_SIZE);
        assertThat(testClothing.getPurchaseDate()).isEqualTo(DEFAULT_PURCHASE_DATE);
        assertThat(testClothing.isNotAvailable()).isEqualTo(DEFAULT_NOT_AVAILABLE);
    }

    @Test
    @Transactional
    public void getAllClothing() throws Exception {
        // Initialize the database
        clothingRepository.saveAndFlush(clothing);

        // Get all the clothing
        restClothingMockMvc.perform(get("/api/clothing?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(clothing.getId().intValue())))
                .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())))
                .andExpect(jsonPath("$.[*].purchaseDate").value(hasItem(DEFAULT_PURCHASE_DATE.toString())))
                .andExpect(jsonPath("$.[*].notAvailable").value(hasItem(DEFAULT_NOT_AVAILABLE.booleanValue())));
    }

    @Test
    @Transactional
    public void getClothing() throws Exception {
        // Initialize the database
        clothingRepository.saveAndFlush(clothing);

        // Get the clothing
        restClothingMockMvc.perform(get("/api/clothing/{id}", clothing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clothing.getId().intValue()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE.toString()))
            .andExpect(jsonPath("$.purchaseDate").value(DEFAULT_PURCHASE_DATE.toString()))
            .andExpect(jsonPath("$.notAvailable").value(DEFAULT_NOT_AVAILABLE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClothing() throws Exception {
        // Get the clothing
        restClothingMockMvc.perform(get("/api/clothing/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClothing() throws Exception {
        // Initialize the database
        clothingRepository.saveAndFlush(clothing);
        int databaseSizeBeforeUpdate = clothingRepository.findAll().size();

        // Update the clothing
        Clothing updatedClothing = clothingRepository.findById(clothing.getId()).orElse(null);
        updatedClothing
                .size(UPDATED_SIZE)
                .purchaseDate(UPDATED_PURCHASE_DATE)
                .notAvailable(UPDATED_NOT_AVAILABLE);
        ClothingDTO clothingDTO = clothingMapper.clothingToClothingDTO(updatedClothing);

        restClothingMockMvc.perform(put("/api/clothing")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(clothingDTO)))
                .andExpect(status().isOk());

        // Validate the Clothing in the database
        List<Clothing> clothing = clothingRepository.findAll();
        assertThat(clothing).hasSize(databaseSizeBeforeUpdate);
        Clothing testClothing = clothing.get(clothing.size() - 1);
        assertThat(testClothing.getSize()).isEqualTo(UPDATED_SIZE);
        assertThat(testClothing.getPurchaseDate()).isEqualTo(UPDATED_PURCHASE_DATE);
        assertThat(testClothing.isNotAvailable()).isEqualTo(UPDATED_NOT_AVAILABLE);
    }

    @Test
    @Transactional
    public void deleteClothing() throws Exception {
        // Initialize the database
        clothingRepository.saveAndFlush(clothing);
        int databaseSizeBeforeDelete = clothingRepository.findAll().size();

        // Get the clothing
        restClothingMockMvc.perform(delete("/api/clothing/{id}", clothing.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Clothing> clothing = clothingRepository.findAll();
        assertThat(clothing).hasSize(databaseSizeBeforeDelete - 1);
    }
}
