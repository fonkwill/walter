package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.repository.NotificationRuleRepository;
import at.simianarmy.service.NotificationRuleService;
import at.simianarmy.service.dto.NotificationRuleDTO;
import at.simianarmy.service.mapper.NotificationRuleMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the NotificationRuleResource REST controller.
 *
 * @see NotificationRuleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class NotificationRuleResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";

    private static final String DEFAULT_ENTITY = "AAAAA";

    private static final String DEFAULT_CONDITIONS = "AAAAA";

    private static final Boolean DEFAULT_ACTIVE = false;

    private static final String DEFAULT_MESSAGE_GROUP = "AAAAA";

    private static final String DEFAULT_MESSAGE_SINGLE = "AAAAA";

    @Inject
    private NotificationRuleRepository notificationRuleRepository;

    @Inject
    private NotificationRuleMapper notificationRuleMapper;

    @Inject
    private NotificationRuleService notificationRuleService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restNotificationRuleMockMvc;

    private NotificationRule notificationRule;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NotificationRuleResource notificationRuleResource = new NotificationRuleResource();
        ReflectionTestUtils.setField(notificationRuleResource, "notificationRuleService", notificationRuleService);
        this.restNotificationRuleMockMvc = MockMvcBuilders.standaloneSetup(notificationRuleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationRule createEntity(EntityManager em) {
        NotificationRule notificationRule = new NotificationRule()
                .title(DEFAULT_TITLE)
                .entity(DEFAULT_ENTITY)
                .conditions(DEFAULT_CONDITIONS)
                .active(DEFAULT_ACTIVE)
                .messageGroup(DEFAULT_MESSAGE_GROUP)
                .messageSingle(DEFAULT_MESSAGE_SINGLE);
        return notificationRule;
    }

    @Before
    public void initTest() {
        notificationRule = createEntity(em);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setTitle(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEntityIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setEntity(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConditionsIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setConditions(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActiveIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setActive(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMessageGroupIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setMessageGroup(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMessageSingleIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationRuleRepository.findAll().size();
        // set the field null
        notificationRule.setMessageSingle(null);

        // Create the NotificationRule, which fails.
        NotificationRuleDTO notificationRuleDTO = notificationRuleMapper.notificationRuleToNotificationRuleDTO(notificationRule);

        restNotificationRuleMockMvc.perform(post("/api/notification-rules")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(notificationRuleDTO)))
                .andExpect(status().isBadRequest());

        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotificationRules() throws Exception {
        // Initialize the database
        notificationRuleRepository.saveAndFlush(notificationRule);

        // Get all the notificationRules
        restNotificationRuleMockMvc.perform(get("/api/notification-rules?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(notificationRule.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].entity").value(hasItem(DEFAULT_ENTITY.toString())))
                .andExpect(jsonPath("$.[*].conditions").value(hasItem(DEFAULT_CONDITIONS.toString())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].messageGroup").value(hasItem(DEFAULT_MESSAGE_GROUP.toString())))
                .andExpect(jsonPath("$.[*].messageSingle").value(hasItem(DEFAULT_MESSAGE_SINGLE.toString())));
    }

    @Test
    @Transactional
    public void getNotificationRule() throws Exception {
        // Initialize the database
        notificationRuleRepository.saveAndFlush(notificationRule);

        // Get the notificationRule
        restNotificationRuleMockMvc.perform(get("/api/notification-rules/{id}", notificationRule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notificationRule.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.entity").value(DEFAULT_ENTITY.toString()))
            .andExpect(jsonPath("$.conditions").value(DEFAULT_CONDITIONS.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.messageGroup").value(DEFAULT_MESSAGE_GROUP.toString()))
            .andExpect(jsonPath("$.messageSingle").value(DEFAULT_MESSAGE_SINGLE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNotificationRule() throws Exception {
        // Get the notificationRule
        restNotificationRuleMockMvc.perform(get("/api/notification-rules/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteNotificationRule() throws Exception {
        // Initialize the database
        notificationRuleRepository.saveAndFlush(notificationRule);
        int databaseSizeBeforeDelete = notificationRuleRepository.findAll().size();

        // Get the notificationRule
        restNotificationRuleMockMvc.perform(delete("/api/notification-rules/{id}", notificationRule.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<NotificationRule> notificationRules = notificationRuleRepository.findAll();
        assertThat(notificationRules).hasSize(databaseSizeBeforeDelete - 1);
    }
}
