package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.Customization;
import at.simianarmy.repository.CustomizationRepository;
import at.simianarmy.service.CustomizationService;
import at.simianarmy.service.PersonGroupService;
import at.simianarmy.service.dto.CustomizationDTO;
import at.simianarmy.service.mapper.CustomizationMapper;
import at.simianarmy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import at.simianarmy.domain.enumeration.CustomizationName;
/**
 * Test class for the CustomizationResource REST controller.
 *
 * @see CustomizationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CustomizationResourceIntTest {

    private static final CustomizationName DEFAULT_NAME = CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION;
    private static final CustomizationName UPDATED_NAME = CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION;

    private static final String DEFAULT_DATA = "2";
    private static final String UPDATED_DATA = "3";


    @Autowired
    private CustomizationRepository customizationRepository;

    @Autowired
    private CustomizationMapper customizationMapper;

    @Autowired
    private CustomizationService customizationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCustomizationMockMvc;

    private Customization customization;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        customizationRepository.deleteAll();
        customizationRepository.flush();
        final CustomizationResource customizationResource = new CustomizationResource(customizationService, new PersonGroupService());
        this.restCustomizationMockMvc = MockMvcBuilders.standaloneSetup(customizationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customization createEntity(EntityManager em) {
        Customization customization = new Customization()
            .name(DEFAULT_NAME)
            .data(DEFAULT_DATA);
        return customization;
    }

    @Before
    public void initTest() {
        customization = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomization() throws Exception {
        int databaseSizeBeforeCreate = customizationRepository.findAll().size();

        // Create the Customization
        CustomizationDTO customizationDTO = customizationMapper.toDto(customization);
        restCustomizationMockMvc.perform(post("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Customization in the database
        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeCreate + 1);
        Customization testCustomization = customizationList.get(customizationList.size() - 1);
        assertThat(testCustomization.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomization.getData()).isEqualTo(DEFAULT_DATA);
    }

    @Test
    @Transactional
    public void createCustomizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customizationRepository.findAll().size();

        // Create the Customization with an existing ID
        customization.setId(1L);
        CustomizationDTO customizationDTO = customizationMapper.toDto(customization);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomizationMockMvc.perform(post("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Customization in the database
        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = customizationRepository.findAll().size();
        // set the field null
        customization.setName(null);

        // Create the Customization, which fails.
        CustomizationDTO customizationDTO = customizationMapper.toDto(customization);

        restCustomizationMockMvc.perform(post("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isBadRequest());

        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = customizationRepository.findAll().size();
        // set the field null
        customization.setData(null);

        // Create the Customization, which fails.
        CustomizationDTO customizationDTO = customizationMapper.toDto(customization);

        restCustomizationMockMvc.perform(post("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isBadRequest());

        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCustomizations() throws Exception {
        // Initialize the database
        customizationRepository.saveAndFlush(customization);

        // Get all the customizationList
        restCustomizationMockMvc.perform(get("/api/customizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customization.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())));
    }

    @Test
    @Transactional
    public void getCustomization() throws Exception {
        // Initialize the database
        customizationRepository.saveAndFlush(customization);

        // Get the customization
        restCustomizationMockMvc.perform(get("/api/customizations/{id}", customization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customization.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomization() throws Exception {
        // Get the customization
        restCustomizationMockMvc.perform(get("/api/customizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomization() throws Exception {
        // Initialize the database
        customizationRepository.saveAndFlush(customization);
        int databaseSizeBeforeUpdate = customizationRepository.findAll().size();

        // Update the customization
        Customization updatedCustomization = customizationRepository.findById(customization.getId()).orElse(null);
        updatedCustomization
            .name(UPDATED_NAME)
            .data(UPDATED_DATA);
        CustomizationDTO customizationDTO = customizationMapper.toDto(updatedCustomization);

        restCustomizationMockMvc.perform(put("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isOk());

        // Validate the Customization in the database
        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeUpdate);
        Customization testCustomization = customizationList.get(customizationList.size() - 1);
        assertThat(testCustomization.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomization.getData()).isEqualTo(UPDATED_DATA);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomization() throws Exception {
        int databaseSizeBeforeUpdate = customizationRepository.findAll().size();

        // Create the Customization
        CustomizationDTO customizationDTO = customizationMapper.toDto(customization);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCustomizationMockMvc.perform(put("/api/customizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customizationDTO)))
            .andExpect(status().isCreated());

        // Validate the Customization in the database
        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCustomization() throws Exception {
        // Initialize the database
        customizationRepository.saveAndFlush(customization);
        int databaseSizeBeforeDelete = customizationRepository.findAll().size();

        // Get the customization
        restCustomizationMockMvc.perform(delete("/api/customizations/{id}", customization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Customization> customizationList = customizationRepository.findAll();
        assertThat(customizationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
//        TestUtil.equalsVerifier(Customization.class);
        Customization customization1 = new Customization();
        customization1.setName(CustomizationName.MEMBER_PERSONGROUP);
        Customization customization2 = new Customization();
        customization2.setName(CustomizationName.MEMBER_PERSONGROUP);
        assertThat(customization1).isEqualTo(customization2);
        customization2.setName(CustomizationName.HOURS_TO_INVALIDATE_VERIFICATION);
        assertThat(customization1).isNotEqualTo(customization2);
        customization1.setId(null);
        assertThat(customization1).isNotEqualTo(customization2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
//        TestUtil.equalsVerifier(CustomizationDTO.class);
        CustomizationDTO customizationDTO1 = new CustomizationDTO();
        customizationDTO1.setId(1L);
        CustomizationDTO customizationDTO2 = new CustomizationDTO();
        assertThat(customizationDTO1).isNotEqualTo(customizationDTO2);
        customizationDTO2.setId(customizationDTO1.getId());
        assertThat(customizationDTO1).isEqualTo(customizationDTO2);
        customizationDTO2.setId(2L);
        assertThat(customizationDTO1).isNotEqualTo(customizationDTO2);
        customizationDTO1.setId(null);
        assertThat(customizationDTO1).isNotEqualTo(customizationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(customizationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(customizationMapper.fromId(null)).isNull();
    }
}
