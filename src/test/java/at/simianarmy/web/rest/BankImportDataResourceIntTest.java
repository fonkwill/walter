package at.simianarmy.web.rest;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.service.BankImportDataService;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the BankImportDataResource REST controller.
 *
 * @see BankImportDataResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class BankImportDataResourceIntTest {

  private static final LocalDate DEFAULT_ENTRY_DATE = LocalDate.ofEpochDay(0L);

  private static final BigDecimal DEFAULT_ENTRY_VALUE = new BigDecimal(1);

  private static final String DEFAULT_ENTRY_TEXT = "AAAAA";

  private static final String DEFAULT_PARTNER_NAME = "AAAAA";

  private static final byte[] DEFAULT_ENTRY_REFERENCE = "AAAAA".getBytes();

  private static final String CATEGORY_NAME = "CAT";

  private static final String CATEGORY_COLOR = "#efefef";

  private static final String CASHBOOK_ACCOUNT_NAME = "ACCOUNT";

  private static final BankImporterType DEFAULT_BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;

  @Inject
  private BankImportDataRepository bankImportDataRepository;

  @Inject
  private BankImportDataService bankImportDataService;

  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restBankImportDataMockMvc;

  private BankImportData bankImportData;

  private CashbookCategory cashbookCategory;

  private CashbookAccount cashbookAccount;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    BankImportDataResource bankImportDataResource = new BankImportDataResource();
    ReflectionTestUtils.setField(bankImportDataResource, "bankImportDataService",
        bankImportDataService);
    this.restBankImportDataMockMvc = MockMvcBuilders.standaloneSetup(bankImportDataResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static BankImportData createEntity(EntityManager em) {
    BankImportData bankImportData = new BankImportData().entryDate(DEFAULT_ENTRY_DATE)
        .entryValue(DEFAULT_ENTRY_VALUE).entryText(DEFAULT_ENTRY_TEXT)
        .partnerName(DEFAULT_PARTNER_NAME).entryReference(DEFAULT_ENTRY_REFERENCE);

    return bankImportData;
  }

  public static CashbookCategory createCashbookCategory(EntityManager em) {
    CashbookCategory cashbookCategory = new CashbookCategory()
      .name(CATEGORY_NAME)
      .color(CATEGORY_COLOR);

    return cashbookCategory;
  }

  public static CashbookAccount createCashbookAccount(EntityManager em) {
    CashbookAccount cashbookAccount = new CashbookAccount()
      .name(CASHBOOK_ACCOUNT_NAME)
      .bankAccount(true)
      .bankImporterType(DEFAULT_BANK_IMPORTER_TYPE)
      .receiptCode("BA")
      .currentNumber(0)
      .defaultForCashbook(false);

    return cashbookAccount;
  }

  @Before
  public void initTest() {
    cashbookCategory = createCashbookCategory(em);
    cashbookAccount = createCashbookAccount(em);
    bankImportData = createEntity(em);
  }

  @Test
  @Transactional
  public void getAllBankImportData() throws Exception {
    // Initialize the database
    cashbookAccountRepository.saveAndFlush(cashbookAccount);
    cashbookCategoryRepository.saveAndFlush(cashbookCategory);
    bankImportData.setCashbookCategory(cashbookCategory);
    bankImportData.setCashbookAccount(cashbookAccount);
    bankImportDataRepository.saveAndFlush(bankImportData);

    // Get all the bankImportData
    restBankImportDataMockMvc.perform(get("/api/bank-import-data?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(bankImportData.getId().intValue())))
        .andExpect(jsonPath("$.[*].entryDate").value(hasItem(DEFAULT_ENTRY_DATE.toString())))
        .andExpect(jsonPath("$.[*].entryValue").value(hasItem(DEFAULT_ENTRY_VALUE.intValue())))
        .andExpect(jsonPath("$.[*].entryText").value(hasItem(DEFAULT_ENTRY_TEXT.toString())))
        .andExpect(jsonPath("$.[*].partnerName").value(hasItem(DEFAULT_PARTNER_NAME.toString())));
  }
}
