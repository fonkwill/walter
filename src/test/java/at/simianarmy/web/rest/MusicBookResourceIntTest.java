package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MusicBook;
import at.simianarmy.repository.MusicBookRepository;
import at.simianarmy.service.MusicBookService;
import at.simianarmy.service.dto.MusicBookDTO;
import at.simianarmy.service.mapper.MusicBookMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the MusicBookResource REST controller.
 *
 * @see MusicBookResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MusicBookResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  private static final String DEFAULT_DESCRIPTION = "AAAAA";
  private static final String UPDATED_DESCRIPTION = "BBBBB";

  @Inject
  private MusicBookRepository musicBookRepository;

  @Inject
  private MusicBookMapper musicBookMapper;

  @Inject
  private MusicBookService musicBookService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restMusicBookMockMvc;

  private MusicBook musicBook;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    MusicBookResource musicBookResource = new MusicBookResource();
    ReflectionTestUtils.setField(musicBookResource, "musicBookService", musicBookService);
    this.restMusicBookMockMvc = MockMvcBuilders.standaloneSetup(musicBookResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static MusicBook createEntity(EntityManager em) {
    MusicBook musicBook = new MusicBook().name(DEFAULT_NAME).description(DEFAULT_DESCRIPTION);
    return musicBook;
  }

  @Before
  public void initTest() {
    musicBook = createEntity(em);
  }

  @Test
  @Transactional
  public void createMusicBook() throws Exception {
    int databaseSizeBeforeCreate = musicBookRepository.findAll().size();

    // Create the MusicBook
    MusicBookDTO musicBookDTO = musicBookMapper.musicBookToMusicBookDTO(musicBook);

    restMusicBookMockMvc
        .perform(post("/api/music-books").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musicBookDTO)))
        .andExpect(status().isCreated());

    // Validate the MusicBook in the database
    List<MusicBook> musicBooks = musicBookRepository.findAll();
    assertThat(musicBooks).hasSize(databaseSizeBeforeCreate + 1);
    MusicBook testMusicBook = musicBooks.get(musicBooks.size() - 1);
    assertThat(testMusicBook.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testMusicBook.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = musicBookRepository.findAll().size();
    // set the field null
    musicBook.setName(null);

    // Create the MusicBook, which fails.
    MusicBookDTO musicBookDTO = musicBookMapper.musicBookToMusicBookDTO(musicBook);

    restMusicBookMockMvc
        .perform(post("/api/music-books").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musicBookDTO)))
        .andExpect(status().isBadRequest());

    List<MusicBook> musicBooks = musicBookRepository.findAll();
    assertThat(musicBooks).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllMusicBooks() throws Exception {
    // Initialize the database
    musicBookRepository.saveAndFlush(musicBook);

    // Get all the musicBooks
    restMusicBookMockMvc.perform(get("/api/music-books?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(musicBook.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
  }

  @Test
  @Transactional
  public void getMusicBook() throws Exception {
    // Initialize the database
    musicBookRepository.saveAndFlush(musicBook);

    // Get the musicBook
    restMusicBookMockMvc.perform(get("/api/music-books/{id}", musicBook.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(musicBook.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingMusicBook() throws Exception {
    // Get the musicBook
    restMusicBookMockMvc.perform(get("/api/music-books/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateMusicBook() throws Exception {
    // Initialize the database
    musicBookRepository.saveAndFlush(musicBook);
    int databaseSizeBeforeUpdate = musicBookRepository.findAll().size();

    // Update the musicBook
    MusicBook updatedMusicBook = musicBookRepository.findById(musicBook.getId()).orElse(null);
    updatedMusicBook.name(UPDATED_NAME).description(UPDATED_DESCRIPTION);
    MusicBookDTO musicBookDTO = musicBookMapper.musicBookToMusicBookDTO(updatedMusicBook);

    restMusicBookMockMvc.perform(put("/api/music-books").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(musicBookDTO))).andExpect(status().isOk());

    // Validate the MusicBook in the database
    List<MusicBook> musicBooks = musicBookRepository.findAll();
    assertThat(musicBooks).hasSize(databaseSizeBeforeUpdate);
    MusicBook testMusicBook = musicBooks.get(musicBooks.size() - 1);
    assertThat(testMusicBook.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testMusicBook.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
  }

  @Test
  @Transactional
  public void deleteMusicBook() throws Exception {
    // Initialize the database
    musicBookRepository.saveAndFlush(musicBook);
    int databaseSizeBeforeDelete = musicBookRepository.findAll().size();

    // Get the musicBook
    restMusicBookMockMvc.perform(
        delete("/api/music-books/{id}", musicBook.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<MusicBook> musicBooks = musicBookRepository.findAll();
    assertThat(musicBooks).hasSize(databaseSizeBeforeDelete - 1);
  }
}
