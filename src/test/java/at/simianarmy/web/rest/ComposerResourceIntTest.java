package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Composer;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.service.ComposerService;
import at.simianarmy.service.dto.ComposerDTO;
import at.simianarmy.service.mapper.ComposerMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the ComposerResource REST controller.
 *
 * @see ComposerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ComposerResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  @Inject
  private ComposerRepository composerRepository;

  @Inject
  private ComposerMapper composerMapper;

  @Inject
  private ComposerService composerService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restComposerMockMvc;

  private Composer composer;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ComposerResource composerResource = new ComposerResource();
    ReflectionTestUtils.setField(composerResource, "composerService", composerService);
    this.restComposerMockMvc = MockMvcBuilders.standaloneSetup(composerResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Composer createEntity(EntityManager em) {
    Composer composer = new Composer().name(DEFAULT_NAME);
    return composer;
  }

  @Before
  public void initTest() {
    composer = createEntity(em);
  }

  @Test
  @Transactional
  public void createComposer() throws Exception {
    int databaseSizeBeforeCreate = composerRepository.findAll().size();

    // Create the Composer
    ComposerDTO composerDTO = composerMapper.composerToComposerDTO(composer);

    restComposerMockMvc
        .perform(post("/api/composers").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(composerDTO)))
        .andExpect(status().isCreated());

    // Validate the Composer in the database
    List<Composer> composers = composerRepository.findAll();
    assertThat(composers).hasSize(databaseSizeBeforeCreate + 1);
    Composer testComposer = composers.get(composers.size() - 1);
    assertThat(testComposer.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = composerRepository.findAll().size();
    // set the field null
    composer.setName(null);

    // Create the Composer, which fails.
    ComposerDTO composerDTO = composerMapper.composerToComposerDTO(composer);

    restComposerMockMvc
        .perform(post("/api/composers").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(composerDTO)))
        .andExpect(status().isBadRequest());

    List<Composer> composers = composerRepository.findAll();
    assertThat(composers).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllComposers() throws Exception {
    // Initialize the database
    composerRepository.saveAndFlush(composer);

    // Get all the composers
    restComposerMockMvc.perform(get("/api/composers?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(composer.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getComposer() throws Exception {
    // Initialize the database
    composerRepository.saveAndFlush(composer);

    // Get the composer
    restComposerMockMvc.perform(get("/api/composers/{id}", composer.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(composer.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingComposer() throws Exception {
    // Get the composer
    restComposerMockMvc.perform(get("/api/composers/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateComposer() throws Exception {
    // Initialize the database
    composerRepository.saveAndFlush(composer);
    int databaseSizeBeforeUpdate = composerRepository.findAll().size();

    // Update the composer
    Composer updatedComposer = composerRepository.findById(composer.getId()).orElse(null);
    updatedComposer.name(UPDATED_NAME);
    ComposerDTO composerDTO = composerMapper.composerToComposerDTO(updatedComposer);

    restComposerMockMvc.perform(put("/api/composers").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(composerDTO))).andExpect(status().isOk());

    // Validate the Composer in the database
    List<Composer> composers = composerRepository.findAll();
    assertThat(composers).hasSize(databaseSizeBeforeUpdate);
    Composer testComposer = composers.get(composers.size() - 1);
    assertThat(testComposer.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteComposer() throws Exception {
    // Initialize the database
    composerRepository.saveAndFlush(composer);
    int databaseSizeBeforeDelete = composerRepository.findAll().size();

    // Get the composer
    restComposerMockMvc
        .perform(
            delete("/api/composers/{id}", composer.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Composer> composers = composerRepository.findAll();
    assertThat(composers).hasSize(databaseSizeBeforeDelete - 1);
  }
}
