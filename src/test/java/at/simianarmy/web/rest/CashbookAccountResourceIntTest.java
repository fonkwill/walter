package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CashbookAccountService;
import at.simianarmy.service.dto.CashbookAccountDTO;
import at.simianarmy.service.mapper.CashbookAccountMapper;
import at.simianarmy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CashbookAccountResource REST controller.
 *
 * @see CashbookAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookAccountResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BANK_ACCOUNT = true;
    private static final Boolean UPDATED_BANK_ACCOUNT = false;

    private static final BankImporterType DEFAULT_BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;
    private static final BankImporterType UPDATED_BANK_IMPORTER_TYPE = null;

    private static final String DEFAULT_RECEIPT_CODE = "A";
    private static final String UPDATED_RECEIPT_CODE = "B";

    private static final Integer DEFAULT_CURRENT_NUMBER = 1;

    @Autowired
    private CashbookAccountRepository cashbookAccountRepository;

    @Autowired
    private CashbookAccountMapper cashbookAccountMapper;

    @Autowired
    private CashbookAccountService cashbookAccountService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCashbookAccountMockMvc;

    private CashbookAccount cashbookAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CashbookAccountResource cashbookAccountResource = new CashbookAccountResource(cashbookAccountService);
        this.restCashbookAccountMockMvc = MockMvcBuilders.standaloneSetup(cashbookAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CashbookAccount createEntity(EntityManager em) {
        CashbookAccount cashbookAccount = new CashbookAccount()
            .name(DEFAULT_NAME)
            .bankAccount(DEFAULT_BANK_ACCOUNT)
            .bankImporterType(DEFAULT_BANK_IMPORTER_TYPE)
            .receiptCode(DEFAULT_RECEIPT_CODE)
            .currentNumber(DEFAULT_CURRENT_NUMBER)
            .defaultForCashbook(false);
        return cashbookAccount;
    }

    @Before
    public void initTest() {
        cashbookAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createCashbookAccount() throws Exception {
        int databaseSizeBeforeCreate = cashbookAccountRepository.findAll().size();

        // Create the CashbookAccount
        CashbookAccountDTO cashbookAccountDTO = cashbookAccountMapper.toDto(cashbookAccount);
        restCashbookAccountMockMvc.perform(post("/api/cashbook-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the CashbookAccount in the database
        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeCreate + 1);
        CashbookAccount testCashbookAccount = cashbookAccountList.get(cashbookAccountList.size() - 1);
        assertThat(testCashbookAccount.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCashbookAccount.isBankAccount()).isEqualTo(DEFAULT_BANK_ACCOUNT);
        assertThat(testCashbookAccount.getBankImporterType()).isEqualTo(DEFAULT_BANK_IMPORTER_TYPE);
        assertThat(testCashbookAccount.getReceiptCode()).isEqualTo(DEFAULT_RECEIPT_CODE);
        assertThat(testCashbookAccount.isDefaultForCashbook()).isEqualTo(false);
    }

    @Test
    @Transactional
    public void createCashbookAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cashbookAccountRepository.findAll().size();

        // Create the CashbookAccount with an existing ID
        cashbookAccount.setId(1L);
        CashbookAccountDTO cashbookAccountDTO = cashbookAccountMapper.toDto(cashbookAccount);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCashbookAccountMockMvc.perform(post("/api/cashbook-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CashbookAccount in the database
        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = cashbookAccountRepository.findAll().size();
        // set the field null
        cashbookAccount.setName(null);

        // Create the CashbookAccount, which fails.
        CashbookAccountDTO cashbookAccountDTO = cashbookAccountMapper.toDto(cashbookAccount);

        restCashbookAccountMockMvc.perform(post("/api/cashbook-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookAccountDTO)))
            .andExpect(status().isBadRequest());

        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCashbookAccounts() throws Exception {
        // Initialize the database
        cashbookAccountRepository.saveAndFlush(cashbookAccount);

        // Get all the cashbookAccountList
        restCashbookAccountMockMvc.perform(get("/api/cashbook-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cashbookAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].bankAccount").value(hasItem(DEFAULT_BANK_ACCOUNT.booleanValue())))
            .andExpect(jsonPath("$.[*].bankImporterType").value(hasItem(DEFAULT_BANK_IMPORTER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].receiptCode").value(hasItem(DEFAULT_RECEIPT_CODE.toString())));
    }

    @Test
    @Transactional
    public void getCashbookAccount() throws Exception {
        // Initialize the database
        cashbookAccountRepository.saveAndFlush(cashbookAccount);

        // Get the cashbookAccount
        restCashbookAccountMockMvc.perform(get("/api/cashbook-accounts/{id}", cashbookAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cashbookAccount.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.bankAccount").value(DEFAULT_BANK_ACCOUNT.booleanValue()))
            .andExpect(jsonPath("$.bankImporterType").value(DEFAULT_BANK_IMPORTER_TYPE.toString()))
            .andExpect(jsonPath("$.receiptCode").value(DEFAULT_RECEIPT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCashbookAccount() throws Exception {
        // Get the cashbookAccount
        restCashbookAccountMockMvc.perform(get("/api/cashbook-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCashbookAccount() throws Exception {
        // Initialize the database
        cashbookAccountRepository.saveAndFlush(cashbookAccount);
        int databaseSizeBeforeUpdate = cashbookAccountRepository.findAll().size();

        // Update the cashbookAccount
        CashbookAccount updatedCashbookAccount = cashbookAccountRepository.findById(cashbookAccount.getId()).orElse(null);
        updatedCashbookAccount
            .name(UPDATED_NAME)
            .bankAccount(UPDATED_BANK_ACCOUNT)
            .bankImporterType(UPDATED_BANK_IMPORTER_TYPE)
            .receiptCode(UPDATED_RECEIPT_CODE)
            .setDefaultForCashbook(false);
        CashbookAccountDTO cashbookAccountDTO = cashbookAccountMapper.toDto(updatedCashbookAccount);

        restCashbookAccountMockMvc.perform(put("/api/cashbook-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookAccountDTO)))
            .andExpect(status().isOk());

        // Validate the CashbookAccount in the database
        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeUpdate);
        CashbookAccount testCashbookAccount = cashbookAccountList.get(cashbookAccountList.size() - 1);
        assertThat(testCashbookAccount.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCashbookAccount.isBankAccount()).isEqualTo(UPDATED_BANK_ACCOUNT);
        assertThat(testCashbookAccount.getBankImporterType()).isEqualTo(UPDATED_BANK_IMPORTER_TYPE);
        assertThat(testCashbookAccount.getReceiptCode()).isEqualTo(UPDATED_RECEIPT_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingCashbookAccount() throws Exception {
        int databaseSizeBeforeUpdate = cashbookAccountRepository.findAll().size();

        // Create the CashbookAccount
        CashbookAccountDTO cashbookAccountDTO = cashbookAccountMapper.toDto(cashbookAccount);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCashbookAccountMockMvc.perform(put("/api/cashbook-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the CashbookAccount in the database
        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCashbookAccount() throws Exception {
        // Initialize the database
        cashbookAccountRepository.saveAndFlush(cashbookAccount);
        int databaseSizeBeforeDelete = cashbookAccountRepository.findAll().size();

        // Get the cashbookAccount
        restCashbookAccountMockMvc.perform(delete("/api/cashbook-accounts/{id}", cashbookAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CashbookAccount> cashbookAccountList = cashbookAccountRepository.findAll();
        assertThat(cashbookAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        CashbookAccount cashbookAccount1 = new CashbookAccount();
        cashbookAccount1.setId(1L);
        CashbookAccount cashbookAccount2 = new CashbookAccount();
        cashbookAccount2.setId(cashbookAccount1.getId());
        assertThat(cashbookAccount1).isEqualTo(cashbookAccount2);
        cashbookAccount2.setId(2L);
        assertThat(cashbookAccount1).isNotEqualTo(cashbookAccount2);
        cashbookAccount1.setId(null);
        assertThat(cashbookAccount1).isNotEqualTo(cashbookAccount2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        CashbookAccountDTO cashbookAccountDTO1 = new CashbookAccountDTO();
        cashbookAccountDTO1.setId(1L);
        CashbookAccountDTO cashbookAccountDTO2 = new CashbookAccountDTO();
        assertThat(cashbookAccountDTO1).isNotEqualTo(cashbookAccountDTO2);
        cashbookAccountDTO2.setId(cashbookAccountDTO1.getId());
        assertThat(cashbookAccountDTO1).isEqualTo(cashbookAccountDTO2);
        cashbookAccountDTO2.setId(2L);
        assertThat(cashbookAccountDTO1).isNotEqualTo(cashbookAccountDTO2);
        cashbookAccountDTO1.setId(null);
        assertThat(cashbookAccountDTO1).isNotEqualTo(cashbookAccountDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(cashbookAccountMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(cashbookAccountMapper.fromId(null)).isNull();
    }
}
