package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.PersonGroup;
import at.simianarmy.repository.PersonGroupRepository;
import at.simianarmy.service.PersonGroupService;
import at.simianarmy.service.dto.PersonGroupDTO;
import at.simianarmy.service.mapper.PersonGroupMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonGroupResource REST controller.
 *
 * @see PersonGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonGroupResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    @Inject
    private PersonGroupRepository personGroupRepository;

    @Inject
    private PersonGroupMapper personGroupMapper;

    @Inject
    private PersonGroupService personGroupService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPersonGroupMockMvc;

    private PersonGroup personGroup;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonGroupResource personGroupResource = new PersonGroupResource();
        ReflectionTestUtils.setField(personGroupResource, "personGroupService", personGroupService);
        this.restPersonGroupMockMvc = MockMvcBuilders.standaloneSetup(personGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonGroup createEntity(EntityManager em) {
        PersonGroup personGroup = new PersonGroup()
                .name(DEFAULT_NAME);
        return personGroup;
    }

    @Before
    public void initTest() {
        personGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonGroup() throws Exception {
        int databaseSizeBeforeCreate = personGroupRepository.findAll().size();

        // Create the PersonGroup
        PersonGroupDTO personGroupDTO = personGroupMapper.personGroupToPersonGroupDTO(personGroup);

        restPersonGroupMockMvc.perform(post("/api/person-groups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personGroupDTO)))
                .andExpect(status().isCreated());

        // Validate the PersonGroup in the database
        List<PersonGroup> personGroups = personGroupRepository.findAll();
        assertThat(personGroups).hasSize(databaseSizeBeforeCreate + 1);
        PersonGroup testPersonGroup = personGroups.get(personGroups.size() - 1);
        assertThat(testPersonGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = personGroupRepository.findAll().size();
        // set the field null
        personGroup.setName(null);

        // Create the PersonGroup, which fails.
        PersonGroupDTO personGroupDTO = personGroupMapper.personGroupToPersonGroupDTO(personGroup);

        restPersonGroupMockMvc.perform(post("/api/person-groups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personGroupDTO)))
                .andExpect(status().isBadRequest());

        List<PersonGroup> personGroups = personGroupRepository.findAll();
        assertThat(personGroups).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPersonGroups() throws Exception {
        // Initialize the database
        personGroupRepository.saveAndFlush(personGroup);

        // Get all the personGroups
        restPersonGroupMockMvc.perform(get("/api/person-groups?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personGroup.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPersonGroup() throws Exception {
        // Initialize the database
        personGroupRepository.saveAndFlush(personGroup);

        // Get the personGroup
        restPersonGroupMockMvc.perform(get("/api/person-groups/{id}", personGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personGroup.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonGroup() throws Exception {
        // Get the personGroup
        restPersonGroupMockMvc.perform(get("/api/person-groups/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonGroup() throws Exception {
        // Initialize the database
        personGroupRepository.saveAndFlush(personGroup);
        int databaseSizeBeforeUpdate = personGroupRepository.findAll().size();

        // Update the personGroup
        PersonGroup updatedPersonGroup = personGroupRepository.findById(personGroup.getId()).orElse(null);
        updatedPersonGroup
                .name(UPDATED_NAME);
        PersonGroupDTO personGroupDTO = personGroupMapper.personGroupToPersonGroupDTO(updatedPersonGroup);

        restPersonGroupMockMvc.perform(put("/api/person-groups")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personGroupDTO)))
                .andExpect(status().isOk());

        // Validate the PersonGroup in the database
        List<PersonGroup> personGroups = personGroupRepository.findAll();
        assertThat(personGroups).hasSize(databaseSizeBeforeUpdate);
        PersonGroup testPersonGroup = personGroups.get(personGroups.size() - 1);
        assertThat(testPersonGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void deletePersonGroup() throws Exception {
        // Initialize the database
        personGroupRepository.saveAndFlush(personGroup);
        int databaseSizeBeforeDelete = personGroupRepository.findAll().size();

        // Get the personGroup
        restPersonGroupMockMvc.perform(delete("/api/person-groups/{id}", personGroup.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonGroup> personGroups = personGroupRepository.findAll();
        assertThat(personGroups).hasSize(databaseSizeBeforeDelete - 1);
    }
}
