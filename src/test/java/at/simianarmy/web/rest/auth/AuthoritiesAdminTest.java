package at.simianarmy.web.rest.auth;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import at.simianarmy.WalterApp;
import at.simianarmy.security.AuthoritiesConstants;
import at.simianarmy.service.dto.PersonDTO;
import at.simianarmy.web.rest.PersonResource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@WithMockUser(authorities={AuthoritiesConstants.MEMBER_ADMIN})
public class AuthoritiesAdminTest {

	@Inject
	PersonResource personResource;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testMethods_ShouldBeAccessible() {
		try{
		personResource.createPerson(new PersonDTO());
		personResource.getAllPeople(null, null, null, null, null, null, null);
		personResource.getPerson(null);
		personResource.deletePerson(null);
		personResource.updatePerson(null);
		}catch (AccessDeniedException e) {
			fail();
		} catch (Exception ex) {
			//ignore
		}
		
	}
	
	@Test
	public void testMethods_ShouldNotBeAccessible() {
		try{

		}catch (AccessDeniedException e) {
			 //ignore
		} catch (Exception ex) {
			//ignore
		}
		
	}

}
