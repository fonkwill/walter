package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.service.CashbookEntryService;
import at.simianarmy.service.dto.CashbookEntryDTO;
import at.simianarmy.service.mapper.CashbookEntryMapper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the CashbookEntryResource REST controller.
 *
 * @see CashbookEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookEntryResourceIntTest {

  private static final String DEFAULT_TITLE = "AAAAAAAAAA";
  private static final String UPDATED_TITLE = "BBBBBBBBBB";

  private static final CashbookEntryType DEFAULT_TYPE = CashbookEntryType.INCOME;
  private static final CashbookEntryType UPDATED_TYPE = CashbookEntryType.SPENDING;

  private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

  private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(0);
  private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(1);

  private static final String CASHBOOK_ACCOUNT_NAME = "ACCOUNT";
  private static final BankImporterType DEFAULT_BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Inject
  private CashbookEntryMapper cashbookEntryMapper;

  @Inject
  private CashbookEntryService cashbookEntryService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restCashbookEntryMockMvc;

  private CashbookEntry cashbookEntry;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    CashbookEntryResource cashbookEntryResource = new CashbookEntryResource();
    ReflectionTestUtils.setField(cashbookEntryResource, "cashbookEntryService",
        cashbookEntryService);
    this.restCashbookEntryMockMvc = MockMvcBuilders.standaloneSetup(cashbookEntryResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static CashbookEntry createEntity(EntityManager em) {
    CashbookEntry cashbookEntry = new CashbookEntry().title(DEFAULT_TITLE).type(DEFAULT_TYPE)
        .date(DEFAULT_DATE).amount(DEFAULT_AMOUNT);
    return cashbookEntry;
  }

  public static CashbookAccount createCashbookAccount(EntityManager em) {
    CashbookAccount cashbookAccount = new CashbookAccount()
      .name(CASHBOOK_ACCOUNT_NAME)
      .bankAccount(true)
      .bankImporterType(DEFAULT_BANK_IMPORTER_TYPE)
      .receiptCode("BA")
      .currentNumber(0)
      .defaultForCashbook(false);

    return cashbookAccount;
  }

  @Before
  public void initTest() {
    cashbookEntry = createEntity(em);
    CashbookAccount cashbookAccount = createCashbookAccount(em);
    this.cashbookAccountRepository.save(cashbookAccount);
    cashbookEntry.setCashbookAccount(cashbookAccount);
  }

  @Test
  @Transactional
  public void createCashbookEntry() throws Exception {
    int databaseSizeBeforeCreate = cashbookEntryRepository.findAll().size();

    // Create the CashbookEntry
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    restCashbookEntryMockMvc
        .perform(post("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isCreated());

    // Validate the CashbookEntry in the database
    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeCreate + 1);
    CashbookEntry testCashbookEntry = cashbookEntries.get(cashbookEntries.size() - 1);
    assertThat(testCashbookEntry.getTitle()).isEqualTo(DEFAULT_TITLE);
    assertThat(testCashbookEntry.getType()).isEqualTo(DEFAULT_TYPE);
    assertThat(testCashbookEntry.getDate()).isEqualTo(DEFAULT_DATE);
    assertThat(testCashbookEntry.getAmount()).isEqualTo(DEFAULT_AMOUNT);
  }

  @Test
  @Transactional
  public void checkTitleIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookEntryRepository.findAll().size();
    // set the field null
    cashbookEntry.setTitle(null);

    // Create the CashbookEntry, which fails.
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    restCashbookEntryMockMvc
        .perform(post("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkTypeIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookEntryRepository.findAll().size();
    // set the field null
    cashbookEntry.setType(null);

    // Create the CashbookEntry, which fails.
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    restCashbookEntryMockMvc
        .perform(post("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookEntryRepository.findAll().size();
    // set the field null
    cashbookEntry.setDate(null);

    // Create the CashbookEntry, which fails.
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    restCashbookEntryMockMvc
        .perform(post("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkAmountIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookEntryRepository.findAll().size();
    // set the field null
    cashbookEntry.setAmount(null);

    // Create the CashbookEntry, which fails.
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(cashbookEntry);

    restCashbookEntryMockMvc
        .perform(post("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllCashbookEntries() throws Exception {
    // Initialize the database
    cashbookEntryRepository.saveAndFlush(cashbookEntry);

    // Get all the cashbookEntries
    restCashbookEntryMockMvc.perform(get("/api/cashbook-entries?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(cashbookEntry.getId().intValue())))
        .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
        .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
        .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
        .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())));
  }

  @Test
  @Transactional
  public void getCashbookEntry() throws Exception {
    // Initialize the database
    cashbookEntryRepository.saveAndFlush(cashbookEntry);

    // Get the cashbookEntry
    restCashbookEntryMockMvc.perform(get("/api/cashbook-entries/{id}", cashbookEntry.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(cashbookEntry.getId().intValue()))
        .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
        .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
        .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
        .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()));
  }

  @Test
  @Transactional
  public void getNonExistingCashbookEntry() throws Exception {
    // Get the cashbookEntry
    restCashbookEntryMockMvc.perform(get("/api/cashbook-entries/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateCashbookEntry() throws Exception {
    // Initialize the database
    cashbookEntryRepository.saveAndFlush(cashbookEntry);
    int databaseSizeBeforeUpdate = cashbookEntryRepository.findAll().size();

    // Update the cashbookEntry
    CashbookEntry updatedCashbookEntry = cashbookEntryRepository.findById(cashbookEntry.getId()).orElse(null);
    updatedCashbookEntry.title(UPDATED_TITLE).type(UPDATED_TYPE).date(UPDATED_DATE)
        .amount(UPDATED_AMOUNT);
    CashbookEntryDTO cashbookEntryDTO = cashbookEntryMapper
        .cashbookEntryToCashbookEntryDTO(updatedCashbookEntry);

    restCashbookEntryMockMvc
        .perform(put("/api/cashbook-entries").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookEntryDTO)))
        .andExpect(status().isOk());

    // Validate the CashbookEntry in the database
    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeUpdate);
    CashbookEntry testCashbookEntry = cashbookEntries.get(cashbookEntries.size() - 1);
    assertThat(testCashbookEntry.getTitle()).isEqualTo(UPDATED_TITLE);
    assertThat(testCashbookEntry.getType()).isEqualTo(UPDATED_TYPE);
    assertThat(testCashbookEntry.getDate()).isEqualTo(UPDATED_DATE);
    assertThat(testCashbookEntry.getAmount()).isEqualTo(UPDATED_AMOUNT);
  }

  @Test
  @Transactional
  public void deleteCashbookEntry() throws Exception {
    // Initialize the database
    cashbookEntryRepository.saveAndFlush(cashbookEntry);
    int databaseSizeBeforeDelete = cashbookEntryRepository.findAll().size();

    // Get the cashbookEntry
    restCashbookEntryMockMvc.perform(delete("/api/cashbook-entries/{id}", cashbookEntry.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<CashbookEntry> cashbookEntries = cashbookEntryRepository.findAll();
    assertThat(cashbookEntries).hasSize(databaseSizeBeforeDelete - 1);
  }
}
