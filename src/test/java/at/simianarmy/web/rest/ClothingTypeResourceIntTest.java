package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.repository.ClothingTypeRepository;
import at.simianarmy.service.ClothingTypeService;
import at.simianarmy.service.dto.ClothingTypeDTO;
import at.simianarmy.service.mapper.ClothingTypeMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the ClothingTypeResource REST controller.
 *
 * @see ClothingTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ClothingTypeResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  @Inject
  private ClothingTypeRepository clothingTypeRepository;

  @Inject
  private ClothingTypeMapper clothingTypeMapper;

  @Inject
  private ClothingTypeService clothingTypeService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restClothingTypeMockMvc;

  private ClothingType clothingType;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ClothingTypeResource clothingTypeResource = new ClothingTypeResource();
    ReflectionTestUtils.setField(clothingTypeResource, "clothingTypeService", clothingTypeService);
    this.restClothingTypeMockMvc = MockMvcBuilders.standaloneSetup(clothingTypeResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static ClothingType createEntity(EntityManager em) {
    ClothingType clothingType = new ClothingType().name(DEFAULT_NAME);
    return clothingType;
  }

  @Before
  public void initTest() {
    clothingType = createEntity(em);
  }

  @Test
  @Transactional
  public void createClothingType() throws Exception {
    int databaseSizeBeforeCreate = clothingTypeRepository.findAll().size();

    // Create the ClothingType
    ClothingTypeDTO clothingTypeDTO = clothingTypeMapper
        .clothingTypeToClothingTypeDTO(clothingType);

    restClothingTypeMockMvc
        .perform(post("/api/clothing-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothingTypeDTO)))
        .andExpect(status().isCreated());

    // Validate the ClothingType in the database
    List<ClothingType> clothingTypes = clothingTypeRepository.findAll();
    assertThat(clothingTypes).hasSize(databaseSizeBeforeCreate + 1);
    ClothingType testClothingType = clothingTypes.get(clothingTypes.size() - 1);
    assertThat(testClothingType.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = clothingTypeRepository.findAll().size();
    // set the field null
    clothingType.setName(null);

    // Create the ClothingType, which fails.
    ClothingTypeDTO clothingTypeDTO = clothingTypeMapper
        .clothingTypeToClothingTypeDTO(clothingType);

    restClothingTypeMockMvc
        .perform(post("/api/clothing-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothingTypeDTO)))
        .andExpect(status().isBadRequest());

    List<ClothingType> clothingTypes = clothingTypeRepository.findAll();
    assertThat(clothingTypes).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllClothingTypes() throws Exception {
    // Initialize the database
    clothingTypeRepository.saveAndFlush(clothingType);

    // Get all the clothingTypes
    restClothingTypeMockMvc.perform(get("/api/clothing-types?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(clothingType.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getClothingType() throws Exception {
    // Initialize the database
    clothingTypeRepository.saveAndFlush(clothingType);

    // Get the clothingType
    restClothingTypeMockMvc.perform(get("/api/clothing-types/{id}", clothingType.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(clothingType.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingClothingType() throws Exception {
    // Get the clothingType
    restClothingTypeMockMvc.perform(get("/api/clothing-types/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateClothingType() throws Exception {
    // Initialize the database
    clothingTypeRepository.saveAndFlush(clothingType);
    int databaseSizeBeforeUpdate = clothingTypeRepository.findAll().size();

    // Update the clothingType
    ClothingType updatedClothingType = clothingTypeRepository.findById(clothingType.getId()).orElse(null);
    updatedClothingType.name(UPDATED_NAME);
    ClothingTypeDTO clothingTypeDTO = clothingTypeMapper
        .clothingTypeToClothingTypeDTO(updatedClothingType);

    restClothingTypeMockMvc
        .perform(put("/api/clothing-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clothingTypeDTO)))
        .andExpect(status().isOk());

    // Validate the ClothingType in the database
    List<ClothingType> clothingTypes = clothingTypeRepository.findAll();
    assertThat(clothingTypes).hasSize(databaseSizeBeforeUpdate);
    ClothingType testClothingType = clothingTypes.get(clothingTypes.size() - 1);
    assertThat(testClothingType.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteClothingType() throws Exception {
    // Initialize the database
    clothingTypeRepository.saveAndFlush(clothingType);
    int databaseSizeBeforeDelete = clothingTypeRepository.findAll().size();

    // Get the clothingType
    restClothingTypeMockMvc.perform(delete("/api/clothing-types/{id}", clothingType.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<ClothingType> clothingTypes = clothingTypeRepository.findAll();
    assertThat(clothingTypes).hasSize(databaseSizeBeforeDelete - 1);
  }
}
