package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.repository.InstrumentTypeRepository;
import at.simianarmy.service.InstrumentTypeService;
import at.simianarmy.service.dto.InstrumentTypeDTO;
import at.simianarmy.service.mapper.InstrumentTypeMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the InstrumentTypeResource REST controller.
 *
 * @see InstrumentTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentTypeResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  @Inject
  private InstrumentTypeRepository instrumentTypeRepository;

  @Inject
  private InstrumentTypeMapper instrumentTypeMapper;

  @Inject
  private InstrumentTypeService instrumentTypeService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restInstrumentTypeMockMvc;

  private InstrumentType instrumentType;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    InstrumentTypeResource instrumentTypeResource = new InstrumentTypeResource();
    ReflectionTestUtils.setField(instrumentTypeResource, "instrumentTypeService",
        instrumentTypeService);
    this.restInstrumentTypeMockMvc = MockMvcBuilders.standaloneSetup(instrumentTypeResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static InstrumentType createEntity(EntityManager em) {
    InstrumentType instrumentType = new InstrumentType().name(DEFAULT_NAME);
    return instrumentType;
  }

  @Before
  public void initTest() {
    instrumentType = createEntity(em);
  }

  @Test
  @Transactional
  public void createInstrumentType() throws Exception {
    int databaseSizeBeforeCreate = instrumentTypeRepository.findAll().size();

    // Create the InstrumentType
    InstrumentTypeDTO instrumentTypeDTO = instrumentTypeMapper
        .instrumentTypeToInstrumentTypeDTO(instrumentType);

    restInstrumentTypeMockMvc
        .perform(post("/api/instrument-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(instrumentTypeDTO)))
        .andExpect(status().isCreated());

    // Validate the InstrumentType in the database
    List<InstrumentType> instrumentTypes = instrumentTypeRepository.findAll();
    assertThat(instrumentTypes).hasSize(databaseSizeBeforeCreate + 1);
    InstrumentType testInstrumentType = instrumentTypes.get(instrumentTypes.size() - 1);
    assertThat(testInstrumentType.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = instrumentTypeRepository.findAll().size();
    // set the field null
    instrumentType.setName(null);

    // Create the InstrumentType, which fails.
    InstrumentTypeDTO instrumentTypeDTO = instrumentTypeMapper
        .instrumentTypeToInstrumentTypeDTO(instrumentType);

    restInstrumentTypeMockMvc
        .perform(post("/api/instrument-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(instrumentTypeDTO)))
        .andExpect(status().isBadRequest());

    List<InstrumentType> instrumentTypes = instrumentTypeRepository.findAll();
    assertThat(instrumentTypes).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllInstrumentTypes() throws Exception {
    // Initialize the database
    instrumentTypeRepository.saveAndFlush(instrumentType);

    // Get all the instrumentTypes
    restInstrumentTypeMockMvc.perform(get("/api/instrument-types?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(instrumentType.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getInstrumentType() throws Exception {
    // Initialize the database
    instrumentTypeRepository.saveAndFlush(instrumentType);

    // Get the instrumentType
    restInstrumentTypeMockMvc.perform(get("/api/instrument-types/{id}", instrumentType.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(instrumentType.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingInstrumentType() throws Exception {
    // Get the instrumentType
    restInstrumentTypeMockMvc.perform(get("/api/instrument-types/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateInstrumentType() throws Exception {
    // Initialize the database
    instrumentTypeRepository.saveAndFlush(instrumentType);
    int databaseSizeBeforeUpdate = instrumentTypeRepository.findAll().size();

    // Update the instrumentType
    InstrumentType updatedInstrumentType = instrumentTypeRepository.findById(instrumentType.getId()).orElse(null);
    updatedInstrumentType.name(UPDATED_NAME);
    InstrumentTypeDTO instrumentTypeDTO = instrumentTypeMapper
        .instrumentTypeToInstrumentTypeDTO(updatedInstrumentType);

    restInstrumentTypeMockMvc
        .perform(put("/api/instrument-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(instrumentTypeDTO)))
        .andExpect(status().isOk());

    // Validate the InstrumentType in the database
    List<InstrumentType> instrumentTypes = instrumentTypeRepository.findAll();
    assertThat(instrumentTypes).hasSize(databaseSizeBeforeUpdate);
    InstrumentType testInstrumentType = instrumentTypes.get(instrumentTypes.size() - 1);
    assertThat(testInstrumentType.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteInstrumentType() throws Exception {
    // Initialize the database
    instrumentTypeRepository.saveAndFlush(instrumentType);
    int databaseSizeBeforeDelete = instrumentTypeRepository.findAll().size();

    // Get the instrumentType
    restInstrumentTypeMockMvc.perform(delete("/api/instrument-types/{id}", instrumentType.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<InstrumentType> instrumentTypes = instrumentTypeRepository.findAll();
    assertThat(instrumentTypes).hasSize(databaseSizeBeforeDelete - 1);
  }
}
