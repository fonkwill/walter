package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.repository.PersonInstrumentRepository;
import at.simianarmy.service.PersonInstrumentService;
import at.simianarmy.service.dto.PersonInstrumentDTO;
import at.simianarmy.service.mapper.PersonInstrumentMapper;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the PersonInstrumentResource REST controller.
 *
 * @see PersonInstrumentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonInstrumentResourceIntTest {

  private static final LocalDate DEFAULT_BEGIN_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_BEGIN_DATE = LocalDate.now(ZoneId.systemDefault());

  private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

  @Inject
  private PersonInstrumentRepository personInstrumentRepository;

  @Inject
  private PersonInstrumentMapper personInstrumentMapper;

  @Inject
  private PersonInstrumentService personInstrumentService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restPersonInstrumentMockMvc;

  private PersonInstrument personInstrument;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    PersonInstrumentResource personInstrumentResource = new PersonInstrumentResource();
    ReflectionTestUtils.setField(personInstrumentResource, "personInstrumentService",
        personInstrumentService);
    this.restPersonInstrumentMockMvc = MockMvcBuilders.standaloneSetup(personInstrumentResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static PersonInstrument createEntity(EntityManager em) {
    PersonInstrument personInstrument = new PersonInstrument().beginDate(DEFAULT_BEGIN_DATE)
        .endDate(DEFAULT_END_DATE);
    return personInstrument;
  }

  @Before
  public void initTest() {
    personInstrument = createEntity(em);
  }

  @Test
  @Transactional
  public void createPersonInstrument() throws Exception {
    int databaseSizeBeforeCreate = personInstrumentRepository.findAll().size();

    // Create the PersonInstrument
    PersonInstrumentDTO personInstrumentDTO = personInstrumentMapper
        .personInstrumentToPersonInstrumentDTO(personInstrument);

    restPersonInstrumentMockMvc
        .perform(post("/api/person-instruments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personInstrumentDTO)))
        .andExpect(status().isCreated());

    // Validate the PersonInstrument in the database
    List<PersonInstrument> personInstruments = personInstrumentRepository.findAll();
    assertThat(personInstruments).hasSize(databaseSizeBeforeCreate + 1);
    PersonInstrument testPersonInstrument = personInstruments.get(personInstruments.size() - 1);
    assertThat(testPersonInstrument.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
    assertThat(testPersonInstrument.getEndDate()).isEqualTo(DEFAULT_END_DATE);
  }

  @Test
  @Transactional
  public void checkBeginDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = personInstrumentRepository.findAll().size();
    // set the field null
    personInstrument.setBeginDate(null);

    // Create the PersonInstrument, which fails.
    PersonInstrumentDTO personInstrumentDTO = personInstrumentMapper
        .personInstrumentToPersonInstrumentDTO(personInstrument);

    restPersonInstrumentMockMvc
        .perform(post("/api/person-instruments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personInstrumentDTO)))
        .andExpect(status().isBadRequest());

    List<PersonInstrument> personInstruments = personInstrumentRepository.findAll();
    assertThat(personInstruments).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllPersonInstruments() throws Exception {
    // Initialize the database
    personInstrumentRepository.saveAndFlush(personInstrument);

    // Get all the personInstruments
    restPersonInstrumentMockMvc.perform(get("/api/person-instruments?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(personInstrument.getId().intValue())))
        .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
        .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
  }

  @Test
  @Transactional
  public void getPersonInstrument() throws Exception {
    // Initialize the database
    personInstrumentRepository.saveAndFlush(personInstrument);

    // Get the personInstrument
    restPersonInstrumentMockMvc
        .perform(get("/api/person-instruments/{id}", personInstrument.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(personInstrument.getId().intValue()))
        .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE.toString()))
        .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingPersonInstrument() throws Exception {
    // Get the personInstrument
    restPersonInstrumentMockMvc.perform(get("/api/person-instruments/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updatePersonInstrument() throws Exception {
    // Initialize the database
    personInstrumentRepository.saveAndFlush(personInstrument);
    int databaseSizeBeforeUpdate = personInstrumentRepository.findAll().size();

    // Update the personInstrument
    PersonInstrument updatedPersonInstrument = personInstrumentRepository
        .findById(personInstrument.getId()).orElse(null);
    updatedPersonInstrument.beginDate(UPDATED_BEGIN_DATE).endDate(UPDATED_END_DATE);
    PersonInstrumentDTO personInstrumentDTO = personInstrumentMapper
        .personInstrumentToPersonInstrumentDTO(updatedPersonInstrument);

    restPersonInstrumentMockMvc
        .perform(put("/api/person-instruments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personInstrumentDTO)))
        .andExpect(status().isOk());

    // Validate the PersonInstrument in the database
    List<PersonInstrument> personInstruments = personInstrumentRepository.findAll();
    assertThat(personInstruments).hasSize(databaseSizeBeforeUpdate);
    PersonInstrument testPersonInstrument = personInstruments.get(personInstruments.size() - 1);
    assertThat(testPersonInstrument.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
    assertThat(testPersonInstrument.getEndDate()).isEqualTo(UPDATED_END_DATE);
  }

  @Test
  @Transactional
  public void deletePersonInstrument() throws Exception {
    // Initialize the database
    personInstrumentRepository.saveAndFlush(personInstrument);
    int databaseSizeBeforeDelete = personInstrumentRepository.findAll().size();

    // Get the personInstrument
    restPersonInstrumentMockMvc
        .perform(delete("/api/person-instruments/{id}", personInstrument.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<PersonInstrument> personInstruments = personInstrumentRepository.findAll();
    assertThat(personInstruments).hasSize(databaseSizeBeforeDelete - 1);
  }
}
