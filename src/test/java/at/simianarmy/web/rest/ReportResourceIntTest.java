package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Report;
import at.simianarmy.repository.ReportRepository;
import at.simianarmy.service.ReportService;
import at.simianarmy.service.dto.ReportDTO;
import at.simianarmy.service.mapper.ReportMapper;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the ReportResource REST controller.
 *
 * @see ReportResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ReportResourceIntTest {

  private static final ZonedDateTime DEFAULT_GENERATION_DATE = ZonedDateTime
      .ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
  private static final ZonedDateTime UPDATED_GENERATION_DATE = ZonedDateTime
      .now(ZoneId.systemDefault()).withNano(0);
  private static final String DEFAULT_GENERATION_DATE_STR = DateTimeFormatter.ISO_OFFSET_DATE_TIME
      .format(DEFAULT_GENERATION_DATE);

  private static final String DEFAULT_ASSOCIATION_ID = "AAAAA";
  private static final String UPDATED_ASSOCIATION_ID = "BBBBB";

  private static final String DEFAULT_ASSOCIATION_NAME = "AAAAA";
  private static final String UPDATED_ASSOCIATION_NAME = "BBBBB";

  private static final String DEFAULT_STREET_ADDRESS = "AAAAA";
  private static final String UPDATED_STREET_ADDRESS = "BBBBB";

  private static final String DEFAULT_POSTAL_CODE = "AAAAA";
  private static final String UPDATED_POSTAL_CODE = "BBBBB";

  private static final String DEFAULT_CITY = "AAAAA";
  private static final String UPDATED_CITY = "BBBBB";

  @Inject
  private ReportRepository reportRepository;

  @Inject
  private ReportMapper reportMapper;

  @Inject
  private ReportService reportService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restReportMockMvc;

  private Report report;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ReportResource reportResource = new ReportResource();
    ReflectionTestUtils.setField(reportResource, "reportService", reportService);
    this.restReportMockMvc = MockMvcBuilders.standaloneSetup(reportResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Report createEntity(EntityManager em) {
    Report report = new Report().generationDate(DEFAULT_GENERATION_DATE)
        .associationId(DEFAULT_ASSOCIATION_ID).associationName(DEFAULT_ASSOCIATION_NAME)
        .streetAddress(DEFAULT_STREET_ADDRESS).postalCode(DEFAULT_POSTAL_CODE).city(DEFAULT_CITY);
    return report;
  }

  @Before
  public void initTest() {
    report = createEntity(em);
  }

  @Test
  @Transactional
  public void createReport() throws Exception {
    int databaseSizeBeforeCreate = reportRepository.findAll().size();

    // Create the Report
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc.perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(reportDTO))).andExpect(status().isCreated());

    // Validate the Report in the database
    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeCreate + 1);
    Report testReport = reports.get(reports.size() - 1);
    assertThat(testReport.getGenerationDate()).isEqualTo(DEFAULT_GENERATION_DATE);
    assertThat(testReport.getAssociationId()).isEqualTo(DEFAULT_ASSOCIATION_ID);
    assertThat(testReport.getAssociationName()).isEqualTo(DEFAULT_ASSOCIATION_NAME);
    assertThat(testReport.getStreetAddress()).isEqualTo(DEFAULT_STREET_ADDRESS);
    assertThat(testReport.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
    assertThat(testReport.getCity()).isEqualTo(DEFAULT_CITY);
  }

  @Test
  @Transactional
  public void checkGenerationDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setGenerationDate(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkAssociationIdIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setAssociationId(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkAssociationNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setAssociationName(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkStreetAddressIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setStreetAddress(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkPostalCodeIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setPostalCode(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkCityIsRequired() throws Exception {
    int databaseSizeBeforeTest = reportRepository.findAll().size();
    // set the field null
    report.setCity(null);

    // Create the Report, which fails.
    ReportDTO reportDTO = reportMapper.reportToReportDTO(report);

    restReportMockMvc
        .perform(post("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(reportDTO)))
        .andExpect(status().isBadRequest());

    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllReports() throws Exception {
    // Initialize the database
    reportRepository.saveAndFlush(report);

    // Get all the reports
    restReportMockMvc.perform(get("/api/reports?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(report.getId().intValue())))
        .andExpect(jsonPath("$.[*].generationDate").value(hasItem(DEFAULT_GENERATION_DATE_STR)))
        .andExpect(
            jsonPath("$.[*].associationId").value(hasItem(DEFAULT_ASSOCIATION_ID.toString())))
        .andExpect(
            jsonPath("$.[*].associationName").value(hasItem(DEFAULT_ASSOCIATION_NAME.toString())))
        .andExpect(
            jsonPath("$.[*].streetAddress").value(hasItem(DEFAULT_STREET_ADDRESS.toString())))
        .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
        .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())));
  }

  @Test
  @Transactional
  public void getReport() throws Exception {
    // Initialize the database
    reportRepository.saveAndFlush(report);

    // Get the report
    restReportMockMvc.perform(get("/api/reports/{id}", report.getId())).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(report.getId().intValue()))
        .andExpect(jsonPath("$.generationDate").value(DEFAULT_GENERATION_DATE_STR))
        .andExpect(jsonPath("$.associationId").value(DEFAULT_ASSOCIATION_ID.toString()))
        .andExpect(jsonPath("$.associationName").value(DEFAULT_ASSOCIATION_NAME.toString()))
        .andExpect(jsonPath("$.streetAddress").value(DEFAULT_STREET_ADDRESS.toString()))
        .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
        .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingReport() throws Exception {
    // Get the report
    restReportMockMvc.perform(get("/api/reports/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateReport() throws Exception {
    // Initialize the database
    reportRepository.saveAndFlush(report);
    int databaseSizeBeforeUpdate = reportRepository.findAll().size();

    // Update the report
    Report updatedReport = reportRepository.findById(report.getId()).orElse(null);
    updatedReport.generationDate(UPDATED_GENERATION_DATE).associationId(UPDATED_ASSOCIATION_ID)
        .associationName(UPDATED_ASSOCIATION_NAME).streetAddress(UPDATED_STREET_ADDRESS)
        .postalCode(UPDATED_POSTAL_CODE).city(UPDATED_CITY);
    ReportDTO reportDTO = reportMapper.reportToReportDTO(updatedReport);

    restReportMockMvc.perform(put("/api/reports").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(reportDTO))).andExpect(status().isOk());

    // Validate the Report in the database
    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeUpdate);
    Report testReport = reports.get(reports.size() - 1);
    assertThat(testReport.getGenerationDate()).isEqualTo(UPDATED_GENERATION_DATE);
    assertThat(testReport.getAssociationId()).isEqualTo(UPDATED_ASSOCIATION_ID);
    assertThat(testReport.getAssociationName()).isEqualTo(UPDATED_ASSOCIATION_NAME);
    assertThat(testReport.getStreetAddress()).isEqualTo(UPDATED_STREET_ADDRESS);
    assertThat(testReport.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
    assertThat(testReport.getCity()).isEqualTo(UPDATED_CITY);
  }

  @Test
  @Transactional
  public void deleteReport() throws Exception {
    // Initialize the database
    reportRepository.saveAndFlush(report);
    int databaseSizeBeforeDelete = reportRepository.findAll().size();

    // Get the report
    restReportMockMvc
        .perform(delete("/api/reports/{id}", report.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Report> reports = reportRepository.findAll();
    assertThat(reports).hasSize(databaseSizeBeforeDelete - 1);
  }
}
