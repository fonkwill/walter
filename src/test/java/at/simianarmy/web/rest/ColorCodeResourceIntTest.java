package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.service.ColorCodeService;
import at.simianarmy.service.dto.ColorCodeDTO;
import at.simianarmy.service.mapper.ColorCodeMapper;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the ColorCodeResource REST controller.
 *
 * @see ColorCodeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ColorCodeResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAAAAAAA";
  private static final String UPDATED_NAME = "BBBBBBBBBB";

  @Inject
  private ColorCodeRepository colorCodeRepository;

  @Inject
  private ColorCodeMapper colorCodeMapper;

  @Inject
  private ColorCodeService colorCodeService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restColorCodeMockMvc;

  private ColorCode colorCode;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ColorCodeResource colorCodeResource = new ColorCodeResource();
    ReflectionTestUtils.setField(colorCodeResource, "colorCodeService", colorCodeService);
    this.restColorCodeMockMvc = MockMvcBuilders.standaloneSetup(colorCodeResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static ColorCode createEntity(EntityManager em) {
    ColorCode colorCode = new ColorCode().name(DEFAULT_NAME);
    return colorCode;
  }

  @Before
  public void initTest() {
    colorCode = createEntity(em);
  }

  @Test
  @Transactional
  public void createColorCode() throws Exception {
    int databaseSizeBeforeCreate = colorCodeRepository.findAll().size();

    // Create the ColorCode
    ColorCodeDTO colorCodeDTO = colorCodeMapper.colorCodeToColorCodeDTO(colorCode);

    restColorCodeMockMvc
        .perform(post("/api/color-codes").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colorCodeDTO)))
        .andExpect(status().isCreated());

    // Validate the ColorCode in the database
    List<ColorCode> colorCodes = colorCodeRepository.findAll();
    assertThat(colorCodes).hasSize(databaseSizeBeforeCreate + 1);
    ColorCode testColorCode = colorCodes.get(colorCodes.size() - 1);
    assertThat(testColorCode.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = colorCodeRepository.findAll().size();
    // set the field null
    colorCode.setName(null);

    // Create the ColorCode, which fails.
    ColorCodeDTO colorCodeDTO = colorCodeMapper.colorCodeToColorCodeDTO(colorCode);

    restColorCodeMockMvc
        .perform(post("/api/color-codes").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(colorCodeDTO)))
        .andExpect(status().isBadRequest());

    List<ColorCode> colorCodes = colorCodeRepository.findAll();
    assertThat(colorCodes).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllColorCodes() throws Exception {
    // Initialize the database
    colorCodeRepository.saveAndFlush(colorCode);

    // Get all the colorCodes
    restColorCodeMockMvc.perform(get("/api/color-codes?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(colorCode.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getColorCode() throws Exception {
    // Initialize the database
    colorCodeRepository.saveAndFlush(colorCode);

    // Get the colorCode
    restColorCodeMockMvc.perform(get("/api/color-codes/{id}", colorCode.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(colorCode.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingColorCode() throws Exception {
    // Get the colorCode
    restColorCodeMockMvc.perform(get("/api/color-codes/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateColorCode() throws Exception {
    // Initialize the database
    colorCodeRepository.saveAndFlush(colorCode);
    int databaseSizeBeforeUpdate = colorCodeRepository.findAll().size();

    // Update the colorCode
    ColorCode updatedColorCode = colorCodeRepository.findById(colorCode.getId()).orElse(null);
    updatedColorCode.name(UPDATED_NAME);
    ColorCodeDTO colorCodeDTO = colorCodeMapper.colorCodeToColorCodeDTO(updatedColorCode);

    restColorCodeMockMvc.perform(put("/api/color-codes").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(colorCodeDTO))).andExpect(status().isOk());

    // Validate the ColorCode in the database
    List<ColorCode> colorCodes = colorCodeRepository.findAll();
    assertThat(colorCodes).hasSize(databaseSizeBeforeUpdate);
    ColorCode testColorCode = colorCodes.get(colorCodes.size() - 1);
    assertThat(testColorCode.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteColorCode() throws Exception {
    // Initialize the database
    colorCodeRepository.saveAndFlush(colorCode);
    int databaseSizeBeforeDelete = colorCodeRepository.findAll().size();

    // Get the colorCode
    restColorCodeMockMvc.perform(
        delete("/api/color-codes/{id}", colorCode.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<ColorCode> colorCodes = colorCodeRepository.findAll();
    assertThat(colorCodes).hasSize(databaseSizeBeforeDelete - 1);
  }
}
