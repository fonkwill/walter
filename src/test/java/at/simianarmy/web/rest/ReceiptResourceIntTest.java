package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.Receipt;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;
import at.simianarmy.repository.ReceiptRepository;
import at.simianarmy.service.ReceiptService;

import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReceiptResource REST controller.
 *
 * @see ReceiptResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ReceiptResourceIntTest {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);

    private static final String DEFAULT_NOTE = "AAAAA";

    private static final String DEFAULT_TITLE = "AAAAA";

    private static final UUID DEFAULT_FILE = UUID.randomUUID();

    private static final String DEFAULT_IDENTIFIER = "A123";

    private static final String CASHBOOK_ACCOUNT_NAME = "ACCOUNT";

    private static final BankImporterType DEFAULT_BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;

    @Inject
    private ReceiptRepository receiptRepository;

    @Inject
    private CashbookEntryRepository cashbookEntryRepository;

    @Inject
    private CashbookAccountRepository cashbookAccountRepository;

    @Inject
    private ReceiptService receiptService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restReceiptMockMvc;

    private Receipt receipt;

    private CashbookEntry cashbookEntry;

    private CashbookAccount cashbookAccount;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReceiptResource receiptResource = new ReceiptResource();
        ReflectionTestUtils.setField(receiptResource, "receiptService", receiptService);
        this.restReceiptMockMvc = MockMvcBuilders.standaloneSetup(receiptResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Receipt createEntity(EntityManager em) {
        Receipt receipt = new Receipt()
                .date(DEFAULT_DATE)
                .note(DEFAULT_NOTE)
                .title(DEFAULT_TITLE)
                .file(DEFAULT_FILE.toString())
                .identifier(DEFAULT_IDENTIFIER)
                .overwriteIdentifier(true);
        return receipt;
    }

    public static CashbookEntry createCashbookEntry(EntityManager em) {
      CashbookEntry cashbookEntry = new CashbookEntry()
        .title("Test")
        .amount(new BigDecimal(10.0))
        .date(LocalDate.MIN)
        .type(CashbookEntryType.INCOME);
      return cashbookEntry;
    }

  public static CashbookAccount createCashbookAccount(EntityManager em) {
    CashbookAccount cashbookAccount = new CashbookAccount()
      .name(CASHBOOK_ACCOUNT_NAME)
      .bankAccount(true)
      .bankImporterType(DEFAULT_BANK_IMPORTER_TYPE)
      .receiptCode("BA")
      .currentNumber(0)
      .defaultForCashbook(false);

    return cashbookAccount;
  }

    @Before
    public void initTest() {
        cashbookEntry = createCashbookEntry(em);
        receipt = createEntity(em);
        cashbookAccount = createCashbookAccount(em);
        this.cashbookAccountRepository.save(cashbookAccount);
        cashbookEntry.setCashbookAccount(cashbookAccount);
    }

    @Test
    @Transactional
    public void getAllReceipts() throws Exception {
        // Initialize the database
        cashbookEntryRepository.saveAndFlush(cashbookEntry);
        receipt.setInitialCashbookEntry(cashbookEntry);
        receiptRepository.saveAndFlush(receipt);

        // Get all the receipts
        restReceiptMockMvc.perform(get("/api/receipts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(receipt.getId().intValue())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].file").value(hasItem(DEFAULT_FILE.toString())))
                .andExpect(jsonPath("$.[*].identifier").value(hasItem(DEFAULT_IDENTIFIER.toString())));
    }

    @Test
    @Transactional
    public void getReceipt() throws Exception {
        // Initialize the database
      cashbookEntryRepository.saveAndFlush(cashbookEntry);
      receipt.setInitialCashbookEntry(cashbookEntry);
      receiptRepository.saveAndFlush(receipt);

        // Get the receipt
        restReceiptMockMvc.perform(get("/api/receipts/{id}", receipt.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(receipt.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.file").value(DEFAULT_FILE.toString()))
            .andExpect(jsonPath("$.identifier").value(DEFAULT_IDENTIFIER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReceipt() throws Exception {
        // Get the receipt
        restReceiptMockMvc.perform(get("/api/receipts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteReceipt() throws Exception {
        // Initialize the database
        cashbookEntryRepository.saveAndFlush(cashbookEntry);
        receipt.setInitialCashbookEntry(cashbookEntry);
        receiptRepository.saveAndFlush(receipt);
        int databaseSizeBeforeDelete = receiptRepository.findAll().size();

        // Get the receipt
        restReceiptMockMvc.perform(delete("/api/receipts/{id}", receipt.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Receipt> receipts = receiptRepository.findAll();
        assertThat(receipts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
