package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonAward;
import at.simianarmy.repository.PersonAwardRepository;
import at.simianarmy.service.PersonAwardService;
import at.simianarmy.service.dto.PersonAwardDTO;
import at.simianarmy.service.mapper.PersonAwardMapper;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the PersonAwardResource REST controller.
 *
 * @see PersonAwardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonAwardResourceIntTest {

  private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

  @Inject
  private PersonAwardRepository personAwardRepository;

  @Inject
  private PersonAwardMapper personAwardMapper;

  @Inject
  private PersonAwardService personAwardService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restPersonAwardMockMvc;

  private PersonAward personAward;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    PersonAwardResource personAwardResource = new PersonAwardResource();
    ReflectionTestUtils.setField(personAwardResource, "personAwardService", personAwardService);
    this.restPersonAwardMockMvc = MockMvcBuilders.standaloneSetup(personAwardResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static PersonAward createEntity(EntityManager em) {
    PersonAward personAward = new PersonAward().date(DEFAULT_DATE);
    return personAward;
  }

  @Before
  public void initTest() {
    personAward = createEntity(em);
  }

  @Test
  @Transactional
  public void createPersonAward() throws Exception {
    int databaseSizeBeforeCreate = personAwardRepository.findAll().size();

    // Create the PersonAward
    PersonAwardDTO personAwardDTO = personAwardMapper.personAwardToPersonAwardDTO(personAward);

    restPersonAwardMockMvc
        .perform(post("/api/person-awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personAwardDTO)))
        .andExpect(status().isCreated());

    // Validate the PersonAward in the database
    List<PersonAward> personAwards = personAwardRepository.findAll();
    assertThat(personAwards).hasSize(databaseSizeBeforeCreate + 1);
    PersonAward testPersonAward = personAwards.get(personAwards.size() - 1);
    assertThat(testPersonAward.getDate()).isEqualTo(DEFAULT_DATE);
  }

  @Test
  @Transactional
  public void checkDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = personAwardRepository.findAll().size();
    // set the field null
    personAward.setDate(null);

    // Create the PersonAward, which fails.
    PersonAwardDTO personAwardDTO = personAwardMapper.personAwardToPersonAwardDTO(personAward);

    restPersonAwardMockMvc
        .perform(post("/api/person-awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personAwardDTO)))
        .andExpect(status().isBadRequest());

    List<PersonAward> personAwards = personAwardRepository.findAll();
    assertThat(personAwards).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllPersonAwards() throws Exception {
    // Initialize the database
    personAwardRepository.saveAndFlush(personAward);

    // Get all the personAwards
    restPersonAwardMockMvc.perform(get("/api/person-awards?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(personAward.getId().intValue())))
        .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
  }

  @Test
  @Transactional
  public void getPersonAward() throws Exception {
    // Initialize the database
    personAwardRepository.saveAndFlush(personAward);

    // Get the personAward
    restPersonAwardMockMvc.perform(get("/api/person-awards/{id}", personAward.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(personAward.getId().intValue()))
        .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingPersonAward() throws Exception {
    // Get the personAward
    restPersonAwardMockMvc.perform(get("/api/person-awards/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updatePersonAward() throws Exception {
    // Initialize the database
    personAwardRepository.saveAndFlush(personAward);
    int databaseSizeBeforeUpdate = personAwardRepository.findAll().size();

    // Update the personAward
    PersonAward updatedPersonAward = personAwardRepository.findById(personAward.getId()).orElse(null);
    updatedPersonAward.date(UPDATED_DATE);
    PersonAwardDTO personAwardDTO = personAwardMapper
        .personAwardToPersonAwardDTO(updatedPersonAward);

    restPersonAwardMockMvc
        .perform(put("/api/person-awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personAwardDTO)))
        .andExpect(status().isOk());

    // Validate the PersonAward in the database
    List<PersonAward> personAwards = personAwardRepository.findAll();
    assertThat(personAwards).hasSize(databaseSizeBeforeUpdate);
    PersonAward testPersonAward = personAwards.get(personAwards.size() - 1);
    assertThat(testPersonAward.getDate()).isEqualTo(UPDATED_DATE);
  }

  @Test
  @Transactional
  public void deletePersonAward() throws Exception {
    // Initialize the database
    personAwardRepository.saveAndFlush(personAward);
    int databaseSizeBeforeDelete = personAwardRepository.findAll().size();

    // Get the personAward
    restPersonAwardMockMvc.perform(delete("/api/person-awards/{id}", personAward.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<PersonAward> personAwards = personAwardRepository.findAll();
    assertThat(personAwards).hasSize(databaseSizeBeforeDelete - 1);
  }
}
