package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.PersonClothing;
import at.simianarmy.repository.PersonClothingRepository;
import at.simianarmy.service.PersonClothingService;
import at.simianarmy.service.dto.PersonClothingDTO;
import at.simianarmy.service.mapper.PersonClothingMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonClothingResource REST controller.
 *
 * @see PersonClothingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonClothingResourceIntTest {

    private static final LocalDate DEFAULT_BEGIN_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BEGIN_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private PersonClothingRepository personClothingRepository;

    @Inject
    private PersonClothingMapper personClothingMapper;

    @Inject
    private PersonClothingService personClothingService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPersonClothingMockMvc;

    private PersonClothing personClothing;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonClothingResource personClothingResource = new PersonClothingResource();
        ReflectionTestUtils.setField(personClothingResource, "personClothingService", personClothingService);
        this.restPersonClothingMockMvc = MockMvcBuilders.standaloneSetup(personClothingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonClothing createEntity(EntityManager em) {
        PersonClothing personClothing = new PersonClothing()
                .beginDate(DEFAULT_BEGIN_DATE)
                .endDate(DEFAULT_END_DATE);
        return personClothing;
    }

    @Before
    public void initTest() {
        personClothing = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonClothing() throws Exception {
        int databaseSizeBeforeCreate = personClothingRepository.findAll().size();

        // Create the PersonClothing
        PersonClothingDTO personClothingDTO = personClothingMapper.personClothingToPersonClothingDTO(personClothing);

        restPersonClothingMockMvc.perform(post("/api/person-clothings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personClothingDTO)))
                .andExpect(status().isCreated());

        // Validate the PersonClothing in the database
        List<PersonClothing> personClothings = personClothingRepository.findAll();
        assertThat(personClothings).hasSize(databaseSizeBeforeCreate + 1);
        PersonClothing testPersonClothing = personClothings.get(personClothings.size() - 1);
        assertThat(testPersonClothing.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
        assertThat(testPersonClothing.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void checkBeginDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = personClothingRepository.findAll().size();
        // set the field null
        personClothing.setBeginDate(null);

        // Create the PersonClothing, which fails.
        PersonClothingDTO personClothingDTO = personClothingMapper.personClothingToPersonClothingDTO(personClothing);

        restPersonClothingMockMvc.perform(post("/api/person-clothings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personClothingDTO)))
                .andExpect(status().isBadRequest());

        List<PersonClothing> personClothings = personClothingRepository.findAll();
        assertThat(personClothings).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPersonClothings() throws Exception {
        // Initialize the database
        personClothingRepository.saveAndFlush(personClothing);

        // Get all the personClothings
        restPersonClothingMockMvc.perform(get("/api/person-clothings?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(personClothing.getId().intValue())))
                .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
                .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPersonClothing() throws Exception {
        // Initialize the database
        personClothingRepository.saveAndFlush(personClothing);

        // Get the personClothing
        restPersonClothingMockMvc.perform(get("/api/person-clothings/{id}", personClothing.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personClothing.getId().intValue()))
            .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonClothing() throws Exception {
        // Get the personClothing
        restPersonClothingMockMvc.perform(get("/api/person-clothings/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonClothing() throws Exception {
        // Initialize the database
        personClothingRepository.saveAndFlush(personClothing);
        int databaseSizeBeforeUpdate = personClothingRepository.findAll().size();

        // Update the personClothing
        PersonClothing updatedPersonClothing = personClothingRepository.findById(personClothing.getId()).orElse(null);
        updatedPersonClothing
                .beginDate(UPDATED_BEGIN_DATE)
                .endDate(UPDATED_END_DATE);
        PersonClothingDTO personClothingDTO = personClothingMapper.personClothingToPersonClothingDTO(updatedPersonClothing);

        restPersonClothingMockMvc.perform(put("/api/person-clothings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(personClothingDTO)))
                .andExpect(status().isOk());

        // Validate the PersonClothing in the database
        List<PersonClothing> personClothings = personClothingRepository.findAll();
        assertThat(personClothings).hasSize(databaseSizeBeforeUpdate);
        PersonClothing testPersonClothing = personClothings.get(personClothings.size() - 1);
        assertThat(testPersonClothing.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
        assertThat(testPersonClothing.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void deletePersonClothing() throws Exception {
        // Initialize the database
        personClothingRepository.saveAndFlush(personClothing);
        int databaseSizeBeforeDelete = personClothingRepository.findAll().size();

        // Get the personClothing
        restPersonClothingMockMvc.perform(delete("/api/person-clothings/{id}", personClothing.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonClothing> personClothings = personClothingRepository.findAll();
        assertThat(personClothings).hasSize(databaseSizeBeforeDelete - 1);
    }
}
