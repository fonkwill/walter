package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.Template;
import at.simianarmy.repository.TemplateRepository;
import at.simianarmy.service.TemplateService;
import at.simianarmy.service.dto.TemplateDTO;
import at.simianarmy.service.mapper.TemplateMapper;

import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TemplateResource REST controller.
 *
 * @see TemplateResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class TemplateResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";

    private static final UUID DEFAULT_FILE = UUID.randomUUID();

    private static final Float DEFAULT_MARGIN_LEFT = 1F;

    private static final Float DEFAULT_MARGIN_RIGHT = 1F;

    private static final Float DEFAULT_MARGIN_TOP = 1F;

    private static final Float DEFAULT_MARGIN_BOTTOM = 1F;

    @Inject
    private TemplateRepository templateRepository;

    @Inject
    private TemplateMapper templateMapper;

    @Inject
    private TemplateService templateService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restTemplateMockMvc;

    private Template template;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TemplateResource templateResource = new TemplateResource();
        ReflectionTestUtils.setField(templateResource, "templateService", templateService);
        this.restTemplateMockMvc = MockMvcBuilders.standaloneSetup(templateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Template createEntity(EntityManager em) {
        Template template = new Template()
                .title(DEFAULT_TITLE)
                .file(DEFAULT_FILE.toString())
                .marginLeft(DEFAULT_MARGIN_LEFT)
                .marginRight(DEFAULT_MARGIN_RIGHT)
                .marginTop(DEFAULT_MARGIN_TOP)
                .marginBottom(DEFAULT_MARGIN_BOTTOM);
        return template;
    }

    @Before
    public void initTest() {
        template = createEntity(em);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = templateRepository.findAll().size();
        // set the field null
        template.setTitle(null);

        // Create the Template, which fails.
        TemplateDTO templateDTO = templateMapper.templateToTemplateDTO(template);

        restTemplateMockMvc.perform(post("/api/templates")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(templateDTO)))
                .andExpect(status().isBadRequest());

        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTemplates() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get all the templates
        restTemplateMockMvc.perform(get("/api/templates?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(template.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].file").value(hasItem(DEFAULT_FILE.toString())))
                .andExpect(jsonPath("$.[*].marginLeft").value(hasItem(DEFAULT_MARGIN_LEFT.doubleValue())))
                .andExpect(jsonPath("$.[*].marginRight").value(hasItem(DEFAULT_MARGIN_RIGHT.doubleValue())))
                .andExpect(jsonPath("$.[*].marginTop").value(hasItem(DEFAULT_MARGIN_TOP.doubleValue())))
                .andExpect(jsonPath("$.[*].marginBottom").value(hasItem(DEFAULT_MARGIN_BOTTOM.doubleValue())));
    }

    @Test
    @Transactional
    public void getTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);

        // Get the template
        restTemplateMockMvc.perform(get("/api/templates/{id}", template.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(template.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.file").value(DEFAULT_FILE.toString()))
            .andExpect(jsonPath("$.marginLeft").value(DEFAULT_MARGIN_LEFT.doubleValue()))
            .andExpect(jsonPath("$.marginRight").value(DEFAULT_MARGIN_RIGHT.doubleValue()))
            .andExpect(jsonPath("$.marginTop").value(DEFAULT_MARGIN_TOP.doubleValue()))
            .andExpect(jsonPath("$.marginBottom").value(DEFAULT_MARGIN_BOTTOM.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTemplate() throws Exception {
        // Get the template
        restTemplateMockMvc.perform(get("/api/templates/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void deleteTemplate() throws Exception {
        // Initialize the database
        templateRepository.saveAndFlush(template);
        int databaseSizeBeforeDelete = templateRepository.findAll().size();

        // Get the template
        restTemplateMockMvc.perform(delete("/api/templates/{id}", template.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Template> templates = templateRepository.findAll();
        assertThat(templates).hasSize(databaseSizeBeforeDelete - 1);
    }
}
