package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Genre;
import at.simianarmy.repository.GenreRepository;
import at.simianarmy.service.GenreService;
import at.simianarmy.service.dto.GenreDTO;
import at.simianarmy.service.mapper.GenreMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the GenreResource REST controller.
 *
 * @see GenreResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class GenreResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  @Inject
  private GenreRepository genreRepository;

  @Inject
  private GenreMapper genreMapper;

  @Inject
  private GenreService genreService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restGenreMockMvc;

  private Genre genre;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    GenreResource genreResource = new GenreResource();
    ReflectionTestUtils.setField(genreResource, "genreService", genreService);
    this.restGenreMockMvc = MockMvcBuilders.standaloneSetup(genreResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Genre createEntity(EntityManager em) {
    Genre genre = new Genre().name(DEFAULT_NAME);
    return genre;
  }

  @Before
  public void initTest() {
    genre = createEntity(em);
  }

  @Test
  @Transactional
  public void createGenre() throws Exception {
    int databaseSizeBeforeCreate = genreRepository.findAll().size();

    // Create the Genre
    GenreDTO genreDTO = genreMapper.genreToGenreDTO(genre);

    restGenreMockMvc.perform(post("/api/genres").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(genreDTO))).andExpect(status().isCreated());

    // Validate the Genre in the database
    List<Genre> genres = genreRepository.findAll();
    assertThat(genres).hasSize(databaseSizeBeforeCreate + 1);
    Genre testGenre = genres.get(genres.size() - 1);
    assertThat(testGenre.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = genreRepository.findAll().size();
    // set the field null
    genre.setName(null);

    // Create the Genre, which fails.
    GenreDTO genreDTO = genreMapper.genreToGenreDTO(genre);

    restGenreMockMvc
        .perform(post("/api/genres").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(genreDTO)))
        .andExpect(status().isBadRequest());

    List<Genre> genres = genreRepository.findAll();
    assertThat(genres).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllGenres() throws Exception {
    // Initialize the database
    genreRepository.saveAndFlush(genre);

    // Get all the genres
    restGenreMockMvc.perform(get("/api/genres?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(genre.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getGenre() throws Exception {
    // Initialize the database
    genreRepository.saveAndFlush(genre);

    // Get the genre
    restGenreMockMvc.perform(get("/api/genres/{id}", genre.getId())).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(genre.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingGenre() throws Exception {
    // Get the genre
    restGenreMockMvc.perform(get("/api/genres/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateGenre() throws Exception {
    // Initialize the database
    genreRepository.saveAndFlush(genre);
    int databaseSizeBeforeUpdate = genreRepository.findAll().size();

    // Update the genre
    Genre updatedGenre = genreRepository.findById(genre.getId()).orElse(null);
    updatedGenre.name(UPDATED_NAME);
    GenreDTO genreDTO = genreMapper.genreToGenreDTO(updatedGenre);

    restGenreMockMvc.perform(put("/api/genres").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(genreDTO))).andExpect(status().isOk());

    // Validate the Genre in the database
    List<Genre> genres = genreRepository.findAll();
    assertThat(genres).hasSize(databaseSizeBeforeUpdate);
    Genre testGenre = genres.get(genres.size() - 1);
    assertThat(testGenre.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteGenre() throws Exception {
    // Initialize the database
    genreRepository.saveAndFlush(genre);
    int databaseSizeBeforeDelete = genreRepository.findAll().size();

    // Get the genre
    restGenreMockMvc
        .perform(delete("/api/genres/{id}", genre.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Genre> genres = genreRepository.findAll();
    assertThat(genres).hasSize(databaseSizeBeforeDelete - 1);
  }
}
