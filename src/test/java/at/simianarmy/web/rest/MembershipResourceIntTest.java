package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Person;
import at.simianarmy.repository.MembershipRepository;
import at.simianarmy.service.MembershipService;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.mapper.MembershipMapper;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the MembershipResource REST controller.
 *
 * @see MembershipResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipResourceIntTest {

  private static final LocalDate DEFAULT_BEGIN_DATE = LocalDate.of(2017, 1, 1);
  private static final LocalDate UPDATED_BEGIN_DATE = LocalDate.of(2018, 1, 1);

  private static final LocalDate DEFAULT_END_DATE = LocalDate.of(2017, 12, 31);
  private static final LocalDate UPDATED_END_DATE = LocalDate.of(2018, 12, 31);

  @Inject
  private MembershipRepository membershipRepository;

  @Inject
  private MembershipMapper membershipMapper;

  @Inject
  private MembershipService membershipService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restMembershipMockMvc;

  private Membership membership;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    MembershipResource membershipResource = new MembershipResource();
    ReflectionTestUtils.setField(membershipResource, "membershipService", membershipService);
    this.restMembershipMockMvc = MockMvcBuilders.standaloneSetup(membershipResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Membership createEntity(EntityManager em) {
    Membership membership = new Membership().beginDate(DEFAULT_BEGIN_DATE)
        .endDate(DEFAULT_END_DATE);
    Person person = PersonResourceIntTest.createEntity(em);
    em.persist(person);
    em.flush();
    membership.setPerson(person);
    return membership;
  }

  @Before
  public void initTest() {
    membership = createEntity(em);
  }

  @Test
  @Transactional
  public void createMembership() throws Exception {
    int databaseSizeBeforeCreate = membershipRepository.findAll().size();

    // Create the Membership
    MembershipDTO membershipDTO = membershipMapper.membershipToMembershipDTO(membership);

    restMembershipMockMvc
        .perform(post("/api/memberships").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipDTO)))
        .andExpect(status().isCreated());

    // Validate the Membership in the database
    List<Membership> memberships = membershipRepository.findAll();
    assertThat(memberships).hasSize(databaseSizeBeforeCreate + 1);
    Membership testMembership = memberships.get(memberships.size() - 1);
    assertThat(testMembership.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
    assertThat(testMembership.getEndDate()).isEqualTo(DEFAULT_END_DATE);
  }

  @Test
  @Transactional
  public void checkBeginDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = membershipRepository.findAll().size();
    // set the field null
    membership.setBeginDate(null);

    // Create the Membership, which fails.
    MembershipDTO membershipDTO = membershipMapper.membershipToMembershipDTO(membership);

    restMembershipMockMvc
        .perform(post("/api/memberships").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipDTO)))
        .andExpect(status().isBadRequest());

    List<Membership> memberships = membershipRepository.findAll();
    assertThat(memberships).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllMemberships() throws Exception {
    // Initialize the database
    membershipRepository.saveAndFlush(membership);

    // Get all the memberships
    restMembershipMockMvc.perform(get("/api/memberships?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(membership.getId().intValue())))
        .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
        .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
  }

  @Test
  @Transactional
  public void getMembership() throws Exception {
    // Initialize the database
    membershipRepository.saveAndFlush(membership);

    // Get the membership
    restMembershipMockMvc.perform(get("/api/memberships/{id}", membership.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(membership.getId().intValue()))
        .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE.toString()))
        .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingMembership() throws Exception {
    // Get the membership
    restMembershipMockMvc.perform(get("/api/memberships/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateMembership() throws Exception {
    // Initialize the database
    membershipRepository.saveAndFlush(membership);
    int databaseSizeBeforeUpdate = membershipRepository.findAll().size();

    // Update the membership
    Membership updatedMembership = membershipRepository.findById(membership.getId()).orElse(null);
    updatedMembership.beginDate(UPDATED_BEGIN_DATE).endDate(UPDATED_END_DATE);
    MembershipDTO membershipDTO = membershipMapper.membershipToMembershipDTO(updatedMembership);

    restMembershipMockMvc
        .perform(put("/api/memberships").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipDTO)))
        .andExpect(status().isOk());

    // Validate the Membership in the database
    List<Membership> memberships = membershipRepository.findAll();
    assertThat(memberships).hasSize(databaseSizeBeforeUpdate);
    Membership testMembership = memberships.get(memberships.size() - 1);
    assertThat(testMembership.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
    assertThat(testMembership.getEndDate()).isEqualTo(UPDATED_END_DATE);
  }

  @Test
  @Transactional
  public void deleteMembership() throws Exception {
    // Initialize the database
    membershipRepository.saveAndFlush(membership);
    int databaseSizeBeforeDelete = membershipRepository.findAll().size();

    // Get the membership
    restMembershipMockMvc.perform(
        delete("/api/memberships/{id}", membership.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Membership> memberships = membershipRepository.findAll();
    assertThat(memberships).hasSize(databaseSizeBeforeDelete - 1);
  }
}
