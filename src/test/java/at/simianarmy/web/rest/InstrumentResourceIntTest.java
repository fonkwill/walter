package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.repository.InstrumentRepository;
import at.simianarmy.service.InstrumentService;
import at.simianarmy.service.dto.InstrumentDTO;
import at.simianarmy.service.mapper.InstrumentMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InstrumentResource REST controller.
 *
 * @see InstrumentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";

    private static final LocalDate DEFAULT_PURCHASE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PURCHASE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_PRIVATE_INSTRUMENT = false;
    private static final Boolean UPDATED_PRIVATE_INSTRUMENT = true;

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    @Inject
    private InstrumentRepository instrumentRepository;

    @Inject
    private InstrumentMapper instrumentMapper;

    @Inject
    private InstrumentService instrumentService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restInstrumentMockMvc;

    private Instrument instrument;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstrumentResource instrumentResource = new InstrumentResource();
        ReflectionTestUtils.setField(instrumentResource, "instrumentService", instrumentService);
        this.restInstrumentMockMvc = MockMvcBuilders.standaloneSetup(instrumentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Instrument createEntity(EntityManager em) {
        Instrument instrument = new Instrument()
                .name(DEFAULT_NAME)
                .purchaseDate(DEFAULT_PURCHASE_DATE)
                .privateInstrument(DEFAULT_PRIVATE_INSTRUMENT)
                .price(DEFAULT_PRICE);
        // Add required entity
        InstrumentType type = InstrumentTypeResourceIntTest.createEntity(em);
        em.persist(type);
        em.flush();
        instrument.setType(type);
        return instrument;
    }

    @Before
    public void initTest() {
        instrument = createEntity(em);
    }

    @Test
    @Transactional
    public void createInstrument() throws Exception {
        int databaseSizeBeforeCreate = instrumentRepository.findAll().size();

        // Create the Instrument
        InstrumentDTO instrumentDTO = instrumentMapper.instrumentToInstrumentDTO(instrument);

        restInstrumentMockMvc.perform(post("/api/instruments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentDTO)))
                .andExpect(status().isCreated());

        // Validate the Instrument in the database
        List<Instrument> instruments = instrumentRepository.findAll();
        assertThat(instruments).hasSize(databaseSizeBeforeCreate + 1);
        Instrument testInstrument = instruments.get(instruments.size() - 1);
        assertThat(testInstrument.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInstrument.getPurchaseDate()).isEqualTo(DEFAULT_PURCHASE_DATE);
        assertThat(testInstrument.isPrivateInstrument()).isEqualTo(DEFAULT_PRIVATE_INSTRUMENT);
        assertThat(testInstrument.getPrice()).isEqualTo(DEFAULT_PRICE);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = instrumentRepository.findAll().size();
        // set the field null
        instrument.setName(null);

        // Create the Instrument, which fails.
        InstrumentDTO instrumentDTO = instrumentMapper.instrumentToInstrumentDTO(instrument);

        restInstrumentMockMvc.perform(post("/api/instruments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentDTO)))
                .andExpect(status().isBadRequest());

        List<Instrument> instruments = instrumentRepository.findAll();
        assertThat(instruments).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInstruments() throws Exception {
        // Initialize the database
        instrumentRepository.saveAndFlush(instrument);

        // Get all the instruments
        restInstrumentMockMvc.perform(get("/api/instruments?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(instrument.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].purchaseDate").value(hasItem(DEFAULT_PURCHASE_DATE.toString())))
                .andExpect(jsonPath("$.[*].privateInstrument").value(hasItem(DEFAULT_PRIVATE_INSTRUMENT.booleanValue())))
                .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())));
    }

    @Test
    @Transactional
    public void getInstrument() throws Exception {
        // Initialize the database
        instrumentRepository.saveAndFlush(instrument);

        // Get the instrument
        restInstrumentMockMvc.perform(get("/api/instruments/{id}", instrument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(instrument.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.purchaseDate").value(DEFAULT_PURCHASE_DATE.toString()))
            .andExpect(jsonPath("$.privateInstrument").value(DEFAULT_PRIVATE_INSTRUMENT.booleanValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingInstrument() throws Exception {
        // Get the instrument
        restInstrumentMockMvc.perform(get("/api/instruments/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstrument() throws Exception {
        // Initialize the database
        instrumentRepository.saveAndFlush(instrument);
        int databaseSizeBeforeUpdate = instrumentRepository.findAll().size();

        // Update the instrument
        Instrument updatedInstrument = instrumentRepository.findById(instrument.getId()).orElse(null);
        updatedInstrument
                .name(UPDATED_NAME)
                .purchaseDate(UPDATED_PURCHASE_DATE)
                .privateInstrument(UPDATED_PRIVATE_INSTRUMENT)
                .price(UPDATED_PRICE);
        InstrumentDTO instrumentDTO = instrumentMapper.instrumentToInstrumentDTO(updatedInstrument);

        restInstrumentMockMvc.perform(put("/api/instruments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentDTO)))
                .andExpect(status().isOk());

        // Validate the Instrument in the database
        List<Instrument> instruments = instrumentRepository.findAll();
        assertThat(instruments).hasSize(databaseSizeBeforeUpdate);
        Instrument testInstrument = instruments.get(instruments.size() - 1);
        assertThat(testInstrument.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInstrument.getPurchaseDate()).isEqualTo(UPDATED_PURCHASE_DATE);
        assertThat(testInstrument.isPrivateInstrument()).isEqualTo(UPDATED_PRIVATE_INSTRUMENT);
        assertThat(testInstrument.getPrice()).isEqualTo(UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void deleteInstrument() throws Exception {
        // Initialize the database
        instrumentRepository.saveAndFlush(instrument);
        int databaseSizeBeforeDelete = instrumentRepository.findAll().size();

        // Get the instrument
        restInstrumentMockMvc.perform(delete("/api/instruments/{id}", instrument.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Instrument> instruments = instrumentRepository.findAll();
        assertThat(instruments).hasSize(databaseSizeBeforeDelete - 1);
    }
}
