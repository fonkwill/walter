package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonHonor;
import at.simianarmy.repository.PersonHonorRepository;
import at.simianarmy.service.PersonHonorService;
import at.simianarmy.service.dto.PersonHonorDTO;
import at.simianarmy.service.mapper.PersonHonorMapper;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the PersonHonorResource REST controller.
 *
 * @see PersonHonorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonHonorResourceIntTest {

  private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

  @Inject
  private PersonHonorRepository personHonorRepository;

  @Inject
  private PersonHonorMapper personHonorMapper;

  @Inject
  private PersonHonorService personHonorService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restPersonHonorMockMvc;

  private PersonHonor personHonor;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    PersonHonorResource personHonorResource = new PersonHonorResource();
    ReflectionTestUtils.setField(personHonorResource, "personHonorService", personHonorService);
    this.restPersonHonorMockMvc = MockMvcBuilders.standaloneSetup(personHonorResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static PersonHonor createEntity(EntityManager em) {
    PersonHonor personHonor = new PersonHonor().date(DEFAULT_DATE);
    return personHonor;
  }

  @Before
  public void initTest() {
    personHonor = createEntity(em);
  }

  @Test
  @Transactional
  public void createPersonHonor() throws Exception {
    int databaseSizeBeforeCreate = personHonorRepository.findAll().size();

    // Create the PersonHonor
    PersonHonorDTO personHonorDTO = personHonorMapper.personHonorToPersonHonorDTO(personHonor);

    restPersonHonorMockMvc
        .perform(post("/api/person-honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personHonorDTO)))
        .andExpect(status().isCreated());

    // Validate the PersonHonor in the database
    List<PersonHonor> personHonors = personHonorRepository.findAll();
    assertThat(personHonors).hasSize(databaseSizeBeforeCreate + 1);
    PersonHonor testPersonHonor = personHonors.get(personHonors.size() - 1);
    assertThat(testPersonHonor.getDate()).isEqualTo(DEFAULT_DATE);
  }

  @Test
  @Transactional
  public void checkDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = personHonorRepository.findAll().size();
    // set the field null
    personHonor.setDate(null);

    // Create the PersonHonor, which fails.
    PersonHonorDTO personHonorDTO = personHonorMapper.personHonorToPersonHonorDTO(personHonor);

    restPersonHonorMockMvc
        .perform(post("/api/person-honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personHonorDTO)))
        .andExpect(status().isBadRequest());

    List<PersonHonor> personHonors = personHonorRepository.findAll();
    assertThat(personHonors).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllPersonHonors() throws Exception {
    // Initialize the database
    personHonorRepository.saveAndFlush(personHonor);

    // Get all the personHonors
    restPersonHonorMockMvc.perform(get("/api/person-honors?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(personHonor.getId().intValue())))
        .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
  }

  @Test
  @Transactional
  public void getPersonHonor() throws Exception {
    // Initialize the database
    personHonorRepository.saveAndFlush(personHonor);

    // Get the personHonor
    restPersonHonorMockMvc.perform(get("/api/person-honors/{id}", personHonor.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(personHonor.getId().intValue()))
        .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingPersonHonor() throws Exception {
    // Get the personHonor
    restPersonHonorMockMvc.perform(get("/api/person-honors/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updatePersonHonor() throws Exception {
    // Initialize the database
    personHonorRepository.saveAndFlush(personHonor);
    int databaseSizeBeforeUpdate = personHonorRepository.findAll().size();

    // Update the personHonor
    PersonHonor updatedPersonHonor = personHonorRepository.findById(personHonor.getId()).orElse(null);
    updatedPersonHonor.date(UPDATED_DATE);
    PersonHonorDTO personHonorDTO = personHonorMapper
        .personHonorToPersonHonorDTO(updatedPersonHonor);

    restPersonHonorMockMvc
        .perform(put("/api/person-honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personHonorDTO)))
        .andExpect(status().isOk());

    // Validate the PersonHonor in the database
    List<PersonHonor> personHonors = personHonorRepository.findAll();
    assertThat(personHonors).hasSize(databaseSizeBeforeUpdate);
    PersonHonor testPersonHonor = personHonors.get(personHonors.size() - 1);
    assertThat(testPersonHonor.getDate()).isEqualTo(UPDATED_DATE);
  }

  @Test
  @Transactional
  public void deletePersonHonor() throws Exception {
    // Initialize the database
    personHonorRepository.saveAndFlush(personHonor);
    int databaseSizeBeforeDelete = personHonorRepository.findAll().size();

    // Get the personHonor
    restPersonHonorMockMvc.perform(delete("/api/person-honors/{id}", personHonor.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<PersonHonor> personHonors = personHonorRepository.findAll();
    assertThat(personHonors).hasSize(databaseSizeBeforeDelete - 1);
  }
}
