package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Appointment;
import at.simianarmy.repository.AppointmentRepository;
import at.simianarmy.service.AppointmentService;
import at.simianarmy.service.dto.AppointmentDTO;
import at.simianarmy.service.mapper.AppointmentMapper;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the AppointmentResource REST controller.
 *
 * @see AppointmentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AppointmentResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAAAAAAA";
  private static final String UPDATED_NAME = "BBBBBBBBBB";

  private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
  private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

  private static final String DEFAULT_STREET_ADDRESS = "AAAAAAAAAA";
  private static final String UPDATED_STREET_ADDRESS = "BBBBBBBBBB";

  private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
  private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

  private static final String DEFAULT_CITY = "AAAAAAAAAA";
  private static final String UPDATED_CITY = "BBBBBBBBBB";

  private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
  private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

  private static final ZonedDateTime DEFAULT_BEGIN_DATE = ZonedDateTime
      .ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
  private static final ZonedDateTime UPDATED_BEGIN_DATE = ZonedDateTime.now(ZoneId.systemDefault())
      .withNano(0);
  private static final String DEFAULT_BEGIN_DATE_STR = DateTimeFormatter.ISO_INSTANT
      .format(DEFAULT_BEGIN_DATE);

  private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime
      .ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
  private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault())
      .withNano(0);
  private static final String DEFAULT_END_DATE_STR = DateTimeFormatter.ISO_INSTANT
      .format(DEFAULT_END_DATE);

  @Inject
  private AppointmentRepository appointmentRepository;

  @Inject
  private AppointmentMapper appointmentMapper;

  @Inject
  private AppointmentService appointmentService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restAppointmentMockMvc;

  private Appointment appointment;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    AppointmentResource appointmentResource = new AppointmentResource();
    ReflectionTestUtils.setField(appointmentResource, "appointmentService", appointmentService);
    this.restAppointmentMockMvc = MockMvcBuilders.standaloneSetup(appointmentResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Appointment createEntity(EntityManager em) {
    Appointment appointment = new Appointment().name(DEFAULT_NAME).description(DEFAULT_DESCRIPTION)
        .streetAddress(DEFAULT_STREET_ADDRESS).postalCode(DEFAULT_POSTAL_CODE).city(DEFAULT_CITY)
        .country(DEFAULT_COUNTRY).beginDate(DEFAULT_BEGIN_DATE).endDate(DEFAULT_END_DATE);
    return appointment;
  }

  @Before
  public void initTest() {
    appointment = createEntity(em);
  }

  @Test
  @Transactional
  public void createAppointment() throws Exception {
    int databaseSizeBeforeCreate = appointmentRepository.findAll().size();

    // Create the Appointment
    AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);

    restAppointmentMockMvc
        .perform(post("/api/appointments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentDTO)))
        .andExpect(status().isCreated());

    // Validate the Appointment in the database
    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeCreate + 1);
    Appointment testAppointment = appointments.get(appointments.size() - 1);
    assertThat(testAppointment.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testAppointment.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    assertThat(testAppointment.getStreetAddress()).isEqualTo(DEFAULT_STREET_ADDRESS);
    assertThat(testAppointment.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
    assertThat(testAppointment.getCity()).isEqualTo(DEFAULT_CITY);
    assertThat(testAppointment.getCountry()).isEqualTo(DEFAULT_COUNTRY);
    assertThat(testAppointment.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
    assertThat(testAppointment.getEndDate()).isEqualTo(DEFAULT_END_DATE);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = appointmentRepository.findAll().size();
    // set the field null
    appointment.setName(null);

    // Create the Appointment, which fails.
    AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);

    restAppointmentMockMvc
        .perform(post("/api/appointments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentDTO)))
        .andExpect(status().isBadRequest());

    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkBeginDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = appointmentRepository.findAll().size();
    // set the field null
    appointment.setBeginDate(null);

    // Create the Appointment, which fails.
    AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);

    restAppointmentMockMvc
        .perform(post("/api/appointments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentDTO)))
        .andExpect(status().isBadRequest());

    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkEndDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = appointmentRepository.findAll().size();
    // set the field null
    appointment.setEndDate(null);

    // Create the Appointment, which fails.
    AppointmentDTO appointmentDTO = appointmentMapper.appointmentToAppointmentDTO(appointment);

    restAppointmentMockMvc
        .perform(post("/api/appointments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentDTO)))
        .andExpect(status().isBadRequest());

    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllAppointments() throws Exception {
    // Initialize the database
    appointmentRepository.saveAndFlush(appointment);

    // Get all the appointments
    restAppointmentMockMvc.perform(get("/api/appointments?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(appointment.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
        .andExpect(
            jsonPath("$.[*].streetAddress").value(hasItem(DEFAULT_STREET_ADDRESS.toString())))
        .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
        .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
        .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
        .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE_STR)))
        .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE_STR)));
  }

  @Test
  @Transactional
  public void getAppointment() throws Exception {
    // Initialize the database
    appointmentRepository.saveAndFlush(appointment);

    // Get the appointment
    restAppointmentMockMvc.perform(get("/api/appointments/{id}", appointment.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(appointment.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
        .andExpect(jsonPath("$.streetAddress").value(DEFAULT_STREET_ADDRESS.toString()))
        .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
        .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
        .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
        .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE_STR))
        .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE_STR));
  }

  @Test
  @Transactional
  public void getNonExistingAppointment() throws Exception {
    // Get the appointment
    restAppointmentMockMvc.perform(get("/api/appointments/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateAppointment() throws Exception {
    // Initialize the database
    appointmentRepository.saveAndFlush(appointment);
    int databaseSizeBeforeUpdate = appointmentRepository.findAll().size();

    // Update the appointment
    Appointment updatedAppointment = appointmentRepository.findById(appointment.getId()).orElse(null);
    updatedAppointment.name(UPDATED_NAME).description(UPDATED_DESCRIPTION)
        .streetAddress(UPDATED_STREET_ADDRESS).postalCode(UPDATED_POSTAL_CODE).city(UPDATED_CITY)
        .country(UPDATED_COUNTRY).beginDate(UPDATED_BEGIN_DATE).endDate(UPDATED_END_DATE);
    AppointmentDTO appointmentDTO = appointmentMapper
        .appointmentToAppointmentDTO(updatedAppointment);

    restAppointmentMockMvc
        .perform(put("/api/appointments").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentDTO)))
        .andExpect(status().isOk());

    // Validate the Appointment in the database
    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeUpdate);
    Appointment testAppointment = appointments.get(appointments.size() - 1);
    assertThat(testAppointment.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testAppointment.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    assertThat(testAppointment.getStreetAddress()).isEqualTo(UPDATED_STREET_ADDRESS);
    assertThat(testAppointment.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
    assertThat(testAppointment.getCity()).isEqualTo(UPDATED_CITY);
    assertThat(testAppointment.getCountry()).isEqualTo(UPDATED_COUNTRY);
    assertThat(testAppointment.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
    assertThat(testAppointment.getEndDate()).isEqualTo(UPDATED_END_DATE);
  }

  @Test
  @Transactional
  public void deleteAppointment() throws Exception {
    // Initialize the database
    appointmentRepository.saveAndFlush(appointment);
    int databaseSizeBeforeDelete = appointmentRepository.findAll().size();

    // Get the appointment
    restAppointmentMockMvc.perform(delete("/api/appointments/{id}", appointment.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<Appointment> appointments = appointmentRepository.findAll();
    assertThat(appointments).hasSize(databaseSizeBeforeDelete - 1);
  }
}
