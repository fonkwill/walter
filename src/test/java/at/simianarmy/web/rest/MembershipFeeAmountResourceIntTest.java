package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.repository.MembershipFeeAmountRepository;
import at.simianarmy.service.MembershipFeeAmountService;
import at.simianarmy.service.dto.MembershipFeeAmountDTO;
import at.simianarmy.service.mapper.MembershipFeeAmountMapper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the MembershipFeeAmountResource REST controller.
 *
 * @see MembershipFeeAmountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeAmountResourceIntTest {

  private static final LocalDate DEFAULT_BEGIN_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_BEGIN_DATE = LocalDate.now(ZoneId.systemDefault());

  private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
  private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

  private static final BigDecimal DEFAULT_AMOUNT = new BigDecimal(1);
  private static final BigDecimal UPDATED_AMOUNT = new BigDecimal(2);

  @Inject
  private MembershipFeeAmountRepository membershipFeeAmountRepository;

  @Inject
  private MembershipFeeAmountMapper membershipFeeAmountMapper;

  @Inject
  private MembershipFeeAmountService membershipFeeAmountService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restMembershipFeeAmountMockMvc;

  private MembershipFeeAmount membershipFeeAmount;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    MembershipFeeAmountResource membershipFeeAmountResource = new MembershipFeeAmountResource();
    ReflectionTestUtils.setField(membershipFeeAmountResource, "membershipFeeAmountService",
        membershipFeeAmountService);
    this.restMembershipFeeAmountMockMvc = MockMvcBuilders
        .standaloneSetup(membershipFeeAmountResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static MembershipFeeAmount createEntity(EntityManager em) {
    MembershipFeeAmount membershipFeeAmount = new MembershipFeeAmount()
        .beginDate(DEFAULT_BEGIN_DATE).endDate(DEFAULT_END_DATE).amount(DEFAULT_AMOUNT);
    MembershipFee fee = MembershipFeeResourceIntTest.createEntity(em);
    em.persist(fee);
    em.flush();
    membershipFeeAmount.setMembershipFee(fee);
    return membershipFeeAmount;
  }

  @Before
  public void initTest() {
    membershipFeeAmount = createEntity(em);
  }

  @Test
  @Transactional
  public void createMembershipFeeAmount() throws Exception {
    int databaseSizeBeforeCreate = membershipFeeAmountRepository.findAll().size();

    // Create the MembershipFeeAmount
    MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountMapper
        .membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount);

    restMembershipFeeAmountMockMvc
        .perform(post("/api/membership-fee-amounts").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipFeeAmountDTO)))
        .andExpect(status().isCreated());

    // Validate the MembershipFeeAmount in the database
    List<MembershipFeeAmount> membershipFeeAmounts = membershipFeeAmountRepository.findAll();
    assertThat(membershipFeeAmounts).hasSize(databaseSizeBeforeCreate + 1);
    MembershipFeeAmount testMembershipFeeAmount = membershipFeeAmounts
        .get(membershipFeeAmounts.size() - 1);
    assertThat(testMembershipFeeAmount.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
    assertThat(testMembershipFeeAmount.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    assertThat(testMembershipFeeAmount.getAmount()).isEqualTo(DEFAULT_AMOUNT);
  }

  @Test
  @Transactional
  public void checkBeginDateIsRequired() throws Exception {
    int databaseSizeBeforeTest = membershipFeeAmountRepository.findAll().size();
    // set the field null
    membershipFeeAmount.setBeginDate(null);

    // Create the MembershipFeeAmount, which fails.
    MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountMapper
        .membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount);

    restMembershipFeeAmountMockMvc
        .perform(post("/api/membership-fee-amounts").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipFeeAmountDTO)))
        .andExpect(status().isBadRequest());

    List<MembershipFeeAmount> membershipFeeAmounts = membershipFeeAmountRepository.findAll();
    assertThat(membershipFeeAmounts).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkAmountIsRequired() throws Exception {
    int databaseSizeBeforeTest = membershipFeeAmountRepository.findAll().size();
    // set the field null
    membershipFeeAmount.setAmount(null);

    // Create the MembershipFeeAmount, which fails.
    MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountMapper
        .membershipFeeAmountToMembershipFeeAmountDTO(membershipFeeAmount);

    restMembershipFeeAmountMockMvc
        .perform(post("/api/membership-fee-amounts").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipFeeAmountDTO)))
        .andExpect(status().isBadRequest());

    List<MembershipFeeAmount> membershipFeeAmounts = membershipFeeAmountRepository.findAll();
    assertThat(membershipFeeAmounts).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllMembershipFeeAmounts() throws Exception {
    // Initialize the database
    membershipFeeAmountRepository.saveAndFlush(membershipFeeAmount);

    // Get all the membershipFeeAmounts
    restMembershipFeeAmountMockMvc.perform(get("/api/membership-fee-amounts?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(membershipFeeAmount.getId().intValue())))
        .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
        .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
        .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())));
  }

  @Test
  @Transactional
  public void getMembershipFeeAmount() throws Exception {
    // Initialize the database
    membershipFeeAmountRepository.saveAndFlush(membershipFeeAmount);

    // Get the membershipFeeAmount
    restMembershipFeeAmountMockMvc
        .perform(get("/api/membership-fee-amounts/{id}", membershipFeeAmount.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(membershipFeeAmount.getId().intValue()))
        .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE.toString()))
        .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
        .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()));
  }

  @Test
  @Transactional
  public void getNonExistingMembershipFeeAmount() throws Exception {
    // Get the membershipFeeAmount
    restMembershipFeeAmountMockMvc.perform(get("/api/membership-fee-amounts/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateMembershipFeeAmount() throws Exception {
    // Initialize the database
    membershipFeeAmountRepository.saveAndFlush(membershipFeeAmount);
    int databaseSizeBeforeUpdate = membershipFeeAmountRepository.findAll().size();

    // Update the membershipFeeAmount
    MembershipFeeAmount updatedMembershipFeeAmount = membershipFeeAmountRepository
        .findById(membershipFeeAmount.getId()).orElse(null);
    updatedMembershipFeeAmount.beginDate(UPDATED_BEGIN_DATE).endDate(UPDATED_END_DATE)
        .amount(UPDATED_AMOUNT);
    MembershipFeeAmountDTO membershipFeeAmountDTO = membershipFeeAmountMapper
        .membershipFeeAmountToMembershipFeeAmountDTO(updatedMembershipFeeAmount);

    restMembershipFeeAmountMockMvc
        .perform(put("/api/membership-fee-amounts").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(membershipFeeAmountDTO)))
        .andExpect(status().isOk());

    // Validate the MembershipFeeAmount in the database
    List<MembershipFeeAmount> membershipFeeAmounts = membershipFeeAmountRepository.findAll();
    assertThat(membershipFeeAmounts).hasSize(databaseSizeBeforeUpdate);
    MembershipFeeAmount testMembershipFeeAmount = membershipFeeAmounts
        .get(membershipFeeAmounts.size() - 1);
    assertThat(testMembershipFeeAmount.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
    assertThat(testMembershipFeeAmount.getEndDate()).isEqualTo(UPDATED_END_DATE);
    assertThat(testMembershipFeeAmount.getAmount()).isEqualTo(UPDATED_AMOUNT);
  }

  @Test
  @Transactional
  public void deleteMembershipFeeAmount() throws Exception {
    // Initialize the database
    membershipFeeAmountRepository.saveAndFlush(membershipFeeAmount);
    int databaseSizeBeforeDelete = membershipFeeAmountRepository.findAll().size();

    // Get the membershipFeeAmount
    restMembershipFeeAmountMockMvc
        .perform(delete("/api/membership-fee-amounts/{id}", membershipFeeAmount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<MembershipFeeAmount> membershipFeeAmounts = membershipFeeAmountRepository.findAll();
    assertThat(membershipFeeAmounts).hasSize(databaseSizeBeforeDelete - 1);
  }
}
