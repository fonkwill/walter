package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.CompositionRepository;
import at.simianarmy.service.CompositionService;
import at.simianarmy.service.dto.CompositionDTO;
import at.simianarmy.service.mapper.CompositionMapper;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the CompositionResource REST controller.
 *
 * @see CompositionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompositionResourceIntTest {

  private static final Integer DEFAULT_NR = 1;

  private static final String DEFAULT_TITLE = "AAAAAAAAAA";
  private static final String UPDATED_TITLE = "BBBBBBBBBB";

  private static final String DEFAULT_NOTE = "AAAAAAAAAA";
  private static final String UPDATED_NOTE = "BBBBBBBBBB";

  private static final Integer DEFAULT_AKM_COMPOSITION_NR = 1;
  private static final Integer UPDATED_AKM_COMPOSITION_NR = 2;

  @Inject
  private CompositionRepository compositionRepository;

  @Inject
  private CompositionMapper compositionMapper;

  @Inject
  private CompositionService compositionService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restCompositionMockMvc;

  private Composition composition;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    CompositionResource compositionResource = new CompositionResource();
    ReflectionTestUtils.setField(compositionResource, "compositionService", compositionService);
    this.restCompositionMockMvc = MockMvcBuilders.standaloneSetup(compositionResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Composition createEntity(EntityManager em) {
    Composition composition = new Composition().nr(DEFAULT_NR).title(DEFAULT_TITLE)
        .note(DEFAULT_NOTE).akmCompositionNr(DEFAULT_AKM_COMPOSITION_NR);
    // Add required entity
    ColorCode colorCode = ColorCodeResourceIntTest.createEntity(em);
    em.persist(colorCode);
    em.flush();
    composition.setColorCode(colorCode);
    return composition;
  }

  @Before
  public void initTest() {
    composition = createEntity(em);
  }

  @Test
  @Transactional
  public void createComposition() throws Exception {
    int databaseSizeBeforeCreate = compositionRepository.findAll().size();

    // Create the Composition
    CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);

    restCompositionMockMvc
        .perform(post("/api/compositions").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compositionDTO)))
        .andExpect(status().isCreated());

    // Validate the Composition in the database
    List<Composition> compositions = compositionRepository.findAll();
    assertThat(compositions).hasSize(databaseSizeBeforeCreate + 1);
    Composition testComposition = compositions.get(compositions.size() - 1);
    assertThat(testComposition.getNr()).isEqualTo(DEFAULT_NR);
    assertThat(testComposition.getTitle()).isEqualTo(DEFAULT_TITLE);
    assertThat(testComposition.getNote()).isEqualTo(DEFAULT_NOTE);
    assertThat(testComposition.getAkmCompositionNr()).isEqualTo(DEFAULT_AKM_COMPOSITION_NR);
  }

  @Test
  @Transactional
  public void checkTitleIsRequired() throws Exception {
    int databaseSizeBeforeTest = compositionRepository.findAll().size();
    // set the field null
    composition.setTitle(null);

    // Create the Composition, which fails.
    CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);

    restCompositionMockMvc
        .perform(post("/api/compositions").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compositionDTO)))
        .andExpect(status().isBadRequest());

    List<Composition> compositions = compositionRepository.findAll();
    assertThat(compositions).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkAkmCompositionNrIsRequired() throws Exception {
    int databaseSizeBeforeTest = compositionRepository.findAll().size();
    // set the field null
    composition.setAkmCompositionNr(null);

    // Create the Composition, which fails.
    CompositionDTO compositionDTO = compositionMapper.compositionToCompositionDTO(composition);

    restCompositionMockMvc
        .perform(post("/api/compositions").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compositionDTO)))
        .andExpect(status().isBadRequest());

    List<Composition> compositions = compositionRepository.findAll();
    assertThat(compositions).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllCompositions() throws Exception {
    // Initialize the database
    compositionRepository.saveAndFlush(composition);

    // Get all the compositions
    restCompositionMockMvc.perform(get("/api/compositions?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(composition.getId().intValue())))
        .andExpect(jsonPath("$.[*].nr").value(hasItem(DEFAULT_NR)))
        .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
        .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
        .andExpect(jsonPath("$.[*].akmCompositionNr").value(hasItem(DEFAULT_AKM_COMPOSITION_NR)));
  }

  @Test
  @Transactional
  public void getComposition() throws Exception {
    // Initialize the database
    compositionRepository.saveAndFlush(composition);

    // Get the composition
    restCompositionMockMvc.perform(get("/api/compositions/{id}", composition.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(composition.getId().intValue()))
        .andExpect(jsonPath("$.nr").value(DEFAULT_NR))
        .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
        .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
        .andExpect(jsonPath("$.akmCompositionNr").value(DEFAULT_AKM_COMPOSITION_NR));
  }

  @Test
  @Transactional
  public void getNonExistingComposition() throws Exception {
    // Get the composition
    restCompositionMockMvc.perform(get("/api/compositions/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateComposition() throws Exception {
    // Initialize the database
    compositionRepository.saveAndFlush(composition);
    int databaseSizeBeforeUpdate = compositionRepository.findAll().size();

    // Update the composition
    Composition updatedComposition = compositionRepository.findById(composition.getId()).orElse(null);
    updatedComposition.title(UPDATED_TITLE).note(UPDATED_NOTE)
        .akmCompositionNr(UPDATED_AKM_COMPOSITION_NR);
    CompositionDTO compositionDTO = compositionMapper
        .compositionToCompositionDTO(updatedComposition);

    restCompositionMockMvc
        .perform(put("/api/compositions").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(compositionDTO)))
        .andExpect(status().isOk());

    // Validate the Composition in the database
    List<Composition> compositions = compositionRepository.findAll();
    assertThat(compositions).hasSize(databaseSizeBeforeUpdate);
    Composition testComposition = compositions.get(compositions.size() - 1);
    assertThat(testComposition.getNr()).isEqualTo(DEFAULT_NR);
    assertThat(testComposition.getTitle()).isEqualTo(UPDATED_TITLE);
    assertThat(testComposition.getNote()).isEqualTo(UPDATED_NOTE);
    assertThat(testComposition.getAkmCompositionNr()).isEqualTo(UPDATED_AKM_COMPOSITION_NR);
  }

  @Test
  @Transactional
  public void deleteComposition() throws Exception {
    // Initialize the database
    compositionRepository.saveAndFlush(composition);
    int databaseSizeBeforeDelete = compositionRepository.findAll().size();

    // Get the composition
    restCompositionMockMvc.perform(delete("/api/compositions/{id}", composition.getId())
        .accept(TestUtil.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

    // Validate the database is empty
    List<Composition> compositions = compositionRepository.findAll();
    assertThat(compositions).hasSize(databaseSizeBeforeDelete - 1);
  }
}
