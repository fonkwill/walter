package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Arranger;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.service.ArrangerService;
import at.simianarmy.service.dto.ArrangerDTO;
import at.simianarmy.service.mapper.ArrangerMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the ArrangerResource REST controller.
 *
 * @see ArrangerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ArrangerResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  @Inject
  private ArrangerRepository arrangerRepository;

  @Inject
  private ArrangerMapper arrangerMapper;

  @Inject
  private ArrangerService arrangerService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restArrangerMockMvc;

  private Arranger arranger;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ArrangerResource arrangerResource = new ArrangerResource();
    ReflectionTestUtils.setField(arrangerResource, "arrangerService", arrangerService);
    this.restArrangerMockMvc = MockMvcBuilders.standaloneSetup(arrangerResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Arranger createEntity(EntityManager em) {
    Arranger arranger = new Arranger().name(DEFAULT_NAME);
    return arranger;
  }

  @Before
  public void initTest() {
    arranger = createEntity(em);
  }

  @Test
  @Transactional
  public void createArranger() throws Exception {
    int databaseSizeBeforeCreate = arrangerRepository.findAll().size();

    // Create the Arranger
    ArrangerDTO arrangerDTO = arrangerMapper.arrangerToArrangerDTO(arranger);

    restArrangerMockMvc
        .perform(post("/api/arrangers").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arrangerDTO)))
        .andExpect(status().isCreated());

    // Validate the Arranger in the database
    List<Arranger> arrangers = arrangerRepository.findAll();
    assertThat(arrangers).hasSize(databaseSizeBeforeCreate + 1);
    Arranger testArranger = arrangers.get(arrangers.size() - 1);
    assertThat(testArranger.getName()).isEqualTo(DEFAULT_NAME);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = arrangerRepository.findAll().size();
    // set the field null
    arranger.setName(null);

    // Create the Arranger, which fails.
    ArrangerDTO arrangerDTO = arrangerMapper.arrangerToArrangerDTO(arranger);

    restArrangerMockMvc
        .perform(post("/api/arrangers").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(arrangerDTO)))
        .andExpect(status().isBadRequest());

    List<Arranger> arrangers = arrangerRepository.findAll();
    assertThat(arrangers).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllArrangers() throws Exception {
    // Initialize the database
    arrangerRepository.saveAndFlush(arranger);

    // Get all the arrangers
    restArrangerMockMvc.perform(get("/api/arrangers?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(arranger.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
  }

  @Test
  @Transactional
  public void getArranger() throws Exception {
    // Initialize the database
    arrangerRepository.saveAndFlush(arranger);

    // Get the arranger
    restArrangerMockMvc.perform(get("/api/arrangers/{id}", arranger.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(arranger.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingArranger() throws Exception {
    // Get the arranger
    restArrangerMockMvc.perform(get("/api/arrangers/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateArranger() throws Exception {
    // Initialize the database
    arrangerRepository.saveAndFlush(arranger);
    int databaseSizeBeforeUpdate = arrangerRepository.findAll().size();

    // Update the arranger
    Arranger updatedArranger = arrangerRepository.findById(arranger.getId()).orElse(null);
    updatedArranger.name(UPDATED_NAME);
    ArrangerDTO arrangerDTO = arrangerMapper.arrangerToArrangerDTO(updatedArranger);

    restArrangerMockMvc.perform(put("/api/arrangers").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(arrangerDTO))).andExpect(status().isOk());

    // Validate the Arranger in the database
    List<Arranger> arrangers = arrangerRepository.findAll();
    assertThat(arrangers).hasSize(databaseSizeBeforeUpdate);
    Arranger testArranger = arrangers.get(arrangers.size() - 1);
    assertThat(testArranger.getName()).isEqualTo(UPDATED_NAME);
  }

  @Test
  @Transactional
  public void deleteArranger() throws Exception {
    // Initialize the database
    arrangerRepository.saveAndFlush(arranger);
    int databaseSizeBeforeDelete = arrangerRepository.findAll().size();

    // Get the arranger
    restArrangerMockMvc
        .perform(
            delete("/api/arrangers/{id}", arranger.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Arranger> arrangers = arrangerRepository.findAll();
    assertThat(arrangers).hasSize(databaseSizeBeforeDelete - 1);
  }
}
