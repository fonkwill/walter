package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Award;
import at.simianarmy.repository.AwardRepository;
import at.simianarmy.service.AwardService;
import at.simianarmy.service.dto.AwardDTO;
import at.simianarmy.service.mapper.AwardMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the AwardResource REST controller.
 *
 * @see AwardResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AwardResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  private static final String DEFAULT_DESCRIPTON = "AAAAA";
  private static final String UPDATED_DESCRIPTON = "BBBBB";

  @Inject
  private AwardRepository awardRepository;

  @Inject
  private AwardMapper awardMapper;

  @Inject
  private AwardService awardService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restAwardMockMvc;

  private Award award;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    AwardResource awardResource = new AwardResource();
    ReflectionTestUtils.setField(awardResource, "awardService", awardService);
    this.restAwardMockMvc = MockMvcBuilders.standaloneSetup(awardResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Award createEntity(EntityManager em) {
    Award award = new Award().name(DEFAULT_NAME).descripton(DEFAULT_DESCRIPTON);
    return award;
  }

  @Before
  public void initTest() {
    award = createEntity(em);
  }

  @Test
  @Transactional
  public void createAward() throws Exception {
    int databaseSizeBeforeCreate = awardRepository.findAll().size();

    // Create the Award
    AwardDTO awardDTO = awardMapper.awardToAwardDTO(award);

    restAwardMockMvc.perform(post("/api/awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(awardDTO))).andExpect(status().isCreated());

    // Validate the Award in the database
    List<Award> awards = awardRepository.findAll();
    assertThat(awards).hasSize(databaseSizeBeforeCreate + 1);
    Award testAward = awards.get(awards.size() - 1);
    assertThat(testAward.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testAward.getDescripton()).isEqualTo(DEFAULT_DESCRIPTON);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = awardRepository.findAll().size();
    // set the field null
    award.setName(null);

    // Create the Award, which fails.
    AwardDTO awardDTO = awardMapper.awardToAwardDTO(award);

    restAwardMockMvc
        .perform(post("/api/awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(awardDTO)))
        .andExpect(status().isBadRequest());

    List<Award> awards = awardRepository.findAll();
    assertThat(awards).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllAwards() throws Exception {
    // Initialize the database
    awardRepository.saveAndFlush(award);

    // Get all the awards
    restAwardMockMvc.perform(get("/api/awards?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(award.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].descripton").value(hasItem(DEFAULT_DESCRIPTON.toString())));
  }

  @Test
  @Transactional
  public void getAward() throws Exception {
    // Initialize the database
    awardRepository.saveAndFlush(award);

    // Get the award
    restAwardMockMvc.perform(get("/api/awards/{id}", award.getId())).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(award.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.descripton").value(DEFAULT_DESCRIPTON.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingAward() throws Exception {
    // Get the award
    restAwardMockMvc.perform(get("/api/awards/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateAward() throws Exception {
    // Initialize the database
    awardRepository.saveAndFlush(award);
    int databaseSizeBeforeUpdate = awardRepository.findAll().size();

    // Update the award
    Award updatedAward = awardRepository.findById(award.getId()).orElse(null);
    updatedAward.name(UPDATED_NAME).descripton(UPDATED_DESCRIPTON);
    AwardDTO awardDTO = awardMapper.awardToAwardDTO(updatedAward);

    restAwardMockMvc.perform(put("/api/awards").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(awardDTO))).andExpect(status().isOk());

    // Validate the Award in the database
    List<Award> awards = awardRepository.findAll();
    assertThat(awards).hasSize(databaseSizeBeforeUpdate);
    Award testAward = awards.get(awards.size() - 1);
    assertThat(testAward.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testAward.getDescripton()).isEqualTo(UPDATED_DESCRIPTON);
  }

  @Test
  @Transactional
  public void deleteAward() throws Exception {
    // Initialize the database
    awardRepository.saveAndFlush(award);
    int databaseSizeBeforeDelete = awardRepository.findAll().size();

    // Get the award
    restAwardMockMvc
        .perform(delete("/api/awards/{id}", award.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Award> awards = awardRepository.findAll();
    assertThat(awards).hasSize(databaseSizeBeforeDelete - 1);
  }
}
