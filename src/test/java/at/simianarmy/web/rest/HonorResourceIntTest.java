package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Honor;
import at.simianarmy.repository.HonorRepository;
import at.simianarmy.service.HonorService;
import at.simianarmy.service.dto.HonorDTO;
import at.simianarmy.service.mapper.HonorMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the HonorResource REST controller.
 *
 * @see HonorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class HonorResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  private static final String DEFAULT_DESCRIPTION = "AAAAA";
  private static final String UPDATED_DESCRIPTION = "BBBBB";

  @Inject
  private HonorRepository honorRepository;

  @Inject
  private HonorMapper honorMapper;

  @Inject
  private HonorService honorService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restHonorMockMvc;

  private Honor honor;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    HonorResource honorResource = new HonorResource();
    ReflectionTestUtils.setField(honorResource, "honorService", honorService);
    this.restHonorMockMvc = MockMvcBuilders.standaloneSetup(honorResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Honor createEntity(EntityManager em) {
    Honor honor = new Honor().name(DEFAULT_NAME).description(DEFAULT_DESCRIPTION);
    return honor;
  }

  @Before
  public void initTest() {
    honor = createEntity(em);
  }

  @Test
  @Transactional
  public void createHonor() throws Exception {
    int databaseSizeBeforeCreate = honorRepository.findAll().size();

    // Create the Honor
    HonorDTO honorDTO = honorMapper.honorToHonorDTO(honor);

    restHonorMockMvc.perform(post("/api/honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(honorDTO))).andExpect(status().isCreated());

    // Validate the Honor in the database
    List<Honor> honors = honorRepository.findAll();
    assertThat(honors).hasSize(databaseSizeBeforeCreate + 1);
    Honor testHonor = honors.get(honors.size() - 1);
    assertThat(testHonor.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testHonor.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = honorRepository.findAll().size();
    // set the field null
    honor.setName(null);

    // Create the Honor, which fails.
    HonorDTO honorDTO = honorMapper.honorToHonorDTO(honor);

    restHonorMockMvc
        .perform(post("/api/honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(honorDTO)))
        .andExpect(status().isBadRequest());

    List<Honor> honors = honorRepository.findAll();
    assertThat(honors).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllHonors() throws Exception {
    // Initialize the database
    honorRepository.saveAndFlush(honor);

    // Get all the honors
    restHonorMockMvc.perform(get("/api/honors?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(honor.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
  }

  @Test
  @Transactional
  public void getHonor() throws Exception {
    // Initialize the database
    honorRepository.saveAndFlush(honor);

    // Get the honor
    restHonorMockMvc.perform(get("/api/honors/{id}", honor.getId())).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(honor.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingHonor() throws Exception {
    // Get the honor
    restHonorMockMvc.perform(get("/api/honors/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateHonor() throws Exception {
    // Initialize the database
    honorRepository.saveAndFlush(honor);
    int databaseSizeBeforeUpdate = honorRepository.findAll().size();

    // Update the honor
    Honor updatedHonor = honorRepository.findById(honor.getId()).orElse(null);
    updatedHonor.name(UPDATED_NAME).description(UPDATED_DESCRIPTION);
    HonorDTO honorDTO = honorMapper.honorToHonorDTO(updatedHonor);

    restHonorMockMvc.perform(put("/api/honors").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(honorDTO))).andExpect(status().isOk());

    // Validate the Honor in the database
    List<Honor> honors = honorRepository.findAll();
    assertThat(honors).hasSize(databaseSizeBeforeUpdate);
    Honor testHonor = honors.get(honors.size() - 1);
    assertThat(testHonor.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testHonor.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
  }

  @Test
  @Transactional
  public void deleteHonor() throws Exception {
    // Initialize the database
    honorRepository.saveAndFlush(honor);
    int databaseSizeBeforeDelete = honorRepository.findAll().size();

    // Get the honor
    restHonorMockMvc
        .perform(delete("/api/honors/{id}", honor.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Honor> honors = honorRepository.findAll();
    assertThat(honors).hasSize(databaseSizeBeforeDelete - 1);
  }
}
