package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.repository.MembershipFeeRepository;
import at.simianarmy.service.MembershipFeeService;
import at.simianarmy.service.dto.MembershipFeeDTO;
import at.simianarmy.service.mapper.MembershipFeeMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import at.simianarmy.domain.enumeration.MembershipFeePeriod;
/**
 * Test class for the MembershipFeeResource REST controller.
 *
 * @see MembershipFeeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final MembershipFeePeriod DEFAULT_PERIOD = MembershipFeePeriod.MONTHLY;
    private static final MembershipFeePeriod UPDATED_PERIOD = MembershipFeePeriod.ANNUALLY;

    private static final Integer DEFAULT_PERIOD_TIME_FRACTION = 1;
    private static final Integer UPDATED_PERIOD_TIME_FRACTION = 2;

    @Inject
    private MembershipFeeRepository membershipFeeRepository;

    @Inject
    private MembershipFeeMapper membershipFeeMapper;

    @Inject
    private MembershipFeeService membershipFeeService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMembershipFeeMockMvc;

    private MembershipFee membershipFee;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MembershipFeeResource membershipFeeResource = new MembershipFeeResource();
        ReflectionTestUtils.setField(membershipFeeResource, "membershipFeeService", membershipFeeService);
        this.restMembershipFeeMockMvc = MockMvcBuilders.standaloneSetup(membershipFeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembershipFee createEntity(EntityManager em) {
        MembershipFee membershipFee = new MembershipFee()
                .description(DEFAULT_DESCRIPTION)
                .period(DEFAULT_PERIOD)
                .periodTimeFraction(DEFAULT_PERIOD_TIME_FRACTION);
        return membershipFee;
    }

    @Before
    public void initTest() {
        membershipFee = createEntity(em);
    }

    @Test
    @Transactional
    public void createMembershipFee() throws Exception {
        int databaseSizeBeforeCreate = membershipFeeRepository.findAll().size();

        // Create the MembershipFee
        MembershipFeeDTO membershipFeeDTO = membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee);

        restMembershipFeeMockMvc.perform(post("/api/membership-fees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeDTO)))
                .andExpect(status().isCreated());

        // Validate the MembershipFee in the database
        List<MembershipFee> membershipFees = membershipFeeRepository.findAll();
        assertThat(membershipFees).hasSize(databaseSizeBeforeCreate + 1);
        MembershipFee testMembershipFee = membershipFees.get(membershipFees.size() - 1);
        assertThat(testMembershipFee.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testMembershipFee.getPeriod()).isEqualTo(DEFAULT_PERIOD);
        assertThat(testMembershipFee.getPeriodTimeFraction()).isEqualTo(DEFAULT_PERIOD_TIME_FRACTION);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = membershipFeeRepository.findAll().size();
        // set the field null
        membershipFee.setDescription(null);

        // Create the MembershipFee, which fails.
        MembershipFeeDTO membershipFeeDTO = membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee);

        restMembershipFeeMockMvc.perform(post("/api/membership-fees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeDTO)))
                .andExpect(status().isBadRequest());

        List<MembershipFee> membershipFees = membershipFeeRepository.findAll();
        assertThat(membershipFees).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPeriodIsRequired() throws Exception {
        int databaseSizeBeforeTest = membershipFeeRepository.findAll().size();
        // set the field null
        membershipFee.setPeriod(null);

        // Create the MembershipFee, which fails.
        MembershipFeeDTO membershipFeeDTO = membershipFeeMapper.membershipFeeToMembershipFeeDTO(membershipFee);

        restMembershipFeeMockMvc.perform(post("/api/membership-fees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeDTO)))
                .andExpect(status().isBadRequest());

        List<MembershipFee> membershipFees = membershipFeeRepository.findAll();
        assertThat(membershipFees).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMembershipFees() throws Exception {
        // Initialize the database
        membershipFeeRepository.saveAndFlush(membershipFee);

        // Get all the membershipFees
        restMembershipFeeMockMvc.perform(get("/api/membership-fees?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(membershipFee.getId().intValue())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].period").value(hasItem(DEFAULT_PERIOD.toString())))
                .andExpect(jsonPath("$.[*].periodTimeFraction").value(hasItem(DEFAULT_PERIOD_TIME_FRACTION)));
    }

    @Test
    @Transactional
    public void getMembershipFee() throws Exception {
        // Initialize the database
        membershipFeeRepository.saveAndFlush(membershipFee);

        // Get the membershipFee
        restMembershipFeeMockMvc.perform(get("/api/membership-fees/{id}", membershipFee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(membershipFee.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.period").value(DEFAULT_PERIOD.toString()))
            .andExpect(jsonPath("$.periodTimeFraction").value(DEFAULT_PERIOD_TIME_FRACTION));
    }

    @Test
    @Transactional
    public void getNonExistingMembershipFee() throws Exception {
        // Get the membershipFee
        restMembershipFeeMockMvc.perform(get("/api/membership-fees/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMembershipFee() throws Exception {
        // Initialize the database
        membershipFeeRepository.saveAndFlush(membershipFee);
        int databaseSizeBeforeUpdate = membershipFeeRepository.findAll().size();

        // Update the membershipFee
        MembershipFee updatedMembershipFee = membershipFeeRepository.findById(membershipFee.getId()).orElse(null);
        updatedMembershipFee
                .description(UPDATED_DESCRIPTION)
                .period(UPDATED_PERIOD)
                .periodTimeFraction(UPDATED_PERIOD_TIME_FRACTION);
        MembershipFeeDTO membershipFeeDTO = membershipFeeMapper.membershipFeeToMembershipFeeDTO(updatedMembershipFee);

        restMembershipFeeMockMvc.perform(put("/api/membership-fees")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeDTO)))
                .andExpect(status().isOk());

        // Validate the MembershipFee in the database
        List<MembershipFee> membershipFees = membershipFeeRepository.findAll();
        assertThat(membershipFees).hasSize(databaseSizeBeforeUpdate);
        MembershipFee testMembershipFee = membershipFees.get(membershipFees.size() - 1);
        assertThat(testMembershipFee.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMembershipFee.getPeriod()).isEqualTo(UPDATED_PERIOD);
        assertThat(testMembershipFee.getPeriodTimeFraction()).isEqualTo(UPDATED_PERIOD_TIME_FRACTION);
    }

    @Test
    @Transactional
    public void deleteMembershipFee() throws Exception {
        // Initialize the database
        membershipFeeRepository.saveAndFlush(membershipFee);
        int databaseSizeBeforeDelete = membershipFeeRepository.findAll().size();

        // Get the membershipFee
        restMembershipFeeMockMvc.perform(delete("/api/membership-fees/{id}", membershipFee.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<MembershipFee> membershipFees = membershipFeeRepository.findAll();
        assertThat(membershipFees).hasSize(databaseSizeBeforeDelete - 1);
    }
}
