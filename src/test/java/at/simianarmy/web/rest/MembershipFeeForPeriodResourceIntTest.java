package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.repository.MembershipFeeForPeriodRepository;
import at.simianarmy.service.MembershipFeeForPeriodService;
import at.simianarmy.service.dto.MembershipFeeForPeriodDTO;
import at.simianarmy.service.mapper.MembershipFeeForPeriodMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MembershipFeeForPeriodResource REST controller.
 *
 * @see MembershipFeeForPeriodResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeForPeriodResourceIntTest {

    private static final Integer DEFAULT_NR = 1;
    private static final Integer UPDATED_NR = 2;

    private static final Integer DEFAULT_YEAR = 1;
    private static final Integer UPDATED_YEAR = 2;

    private static final String DEFAULT_REFERENCE_CODE = "AAAAA";
    private static final String UPDATED_REFERENCE_CODE = "BBBBB";

    private static final Boolean DEFAULT_PAID = false;
    private static final Boolean UPDATED_PAID = true;

    @Inject
    private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;

    @Inject
    private MembershipFeeForPeriodMapper membershipFeeForPeriodMapper;

    @Inject
    private MembershipFeeForPeriodService membershipFeeForPeriodService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restMembershipFeeForPeriodMockMvc;

    private MembershipFeeForPeriod membershipFeeForPeriod;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        MembershipFeeForPeriodResource membershipFeeForPeriodResource = new MembershipFeeForPeriodResource();
        ReflectionTestUtils.setField(membershipFeeForPeriodResource, "membershipFeeForPeriodService", membershipFeeForPeriodService);
        this.restMembershipFeeForPeriodMockMvc = MockMvcBuilders.standaloneSetup(membershipFeeForPeriodResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembershipFeeForPeriod createEntity(EntityManager em) {
        MembershipFeeForPeriod membershipFeeForPeriod = new MembershipFeeForPeriod()
                .nr(DEFAULT_NR)
                .year(DEFAULT_YEAR)
                .referenceCode(DEFAULT_REFERENCE_CODE)
                .paid(DEFAULT_PAID);
        return membershipFeeForPeriod;
    }

    @Before
    public void initTest() {
        membershipFeeForPeriod = createEntity(em);
    }

    @Test
    @Transactional
    public void createMembershipFeeForPeriod() throws Exception {
        int databaseSizeBeforeCreate = membershipFeeForPeriodRepository.findAll().size();

        // Create the MembershipFeeForPeriod
        MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod);

        restMembershipFeeForPeriodMockMvc.perform(post("/api/membership-fee-for-periods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeForPeriodDTO)))
                .andExpect(status().isCreated());

        // Validate the MembershipFeeForPeriod in the database
        List<MembershipFeeForPeriod> membershipFeeForPeriods = membershipFeeForPeriodRepository.findAll();
        assertThat(membershipFeeForPeriods).hasSize(databaseSizeBeforeCreate + 1);
        MembershipFeeForPeriod testMembershipFeeForPeriod = membershipFeeForPeriods.get(membershipFeeForPeriods.size() - 1);
        assertThat(testMembershipFeeForPeriod.getNr()).isEqualTo(DEFAULT_NR);
        assertThat(testMembershipFeeForPeriod.getYear()).isEqualTo(DEFAULT_YEAR);
        assertThat(testMembershipFeeForPeriod.getReferenceCode()).isEqualTo(DEFAULT_REFERENCE_CODE);
        assertThat(testMembershipFeeForPeriod.isPaid()).isEqualTo(DEFAULT_PAID);
    }

    @Test
    @Transactional
    public void checkNrIsRequired() throws Exception {
        int databaseSizeBeforeTest = membershipFeeForPeriodRepository.findAll().size();
        // set the field null
        membershipFeeForPeriod.setNr(null);

        // Create the MembershipFeeForPeriod, which fails.
        MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod);

        restMembershipFeeForPeriodMockMvc.perform(post("/api/membership-fee-for-periods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeForPeriodDTO)))
                .andExpect(status().isBadRequest());

        List<MembershipFeeForPeriod> membershipFeeForPeriods = membershipFeeForPeriodRepository.findAll();
        assertThat(membershipFeeForPeriods).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYearIsRequired() throws Exception {
        int databaseSizeBeforeTest = membershipFeeForPeriodRepository.findAll().size();
        // set the field null
        membershipFeeForPeriod.setYear(null);

        // Create the MembershipFeeForPeriod, which fails.
        MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper.membershipFeeForPeriodToMembershipFeeForPeriodDTO(membershipFeeForPeriod);

        restMembershipFeeForPeriodMockMvc.perform(post("/api/membership-fee-for-periods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeForPeriodDTO)))
                .andExpect(status().isBadRequest());

        List<MembershipFeeForPeriod> membershipFeeForPeriods = membershipFeeForPeriodRepository.findAll();
        assertThat(membershipFeeForPeriods).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMembershipFeeForPeriods() throws Exception {
        // Initialize the database
        membershipFeeForPeriodRepository.saveAndFlush(membershipFeeForPeriod);

        // Get all the membershipFeeForPeriods
        restMembershipFeeForPeriodMockMvc.perform(get("/api/membership-fee-for-periods?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(membershipFeeForPeriod.getId().intValue())))
                .andExpect(jsonPath("$.[*].nr").value(hasItem(DEFAULT_NR)))
                .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
                .andExpect(jsonPath("$.[*].referenceCode").value(hasItem(DEFAULT_REFERENCE_CODE.toString())))
                .andExpect(jsonPath("$.[*].paid").value(hasItem(DEFAULT_PAID.booleanValue())));
    }

    @Test
    @Transactional
    public void getMembershipFeeForPeriod() throws Exception {
        // Initialize the database
        membershipFeeForPeriodRepository.saveAndFlush(membershipFeeForPeriod);

        // Get the membershipFeeForPeriod
        restMembershipFeeForPeriodMockMvc.perform(get("/api/membership-fee-for-periods/{id}", membershipFeeForPeriod.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(membershipFeeForPeriod.getId().intValue()))
            .andExpect(jsonPath("$.nr").value(DEFAULT_NR))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR))
            .andExpect(jsonPath("$.referenceCode").value(DEFAULT_REFERENCE_CODE.toString()))
            .andExpect(jsonPath("$.paid").value(DEFAULT_PAID.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMembershipFeeForPeriod() throws Exception {
        // Get the membershipFeeForPeriod
        restMembershipFeeForPeriodMockMvc.perform(get("/api/membership-fee-for-periods/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMembershipFeeForPeriod() throws Exception {
        // Initialize the database
        membershipFeeForPeriodRepository.saveAndFlush(membershipFeeForPeriod);
        int databaseSizeBeforeUpdate = membershipFeeForPeriodRepository.findAll().size();

        // Update the membershipFeeForPeriod
        MembershipFeeForPeriod updatedMembershipFeeForPeriod = membershipFeeForPeriodRepository.findById(membershipFeeForPeriod.getId()).orElse(null);
        updatedMembershipFeeForPeriod
                .nr(UPDATED_NR)
                .year(UPDATED_YEAR)
                .referenceCode(UPDATED_REFERENCE_CODE)
                .paid(UPDATED_PAID);
        MembershipFeeForPeriodDTO membershipFeeForPeriodDTO = membershipFeeForPeriodMapper.membershipFeeForPeriodToMembershipFeeForPeriodDTO(updatedMembershipFeeForPeriod);

        restMembershipFeeForPeriodMockMvc.perform(put("/api/membership-fee-for-periods")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(membershipFeeForPeriodDTO)))
                .andExpect(status().isOk());

        // Validate the MembershipFeeForPeriod in the database
        List<MembershipFeeForPeriod> membershipFeeForPeriods = membershipFeeForPeriodRepository.findAll();
        assertThat(membershipFeeForPeriods).hasSize(databaseSizeBeforeUpdate);
        MembershipFeeForPeriod testMembershipFeeForPeriod = membershipFeeForPeriods.get(membershipFeeForPeriods.size() - 1);
        assertThat(testMembershipFeeForPeriod.getNr()).isEqualTo(UPDATED_NR);
        assertThat(testMembershipFeeForPeriod.getYear()).isEqualTo(UPDATED_YEAR);
        assertThat(testMembershipFeeForPeriod.getReferenceCode()).isEqualTo(UPDATED_REFERENCE_CODE);
        assertThat(testMembershipFeeForPeriod.isPaid()).isEqualTo(UPDATED_PAID);
    }

    @Test
    @Transactional
    public void deleteMembershipFeeForPeriod() throws Exception {
        // Initialize the database
        membershipFeeForPeriodRepository.saveAndFlush(membershipFeeForPeriod);
        int databaseSizeBeforeDelete = membershipFeeForPeriodRepository.findAll().size();

        // Get the membershipFeeForPeriod
        restMembershipFeeForPeriodMockMvc.perform(delete("/api/membership-fee-for-periods/{id}", membershipFeeForPeriod.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<MembershipFeeForPeriod> membershipFeeForPeriods = membershipFeeForPeriodRepository.findAll();
        assertThat(membershipFeeForPeriods).hasSize(databaseSizeBeforeDelete - 1);
    }
}
