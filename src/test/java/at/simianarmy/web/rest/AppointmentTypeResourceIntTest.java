package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.AppointmentType;
import at.simianarmy.repository.AppointmentTypeRepository;
import at.simianarmy.service.AppointmentTypeService;
import at.simianarmy.service.dto.AppointmentTypeDTO;
import at.simianarmy.service.mapper.AppointmentTypeMapper;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the AppointmentTypeResource REST controller.
 *
 * @see AppointmentTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AppointmentTypeResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAAAAAAA";
  private static final String UPDATED_NAME = "BBBBBBBBBB";

  private static final Boolean DEFAULT_IS_OFFICIAL = false;
  private static final Boolean UPDATED_IS_OFFICIAL = true;

  @Inject
  private AppointmentTypeRepository appointmentTypeRepository;

  @Inject
  private AppointmentTypeMapper appointmentTypeMapper;

  @Inject
  private AppointmentTypeService appointmentTypeService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restAppointmentTypeMockMvc;

  private AppointmentType appointmentType;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    AppointmentTypeResource appointmentTypeResource = new AppointmentTypeResource();
    ReflectionTestUtils.setField(appointmentTypeResource, "appointmentTypeService",
        appointmentTypeService);
    this.restAppointmentTypeMockMvc = MockMvcBuilders.standaloneSetup(appointmentTypeResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static AppointmentType createEntity(EntityManager em) {
    AppointmentType appointmentType = new AppointmentType().name(DEFAULT_NAME)
        .isOfficial(DEFAULT_IS_OFFICIAL);
    return appointmentType;
  }

  @Before
  public void initTest() {
    appointmentType = createEntity(em);
  }

  @Test
  @Transactional
  public void createAppointmentType() throws Exception {
    int databaseSizeBeforeCreate = appointmentTypeRepository.findAll().size();

    // Create the AppointmentType
    AppointmentTypeDTO appointmentTypeDTO = appointmentTypeMapper
        .appointmentTypeToAppointmentTypeDTO(appointmentType);

    restAppointmentTypeMockMvc
        .perform(post("/api/appointment-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentTypeDTO)))
        .andExpect(status().isCreated());

    // Validate the AppointmentType in the database
    List<AppointmentType> appointmentTypes = appointmentTypeRepository.findAll();
    assertThat(appointmentTypes).hasSize(databaseSizeBeforeCreate + 1);
    AppointmentType testAppointmentType = appointmentTypes.get(appointmentTypes.size() - 1);
    assertThat(testAppointmentType.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testAppointmentType.isIsOfficial()).isEqualTo(DEFAULT_IS_OFFICIAL);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = appointmentTypeRepository.findAll().size();
    // set the field null
    appointmentType.setName(null);

    // Create the AppointmentType, which fails.
    AppointmentTypeDTO appointmentTypeDTO = appointmentTypeMapper
        .appointmentTypeToAppointmentTypeDTO(appointmentType);

    restAppointmentTypeMockMvc
        .perform(post("/api/appointment-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentTypeDTO)))
        .andExpect(status().isBadRequest());

    List<AppointmentType> appointmentTypes = appointmentTypeRepository.findAll();
    assertThat(appointmentTypes).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkIsOfficialIsRequired() throws Exception {
    int databaseSizeBeforeTest = appointmentTypeRepository.findAll().size();
    // set the field null
    appointmentType.setIsOfficial(null);

    // Create the AppointmentType, which fails.
    AppointmentTypeDTO appointmentTypeDTO = appointmentTypeMapper
        .appointmentTypeToAppointmentTypeDTO(appointmentType);

    restAppointmentTypeMockMvc
        .perform(post("/api/appointment-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentTypeDTO)))
        .andExpect(status().isBadRequest());

    List<AppointmentType> appointmentTypes = appointmentTypeRepository.findAll();
    assertThat(appointmentTypes).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllAppointmentTypes() throws Exception {
    // Initialize the database
    appointmentTypeRepository.saveAndFlush(appointmentType);

    // Get all the appointmentTypes
    restAppointmentTypeMockMvc.perform(get("/api/appointment-types?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(appointmentType.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].isOfficial").value(hasItem(DEFAULT_IS_OFFICIAL.booleanValue())));
  }

  @Test
  @Transactional
  public void getAppointmentType() throws Exception {
    // Initialize the database
    appointmentTypeRepository.saveAndFlush(appointmentType);

    // Get the appointmentType
    restAppointmentTypeMockMvc.perform(get("/api/appointment-types/{id}", appointmentType.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(appointmentType.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.isOfficial").value(DEFAULT_IS_OFFICIAL.booleanValue()));
  }

  @Test
  @Transactional
  public void getNonExistingAppointmentType() throws Exception {
    // Get the appointmentType
    restAppointmentTypeMockMvc.perform(get("/api/appointment-types/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateAppointmentType() throws Exception {
    // Initialize the database
    appointmentTypeRepository.saveAndFlush(appointmentType);
    int databaseSizeBeforeUpdate = appointmentTypeRepository.findAll().size();

    // Update the appointmentType
    AppointmentType updatedAppointmentType = appointmentTypeRepository
        .findById(appointmentType.getId()).orElse(null);
    updatedAppointmentType.name(UPDATED_NAME).isOfficial(UPDATED_IS_OFFICIAL);
    AppointmentTypeDTO appointmentTypeDTO = appointmentTypeMapper
        .appointmentTypeToAppointmentTypeDTO(updatedAppointmentType);

    restAppointmentTypeMockMvc
        .perform(put("/api/appointment-types").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(appointmentTypeDTO)))
        .andExpect(status().isOk());

    // Validate the AppointmentType in the database
    List<AppointmentType> appointmentTypes = appointmentTypeRepository.findAll();
    assertThat(appointmentTypes).hasSize(databaseSizeBeforeUpdate);
    AppointmentType testAppointmentType = appointmentTypes.get(appointmentTypes.size() - 1);
    assertThat(testAppointmentType.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testAppointmentType.isIsOfficial()).isEqualTo(UPDATED_IS_OFFICIAL);
  }

  @Test
  @Transactional
  public void deleteAppointmentType() throws Exception {
    // Initialize the database
    appointmentTypeRepository.saveAndFlush(appointmentType);
    int databaseSizeBeforeDelete = appointmentTypeRepository.findAll().size();

    // Get the appointmentType
    restAppointmentTypeMockMvc
        .perform(delete("/api/appointment-types/{id}", appointmentType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<AppointmentType> appointmentTypes = appointmentTypeRepository.findAll();
    assertThat(appointmentTypes).hasSize(databaseSizeBeforeDelete - 1);
  }
}
