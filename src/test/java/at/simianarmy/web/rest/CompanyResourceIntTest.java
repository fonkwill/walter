package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Company;
import at.simianarmy.repository.CompanyRepository;
import at.simianarmy.service.CompanyService;
import at.simianarmy.service.dto.CompanyDTO;
import at.simianarmy.service.mapper.CompanyMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the CompanyResource REST controller.
 *
 * @see CompanyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompanyResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  private static final String DEFAULT_STREET_ADDRESS = "AAAAA";
  private static final String UPDATED_STREET_ADDRESS = "BBBBB";

  private static final String DEFAULT_POSTAL_CODE = "AAAAA";
  private static final String UPDATED_POSTAL_CODE = "BBBBB";

  private static final String DEFAULT_CITY = "AAAAA";
  private static final String UPDATED_CITY = "BBBBB";

  private static final String DEFAULT_COUNTRY = "AAAAA";
  private static final String UPDATED_COUNTRY = "BBBBB";

  @Inject
  private CompanyRepository companyRepository;

  @Inject
  private CompanyMapper companyMapper;

  @Inject
  private CompanyService companyService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restCompanyMockMvc;

  private Company company;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    CompanyResource companyResource = new CompanyResource();
    ReflectionTestUtils.setField(companyResource, "companyService", companyService);
    this.restCompanyMockMvc = MockMvcBuilders.standaloneSetup(companyResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static Company createEntity(EntityManager em) {
    Company company = new Company().name(DEFAULT_NAME).streetAddress(DEFAULT_STREET_ADDRESS)
        .postalCode(DEFAULT_POSTAL_CODE).city(DEFAULT_CITY).country(DEFAULT_COUNTRY);
    return company;
  }

  @Before
  public void initTest() {
    company = createEntity(em);
  }

  @Test
  @Transactional
  public void createCompany() throws Exception {
    int databaseSizeBeforeCreate = companyRepository.findAll().size();

    // Create the Company
    CompanyDTO companyDTO = companyMapper.companyToCompanyDTO(company);

    restCompanyMockMvc
        .perform(post("/api/companies").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
        .andExpect(status().isCreated());

    // Validate the Company in the database
    List<Company> companies = companyRepository.findAll();
    assertThat(companies).hasSize(databaseSizeBeforeCreate + 1);
    Company testCompany = companies.get(companies.size() - 1);
    assertThat(testCompany.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testCompany.getStreetAddress()).isEqualTo(DEFAULT_STREET_ADDRESS);
    assertThat(testCompany.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
    assertThat(testCompany.getCity()).isEqualTo(DEFAULT_CITY);
    assertThat(testCompany.getCountry()).isEqualTo(DEFAULT_COUNTRY);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = companyRepository.findAll().size();
    // set the field null
    company.setName(null);

    // Create the Company, which fails.
    CompanyDTO companyDTO = companyMapper.companyToCompanyDTO(company);

    restCompanyMockMvc
        .perform(post("/api/companies").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
        .andExpect(status().isBadRequest());

    List<Company> companies = companyRepository.findAll();
    assertThat(companies).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllCompanies() throws Exception {
    // Initialize the database
    companyRepository.saveAndFlush(company);

    // Get all the companies
    restCompanyMockMvc.perform(get("/api/companies?sort=id,desc")).andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(
            jsonPath("$.[*].streetAddress").value(hasItem(DEFAULT_STREET_ADDRESS.toString())))
        .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE.toString())))
        .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
        .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())));
  }

  @Test
  @Transactional
  public void getCompany() throws Exception {
    // Initialize the database
    companyRepository.saveAndFlush(company);

    // Get the company
    restCompanyMockMvc.perform(get("/api/companies/{id}", company.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(company.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.streetAddress").value(DEFAULT_STREET_ADDRESS.toString()))
        .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE.toString()))
        .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
        .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingCompany() throws Exception {
    // Get the company
    restCompanyMockMvc.perform(get("/api/companies/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateCompany() throws Exception {
    // Initialize the database
    companyRepository.saveAndFlush(company);
    int databaseSizeBeforeUpdate = companyRepository.findAll().size();

    // Update the company
    Company updatedCompany = companyRepository.findById(company.getId()).orElse(null);
    updatedCompany.name(UPDATED_NAME).streetAddress(UPDATED_STREET_ADDRESS)
        .postalCode(UPDATED_POSTAL_CODE).city(UPDATED_CITY).country(UPDATED_COUNTRY);
    CompanyDTO companyDTO = companyMapper.companyToCompanyDTO(updatedCompany);

    restCompanyMockMvc.perform(put("/api/companies").contentType(TestUtil.APPLICATION_JSON_UTF8)
        .content(TestUtil.convertObjectToJsonBytes(companyDTO))).andExpect(status().isOk());

    // Validate the Company in the database
    List<Company> companies = companyRepository.findAll();
    assertThat(companies).hasSize(databaseSizeBeforeUpdate);
    Company testCompany = companies.get(companies.size() - 1);
    assertThat(testCompany.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testCompany.getStreetAddress()).isEqualTo(UPDATED_STREET_ADDRESS);
    assertThat(testCompany.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
    assertThat(testCompany.getCity()).isEqualTo(UPDATED_CITY);
    assertThat(testCompany.getCountry()).isEqualTo(UPDATED_COUNTRY);
  }

  @Test
  @Transactional
  public void deleteCompany() throws Exception {
    // Initialize the database
    companyRepository.saveAndFlush(company);
    int databaseSizeBeforeDelete = companyRepository.findAll().size();

    // Get the company
    restCompanyMockMvc
        .perform(
            delete("/api/companies/{id}", company.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<Company> companies = companyRepository.findAll();
    assertThat(companies).hasSize(databaseSizeBeforeDelete - 1);
  }
}
