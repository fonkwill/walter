package at.simianarmy.web.rest;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.InstrumentService;
import at.simianarmy.domain.Instrument;
import at.simianarmy.repository.InstrumentServiceRepository;
import at.simianarmy.service.InstrumentServiceService;
import at.simianarmy.service.dto.InstrumentServiceDTO;
import at.simianarmy.service.mapper.InstrumentServiceMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InstrumentServiceResource REST controller.
 *
 * @see InstrumentServiceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class InstrumentServiceResourceIntTest {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NOTE = "AAAAA";
    private static final String UPDATED_NOTE = "BBBBB";

    @Inject
    private InstrumentServiceRepository instrumentServiceRepository;

    @Inject
    private InstrumentServiceMapper instrumentServiceMapper;

    @Inject
    private InstrumentServiceService instrumentServiceService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restInstrumentServiceMockMvc;

    private InstrumentService instrumentService;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InstrumentServiceResource instrumentServiceResource = new InstrumentServiceResource();
        ReflectionTestUtils.setField(instrumentServiceResource, "instrumentServiceService", instrumentServiceService);
        this.restInstrumentServiceMockMvc = MockMvcBuilders.standaloneSetup(instrumentServiceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InstrumentService createEntity(EntityManager em) {
        InstrumentService instrumentService = new InstrumentService()
                .date(DEFAULT_DATE)
                .note(DEFAULT_NOTE);
        // Add required entity
        Instrument instrument = InstrumentResourceIntTest.createEntity(em);
        em.persist(instrument);
        em.flush();
        instrumentService.setInstrument(instrument);
        return instrumentService;
    }

    @Before
    public void initTest() {
        instrumentService = createEntity(em);
    }

    @Test
    @Transactional
    public void createInstrumentService() throws Exception {
        int databaseSizeBeforeCreate = instrumentServiceRepository.findAll().size();

        // Create the InstrumentService
        InstrumentServiceDTO instrumentServiceDTO = instrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(instrumentService);

        restInstrumentServiceMockMvc.perform(post("/api/instrument-services")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentServiceDTO)))
                .andExpect(status().isCreated());

        // Validate the InstrumentService in the database
        List<InstrumentService> instrumentServices = instrumentServiceRepository.findAll();
        assertThat(instrumentServices).hasSize(databaseSizeBeforeCreate + 1);
        InstrumentService testInstrumentService = instrumentServices.get(instrumentServices.size() - 1);
        assertThat(testInstrumentService.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testInstrumentService.getNote()).isEqualTo(DEFAULT_NOTE);
    }

    @Test
    @Transactional
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = instrumentServiceRepository.findAll().size();
        // set the field null
        instrumentService.setDate(null);

        // Create the InstrumentService, which fails.
        InstrumentServiceDTO instrumentServiceDTO = instrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(instrumentService);

        restInstrumentServiceMockMvc.perform(post("/api/instrument-services")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentServiceDTO)))
                .andExpect(status().isBadRequest());

        List<InstrumentService> instrumentServices = instrumentServiceRepository.findAll();
        assertThat(instrumentServices).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInstrumentServices() throws Exception {
        // Initialize the database
        instrumentServiceRepository.saveAndFlush(instrumentService);

        // Get all the instrumentServices
        restInstrumentServiceMockMvc.perform(get("/api/instrument-services?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(instrumentService.getId().intValue())))
                .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
                .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    @Test
    @Transactional
    public void getInstrumentService() throws Exception {
        // Initialize the database
        instrumentServiceRepository.saveAndFlush(instrumentService);

        // Get the instrumentService
        restInstrumentServiceMockMvc.perform(get("/api/instrument-services/{id}", instrumentService.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(instrumentService.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInstrumentService() throws Exception {
        // Get the instrumentService
        restInstrumentServiceMockMvc.perform(get("/api/instrument-services/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInstrumentService() throws Exception {
        // Initialize the database
        instrumentServiceRepository.saveAndFlush(instrumentService);
        int databaseSizeBeforeUpdate = instrumentServiceRepository.findAll().size();

        // Update the instrumentService
        InstrumentService updatedInstrumentService = instrumentServiceRepository.findById(instrumentService.getId()).orElse(null);
        updatedInstrumentService
                .date(UPDATED_DATE)
                .note(UPDATED_NOTE);
        InstrumentServiceDTO instrumentServiceDTO = instrumentServiceMapper.instrumentServiceToInstrumentServiceDTO(updatedInstrumentService);

        restInstrumentServiceMockMvc.perform(put("/api/instrument-services")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(instrumentServiceDTO)))
                .andExpect(status().isOk());

        // Validate the InstrumentService in the database
        List<InstrumentService> instrumentServices = instrumentServiceRepository.findAll();
        assertThat(instrumentServices).hasSize(databaseSizeBeforeUpdate);
        InstrumentService testInstrumentService = instrumentServices.get(instrumentServices.size() - 1);
        assertThat(testInstrumentService.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testInstrumentService.getNote()).isEqualTo(UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void deleteInstrumentService() throws Exception {
        // Initialize the database
        instrumentServiceRepository.saveAndFlush(instrumentService);
        int databaseSizeBeforeDelete = instrumentServiceRepository.findAll().size();

        // Get the instrumentService
        restInstrumentServiceMockMvc.perform(delete("/api/instrument-services/{id}", instrumentService.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<InstrumentService> instrumentServices = instrumentServiceRepository.findAll();
        assertThat(instrumentServices).hasSize(databaseSizeBeforeDelete - 1);
    }
}
