package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.repository.CashbookCategoryRepository;
import at.simianarmy.service.CashbookCategoryService;
import at.simianarmy.service.dto.CashbookCategoryDTO;
import at.simianarmy.service.mapper.CashbookCategoryMapper;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the CashbookCategoryResource REST controller.
 *
 * @see CashbookCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookCategoryResourceIntTest {

  private static final String DEFAULT_NAME = "AAAAA";
  private static final String UPDATED_NAME = "BBBBB";

  private static final String DEFAULT_DESCRIPTION = "AAAAA";
  private static final String UPDATED_DESCRIPTION = "BBBBB";

  private static final String DEFAULT_COLOR = "#efefef";
  private static final String UPDATED_COLOR = "#fefefe";

  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  private CashbookCategoryMapper cashbookCategoryMapper;

  @Inject
  private CashbookCategoryService cashbookCategoryService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restCashbookCategoryMockMvc;

  private CashbookCategory cashbookCategory;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    CashbookCategoryResource cashbookCategoryResource = new CashbookCategoryResource();
    ReflectionTestUtils.setField(cashbookCategoryResource, "cashbookCategoryService",
        cashbookCategoryService);
    this.restCashbookCategoryMockMvc = MockMvcBuilders.standaloneSetup(cashbookCategoryResource)
        .setCustomArgumentResolvers(pageableArgumentResolver)
        .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * Create an entity for this test.
   *
   * This is a static method, as tests for other entities might also need it, if they test an entity
   * which requires the current entity.
   */
  public static CashbookCategory createEntity(EntityManager em) {
    CashbookCategory cashbookCategory = new CashbookCategory().name(DEFAULT_NAME)
        .description(DEFAULT_DESCRIPTION).color(DEFAULT_COLOR);
    return cashbookCategory;
  }

  @Before
  public void initTest() {
    cashbookCategory = createEntity(em);
  }

  @Test
  @Transactional
  public void createCashbookCategory() throws Exception {
    int databaseSizeBeforeCreate = cashbookCategoryRepository.findAll().size();

    // Create the CashbookCategory
    CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryMapper
        .cashbookCategoryToCashbookCategoryDTO(cashbookCategory);

    restCashbookCategoryMockMvc
        .perform(post("/api/cashbook-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookCategoryDTO)))
        .andExpect(status().isCreated());

    // Validate the CashbookCategory in the database
    List<CashbookCategory> cashbookCategories = cashbookCategoryRepository.findAll();
    assertThat(cashbookCategories).hasSize(databaseSizeBeforeCreate + 1);
    CashbookCategory testCashbookCategory = cashbookCategories.get(cashbookCategories.size() - 1);
    assertThat(testCashbookCategory.getName()).isEqualTo(DEFAULT_NAME);
    assertThat(testCashbookCategory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    assertThat(testCashbookCategory.getColor()).isEqualTo(DEFAULT_COLOR);
  }

  @Test
  @Transactional
  public void checkNameIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookCategoryRepository.findAll().size();
    // set the field null
    cashbookCategory.setName(null);

    // Create the CashbookCategory, which fails.
    CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryMapper
        .cashbookCategoryToCashbookCategoryDTO(cashbookCategory);

    restCashbookCategoryMockMvc
        .perform(post("/api/cashbook-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookCategoryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookCategory> cashbookCategories = cashbookCategoryRepository.findAll();
    assertThat(cashbookCategories).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void checkColorIsRequired() throws Exception {
    int databaseSizeBeforeTest = cashbookCategoryRepository.findAll().size();
    // set the field null
    cashbookCategory.setColor(null);

    // Create the CashbookCategory, which fails.
    CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryMapper
        .cashbookCategoryToCashbookCategoryDTO(cashbookCategory);

    restCashbookCategoryMockMvc
        .perform(post("/api/cashbook-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookCategoryDTO)))
        .andExpect(status().isBadRequest());

    List<CashbookCategory> cashbookCategories = cashbookCategoryRepository.findAll();
    assertThat(cashbookCategories).hasSize(databaseSizeBeforeTest);
  }

  @Test
  @Transactional
  public void getAllCashbookCategories() throws Exception {
    // Initialize the database
    cashbookCategoryRepository.saveAndFlush(cashbookCategory);

    // Get all the cashbookCategories
    restCashbookCategoryMockMvc.perform(get("/api/cashbook-categories?sort=id,desc"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.[*].id").value(hasItem(cashbookCategory.getId().intValue())))
        .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
        .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
        .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR.toString())));
  }

  @Test
  @Transactional
  public void getCashbookCategory() throws Exception {
    // Initialize the database
    cashbookCategoryRepository.saveAndFlush(cashbookCategory);

    // Get the cashbookCategory
    restCashbookCategoryMockMvc
        .perform(get("/api/cashbook-categories/{id}", cashbookCategory.getId()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$.id").value(cashbookCategory.getId().intValue()))
        .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
        .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
        .andExpect(jsonPath("$.color").value(DEFAULT_COLOR.toString()));
  }

  @Test
  @Transactional
  public void getNonExistingCashbookCategory() throws Exception {
    // Get the cashbookCategory
    restCashbookCategoryMockMvc.perform(get("/api/cashbook-categories/{id}", Long.MAX_VALUE))
        .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateCashbookCategory() throws Exception {
    // Initialize the database
    cashbookCategoryRepository.saveAndFlush(cashbookCategory);
    int databaseSizeBeforeUpdate = cashbookCategoryRepository.findAll().size();

    // Update the cashbookCategory
    CashbookCategory updatedCashbookCategory = cashbookCategoryRepository
        .findById(cashbookCategory.getId()).orElse(null);
    updatedCashbookCategory.name(UPDATED_NAME).description(UPDATED_DESCRIPTION)
        .color(UPDATED_COLOR);
    CashbookCategoryDTO cashbookCategoryDTO = cashbookCategoryMapper
        .cashbookCategoryToCashbookCategoryDTO(updatedCashbookCategory);

    restCashbookCategoryMockMvc
        .perform(put("/api/cashbook-categories").contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cashbookCategoryDTO)))
        .andExpect(status().isOk());

    // Validate the CashbookCategory in the database
    List<CashbookCategory> cashbookCategories = cashbookCategoryRepository.findAll();
    assertThat(cashbookCategories).hasSize(databaseSizeBeforeUpdate);
    CashbookCategory testCashbookCategory = cashbookCategories.get(cashbookCategories.size() - 1);
    assertThat(testCashbookCategory.getName()).isEqualTo(UPDATED_NAME);
    assertThat(testCashbookCategory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    assertThat(testCashbookCategory.getColor()).isEqualTo(UPDATED_COLOR);
  }

  @Test
  @Transactional
  public void deleteCashbookCategory() throws Exception {
    // Initialize the database
    cashbookCategoryRepository.saveAndFlush(cashbookCategory);
    int databaseSizeBeforeDelete = cashbookCategoryRepository.findAll().size();

    // Get the cashbookCategory
    restCashbookCategoryMockMvc
        .perform(delete("/api/cashbook-categories/{id}", cashbookCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk());

    // Validate the database is empty
    List<CashbookCategory> cashbookCategories = cashbookCategoryRepository.findAll();
    assertThat(cashbookCategories).hasSize(databaseSizeBeforeDelete - 1);
  }
}
