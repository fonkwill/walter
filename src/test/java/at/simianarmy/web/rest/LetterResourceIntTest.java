package at.simianarmy.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import at.simianarmy.WalterApp;

import at.simianarmy.domain.Letter;
import at.simianarmy.domain.enumeration.LetterType;
import at.simianarmy.repository.LetterRepository;
import at.simianarmy.service.LetterService;
import at.simianarmy.service.dto.LetterDTO;
import at.simianarmy.service.mapper.LetterMapper;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;


/**
 * Test class for the LetterResource REST controller.
 *
 * @see LetterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class LetterResourceIntTest {

  private static final String DEFAULT_TITLE = "AAAAA";
  private static final String UPDATED_TITLE = "BBBBB";

  private static final String DEFAULT_TEXT = "AAAAA";
  private static final String UPDATED_TEXT = "BBBBB";

  private static final Boolean DEFAULT_PAYMENT = false;
  private static final Boolean UPDATED_PAYMENT = true;

  private static final Boolean DEFAULT_CURRENT_PERIOD = false;
  private static final Boolean UPDATED_CURRENT_PERIOD = true;

  private static final Boolean DEFAULT_PREVIOUS_PERIOD = false;
  private static final Boolean UPDATED_PREVIOUS_PERIOD = true;

  @Inject
  private LetterRepository letterRepository;

  @Inject
  private LetterMapper letterMapper;

  @Inject
  private LetterService letterService;

  @Inject
  private MappingJackson2HttpMessageConverter jacksonMessageConverter;

  @Inject
  private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

  @Inject
  private EntityManager em;

  private MockMvc restLetterMockMvc;

  private Letter letter;

  @PostConstruct
  public void setup() {
    MockitoAnnotations.initMocks(this);
    LetterResource letterResource = new LetterResource();
    ReflectionTestUtils.setField(letterResource, "letterService", letterService);
    this.restLetterMockMvc = MockMvcBuilders.standaloneSetup(letterResource)
      .setCustomArgumentResolvers(pageableArgumentResolver)
      .setMessageConverters(jacksonMessageConverter).build();
  }

  /**
   * <p></p>Create an entity for this test.</p>
   * <p>This is a static method, as tests for other entities might also need it,
   * if they test an entity which requires the current entity.</p>
   */
  public static Letter createEntity(EntityManager em) {
    Letter letter = new Letter()
        .title(DEFAULT_TITLE)
        .text(DEFAULT_TEXT)
        .payment(DEFAULT_PAYMENT)
        .currentPeriod(DEFAULT_CURRENT_PERIOD)
        .previousPeriod(DEFAULT_PREVIOUS_PERIOD)
        .letterType(LetterType.DEMAND_LETTER);
    return letter;
  }

  @Before
  public void initTest() {
    letter = createEntity(em);
  }

  @Test
  @Transactional
  public void createLetter() throws Exception {
    int databaseSizeBeforeCreate = letterRepository.findAll().size();

    // Create the Letter
    LetterDTO letterDTO = letterMapper.letterToLetterDTO(letter);

    restLetterMockMvc.perform(post("/api/letters")
      .contentType(TestUtil.APPLICATION_JSON_UTF8)
      .content(TestUtil.convertObjectToJsonBytes(letterDTO)))
      .andExpect(status().isCreated());

    // Validate the Letter in the database
    List<Letter> letters = letterRepository.findAll();
    assertThat(letters).hasSize(databaseSizeBeforeCreate + 1);
    Letter testLetter = letters.get(letters.size() - 1);
    assertThat(testLetter.getTitle()).isEqualTo(DEFAULT_TITLE);
    assertThat(testLetter.getText()).isEqualTo(DEFAULT_TEXT);
    assertThat(testLetter.isPayment()).isEqualTo(DEFAULT_PAYMENT);
    assertThat(testLetter.isCurrentPeriod()).isEqualTo(DEFAULT_CURRENT_PERIOD);
    assertThat(testLetter.isPreviousPeriod()).isEqualTo(DEFAULT_PREVIOUS_PERIOD);
  }

  @Test
  @Transactional
  public void getAllLetters() throws Exception {
    // Initialize the database
    letterRepository.saveAndFlush(letter);

    // Get all the letters
    restLetterMockMvc.perform(get("/api/letters?sort=id,desc&letterType=DEMAND_LETTER"))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
      .andExpect(jsonPath("$.[*].id").value(hasItem(letter.getId().intValue())))
      .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
      .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
      .andExpect(jsonPath("$.[*].payment").value(hasItem(DEFAULT_PAYMENT.booleanValue())))
      .andExpect(jsonPath("$.[*].currentPeriod").value(hasItem(DEFAULT_CURRENT_PERIOD.booleanValue())))
      .andExpect(jsonPath("$.[*].previousPeriod").value(hasItem(DEFAULT_PREVIOUS_PERIOD.booleanValue())));
  }

  @Test
  @Transactional
  public void getLetter() throws Exception {
    // Initialize the database
    letterRepository.saveAndFlush(letter);

    // Get the letter
    restLetterMockMvc.perform(get("/api/letters/{id}", letter.getId()))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
      .andExpect(jsonPath("$.id").value(letter.getId().intValue()))
      .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
      .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
      .andExpect(jsonPath("$.payment").value(DEFAULT_PAYMENT.booleanValue()))
      .andExpect(jsonPath("$.currentPeriod").value(DEFAULT_CURRENT_PERIOD.booleanValue()))
      .andExpect(jsonPath("$.previousPeriod").value(DEFAULT_PREVIOUS_PERIOD.booleanValue()));
  }

  @Test
  @Transactional
  public void getNonExistingLetter() throws Exception {
    // Get the letter
    restLetterMockMvc.perform(get("/api/letters/{id}", Long.MAX_VALUE))
      .andExpect(status().isNotFound());
  }

  @Test
  @Transactional
  public void updateLetter() throws Exception {
    // Initialize the database
    letterRepository.saveAndFlush(letter);
    int databaseSizeBeforeUpdate = letterRepository.findAll().size();

    // Update the letter
    Letter updatedLetter = letterRepository.findById(letter.getId()).orElse(null);
    updatedLetter
      .title(UPDATED_TITLE)
      .text(UPDATED_TEXT)
      .payment(UPDATED_PAYMENT)
      .currentPeriod(UPDATED_CURRENT_PERIOD)
      .previousPeriod(UPDATED_PREVIOUS_PERIOD);
    LetterDTO letterDTO = letterMapper.letterToLetterDTO(updatedLetter);

    restLetterMockMvc.perform(put("/api/letters")
      .contentType(TestUtil.APPLICATION_JSON_UTF8)
      .content(TestUtil.convertObjectToJsonBytes(letterDTO)))
      .andExpect(status().isOk());

    // Validate the Letter in the database
    List<Letter> letters = letterRepository.findAll();
    assertThat(letters).hasSize(databaseSizeBeforeUpdate);
    Letter testLetter = letters.get(letters.size() - 1);
    assertThat(testLetter.getTitle()).isEqualTo(UPDATED_TITLE);
    assertThat(testLetter.getText()).isEqualTo(UPDATED_TEXT);
    assertThat(testLetter.isPayment()).isEqualTo(UPDATED_PAYMENT);
    assertThat(testLetter.isCurrentPeriod()).isEqualTo(UPDATED_CURRENT_PERIOD);
    assertThat(testLetter.isPreviousPeriod()).isEqualTo(UPDATED_PREVIOUS_PERIOD);
  }

  @Test
  @Transactional
  public void deleteLetter() throws Exception {
    // Initialize the database
    letterRepository.saveAndFlush(letter);
    int databaseSizeBeforeDelete = letterRepository.findAll().size();

    // Get the letter
    restLetterMockMvc.perform(delete("/api/letters/{id}", letter.getId())
      .accept(TestUtil.APPLICATION_JSON_UTF8))
      .andExpect(status().isOk());

    // Validate the database is empty
    List<Letter> letters = letterRepository.findAll();
    assertThat(letters).hasSize(databaseSizeBeforeDelete - 1);
  }
}
