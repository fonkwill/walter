package at.simianarmy.imports.data.csv.mapper.qualifier;

import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.repository.MembershipFeeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;


public class PeopleCSVOQualifierTest {

  private static final String VOLLZAHLER = "Vollzahler";

  private PeopleCSVOQualifier peopleCSVOQualifier;


  @Before
  public void setUp() throws Exception {


    MembershipFeeRepository membershipFeeRepository = Mockito.mock(MembershipFeeRepository.class);
    MembershipFee feeVollzahler = new MembershipFee();
    feeVollzahler.setId(1L);
    feeVollzahler.setDescription(VOLLZAHLER);


    Mockito.when(membershipFeeRepository.findFirstByDescription(ArgumentMatchers.contains(VOLLZAHLER))).thenReturn(feeVollzahler);
    Mockito.when(membershipFeeRepository.findFirstByDescription(AdditionalMatchers.not(ArgumentMatchers.contains(VOLLZAHLER)))).thenReturn(null);

    peopleCSVOQualifier = new PeopleCSVOQualifier(membershipFeeRepository);

  }

  @Test
  public void emailTypeForEmailWithEMail_ReturnManually() {
    EmailType type = peopleCSVOQualifier.emailTypeForEmail("abc@gmx.at");
    Assert.assertEquals(EmailType.MANUALLY, type);
  }

  @Test
  public void emailTypeForEmailWithNull_ReturnVerified() {
    EmailType type = peopleCSVOQualifier.emailTypeForEmail(null);
    Assert.assertEquals(EmailType.VERIFIED, type);
  }

  @Test
  public void emailTypeForEmailWithEmpty_ReturnVerified() {
    EmailType type = peopleCSVOQualifier.emailTypeForEmail("");
    Assert.assertEquals(EmailType.VERIFIED, type);
  }

  @Test
  public void membershipFeeIdForMembershipDescriptionWithExisting_ReturnId() {
    Long id = peopleCSVOQualifier.membershipFeeIdForMembershipDescription("Vollzahler");
    Assert.assertEquals(Long.valueOf(1), id);

  }

  @Test
  public void membershipFeeIdForMembershipDescriptionWithNonExisting_ReturnNull() {
    Long id = peopleCSVOQualifier.membershipFeeIdForMembershipDescription("Ermäßigt");
    Assert.assertNull(id);

  }
}
