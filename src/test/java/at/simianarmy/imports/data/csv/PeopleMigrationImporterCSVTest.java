package at.simianarmy.imports.data.csv;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;
import at.simianarmy.imports.data.DataImporterException;
import at.simianarmy.imports.data.csv.mapper.PeopleCSVOMapperImpl;
import at.simianarmy.imports.data.csv.mapper.qualifier.PeopleCSVOQualifier;
import at.simianarmy.service.dto.MemberDTO;
import at.simianarmy.service.dto.MembershipDTO;
import at.simianarmy.service.dto.PersonDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PeopleMigrationImporterCSVTest {

  private static String invalidCSV = "/csv/person_csv_test_invalid_headers.csv";
  private static String onePersonCSV = "/csv/person_csv_one_person.csv";
  private static String oneMemberCSV = "/csv/person_csv_one_member.csv";
  private static String oneMemberOnePersonCSV = "/csv/person_csv_one_person_one_member.csv";

  private static final String FILENAME = "testfile.csv";
  private static final String ORIG_FILENAME = "testfile.csv";
  private static final String CONTENT_TYPE_CSV = "application/csv";


  private PeopleImporterCSV sut;

  @Mock
  private PeopleCSVOQualifier peopleCSVOQualifier;

  @InjectMocks
  private PeopleCSVOMapperImpl peopleCSVOMapper;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(PeopleImporterCSV.class);

    Mockito.when(peopleCSVOQualifier.membershipFeeIdForMembershipDescription(ArgumentMatchers.contains("Vollzahler"))).thenReturn(Long.valueOf(1));
    Mockito.when(peopleCSVOQualifier.emailTypeForEmail(ArgumentMatchers.any())).thenCallRealMethod();

    sut = new PeopleImporterCSV(peopleCSVOMapper);


  }

  private MultipartFile getMultipartFile(String file)  {

    try {
      Path path = Paths.get(this.getClass().getResource(file).toURI());
      MultipartFile csvFile = new MockMultipartFile(FILENAME, ORIG_FILENAME, CONTENT_TYPE_CSV,
        Files.readAllBytes(path));
      return csvFile;
    } catch (URISyntaxException | IOException e) {
      return null;
    }
  }

  @Test(expected = DataImporterException.class)
  public void readInvalidData_throwExpection() throws DataImporterException {
    MultipartFile file = getMultipartFile(invalidCSV);
    List<MemberDTO> members = sut.readData(file);
  }

  @Test
  public void readInData_correctDataForOnePerson() throws DataImporterException {
    MultipartFile file = getMultipartFile(onePersonCSV);
    List<MemberDTO> members = sut.readData(file);

    Assert.assertEquals(1, members.size());

    MemberDTO member = members.iterator().next();
    if (member == null) {
      Assert.fail();
    }
    validateMale(member);
  }

  @Test
  public void readInData_correctDataForOneMember() throws DataImporterException {
    MultipartFile file = getMultipartFile(oneMemberCSV);
    List<MemberDTO> members = sut.readData(file);

    Assert.assertEquals(1, members.size());

    MemberDTO member = members.iterator().next();
    if (member == null) {
      Assert.fail();
    }

    validateFemale(member);

  }

  @Test
  public void readInData_correctDataForOneMemberAndOnePerson() throws DataImporterException {
    MultipartFile file = getMultipartFile(oneMemberOnePersonCSV);
    List<MemberDTO> members = sut.readData(file);

    Assert.assertEquals(2, members.size());

    members.stream().forEach(m -> {
      if (m.getPerson().getGender().equals(Gender.MALE)) {
        validateMale(m);
      } else {
        validateFemale(m);
      }
    });
  }

  private void validateFemale(MemberDTO member) {
    PersonDTO person = member.getPerson();

    Assert.assertEquals(Gender.FEMALE, person.getGender());
    Assert.assertEquals("Huber", person.getLastName());
    Assert.assertEquals("Brigitte", person.getFirstName());
    Assert.assertEquals(LocalDate.of(1965, 06, 02), person.getBirthDate());
    Assert.assertEquals(EmailType.VERIFIED, person.getEmailType());
    Assert.assertEquals("0660/34526354", person.getTelephoneNumber());
    Assert.assertEquals("Obere Huberstraße 32", person.getStreetAddress());
    Assert.assertEquals("3546", person.getPostalCode());
    Assert.assertEquals("Unterbrunn", person.getCity());
    Assert.assertEquals("Österreich",  person.getCountry());
    Assert.assertNotNull(person.getHasDirectDebit());

    MembershipDTO membership = member.getMembership();

    Assert.assertEquals(LocalDate.of(2018, 01, 01), membership.getBeginDate());
    Assert.assertEquals(null, membership.getEndDate());
    Assert.assertEquals(Long.valueOf(1), membership.getMembershipFeeId());
  }

  private void validateMale(MemberDTO member) {
    PersonDTO person = member.getPerson();

    MembershipDTO membershipDTO = new MembershipDTO();
    Assert.assertEquals(member.getMembership(), membershipDTO);

    Assert.assertEquals(Gender.MALE, person.getGender());
    Assert.assertEquals("Huber", person.getLastName());
    Assert.assertEquals("Hans", person.getFirstName());
    Assert.assertEquals(LocalDate.of(1963, 05, 01), person.getBirthDate());
    Assert.assertEquals("huber.hans@gmx.at", person.getEmail());
    Assert.assertEquals(EmailType.MANUALLY, person.getEmailType());
    Assert.assertEquals("0664/12134567", person.getTelephoneNumber());
    Assert.assertEquals("Unter Huberstraße 55", person.getStreetAddress());
    Assert.assertEquals("3746", person.getPostalCode());
    Assert.assertEquals("Oberbrunn", person.getCity());
    Assert.assertEquals("Österreich",  person.getCountry());
    Assert.assertNotNull(person.getHasDirectDebit());
  }
}
