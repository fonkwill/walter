package at.simianarmy.imports.accounting.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterGeneral;
import at.simianarmy.imports.accounting.BankImporterGeneralTest;
import at.simianarmy.imports.accounting.api.client.APIClientException;
import at.simianarmy.imports.accounting.api.client.AuthenticationFailureException;
import at.simianarmy.imports.accounting.api.client.GeorgeAPIClient;
import at.simianarmy.imports.accounting.api.client.json.GeorgeAmount;
import at.simianarmy.imports.accounting.api.client.json.GeorgeResponse;
import at.simianarmy.repository.BankImportDataRepository;

import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CustomizationService;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class BankImporterGeorgeAPITest extends BankImporterGeneralTest {

  private static final String DEFAULT_USER = "TESTUSER";
  private static final String DEFAULT_PASSWORD = "TESTPWD";
  private static final String DEFAULT_NAME = "TESTNAME";
  private static final String DEFAULT_TEXT = "TEXT";
  private static final String DEFAULT_REFERENCE = "12345";
  private static final Long CASHBOOK_ACCOUNT_ID = new Long(1);

  private CashbookCategory cashbookCategory = Mockito.mock(CashbookCategory.class);
  private CashbookAccount cashbookAccount;

  @Mock
  private BankImportDataRepository bankImportDataRepository;

  @Mock
  private GeorgeAPIClient client;

  @Mock
  private CashbookAccountRepository cashbookAccountRepository;

  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private BankImporterGeorgeAPI sut;

  @Before
  public void init() {

    this.cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccount.getId()).thenReturn(CASHBOOK_ACCOUNT_ID);
    Mockito.when(this.cashbookAccount.getBankImporterType()).thenReturn(BankImporterType.GEORGE_IMPORTER);
    Mockito.when(this.cashbookAccount.isBankAccount()).thenReturn(true);

    Mockito
        .when(this.bankImportDataRepository.findByCashbookAccountAndReference(
      Mockito.eq(this.getCashbookAccount().getId()), Mockito.any(byte[].class)))
        .thenReturn(new ArrayList<BankImportData>());
    Mockito.when(this.bankImportDataRepository.save(Mockito.any(BankImportData.class)))
        .thenAnswer(new Answer<BankImportData>() {
          public BankImportData answer(InvocationOnMock invocation) {
            return invocation.getArgument(0);
          }
        });
  }

  @Test
  public void testReadData() throws BankImporterException, ParseException, APIClientException {

    DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    Date from = formatter.parse("01.01.2017");
    Date to = formatter.parse("31.01.2017");
    Date resp1Date = formatter.parse("02.01.2017");
    Date resp2Date = formatter.parse("16.01.2017");

    GeorgeResponse resp1 = new GeorgeResponse();
    GeorgeAmount amount1 = new GeorgeAmount();
    amount1.setValue(455);
    amount1.setPrecision(2);
    resp1.setAmount(amount1);
    resp1.setBooking(resp1Date);
    resp1.setPartnerName(DEFAULT_NAME);
    resp1.setReference(DEFAULT_TEXT);
    resp1.setReferenceNumber(DEFAULT_REFERENCE + 1);

    GeorgeResponse resp2 = new GeorgeResponse();
    GeorgeAmount amount2 = new GeorgeAmount();
    amount2.setValue(-20);
    amount2.setPrecision(2);
    resp2.setAmount(amount2);
    resp2.setBooking(resp2Date);
    resp2.setPartnerName(DEFAULT_NAME);
    resp2.setReference(DEFAULT_TEXT);
    resp2.setReferenceNumber(DEFAULT_REFERENCE + 2);

    List<GeorgeResponse> response = new ArrayList<GeorgeResponse>(Arrays.asList(resp1));

    Mockito.when(this.client.getTransactions(from, to)).thenReturn(response);

    List<BankImportData> result = this.sut.readData(DEFAULT_USER, DEFAULT_PASSWORD, from, to, this.cashbookAccount, this.cashbookCategory);

    Mockito.verify(this.client, Mockito.times(1)).setUsername(DEFAULT_USER);
    Mockito.verify(this.client, Mockito.times(1)).setPassword(DEFAULT_PASSWORD);
    Mockito.verify(this.client, Mockito.times(1)).getTransactions(from, to);

    assertEquals(result.size(), response.size());

    for (int i = 0; i < result.size(); i++) {
      this.assertEqualsGeorgeResponse(result.get(i), response.get(i));
    }
  }

  @Test
  public void testReadDataWithAuthenticationException() throws ParseException, APIClientException {
    DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    Date from = formatter.parse("01.01.2017");
    Date to = formatter.parse("31.01.2017");

    AuthenticationFailureException toThrow = new AuthenticationFailureException(
        "Authentication Failed!");

    Mockito.when(this.client.getTransactions(from, to)).thenThrow(toThrow);

    try {
      this.sut.readData(DEFAULT_USER, DEFAULT_PASSWORD, from, to, this.cashbookAccount, this.cashbookCategory);
      fail();
    } catch (BankImporterException ex) {
      assertEquals(toThrow, ex.getCause());
    }
  }

  @Test(expected = BankImporterException.class)
  public void testReadDataWithAPIException()
      throws ParseException, APIClientException, BankImporterException {
    DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    Date from = formatter.parse("01.01.2017");
    Date to = formatter.parse("31.01.2017");

    AuthenticationFailureException toThrow = new AuthenticationFailureException(
        "Authentication Failed!");

    Mockito.when(this.client.getTransactions(from, to)).thenThrow(toThrow);

    this.sut.readData(DEFAULT_USER, DEFAULT_PASSWORD, from, to, this.cashbookAccount, this.cashbookCategory);
  }

  private void assertEqualsGeorgeResponse(BankImportData data, GeorgeResponse response) {
    assertEquals(new String(data.getEntryReference()), response.getReferenceNumber());
    assertEquals(data.getCashbookAccount(), this.cashbookAccount);
    assertEquals(data.getPartnerName(), response.getPartnerName());

    LocalDate entryDate = response.getBooking().toInstant().atZone(ZoneId.systemDefault())
        .toLocalDate();
    BigInteger value = BigInteger.valueOf(response.getAmount().getValue());
    int precision = response.getAmount().getPrecision();
    BigDecimal entryValue = new BigDecimal(value, precision);

    assertEquals(data.getEntryValue(), entryValue);
    assertEquals(data.getEntryDate(), entryDate);
    assertEquals(this.cashbookCategory, data.getCashbookCategory());
  }

  @Override
  protected BankImporterGeneral getSut() {
    return this.sut;
  }

  @Override
  protected CashbookAccount getCashbookAccount() {
    return this.cashbookAccount;
  }

  @Override
  protected BankImportDataRepository getBankImportDataRepositoryMock() {
    return bankImportDataRepository;
  }

  @Override
  protected CashbookAccountRepository getCashbookAccountRepositoryMock() {
    return this.cashbookAccountRepository;
  }

  @Override
  protected CustomizationService getCustomizationServiceMock() {
    return this.customizationService;
  }

}
