package at.simianarmy.imports.accounting.csv;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.BankImportDataRepository;

import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CustomizationService;
import java.util.ArrayList;

import org.apache.commons.csv.CSVRecord;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class BankImporterGeorgeCSVTest extends BankImporterCSVWithHeaderTest {

  public static final String VALID_CSV_URL = "/csv/george_csv_test.csv";
  public static final String VALID_CSV_LONG_STRING_URL = "/csv/george_csv_test_long_string.csv";
  public static final String INVALID_HEADERS_CSV_URL = "/csv/george_csv_test_invalid_headers.csv";
  public static final String CHARSET = "UTF-16";
  public static final BankImporterType BANK_IMPORTER_TYPE = BankImporterType.GEORGE_IMPORTER;

  @Mock
  private BankImportDataRepository bankImportDataRepository;

  @Mock
  private CashbookAccountRepository cashbookAccountReposiory;

  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private BankImporterGeorgeCSV sut;

  @Before
  public void init() {
    super.init();

    Mockito
        .when(this.bankImportDataRepository.findByCashbookAccountAndReference(
          Mockito.eq(this.getCashbookAccount().getId()), Mockito.any(byte[].class)))
        .thenReturn(new ArrayList<BankImportData>());
    Mockito.when(this.bankImportDataRepository.save(Mockito.any(BankImportData.class)))
        .thenAnswer(new Answer<BankImportData>() {
          public BankImportData answer(InvocationOnMock invocation) {
            return invocation.getArgument(0);
          }
        });
  }

  @Override
  protected BankImporterType getBankImporterType() {
    return BANK_IMPORTER_TYPE;
  }

  @Override
  protected String getValidCSVPath() {
    return VALID_CSV_URL;
  }

  @Override
  protected String getInvalidHeadersCSVPath() {
    return INVALID_HEADERS_CSV_URL;
  }

  @Override
  protected String getValidCSVLongStringPath() {
    return VALID_CSV_LONG_STRING_URL;
  }
  
  @Override
  protected String getCharset() {
    return CHARSET;
  }

  @Override
  protected BankImporterCSVWithHeader getSut() {
    return this.sut;
  }
  
  @Override
  protected BankImportDataRepository getBankImportDataRepositoryMock() {
    return this.bankImportDataRepository;
  }

  @Override
  protected CashbookAccountRepository getCashbookAccountRepositoryMock() {
    return this.cashbookAccountReposiory;
  }

  @Override
  protected CustomizationService getCustomizationServiceMock() {
    return this.customizationService;
  }

  @Override
  protected byte[] getExpectedEntryReference(CSVRecord record) {
    return record.get(BankImporterGeorgeCSV.FIELD_ENTRY_REFERENCE).getBytes();
  }
}
