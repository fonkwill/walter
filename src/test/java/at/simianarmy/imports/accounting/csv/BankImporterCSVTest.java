package at.simianarmy.imports.accounting.csv;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.BankImporterException;
import at.simianarmy.imports.accounting.BankImporterGeneralTest;
import at.simianarmy.repository.BankImportDataRepository;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BankImporterCSVTest extends BankImporterGeneralTest {

  protected static final String FILENAME = "testfile.csv";
  protected static final String ORIG_FILENAME = "testfile.csv";
  protected static final String CONTENT_TYPE_CSV = "application/csv";
  protected static final Long CASHBOOK_ACCOUNT_ID = new Long(1);

  protected CashbookCategory cashbookCategory = Mockito.mock(CashbookCategory.class);
  protected CashbookAccount cashbookAccount;

  @Before
  public void init() {
    this.cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(this.cashbookAccount.getId()).thenReturn(CASHBOOK_ACCOUNT_ID);
    Mockito.when(this.cashbookAccount.getBankImporterType()).thenReturn(this.getBankImporterType());
    Mockito.when(this.cashbookAccount.isBankAccount()).thenReturn(true);
  }

  @Test
  public void testReadData() throws BankImporterException, IOException, URISyntaxException {
    Path path = Paths.get(this.getClass().getResource(this.getValidCSVPath()).toURI());

    MultipartFile csvFile = new MockMultipartFile(FILENAME, ORIG_FILENAME, CONTENT_TYPE_CSV,
        Files.readAllBytes(path));

    List<BankImportData> data = this.getSut().readData(csvFile, this.getCashbookAccount(), this.cashbookCategory);

    Reader in = new InputStreamReader(csvFile.getInputStream(), this.getCharset());

    CSVParser csvFileParser = new CSVParser(in, this.getSut().getFormat());

    List<CSVRecord> csvRecords = csvFileParser.getRecords();

    for (int i = 0; i < data.size(); i++) {
      this.assertEqualsCSVData(csvRecords.get(i), data.get(i));
    }

    System.out.println(data);
    System.out.println(csvRecords);
    
    csvFileParser.close();
  }

  @Test
  public void testReadBankSpecificDataLongString()
      throws IOException, BankImporterException, URISyntaxException {
    Path path = Paths.get(this.getClass().getResource(this.getValidCSVLongStringPath()).toURI());
    MultipartFile csvFile = new MockMultipartFile(FILENAME, ORIG_FILENAME, CONTENT_TYPE_CSV,
        Files.readAllBytes(path));

    List<BankImportData> data = this.getSut().readData(csvFile, this.getCashbookAccount(), this.cashbookCategory);

    Reader in = new InputStreamReader(csvFile.getInputStream(), this.getCharset());

    CSVParser csvFileParser = new CSVParser(in, this.getSut().getFormat());

    List<CSVRecord> csvRecords = csvFileParser.getRecords();

    for (int i = 0; i < data.size(); i++) {
      this.assertEqualsCSVData(csvRecords.get(i), data.get(i));
    }

    csvFileParser.close();
  }

  protected CashbookAccount getCashbookAccount() {
    return this.cashbookAccount;
  }

  protected abstract BankImporterType getBankImporterType();

  protected abstract String getValidCSVPath();

  protected abstract String getValidCSVLongStringPath();
  
  protected abstract String getCharset();

  protected abstract BankImporterCSV getSut();

  protected abstract BankImportDataRepository getBankImportDataRepositoryMock();

  protected abstract byte[] getExpectedEntryReference(CSVRecord record);

  protected abstract void assertEqualsCSVData(CSVRecord record, BankImportData data);
}
