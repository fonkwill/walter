package at.simianarmy.imports.accounting.csv;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.imports.accounting.BankImporterException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.commons.csv.CSVRecord;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

public abstract class BankImporterCSVWithHeaderTest extends BankImporterCSVTest {

  protected void assertEqualsCSVData(CSVRecord record, BankImportData data) {
    assertEquals(LocalDate.parse(record.get(this.getSut().getEntryDateName()),
      DateTimeFormatter.ofPattern("dd.MM.yyyy")), data.getEntryDate());
    assertEquals(new BigDecimal(record.get(this.getSut().getEntryValueName()).replace(".", "").replace(',', '.')),
      data.getEntryValue());
    assertEquals(record.get(this.getSut().getPartnerNameName()), data.getPartnerName());
    assertEquals(new String(this.getExpectedEntryReference(record)), new String(data.getEntryReference()));

    String entryText = record.get(this.getSut().getEntryTextName());

    if (entryText.length() > 255) {
      entryText = entryText.substring(0, 252);
      entryText = entryText + "...";
    }

    assertEquals(entryText, data.getEntryText());
    assertEquals(this.getCashbookAccount(), data.getCashbookAccount());
    assertEquals(this.cashbookCategory, data.getCashbookCategory());
  }

  @Test(expected = BankImporterException.class)
  public void testReadBankSpecificDataShouldThrowException()
    throws IOException, BankImporterException, URISyntaxException {

    Path path = Paths.get(this.getClass().getResource(this.getInvalidHeadersCSVPath()).toURI());

    MultipartFile csvFile = new MockMultipartFile(FILENAME, ORIG_FILENAME, CONTENT_TYPE_CSV,
      Files.readAllBytes(path));

    this.getSut().readData(csvFile, this.getCashbookAccount(), this.cashbookCategory);
  }

  protected abstract String getInvalidHeadersCSVPath();

  protected abstract BankImporterCSVWithHeader getSut();
}
