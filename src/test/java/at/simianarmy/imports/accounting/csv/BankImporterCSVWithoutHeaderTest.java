package at.simianarmy.imports.accounting.csv;

import static org.junit.Assert.assertEquals;

import at.simianarmy.domain.BankImportData;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.apache.commons.csv.CSVRecord;

public abstract class BankImporterCSVWithoutHeaderTest extends BankImporterCSVTest {

  protected void assertEqualsCSVData(CSVRecord record, BankImportData data) {

    String date = record.get(this.getSut().getEntryDatePosition());

    if (date.length() > 10) {
      date = record.get(this.getSut().getEntryDatePosition()).substring(1);
    }

    assertEquals(LocalDate.parse(date,
      DateTimeFormatter.ofPattern("dd.MM.yyyy")), data.getEntryDate());
    assertEquals(new BigDecimal(record.get(this.getSut().getEntryValuePosition()).replace(".", "").replace(',', '.')),
      data.getEntryValue());
    this.assertEqualsPartnerName(record, data);
    assertEquals(new String(this.getExpectedEntryReference(record)), new String(data.getEntryReference()));

    String entryText = record.get(this.getSut().getEntryTextPosition());

    if (entryText.length() > 255) {
      entryText = entryText.substring(0, 252);
      entryText = entryText + "...";
    }

    assertEquals(entryText, data.getEntryText());
    assertEquals(this.getCashbookAccount(), data.getCashbookAccount());
    assertEquals(this.cashbookCategory, data.getCashbookCategory());
  }

  protected void assertEqualsPartnerName(CSVRecord record, BankImportData data) {
    assertEquals(record.get(this.getSut().getPartnerNamePosition()), data.getPartnerName());
  }

  protected abstract BankImporterCSVWithoutHeader getSut();

}
