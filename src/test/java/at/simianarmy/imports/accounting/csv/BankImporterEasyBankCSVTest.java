package at.simianarmy.imports.accounting.csv;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.BankImportDataRepository;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CustomizationService;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import org.apache.commons.csv.CSVRecord;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class BankImporterEasyBankCSVTest extends BankImporterCSVWithoutHeaderTest {

  public static final String VALID_CSV_URL = "/csv/easybank_csv_test.csv";
  public static final String VALID_CSV_LONG_STRING_URL = "/csv/easybank_csv_test_long_string.csv";
  public static final String CHARSET = "UTF-8";
  public static final BankImporterType BANK_IMPORTER_TYPE = BankImporterType.EASYBANK_IMPORTER;

  @Mock
  private BankImportDataRepository bankImportDataRepository;

  @Mock
  private CashbookAccountRepository cashbookAccountReposiory;

  @Mock
  private CustomizationService customizationService;

  @InjectMocks
  private BankImporterEasyBankCSV sut;

  @Before
  public void init() {
    super.init();

    Mockito
      .when(this.bankImportDataRepository.findByCashbookAccountAndReference(
        Mockito.eq(this.getCashbookAccount().getId()), Mockito.any(byte[].class)))
      .thenReturn(new ArrayList<BankImportData>());
    Mockito.when(this.bankImportDataRepository.save(Mockito.any(BankImportData.class)))
      .thenAnswer(new Answer<BankImportData>() {
        public BankImportData answer(InvocationOnMock invocation) {
          return invocation.getArgument(0);
        }
      });
  }

  @Override
  protected BankImporterType getBankImporterType() {
    return BANK_IMPORTER_TYPE;
  }

  @Override
  protected String getValidCSVPath() {
    return VALID_CSV_URL;
  }

  @Override
  protected String getValidCSVLongStringPath() {
    return VALID_CSV_LONG_STRING_URL;
  }

  @Override
  protected String getCharset() {
    return CHARSET;
  }

  @Override
  protected BankImporterCSVWithoutHeader getSut() {
    return this.sut;
  }

  @Override
  protected BankImportDataRepository getBankImportDataRepositoryMock() {
    return this.bankImportDataRepository;
  }

  @Override
  protected CashbookAccountRepository getCashbookAccountRepositoryMock() {
    return this.cashbookAccountReposiory;
  }

  @Override
  protected CustomizationService getCustomizationServiceMock() {
    return this.customizationService;
  }

  @Override
  protected byte[] getExpectedEntryReference(CSVRecord record) {
    String entryReference = record.get(BankImporterEasyBankCSV.FIELD_ENTRY_DATE) +
      record.get(BankImporterEasyBankCSV.FIELD_ENTRY_TEXT) +
      record.get(BankImporterEasyBankCSV.FIELD_ENTRY_VALUE);

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-512");
      md.update(entryReference.getBytes());
      byte[] digest = md.digest();

      return digest;
    } catch(NoSuchAlgorithmException e) {
      fail(e.getMessage());
    }

    return entryReference.getBytes();
  }

  protected void assertEqualsPartnerName(CSVRecord record, BankImportData data) {
    assertNull(data.getPartnerName());
  }

}
