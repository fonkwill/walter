package at.simianarmy.imports.accounting;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.imports.accounting.api.BankImporterAPI;
import at.simianarmy.imports.accounting.api.BankImporterGeorgeAPI;
import at.simianarmy.imports.accounting.csv.BankImporterBACACSV;
import at.simianarmy.imports.accounting.csv.BankImporterCSV;
import at.simianarmy.imports.accounting.csv.BankImporterEasyBankCSV;
import at.simianarmy.imports.accounting.csv.BankImporterGeorgeCSV;

import at.simianarmy.imports.accounting.csv.BankImporterRAIKACSV;
import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class BankImporterFactoryTest {

  @Inject
  private BankImporterFactory sut;

  @Test
  public void testCreateGeorgeCSVImporter() {
    BankImporterCSV importer = this.sut.createCSVImporter(this.newCashbookAccountMock(BankImporterType.GEORGE_IMPORTER));
    assertTrue(importer instanceof BankImporterGeorgeCSV);
    assertNotNull(importer);
  }

  @Test
  public void testCreateBACACSVImporter() {
    BankImporterCSV importer = this.sut.createCSVImporter(this.newCashbookAccountMock(BankImporterType.BACA_IMPORTER));
    assertTrue(importer instanceof BankImporterBACACSV);
    assertNotNull(importer);
  }

  @Test
  public void testCreateRAIKACSVImporter() {
    BankImporterCSV importer = this.sut.createCSVImporter(this.newCashbookAccountMock(BankImporterType.RAIKA_IMPORTER));
    assertTrue(importer instanceof BankImporterRAIKACSV);
    assertNotNull(importer);
  }

  @Test
  public void testCreateEasyBankCSVImporter() {
    BankImporterCSV importer = this.sut.createCSVImporter(this.newCashbookAccountMock(BankImporterType.EASYBANK_IMPORTER));
    assertTrue(importer instanceof BankImporterEasyBankCSV);
    assertNotNull(importer);
  }

  @Test
  public void testCreateGeorgeAPIImporter() {
    BankImporterAPI importer = this.sut.createAPIImporter(this.newCashbookAccountMock(BankImporterType.GEORGE_IMPORTER));
    assertTrue(importer instanceof BankImporterGeorgeAPI);
    assertNotNull(importer);
  }

  private CashbookAccount newCashbookAccountMock(BankImporterType bankImporterType) {
    CashbookAccount cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(cashbookAccount.getBankImporterType()).thenReturn(bankImporterType);
    return cashbookAccount;
  }
}
