package at.simianarmy.imports.accounting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.repository.BankImportDataRepository;

import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.service.CustomizationService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BankImporterGeneralTest {

  private static final String DEFAULT_REFERENCE = "123456";
  private static final String DEFAULT_TEXT_ABHEBUNG = "123 Abhebung 123";
  private static final String DEFAULT_TEXT_EINZAHLUNG = "456 Einzahlung 456";

  @Test
  public void testPersistData() {
    BankImportData data1 = Mockito.mock(BankImportData.class);
    BankImportData data2 = Mockito.mock(BankImportData.class);
    BankImportData data3 = Mockito.mock(BankImportData.class);
    Mockito.when(data1.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "1").getBytes());
    Mockito.when(data2.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "2").getBytes());
    Mockito.when(data3.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "3").getBytes());

    List<BankImportData> dataToPersist = new ArrayList<BankImportData>(
        Arrays.asList(data1, data2, data3));

    List<BankImportData> result = this.getSut().persistNotImportedBankImportData(dataToPersist,
        this.getCashbookAccount());

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data1);
    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data2);
    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data3);

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.never())
        .delete(Mockito.any(BankImportData.class));

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).flush();

    assertThat(result, Matchers.equalTo(dataToPersist));
  }

  @Test
  public void testPersistDataOneExisting() {
    BankImportData data1 = Mockito.mock(BankImportData.class);
    BankImportData data2 = Mockito.mock(BankImportData.class);
    BankImportData data3 = Mockito.mock(BankImportData.class);
    Mockito.when(data1.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "1").getBytes());
    Mockito.when(data2.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "2").getBytes());
    Mockito.when(data3.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "3").getBytes());

    Mockito
        .when(this.getBankImportDataRepositoryMock().findByCashbookAccountAndReference(
            this.getCashbookAccount().getId(), data1.getEntryReference()))
        .thenReturn(new ArrayList<BankImportData>());
    Mockito
        .when(this.getBankImportDataRepositoryMock().findByCashbookAccountAndReference(
          this.getCashbookAccount().getId(), data2.getEntryReference()))
        .thenReturn(new ArrayList<BankImportData>());
    Mockito
        .when(this.getBankImportDataRepositoryMock().findByCashbookAccountAndReference(
          this.getCashbookAccount().getId(), data3.getEntryReference()))
        .thenReturn(new ArrayList<BankImportData>(Arrays.asList(data3)));

    List<BankImportData> dataToPersist = new ArrayList<BankImportData>(
        Arrays.asList(data1, data2, data3));

    List<BankImportData> result = this.getSut().persistNotImportedBankImportData(dataToPersist,
        this.getCashbookAccount());

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data1);
    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data2);
    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.never()).save(data3);
    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).delete(data3);

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).flush();

    assertTrue(result.contains(data1));
    assertTrue(result.contains(data2));
    assertFalse(result.contains(data3));
  }

  @Test
  public void testPersistDataWithAutomaticTransferIntoAccount() {
    BankImportData data1 = Mockito.mock(BankImportData.class);
    Mockito.when(data1.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "1").getBytes());
    Mockito.when(data1.getEntryText()).thenReturn(DEFAULT_TEXT_EINZAHLUNG);
    Mockito.when(data1.getCashbookAccount()).thenReturn(this.getCashbookAccount());

    CashbookAccount cashbookAccount = this.newCashbookAccountMock("Einzahlung", null);
    CashbookCategory cashbookCategory = this.newCashbookCategoryMock(true);

    Mockito.when(this.getCashbookAccountRepositoryMock().findAllByTextForAutoTransferFromIsNotNullAndNotEmpty()).thenReturn(
      Arrays.asList(cashbookAccount)
    );

    Mockito
      .when(this.getBankImportDataRepositoryMock().findByCashbookAccountAndReference(
        this.getCashbookAccount().getId(), data1.getEntryReference()))
      .thenReturn(new ArrayList<BankImportData>());

    List<BankImportData> dataToPersist = new ArrayList<BankImportData>(
      Arrays.asList(data1));

    List<BankImportData> result = this.getSut().persistNotImportedBankImportData(dataToPersist,
      this.getCashbookAccount());

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data1);

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).flush();

    assertTrue(result.contains(data1));

    Mockito.verify(data1, Mockito.times(1)).setTransfer(true);
    Mockito.verify(data1, Mockito.times(1)).setCashbookCategory(cashbookCategory);
    Mockito.verify(data1, Mockito.times(1)).setCashbookAccountFrom(cashbookAccount);
    Mockito.verify(data1, Mockito.times(1)).setCashbookAccountTo(this.getCashbookAccount());
  }

  @Test
  public void testPersistDataWithAutomaticTransferFromAccount() {
    BankImportData data1 = Mockito.mock(BankImportData.class);
    Mockito.when(data1.getEntryReference()).thenReturn((DEFAULT_REFERENCE + "1").getBytes());
    Mockito.when(data1.getEntryText()).thenReturn(DEFAULT_TEXT_ABHEBUNG);
    Mockito.when(data1.getCashbookAccount()).thenReturn(this.getCashbookAccount());

    CashbookAccount cashbookAccount = this.newCashbookAccountMock(null, "Abhebung");
    CashbookCategory cashbookCategory = this.newCashbookCategoryMock(true);

    Mockito.when(this.getCashbookAccountRepositoryMock().findAllByTextForAutoTransferToIsNotNullAndNotEmpty()).thenReturn(
      Arrays.asList(cashbookAccount)
    );

    Mockito
      .when(this.getBankImportDataRepositoryMock().findByCashbookAccountAndReference(
        this.getCashbookAccount().getId(), data1.getEntryReference()))
      .thenReturn(new ArrayList<BankImportData>());

    List<BankImportData> dataToPersist = new ArrayList<BankImportData>(
      Arrays.asList(data1));

    List<BankImportData> result = this.getSut().persistNotImportedBankImportData(dataToPersist,
      this.getCashbookAccount());

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).save(data1);

    Mockito.verify(this.getBankImportDataRepositoryMock(), Mockito.times(1)).flush();

    assertTrue(result.contains(data1));

    Mockito.verify(data1, Mockito.times(1)).setTransfer(true);
    Mockito.verify(data1, Mockito.times(1)).setCashbookCategory(cashbookCategory);
    Mockito.verify(data1, Mockito.times(1)).setCashbookAccountTo(cashbookAccount);
    Mockito.verify(data1, Mockito.times(1)).setCashbookAccountFrom(this.getCashbookAccount());
  }

  private CashbookAccount newCashbookAccountMock(String textForAutoTransferFrom, String textForAutoTransferTo) {
    CashbookAccount cashbookAccount = Mockito.mock(CashbookAccount.class);
    Mockito.when(cashbookAccount.getTextForAutoTransferFrom()).thenReturn(textForAutoTransferFrom);
    Mockito.when(cashbookAccount.getTextForAutoTransferTo()).thenReturn(textForAutoTransferTo);

    return cashbookAccount;
  }

  private CashbookCategory newCashbookCategoryMock(Boolean isDefaultForTranser) {
    CashbookCategory cashbookCategory = Mockito.mock(CashbookCategory.class);
    if (isDefaultForTranser) {
      Mockito.when(this.getCustomizationServiceMock().getDefaultCashbookCategoryForTransfers()).thenReturn(cashbookCategory);
    }

    return cashbookCategory;
  }

  protected abstract BankImporterGeneral getSut();

  protected abstract CashbookAccount getCashbookAccount();

  protected abstract BankImportDataRepository getBankImportDataRepositoryMock();

  protected abstract CashbookAccountRepository getCashbookAccountRepositoryMock();

  protected abstract CustomizationService getCustomizationServiceMock();

}
