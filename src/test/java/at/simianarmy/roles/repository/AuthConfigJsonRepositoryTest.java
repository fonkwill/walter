package at.simianarmy.roles.repository;

import static org.junit.Assert.*;

import java.util.List;

import at.simianarmy.roles.domain.RequiredAuth;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import at.simianarmy.WalterApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AuthConfigJsonRepositoryTest {

	private static String user = "USER";
	private static String user2 = "USER2";
	private static String appointment1 = "APPOINTMENT1";
	private static String appointment2 = "APPOINTMENT2";
	private static String appointment3 = "APPOINTMENT3";
	private static String abc = "ABC";
	private static String def = "DEF";
	private static String ghi = "GHI";
	
	@Autowired
	private  AuthConfigRepository sut;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAllPossibleAuthorities() {
		List<String> auths = sut.getAllPossibleAuthorities();
		assertThat(auths, Matchers.hasSize(5));
		assertThat(auths, Matchers.containsInAnyOrder(user, user2, appointment1, appointment2, appointment3));
		
	}
	
	@Test
	public void testGetAuthorityForRequest1() {
	
		List<String> auths = sut.getAllAuthoritiesForRequest("/api/appointments/12");
		assertThat(auths, Matchers.hasSize(2));
		assertThat(auths,  Matchers.containsInAnyOrder(user, appointment1));
		
	}
	
	@Test
	public void testGetAuthorityForRequest2() {
	
		List<String> auths = sut.getAllAuthoritiesForRequest("/api/appointments/12/people");
		assertThat(auths, Matchers.hasSize(2));
		assertThat(auths,  Matchers.containsInAnyOrder(user, appointment2));
		
	}
	
	@Test
	public void testGetAuthorityForRequest3() {
	
		List<String> auths = sut.getAllAuthoritiesForRequest("/api/appointments/12/people/34");
		assertThat(auths, Matchers.hasSize(2));
		assertThat(auths,  Matchers.containsInAnyOrder(user2, appointment3));
		
	}
	
	@Test
	public void testGetElementsForAuthority() {
		List<String> elements = sut.getElementsForAuthority(user);
		assertThat(elements, Matchers.hasSize(3));
		assertThat(elements, Matchers.containsInAnyOrder(abc, def, ghi));
	}
	
	@Test
	public void testGetElementsForAuthority2() {
		List<String> elements = sut.getElementsForAuthority(appointment1);
		assertThat(elements, Matchers.hasSize(2));
		assertThat(elements, Matchers.containsInAnyOrder(abc, def));
	}
	@Test
	public void testGetElementsForAuthority3() {
		List<String> elements = sut.getElementsForAuthority(appointment2);
		assertThat(elements, Matchers.hasSize(1));
		assertThat(elements, Matchers.containsInAnyOrder(ghi));
	}

	@Test
  public void testGetRequiredAuths(){
	  List<RequiredAuth> reqAuths = sut.getRequiredAuths();
	  assertThat(reqAuths, Matchers.hasSize(1));
	  RequiredAuth a = reqAuths.get(0);
	  assertThat(a.getAuthority(), Matchers.equalTo("PERSON_AWARD"));
	  assertThat(a.getRequiredAuthorities(), Matchers.hasSize(2));
	  assertThat(a.getRequiredAuthorities(), Matchers.allOf(Matchers.hasItem("AWARD"), Matchers.hasItem("PERSON")));

  }


}
