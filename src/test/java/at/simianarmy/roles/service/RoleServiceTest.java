package at.simianarmy.roles.service;

import at.simianarmy.WalterApp;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.RequiredAuth;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.repository.AuthConfigRepository;
import at.simianarmy.roles.repository.RoleRepository;
import at.simianarmy.roles.service.dto.AuthorityDTO;
import at.simianarmy.roles.service.dto.RoleDTO;
import at.simianarmy.roles.service.mapper.RoleMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class RoleServiceTest {


  @Mock
  private AuthConfigRepository authConfigRepository;


  @Mock
  private RoleRepository roleRepository;

  @Inject
  @Spy
  RoleMapper roleMapper;

  @InjectMocks
  private RoleService sut;



  @Before
  public void setUp(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void saveRoleWithRequiredAuthorities_ShouldUpdateRole() {
    RequiredAuth requiredAuth = new RequiredAuth();
    requiredAuth.setAuthority("PERSON_AWARD");

    List<String> requiredAuths = new ArrayList<>();
    requiredAuths.add("AWARD");
    requiredAuths.add("PERSON");
    requiredAuth.setRequiredAuthorities(requiredAuths);

    List<RequiredAuth> returnRequiredAuths = new ArrayList<>();

    Mockito.when(authConfigRepository.getRequiredAuths()).thenReturn(returnRequiredAuths);

    Authority a = new Authority();
    a.setName("PERSON_AWARD");
    a.setWrite(true);

    Role role = new Role();
    role.setName("Role");

    Set<Authority> authorities = new HashSet<>();
    authorities.add(a);
    role.setAuthorities(authorities);
    returnRequiredAuths.add(requiredAuth);

    Mockito.when(roleRepository.save(Mockito.any())).then(i -> i.getArguments()[0]);


    RoleDTO roleDTO =  roleMapper.roleToRoleDTO(role);
    RoleDTO result = sut.save(roleDTO);

    assertThat(result.getAuthorities(), Matchers.hasSize(3) );

    AuthorityDTO personAward = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON_AWARD")).findFirst().orElse(null);
    AuthorityDTO person = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON")).findFirst().orElse(null);
    AuthorityDTO award = result.getAuthorities().stream().filter(au -> au.getName().equals("AWARD")).findFirst().orElse(null);

    assertThat(personAward.getWrite(), Matchers.equalTo(true));
    assertThat(award.getWrite(), Matchers.equalTo(false));
    assertThat(person.getWrite(), Matchers.equalTo(false));

  }

  @Test
  public void saveRoleWithRequiredAuthorities_ShouldNotUpdateRole() {
    RequiredAuth requiredAuth = new RequiredAuth();
    requiredAuth.setAuthority("PERSON_AWARD");

    List<String> requiredAuths = new ArrayList<>();
    requiredAuths.add("AWARD");
    requiredAuths.add("PERSON");
    requiredAuth.setRequiredAuthorities(requiredAuths);

    List<RequiredAuth> returnRequiredAuths = new ArrayList<>();
    returnRequiredAuths.add(requiredAuth);

    Mockito.when(authConfigRepository.getRequiredAuths()).thenReturn(returnRequiredAuths);

    Authority a = new Authority();
    a.setName("PERSON_AWARD");
    a.setWrite(false);

    Role role = new Role();
    role.setName("Role");

    Set<Authority> authorities = new HashSet<>();
    authorities.add(a);
    role.setAuthorities(authorities);

    Mockito.when(roleRepository.save(Mockito.any())).then(i -> i.getArguments()[0]);


    RoleDTO roleDTO =  roleMapper.roleToRoleDTO(role);
    RoleDTO result = sut.save(roleDTO);

    assertThat(result.getAuthorities(), Matchers.hasSize(1) );

    AuthorityDTO personAward = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON_AWARD")).findFirst().orElse(null);
    assertThat(personAward.getWrite(), Matchers.equalTo(false));

  }

  @Test
  public void saveRoleWithRequiredAuthoritiesMultipleAuthorities_ShouldUpdate() {
    RequiredAuth requiredAuth = new RequiredAuth();
    requiredAuth.setAuthority("PERSON_AWARD");

    List<String> requiredAuths = new ArrayList<>();
    requiredAuths.add("AWARD");
    requiredAuths.add("PERSON");
    requiredAuth.setRequiredAuthorities(requiredAuths);

    List<RequiredAuth> returnRequiredAuths = new ArrayList<>();
    returnRequiredAuths.add(requiredAuth);

    RequiredAuth requiredAuth2 = new RequiredAuth();
    requiredAuth2.setAuthority("PERSON_HONOR");

    List<String> requiredAuths2 = new ArrayList<>();
    requiredAuths2.add("HONOR");
    requiredAuths2.add("PERSON");
    requiredAuth2.setRequiredAuthorities(requiredAuths2);

    returnRequiredAuths.add(requiredAuth2);
    Mockito.when(authConfigRepository.getRequiredAuths()).thenReturn(returnRequiredAuths);

    Authority a = new Authority();
    a.setName("PERSON_AWARD");
    a.setWrite(true);
    Authority b = new Authority();
    b.setName("PERSON_HONOR");
    b.setWrite(true);

    Role role = new Role();
    role.setName("Role");

    Set<Authority> authorities = new HashSet<>();
    authorities.add(a);
    authorities.add(b);
    role.setAuthorities(authorities);

    Mockito.when(roleRepository.save(Mockito.any())).then(i -> i.getArguments()[0]);


    RoleDTO roleDTO =  roleMapper.roleToRoleDTO(role);
    RoleDTO result = sut.save(roleDTO);

    assertThat(result.getAuthorities(), Matchers.hasSize(5) );

    AuthorityDTO personAward = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON_AWARD")).findFirst().orElse(null);
    AuthorityDTO personHonor = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON_HONOR")).findFirst().orElse(null);
    AuthorityDTO person = result.getAuthorities().stream().filter(au -> au.getName().equals("PERSON")).findFirst().orElse(null);
    AuthorityDTO award = result.getAuthorities().stream().filter(au -> au.getName().equals("AWARD")).findFirst().orElse(null);
    AuthorityDTO honor = result.getAuthorities().stream().filter(au -> au.getName().equals("HONOR")).findFirst().orElse(null);


    assertThat(personAward.getWrite(), Matchers.equalTo(true));
    assertThat(personHonor.getWrite(), Matchers.equalTo(true));
    assertThat(award.getWrite(), Matchers.equalTo(false));
    assertThat(person.getWrite(), Matchers.equalTo(false));
    assertThat(honor.getWrite(), Matchers.equalTo(false));
  }
}
