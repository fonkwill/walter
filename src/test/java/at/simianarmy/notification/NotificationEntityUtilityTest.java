package at.simianarmy.notification;


import at.simianarmy.WalterApp;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.domain.Person;



import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class NotificationEntityUtilityTest {
  
  private NotificationRule rule1;
  private NotificationRule rule2;
  private NotificationRule rule3;

  @Inject
  private NotificationEntityUtility notificationEntityUtility;

  @Before
  public void setUp() throws Exception {    
    rule1 = new NotificationRule();
    rule1.setEntity("Person");
    rule1.setConditions("select person from Person person");
    
    rule2 = new NotificationRule();
    rule2.setEntity("XXPersonXX");

    rule3 = new NotificationRule();
    rule3.setEntity("NotificationRule");
  }

  @After
  public void tearDown() throws Exception {}

  @Test
  public void testGetFullyQualifiedClassNameFromRule() {
    String fqc = notificationEntityUtility.getFullyQualifiedClassNameFromRule(rule1);
    
    assertThat(fqc , equalTo("at.simianarmy.domain.Person") );
  }
  
  @Test
  public void testGetEntityClassFromRule() throws NotificationException {
    NotificationEntity entity = notificationEntityUtility.getEntityClassFromRule(rule1);
    
    assertThat(entity.getClass(), equalTo(Person.class) );
  }

  @Test(expected = NotificationException.class)
  public void testGetEntityClassFromRuleWithInvalidClassname() throws NotificationException {
    notificationEntityUtility.getEntityClassFromRule(rule2);
  }

  @Test(expected = NotificationException.class)
  public void testGetEntityClassFromRuleWithInvalidClass() throws NotificationException {
    notificationEntityUtility.getEntityClassFromRule(rule3);
  }
}
