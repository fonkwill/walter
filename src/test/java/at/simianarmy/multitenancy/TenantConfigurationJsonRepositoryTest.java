package at.simianarmy.multitenancy;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;

import at.simianarmy.WalterApp;
import at.simianarmy.multitenancy.domain.DBConfiguration;
import at.simianarmy.multitenancy.domain.TenantConfiguration;
import at.simianarmy.multitenancy.repository.TenantConfigurationJsonRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class TenantConfigurationJsonRepositoryTest {

	@Autowired
	private TenantConfigurationJsonRepository sut;

	@Value("classpath:config/tenantConfig.json")
	private Resource tenantConfigFile;

	@Value("classpath:config/tenantConfig_orig.json")
	private Resource tenantConfigOrig;

	@Before
	public void setUp() throws Exception {
		FileCopyUtils.copy(tenantConfigOrig.getFile(), tenantConfigFile.getFile());
		try {
			// give it some time
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sut.initialize();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testOneConfiguration() {
		TenantConfiguration tconf = sut.getTenantConfigurationForTenant("tenant1");
		assertThat(tconf.getTenantId(), equalTo("tenant1"));
		assertThat(tconf.getDbConfiguration().getUrl(), equalTo("jdbc:postgresql://localhost:5432/walter"));
		assertThat(tconf.getSchema(), equalTo("tenant1"));

	}

	@Test
	public void testAllConfigurations() {
		Map<String, TenantConfiguration> result = sut.getAllTenantConfigurations();
		assertThat(result.keySet(), containsInAnyOrder("tenant1", "tenant2"));

	}

	@Test
	public void testDeleteConfiguration() {
		sut.deleteForTenant("tenant1");
		try {
			// give it some time
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertThat(sut.getTenantConfigurationForTenant("tenant1"), equalTo(null));
	}

	@Test
	public void testSaveConfiguration() {
		TenantConfiguration conf = new TenantConfiguration();
		DBConfiguration dbconf = sut.getAllConnections().get(0);
		conf.setDbConfiguration(dbconf);
		conf.setSchema("tenant3");
		conf.setTenantId("tenant3");
		sut.save(conf);
		try {
			// give it some time
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sut.initialize();
		try {
			// give it some time
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		TenantConfiguration savedConf = sut.getTenantConfigurationForTenant("tenant3");
		assertThat(savedConf.getDbConfiguration(), notNullValue());
		assertThat(savedConf.getSchema(), equalTo(conf.getSchema()));
		assertThat(savedConf.getTenantId(), equalTo(conf.getTenantId()));
		assertThat(savedConf.getDbConfiguration().getUrl(), equalTo(conf.getDbConfiguration().getUrl()));
	}

	@Test
	public void testUpdateConfiguration() {
		TenantConfiguration conf = sut.getTenantConfigurationForTenant("tenant1");
		conf.setSchema("schemaupd");
		sut.save(conf);
		try {
			// give it some time
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sut.initialize();
		try {
			// give it some time
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		TenantConfiguration savedConf = sut.getTenantConfigurationForTenant("tenant1");
		assertThat(savedConf.getSchema(), equalTo("schemaupd"));
	}

}
