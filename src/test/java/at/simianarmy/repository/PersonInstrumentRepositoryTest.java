package at.simianarmy.repository;

import static org.junit.Assert.assertThat;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Instrument;
import at.simianarmy.domain.InstrumentType;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonInstrument;
import at.simianarmy.service.specification.PersonSpecifications;

import java.time.LocalDate;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonInstrumentRepositoryTest {


  private static final String firstName = "hans";
  private static final String lastName = "huber";
  private static final String address = "abc1";
  private static final String plz = "1010";
  private static final String city = "Wien";
  private static final String country = "Österreich";


  // pageable like the rest interface creates
  private static final Pageable page = PageRequest.of(0, 20, Sort.Direction.ASC, "id");

  @Inject
  private PersonInstrumentRepository personInstrumentRepository;

  @Inject
  private PersonRepository personRepository;

  @Inject
  private InstrumentRepository instrumentRepository;

  @Inject
  private InstrumentTypeRepository instrumentTypeRepository;

  private PersonInstrument pi1;

  private Person p1;


  @Before
  public void setUp() throws Exception {
    p1 = new Person();
    p1.setFirstName(firstName);
    p1.setLastName(lastName);
    p1.setStreetAddress(address);
    p1.setPostalCode(plz);
    p1.setCity(city);
    p1.setCountry(country);
    p1.setHasDirectDebit(false);

    p1 = personRepository.save(p1);

    InstrumentType it1 = new InstrumentType();
    it1.setName("test1");

    it1 = instrumentTypeRepository.save(it1);

    Instrument instrument = new Instrument();
    instrument.setType(it1);
    instrument.setName("abc");
    instrument.setPurchaseDate(LocalDate.of(2016, 10, 1));

    instrument = instrumentRepository.save(instrument);

    pi1 = new PersonInstrument();
    pi1.setPerson(p1);
    pi1.setInstrument(instrument);
    pi1.setBeginDate(LocalDate.of(2016, 10, 2));

    pi1 = personInstrumentRepository.save(pi1);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    Long personId = p1.getId();
    Specification<PersonInstrument> searchSpec = Specification
        .where(PersonSpecifications.hasPersonIdFromPersonInstrument(personId));
    Page<PersonInstrument> page = personInstrumentRepository.findAll(
        searchSpec, PersonInstrumentRepositoryTest.page);

    assertThat(page.getContent(), Matchers.contains(pi1));
  }

}
