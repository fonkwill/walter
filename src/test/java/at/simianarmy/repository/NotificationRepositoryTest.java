package at.simianarmy.repository;

import static org.assertj.core.api.Assertions.assertThat;
import at.simianarmy.WalterApp;
import java.util.List;

import javax.inject.Inject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import at.simianarmy.domain.Notification;
import at.simianarmy.domain.NotificationRule;
import at.simianarmy.domain.User;
import at.simianarmy.roles.domain.Authority;
import at.simianarmy.roles.domain.Role;
import at.simianarmy.roles.repository.RoleRepository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import org.junit.After;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class NotificationRepositoryTest {
  
  private NotificationRule rule1;
  private NotificationRule rule2;
  
  private Role authority1;
  private Role authority2;
  
  private static final Long ENTITY_ID_1 = new Long(1);
  private static final Long ENTITY_ID_2 = new Long(2);
  private static final Long ENTITY_ID_3 = new Long(3);
  
  private Notification notification1;
  private Notification notification2;
  private Notification notification3;
  
  private static final String AUTHORITY_1 = "ROLE_ADMIN2";
  private static final String AUTHORITY_2 = "ROLE_USER2";
  
  private static final Long USER_ID = new Long(1);
  
  @Inject
  private NotificationRuleRepository notificationRuleRepository;
  
  @Inject
  private NotificationRepository notificationRepository;
  
  @Inject
  private RoleRepository roleRepository;

//  @MockBean
//  private UserRepository userRepository;

  @Before
  public void setUp() {
//    MockitoAnnotations.initMocks(this);
    
    User user = new User();
    user.setId(USER_ID);
    user.setFirstName("john");
    user.setLastName("doe");
//    when(userRepository.findOne(USER_ID)).thenReturn(user);
    
    authority1 = new Role();
    authority1.setName(AUTHORITY_1);
    roleRepository.saveAndFlush(authority1);
    
    
    authority2 = new Role();
    authority2.setName(AUTHORITY_2);
    roleRepository.saveAndFlush(authority2);
    
    rule1 = new NotificationRule();
    rule1.setTitle("Test Rule");
    rule1.setEntity("Person");
    rule1.setConditions("select person from Person person");
    rule1.setActive(true);
    rule1.setMessageGroup("message for group");
    rule1.setMessageSingle("mesage for single");
    rule1.addTargetAudiences(authority1);
    
    rule1 = notificationRuleRepository.saveAndFlush(rule1);
    
    rule2 = new NotificationRule();
    rule2.setTitle("Test Rule");
    rule2.setEntity("Person");
    rule2.setConditions("select person from Person person");
    rule2.setActive(true);
    rule2.setMessageGroup("message for group");
    rule2.setMessageSingle("mesage for single");
    rule2.addTargetAudiences(authority1).addTargetAudiences(authority2);
    
    rule2 = notificationRuleRepository.saveAndFlush(rule2);
    
    notification1 = new Notification();
    notification1.setEntityId(ENTITY_ID_1);
    notification1.setNotificationRule(rule1);
    notification1.setCreatedAt(ZonedDateTime.now());
    
    notification1 = notificationRepository.saveAndFlush(notification1);
    
    notification2 = new Notification();
    notification2.setEntityId(ENTITY_ID_2);
    notification2.setNotificationRule(rule1);
    notification2.setCreatedAt(ZonedDateTime.now());
    
    notification2 = notificationRepository.saveAndFlush(notification2);
    
    notification3 = new Notification();
    notification3.setEntityId(ENTITY_ID_3);
    notification3.setNotificationRule(rule2);
    notification3.setCreatedAt(ZonedDateTime.now());
    notification3.readByUser(user);
    
    notification3 = notificationRepository.saveAndFlush(notification3);
  }
  
  @After
  public void tearDown() {
    notificationRepository.delete(notification1);
    notificationRepository.delete(notification2);
    notificationRepository.delete(notification3);
    
    notificationRuleRepository.delete(rule1);
    notificationRuleRepository.delete(rule2);
    
    roleRepository.delete(authority1);
    roleRepository.delete(authority2);
  }
  
  @Test
  public void testFindByNotificationRule() {
    List<Notification> notifications = notificationRepository.findByNotificationRule(rule1);
    
    assertThat(notifications.size()).isEqualTo(2);
    assertThat(notifications).contains(notification1);
    assertThat(notifications).contains(notification2);
  }
  
  @Test
  public void testFindByNotificationRuleAndEntityId() {
    List<Notification> notifications = notificationRepository.findByNotificationRuleAndEntityId(rule1, notification1.getEntityId());
    
    assertThat(notifications.size()).isEqualTo(1);
    assertThat(notifications).contains(notification1);
  }
  
  @Test
  public void testFindAllForAuthorities() {
    List<String> authorities = new ArrayList<String>();
    authorities.add(AUTHORITY_2);
    
    List<Notification> notifications = notificationRepository.findAllForAuthorities(authorities);
    
    assertThat(notifications.size()).isEqualTo(1);
    assertThat(notifications).contains(notification3);
  }
  
  @Test
  public void testCountUnread() {
    Long count = notificationRepository.countUnread();
    
    assertThat(count).isEqualTo(2);
  }
  
  @Test
  public void testCountUnreadForAuthorities() {
    List<String> authorities = new ArrayList<String>();
    authorities.add(AUTHORITY_2);
    
    Long count = notificationRepository.countUnreadForAuthorities(authorities);
    
    assertThat(count).isEqualTo(0);
  }
  
  @Test
  public void testCountTotal() {
    Long count = notificationRepository.countTotal();
    
    assertThat(count).isEqualTo(3);
  }
  
  @Test
  public void testCountTotalForAuthorities() {
    List<String> authorities = new ArrayList<String>();
    authorities.add(AUTHORITY_2);
    
    Long count = notificationRepository.countTotalForAuthorities(authorities);
    
    assertThat(count).isEqualTo(1);
  }

}
