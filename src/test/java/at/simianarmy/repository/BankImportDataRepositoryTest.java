package at.simianarmy.repository;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookAccount;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;

import at.simianarmy.service.CashbookAccountService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class BankImportDataRepositoryTest {

  private static final BankImporterType DEFAULT_TYPE = BankImporterType.GEORGE_IMPORTER;
  private static final String DEFAULT_REFERENCE = "12345AT";
  private static final LocalDate DEFAULT_DATE = LocalDate.now();
  private static final BigDecimal DEFAULT_VALUE = new BigDecimal("2.22");
  private static final CashbookEntryType DEFAULT_ENTRY_TYPE = CashbookEntryType.INCOME;
  private static final String DEFAULT_TITLE = "TITLE";
  private static final String CATEGORY_NAME = "CAT";
  private static final String CATEGORY_COLOR = "#efefef";
  private static final String DEFAULT_CASHBOOK_ACCOUNT_NAME = "BA_ACCOUNT";
  private static final String DEFAULT_RECEIPT_CODE = "BA";

  @Inject
  private BankImportDataRepository sut;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  private BankImportData data1;
  private BankImportData data2;
  private BankImportData data3;
  private CashbookEntry entry;
  private CashbookCategory cashbookCategory;
  private CashbookAccount cashbookAccount;

  @Before
  public void init() {

    this.cashbookCategory = new CashbookCategory();
    this.cashbookCategory.setName(CATEGORY_NAME);
    this.cashbookCategory.setColor(CATEGORY_COLOR);
    this.cashbookCategoryRepository.save(this.cashbookCategory);

    this.cashbookAccount = new CashbookAccount();
    this.cashbookAccount.setName(DEFAULT_CASHBOOK_ACCOUNT_NAME);
    this.cashbookAccount.setReceiptCode(DEFAULT_RECEIPT_CODE);
    this.cashbookAccount.setBankAccount(true);
    this.cashbookAccount.setBankImporterType(DEFAULT_TYPE);
    this.cashbookAccount.setCurrentNumber(CashbookAccountService.START_CURRENT_NUMBER);
    this.cashbookAccount.setDefaultForCashbook(false);
    this.cashbookAccountRepository.save(this.cashbookAccount);

    this.data1 = new BankImportData();
    this.data1.setCashbookAccount(this.cashbookAccount);
    this.data1.setEntryReference((DEFAULT_REFERENCE + "1").getBytes());
    this.data1.setEntryDate(DEFAULT_DATE);
    this.data1.setEntryValue(DEFAULT_VALUE);
    this.data1.setEntryText(DEFAULT_TITLE);
    this.data1.setCashbookCategory(this.cashbookCategory);

    this.data2 = new BankImportData();
    this.data2.setCashbookAccount(this.cashbookAccount);
    this.data2.setEntryReference((DEFAULT_REFERENCE + "2").getBytes());
    this.data2.setEntryDate(DEFAULT_DATE);
    this.data2.setEntryValue(DEFAULT_VALUE);
    this.data2.setEntryText(DEFAULT_TITLE);
    this.data2.setCashbookCategory(this.cashbookCategory);

    this.data3 = new BankImportData();
    this.data3.setCashbookAccount(this.cashbookAccount);
    this.data3.setEntryReference((DEFAULT_REFERENCE + "3").getBytes());
    this.data3.setEntryDate(DEFAULT_DATE);
    this.data3.setEntryValue(DEFAULT_VALUE);
    this.data3.setEntryText(DEFAULT_TITLE);
    this.data3.setCashbookCategory(this.cashbookCategory);

    this.entry = new CashbookEntry();
    this.entry.setCashbookAccount(this.cashbookAccount);
    this.entry.setDate(this.data3.getEntryDate());
    this.entry.setAmount(this.data3.getEntryValue());
    this.entry.setTitle(this.data3.getEntryText());
    this.entry.setType(DEFAULT_ENTRY_TYPE);
    this.entry.setCashbookCategory(this.cashbookCategory);
    this.entry = this.cashbookEntryRepository.saveAndFlush(this.entry);

    this.data3.setCashbookEntry(this.entry);

    this.entry = this.cashbookEntryRepository.saveAndFlush(this.entry);
    
    this.data1 = this.sut.saveAndFlush(this.data1);
    this.data2 = this.sut.saveAndFlush(this.data2);
    this.data3 = this.sut.saveAndFlush(this.data3);

  }

  @After
  public void tearDown() {
    this.sut.delete(this.data1);
    this.sut.delete(this.data2);
    this.sut.delete(this.data3);
    this.cashbookEntryRepository.delete(this.entry);
    this.cashbookCategoryRepository.delete(this.cashbookCategory);
    this.cashbookAccountRepository.delete(this.cashbookAccount);
  }

  @Test
  public void testFindAllNotImported() {
    List<BankImportData> result = this.sut.findAllNotImported();

    assertThat(result, containsInAnyOrder(this.data1, this.data2) );
    assertThat(result, not(containsInAnyOrder(this.data3)));
  }

  @Test
  public void testFindAllNotImported2() {

    this.data3.setCashbookEntry(null);
    this.sut.saveAndFlush(this.data3);

    List<BankImportData> result = this.sut.findAllNotImported();

    assertThat(result, containsInAnyOrder(this.data1, this.data2, this.data3));
  }

  @Test
  public void testFindByBankImporterTypeAndReference() {
    List<BankImportData> result = this.sut.findByCashbookAccountAndReference(this.cashbookAccount.getId(),
      (DEFAULT_REFERENCE + "1").getBytes());
    assertThat(result, contains(this.data1));
    assertThat(result, hasSize(1));
  }
  
  @Test
  public void testFindByBankImporterTypeAndReferenceNoneFound() {
    List<BankImportData> result = this.sut.findByCashbookAccountAndReference(this.cashbookAccount.getId(),
      (DEFAULT_REFERENCE + "X").getBytes());
    assertThat(result, hasSize(0));
  }
}
