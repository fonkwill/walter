package at.simianarmy.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MembershipFeeForPeriod;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeForPeriodRepositoryTest {

  private static final Integer DEFAULT_NR = 1;
  private static final Integer DEFAULT_YEAR = 2017;
  private static final String DEFAULT_REFERENCECODE = "00001012017";
  private static final String DEFAULT_REFERENCECODE2 = "00002012017";
  
  private MembershipFeeForPeriod m1;
  
  @Inject
  private MembershipFeeForPeriodRepository sut;
  
  @Before
  public void init() {    
    this.m1 = new MembershipFeeForPeriod();
    this.m1.setNr(DEFAULT_NR);
    this.m1.setYear(DEFAULT_YEAR);
    this.m1.setReferenceCode(DEFAULT_REFERENCECODE);
    
    this.sut.saveAndFlush(this.m1);
  }
  
  @After
  public void tearDown() {
    this.sut.deleteAll();
  }
  
  @Test
  public void testFindByReferenceCode() {
    MembershipFeeForPeriod result = this.sut.findByReferenceCode(DEFAULT_REFERENCECODE);
    assertEquals(this.m1, result);
  }
  
  @Test
  public void testFindByReferenceCodeNotFound() {
    MembershipFeeForPeriod result = this.sut.findByReferenceCode(DEFAULT_REFERENCECODE2);
    assertNull(result);
  }
  
}
