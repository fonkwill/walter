package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.MembershipFeeForPeriod;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonRepositoryTest {

  @Inject
  private PersonRepository sut;

  @Inject
  private MembershipRepository membershipRepository;

  @Inject
  private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;

  @Inject
  private EmailVerificationLinkRepository emailVerificationLinkRepository;

  @Inject
  private PersonGroupRepository personGroupRepository;

  private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.of(1960, 1, 1);
  private static final String DEFAULT_FIRSTNAME = "Vorname";
  private static final String DEFAULT_LASTNAME = "Nachname";
  private static final String DEFAULT_STREET = "Straße";
  private static final String DEFAULT_PLZ = "12345";
  private static final String DEFAULT_CITY = "Ort";
  private static final String DEFAULT_COUNTRY = "AT";

  private static final String DEFAULT_REF_CODE = "ref_12345";


  private Person person1;
  private Person person2;
  private Person person3;
  private Person person4;

  private PersonGroup pg1;
  private PersonGroup pg2;

  private EmailVerificationLink link1;
  private EmailVerificationLink link2;
  private EmailVerificationLink link3;

  private void initFeeForPeriods(Person person, Boolean paid) {
    List<Membership> memberships = membershipRepository
        .findByPerson(person.getId(), null).getContent();
    for (Membership membership : memberships) {
      int beginYear = membership.getBeginDate().getYear();
      int endYear = membership.getEndDate() == null
          ? LocalDate.now().getYear()
          : membership.getEndDate().getYear();
      for (int year = beginYear; year <= endYear; year++) {
        MembershipFeeForPeriod period = new MembershipFeeForPeriod();
        period.setMembership(membership);
        period.setReferenceCode(DEFAULT_REF_CODE);
        period.setYear(year);
        period.setPaid(paid);
        period.setNr(1);
        membershipFeeForPeriodRepository.saveAndFlush(period);
      }
    }
  }

  @Transactional
  @Before
  public void setUp() throws Exception {
    pg1 = new PersonGroup();
    pg1.setName("GROUP1");
    this.personGroupRepository.saveAndFlush(pg1);

    pg2 = new PersonGroup();
    pg2.setName("GROUP2");
    this.personGroupRepository.saveAndFlush(pg2);

    person1 = new Person();
    person1.setFirstName(DEFAULT_FIRSTNAME + "1");
    person1.setLastName(DEFAULT_LASTNAME + "1");
    person1.setBirthDate(DEFAULT_BIRTHDATE);
    person1.setStreetAddress(DEFAULT_STREET + " 1");
    person1.setPostalCode(DEFAULT_PLZ);
    person1.setCity(DEFAULT_CITY);
    person1.setCountry(DEFAULT_COUNTRY);
    person1.setGender(Gender.FEMALE);
    person1.setHasDirectDebit(false);
    person1.addPersonGroups(pg1);
    person1.setEmailType(EmailType.VERIFIED);
    person1.setEmail(null);

    person1 = sut.saveAndFlush(person1);


    Membership membership11 = new Membership();
    membership11.setPerson(person1);
    membership11.setBeginDate(LocalDate.of(2010, 1, 1));
    membership11.setEndDate(LocalDate.of(2012, 12, 31));
    membershipRepository.saveAndFlush(membership11);

    Membership membership12 = new Membership();
    membership12.setPerson(person1);
    membership12.setBeginDate(LocalDate.of(2016, 1, 1));
    membershipRepository.saveAndFlush(membership12);

    initFeeForPeriods(person1, null);

    person2 = new Person();
    person2.setFirstName(DEFAULT_FIRSTNAME + "2");
    person2.setLastName(DEFAULT_LASTNAME + "2");
    person2.setBirthDate(DEFAULT_BIRTHDATE);
    person2.setStreetAddress(DEFAULT_STREET + " 2");
    person2.setPostalCode(DEFAULT_PLZ);
    person2.setCity(DEFAULT_CITY);
    person2.setCountry(DEFAULT_COUNTRY);
    person2.setGender(Gender.MALE);
    person2.setHasDirectDebit(false);
    person2.addPersonGroups(pg1);
    person2.addPersonGroups(pg2);
    person2.setEmailType(EmailType.VERIFIED);
    person2.setEmail("person2@walter.at");

    person2 = sut.saveAndFlush(person2);

    Membership membership2 = new Membership();
    membership2.setPerson(person2);
    membership2.setBeginDate(LocalDate.of(2013, 1, 1));
    membership2.setEndDate(LocalDate.of(2016, 12, 31));

    membershipRepository.saveAndFlush(membership2);

    initFeeForPeriods(person2, false);

    person3 = new Person();
    person3.setFirstName(DEFAULT_FIRSTNAME + "3");
    person3.setLastName(DEFAULT_LASTNAME + "3");
    person3.setBirthDate(DEFAULT_BIRTHDATE);
    person3.setStreetAddress(DEFAULT_STREET + " 3");
    person3.setPostalCode(DEFAULT_PLZ);
    person3.setCity(DEFAULT_CITY);
    person3.setCountry(DEFAULT_COUNTRY);
    person3.setGender(Gender.MALE);
    person3.setHasDirectDebit(false);
    person3.setEmailType(EmailType.MANUALLY);
    person3.setEmail("person3@walter.at");
    person3.addPersonGroups(pg1);

    person3 = sut.saveAndFlush(person3);

    Membership membership3 = new Membership();
    membership3.setPerson(person3);
    membership3.setBeginDate(LocalDate.of(2017, 1, 1));

    membershipRepository.saveAndFlush(membership3);

    initFeeForPeriods(person3, false);

    person4 = new Person();
    person4.setFirstName(DEFAULT_FIRSTNAME + "4");
    person4.setLastName(DEFAULT_LASTNAME + "4");
    person4.setBirthDate(DEFAULT_BIRTHDATE);
    person4.setStreetAddress(DEFAULT_STREET + " 4");
    person4.setPostalCode(DEFAULT_PLZ);
    person4.setCity(DEFAULT_CITY);
    person4.setCountry(DEFAULT_COUNTRY);
    person4.setGender(Gender.MALE);
    person4.setHasDirectDebit(false);
    person4.setEmail(null);
    person4.setEmailType(EmailType.MANUALLY);

    person4 = sut.saveAndFlush(person4);

    Membership membership4 = new Membership();
    membership4.setPerson(person4);
    membership4.setBeginDate(LocalDate.of(1960, 1, 1));

    membershipRepository.saveAndFlush(membership4);

    initFeeForPeriods(person4, true);

    link1 = new EmailVerificationLink();
    link1.setPerson(person1);
    link1.setValidationDate(null);
    link1.setInvalid(false);
    link1.setSecretKey("ABC");
    link1.setCreationDate(ZonedDateTime.now().minusDays(1));
    this.emailVerificationLinkRepository.saveAndFlush(link1);

    link2 = new EmailVerificationLink();
    link2.setPerson(person2);
    link2.setValidationDate(null);
    link2.setInvalid(true);
    link2.setSecretKey("ABC");
    link2.setCreationDate(ZonedDateTime.now().minusDays(14));
    this.emailVerificationLinkRepository.saveAndFlush(link2);

    link3 = new EmailVerificationLink();
    link3.setPerson(person2);
    link3.setValidationDate(ZonedDateTime.now().minusHours(3));
    link3.setInvalid(false);
    link3.setSecretKey("ABC");
    link3.setCreationDate(ZonedDateTime.now().minusDays(1));
    this.emailVerificationLinkRepository.saveAndFlush(link3);
  }

  @After
  public void tearDown() throws Exception {
    membershipFeeForPeriodRepository.deleteAll();
    membershipRepository.deleteAll();
    this.emailVerificationLinkRepository.deleteAll();
    sut.deleteAll();
    this.personGroupRepository.deleteAll();
  }

  @Test
  public void findAllWithUnpaidFeesCurrentOnly() throws Exception {
    List<Person> personList = sut.findAllWithUnpaidFeesAndNoDirectDebit(2017, 2017);
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person3));
  }

  @Test
  public void findAllWithUnpaidFeesOldAndCurrent() throws Exception {
    List<Person> personList = sut.findAllWithUnpaidFeesAndNoDirectDebit(1900, 2017);
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person3));
  }

  @Test
  public void findAllWithUnpaidFeesOldAndCurrentAndDirectDebit() throws Exception {
    person1.setHasDirectDebit(true);
    this.sut.saveAndFlush(person1);
    List<Person> personList = sut.findAllWithUnpaidFeesAndNoDirectDebit(1900, 2017);
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person3));
  }

  @Test
  public void findAllWithUnpaidFeesOldOnly() throws Exception {
    List<Person> personList = sut.findAllWithUnpaidFeesAndNoDirectDebit(1900, 2016);
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1));
  }

  @Test
  public void findAllWithUnpaidFeesOldOnlyAndDirectDebit() throws Exception {
    person1.setHasDirectDebit(true);
    this.sut.saveAndFlush(person1);
    List<Person> personList = sut.findAllWithUnpaidFeesAndNoDirectDebit(1900, 2016);
    Assert.assertThat(personList.size(), Matchers.equalTo(0));
  }

  @Test
  public void findAllWithCurrentFees() throws Exception {
    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = sut.findAllWithCurrentFees(pageable).getContent();
    Assert.assertThat(personList.size(), Matchers.equalTo(3));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person3, person4));
  }

  @Test
  public void findAllWithEmailVerificationLinksToVerify() throws Exception {
    List<Person> personList = this.sut.findAllByPersonGroupsAndEmailVerificationLinksToVerify(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1));
  }

  @Test
  public void findAllWithEmailVerificationLinksToVerifyPg1() throws Exception {
    List<Person> personList = this.sut.findAllByPersonGroupsAndEmailVerificationLinksToVerify(new ArrayList<Long>(
      Arrays.asList(this.pg1.getId())));
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1));
  }

  @Test
  public void findAllWithEmailVerificationLinksToVerifyPg2() throws Exception {
    List<Person> personList = this.sut.findAllByPersonGroupsAndEmailVerificationLinksToVerify(new ArrayList<Long>(
      Arrays.asList(this.pg2.getId())));
    Assert.assertThat(personList.size(), Matchers.equalTo(0));
  }

  @Test
  public void findAllWithEmailVerificationLinksToVerifyPg1Pg2() throws Exception {
    List<Person> personList = this.sut.findAllByPersonGroupsAndEmailVerificationLinksToVerify(new ArrayList<Long>(
      Arrays.asList(this.pg1.getId(), this.pg2.getId())));
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups1() throws Exception {
    //returns person2 (validated link)
    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person2));
  }

  @Test
  public void findAllByInActiveEmailAndPersonGroups1() throws Exception {
    //returns person1, person3 and person4
    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(3));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person3, person4));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups2() throws Exception {
    //returns none

    EmailVerificationLink linkP2 = new EmailVerificationLink();
    linkP2.setInvalid(false);
    linkP2.setValidationDate(null);
    linkP2.setCreationDate(ZonedDateTime.now());
    linkP2.setPerson(person2);
    linkP2.setSecretKey("SECRET");
    this.emailVerificationLinkRepository.saveAndFlush(linkP2);

    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(0));
  }

  @Test
  public void findAllByInActiveEmailAndPersonGroups2() throws Exception {
    //returns person1, person2, person 3 and person4

    EmailVerificationLink linkP2 = new EmailVerificationLink();
    linkP2.setInvalid(false);
    linkP2.setValidationDate(null);
    linkP2.setCreationDate(ZonedDateTime.now());
    linkP2.setPerson(person2);
    linkP2.setSecretKey("SECRET");
    this.emailVerificationLinkRepository.saveAndFlush(linkP2);

    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(4));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person2, person3, person4));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups3() throws Exception {
    //returns person1, person2

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg1.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person2));
  }

  @Test
  public void findAllByInActiveEmailAndPersonGroups3() throws Exception {
    //returns nothing

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg1.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person3));
  }

  @Test
  public void findAllByInActiveEmailAndPersonGroups3WithoutPersonGroup() throws Exception {
    //returns person3, person4

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person3, person4));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups4() throws Exception {
    //returns person2

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg2.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person2));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups5() throws Exception {
    //returns person1, person2

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg2.getId(), this.pg1.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person2));
  }

  @Test
  public void findAllByActiveEmailAndPersonGroups6() throws Exception {
    //returns person2

    this.person1.setEmail(null);
    this.sut.saveAndFlush(person1);

    this.link1.setInvalid(true);
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    List<Person> personList = this.sut.findAllByActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg2.getId(), this.pg1.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(1));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person2));
  }

  @Test
  public void findAllByInActiveEmailAndPersonGroups6() throws Exception {
    //returns person1, person3

    this.person1.setEmail(null);
    this.sut.saveAndFlush(person1);

    this.link1.setInvalid(true);
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(new ArrayList<Long>(
      Arrays.asList(this.pg2.getId(), this.pg1.getId())
    ));
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person1, person3));
  }

  @Test
  public void findAllByInActiveEmailEmpty() throws Exception {
    //returns person3 and person4

    this.person1.setEmail("person1@walter.at");
    this.sut.saveAndFlush(person1);

    this.link1.setValidationDate(ZonedDateTime.now());
    this.emailVerificationLinkRepository.saveAndFlush(this.link1);

    this.person4.setEmail("");
    this.sut.saveAndFlush(person4);

    Pageable pageable = PageRequest.of(0, 20);
    List<Person> personList = this.sut.findAllByInActiveEmailAndPersonGroups(null);
    Assert.assertThat(personList.size(), Matchers.equalTo(2));
    Assert.assertThat(personList, Matchers.containsInAnyOrder(person3, person4));
  }
}
