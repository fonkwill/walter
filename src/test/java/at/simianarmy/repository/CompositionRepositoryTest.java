package at.simianarmy.repository;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composition;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompositionRepositoryTest {

  private static final String DEFAULT_TITLE = "DEFAULT_TITLE";
  private static final Integer DEFAULT_AKM_NR = 12345;

  private ColorCode redCode;
  private ColorCode blueCode;
  private ColorCode whiteCode;
  private ColorCode orangeCode;

  @Inject
  CompositionRepository sut;

  @Inject
  ColorCodeRepository colorCodeRepository;

  @Transactional
  @Before
  public void initTest() {
    redCode = new ColorCode();
    redCode.setName("Rot");
    this.colorCodeRepository.saveAndFlush(redCode);

    blueCode = new ColorCode();
    blueCode.setName("Blau");
    this.colorCodeRepository.saveAndFlush(blueCode);

    whiteCode = new ColorCode();
    whiteCode.setName("Weiß");
    this.colorCodeRepository.saveAndFlush(whiteCode);

    orangeCode = new ColorCode();
    orangeCode.setName("Orange");
    this.colorCodeRepository.saveAndFlush(orangeCode);
  }

  @Transactional
  private List<Composition> createCompositionsForColor(int count, ColorCode colorCode) {

    List<Composition> returnList = new ArrayList<Composition>();

    for (int i = 1; i <= count; i++) {
      Composition comp = new Composition();
      comp.setNr(i);
      comp.setTitle(DEFAULT_TITLE);
      comp.setAkmCompositionNr(DEFAULT_AKM_NR);
      comp.setColorCode(colorCode);

      this.sut.saveAndFlush(comp);
      returnList.add(comp);
    }

    return returnList;
  }

  @Test
  public void testNextNumberFor3Compositions() {
    this.createCompositionsForColor(3, this.redCode);
    Integer nextNumber = this.sut.getNextNrForColorCode(this.redCode);

    assertThat(nextNumber, equalTo(4) );
  }

  @Test
  public void testNextNumberForNoCompositions() {
    Integer nextNumber = this.sut.getNextNrForColorCode(this.redCode);

    assertThat(nextNumber, equalTo(1));
  }

  @Test
  public void testWithVariousColors() {
    this.createCompositionsForColor(165, this.redCode);
    this.createCompositionsForColor(67, this.blueCode);
    this.createCompositionsForColor(111, this.orangeCode);

    assertThat(this.sut.getNextNrForColorCode(this.redCode), equalTo(166) );
    assertThat(this.sut.getNextNrForColorCode(this.blueCode), equalTo(68) );
    assertThat(this.sut.getNextNrForColorCode(this.whiteCode), equalTo(1) );
    assertThat(this.sut.getNextNrForColorCode(this.orangeCode), equalTo(112) );
  }
}
