package at.simianarmy.repository;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Filter;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class FilterRepositoryTest {

  private final String LIST_NAME = "PERSON";
  private final String NAME = "PERSON_FILTER";
  private final String FILTER = "{name='pf'}";

  private Filter filter;

  @Inject
  FilterRepository sut;

  @Before
  public void init() {
    this.filter = new Filter();
    filter.setListName(LIST_NAME);
    filter.setName(NAME);
    filter.setFilter(FILTER);

    this.sut.saveAndFlush(filter);
  }

  @After
  public void destroy() {
    this.sut.delete(this.filter);
  }

  @Test
  public void testExsistsByListNameAndName() {
    assertTrue(this.sut.existsByListNameAndName(LIST_NAME, NAME));
  }

  @Test
  public void testExsistsByListNameAndName2() {
    assertFalse(this.sut.existsByListNameAndName("PERSON2", NAME));
  }

  @Test
  public void testExsistsByListNameAndName3() {
    assertFalse(this.sut.existsByListNameAndName(LIST_NAME, "PERSON_FILTER2"));
  }

  @Test
  public void testExsistsByListNameAndName4() {
    assertFalse(this.sut.existsByListNameAndName("PERSON2", "PERSON_FILTER2"));
  }

  @Test
  public void testFindByListName() {
    List<Filter> result = this.sut.findByListName(LIST_NAME);
    assertTrue(result.contains(this.filter));
    assertEquals(1, result.size());
  }

}
