package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.PersonGroup;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonGroupRepositoryTest {

  @Inject
  private PersonGroupRepository sut;

  PersonGroup pg1;

  PersonGroup pg2;

  PersonGroup pg3;


  @Before
  public void setUp(){
    sut.deleteAll();
    sut.flush();

    pg1 = new PersonGroup();
    pg1.setName("relevant");
    pg1.setHasPersonRelevance(true);

    pg2 = new PersonGroup();
    pg2.setName("not-relevant");
    pg2.setHasPersonRelevance(false);

    pg3 = new PersonGroup();
    pg3.setName("also-relevant");
    pg3.setHasPersonRelevance(true);

    sut.save(pg1);
    sut.save(pg2);
    sut.save(pg3);
    sut.flush();

  }

  @After
  public void tearDown() {
    sut.deleteAll();;
    sut.flush();
  }

  @Test
  public void findAll_returnAll() {
    List<PersonGroup> pgs = sut.findAll();

    assertThat(pgs, hasSize(3));
    assertThat(pgs, allOf(hasItem(pg1), hasItem(pg2), hasItem(pg3)));

  }

  @Test
  public void findPersonRelevant_ReturnPersonRelevant() {
    List<PersonGroup> pgs = sut.findByHasPersonRelevanceIsTrue();

    assertThat(pgs, hasSize(2));
    assertThat(pgs, allOf(hasItem(pg1), hasItem(pg3)));
  }


}
