package at.simianarmy.repository;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Clothing;
import at.simianarmy.domain.ClothingType;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonClothing;
import at.simianarmy.service.specification.ClothingSpecification;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class ClothingRepositoryTest {
//	PersonData
	private static final String firstName = "hans";
	private static final String lastName = "huber";
	private static final String address = "abc1";
	private static final String plz = "1010";
	private static final String city = "Wien";
	private static final String country = "Österreich";
	
//	ClothingData
	private static final String size1 = "52";
	private static final String size2 = "54";
	
//	ClothingTypeData
	private static final String name1 = "T-Shirt";
	private static final String name2 = "Hose";
	
	private Clothing c1;
	private Clothing c2;
	private Clothing c3;
	private Clothing c4;
	private Clothing c5;
	private Clothing c6;
	private Clothing c7;

	
	private ClothingType ct1;
	private ClothingType ct2;
	
	private Person p1;
	private Person p2;
	
	private PersonClothing pc1;
	private PersonClothing pc2;
	private PersonClothing pc3;
	private PersonClothing pc4;
	private PersonClothing pc5;
	
	@Inject
	ClothingRepository clothingRepository;
	
	@Inject
	PersonRepository personRepository;
	
	@Inject
	PersonClothingRepository personClothingRepository;
	
	@Inject
	ClothingTypeRepository ClothingTypeRepository;

	@Before
	public void setUp() throws Exception {
		p1 = new Person();
		p1.setFirstName(firstName);
		p1.setLastName(lastName);
		p1.setStreetAddress(address);
		p1.setPostalCode(plz);
		p1.setCity(city);
		p1.setCountry(country);
		p1.setHasDirectDebit(false);
		
		
		p2 = new Person();
		p2.setFirstName(firstName);
		p2.setLastName(lastName);
		p2.setStreetAddress(address);
		p2.setPostalCode(plz);
		p2.setCity(city);
		p2.setCountry(country);
		p2.setHasDirectDebit(false);
		
		personRepository.save(p2);
		personRepository.save(p1);
		
		ct1 = new ClothingType();
		ct1.setName(name1);
		
		
		ct2 = new ClothingType();
		ct2.setName(name2);
		
		ClothingTypeRepository.save(ct1);
		ClothingTypeRepository.save(ct2);
		
		c1 = new Clothing();
		c1.setType(ct1);
		c1.setSize(size1);
		
		c2 = new Clothing();
		c2.setType(ct2);
		c2.setSize(size2);
		
		c3 = new Clothing();
		c3.setType(ct1);
		c3.setSize(size2);
		c3.setNotAvailable(true);
		
		c4 = new Clothing();
		c4.setType(ct2);
		c4.setSize(size1);
		
		c5 = new Clothing();
		c5.setType(ct1);
		
		c6 = new Clothing();
		c6.setType(ct2);
		c6.setNotAvailable(true);
		
		c7 = new Clothing();
		c7.setType(ct1);
		
		
		
		pc1 = new PersonClothing();
		pc1.setBeginDate(LocalDate.of(2016, 5, 1));
		pc1.setEndDate(LocalDate.of(2016, 9, 15));
		pc1.setPerson(p1);
		p1.addPersonClothings(pc1);
		pc1.setClothing(c1);
		c1.addPersonClothings(pc1);
		pc1 = personClothingRepository.save(pc1);
		
		pc2 = new PersonClothing();
		pc2.setBeginDate(LocalDate.of(2016, 7, 10));
		pc2.setPerson(p2);
		p2.addPersonClothings(pc2);
		pc2.setClothing(c2);
		c2.addPersonClothings(pc2);
		pc2 = personClothingRepository.save(pc2);
		
		pc3 = new PersonClothing();
		pc3.setClothing(c3);
		c3.addPersonClothings(pc3);
		pc3.setPerson(p1);
		p1.addPersonClothings(pc3);
		pc3.setBeginDate(LocalDate.of(2016, 5, 1));
		pc3.setEndDate(LocalDate.of(2016, 9, 15));
		pc3 = personClothingRepository.save(pc3);

		pc4 = new PersonClothing();
		pc4.setClothing(c4);
		c4.addPersonClothings(pc4);
		pc4.setPerson(p2);
		p2.addPersonClothings(pc4);
		pc4.setBeginDate(LocalDate.of(2016, 5, 10));
		pc4.setEndDate(LocalDate.of(2016, 9, 10));
		pc4 = personClothingRepository.save(pc4);

		pc5 = new PersonClothing();
		pc5.setClothing(c5);
		c5.addPersonClothings(pc5);
		pc5.setPerson(p2);
		p2.addPersonClothings(pc5);
		pc5.setBeginDate(LocalDate.of(2016, 12, 05));
		pc5 = personClothingRepository.save(pc5);
		c7 = clothingRepository.save(c7);
		c6 = clothingRepository.save(c6);
		c5 = clothingRepository.save(c5);
		c4 = clothingRepository.save(c4);
		c3 = clothingRepository.save(c3);
		c2 = clothingRepository.save(c2);
		c1 = clothingRepository.save(c1);

		personRepository.flush();
		clothingRepository.flush();
		ClothingTypeRepository.flush();
		personClothingRepository.flush();


	}
	
	@Test
	public void getFreeClothingsWithBeginDateBeforeAllSavedOfAnyTypeAndSize() {
		Specification<Clothing> specs = ClothingSpecification.availableWith(LocalDate.of(2016, 1, 1), null, null, null);
		
		List<Clothing> res = clothingRepository.findAll(specs);
		assertThat(res, Matchers.hasSize(1) );
		assertThat(res, Matchers.contains(c7));
		
	}
	
	@Test
	public void getFreeClothingsWithBeginDateOfAnyTypeAndSize() {
		Specification<Clothing> specs = ClothingSpecification.availableWith(LocalDate.of(2016, 10, 1), null, null, null);
		
		List<Clothing> res = clothingRepository.findAll(specs);
		assertThat(res, Matchers.hasSize(3) );
		assertThat(res, Matchers.containsInAnyOrder(c1, c4, c7));

	}
	
	@Test
	public void getFreeClothingsWithBeginDateAndEndDateOfAnyTypeAndSize() {
		Specification<Clothing> specs = Specification.where(ClothingSpecification.availableWith(LocalDate.of(2016, 10, 1), LocalDate.of(2016, 10, 31), null, null));
		
		List<Clothing> res = clothingRepository.findAll(specs);
		assertThat(res, Matchers.hasSize(4) );
		assertThat(res, Matchers.containsInAnyOrder(c7, c4, c5, c1));
	}
	
	
	@Test
	public void getAllWithNoPerson() {
		Specification<Clothing> specs = ClothingSpecification.allWithNoPerson(null, null);
		
		List<Clothing> res = clothingRepository.findAll(specs);
		assertThat(res, Matchers.hasSize(2) );
		assertThat(res, Matchers.containsInAnyOrder(c7, c6));
		
	}
	
	

	
	@Test
	public void getAllDistinct() throws Exception {
			Clothing c8 = new Clothing();
			Clothing c9 = new Clothing();
		
			c8.setType(ct1);
			c8.setSize(size1);
			
			
			c9.setType(ct2); 
			c9.setSize(size2);
			
			clothingRepository.saveAndFlush(c8);
			clothingRepository.saveAndFlush(c9);
			
			
//			List<String[]> res = clothingRepository.getAllDistinctValues();
//			assertThat(res, Matchers.hasSize(7) );
			
			
		
	}

	
}
