package at.simianarmy.repository;

import static org.junit.Assert.assertThat;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.MembershipFeeAmount;
import at.simianarmy.domain.enumeration.MembershipFeePeriod;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeAmountRepositoryTest {

  private static LocalDate DEAULT_DATE = LocalDate.parse("2017-01-01");
  private static LocalDate DEAULT_DATE_END = LocalDate.parse("2017-01-31");
  private static LocalDate DEFAULT_DATE_END2 = LocalDate.parse("2019-12-31");
  private static BigDecimal DEFAULT_AMOUNT = new BigDecimal(99.99);
  private static String DEFAULT_DESCRIPTION = "DESCRIPTION";
  private static Integer DEFAULT_TIME_FRACTION = 1;
  private static MembershipFeePeriod DEFAULT_PERIOD = MembershipFeePeriod.ANNUALLY;

  private MembershipFeeAmount m1;
  private MembershipFee fee1;

  @Inject
  private MembershipFeeAmountRepository sut;

  @Inject
  private MembershipFeeRepository membershipFeeRepository;

  @Before
  public void init() {
    this.fee1 = new MembershipFee();
    fee1.setDescription(DEFAULT_DESCRIPTION);
    fee1.setPeriodTimeFraction(DEFAULT_TIME_FRACTION);
    fee1.setPeriod(DEFAULT_PERIOD);

    this.membershipFeeRepository.saveAndFlush(fee1);

    this.m1 = new MembershipFeeAmount();
    m1.setBeginDate(DEAULT_DATE);
    m1.setEndDate(DEAULT_DATE_END);
    m1.setAmount(DEFAULT_AMOUNT);
    m1.setMembershipFee(fee1);

    this.sut.saveAndFlush(m1);

  }

  @After
  public void tearDown() {
    this.sut.deleteAll();
    this.membershipFeeRepository.deleteAll();
  }

  @Test
  public void testFindByMembershipFee() {
    Page<MembershipFeeAmount> result = this.sut.findByMembershipFee(this.fee1.getId(), null);
    assertThat(result.getContent(), Matchers.hasSize(1));
    assertThat(result.getContent(), Matchers.contains(this.m1));
  }

  @Test
  public void testFindByMembershipFeeAndYear() {
    MembershipFeeAmount result = this.sut.findByMembershipFeeAndYear(this.fee1.getId(), 2017);
    assertThat(result, Matchers.equalTo(this.m1));
  }
  
  @Test
  public void testFindByMembershipFeeAndYear2YearsValid() {
    this.m1.setEndDate(DEFAULT_DATE_END2);
    this.sut.saveAndFlush(this.m1);
    
    MembershipFeeAmount result = this.sut.findByMembershipFeeAndYear(this.fee1.getId(), 2018);
    assertThat(result, Matchers.equalTo(this.m1));
  }
  
  @Test
  public void testFindByMembershipFeeAndYearNotFound() {    
    MembershipFeeAmount result = this.sut.findByMembershipFeeAndYear(this.fee1.getId(), 2018);
    assertThat(result, Matchers.nullValue());
  }
  
  @Test
  public void testFindByMembershipFeeAndYearNoEndDate() {  
    this.m1.setEndDate(null);
    this.sut.saveAndFlush(this.m1);
    
    MembershipFeeAmount result = this.sut.findByMembershipFeeAndYear(this.fee1.getId(), 2019);
    assertThat(result, Matchers.equalTo(this.m1));
  }
  
  @Test
  public void testFindByMembershipFeeAndYearNoEndDate2() {  
    this.m1.setEndDate(null);
    this.sut.saveAndFlush(this.m1);
    
    MembershipFeeAmount result = this.sut.findByMembershipFeeAndYear(this.fee1.getId(), 2015);
    assertThat(result, Matchers.nullValue());
  }

}
