package at.simianarmy.repository;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Person;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipRepositoryTest {

	@Inject
	private MembershipRepository membershipRepository;
	
	@Inject
	private PersonRepository personRepository;
	
	private Person p1;
	private Person p2;
	
	private Membership m1;
	private Membership m2;
	private Membership m3;

	
	@Before
	public void setUp() throws Exception {
		 p1 = new Person();
      p1.setFirstName("fn1");
      p1.setLastName("ln1");
      p1.setStreetAddress("street 1");
      p1.setCity("city 1");
      p1.setCountry("country 1");
      p1.setPostalCode("1010");
      p1.setBirthDate(LocalDate.of(1969, 12, 31));
      p1.setHasDirectDebit(false);
		
	    p2 = new Person();
	    p2.setFirstName("fn2");
	    p2.setLastName("ln2");
	    p2.setStreetAddress("street 2");
	    p2.setCity("city 2");
	    p2.setCountry("country 2");
	    p2.setPostalCode("1020");
	    p2.setBirthDate(LocalDate.of(1969, 12, 31));
	    p2.setHasDirectDebit(false);
		    
		m1 = new Membership();
		m2 = new Membership();
		m3 = new Membership();
		
		
		m1.setBeginDate(LocalDate.of(2013, 01, 01));
		m1.setEndDate(LocalDate.of(2015, 12, 31));
		
		m2.setBeginDate(LocalDate.of(2017, 01, 01));
		
		m3.setBeginDate(LocalDate.of(2014, 01, 01));
		m3.setEndDate(LocalDate.of(2014, 12, 31));
		
		
		m2 = membershipRepository.save(m2);
		m1 = membershipRepository.save(m1);
		m3 = membershipRepository.save(m3);
		membershipRepository.flush();
		
		p1.addMemberships(m1);
		p1.addMemberships(m2);
		
		p2.addMemberships(m3);
		personRepository.save(p1);
		personRepository.save(p2);
		personRepository.flush();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testYear_withEnd() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2015);
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m1));
		
	}
	
	@Test
	public void testYear_withMiddle() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2014);
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m1));
		
	}
	
	public void testYear_withBegin() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2013);
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m1));
		
	}

	@Test
	public void testYear_withoutEndBegin() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2017);
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m2));
	}
	
	@Test
	public void testYear_withoutEndFollowingYear() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2018);
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m2));
	}
	
	
	@Test
	public void testYear_withoutResult() {
		List<Membership> result = membershipRepository.findByPersonAndYear(p1.getId(), 2016);
		assertThat(result, Matchers.hasSize(0));
	}
	
	@Test
	public void testDueDate_withBeginAndEnd() {
		List<Membership> result = membershipRepository.findByPersonAndDueDate(p1.getId(), LocalDate.of(2014, 05, 10));
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m1));
	}
	
	@Test
	public void testDueDate_withoutResult() {
		List<Membership> result = membershipRepository.findByPersonAndDueDate(p1.getId(), LocalDate.of(2016, 05, 10));
		assertThat(result, Matchers.hasSize(0));
	}
	
	@Test
	public void testDueDate_onlyBegin() {
		List<Membership> result = membershipRepository.findByPersonAndDueDate(p1.getId(), LocalDate.of(2017, 05, 10));
		assertThat(result, Matchers.hasSize(1));
		assertThat(result, Matchers.contains(m2));
	}
	
	@Test
	public void testDueDate_invalidParameters_person() {
		List<Membership> result = membershipRepository.findByPersonAndDueDate(null, LocalDate.of(2017, 05, 10));
		assertThat(result, Matchers.hasSize(0));
	}
	
	@Test
	public void testDueDate_invalidParameters_dueDate() {
		List<Membership> result = membershipRepository.findByPersonAndDueDate(p1.getId(),null);
		assertThat(result, Matchers.hasSize(0));
	}
	
	@Test
	public void testDueDate_multiplePersons() {
		List<Membership> result = membershipRepository.findByDueDate(LocalDate.of(2014, 01, 01));
		assertThat(result, Matchers.containsInAnyOrder(m1, m3));
	}
	
	

	
}
