package at.simianarmy.repository;

import static org.assertj.core.api.Assertions.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.NotificationRule;

import java.util.List;

import javax.inject.Inject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class NotificationRuleRepositoryTest {
  
  private NotificationRule rule1;
  private NotificationRule rule2;
  private NotificationRule rule3;
  
  @Inject
  private NotificationRuleRepository notificationRuleRepository;

  @Before
  public void setUp() {
	notificationRuleRepository.deleteAll();  
	
    rule1 = new NotificationRule();
    rule1.setTitle("Test Rule");
    rule1.setEntity("Person");
    rule1.setConditions("select person from Person person");
    rule1.setActive(true);
    rule1.setMessageGroup("message for group");
    rule1.setMessageSingle("mesage for single");
    
    rule1 = notificationRuleRepository.saveAndFlush(rule1);
    
    rule2 = new NotificationRule();
    rule2.setTitle("Test Rule");
    rule2.setEntity("Person");
    rule2.setConditions("select person from Person person");
    rule2.setActive(false);
    rule2.setMessageGroup("message for group");
    rule2.setMessageSingle("mesage for single");
    
    rule2 = notificationRuleRepository.saveAndFlush(rule2);
    
    rule3 = new NotificationRule();
    rule3.setTitle("Test Rule");
    rule3.setEntity("Person");
    rule3.setConditions("select person from Person person");
    rule3.setActive(true);
    rule3.setMessageGroup("message for group");
    rule3.setMessageSingle("mesage for single");
    
    rule3 = notificationRuleRepository.saveAndFlush(rule3);
  }
  
  @After
  public void tearDown() {
    notificationRuleRepository.delete(rule1);
    notificationRuleRepository.delete(rule2);
    notificationRuleRepository.delete(rule3);
  }
  
  @Test
  public void testFindAllActive() {
    List<NotificationRule> rules = notificationRuleRepository.findAllActive();
    assertThat(rules.size()).isEqualTo(2);
    assertThat(rules).contains(rule1);
    assertThat(rules).contains(rule3);
  }
  
}
