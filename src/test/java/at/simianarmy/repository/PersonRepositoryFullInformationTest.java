package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.*;
import at.simianarmy.domain.enumeration.EmailType;
import at.simianarmy.domain.enumeration.Gender;
import groovy.lang.DelegatesTo;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.lang.reflect.Member;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonRepositoryFullInformationTest {

  @Inject
  private PersonRepository sut;

  @Inject
  private PersonGroupRepository personGroupRepository;

  @Inject
  private PersonInstrumentRepository personInstrumentRepository;

  @Inject
  private PersonClothingRepository personClothingRepository;

  @Inject
  private InstrumentRepository instrumentRepository;

  @Inject
  private ClothingRepository clothingRepository;

  @Inject
  private AppointmentRepository appointmentRepository;

  @Inject
  private MembershipFeeRepository membershipFeeRepository;

  @Inject
  private MembershipRepository membershipRepository;

  @Inject
  private MembershipFeeForPeriodRepository membershipFeeForPeriodRepository;


  @Inject
  private PersonAwardRepository personAwardRepository;

  @Inject
  private PersonHonorRepository personHonorRepository;

  @Inject
  private AwardRepository awardRepository;

  @Inject
  private HonorRepository honorRepository;

  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;

  @Inject
  private BankImportDataRepository bankImportDataRepository;

  @Before
  public void setUp() {

  }

  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void deleteAllPersonInfo_ShouldDeleteAllInfo() {
    Person p = sut.findOneWithAllEagerRelations(1L);

    Set<PersonInstrument> pi = p.getPersonInstruments();
    Set<PersonClothing> pc = p.getPersonClothings();
    Set<PersonGroup> befPersonGroups = p.getPersonGroups();
    Set<Instrument> i = p.getPersonInstruments().stream().map(pis -> pis.getInstrument()).collect(Collectors.toSet());
    Set<Clothing> c = p.getPersonClothings().stream().map(pcs -> pcs.getClothing()).collect(Collectors.toSet());
    Set<Appointment> pa = p.getAppointments();
    Set<Membership> mems = p.getMemberships();
    Set<MembershipFeeForPeriod> memFeePeriod = new HashSet<>();
    mems.stream().forEach(m -> {
      memFeePeriod.addAll(m.getMembershipFeeForPeriods());
    });
    Set<MembershipFee> memFee = mems.stream().map(mem -> mem.getMembershipFee()).collect(Collectors.toSet());
    Set<PersonAward> befPersonAwards = p.getPersonAwards();
    Set<PersonHonor> befPersonHonors = p.getPersonHonors();
    Set<Honor> befHonors = p.getPersonHonors().stream().map(h -> h.getHonor()).collect(Collectors.toSet());
    Set<Award> befAwards = p.getPersonAwards().stream().map(a -> a.getAward()).collect(Collectors.toSet());

    Set<CashbookEntry> befCashbookEntries = memFeePeriod.stream().map(mfp -> mfp.getCashbookEntry()).filter(cb -> cb!=null).collect(Collectors.toSet());

    Set<BankImportData> befBankImportData = befCashbookEntries.stream().map(cb -> cb.getBankImportData()).collect(Collectors.toSet());



    sut.delete(p);
    sut.flush();


    List<PersonInstrument> pir = personInstrumentRepository.findAll();
    for (PersonInstrument pil : pi) {
      assertThat( pir, not(hasItem(pil)));
    }
    List<PersonClothing> pcr = personClothingRepository.findAll();
    for (PersonClothing pcl : pc) {
      assertThat( pcr, not(hasItem(pcl)));
    }
    List<Instrument>  ir = instrumentRepository.findAll();
    for (Instrument il : i) {
      assertThat( ir, hasItem(il));
    }
    List<Clothing> cr = clothingRepository.findAll();
    for (Clothing cl : c) {
      assertThat( cr, hasItem(cl));
    }
    List<Appointment> ap = appointmentRepository.findAll();
    for (Appointment apl: pa) {
      assertThat( ap, hasItem(apl));
    }
    List<MembershipFee> fee = membershipFeeRepository.findAll();
    for (MembershipFee f : memFee) {
      assertThat(fee, hasItem(f));
    }
    List<Membership> memberships = membershipRepository.findAll();
    for (Membership mm: mems) {
      assertThat(memberships, not(hasItem(mm)));
    }
    List<MembershipFeeForPeriod> memFeeForPeriod = membershipFeeForPeriodRepository.findAll();
    for (MembershipFeeForPeriod mfeeP : memFeePeriod) {
      assertThat(memFeeForPeriod, not(hasItem(mfeeP)));
    }
    List<PersonGroup> aftPersonGroups = personGroupRepository.findAll();
    for (PersonGroup befPersonGroup : befPersonGroups) {
      assertThat(aftPersonGroups, hasItem(befPersonGroup));
    }
    List<PersonAward> aftPersonAward = personAwardRepository.findAll();
    for (PersonAward befPersonAward : befPersonAwards) {
      assertThat(aftPersonAward, not(hasItem(befPersonAward)));
    }
    List<PersonHonor> aftPersonHonor = personHonorRepository.findAll();
    for (PersonHonor befPersonHonor : befPersonHonors) {
      assertThat(aftPersonHonor, not(hasItem(befPersonHonor)));
    }
    List<Honor> aftHonors = honorRepository.findAll();
    for (Honor befHonor : befHonors) {
      assertThat(aftHonors, hasItem(befHonor));
    }
    List<Award> aftAwards = awardRepository.findAll();
    for (Award befAward: befAwards) {
      assertThat(aftAwards, hasItem(befAward));
    }
    List<CashbookEntry> aftCashbookEntries = cashbookEntryRepository.findAll();
    for (CashbookEntry befCashbook : befCashbookEntries) {
      assertThat(aftCashbookEntries, hasItem(befCashbook) );
    }
    List<BankImportData> aftBankImportData = bankImportDataRepository.findAll();
    for (BankImportData befBankImpor : befBankImportData) {
      assertThat(aftBankImportData, hasItem(befBankImpor));
    }


  }


  @Test
  @Sql(scripts = {"/data/person_test.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
  public void getAllPersonInfo_ShouldReturnAllInfo() {
    Person p = sut.findOneWithAllEagerRelations(1L);

    Person exp = new Person();
    exp.setFirstName("Hansi");
    exp.setLastName("Huber");
    exp.setBirthDate(LocalDate.of(1989, 02, 06));
    exp.setPostalCode("2256");
    exp.setStreetAddress("Untere Hauptstrasse 22");
    exp.setEmail("hansili.huber@gmx.at");
    exp.setCity("Oberndorf");
    exp.setCountry("Austria");
    exp.setEmailType(EmailType.VERIFIED);
    exp.setGender(Gender.MALE);
    exp.setHasDirectDebit(false);

    PersonGroup expPg1 = new PersonGroup();
    expPg1.setId(1L);
    expPg1.setName("Vorstand");

    PersonGroup expPg2= new PersonGroup();
    expPg1.setId(2L);
    expPg1.setName("Musiker");

    exp.addPersonGroups(expPg1);
    exp.addPersonGroups(expPg2);


    assertEquals(exp.getFirstName(), p.getFirstName());
    assertEquals(exp.getLastName(), p.getLastName());
    assertEquals(exp.getBirthDate(), p.getBirthDate());
    assertEquals(exp.getEmail(), p.getEmail());
    assertEquals(exp.getStreetAddress(), p.getStreetAddress());
    assertEquals(exp.getPostalCode(), p.getPostalCode());
    assertEquals(exp.getCity(), p.getCity());
    assertEquals(exp.getCountry(), p.getCountry());
    assertEquals(exp.getGender(), p.getGender());
    assertEquals(exp.getEmailType(), p.getEmailType());
    assertEquals(exp.getHasDirectDebit(), p.getHasDirectDebit());

    //persongroups
    Set<PersonGroup> pg = p.getPersonGroups();
    assertThat(pg, hasItems(
                      hasProperty("id", equalTo(expPg1.getId())),
                      hasProperty("id", equalTo(expPg1.getId())),
                      hasProperty("name", equalTo(expPg1.getName())),
                      hasProperty("name", equalTo(expPg1.getName()))
                    )
    );

    //personinstruments
    Set<PersonInstrument> pin = p.getPersonInstruments();
    assertThat(pin, hasSize(2));
    assertThat(pin, hasItems(
                      hasProperty("beginDate", equalTo(LocalDate.of(2018, 1, 1))),
                      hasProperty("beginDate", equalTo(LocalDate.of(2018, 1, 1))),
                      hasProperty("endDate", equalTo(LocalDate.of(2018, 10, 31)))
    ));

    Set<Instrument> in = p.getPersonInstruments().stream().map(pins -> pins.getInstrument()).collect(Collectors.toSet());
    assertThat(in, hasItems(
                      hasProperty("name", equalTo("Trompete")),
                      hasProperty("name", equalTo("Saxophon"))
    ));

    Set<InstrumentType> inty  = in.stream().map(intys -> intys.getType()).collect(Collectors.toSet());
    assertThat(inty, hasItems(
      hasProperty("name", equalTo("Blasinstrument"))
    ));

    //clothings
    Set<PersonClothing> pcl = p.getPersonClothings();
    assertThat(pcl, hasSize(2));
    assertThat(pcl, hasItems(
      hasProperty("beginDate", equalTo(LocalDate.of(2019, 1, 1))),
      hasProperty("beginDate", equalTo(LocalDate.of(2018, 11, 12)))
    ));

    Set<Clothing> cl = pcl.stream().map(pins -> pins.getClothing()).collect(Collectors.toSet());
    assertThat(cl, hasItems(
      hasProperty("size", equalTo("L")),
      hasProperty("size", equalTo("52"))
    ));

    Set<ClothingType> clty  = cl.stream().map(intys -> intys.getType()).collect(Collectors.toSet());
    assertThat(clty, hasItems(
      hasProperty("name", equalTo("Jacke")),
      hasProperty("name", equalTo("Hose"))
    ));

    //awards
    Set<PersonAward> paw = p.getPersonAwards();
    assertThat(paw, hasSize(2));
    assertThat(paw, hasItems(
      hasProperty("date", equalTo(LocalDate.of(2018, 9, 4))),
      hasProperty("date", equalTo(LocalDate.of(2018, 4, 1)))
    ));

    Set<Award> aw = paw.stream().map(pins -> pins.getAward()).collect(Collectors.toSet());
    assertThat(aw, hasItems(
      hasProperty("name", equalTo("Goldenes Abzeichen")),
      hasProperty("name", equalTo("Silbernes Abzeichen"))
    ));

    //honor
    Set<PersonHonor> pho = p.getPersonHonors();
    assertThat(pho, hasSize(2));
    assertThat(pho, hasItems(
      hasProperty("date", equalTo(LocalDate.of(2018, 11, 1))),
      hasProperty("date", equalTo(LocalDate.of(2019, 02, 11)))
    ));

    Set<Honor> ho = pho.stream().map(pins -> pins.getHonor()).collect(Collectors.toSet());
    assertThat(ho, hasItems(
      hasProperty("name", equalTo("10 Jahre Mtiglied")),
      hasProperty("name", equalTo("5 Jahre Vorstand"))
    ));

    //appointment
    Set<Appointment> ap = p.getAppointments();
    assertThat(ap, hasSize(2));
    assertThat(ap, hasItems(
      hasProperty("name", equalTo("Probe")),
      hasProperty("name", equalTo("Vorstands-Sitzung")),
      hasProperty("streetAddress", equalTo("Wirthaus Hansi")),
      hasProperty("streetAddress", equalTo("Untere Oberstrasse"))
    ));

    Set<AppointmentType> apt = ap.stream().map(apty -> apty.getType()).collect(Collectors.toSet());
    assertThat(apt, hasItems(
      hasProperty("name", equalTo("Probe")),
      hasProperty("name", equalTo("Sitzung"))
    ));

    //memberships
    Set<Membership> mem = p.getMemberships();
    assertThat(mem, hasSize(1));
    assertThat(mem, hasItems(
      hasProperty("beginDate", equalTo(LocalDate.of(2018,01,01)))
    ));

    //membershipFee
    Set<MembershipFee> memFee = mem.stream().map(mems -> mems.getMembershipFee()).collect(Collectors.toSet());
    assertThat(memFee, hasSize(1));
    assertThat(memFee, hasItems(
      hasProperty("description", equalTo("Vollzahler"))
      ));

    Set<MembershipFeeForPeriod> memFeePeriod = new HashSet<>();
    mem.stream().forEach(mems -> memFeePeriod.addAll(mems.getMembershipFeeForPeriods()));
    assertThat(memFeePeriod, hasSize(2));
    assertThat(memFeePeriod, hasItems(
      //hasProperty("paid", equalTo(true)),
      hasProperty("year", equalTo(2019)),
      hasProperty("year", equalTo(2018))
    ));

    //cashbookEntry
    Set<CashbookEntry> cbe = memFeePeriod.stream().map(mems -> mems.getCashbookEntry()).filter(cb -> cb != null).collect(Collectors.toSet());
    assertThat(cbe, hasSize(1));
    assertThat(cbe, hasItems(
      hasProperty("title", equalTo("00001012019"))
    ));

    //bankImportData
    Set<BankImportData> bid = cbe.stream().map(cbs -> cbs.getBankImportData()).filter(cb -> cb != null).collect(Collectors.toSet());
    System.out.println(bid.iterator().next());
    assertThat(bid, hasSize(1));
    assertThat(bid, hasItems(
      hasProperty("entryReference", equalTo("12000168945021665875476345231234".getBytes())),
      hasProperty("partnerName", equalTo("Huber Hansi"))
    ));






  }





}
