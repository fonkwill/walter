package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MembershipFee;
import at.simianarmy.domain.enumeration.MembershipFeePeriod;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MembershipFeeRepositoryTest {

  private static final String FEE1_DES = "Vollzahler";
  private static final String FEE2_DES = "Eine Ermäßigung muss schon sein";

  private MembershipFee fee1;
  private MembershipFee fee2;

  @Autowired
  MembershipFeeRepository sut;

  @Before
  public void setUp() throws Exception {
    fee1 = new MembershipFee();
    fee1.setDescription(FEE1_DES);
    fee1.setPeriod(MembershipFeePeriod.ANNUALLY);
    fee1.setPeriodTimeFraction(1);
    fee1 = sut.save(fee1);

    fee2 = new MembershipFee();
    fee2.setDescription(FEE2_DES);
    fee2.setPeriod(MembershipFeePeriod.ANNUALLY);
    fee2.setPeriodTimeFraction(1);
    fee2 = sut.save(fee2);
    sut.flush();

  }

  @After
  public void tearDown() throws Exception {
    sut.deleteAll();
    sut.flush();

  }

  @Test
  public void findByDescription_ShouldFindOne(){
    MembershipFee fee = sut.findFirstByDescription(FEE1_DES);
    Assert.assertEquals(fee1.getId(), fee.getId());
  }

  @Test
  public void findByDescription_ShouldFindNone(){
    MembershipFee fee = sut.findFirstByDescription("Auch Zahler");
    Assert.assertEquals(null, fee);
  }

  @Test
  public void findByDescriptionWithSpace_ShouldFindOne(){
    MembershipFee fee = sut.findFirstByDescription(FEE2_DES);
    Assert.assertEquals(fee2.getId(), fee.getId());
  }


}
