package at.simianarmy.repository;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.BankImportData;
import at.simianarmy.domain.CashbookCategory;
import at.simianarmy.domain.CashbookEntry;
import at.simianarmy.domain.EmailVerificationLink;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.enumeration.BankImporterType;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import javax.inject.Inject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class EmailVerificationLinkRepositoryTest {

  private static String DEFAULT_KEY = "ABCDE";
  private static String DEFAULT_NAME = "ASDF";
  private static String DEFAULT_ADRESS = "BCEDE";
  private static String DEFAULT_POST_CODE = "1100";
  private static ZonedDateTime DEFAULT_DATE = ZonedDateTime.now();

  @Inject
  EmailVerificationLinkRepository sut;

  @Inject
  PersonRepository personRepository;

  EmailVerificationLink link1;
  EmailVerificationLink link2;
  EmailVerificationLink link3;

  Person person1;
  Person person2;
  Person person3;

  @Before
  public void init() {
    person1 = new Person();
    person1.setFirstName(DEFAULT_NAME);
    person1.setLastName(DEFAULT_NAME);
    person1.setCity(DEFAULT_ADRESS);
    person1.setPostalCode(DEFAULT_POST_CODE);
    person1.setCountry(DEFAULT_ADRESS);
    person1.setStreetAddress(DEFAULT_ADRESS);
    person1.setHasDirectDebit(false);
    this.personRepository.save(person1);

    link1 = new EmailVerificationLink();
    link1.setInvalid(false);
    link1.setSecretKey(DEFAULT_KEY);
    link1.setPerson(person1);
    link1.setCreationDate(DEFAULT_DATE);
    this.sut.save(link1);

    person2 = new Person();
    person2.setFirstName(DEFAULT_NAME);
    person2.setLastName(DEFAULT_NAME);
    person2.setCity(DEFAULT_ADRESS);
    person2.setPostalCode(DEFAULT_POST_CODE);
    person2.setCountry(DEFAULT_ADRESS);
    person2.setStreetAddress(DEFAULT_ADRESS);
    person2.setHasDirectDebit(false);
    this.personRepository.save(person2);

    link2 = new EmailVerificationLink();
    link2.setInvalid(false);
    link2.setSecretKey(DEFAULT_KEY);
    link2.setPerson(person2);
    link2.setCreationDate(DEFAULT_DATE);
    this.sut.save(link2);

    person3 = new Person();
    person3.setFirstName(DEFAULT_NAME);
    person3.setLastName(DEFAULT_NAME);
    person3.setCity(DEFAULT_ADRESS);
    person3.setPostalCode(DEFAULT_POST_CODE);
    person3.setCountry(DEFAULT_ADRESS);
    person3.setStreetAddress(DEFAULT_ADRESS);
    person3.setHasDirectDebit(false);
    this.personRepository.save(person3);

    link3 = new EmailVerificationLink();
    link3.setInvalid(false);
    link3.setSecretKey(DEFAULT_KEY);
    link3.setPerson(person3);
    link3.setCreationDate(DEFAULT_DATE);
    this.sut.save(link3);

  }

  @After
  public void tearDown() {
    this.personRepository.delete(person1);
    this.personRepository.delete(person2);
    this.personRepository.delete(person3);
  }

  @Test
  public void testFindAllToInvalidate() {
    ZonedDateTime referenceDate = DEFAULT_DATE.plusMinutes(30);

    List<EmailVerificationLink> result = this.sut.findAllToInvalidate(referenceDate);

    assertTrue(result.contains(this.link1));
    assertTrue(result.contains(this.link2));
    assertTrue(result.contains(this.link3));
    assertEquals(3, result.size());

  }

  @Test
  public void testFindAllToInvalidateOneAlreadyInvalid() {
    ZonedDateTime referenceDate = DEFAULT_DATE.plusMinutes(30);

    this.link3.setInvalid(true);
    this.sut.save(this.link3);

    List<EmailVerificationLink> result = this.sut.findAllToInvalidate(referenceDate);

    assertTrue(result.contains(this.link1));
    assertTrue(result.contains(this.link2));
    assertFalse(result.contains(this.link3));
    assertEquals(2, result.size());
  }

  @Test
  public void testFindAllToInvalidateOneValidated() {
    ZonedDateTime referenceDate = DEFAULT_DATE.plusMinutes(30);

    this.link3.setValidationDate(DEFAULT_DATE.minusMinutes(30));
    this.sut.save(this.link3);

    List<EmailVerificationLink> result = this.sut.findAllToInvalidate(referenceDate);

    assertTrue(result.contains(this.link1));
    assertTrue(result.contains(this.link2));
    assertFalse(result.contains(this.link3));
    assertEquals(2, result.size());
  }

  @Test
  public void testFindAllToInvalidateTwoInFuture() {
    ZonedDateTime referenceDate = DEFAULT_DATE.plusMinutes(30);

    this.link1.setCreationDate(DEFAULT_DATE.plusMinutes(10));

    this.link3.setCreationDate(DEFAULT_DATE.plusMinutes(31));
    this.sut.save(this.link3);

    this.link2.setCreationDate(DEFAULT_DATE.plusMinutes(40));
    this.sut.save(this.link2);

    List<EmailVerificationLink> result = this.sut.findAllToInvalidate(referenceDate);

    assertTrue(result.contains(this.link1));
    assertEquals(1, result.size());
  }

  @Test
  public void testExistsActiveForPerson() {
    this.link1.setInvalid(true);
    this.link1.setValidationDate(null);
    this.link2.setInvalid(true);
    this.link2.setValidationDate(DEFAULT_DATE);
    this.link3.setInvalid(false);
    this.link3.setValidationDate(DEFAULT_DATE);

    this.sut.save(link1);
    this.sut.save(link2);
    this.sut.save(link3);

    assertFalse(this.sut.existsByPersonAndInvalidFalseAndValidationDateIsNull(this.person1));
    assertFalse(this.sut.existsByPersonAndInvalidFalseAndValidationDateIsNull(this.person2));
    assertFalse(this.sut.existsByPersonAndInvalidFalseAndValidationDateIsNull(this.person3));
  }

  @Test
  public void testExistsActiveForPersonOneActive() {
    this.link1.setInvalid(false);
    this.link1.setValidationDate(null);

    this.sut.save(link1);

    assertTrue(this.sut.existsByPersonAndInvalidFalseAndValidationDateIsNull(this.person1));
    assertEquals(this.link1, this.sut.findFirstByPersonAndInvalidFalseAndValidationDateIsNull(this.person1));
  }

}
