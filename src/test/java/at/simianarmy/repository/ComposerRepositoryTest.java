package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest(classes = WalterApp.class)
public class ComposerRepositoryTest {

  @Inject
  ComposerRepository sut;

  @Inject
  CompositionRepository compositionRepository;

  @Inject
  ColorCodeRepository colorCodeRepository;

  private ColorCode redCode;
  private ColorCode blueCode;

  private Composition compositionA;
  private Composition compositionB;

  private Composer composerX;
  private Composer composerY;
  Set<Composer> composers = new HashSet<>();

  @Before
  public void setUp() {
    composerX = new Composer();
    composerX.setName("Composer X");
    composers.add(composerX);

    composerY = new Composer();
    composerY.setName("Composer Y");
    composers.add(composerY);

    redCode = new ColorCode();
    redCode.setName("Rot");
    colorCodeRepository.saveAndFlush(redCode);

    blueCode = new ColorCode();
    blueCode.setName("Blau");
    colorCodeRepository.saveAndFlush(blueCode);

    compositionA = new Composition();
    compositionA.setNr(1);
    compositionA.setTitle("Composition A");
    compositionA.setColorCode(redCode);
    compositionA.setAkmCompositionNr(1);
    compositionRepository.saveAndFlush(compositionA);

    compositionB = new Composition();
    compositionB.setNr(2);
    compositionB.setTitle("Composition B");
    compositionB.setColorCode(blueCode);
    compositionB.setAkmCompositionNr(2);
    compositionRepository.saveAndFlush(compositionB);
  }


  @Test
  public void findByCompositionA() throws Exception {
    sut.saveAll(composers);

    compositionA.addComposers(composerX);
    compositionB.addComposers(composerY);

    List<Composer> result = sut.findByComposition(compositionA.getId());
    Assert.assertThat(result.size(), Matchers.equalTo(1));
    Assert.assertThat(result.get(0), Matchers.equalTo(composerX));
  }


  @Test
  public void findByComposition() throws Exception {
    sut.saveAll(composers);

    composerX.addCompositions(compositionA);
    composerX.addCompositions(compositionB);
    sut.save(composerX);

    composerY.addCompositions(compositionA);
    sut.save(composerY);

    List<Composer> result = sut.findByComposition(compositionB.getId());
    Assert.assertThat(result.size(), Matchers.equalTo(1));
    Assert.assertThat(result.get(0), Matchers.equalTo(composerX));

    result = sut.findByComposition(compositionA.getId());
    Assert.assertThat(result.size(), Matchers.equalTo(2));
    Assert.assertThat(result, Matchers.contains(composerX, composerY));
  }

}
