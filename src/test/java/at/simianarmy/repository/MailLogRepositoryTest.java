package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.MailLog;
import at.simianarmy.domain.enumeration.MailLogStatus;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MailLogRepositoryTest {

  private final String DEFAULT_EMAIL = "test@test.com";
  private final String DEFAULT_SUBJECT = "SUBJECT";
  private final String DEFAULT_MSG = "MSG";
  private final Pageable pageable = PageRequest.of(0, 20);

  @Inject
  private MailLogRepository sut;

  private MailLog log1;
  private MailLog log2;
  private MailLog log3;
  private MailLog log4;

  @Before
  public void init() {

    this.log1 = new MailLog();
    this.log1.setMailLogStatus(MailLogStatus.SUCCESSFUL);
    this.log1.setDate(ZonedDateTime.now().minusHours(4));
    this.log1.setEmail(DEFAULT_EMAIL);
    this.log1.setSubject(DEFAULT_SUBJECT);
    this.log1.setErrorMessage(DEFAULT_MSG);

    this.sut.saveAndFlush(this.log1);

    this.log2 = new MailLog();
    this.log2.setMailLogStatus(MailLogStatus.UNSUCCESSFUL);
    this.log2.setDate(ZonedDateTime.now().minusHours(3));
    this.log2.setEmail(DEFAULT_EMAIL);
    this.log2.setSubject(DEFAULT_SUBJECT);
    this.log2.setErrorMessage(DEFAULT_MSG);

    this.sut.saveAndFlush(this.log2);

    this.log3 = new MailLog();
    this.log3.setMailLogStatus(MailLogStatus.SUCCESSFUL);
    this.log3.setDate(ZonedDateTime.now().minusMinutes(40));
    this.log3.setEmail(DEFAULT_EMAIL);
    this.log3.setSubject(DEFAULT_SUBJECT);
    this.log3.setErrorMessage(DEFAULT_MSG);

    this.sut.saveAndFlush(this.log3);

    this.log4 = new MailLog();
    this.log4.setMailLogStatus(MailLogStatus.WAITING);
    this.log4.setDate(ZonedDateTime.now().minusMinutes(5));
    this.log4.setEmail(DEFAULT_EMAIL);
    this.log4.setSubject(DEFAULT_SUBJECT);
    this.log4.setErrorMessage(DEFAULT_MSG);

    this.sut.saveAndFlush(this.log4);

  }

  @After
  public void tearDown() {
    this.sut.deleteAll();
  }

  @Test
  public void testFindByDate() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = new ArrayList<>(Arrays.asList(MailLogStatus.values()));

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(4));
    Assert.assertThat(result, Matchers.contains(this.log4, this.log3, this.log2, this.log1));

  }

  @Test
  public void testFindByDateAndNoStatus() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = null;

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(4));
    Assert.assertThat(result, Matchers.contains(this.log4, this.log3, this.log2, this.log1));

  }

  @Test
  public void testFindByDateAndUnsuccessful() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = new ArrayList<>(Arrays.asList(MailLogStatus.UNSUCCESSFUL));

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(1));
    Assert.assertThat(result, Matchers.contains(this.log2));

  }

  @Test
  public void testFindByDateAndSuccessful() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = new ArrayList<>(Arrays.asList(MailLogStatus.SUCCESSFUL));

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(2));
    Assert.assertThat(result, Matchers.contains(this.log3, this.log1));

  }

  @Test
  public void testFindByDateAndWaiting() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = new ArrayList<>(Arrays.asList(MailLogStatus.WAITING));

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(1));
    Assert.assertThat(result, Matchers.contains(this.log4));

  }

  @Test
  public void testFindByDateAndWaitingAndSuccessful() {

    LocalDate date = LocalDate.now();
    List<MailLogStatus> statusList = new ArrayList<>(Arrays.asList(MailLogStatus.WAITING, MailLogStatus.SUCCESSFUL));

    List<MailLog> result = this.sut.findByDateAndMailLogStatus(date, statusList, this.pageable).getContent();

    Assert.assertThat(result.size(), Matchers.equalTo(3));
    Assert.assertThat(result, Matchers.contains(this.log4, this.log3, this.log1));

  }

  @Test
  public void testFindByDateBefore() {

    ZonedDateTime date = ZonedDateTime.now().minusHours(1);

    List<MailLog> result = this.sut.findByDateBefore(date);

    Assert.assertThat(result.size(), Matchers.equalTo(2));
    Assert.assertThat(result, Matchers.containsInAnyOrder(this.log1, this.log2));

  }

}
