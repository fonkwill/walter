package at.simianarmy.repository;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Membership;
import at.simianarmy.domain.Person;
import at.simianarmy.domain.PersonGroup;
import at.simianarmy.domain.enumeration.Gender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
@Transactional
public class PersonRepositoryPersonRelevanceTest {


  @Inject
  PersonGroupRepository personGroupRepository;

  @Inject
  MembershipRepository membershipRepository;

  @Inject
  PersonRepository sut;


  private Person person1;

  private Person person2;

  private PersonGroup pgRelevant;

  private PersonGroup pgNotRelevant;

  private Membership m1;

  private Membership m2;

  private static final LocalDate DEFAULT_BIRTHDATE = LocalDate.of(1960, 1, 1);
  private static final String DEFAULT_FIRSTNAME = "Vorname";
  private static final String DEFAULT_LASTNAME = "Nachname";
  private static final String DEFAULT_STREET = "Straße";
  private static final String DEFAULT_PLZ = "12345";
  private static final String DEFAULT_CITY = "Ort";
  private static final String DEFAULT_COUNTRY = "AT";

  @Before
  public void setup(){
    pgRelevant = new PersonGroup();
    pgRelevant.setName("relevant");
    pgRelevant.setHasPersonRelevance(true);

    pgNotRelevant = new PersonGroup();
    pgNotRelevant.setName("notrelevant");
    pgNotRelevant.setHasPersonRelevance(false);



    person1 = new Person();
    person1.setFirstName(DEFAULT_FIRSTNAME + "1");
    person1.setLastName(DEFAULT_LASTNAME + "1");
    person1.setBirthDate(DEFAULT_BIRTHDATE);
    person1.setStreetAddress(DEFAULT_STREET + " 1");
    person1.setPostalCode(DEFAULT_PLZ);
    person1.setCity(DEFAULT_CITY);
    person1.setCountry(DEFAULT_COUNTRY);
    person1.setGender(Gender.FEMALE);
    person1.setHasDirectDebit(false);
    person1.setDateLostRelevance(null);

    person2 = new Person();
    person2.setFirstName(DEFAULT_FIRSTNAME + "2");
    person2.setLastName(DEFAULT_LASTNAME + "2");
    person2.setBirthDate(DEFAULT_BIRTHDATE);
    person2.setStreetAddress(DEFAULT_STREET + " 2");
    person2.setPostalCode(DEFAULT_PLZ);
    person2.setCity(DEFAULT_CITY);
    person2.setCountry(DEFAULT_COUNTRY);
    person2.setGender(Gender.FEMALE);
    person2.setHasDirectDebit(false);
    person2.setDateLostRelevance(null);

    m1 = new Membership();
    m1.setBeginDate(LocalDate.of(2018, 1,1));
    m1.setEndDate(LocalDate.of(2019, 1, 31));

    m2 = new Membership();
    m2.setBeginDate(LocalDate.of(2019, 1,1));
    m2.setEndDate(LocalDate.of(2019, 12, 31));


  }

  @Test
  public void notRelevantTwoWithRelevantGroup_ShouldReturnNone(){
    pgRelevant = personGroupRepository.save(pgRelevant);
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());

    person1.addPersonGroups(pgRelevant);
    person1.addPersonGroups(pgNotRelevant);
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(0) );


  }

  @Test
  public void relevantTwoWithRelevantGroupNotMarked_ShouldReturnNone(){
    pgRelevant = personGroupRepository.save(pgRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());

    person1.addPersonGroups(pgRelevant);
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(0) );


  }

  @Test
  public void relevantTwoWithRelevantGroupMarked_ShouldReturnBoth(){
    pgRelevant = personGroupRepository.save(pgRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());

    person1.setDateLostRelevance(LocalDate.of(2019, 1,1));
    person1.addPersonGroups(pgRelevant);
    person1 = sut.save(person1);

    person2.setDateLostRelevance(LocalDate.of(2019, 1,1));
    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(2) );
    assertThat(result, allOf(hasItem(person1), hasItem(person2)));
  }

  @Test
  public void notRelevantOneWithRelevantGroupOneWithChild_ShouldReturnBoth() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    pgRelevant.addChildren(pgNotRelevant);
    pgRelevant = personGroupRepository.save(pgRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());
    relevantPgIds.add(pgNotRelevant.getId());

    person1.addPersonGroups(pgNotRelevant);
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);

    sut.flush();

    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(0) );

  }

  @Test
  public void relevantOneWithRelevantGroupOneWithChildAlreadyMarked_ShouldReturnOne() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    pgRelevant.addChildren(pgNotRelevant);
    pgRelevant = personGroupRepository.save(pgRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());
    relevantPgIds.add(pgNotRelevant.getId());

    person1.addPersonGroups(pgNotRelevant);
    person1.setDateLostRelevance(LocalDate.of(2019,01,01));
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);

    sut.flush();

    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(1) );
    assertThat(result, allOf(hasItem(person1)));

  }

  @Test
  public void notRelevantOneWithRelevantGroupOneWithNotRelevantParent_ShouldReturnOne() {
    pgRelevant = personGroupRepository.save(pgRelevant);


    pgNotRelevant.addChildren(pgRelevant);
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);

    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());

    //person1.addPersonGroups(pgNotRelevant);
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2 = sut.save(person2);

    sut.flush();

    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(1) );
    assertThat(result, hasItem(person1));

  }

  @Test
  public void relevantOneWithRelevantGroupAlreadyMarkedOneWithNotRelevantParentAlreadyMarked_ShouldReturnOne() {
    pgRelevant = personGroupRepository.save(pgRelevant);


    pgNotRelevant.addChildren(pgRelevant);
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    List<Long> relevantPgIds = new ArrayList<>();
    relevantPgIds.add(pgRelevant.getId());

    person1.addPersonGroups(pgNotRelevant);
    person1.setDateLostRelevance(LocalDate.of(2019,01,01));
    person1 = sut.save(person1);

    person2.addPersonGroups(pgRelevant);
    person2.setDateLostRelevance(LocalDate.of(2019,01,01));
    person2 = sut.save(person2);

    sut.flush();

    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 01, 01), relevantPgIds);

    assertThat(result, hasSize(1) );
    assertThat(result, hasItem(person2));

  }

  @Test
  public void notRelevanTwoWithActiveMembership_ShouldReturnNone() {
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 1, 5), null);

    assertThat(result, hasSize(0) );

  }

  @Test
  public void relevanTwoWithActiveMembershiOneAlreadyMarked_ShouldReturnNone() {
    person1.setDateLostRelevance(LocalDate.of(2019,01,01));
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 1, 5), new ArrayList<>());

    assertThat(result, hasSize(1) );
    assertThat(result, hasItem(person1) );

  }

  @Test
  public void relevantOneWithActiveMembershipOneWithFormerMembershipAlreadyMarked_ShouldReturnNone() {
    person1.setDateLostRelevance(LocalDate.of(2019,01,01));
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 02, 05), null);

    assertThat(result, hasSize(0) );

  }

  @Test
  public void notRelevantOneWithInActiveMembershipOneWithNotRelevantPersonGroup_ShouldReturnBoth() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    person1.addPersonGroups(pgNotRelevant);
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.flush();

    person2.addMemberships(m1);
    sut.flush();

    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 2, 5), new ArrayList<>());

    assertThat(result, hasSize(2) );
    assertThat(result, allOf(hasItem(person2), hasItem(person1)));

    boolean hasMembership = false;
    for (Person p : result) {
      if (p.getId().equals(person2.getId())) {
        hasMembership =  p.getMemberships().size() > 0;
      }
    }

    assertThat(hasMembership, is(true));


  }
  @Test
  public void notRelevantOneWithInActiveAndActiveMembership_ShouldReturnNone() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();


    person1 = sut.save(person1);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person1);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 2, 5),null);

    assertThat(result, hasSize(0) );
  }


  @Test
  public void relevantOneWithInActiveMembershipNotMarkedOneWithNotRelevantPersonGroupAlreadyMarked_ShouldReturnNone() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    person2.addPersonGroups(pgNotRelevant);
    person2.setDateLostRelevance(LocalDate.of(2019,01,01));
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    membershipRepository.save(m1);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 2, 5), null);

    assertThat(result, hasSize(0) );
  }

  @Test
  public void notRelevantOneWithInActiveMembershipOneWithNotRelevantPersonGroupAlreadyMarked_ShouldReturnOne() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    person2.addPersonGroups(pgNotRelevant);
    person2.setDateLostRelevance(LocalDate.of(2019,1,1));
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithEmptyLostRelevanceDateAndInactiveMembershipOrNotInPersonGroups(LocalDate.of(2019, 2, 5), null);

    assertThat(result, hasSize(1) );
    assertThat(result, hasItem(person1));
  }


  @Test
  public void notRelevantOneWithInActiveMembershipAlreadyMarkedOneWithNotRelevantPersonGroup_ShouldReturnNone() {
    pgNotRelevant = personGroupRepository.save(pgNotRelevant);
    personGroupRepository.flush();

    person2.addPersonGroups(pgNotRelevant);
    person1.setDateLostRelevance(LocalDate.of(2019, 1,1

    ));
    person1 = sut.save(person1);
    person2 = sut.save(person2);
    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person2);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 2, 5), new ArrayList<>());

    assertThat(result, hasSize(0) );
  }

  @Test
  public void relevantOneWithInActiveAndActiveMembershipMarked_ShouldReturnOne() {

    person1.setDateLostRelevance(LocalDate.of(2019,1,1));
    person1 = sut.save(person1);

    sut.flush();

    m1.setPerson(person1);
    m2.setPerson(person1);
    membershipRepository.save(m1);
    membershipRepository.save(m2);
    membershipRepository.flush();


    List<Person> result = sut.findAllWithLostRelevanceDateButActiveMembershipOrInPersonGroups(LocalDate.of(2019, 2, 5), null);

    assertThat(result, hasSize(1) );
    assertThat(result, hasItem(person1));
  }

  @Test
  public void getWithNoRelevanceOneFilledOneNot_ShouldReturnOne() {

    person1.setDateLostRelevance(LocalDate.of(2019,1,1));
    person1 = sut.save(person1);

    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllByDateLostRelevanceIsBefore(LocalDate.of(2019, 1, 2));

    assertThat(result, hasSize(1));
    assertThat(result, hasItem(person1));
  }

  @Test
  public void getWithNoRelevanceOneBeforeOneAfter_ShouldReturnOne() {

    person1.setDateLostRelevance(LocalDate.of(2019,1,1));
    person1 = sut.save(person1);

    person2.setDateLostRelevance(LocalDate.of(2019,1,5));
    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllByDateLostRelevanceIsBefore(LocalDate.of(2019, 1, 2));

    assertThat(result, hasSize(1));
    assertThat(result, hasItem(person1));
  }

  @Test
  public void getWithNoRelevanceTwoBefore_ShouldReturnBoth() {

    person1.setDateLostRelevance(LocalDate.of(2019,1,1));
    person1 = sut.save(person1);

    person2.setDateLostRelevance(LocalDate.of(2018,1,5));
    person2 = sut.save(person2);
    sut.flush();

    List<Person> result = sut.findAllByDateLostRelevanceIsBefore(LocalDate.of(2019, 1, 2));

    assertThat(result, hasSize(2));
    assertThat(result, allOf(hasItem(person1), hasItem(person2)));
  }









}
