package at.simianarmy.domain;


import at.simianarmy.WalterApp;
import at.simianarmy.domain.enumeration.CashbookEntryType;
import at.simianarmy.repository.CashbookAccountRepository;
import at.simianarmy.repository.CashbookEntryRepository;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookEntryTest {

  private static final String DEFAULT_TITLE = "Test-Title";
  private static final CashbookEntryType DEFAULT_TYPE = CashbookEntryType.INCOME;
  private static final LocalDate DEFAULT_DATE = LocalDate.now();
  private static final BigDecimal DEFAULT_AMOUNT = BigDecimal.valueOf(12.25);
  private static final String DEFAULT_NAME = "Account";
  private static final String DEFAULT_CODE = "A";
  
  private CashbookEntry existingEntry;

  private CashbookEntry entry;

  private CashbookAccount cashbookAccount;
  
  @Inject
  private CashbookEntryRepository cashbookEntryRepository;

  @Inject
  private CashbookAccountRepository cashbookAccountRepository;
  
  private void assertEqualsCashbookEntry(CashbookEntry expected, CashbookEntry given) {
    assertThat(given.getTitle() , equalTo(expected.getTitle()));
    assertThat(given.getType(), equalTo(expected.getType()));
    assertThat(given.getDate(), equalTo(expected.getDate()));
    assertThat(given.getAmount(), equalTo(expected.getAmount()));
    assertThat(given.getCashbookAccount(), equalTo(expected.getCashbookAccount()));
  }
  
  @Before
  public void initTest() {

      this.cashbookAccount = new CashbookAccount();
      this.cashbookAccount.setName(DEFAULT_NAME);
      this.cashbookAccount.setCurrentNumber(0);
      this.cashbookAccount.setBankAccount(false);
      this.cashbookAccount.setReceiptCode(DEFAULT_CODE);
      this.cashbookAccount.setDefaultForCashbook(false);
      this.cashbookAccountRepository.save(this.cashbookAccount);

      this.existingEntry = new CashbookEntry();
      this.existingEntry.setTitle(DEFAULT_TITLE);
      this.existingEntry.setType(DEFAULT_TYPE);
      this.existingEntry.setDate(DEFAULT_DATE);
      this.existingEntry.setAmount(DEFAULT_AMOUNT);
      this.existingEntry.setCashbookAccount(this.cashbookAccount);
      this.cashbookEntryRepository.save(this.existingEntry);
  }

  @After
  public void tearDown() {
    if (this.entry != null && this.entry.getId() != null && this.cashbookEntryRepository.findById(this.entry.getId()).isPresent()) {
      this.cashbookEntryRepository.delete(this.entry);
    }
    this.cashbookEntryRepository.delete(this.existingEntry);
    this.cashbookAccountRepository.delete(cashbookAccount);
  }

  @Test
  public void testCreate() {
    this.entry = new CashbookEntry();
    entry.setTitle(DEFAULT_TITLE);
    entry.setType(DEFAULT_TYPE);
    entry.setDate(DEFAULT_DATE);
    entry.setAmount(DEFAULT_AMOUNT);
    entry.setCashbookAccount(this.cashbookAccount);

    this.cashbookEntryRepository.save(entry);

    CashbookEntry created = this.cashbookEntryRepository.findById(entry.getId()).orElse(null);

    assertEqualsCashbookEntry(entry, created);
  }
  
  @Test
  public void testUpdate() {
    this.existingEntry.setTitle("Updated Test-Title");
    this.existingEntry.setType(CashbookEntryType.SPENDING);
    this.existingEntry.setDate(LocalDate.of(2011, 11, 11));
    this.existingEntry.setAmount(BigDecimal.valueOf(23.12));
    this.cashbookEntryRepository.save(this.existingEntry);
    
    CashbookEntry updated = this.cashbookEntryRepository.findById(this.existingEntry.getId()).orElse(null);
    
    assertEqualsCashbookEntry(this.existingEntry, updated);
  }
  
  @Test
  public void testDelete() {
    Long id = this.existingEntry.getId();
    this.cashbookEntryRepository.delete(this.existingEntry);
    assertFalse(this.cashbookEntryRepository.existsById(id));
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithMissingTitle() {
    this.entry = new CashbookEntry();

    entry.setType(CashbookEntryType.SPENDING);
    entry.setDate(LocalDate.now());
    entry.setAmount(BigDecimal.valueOf(23.12));
    entry.setCashbookAccount(this.cashbookAccount);
    
    this.cashbookEntryRepository.save(entry);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithMissingType() {
    this.entry = new CashbookEntry();

    entry.setTitle("Test-Title");
    entry.setDate(LocalDate.now());
    entry.setAmount(BigDecimal.valueOf(23.12));
    entry.setCashbookAccount(this.cashbookAccount);
    
    this.cashbookEntryRepository.save(entry);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithMissingDate() {
    this.entry = new CashbookEntry();

    entry.setTitle("Test-Title");
    entry.setType(CashbookEntryType.SPENDING);
    entry.setAmount(BigDecimal.valueOf(23.12));
    entry.setCashbookAccount(this.cashbookAccount);
    
    this.cashbookEntryRepository.save(entry);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithMissingAmount() {
    this.entry = new CashbookEntry();

    entry.setTitle("Test-Title");
    entry.setType(CashbookEntryType.SPENDING);
    entry.setDate(LocalDate.MAX);
    entry.setCashbookAccount(this.cashbookAccount);
    
    this.cashbookEntryRepository.save(entry);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithAmountLessThanZero() {
    this.entry = new CashbookEntry();

    entry.setTitle("Test-Title");
    entry.setType(CashbookEntryType.SPENDING);
    entry.setDate(LocalDate.now());
    entry.setAmount(BigDecimal.valueOf(-23.12));
    entry.setCashbookAccount(this.cashbookAccount);
    
    this.cashbookEntryRepository.save(entry);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithMissingCashbookAccount() {
    this.entry = new CashbookEntry();

    entry.setTitle("Test-Title");
    entry.setType(CashbookEntryType.SPENDING);
    entry.setDate(LocalDate.now());
    entry.setAmount(BigDecimal.valueOf(23.12));

    this.cashbookEntryRepository.save(entry);
  }
}
