package at.simianarmy.domain;


import at.simianarmy.WalterApp;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.repository.CompositionRepository;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompositionTest {

  private static final String DEFAULT_TITLE = "DEFAULT_TITLE";
  private static final Integer DEFAULT_AKM_NR = 12345;
  private static final String COLOR_NAME1 = "red";
  private static final String COLOR_NAME2 = "blue";
  
  private Composition existingComposition;

  @Inject
  private CompositionRepository compositionRepository;
  
  @Inject
  private ColorCodeRepository colorCodeRepository;
  
  @Before
  public void init() {
    this.existingComposition = new Composition();
    this.existingComposition.setTitle(DEFAULT_TITLE + 1);
    this.existingComposition.setAkmCompositionNr(DEFAULT_AKM_NR + 1);
    this.existingComposition.setColorCode(this.getColorCode(COLOR_NAME2));
    this.compositionRepository.saveAndFlush(this.existingComposition);
  }

  private void assertEqualsComposition(final Composition expected, final Composition given) {
    assertThat(given.getId(), equalTo(expected.getId()));
    assertThat(given.getNr(), equalTo(expected.getNr()));
    assertThat(given.getAkmCompositionNr(), equalTo(expected.getAkmCompositionNr()));
    assertThat(given.getColorCode(), equalTo(expected.getColorCode()));
  }
  
  private ColorCode getColorCode(String name) {
    ColorCode code = new ColorCode();
    code.setName(name);
    this.colorCodeRepository.saveAndFlush(code);
    return code;
  }

  @Test
  public void testCreate() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.getColorCode(COLOR_NAME1));
    
    Composition created = this.compositionRepository.saveAndFlush(composition);
    
    this.assertEqualsComposition(composition, created);
  }
  
  @Test
  public void testUpdate() {
    this.existingComposition.setTitle(DEFAULT_TITLE + "_updated");
    
    Composition updated = this.compositionRepository.saveAndFlush(this.existingComposition);
    
    this.assertEqualsComposition(this.existingComposition, updated);
  }
  
  @Test
  public void testDelete() {
    Long id = this.existingComposition.getId();
    this.compositionRepository.delete(this.existingComposition);
    assertFalse(this.compositionRepository.existsById(id));
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithoutTitleShouldFail() {
    Composition composition = new Composition();
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.getColorCode(COLOR_NAME1));
    
    this.compositionRepository.saveAndFlush(composition);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithoutAkmNrShouldFail() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setColorCode(this.getColorCode(COLOR_NAME1));
    
    this.compositionRepository.saveAndFlush(composition);
  }
  
  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithoutColorCodeShouldFail() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    
    this.compositionRepository.saveAndFlush(composition);
  }
}
