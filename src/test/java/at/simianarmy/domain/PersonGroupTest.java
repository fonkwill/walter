package at.simianarmy.domain;

import static org.junit.Assert.*;

import java.util.Set;

import static org.hamcrest.Matchers.*;

import javax.inject.Inject;
import javax.validation.ValidationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import at.simianarmy.WalterApp;
import at.simianarmy.repository.PersonGroupRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class PersonGroupTest {

  private static final String name1 = "NAME1";
  private static final String name2 = "NAME2";
  private PersonGroup pgOne;

  @Inject
  private PersonGroupRepository personGroupRepository;

  @Before
  public void setUp() throws Exception {
    pgOne = new PersonGroup();
    pgOne.setName(name1);
    pgOne = personGroupRepository.save(pgOne);

  }

  @After
  public void tearDown() throws Exception {
    pgOne = null;
  }

  @Test
  public void testSave() {
    PersonGroup toSave = new PersonGroup();
    toSave.setName(name1);
    PersonGroup saved = personGroupRepository.save(toSave);

    personGroupRepository.findById(saved.getId()).orElse(null);
    assertThatEquals(saved, toSave);
  }

  @Test
  public void testUpdate() {
    pgOne.setName(name2);
    personGroupRepository.save(pgOne);

    PersonGroup pgUpdated = personGroupRepository.findById(pgOne.getId()).orElse(null);
    assertThatEquals(pgOne, pgUpdated);
  }

  @Test
  public void testDelete() {
    personGroupRepository.deleteById(pgOne.getId());

    assertFalse(personGroupRepository.existsById(pgOne.getId()));
  }

  @Test(expected = ValidationException.class)
  public void saveWithoutName() {
    PersonGroup toSave = new PersonGroup();
    PersonGroup saved = personGroupRepository.save(toSave);

    personGroupRepository.findById(saved.getId()).orElse(null);
    assertThatEquals(saved, toSave);
  }

  @Test
  @Transactional
  public void saveWithParent() {
    PersonGroup child = new PersonGroup();
    child.setName(name2);
    child.setParent(pgOne);

    child = personGroupRepository.save(child);
    child = personGroupRepository.findById(child.getId()).orElse(null);

    assertThatEquals(child.getParent(), pgOne);
   

  }

  @Test
  @Transactional
  public void saveWithChildren() {
    PersonGroup pgParent = new PersonGroup();
    pgParent.setName(name2);
    pgParent.addChildren(pgOne);

    pgParent = personGroupRepository.save(pgParent);
    pgParent = personGroupRepository.findById(pgParent.getId()).orElse(null);

    Set<PersonGroup> children = pgParent.getChildren();

    assertThat(children.size(), greaterThan(0));

  }

  private void assertThatEquals(PersonGroup a, PersonGroup b) {
    assertThat(a, equalTo(b));
    assertThat(a.getName(), equalTo(b.getName()));
    assertThat(a.getParent(), equalTo(b.getParent()));
  }

}
