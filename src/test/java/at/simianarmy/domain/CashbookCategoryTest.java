package at.simianarmy.domain;


import at.simianarmy.WalterApp;
import at.simianarmy.repository.CashbookCategoryRepository;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CashbookCategoryTest {

  private static final String DEFAULT_NAME = "TEST_NAME";
  private static final String DEFAULT_DESCRIPTION = "TEST_DESCRIPTION";
  private static final String DEFAULT_COLOR = "#efefef";
  private static final String DEFAULT_COLOR_INVALID1 = "invalid";
  private static final String DEFAULT_COLOR_INVALID2 = "#ef";
  private static final String DEFAULT_COLOR_INVALID3 = "#efefzz";
  private static final String DEFAULT_COLOR_INVALID4 = "e#";
  private CashbookCategory existingCategory;

  @Inject
  private CashbookCategoryRepository cashbookCategoryRepository;

  @Before
  public void initTest() {
    this.existingCategory = new CashbookCategory();
    this.existingCategory.setName(DEFAULT_NAME);
    this.existingCategory.setDescription(DEFAULT_DESCRIPTION);
    this.existingCategory.setColor(DEFAULT_COLOR);
    this.cashbookCategoryRepository.save(this.existingCategory);
  }

  private void assertEqualsCashbookCategory(CashbookCategory expected, CashbookCategory given) {
    assertThat(given.getDescription(), equalTo(expected.getDescription()));
    assertThat(given.getColor() , equalTo(expected.getColor()) ) ;
    assertThat(given.getParent(), equalTo(expected.getParent()) );
  }

  @Test
  public void testCreate() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setDescription(DEFAULT_DESCRIPTION);
    category.setColor(DEFAULT_COLOR);

    this.cashbookCategoryRepository.save(category);

    CashbookCategory created = this.cashbookCategoryRepository.findById(category.getId()).orElse(null);

    assertEqualsCashbookCategory(category, created);
  }

  @Test
  public void testUpdate() {
    this.existingCategory.setName("Updated_Name");
    this.existingCategory.setDescription("Updated Description");
    this.cashbookCategoryRepository.save(this.existingCategory);

    CashbookCategory updated = this.cashbookCategoryRepository
        .findById(this.existingCategory.getId()).orElse(null);

    assertEqualsCashbookCategory(this.existingCategory, updated);
  }

  @Test
  public void testDelete() {
    Long id = this.existingCategory.getId();
    this.cashbookCategoryRepository.delete(this.existingCategory);
    assertFalse(this.cashbookCategoryRepository.existsById(id));
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithNoName() {
    CashbookCategory category = new CashbookCategory();
    category.setName(null);
    category.setColor(DEFAULT_COLOR);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithNoColor() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor(null);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithInvalidColor1() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor(DEFAULT_COLOR_INVALID1);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithInvalidColor2() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor(DEFAULT_COLOR_INVALID2);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithInvalidColor3() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor(DEFAULT_COLOR_INVALID3);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithInvalidColor4() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor(DEFAULT_COLOR_INVALID4);
    this.cashbookCategoryRepository.save(category);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithInvalidColor5() {
    CashbookCategory category = new CashbookCategory();
    category.setName(DEFAULT_NAME);
    category.setColor("");
    this.cashbookCategoryRepository.save(category);
  }
}
