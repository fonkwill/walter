package at.simianarmy.domain.listener;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.ColorCode;
import at.simianarmy.domain.Composition;
import at.simianarmy.repository.ColorCodeRepository;
import at.simianarmy.repository.CompositionRepository;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class CompositionListenerTest {

  private static final String DEFAULT_TITLE = "DEFAULT_TITLE";
  private static final Integer DEFAULT_AKM_NR = 12345;
  private static final String DEFAULT_COLOR = "DEFAULT_COLOR";
  private static final String DEFAULT_COLOR2 = "DEFAULT_COLOR2";

  private ColorCode colorCode;
  private ColorCode colorCode2;

  @Inject
  private CompositionRepository compositionRepository;

  @Inject
  private ColorCodeRepository colorCodeRepository;

  @Before
  public void init() {
    this.colorCode = new ColorCode();
    this.colorCode.setName(DEFAULT_COLOR);
    this.colorCodeRepository.saveAndFlush(this.colorCode);
    
    this.colorCode2 = new ColorCode();
    this.colorCode2.setName(DEFAULT_COLOR2);
    this.colorCodeRepository.saveAndFlush(this.colorCode2);
  }

  @Test
  public void testCreateCompositionWithNullAsNumber() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr() , equalTo(1) );
  }

  @Test
  public void testCreateCompositionWithNumber() {
    Composition composition = new Composition();
    composition.setNr(12345);
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr() , equalTo(1) );
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateCompositionWithNoColor() {
    Composition composition = new Composition();
    composition.setNr(12345);
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);

    this.compositionRepository.saveAndFlush(composition);
  }

  @Test
  public void testUpdateShouldNotChangeNumber() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr(), equalTo(1) );

    composition.setTitle(DEFAULT_TITLE + "1");

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr() , equalTo(1) );
  }
  
  @Test
  public void testUpdateShouldNotChangeNumber2() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr(), equalTo(1) );

    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr(), equalTo(1) );
  }
  
  @Test
  public void testUpdateShouldChangeNumber() {
    Composition composition = new Composition();
    composition.setTitle(DEFAULT_TITLE);
    composition.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition.setColorCode(this.colorCode);

    this.compositionRepository.saveAndFlush(composition);
    assertThat(composition.getNr(), equalTo(1) );

    Composition composition2 = new Composition();
    composition2.setTitle(DEFAULT_TITLE);
    composition2.setAkmCompositionNr(DEFAULT_AKM_NR);
    composition2.setColorCode(this.colorCode);
    
    this.compositionRepository.saveAndFlush(composition2);
    assertThat(composition2.getNr(), equalTo(2) );
    
    composition2.setColorCode(this.colorCode2);

    this.compositionRepository.saveAndFlush(composition2);
    
    Composition toCompare = this.compositionRepository.findById(composition2.getId()).orElse(null);
    
    assertThat(toCompare.getNr(), equalTo(1) );
  }
}
