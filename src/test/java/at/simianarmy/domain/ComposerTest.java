package at.simianarmy.domain;


import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import at.simianarmy.WalterApp;
import at.simianarmy.repository.ComposerRepository;

import javax.inject.Inject;
import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class ComposerTest {

  private static final String DEFAULT_NAME = "TEST_NAME";
  private static final String DEFAULT_NAME2 = "TEST_NAME2";

  @Inject
  private ComposerRepository composerRepository;

  private Composer existingComposer;

  private void assertEqualsComposer(Composer expected, Composer given) {
    assertThat(given.getName(), equalTo(expected.getName()));
  }

  @Before
  public void initTest() {
    this.existingComposer = new Composer();
    this.existingComposer.setName(DEFAULT_NAME2);
    this.composerRepository.save(this.existingComposer);
  }

  @Test
  public void testCreate() {
    Composer composer = new Composer();
    composer.setName(DEFAULT_NAME);
    this.composerRepository.save(composer);

    Composer created = this.composerRepository.findById(composer.getId()).orElse(null);

    this.assertEqualsComposer(composer, created);
  }

  @Test
  public void testUpdate() {
    this.existingComposer.setName("Updated_Name");

    this.composerRepository.save(this.existingComposer);

    Composer updated = this.composerRepository.findById(this.existingComposer.getId()).orElse(null);

    assertEqualsComposer(this.existingComposer, updated);
  }

  @Test
  public void testDelete() {
    Long id = this.existingComposer.getId();
    this.composerRepository.delete(this.existingComposer);
    assertFalse(this.composerRepository.existsById(id));
  }

  @Test(expected = ConstraintViolationException.class)
  public void testCreateWithNoName() {
    Composer composer = new Composer();
    composer.setName(null);
    this.composerRepository.save(composer);
  }
}
