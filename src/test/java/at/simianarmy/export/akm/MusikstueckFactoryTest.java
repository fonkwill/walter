package at.simianarmy.export.akm;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Arranger;
import at.simianarmy.domain.Composer;
import at.simianarmy.domain.Composition;
import at.simianarmy.export.akm.xml.Musikstueck;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.repository.ComposerRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

@Component
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class MusikstueckFactoryTest {

  private static final long piratesID = 12345;
  private static final long adagioID = 34567;

  private static final int piratesAkmId = 98765;
  private static final int adagioAkmId = 54321;


  Composition piratesComposition;
  Composition adagioComposition;

  private List<Arranger> arrangers;
  private List<Composer> pirateComposers;
  private List<Composer> adagioComposers;

  @Mock
  private ComposerRepository composerRepository;

  @Mock
  private ArrangerRepository arrangerRepository;

  private VeranstaltungFactoryTest veranstaltungTest = new VeranstaltungFactoryTest();

  @InjectMocks
  private MusikstueckFactory sut;


  public void initMocks(ArrangerRepository arrangerRepo, ComposerRepository composerRepo) {
    Mockito.when(composerRepo.findByComposition(piratesID))
      .thenReturn(pirateComposers);
    Mockito.when(composerRepo.findByComposition(adagioID))
      .thenReturn(adagioComposers);
    Mockito.when(arrangerRepo.findByComposition(piratesID))
      .thenReturn(arrangers);
  }


  @Before
  public void setUp() throws Exception {
    veranstaltungTest.setUp();

    piratesComposition = new Composition();
    piratesComposition.setId(piratesID);
    piratesComposition.setAkmCompositionNr(piratesAkmId);
    piratesComposition.setTitle("Pirates of the Caribbean");

    Composer klausBadelt = new Composer();
    klausBadelt.setId(123L);
    klausBadelt.setName("Klaus Badelt");

    Composer hansZimmer = new Composer();
    hansZimmer.setId(124L);
    hansZimmer.setName("Hans Zimmer");

    pirateComposers = new LinkedList<>();
    pirateComposers.add(klausBadelt);
    pirateComposers.add(hansZimmer);
    piratesComposition.setComposers(new HashSet<>(pirateComposers));

    Arranger johnWasson = new Arranger();
    johnWasson.setName("John Wasson");
    arrangers = new LinkedList<>();
    arrangers.add(johnWasson);
    piratesComposition.setArrangers(new HashSet<>(arrangers));

    adagioComposition = new Composition();
    adagioComposition.setId(adagioID);
    adagioComposition.setAkmCompositionNr(adagioAkmId);
    adagioComposition.setTitle("Adagio");

    Composer jsBach = new Composer();
    jsBach.setId(125L);
    jsBach.setName("Johann Sebastian Bach");
    adagioComposers = new LinkedList<>();
    adagioComposers.add(jsBach);
    adagioComposition.setComposers(new HashSet<>(adagioComposers));
  }


  @Test
  public void testGetAkmCompositionNr() throws Exception {
    Musikstueck musikstueck = sut.createMusikstueck(piratesComposition);
    Assert.assertThat(musikstueck.getWerknummer(), Matchers.equalTo(piratesAkmId));
  }


  @Test
  public void testMusikstueckMarshal() throws Exception {
    initMocks(arrangerRepository, composerRepository);

    JAXBContext jc = JAXBContext.newInstance(Musikstueck.class);
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();

    // workaround for getting rid of standalone="yes"
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    xmlStream.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n".getBytes());

    Musikstueck musikstueck = sut.createMusikstueck(piratesComposition);
    musikstueck.setJahr(2017);
    marshaller.marshal(musikstueck, xmlStream);

    String result = xmlStream.toString("ISO-8859-1");
//    System.out.println(result);

    String musikstueckXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        + "<musikstueck>\n"
        + "    <aks_id>12345</aks_id>\n"
        + "    <aks_werknummer>98765</aks_werknummer>\n"
        + "    <aks_titel>Pirates of the Caribbean</aks_titel>\n"
        + "    <aks_komponist>Klaus Badelt, Hans Zimmer</aks_komponist>\n"
        + "    <aks_arrangeur>John Wasson</aks_arrangeur>\n"
        + "    <aks_anz_auffuehrungen>0</aks_anz_auffuehrungen>\n"
        + "    <aks_jahr>2017</aks_jahr>\n"
        + "</musikstueck>";
    Assert.assertThat(result, Matchers.equalToIgnoringWhiteSpace(musikstueckXML));
  }

}
