package at.simianarmy.export.akm;

import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.OfficialAppointment;

import java.io.ByteArrayOutputStream;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import at.simianarmy.export.akm.xml.Veranstaltung;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class VeranstaltungFactoryTest {

  Appointment appointmentAdventkonzert2015;
  Appointment appointmentSaalkonzert2016;
  Appointment appointmentAdventkonzert2016;

  private OfficialAppointment offAppAdventkonzert2015;
  private OfficialAppointment offAppSaalkonzert2016;
  private OfficialAppointment offAppAdventkonzert2016;

  private VeranstaltungFactory sut = new VeranstaltungFactory();


  @Before
  public void setUp() throws Exception {
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm z");

    appointmentAdventkonzert2015 = new Appointment();
    appointmentAdventkonzert2015.setBeginDate(
      ZonedDateTime.parse("20.12.2015 18:00 Europe/Vienna", formatter));
    appointmentAdventkonzert2015.setEndDate(
      ZonedDateTime.parse("20.12.2015 20:00 Europe/Vienna", formatter));
    appointmentAdventkonzert2015.setStreetAddress("Pfarrkirche, Dorfplatz 1");
    appointmentAdventkonzert2015.setPostalCode("9876");
    appointmentAdventkonzert2015.setCity("Teststädtchen");
    appointmentAdventkonzert2015.setName("Adventskonzert 2015");

    offAppAdventkonzert2015 = new OfficialAppointment();
    offAppAdventkonzert2015.setOrganizerName("Organisator");
    offAppAdventkonzert2015.setOrganizerAddress("Teststraße 1");
    appointmentAdventkonzert2015.setOfficialAppointment(offAppAdventkonzert2015);

    appointmentSaalkonzert2016 = new Appointment();
    appointmentSaalkonzert2016.setBeginDate(
      ZonedDateTime.parse("20.02.2016 20:00 Europe/Vienna", formatter));
    appointmentSaalkonzert2016.setEndDate(
      ZonedDateTime.parse("20.02.2016 22:00 Europe/Vienna", formatter));
    appointmentSaalkonzert2016.setStreetAddress("Kultursaal");
    appointmentSaalkonzert2016.setPostalCode("39057");
    appointmentSaalkonzert2016.setCity("St. Michael");
    appointmentSaalkonzert2016.setName("Saalkonzert 2016");

    offAppSaalkonzert2016 = new OfficialAppointment();
    offAppSaalkonzert2016.setOrganizerName("Organisator");
    offAppSaalkonzert2016.setOrganizerAddress("Teststraße 1, 1234 XYZ");
    offAppSaalkonzert2016.setIsHeadQuota(true);
    appointmentSaalkonzert2016.setOfficialAppointment(offAppSaalkonzert2016);

    appointmentAdventkonzert2016 = new Appointment();
    appointmentAdventkonzert2016.setBeginDate(
      ZonedDateTime.parse("18.12.2016 18:00 Europe/Vienna", formatter));
    appointmentAdventkonzert2016.setEndDate(
      ZonedDateTime.parse("18.12.2016 20:00 Europe/Vienna", formatter));
    appointmentAdventkonzert2016.setStreetAddress("Teststraße 123");      // Test ß
    appointmentAdventkonzert2016.setPostalCode("9876");
    appointmentAdventkonzert2016.setCity("Teststädtchen");                // Test Umlaut
    appointmentAdventkonzert2016.setName("Adventskonzert 2016");

    offAppAdventkonzert2016 = new OfficialAppointment();
    offAppAdventkonzert2016.setOrganizerName("Organisator");
    offAppAdventkonzert2016.setOrganizerAddress("Teststraße 1");
    appointmentAdventkonzert2016.setOfficialAppointment(offAppAdventkonzert2016);
  }


  @Test(expected = NotAnOfficialAppointmentException.class)
  public void testVeranstaltungShouldThrowNotAnOfficialAppointmentException()
      throws Exception {
    appointmentSaalkonzert2016.setOfficialAppointment(null);
    sut.createVeranstaltung(appointmentSaalkonzert2016);
    Assert.fail();
  }


  @Test(expected = IncompleteAppointmentException.class)
  public void testVeranstaltungShouldThrowIncompleteAppointmentException()
      throws Exception {
    offAppSaalkonzert2016.setOrganizerName(null);
    sut.createVeranstaltung(appointmentSaalkonzert2016);
    Assert.fail();
  }


  @Test
  public void testGetAKMJahr() throws Exception {
    Veranstaltung veranstaltungFeb = sut.createVeranstaltung(appointmentSaalkonzert2016);
    Veranstaltung veranstaltungDez = sut.createVeranstaltung(appointmentAdventkonzert2016);
    Assert.assertThat(veranstaltungFeb.getAkmJahr(), Matchers.equalTo(2016));
    Assert.assertThat(veranstaltungDez.getAkmJahr(), Matchers.equalTo(2017));
  }


  @Test
  public void testGetOrt() throws Exception {
    appointmentSaalkonzert2016.setStreetAddress("Kultursaal");
    appointmentSaalkonzert2016.setPostalCode("39057");
    appointmentSaalkonzert2016.setCity("St. Michael");

    Veranstaltung veranstaltungFeb = sut.createVeranstaltung(appointmentSaalkonzert2016);

    Assert.assertThat(veranstaltungFeb.getOrt(),
        Matchers.equalTo("Kultursaal, 39057 St. Michael"));

    appointmentSaalkonzert2016.setPostalCode(null);
    veranstaltungFeb = sut.createVeranstaltung(appointmentSaalkonzert2016);
    Assert.assertThat(veranstaltungFeb.getOrt(),
        Matchers.equalTo("Kultursaal, St. Michael"));

    appointmentSaalkonzert2016.setStreetAddress(null);
    veranstaltungFeb = sut.createVeranstaltung(appointmentSaalkonzert2016);
    Assert.assertThat(veranstaltungFeb.getOrt(),
        Matchers.equalTo("St. Michael"));
  }


  @Test
  public void testDateAndTime() throws Exception {
    Veranstaltung veranstaltungFeb = sut.createVeranstaltung(appointmentSaalkonzert2016);
    // BeginDate: 20.02.2016 20:00 Europe/Vienna
    // EndDate: 20.02.2016 22:00 Europe/Vienna
    Assert.assertThat(veranstaltungFeb.getDatum(), Matchers.equalTo("2016-02-20"));
    Assert.assertThat(veranstaltungFeb.getVon(), Matchers.equalTo("20:00"));
    Assert.assertThat(veranstaltungFeb.getBis(), Matchers.equalTo("22:00"));
  }


  @Test
  public void testOfficialAppointmentInfo() throws Exception {
    // offAppAdventkonzert2016.setOrganizerName("Organisator");
    // offAppAdventkonzert2016.setOrganizerAddress("Teststraße 1");
    Veranstaltung veranstaltungDez = sut.createVeranstaltung(appointmentAdventkonzert2016);
    Assert.assertThat(veranstaltungDez.getVeranstalter(),
        Matchers.equalTo("Organisator"));
    Assert.assertThat(veranstaltungDez.getVeranstalterAdresse(),
        Matchers.equalTo("Teststraße 1"));
  }


  @Test
  public void testVeranstaltungMarshal() throws Exception {
    JAXBContext jc = JAXBContext.newInstance(Veranstaltung.class);
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();

    // workaround for getting rid of standalone="yes"
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    xmlStream.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n".getBytes());

    Veranstaltung veranstaltungDez = sut.createVeranstaltung(appointmentAdventkonzert2016);
    marshaller.marshal(veranstaltungDez, xmlStream);

    String result = xmlStream.toString("ISO-8859-1");
//    System.out.println(result);

    String veranstaltungXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
        + "<veranstaltung>\n"
        + "    <akv_name>Adventskonzert 2016</akv_name>\n"
        + "    <akv_datum>2016-12-18</akv_datum>\n"
        + "    <akv_von>18:00</akv_von>\n"
        + "    <akv_bis>20:00</akv_bis>\n"
        + "    <akv_ort>Teststraße 123, 9876 Teststädtchen</akv_ort>\n"
        + "    <akv_veranstalter>Organisator</akv_veranstalter>\n"
        + "    <akv_veranstalter_adresse>Teststraße 1</akv_veranstalter_adresse>\n"
        + "    <akv_kopfquote>0</akv_kopfquote>\n"
        + "</veranstaltung>";
    Assert.assertThat(result, Matchers.equalToIgnoringWhiteSpace(veranstaltungXML));
  }

}
