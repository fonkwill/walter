package at.simianarmy.export.akm;

import at.simianarmy.WalterApp;
import at.simianarmy.domain.Appointment;
import at.simianarmy.domain.Composition;
import at.simianarmy.domain.OfficialAppointment;
import at.simianarmy.export.akm.xml.AkmMeldung;
import at.simianarmy.export.akm.xml.AkmReport;
import at.simianarmy.export.akm.xml.Allgemeines;
import at.simianarmy.export.akm.xml.Musikstueck;
import at.simianarmy.export.akm.xml.Veranstaltung;
import at.simianarmy.repository.ArrangerRepository;
import at.simianarmy.repository.ComposerRepository;
import at.simianarmy.service.CustomizationService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalterApp.class)
public class AkmReportFactoryTest {

  private static final String REFERENCE_XML_URL = "/akmreport/akmReport_testReference.xml";

  private Set<Appointment> appointments;


  @Mock
  private ComposerRepository composerRepository;

  @Mock
  private ArrangerRepository arrangerRepository;

  private VeranstaltungFactoryTest veranstaltungFactoryTest = new VeranstaltungFactoryTest();


  private MusikstueckFactoryTest musikstueckTest = new MusikstueckFactoryTest();

  @InjectMocks
  private MusikstueckFactory musikstueckFactory;
  
  @Mock
  private CustomizationService customizationService;

  @Inject
  @InjectMocks
  private AkmReportFactory sut;

  private Allgemeines vereinsdaten;

  private Appointment adventkonzert2015Appointment;
  private Appointment saalkonzert2016Appointment;
  private Appointment adventkonzert2016Appointment;


  @Before
  public void setUp() throws Exception {
	MockitoAnnotations.initMocks(this);
	  
    musikstueckTest.setUp();
    veranstaltungFactoryTest.setUp();

    appointments = new HashSet<>();

    Set<Composition> compositionsKonzert = new HashSet<>();
    compositionsKonzert.add(musikstueckTest.adagioComposition);
    compositionsKonzert.add(musikstueckTest.piratesComposition);
    saalkonzert2016Appointment = veranstaltungFactoryTest.appointmentSaalkonzert2016;
    saalkonzert2016Appointment.getOfficialAppointment()
      .setCompositions(compositionsKonzert);

    Set<Composition> compositionsAdvent = new HashSet<>();
    compositionsAdvent.add(musikstueckTest.adagioComposition);
    adventkonzert2015Appointment = veranstaltungFactoryTest.appointmentAdventkonzert2015;
    adventkonzert2015Appointment.getOfficialAppointment()
      .setCompositions(compositionsAdvent);

    adventkonzert2016Appointment = veranstaltungFactoryTest.appointmentAdventkonzert2016;
    adventkonzert2016Appointment.getOfficialAppointment()
      .setCompositions(compositionsAdvent);

    // FIXME there should be some other way to do this
    sut.musikstueckFactory = this.musikstueckFactory;
    musikstueckTest.initMocks(arrangerRepository, composerRepository);
    
    vereinsdaten = new Allgemeines();
    vereinsdaten.setAdrOrt("Testort");
    vereinsdaten.setAdrPlz("1234");
    vereinsdaten.setAdrStrasse("Teststraße 123");
    vereinsdaten.setAusstellerAdresse("Straße Nr, Plz Ort");
    vereinsdaten.setAusstellerName("Vorname Zuname");
    vereinsdaten.setVerId("KA-22-022");
    vereinsdaten.setVerName("Marktmusikkapelle XY");
    
    Mockito.when(customizationService.getAKMData()).thenReturn(vereinsdaten);
    
    
  }


  @Test
  public void testIfNonOfficialAppointmentsAreIgnored() throws Exception {
    appointments.add(adventkonzert2015Appointment);

    int oldAppCount = appointments.size();
    Assert.assertThat(oldAppCount, Matchers.greaterThan(0));

    // inofficialAppointment has no associated OfficialAppointment, should be ignored in the report
    Appointment inofficialAppointment = new Appointment();
    appointments.add(inofficialAppointment);

    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);

    Assert.assertThat(akmMeldung.getVeranstaltungen().size(), Matchers.equalTo(oldAppCount));
  }


  @Test(expected = IncompleteAppointmentException.class)
  public void testAkmMeldungShouldThrowIncompleteAppointmentException() throws Exception {
    OfficialAppointment incompleteOfficialAppointment = new OfficialAppointment();
    Appointment incompleteAppointment = new Appointment();
    incompleteAppointment.setOfficialAppointment(incompleteOfficialAppointment);
    appointments.add(incompleteAppointment);

    sut.createAkmMeldung(appointments);

    Assert.fail();
  }


  @Test
  public void testGetVereinsID() throws Exception {
    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);
    Assert.assertThat(akmMeldung.getVereinsID(), Matchers.equalTo("KA-22-022"));
  }


  @Test
  public void testGetVeranstaltungen() throws Exception {
    appointments.add(adventkonzert2015Appointment);
    appointments.add(saalkonzert2016Appointment);
    appointments.add(adventkonzert2016Appointment);

    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);

    SortedSet<Veranstaltung> veranstaltungen = akmMeldung.getVeranstaltungen();
    Assert.assertThat(veranstaltungen.size(), Matchers.greaterThan(1));

    // test correct order (by date/time => compare strings, format is yyyy-MM-dd or HH:mm)
    Iterator<Veranstaltung> iter = veranstaltungen.iterator();
    Veranstaltung earlier = iter.next();
    while (iter.hasNext()) {
      Veranstaltung later = iter.next();
      Assert.assertThat(later.getDatum(), Matchers.greaterThanOrEqualTo(earlier.getDatum()));
      if (later.getDatum().equals(earlier.getDatum())) {
        Assert.assertThat(later.getVon(), Matchers.greaterThanOrEqualTo(earlier.getVon()));
      }
      earlier = later;
    }
  }


  @Test
  public void testBillingSameYear() throws Exception {
    appointments.add(saalkonzert2016Appointment);

    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm z");
    ZonedDateTime dateTime = ZonedDateTime.parse("20.02.2016 20:00 Europe/Vienna", formatter);
    // this is just to ensure correct initialization
    Assert.assertThat(saalkonzert2016Appointment.getBeginDate(), Matchers.equalTo(dateTime));

    // February 2016 is billed in 2016
    for (Musikstueck musikstueck : akmMeldung.getMusikstuecke()) {
      Assert.assertThat(musikstueck.getJahr(), Matchers.equalTo(2016));
    }
  }

  @Test
  public void testBillingNextYear() throws Exception {
    appointments.add(adventkonzert2016Appointment);

    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm z");
    ZonedDateTime dateTime = ZonedDateTime.parse("18.12.2016 18:00 Europe/Vienna", formatter);
    // this is just to ensure correct initialization
    Assert.assertThat(adventkonzert2016Appointment.getBeginDate(), Matchers.equalTo(dateTime));

    // December 2016 is billed in 2017
    for (Musikstueck musikstueck : akmMeldung.getMusikstuecke()) {
      Assert.assertThat(musikstueck.getJahr(), Matchers.equalTo(2017));
    }
  }


  @Test
  public void testGetMusikStuecke() throws Exception {
    appointments.add(adventkonzert2015Appointment);
    appointments.add(saalkonzert2016Appointment);
    appointments.add(adventkonzert2016Appointment);

    AkmMeldung akmMeldung = sut.createAkmMeldung(appointments);

    List<Musikstueck> musikstuecke = akmMeldung.getMusikstuecke();
    Assert.assertThat(musikstuecke.size(), Matchers.greaterThan(1));

    // test correct order (by aks_jahr, then by id)
    Musikstueck previous = null;
    for (Musikstueck musikstueck : musikstuecke) {
      // must have a composer (because of initialization, not in general)
      Assert.assertThat(musikstueck.getKomponist(), Matchers.not(""));
      // must been played at least once
      Assert.assertThat(musikstueck.getAnz(), Matchers.greaterThan(0));
      if (previous != null) {
        Assert.assertThat(musikstueck.getJahr(), Matchers.greaterThanOrEqualTo(previous.getJahr()));
        if (musikstueck.getJahr() == previous.getJahr()) {
          Assert.assertThat(musikstueck.getId(), Matchers.greaterThanOrEqualTo(previous.getId()));
        }
      }
      previous = musikstueck;
    }
  }


  @Test
  public void testAkmMeldungMarshal() throws Exception {
    JAXBContext jc = JAXBContext.newInstance(AkmReport.class);
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    ByteArrayOutputStream xmlStream = new ByteArrayOutputStream();

    // workaround for getting rid of standalone="yes"
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    xmlStream.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n".getBytes());

    appointments.add(adventkonzert2015Appointment);
    appointments.add(saalkonzert2016Appointment);
    appointments.add(adventkonzert2016Appointment);

    AkmReport akmReport = sut.createAkmReport(appointments);
    marshaller.marshal(akmReport, xmlStream);

    String result = xmlStream.toString("ISO-8859-1");
//    System.out.println(result);

    // read reference file
    File referenceFile = new File(this.getClass().getResource(REFERENCE_XML_URL).getPath());
    String reference = FileUtils.readFileToString(referenceFile, "ISO-8859-1");

    Assert.assertThat(result, Matchers.equalToIgnoringWhiteSpace(reference));
  }

}
