--
-- PostgreSQL database dump
--

-- Dumped from database version 11.0
-- Dumped by pg_dump version 11.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.address (
    id bigint NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    state character varying(255) NOT NULL
);


ALTER TABLE public.address OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO postgres;

--
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;


--
-- Name: appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    begin_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone NOT NULL,
    official_appointment_id bigint,
    type_id bigint
);


ALTER TABLE public.appointment OWNER TO postgres;

--
-- Name: appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointment_id_seq OWNER TO postgres;

--
-- Name: appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointment_id_seq OWNED BY public.appointment.id;


--
-- Name: appointment_persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment_persons (
    persons_id bigint NOT NULL,
    appointments_id bigint NOT NULL
);


ALTER TABLE public.appointment_persons OWNER TO postgres;

--
-- Name: appointment_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.appointment_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    is_official boolean NOT NULL,
    person_group_id bigint
);


ALTER TABLE public.appointment_type OWNER TO postgres;

--
-- Name: appointment_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.appointment_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointment_type_id_seq OWNER TO postgres;

--
-- Name: appointment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.appointment_type_id_seq OWNED BY public.appointment_type.id;


--
-- Name: arranger; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arranger (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.arranger OWNER TO postgres;

--
-- Name: arranger_compositions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arranger_compositions (
    compositions_id bigint NOT NULL,
    arrangers_id bigint NOT NULL
);


ALTER TABLE public.arranger_compositions OWNER TO postgres;

--
-- Name: arranger_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arranger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arranger_id_seq OWNER TO postgres;

--
-- Name: arranger_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arranger_id_seq OWNED BY public.arranger.id;


--
-- Name: award; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.award (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    descripton character varying(255)
);


ALTER TABLE public.award OWNER TO postgres;

--
-- Name: award_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.award_id_seq OWNER TO postgres;

--
-- Name: award_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.award_id_seq OWNED BY public.award.id;


--
-- Name: bank_import_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bank_import_data (
    id bigint NOT NULL,
    entry_date date NOT NULL,
    entry_value numeric(10,2) NOT NULL,
    entry_text character varying(255),
    partner_name character varying(255),
    entry_reference bytea NOT NULL,
    cashbook_entry_id bigint,
    cashbook_category_id bigint,
    cashbook_account_id bigint NOT NULL,
    cashbook_account_id_from bigint,
    cashbook_account_id_to bigint,
    is_transfer boolean DEFAULT false NOT NULL
);


ALTER TABLE public.bank_import_data OWNER TO postgres;

--
-- Name: bank_import_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_import_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_import_data_id_seq OWNER TO postgres;

--
-- Name: bank_import_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bank_import_data_id_seq OWNED BY public.bank_import_data.id;


--
-- Name: cashbook; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.cashbook OWNER TO postgres;

--
-- Name: cashbook_account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_account (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    bank_account boolean,
    bank_importer_type character varying(255),
    receipt_code character varying(255) NOT NULL,
    current_number integer DEFAULT 0 NOT NULL,
    text_for_auto_transfer_from character varying(255),
    text_for_auto_transfer_to character varying(255),
    default_for_cashbook boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cashbook_account OWNER TO postgres;

--
-- Name: cashbook_account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_account_id_seq OWNER TO postgres;

--
-- Name: cashbook_account_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_account_id_seq OWNED BY public.cashbook_account.id;


--
-- Name: cashbook_cashbook_entries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_cashbook_entries (
    cashbook_entries_id bigint NOT NULL,
    cashbooks_id bigint NOT NULL
);


ALTER TABLE public.cashbook_cashbook_entries OWNER TO postgres;

--
-- Name: cashbook_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_category (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    color character varying(255) NOT NULL,
    parent_id bigint
);


ALTER TABLE public.cashbook_category OWNER TO postgres;

--
-- Name: cashbook_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_category_id_seq OWNER TO postgres;

--
-- Name: cashbook_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_category_id_seq OWNED BY public.cashbook_category.id;


--
-- Name: cashbook_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cashbook_entry (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    date date NOT NULL,
    amount numeric(10,2) NOT NULL,
    appointment_id bigint,
    cashbook_category_id bigint,
    receipt_id bigint,
    cashbook_account_id bigint NOT NULL
);


ALTER TABLE public.cashbook_entry OWNER TO postgres;

--
-- Name: cashbook_entry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_entry_id_seq OWNER TO postgres;

--
-- Name: cashbook_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_entry_id_seq OWNED BY public.cashbook_entry.id;


--
-- Name: cashbook_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cashbook_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cashbook_id_seq OWNER TO postgres;

--
-- Name: cashbook_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cashbook_id_seq OWNED BY public.cashbook.id;


--
-- Name: clothing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing (
    id bigint NOT NULL,
    size character varying(255),
    purchase_date date,
    not_available boolean,
    type_id bigint
);


ALTER TABLE public.clothing OWNER TO postgres;

--
-- Name: clothing_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing_group (
    id bigint NOT NULL,
    size character varying(255),
    quantity integer,
    quantity_available integer,
    type_id bigint
);


ALTER TABLE public.clothing_group OWNER TO postgres;

--
-- Name: clothing_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_group_id_seq OWNER TO postgres;

--
-- Name: clothing_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_group_id_seq OWNED BY public.clothing_group.id;


--
-- Name: clothing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_id_seq OWNER TO postgres;

--
-- Name: clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_id_seq OWNED BY public.clothing.id;


--
-- Name: clothing_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clothing_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.clothing_type OWNER TO postgres;

--
-- Name: clothing_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clothing_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clothing_type_id_seq OWNER TO postgres;

--
-- Name: clothing_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clothing_type_id_seq OWNED BY public.clothing_type.id;


--
-- Name: color_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.color_code (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.color_code OWNER TO postgres;

--
-- Name: color_code_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.color_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.color_code_id_seq OWNER TO postgres;

--
-- Name: color_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.color_code_id_seq OWNED BY public.color_code.id;


--
-- Name: column_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.column_config (
    id bigint NOT NULL,
    entity character varying(255) NOT NULL,
    column_name character varying(255) NOT NULL,
    default_visible boolean NOT NULL,
    default_position integer NOT NULL,
    column_display_name character varying(255) NOT NULL,
    sort_by_name character varying(255) NOT NULL
);


ALTER TABLE public.column_config OWNER TO postgres;

--
-- Name: column_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.column_config_id_seq OWNER TO postgres;

--
-- Name: column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.column_config_id_seq OWNED BY public.column_config.id;


--
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    street_address character varying(255),
    postal_code character varying(255),
    city character varying(255),
    country character varying(255),
    recipient character varying(255)
);


ALTER TABLE public.company OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_id_seq OWNER TO postgres;

--
-- Name: company_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;


--
-- Name: company_person_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company_person_groups (
    person_groups_id bigint NOT NULL,
    companies_id bigint NOT NULL
);


ALTER TABLE public.company_person_groups OWNER TO postgres;

--
-- Name: composer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composer (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.composer OWNER TO postgres;

--
-- Name: composer_compositions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composer_compositions (
    compositions_id bigint NOT NULL,
    composers_id bigint NOT NULL
);


ALTER TABLE public.composer_compositions OWNER TO postgres;

--
-- Name: composer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.composer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.composer_id_seq OWNER TO postgres;

--
-- Name: composer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.composer_id_seq OWNED BY public.composer.id;


--
-- Name: composition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composition (
    id bigint NOT NULL,
    nr integer NOT NULL,
    title character varying(255) NOT NULL,
    note character varying(255),
    akm_composition_nr integer NOT NULL,
    cashbook_entry_id bigint,
    ordering_company_id bigint,
    publisher_id bigint,
    genre_id bigint,
    music_book_id bigint,
    color_code_id bigint NOT NULL
);


ALTER TABLE public.composition OWNER TO postgres;

--
-- Name: composition_appointments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.composition_appointments (
    appointments_id bigint NOT NULL,
    compositions_id bigint NOT NULL
);


ALTER TABLE public.composition_appointments OWNER TO postgres;

--
-- Name: composition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.composition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.composition_id_seq OWNER TO postgres;

--
-- Name: composition_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.composition_id_seq OWNED BY public.composition.id;


--
-- Name: customization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customization (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    data character varying(255) NOT NULL,
    text character varying(255)
);


ALTER TABLE public.customization OWNER TO postgres;

--
-- Name: customization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customization_id_seq OWNER TO postgres;

--
-- Name: customization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customization_id_seq OWNED BY public.customization.id;


--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- Name: email_verification_link; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.email_verification_link (
    id bigint NOT NULL,
    secret_key character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    validation_date timestamp without time zone,
    invalid boolean NOT NULL,
    person_id bigint NOT NULL
);


ALTER TABLE public.email_verification_link OWNER TO postgres;

--
-- Name: email_verification_link_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.email_verification_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_verification_link_id_seq OWNER TO postgres;

--
-- Name: email_verification_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.email_verification_link_id_seq OWNED BY public.email_verification_link.id;


--
-- Name: file_store_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.file_store_metadata (
    id uuid NOT NULL,
    extension character varying(255),
    original_file character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    storage character varying(255) NOT NULL,
    parent_id uuid,
    created_at timestamp without time zone NOT NULL,
    size bigint
);


ALTER TABLE public.file_store_metadata OWNER TO postgres;

--
-- Name: filter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filter (
    id bigint NOT NULL,
    list_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    filter character varying(255) NOT NULL
);


ALTER TABLE public.filter OWNER TO postgres;

--
-- Name: filter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filter_id_seq OWNER TO postgres;

--
-- Name: filter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filter_id_seq OWNED BY public.filter.id;


--
-- Name: genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genre (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.genre OWNER TO postgres;

--
-- Name: genre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_id_seq OWNER TO postgres;

--
-- Name: genre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genre_id_seq OWNED BY public.genre.id;


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- Name: honor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.honor (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.honor OWNER TO postgres;

--
-- Name: honor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.honor_id_seq OWNER TO postgres;

--
-- Name: honor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.honor_id_seq OWNED BY public.honor.id;


--
-- Name: instrument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    purchase_date date,
    private_instrument boolean,
    price numeric(10,2),
    type_id bigint NOT NULL,
    cashbook_entry_id bigint
);


ALTER TABLE public.instrument OWNER TO postgres;

--
-- Name: instrument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_id_seq OWNER TO postgres;

--
-- Name: instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_id_seq OWNED BY public.instrument.id;


--
-- Name: instrument_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument_service (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    cashbook_entry_id bigint,
    instrument_id bigint NOT NULL
);


ALTER TABLE public.instrument_service OWNER TO postgres;

--
-- Name: instrument_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_service_id_seq OWNER TO postgres;

--
-- Name: instrument_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_service_id_seq OWNED BY public.instrument_service.id;


--
-- Name: instrument_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instrument_type (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.instrument_type OWNER TO postgres;

--
-- Name: instrument_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instrument_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instrument_type_id_seq OWNER TO postgres;

--
-- Name: instrument_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instrument_type_id_seq OWNED BY public.instrument_type.id;


--
-- Name: jhi_authority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_authority (
    name character varying(50) NOT NULL,
    role_id bigint NOT NULL,
    id bigint NOT NULL,
    write boolean
);


ALTER TABLE public.jhi_authority OWNER TO postgres;

--
-- Name: jhi_authority_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_authority_id_seq OWNER TO postgres;

--
-- Name: jhi_authority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_authority_id_seq OWNED BY public.jhi_authority.id;


--
-- Name: jhi_persistent_audit_event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_persistent_audit_event (
    event_id bigint NOT NULL,
    principal character varying(50) NOT NULL,
    event_date timestamp without time zone,
    event_type character varying(255)
);


ALTER TABLE public.jhi_persistent_audit_event OWNER TO postgres;

--
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_persistent_audit_event_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_persistent_audit_event_event_id_seq OWNER TO postgres;

--
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_persistent_audit_event_event_id_seq OWNED BY public.jhi_persistent_audit_event.event_id;


--
-- Name: jhi_persistent_audit_evt_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_persistent_audit_evt_data (
    event_id bigint NOT NULL,
    name character varying(150) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.jhi_persistent_audit_evt_data OWNER TO postgres;

--
-- Name: jhi_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_user (
    id bigint NOT NULL,
    login character varying(50) NOT NULL,
    password_hash character varying(60),
    first_name character varying(50),
    last_name character varying(50),
    email character varying(100),
    activated boolean NOT NULL,
    lang_key character varying(5),
    activation_key character varying(20),
    reset_key character varying(20),
    created_by character varying(50) NOT NULL,
    created_date timestamp without time zone NOT NULL,
    reset_date timestamp without time zone,
    last_modified_by character varying(50),
    last_modified_date timestamp without time zone
);


ALTER TABLE public.jhi_user OWNER TO postgres;

--
-- Name: jhi_user_authority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jhi_user_authority (
    user_id bigint NOT NULL,
    role_id bigint DEFAULT '2'::bigint NOT NULL
);


ALTER TABLE public.jhi_user_authority OWNER TO postgres;

--
-- Name: jhi_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jhi_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jhi_user_id_seq OWNER TO postgres;

--
-- Name: jhi_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jhi_user_id_seq OWNED BY public.jhi_user.id;


--
-- Name: letter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.letter (
    id bigint NOT NULL,
    title character varying(255),
    text text,
    payment boolean,
    current_period boolean,
    previous_period boolean,
    template_id bigint,
    letter_type character varying(255) NOT NULL,
    subject character varying(255),
    mail_text character varying(255),
    attachment_file_name character varying(255)
);


ALTER TABLE public.letter OWNER TO postgres;

--
-- Name: letter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.letter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.letter_id_seq OWNER TO postgres;

--
-- Name: letter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.letter_id_seq OWNED BY public.letter.id;


--
-- Name: letter_person_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.letter_person_group (
    person_groups_id bigint NOT NULL,
    letters_id bigint NOT NULL
);


ALTER TABLE public.letter_person_group OWNER TO postgres;

--
-- Name: mail_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mail_log (
    id bigint NOT NULL,
    date timestamp without time zone NOT NULL,
    email character varying(255) NOT NULL,
    subject character varying(255) NOT NULL,
    mail_log_status character varying(255) NOT NULL,
    error_message character varying(255)
);


ALTER TABLE public.mail_log OWNER TO postgres;

--
-- Name: mail_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mail_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mail_log_id_seq OWNER TO postgres;

--
-- Name: mail_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mail_log_id_seq OWNED BY public.mail_log.id;


--
-- Name: membership; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    person_id bigint,
    membership_fee_id bigint
);


ALTER TABLE public.membership OWNER TO postgres;

--
-- Name: membership_fee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    period character varying(255) NOT NULL,
    period_time_fraction integer
);


ALTER TABLE public.membership_fee OWNER TO postgres;

--
-- Name: membership_fee_amount; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee_amount (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    amount numeric(10,2) NOT NULL,
    membership_fee_id bigint
);


ALTER TABLE public.membership_fee_amount OWNER TO postgres;

--
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_amount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_amount_id_seq OWNER TO postgres;

--
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_amount_id_seq OWNED BY public.membership_fee_amount.id;


--
-- Name: membership_fee_for_period; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.membership_fee_for_period (
    id bigint NOT NULL,
    nr integer NOT NULL,
    jhi_year integer NOT NULL,
    reference_code character varying(255),
    paid boolean,
    note character varying(255),
    cashbook_entry_id bigint,
    membership_id bigint
);


ALTER TABLE public.membership_fee_for_period OWNER TO postgres;

--
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_for_period_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_for_period_id_seq OWNER TO postgres;

--
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_for_period_id_seq OWNED BY public.membership_fee_for_period.id;


--
-- Name: membership_fee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_fee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_fee_id_seq OWNER TO postgres;

--
-- Name: membership_fee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_fee_id_seq OWNED BY public.membership_fee.id;


--
-- Name: membership_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.membership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.membership_id_seq OWNER TO postgres;

--
-- Name: membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.membership_id_seq OWNED BY public.membership.id;


--
-- Name: music_book; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.music_book (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255)
);


ALTER TABLE public.music_book OWNER TO postgres;

--
-- Name: music_book_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.music_book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.music_book_id_seq OWNER TO postgres;

--
-- Name: music_book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.music_book_id_seq OWNED BY public.music_book.id;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification (
    id bigint NOT NULL,
    entity_id bigint,
    created_at timestamp without time zone NOT NULL,
    read_by_user_id bigint,
    notification_rule_id bigint
);


ALTER TABLE public.notification OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_id_seq OWNER TO postgres;

--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- Name: notification_rule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_rule (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    entity character varying(255) NOT NULL,
    conditions text NOT NULL,
    active boolean NOT NULL,
    message_group character varying(255) NOT NULL,
    message_single character varying(255) NOT NULL
);


ALTER TABLE public.notification_rule OWNER TO postgres;

--
-- Name: notification_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_rule_id_seq OWNER TO postgres;

--
-- Name: notification_rule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notification_rule_id_seq OWNED BY public.notification_rule.id;


--
-- Name: notification_rule_target_audiences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification_rule_target_audiences (
    notification_rules_id bigint NOT NULL,
    target_role_id bigint NOT NULL
);


ALTER TABLE public.notification_rule_target_audiences OWNER TO postgres;

--
-- Name: official_appointment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.official_appointment (
    id bigint NOT NULL,
    organizer_name character varying(255) NOT NULL,
    organizer_address character varying(255) NOT NULL,
    is_head_quota boolean NOT NULL,
    report_id bigint
);


ALTER TABLE public.official_appointment OWNER TO postgres;

--
-- Name: official_appointment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.official_appointment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.official_appointment_id_seq OWNER TO postgres;

--
-- Name: official_appointment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.official_appointment_id_seq OWNED BY public.official_appointment.id;


--
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    id bigint NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    birth_date date,
    email character varying(255),
    telephone_number character varying(255),
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    gender character varying(255),
    email_type character varying(255) DEFAULT 'MANUALLY'::character varying NOT NULL,
    has_direct_debit boolean DEFAULT false NOT NULL,
    date_lost_relevance date
);


ALTER TABLE public.person OWNER TO postgres;

--
-- Name: person_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_address (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date NOT NULL,
    address_id bigint,
    person_id bigint
);


ALTER TABLE public.person_address OWNER TO postgres;

--
-- Name: person_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_address_id_seq OWNER TO postgres;

--
-- Name: person_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_address_id_seq OWNED BY public.person_address.id;


--
-- Name: person_award; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_award (
    id bigint NOT NULL,
    date date NOT NULL,
    award_id bigint,
    person_id bigint
);


ALTER TABLE public.person_award OWNER TO postgres;

--
-- Name: person_award_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_award_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_award_id_seq OWNER TO postgres;

--
-- Name: person_award_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_award_id_seq OWNED BY public.person_award.id;


--
-- Name: person_clothing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_clothing (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    clothing_id bigint,
    person_id bigint
);


ALTER TABLE public.person_clothing OWNER TO postgres;

--
-- Name: person_clothing_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_clothing_id_seq OWNER TO postgres;

--
-- Name: person_clothing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_clothing_id_seq OWNED BY public.person_clothing.id;


--
-- Name: person_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_group (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    parent_id bigint,
    has_person_relevance boolean DEFAULT false NOT NULL
);


ALTER TABLE public.person_group OWNER TO postgres;

--
-- Name: person_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_group_id_seq OWNER TO postgres;

--
-- Name: person_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_group_id_seq OWNED BY public.person_group.id;


--
-- Name: person_honor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_honor (
    id bigint NOT NULL,
    date date NOT NULL,
    honor_id bigint,
    person_id bigint
);


ALTER TABLE public.person_honor OWNER TO postgres;

--
-- Name: person_honor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_honor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_honor_id_seq OWNER TO postgres;

--
-- Name: person_honor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_honor_id_seq OWNED BY public.person_honor.id;


--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_id_seq OWNER TO postgres;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: person_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_info (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.person_info OWNER TO postgres;

--
-- Name: person_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_info_id_seq OWNER TO postgres;

--
-- Name: person_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_info_id_seq OWNED BY public.person_info.id;


--
-- Name: person_instrument; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_instrument (
    id bigint NOT NULL,
    begin_date date NOT NULL,
    end_date date,
    instrument_id bigint,
    person_id bigint
);


ALTER TABLE public.person_instrument OWNER TO postgres;

--
-- Name: person_instrument_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_instrument_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_instrument_id_seq OWNER TO postgres;

--
-- Name: person_instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_instrument_id_seq OWNED BY public.person_instrument.id;


--
-- Name: person_person_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person_person_groups (
    person_groups_id bigint NOT NULL,
    people_id bigint NOT NULL
);


ALTER TABLE public.person_person_groups OWNER TO postgres;

--
-- Name: receipt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.receipt (
    id bigint NOT NULL,
    date date NOT NULL,
    note character varying(255),
    title character varying(255) NOT NULL,
    file character varying(255),
    identifier character varying(255),
    company_id bigint,
    overwrite_identifier boolean DEFAULT false NOT NULL,
    initial_cashbook_entry_id bigint NOT NULL
);


ALTER TABLE public.receipt OWNER TO postgres;

--
-- Name: receipt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.receipt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.receipt_id_seq OWNER TO postgres;

--
-- Name: receipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.receipt_id_seq OWNED BY public.receipt.id;


--
-- Name: report; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.report (
    id bigint NOT NULL,
    generation_date timestamp without time zone NOT NULL,
    association_id character varying(255) NOT NULL,
    association_name character varying(255) NOT NULL,
    street_address character varying(255) NOT NULL,
    postal_code character varying(255) NOT NULL,
    city character varying(255) NOT NULL
);


ALTER TABLE public.report OWNER TO postgres;

--
-- Name: report_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_id_seq OWNER TO postgres;

--
-- Name: report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.report_id_seq OWNED BY public.report.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.template (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    file character varying(255),
    margin_left real,
    margin_right real,
    margin_top real,
    margin_bottom real
);


ALTER TABLE public.template OWNER TO postgres;

--
-- Name: template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.template_id_seq OWNER TO postgres;

--
-- Name: template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.template_id_seq OWNED BY public.template.id;


--
-- Name: user_column_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_column_config (
    id bigint NOT NULL,
    visible boolean NOT NULL,
    "position" integer NOT NULL,
    column_config_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.user_column_config OWNER TO postgres;

--
-- Name: user_column_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_column_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_column_config_id_seq OWNER TO postgres;

--
-- Name: user_column_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_column_config_id_seq OWNED BY public.user_column_config.id;


--
-- Name: address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);


--
-- Name: appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment ALTER COLUMN id SET DEFAULT nextval('public.appointment_id_seq'::regclass);


--
-- Name: appointment_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type ALTER COLUMN id SET DEFAULT nextval('public.appointment_type_id_seq'::regclass);


--
-- Name: arranger id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger ALTER COLUMN id SET DEFAULT nextval('public.arranger_id_seq'::regclass);


--
-- Name: award id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.award ALTER COLUMN id SET DEFAULT nextval('public.award_id_seq'::regclass);


--
-- Name: bank_import_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data ALTER COLUMN id SET DEFAULT nextval('public.bank_import_data_id_seq'::regclass);


--
-- Name: cashbook id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook ALTER COLUMN id SET DEFAULT nextval('public.cashbook_id_seq'::regclass);


--
-- Name: cashbook_account id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account ALTER COLUMN id SET DEFAULT nextval('public.cashbook_account_id_seq'::regclass);


--
-- Name: cashbook_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category ALTER COLUMN id SET DEFAULT nextval('public.cashbook_category_id_seq'::regclass);


--
-- Name: cashbook_entry id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry ALTER COLUMN id SET DEFAULT nextval('public.cashbook_entry_id_seq'::regclass);


--
-- Name: clothing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing ALTER COLUMN id SET DEFAULT nextval('public.clothing_id_seq'::regclass);


--
-- Name: clothing_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group ALTER COLUMN id SET DEFAULT nextval('public.clothing_group_id_seq'::regclass);


--
-- Name: clothing_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_type ALTER COLUMN id SET DEFAULT nextval('public.clothing_type_id_seq'::regclass);


--
-- Name: color_code id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code ALTER COLUMN id SET DEFAULT nextval('public.color_code_id_seq'::regclass);


--
-- Name: column_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.column_config ALTER COLUMN id SET DEFAULT nextval('public.column_config_id_seq'::regclass);


--
-- Name: company id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);


--
-- Name: composer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer ALTER COLUMN id SET DEFAULT nextval('public.composer_id_seq'::regclass);


--
-- Name: composition id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition ALTER COLUMN id SET DEFAULT nextval('public.composition_id_seq'::regclass);


--
-- Name: customization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization ALTER COLUMN id SET DEFAULT nextval('public.customization_id_seq'::regclass);


--
-- Name: email_verification_link id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link ALTER COLUMN id SET DEFAULT nextval('public.email_verification_link_id_seq'::regclass);


--
-- Name: filter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter ALTER COLUMN id SET DEFAULT nextval('public.filter_id_seq'::regclass);


--
-- Name: genre id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre ALTER COLUMN id SET DEFAULT nextval('public.genre_id_seq'::regclass);


--
-- Name: honor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.honor ALTER COLUMN id SET DEFAULT nextval('public.honor_id_seq'::regclass);


--
-- Name: instrument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument ALTER COLUMN id SET DEFAULT nextval('public.instrument_id_seq'::regclass);


--
-- Name: instrument_service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service ALTER COLUMN id SET DEFAULT nextval('public.instrument_service_id_seq'::regclass);


--
-- Name: instrument_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_type ALTER COLUMN id SET DEFAULT nextval('public.instrument_type_id_seq'::regclass);


--
-- Name: jhi_authority id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_authority ALTER COLUMN id SET DEFAULT nextval('public.jhi_authority_id_seq'::regclass);


--
-- Name: jhi_persistent_audit_event event_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_event ALTER COLUMN event_id SET DEFAULT nextval('public.jhi_persistent_audit_event_event_id_seq'::regclass);


--
-- Name: jhi_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user ALTER COLUMN id SET DEFAULT nextval('public.jhi_user_id_seq'::regclass);


--
-- Name: letter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter ALTER COLUMN id SET DEFAULT nextval('public.letter_id_seq'::regclass);


--
-- Name: mail_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_log ALTER COLUMN id SET DEFAULT nextval('public.mail_log_id_seq'::regclass);


--
-- Name: membership id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership ALTER COLUMN id SET DEFAULT nextval('public.membership_id_seq'::regclass);


--
-- Name: membership_fee id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_id_seq'::regclass);


--
-- Name: membership_fee_amount id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_amount_id_seq'::regclass);


--
-- Name: membership_fee_for_period id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period ALTER COLUMN id SET DEFAULT nextval('public.membership_fee_for_period_id_seq'::regclass);


--
-- Name: music_book id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.music_book ALTER COLUMN id SET DEFAULT nextval('public.music_book_id_seq'::regclass);


--
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- Name: notification_rule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule ALTER COLUMN id SET DEFAULT nextval('public.notification_rule_id_seq'::regclass);


--
-- Name: official_appointment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment ALTER COLUMN id SET DEFAULT nextval('public.official_appointment_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Name: person_address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address ALTER COLUMN id SET DEFAULT nextval('public.person_address_id_seq'::regclass);


--
-- Name: person_award id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award ALTER COLUMN id SET DEFAULT nextval('public.person_award_id_seq'::regclass);


--
-- Name: person_clothing id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing ALTER COLUMN id SET DEFAULT nextval('public.person_clothing_id_seq'::regclass);


--
-- Name: person_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group ALTER COLUMN id SET DEFAULT nextval('public.person_group_id_seq'::regclass);


--
-- Name: person_honor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor ALTER COLUMN id SET DEFAULT nextval('public.person_honor_id_seq'::regclass);


--
-- Name: person_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_info ALTER COLUMN id SET DEFAULT nextval('public.person_info_id_seq'::regclass);


--
-- Name: person_instrument id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument ALTER COLUMN id SET DEFAULT nextval('public.person_instrument_id_seq'::regclass);


--
-- Name: receipt id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt ALTER COLUMN id SET DEFAULT nextval('public.receipt_id_seq'::regclass);


--
-- Name: report id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report ALTER COLUMN id SET DEFAULT nextval('public.report_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: template id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.template ALTER COLUMN id SET DEFAULT nextval('public.template_id_seq'::regclass);


--
-- Name: user_column_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config ALTER COLUMN id SET DEFAULT nextval('public.user_column_config_id_seq'::regclass);


--
-- Name: 24298; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('24298');


ALTER LARGE OBJECT 24298 OWNER TO postgres;

--
-- Name: 24299; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('24299');


ALTER LARGE OBJECT 24299 OWNER TO postgres;

--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.address (id, street_address, postal_code, city, state) FROM stdin;
\.


--
-- Data for Name: appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment (id, name, description, street_address, postal_code, city, country, begin_date, end_date, official_appointment_id, type_id) FROM stdin;
1	Großer österr. Zapfenstreich	\N	\N	\N	\N	\N	2019-10-26 18:00:00	2019-10-26 20:00:00	\N	2
2	Marschwertung 2019	\N	\N	\N	\N	\N	2019-06-13 09:00:13.185	2019-06-14 09:00:00	\N	1
3	Kameradschaftsabend		\N	\N	\N	\N	2019-11-27 18:00:00	2019-11-28 00:00:00	\N	2
\.


--
-- Data for Name: appointment_persons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment_persons (persons_id, appointments_id) FROM stdin;
\.


--
-- Data for Name: appointment_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.appointment_type (id, name, is_official, person_group_id) FROM stdin;
1	Feste	f	\N
2	Veranstaltung	f	\N
\.


--
-- Data for Name: arranger; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arranger (id, name) FROM stdin;
\.


--
-- Data for Name: arranger_compositions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arranger_compositions (compositions_id, arrangers_id) FROM stdin;
\.


--
-- Data for Name: award; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.award (id, name, descripton) FROM stdin;
\.


--
-- Data for Name: bank_import_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bank_import_data (id, entry_date, entry_value, entry_text, partner_name, entry_reference, cashbook_entry_id, cashbook_category_id, cashbook_account_id, cashbook_account_id_from, cashbook_account_id_to, is_transfer) FROM stdin;
51	2019-03-04	-2.97	Bezahlung Karte                              MC/000001422 2320  K002 04.03. 12:00 BILLA 0330\\\\WIEN\\1030      W	\N	\\xb68f54e51f9fd7765ffa43fea8d3605642495036039910ab73dde9586ce483a7776196a850a2569fe89ec78738d9322be1cc49d7e3717d65824966e02372b056	10	\N	3	\N	\N	f
52	2019-03-01	-7.30	Bezahlung Karte                              MC/000001421 2340  K002 01.03. 19:19 MCDONALDS 220\\\\WIEN\\1100	\N	\\x9dffcc8c0d4260152b6929301b414b27b84afff0e1b07643281102426b506f472e934d191ff809c2cb0163ef4eceae16cd3f94ebd1b0869979d721c48590b51f	11	\N	3	\N	\N	f
53	2019-02-27	-7.83	Bezahlung Karte                              MC/000001420 2800  K002 27.02. 12:25 INTERSPAR DANKT\\\\WIEN\\1030	\N	\\x6cc1808d99b45098d5cc5d6531441eb041afc47c470aa9ca4d6ce17caa7fc47e3fe48facba2721569d6a5e592c717b8ee8e296da04edeaa6de1bed1b41d842bf	12	\N	3	\N	\N	f
61	2019-02-24	-100.00	Auszahlung Karte                             MC/000001418 AUTOMAT   00026303 K002 24.02. 13:50	\N	\\x2ae29e1511a8897e0bab30328ca1536d6f38ca8c06b30498096b6f6d4c6a37a74640ffd63ec6c36408e76267e376e833e2b7e68881d75a072ab893ae446abd59	\N	\N	3	\N	\N	f
62	2019-02-19	-0.55	Bezahlung Karte                              MC/000001417 2801  K002 19.02. 17:39 INTERSPAR DANKT\\\\WIEN\\1100	\N	\\xfeb2105c8cf33b6330e9e5de1f3c7d046a9442489ff20d57fa1a2de83a4b2250beb917f857c8ee2eb574bf63ba8e6789d3c82679e9e012a43ae90e5356f356f3	\N	\N	3	\N	\N	f
63	2019-02-20	-7.40	Bezahlung Karte                              MC/000001416 2320  K002 20.02. 11:47 VIENNA CITY TOWER 4112\\\\WIEN\\1	\N	\\x733c6b08bf620f40652f53ce3fd58ed6f50babb9a6843fdb4f54d46686d6e239b82d3a2638f649615a00aef0d636f81b2981ccf002fa39970423ddb2fb76349c	\N	\N	3	\N	\N	f
64	2019-02-19	-9.99	D01-7772162-7019854 Zweck 1S431V7BJTOGG3V4 OG/000001415 TUBDDEDDXXX DE64300308801938627009 FIRMA ABC .L.	\N	\\x3da9922aead80599cf15ba279617bf4822dfa2e2e5202dc775f005d75c90335a016611464f4f0a3dd5d6daa98a7486fb19118ea15bd416c5282d76e3c241d228	\N	\N	3	\N	\N	f
\.


--
-- Data for Name: cashbook; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook (id, name, description) FROM stdin;
\.


--
-- Data for Name: cashbook_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_account (id, name, bank_account, bank_importer_type, receipt_code, current_number, text_for_auto_transfer_from, text_for_auto_transfer_to, default_for_cashbook) FROM stdin;
1	Kassabuch	f	\N	KA	0	\N	\N	t
2	Bank Austria Konto	t	BACA_IMPORTER	BA	0	\N	\N	f
3	EasyBank	t	EASYBANK_IMPORTER	EB	0	\N	\N	f
\.


--
-- Data for Name: cashbook_cashbook_entries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_cashbook_entries (cashbook_entries_id, cashbooks_id) FROM stdin;
\.


--
-- Data for Name: cashbook_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_category (id, name, description, color, parent_id) FROM stdin;
1	Gage	\N	#008000	\N
2	Getränkeankauf		#ff8040	\N
3	Einkauf Noten	\N	#ff8080	\N
4	Überträge	\N	#808000	\N
\.


--
-- Data for Name: cashbook_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cashbook_entry (id, title, type, date, amount, appointment_id, cashbook_category_id, receipt_id, cashbook_account_id) FROM stdin;
1	Gage Sommerfest	INCOME	2019-06-15	360.00	\N	1	\N	1
2	Einkauf Bier Probenlokal	SPENDING	2019-06-01	50.00	\N	2	\N	1
3	Getränke und Speisen	SPENDING	2019-11-01	150.00	3	2	\N	1
4	Einnahmen Zapfenstreich	INCOME	2019-10-27	3200.00	1	1	\N	1
5	Gage Marschwertung	INCOME	2019-06-29	200.00	2	\N	\N	1
8	Kontenübertrag nach Bank Austria Konto	SPENDING	2019-06-13	3000.00	\N	4	\N	1
9	Kontenübertrag von Kassabuch	INCOME	2019-06-13	3000.00	\N	4	\N	2
10	Bezahlung Karte                              MC/000001422 2320  K002 04.03. 12:00 BILLA 0330\\\\WIEN\\1030      W	SPENDING	2019-03-04	2.97	\N	\N	\N	3
11	Bezahlung Karte                              MC/000001421 2340  K002 01.03. 19:19 MCDONALDS 220\\\\WIEN\\1100	SPENDING	2019-03-01	7.30	\N	\N	\N	3
12	Bezahlung Karte                              MC/000001420 2800  K002 27.02. 12:25 INTERSPAR DANKT\\\\WIEN\\1030	SPENDING	2019-02-27	7.83	\N	\N	\N	3
\.


--
-- Data for Name: clothing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing (id, size, purchase_date, not_available, type_id) FROM stdin;
\.


--
-- Data for Name: clothing_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing_group (id, size, quantity, quantity_available, type_id) FROM stdin;
\.


--
-- Data for Name: clothing_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clothing_type (id, name) FROM stdin;
\.


--
-- Data for Name: color_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.color_code (id, name) FROM stdin;
\.


--
-- Data for Name: column_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.column_config (id, entity, column_name, default_visible, default_position, column_display_name, sort_by_name) FROM stdin;
1	COMPOSITION	colorCodeName	t	1	walterApp.composition.colorCode	colorCode.name
2	COMPOSITION	nr	t	2	walterApp.composition.nr	nr
3	COMPOSITION	title	t	3	walterApp.composition.title	title
4	COMPOSITION	genreName	t	4	walterApp.composition.genre	genre.name
5	COMPOSITION	musicBookName	t	5	walterApp.composition.musicBook	musicBook.name
6	COMPOSITION	orderingCompanyName	f	1	walterApp.composition.orderingCompany	orderingCompany.name
7	COMPOSITION	publisherName	f	2	walterApp.composition.publisher	publisher.name
8	PERSON	firstName	t	1	walterApp.person.firstName	firstName
9	PERSON	lastName	t	2	walterApp.person.lastName	lastName
10	PERSON	birthDate	t	3	walterApp.person.birthDate	birthDate
11	PERSON	gender	f	1	walterApp.person.gender	gender
12	PERSON	email	f	2	walterApp.person.email	email
13	INSTRUMENT	name	t	1	walterApp.instrument.name	name
14	INSTRUMENT	price	t	2	walterApp.instrument.price	price
15	INSTRUMENT	purchaseDate	t	3	walterApp.instrument.purchaseDate	purchaseDate
16	INSTRUMENT	privateInstrument	t	4	walterApp.instrument.privateInstrument	privateInstrument
17	INSTRUMENT	typeName	t	5	walterApp.instrument.type	typeName
18	CASHBOOK-ENTRY	date	t	1	walterApp.cashbookEntry.date	date
19	CASHBOOK-ENTRY	title	t	2	walterApp.cashbookEntry.title	title
20	CASHBOOK-ENTRY	receiptIdentifier	t	3	walterApp.cashbookEntry.receipt	receipt.identifier
21	CASHBOOK-ENTRY	cashbookCategoryName	t	4	walterApp.cashbookEntry.cashbookCategory	cashbookCategory.name
22	CASHBOOK-ENTRY	amount	t	5	walterApp.CashbookEntryType.INCOME	
23	CASHBOOK-ENTRY	amount	t	6	walterApp.CashbookEntryType.SPENDING	
24	CASHBOOK-ENTRY	cashbookAccountName	f	1	walterApp.cashbookEntry.cashbookAccount	cashbookAccount.name
25	APPOINTMENT	beginDate	t	1	walterApp.appointment.beginDate	beginDate
26	APPOINTMENT	endDate	t	2	walterApp.appointment.endDate	endDate
27	APPOINTMENT	name	t	3	walterApp.appointment.name	name
28	APPOINTMENT	typeName	t	4	walterApp.appointment.type	appointmentType.name
29	APPOINTMENT	description	f	1	walterApp.appointment.description	description
30	APPOINTMENT	streetAddress	f	2	walterApp.appointment.streetAddress	streetAddress
31	APPOINTMENT	postalCode	f	3	walterApp.appointment.postalCode	postalCode
32	APPOINTMENT	city	f	4	walterApp.appointment.city	city
33	APPOINTMENT	country	f	5	walterApp.appointment.country	country
\.


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company (id, name, street_address, postal_code, city, country, recipient) FROM stdin;
\.


--
-- Data for Name: company_person_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company_person_groups (person_groups_id, companies_id) FROM stdin;
\.


--
-- Data for Name: composer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composer (id, name) FROM stdin;
\.


--
-- Data for Name: composer_compositions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composer_compositions (compositions_id, composers_id) FROM stdin;
\.


--
-- Data for Name: composition; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composition (id, nr, title, note, akm_composition_nr, cashbook_entry_id, ordering_company_id, publisher_id, genre_id, music_book_id, color_code_id) FROM stdin;
\.


--
-- Data for Name: composition_appointments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.composition_appointments (appointments_id, compositions_id) FROM stdin;
\.


--
-- Data for Name: customization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customization (id, name, data, text) FROM stdin;
2	MEMBER_PERSONGROUP	0	
3	DEACTIVATED_FEATUREGROUPS	[]	
4	AKM_DATA	{}	
7	MONTHS_TO_DELETE_PERSON_DATA	24	
8	ORGANIZATION_COMPANY	0	
5	HOURS_TO_INVALIDATE_VERIFICATION	48	48
6	DAYS_TO_DELETE_MAILLOG	7	7
9	DEFAULT_CASHBOOK_ACCOUNT	1	
10	DEFAULT_CASHBOOK_CATEGORY_FOR_TRANSFER	4	Überträge
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels) FROM stdin;
00000000000000	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-06-13 18:55:39.1335	1	EXECUTED	7:eda8cd7fd15284e6128be97bd8edea82	createSequence		\N	3.4.2	\N	\N
00000000000001	jhipster	classpath:config/liquibase/changelog/00000000000000_initial_schema.xml	2019-06-13 18:55:39.352578	2	EXECUTED	7:b9be43015ddcfe6cc0173fe720f5a3ec	createTable, createIndex (x2), createTable (x2), addPrimaryKey, addForeignKeyConstraint (x2), loadData, dropDefaultValue, loadData (x2), createTable (x2), addPrimaryKey, createIndex (x2), addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104914-1	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_Person.xml	2019-06-13 18:55:39.407641	3	EXECUTED	7:f9a5278625d3f0c2031c364518a6f220	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104915-1	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_PersonGroup.xml	2019-06-13 18:55:39.427649	4	EXECUTED	7:e17bca58c36e5e044deab7534eb463ff	createTable		\N	3.4.2	\N	\N
20161102104916-1	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_WalterUser.xml	2019-06-13 18:55:39.463666	5	EXECUTED	7:05aafd4a1058b1bd1fa2e91c57c3b38d	createTable		\N	3.4.2	\N	\N
20161102104917-1	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_Role.xml	2019-06-13 18:55:39.500691	6	EXECUTED	7:b0fb658f2c19f8e68374c33846e78b24	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104918-1	jhipster	classpath:config/liquibase/changelog/20161102104918_added_entity_PersonInfo.xml	2019-06-13 18:55:39.519634	7	EXECUTED	7:37371b6f98f2929dbd78eab5351c882e	createTable		\N	3.4.2	\N	\N
20161102104919-1	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_Membership.xml	2019-06-13 18:55:39.536701	8	EXECUTED	7:7022f3fb55947358348baa4efd892655	createTable		\N	3.4.2	\N	\N
20161102104920-1	jhipster	classpath:config/liquibase/changelog/20161102104920_added_entity_MembershipFee.xml	2019-06-13 18:55:39.56472	9	EXECUTED	7:22e533231a2ba6fcb4ed987fc8957432	createTable		\N	3.4.2	\N	\N
20161102104921-1	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_MembershipFeeAmount.xml	2019-06-13 18:55:39.581727	10	EXECUTED	7:6bb3458e477095b053e7ae921e26de4d	createTable		\N	3.4.2	\N	\N
20161102104922-1	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_MembershipFeeForPeriod.xml	2019-06-13 18:55:39.616746	11	EXECUTED	7:e1591f04339e27e9b024c598c5633905	createTable		\N	3.4.2	\N	\N
20161102104923-1	jhipster	classpath:config/liquibase/changelog/20161102104923_added_entity_Address.xml	2019-06-13 18:55:39.644759	12	EXECUTED	7:901335603ea0d0197968a7fcfc2d36a1	createTable		\N	3.4.2	\N	\N
20161102104924-1	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_PersonInstrument.xml	2019-06-13 18:55:39.661765	13	EXECUTED	7:dfb8c080f3bcdd58cd6d369297feccdf	createTable		\N	3.4.2	\N	\N
20161102104925-1	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_Instrument.xml	2019-06-13 18:55:39.686782	14	EXECUTED	7:d404e23102c8268532fd0a1fcda881ba	createTable		\N	3.4.2	\N	\N
20161102104926-1	jhipster	classpath:config/liquibase/changelog/20161102104926_added_entity_InstrumentType.xml	2019-06-13 18:55:39.708788	15	EXECUTED	7:5ca4785573c595749d48676b920e2b35	createTable		\N	3.4.2	\N	\N
20161102104927-1	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_InstrumentService.xml	2019-06-13 18:55:39.733803	16	EXECUTED	7:08ed231981c6842b34551314095aa782	createTable		\N	3.4.2	\N	\N
20161102104928-1	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_PersonClothing.xml	2019-06-13 18:55:39.753817	17	EXECUTED	7:fa3796f1649cc12f659ed9f61b1b9bdf	createTable		\N	3.4.2	\N	\N
20161102104929-1	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_Clothing.xml	2019-06-13 18:55:39.772794	18	EXECUTED	7:54aa0caf46fa900362aded1b3f2d371a	createTable		\N	3.4.2	\N	\N
20161102104930-1	jhipster	classpath:config/liquibase/changelog/20161102104930_added_entity_ClothingType.xml	2019-06-13 18:55:39.790829	19	EXECUTED	7:5dada8ca39d16a26bafaf105ca46db8e	createTable		\N	3.4.2	\N	\N
20161102104931-1	jhipster	classpath:config/liquibase/changelog/20161102104931_added_entity_Honor.xml	2019-06-13 18:55:39.81785	20	EXECUTED	7:52f16f4cbf9599f8a2f206c40890cf12	createTable		\N	3.4.2	\N	\N
20161102104932-1	jhipster	classpath:config/liquibase/changelog/20161102104932_added_entity_Award.xml	2019-06-13 18:55:39.851867	21	EXECUTED	7:2b50b6a3b36da0e2890627fde009b833	createTable		\N	3.4.2	\N	\N
20161102104933-1	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_PersonHonor.xml	2019-06-13 18:55:39.87287	22	EXECUTED	7:988f3ea4fbb6eff4d455512cd105e8b2	createTable		\N	3.4.2	\N	\N
20161102104934-1	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_PersonAward.xml	2019-06-13 18:55:39.894889	23	EXECUTED	7:6854b28e0d1e31b2fa8f50a3fca05321	createTable		\N	3.4.2	\N	\N
20161102104935-1	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_PersonAddress.xml	2019-06-13 18:55:39.913888	24	EXECUTED	7:a2321f6ae1eb12f070558f851866800f	createTable		\N	3.4.2	\N	\N
20161102104936-1	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_Cashbook.xml	2019-06-13 18:55:39.952917	25	EXECUTED	7:0192ec399e1521a58cd2b85d4e0cef41	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104937-1	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_CashbookEntry.xml	2019-06-13 18:55:39.981929	26	EXECUTED	7:29dd4da295fce1393dccd54b90e015ed	createTable		\N	3.4.2	\N	\N
20161102104938-1	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_CashbookCategory.xml	2019-06-13 18:55:40.01033	27	EXECUTED	7:d2608f5e4b02701e2d9d6ea3194c7634	createTable		\N	3.4.2	\N	\N
20161102104939-1	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_Receipt.xml	2019-06-13 18:55:40.025956	28	EXECUTED	7:d8186d6ed7d45b615d7c556aa5158825	createTable		\N	3.4.2	\N	\N
20161102104940-1	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_Company.xml	2019-06-13 18:55:40.072916	29	EXECUTED	7:6b77cafdde0392639a51d6893de98e7d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104941-1	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_Appointment.xml	2019-06-13 18:55:40.127962	30	EXECUTED	7:077a2164c35698bc753460fd27eb1980	createTable, dropDefaultValue (x2), createTable, addPrimaryKey		\N	3.4.2	\N	\N
20161102104942-1	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_AppointmentType.xml	2019-06-13 18:55:40.143593	31	EXECUTED	7:7f25c9f4fd7051bc46d3871488a6eace	createTable		\N	3.4.2	\N	\N
20161102104943-1	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_Composition.xml	2019-06-13 18:55:40.18094	32	EXECUTED	7:269d44883ecb2c77afa1d8e758c46ecf	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104944-1	jhipster	classpath:config/liquibase/changelog/20161102104944_added_entity_ColorCode.xml	2019-06-13 18:55:40.21225	33	EXECUTED	7:5f9754f91d90ece349915983e984085c	createTable		\N	3.4.2	\N	\N
20161102104945-1	jhipster	classpath:config/liquibase/changelog/20161102104945_added_entity_MusicBook.xml	2019-06-13 18:55:40.243548	34	EXECUTED	7:3ed82be4541ab8c3fd45d07930b7395d	createTable		\N	3.4.2	\N	\N
20161102104946-1	jhipster	classpath:config/liquibase/changelog/20161102104946_added_entity_Genre.xml	2019-06-13 18:55:40.259119	35	EXECUTED	7:a0c6c19aa664b40a5af2a57db0ce57b2	createTable		\N	3.4.2	\N	\N
20161102104947-1	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_Composer.xml	2019-06-13 18:55:40.297015	36	EXECUTED	7:31e2a0aeb10819ff47c990c2a6508cc9	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104948-1	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_Arranger.xml	2019-06-13 18:55:40.328244	37	EXECUTED	7:fa875605d756b8c4ad811d6e3aead79d	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20161102104949-1	jhipster	classpath:config/liquibase/changelog/20161102104949_added_entity_Report.xml	2019-06-13 18:55:40.343906	38	EXECUTED	7:3678897ecea8ea9f19faa890389fb785	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20161113155740-1	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_OfficialAppointment.xml	2019-06-13 18:55:40.383809	39	EXECUTED	7:6821b236618c98ce49c5c77cab7796d7	createTable		\N	3.4.2	\N	\N
20161214053409-1	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_ClothingGroup.xml	2019-06-13 18:55:40.402155	40	EXECUTED	7:f6c8a7bdd4e1ea562d5a3337885ababc	createTable		\N	3.4.2	\N	\N
20170104210724-1	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_BankImportData.xml	2019-06-13 18:55:40.434983	41	EXECUTED	7:71b0a5b780302dadfd482b433724b74c	createTable		\N	3.4.2	\N	\N
20170104190142-1	jhipster	classpath:config/liquibase/changelog/20170104190142_added_entity_Template.xml	2019-06-13 18:55:40.465566	42	EXECUTED	7:26b54acd02768919aafc83838dd85a55	createTable		\N	3.4.2	\N	\N
20170104190709-1	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_Letter.xml	2019-06-13 18:55:40.504537	43	EXECUTED	7:3ed461b478f749dd060394fca492e250	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122114257-1	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_NotificationRule.xml	2019-06-13 18:55:40.537354	44	EXECUTED	7:b5e036d48a87e243f25312fea04e341f	createTable (x2), addPrimaryKey		\N	3.4.2	\N	\N
20170122115258-1	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_Notification.xml	2019-06-13 18:55:40.557947	45	EXECUTED	7:7faa054b9dc60d5ab4ddb11f0926891c	createTable, dropDefaultValue		\N	3.4.2	\N	\N
20170930130601-1	jhipster	classpath:config/liquibase/changelog/20170930130601_added_entity_Customization.xml	2019-06-13 18:55:40.588624	46	EXECUTED	7:769cc51ccd6e1c24ac182213fc19c2c9	createTable		\N	3.4.2	\N	\N
20171030171006-1	jhipster	classpath:config/liquibase/changelog/20171030171006_added_entity_CashbookAccount.xml	2019-06-13 18:55:40.619329	47	EXECUTED	7:700413e98436a448c213c16e33ad285f	createTable		\N	3.4.2	\N	\N
20161102104914-2	jhipster	classpath:config/liquibase/changelog/20161102104914_added_entity_constraints_Person.xml	2019-06-13 18:55:40.637697	48	EXECUTED	7:88759aeb6a26c2d102adca65c5c0a01a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104915-2	jhipster	classpath:config/liquibase/changelog/20161102104915_added_entity_constraints_PersonGroup.xml	2019-06-13 18:55:40.639845	49	EXECUTED	7:21b983b33da2aa2ffc57ee6555ca5696	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104916-2	jhipster	classpath:config/liquibase/changelog/20161102104916_added_entity_constraints_WalterUser.xml	2019-06-13 18:55:40.650073	50	EXECUTED	7:8276b656b3639d4bf6e7aff7ba06dbfc	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104917-2	jhipster	classpath:config/liquibase/changelog/20161102104917_added_entity_constraints_Role.xml	2019-06-13 18:55:40.670434	51	EXECUTED	7:405ae59b31b851353ce6135e9e49032a	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104919-2	jhipster	classpath:config/liquibase/changelog/20161102104919_added_entity_constraints_Membership.xml	2019-06-13 18:55:40.680697	52	EXECUTED	7:7f9df05df792e84d585e3a87791200fb	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104921-2	jhipster	classpath:config/liquibase/changelog/20161102104921_added_entity_constraints_MembershipFeeAmount.xml	2019-06-13 18:55:40.690966	53	EXECUTED	7:87d4d44d7570eeb06613cd9ff7980a38	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104922-2	jhipster	classpath:config/liquibase/changelog/20161102104922_added_entity_constraints_MembershipFeeForPeriod.xml	2019-06-13 18:55:40.711389	54	EXECUTED	7:64c63ba937f2e90697f666693f8d97af	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104924-2	jhipster	classpath:config/liquibase/changelog/20161102104924_added_entity_constraints_PersonInstrument.xml	2019-06-13 18:55:40.731832	55	EXECUTED	7:9896da86e9715a890aa1000012c3bb06	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104925-2	jhipster	classpath:config/liquibase/changelog/20161102104925_added_entity_constraints_Instrument.xml	2019-06-13 18:55:40.742186	56	EXECUTED	7:35f47a04edd6853298522c7565ab3bd7	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104927-2	jhipster	classpath:config/liquibase/changelog/20161102104927_added_entity_constraints_InstrumentService.xml	2019-06-13 18:55:40.752396	57	EXECUTED	7:86e3a9f61ffc8d438597e086d9db6fc2	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104928-2	jhipster	classpath:config/liquibase/changelog/20161102104928_added_entity_constraints_PersonClothing.xml	2019-06-13 18:55:40.772944	58	EXECUTED	7:20e0c2c6ed2ee7e95745bfd00d4ac6a1	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104929-2	jhipster	classpath:config/liquibase/changelog/20161102104929_added_entity_constraints_Clothing.xml	2019-06-13 18:55:40.783274	59	EXECUTED	7:a0aad378f7097d9619b68d6246d971c5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104933-2	jhipster	classpath:config/liquibase/changelog/20161102104933_added_entity_constraints_PersonHonor.xml	2019-06-13 18:55:40.793432	60	EXECUTED	7:0dc636021a5fa29f7ffc57df0f0f1804	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104934-2	jhipster	classpath:config/liquibase/changelog/20161102104934_added_entity_constraints_PersonAward.xml	2019-06-13 18:55:40.811815	61	EXECUTED	7:5c4037e9e9eb6843362f3636cee3365b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104935-2	jhipster	classpath:config/liquibase/changelog/20161102104935_added_entity_constraints_PersonAddress.xml	2019-06-13 18:55:40.824154	62	EXECUTED	7:f852f2459b9b5b6fcf3ecc5bdf33ee13	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104936-2	jhipster	classpath:config/liquibase/changelog/20161102104936_added_entity_constraints_Cashbook.xml	2019-06-13 18:55:40.834393	63	EXECUTED	7:d3ccfe4859720998a3563b705d361d90	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104937-2	jhipster	classpath:config/liquibase/changelog/20161102104937_added_entity_constraints_CashbookEntry.xml	2019-06-13 18:55:40.865093	64	EXECUTED	7:89905ed52236d1b70d976c728388902a	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104938-2	jhipster	classpath:config/liquibase/changelog/20161102104938_added_entity_constraints_CashbookCategory.xml	2019-06-13 18:55:40.873213	65	EXECUTED	7:d2bcaebedffa74fd5dcc7cdb4b2074b5	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104939-2	jhipster	classpath:config/liquibase/changelog/20161102104939_added_entity_constraints_Receipt.xml	2019-06-13 18:55:40.88345	66	EXECUTED	7:9ed379388af0889d05d4d9c44d8d5523	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104940-2	jhipster	classpath:config/liquibase/changelog/20161102104940_added_entity_constraints_Company.xml	2019-06-13 18:55:40.893648	67	EXECUTED	7:38fadcf668fde47fd50887e3061270e4	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104941-2	jhipster	classpath:config/liquibase/changelog/20161102104941_added_entity_constraints_Appointment.xml	2019-06-13 18:55:40.926492	68	EXECUTED	7:7788ef135fd9f5af1ce4792ef95fff69	addForeignKeyConstraint (x4)		\N	3.4.2	\N	\N
20161102104942-2	jhipster	classpath:config/liquibase/changelog/20161102104942_added_entity_constraints_AppointmentType.xml	2019-06-13 18:55:40.934575	69	EXECUTED	7:7eef97fb0ecb478a954685a99a37305c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161102104943-2	jhipster	classpath:config/liquibase/changelog/20161102104943_added_entity_constraints_Composition.xml	2019-06-13 18:55:40.967454	70	EXECUTED	7:da9379c2b1e784bc095b0004949cc923	addForeignKeyConstraint (x8)		\N	3.4.2	\N	\N
20161102104947-2	jhipster	classpath:config/liquibase/changelog/20161102104947_added_entity_constraints_Composer.xml	2019-06-13 18:55:40.987835	71	EXECUTED	7:6fe0805e298261fd2d92060ae88af2a5	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161102104948-2	jhipster	classpath:config/liquibase/changelog/20161102104948_added_entity_constraints_Arranger.xml	2019-06-13 18:55:40.998059	72	EXECUTED	7:f89f75ef2ffe3d7e2f0c9a8059b0e267	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20161113155740-2	jhipster	classpath:config/liquibase/changelog/20161113155740_added_entity_constraints_OfficialAppointment.xml	2019-06-13 18:55:41.008326	73	EXECUTED	7:6f06af06f73b7e1c13230f87db52be0c	addForeignKeyConstraint		\N	3.4.2	\N	\N
20161214053409-2	jhipster	classpath:config/liquibase/changelog/20161214053409_added_entity_constraints_ClothingGroup.xml	2019-06-13 18:55:41.018521	74	EXECUTED	7:5e6bb7daa6a9f43e0e42a56df4e7e53e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170104210724-2	jhipster	classpath:config/liquibase/changelog/20170104210724_added_entity_constraints_BankImportData.xml	2019-06-13 18:55:41.028794	75	EXECUTED	7:bdb156dd17d8e9c58a28cecc3df1221e	addForeignKeyConstraint		\N	3.4.2	\N	\N
20170122114257-2	jhipster	classpath:config/liquibase/changelog/20170122114257_added_entity_constraints_NotificationRule.xml	2019-06-13 18:55:41.049376	76	EXECUTED	7:e67d6025922b570718da7976ce33b22b	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170122115258-2	jhipster	classpath:config/liquibase/changelog/20170122115258_added_entity_constraints_Notification.xml	2019-06-13 18:55:41.069737	77	EXECUTED	7:4ccc834b3f211eaa544371ed31013737	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20170104190709-2	jhipster	classpath:config/liquibase/changelog/20170104190709_added_entity_constraints_Letter.xml	2019-06-13 18:55:41.090343	78	EXECUTED	7:cd53991884ecf839288a05c7e51fdca6	addForeignKeyConstraint (x3)		\N	3.4.2	\N	\N
20171001073000-1	fonkwill	classpath:config/liquibase/changelog/20171001073000_added_entity_constraints_Customization.xml	2019-06-13 18:55:41.110721	79	EXECUTED	7:f7567b71347f418f89c20f8ffcae1e80	addUniqueConstraint		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Customization.xml	2019-06-13 18:55:41.120958	80	EXECUTED	7:a2924d761292b8776126b9fae0fed6ac	loadData		\N	3.4.2	\N	\N
20171020073000-1	fonkwill	classpath:config/liquibase/changelog/20171020073000_added_load_data_Notification_Rule.xml	2019-06-13 18:55:41.129092	81	EXECUTED	7:2694452973080e16bcd9c41dcc3b8500	loadData		\N	3.4.2	\N	\N
201809222213	fonkwill	classpath:config/liquibase/changelog/201809222213_added_database_tag_v10.xml	2019-06-13 18:55:41.131228	82	EXECUTED	7:c0a27752fc13556fc6746108e7b21be9	tagDatabase		v1.0	3.4.2	\N	\N
201809280957	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-06-13 18:55:41.159864	83	EXECUTED	7:0aea42cc1bc0e506264e6740187aced2	dropForeignKeyConstraint (x3), dropTable (x2)		\N	3.4.2	\N	\N
201809280957-2	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-06-13 18:55:41.2236	84	EXECUTED	7:bf918496ecdd20e49ba05c8936668a63	delete, loadData, addColumn, update (x2), dropPrimaryKey, addPrimaryKey, addForeignKeyConstraint, dropColumn		\N	3.4.2	\N	\N
201809280957-3	fonkwill	classpath:config/liquibase/changelog/201809280957_changed_roles_authorities.xml	2019-06-13 18:55:41.315957	85	EXECUTED	7:f0cb4324e76f152007ddf4ac5c66d9c4	delete, addColumn (x3), addUniqueConstraint, dropForeignKeyConstraint, dropPrimaryKey, dropColumn, addColumn, addNotNullConstraint, addPrimaryKey, addForeignKeyConstraint, dropPrimaryKey, addPrimaryKey, loadData		\N	3.4.2	\N	\N
20181030171007	jhipster	classpath:config/liquibase/changelog/20181030171007_added_fields_entity_CashbookAccount.xml	2019-06-13 18:55:41.336487	86	EXECUTED	7:36f0be8f5225fbb0432ab67a2385b563	addColumn, sql, addUniqueConstraint, addNotNullConstraint, addColumn, addNotNullConstraint		\N	3.4.2	\N	\N
20181030171008	jhipster	classpath:config/liquibase/changelog/20181030171008_added_fields_entity_BankImportData.xml	2019-06-13 18:55:41.346662	87	EXECUTED	7:c437869a48070aa46b9ba00e83962521	addColumn, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181030171009	jhipster	classpath:config/liquibase/changelog/20181030171009_added_fields_entity_Receipt.xml	2019-06-13 18:55:41.356863	88	EXECUTED	7:c0b6024f7b5d77f7a807906386f9e21c	addColumn (x2), sql, addNotNullConstraint, addForeignKeyConstraint		\N	3.4.2	\N	\N
20181116150506-1	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_EmailVerificationLink.xml	2019-06-13 18:55:41.377377	89	EXECUTED	7:8b5960e5d3de2a3721bac81fe2301885	createTable, dropDefaultValue (x2)		\N	3.4.2	\N	\N
20181116150506-2	jhipster	classpath:config/liquibase/changelog/20181116150506_added_entity_constraints_EmailVerificationLink.xml	2019-06-13 18:55:41.387628	90	EXECUTED	7:2df6a8baf651650337e85e89c0fc7f0b	addForeignKeyConstraint		\N	3.4.2	\N	\N
20181201171009	jhipster	classpath:config/liquibase/changelog/20181201171009_added_fields_entity_Person.xml	2019-06-13 18:55:41.39786	91	EXECUTED	7:283254850972190046fc015604c56808	addColumn		\N	3.4.2	\N	\N
20181209160512-1	jhipster	classpath:config/liquibase/changelog/20181209160512_added_entity_Filter.xml	2019-06-13 18:55:41.426519	92	EXECUTED	7:a47059f41a8b45f602684fca638eb32c	createTable		\N	3.4.2	\N	\N
20181214184940-1	jhipster	classpath:config/liquibase/changelog/20181214184940_added_entity_ColumnConfig.xml	2019-06-13 18:55:41.449137	93	EXECUTED	7:f742d2b11b2e2cb3b75a413177f86481	createTable		\N	3.4.2	\N	\N
20181214184959	jhipster	classpath:config/liquibase/changelog/20181214184959_added_ColumnConfig_Compositions.xml	2019-06-13 18:55:41.467522	94	EXECUTED	7:a60a23361bcd5e5b40cff16faef538c1	insert (x7)		\N	3.4.2	\N	\N
20181214185854-1	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_UserColumnConfig.xml	2019-06-13 18:55:41.487945	95	EXECUTED	7:123261b8b8410ab942c2832d60496343	createTable		\N	3.4.2	\N	\N
20181214185854-2	jhipster	classpath:config/liquibase/changelog/20181214185854_added_entity_constraints_UserColumnConfig.xml	2019-06-13 18:55:41.500414	96	EXECUTED	7:a4d8cb578d5753329dc0afd908936e08	addForeignKeyConstraint (x2)		\N	3.4.2	\N	\N
20190501140300-0	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-06-13 18:55:41.510643	97	EXECUTED	7:304971ccf8e6b35a933685d572ec99d9	delete		\N	3.4.2	\N	\N
20190501140300-1	fonkwill	classpath:config/liquibase/changelog/20190105140300_added_load_data_Customization.xml	2019-06-13 18:55:41.510643	98	EXECUTED	7:2cb713628de375c86d9f32b66fbf220c	loadData		\N	3.4.2	\N	\N
20190109193459	jhipster	classpath:config/liquibase/changelog/20190109193459_added_ColumnConfig_Person.xml	2019-06-13 18:55:41.520822	99	EXECUTED	7:547b36312f7cc468f198695b357ff71b	insert (x5)		\N	3.4.2	\N	\N
20190111103059	jhipster	classpath:config/liquibase/changelog/20190111103059_added_ColumnConfig_Instrument.xml	2019-06-13 18:55:41.531023	100	EXECUTED	7:7d876ee688d09650d4fc5190aaa0f511	insert (x5)		\N	3.4.2	\N	\N
20190113105859	jhipster	classpath:config/liquibase/changelog/20190113105859_added_ColumnConfig_CashbookEntry.xml	2019-06-13 18:55:41.541322	101	EXECUTED	7:b553c8721b4cbdc0d1ddcdc7b50dfabd	insert (x7)		\N	3.4.2	\N	\N
20190113132859	jhipster	classpath:config/liquibase/changelog/20190113132859_added_ColumnConfig_Appointment.xml	2019-06-13 18:55:41.551556	102	EXECUTED	7:f98b2a57358b4d92f1c57e3f085a57bc	insert (x9)		\N	3.4.2	\N	\N
20190120132109	jhipster	classpath:config/liquibase/changelog/20190120132109_added_fields_entity_Person.xml	2019-06-13 18:55:41.561935	103	EXECUTED	7:4a5d035a7993e882776fa72f31155e11	addColumn		\N	3.4.2	\N	\N
20190120151809	jhipster	classpath:config/liquibase/changelog/20190120151809_added_fields_entity_Letter.xml	2019-06-13 18:55:41.572135	104	EXECUTED	7:66effb2ac679f7050ea23422da76d5d1	addColumn (x4), update (x2), addNotNullConstraint		\N	3.4.2	\N	\N
20190204164944	jhipster	classpath:config/liquibase/changelog/20190204164944_added_entity_MailLog.xml	2019-06-13 18:55:41.602851	105	EXECUTED	7:27e9783cd4a6a36e122c2f4e75416b02	createTable		\N	3.4.2	\N	\N
2019021116300-1	fonkwill	classpath:config/liquibase/changelog/20190211161300_added_load_data_Customization.xml	2019-06-13 18:55:41.613023	106	EXECUTED	7:a69ce2b8a7661a76f48f77f8c75cac14	loadData		\N	3.4.2	\N	\N
20190211173009	mfabsich	classpath:config/liquibase/changelog/20190211173009_added_fields_entity_Person.xml	2019-06-13 18:55:41.613023	107	EXECUTED	7:056c2a9c5da441565b1fe89df312b2fa	addColumn		\N	3.4.2	\N	\N
20190120132109	jhipster	classpath:config/liquibase/changelog/20190211173309_added_fields_entity_PersonGroup.xml	2019-06-13 18:55:41.623338	108	EXECUTED	7:380cfa08df1562ecb1d64b1263ccc964	addColumn		\N	3.4.2	\N	\N
20190224133800	jhipster	classpath:config/liquibase/changelog/20190224133800_added_fields_entity_BankImportData.xml	2019-06-13 18:55:41.631477	109	EXECUTED	7:0e7fc1217b560d62772ad97ee1c580f0	addColumn, dropColumn		\N	3.4.2	\N	\N
20190224164800	fonkwill	classpath:config/liquibase/changelog/20190224164800_added_load_data_Customization.xml	2019-06-13 18:55:41.633604	110	EXECUTED	7:ea8cdc041384ce7bba296a416f7fbe9b	loadData		\N	3.4.2	\N	\N
20190228191954	jhipster	classpath:config/liquibase/changelog/20190228191954_added_entity_constraints_CashbookEntry.xml	2019-06-13 18:55:41.641746	111	EXECUTED	7:c31dec00244a5c126a3431ce29c28499	addNotNullConstraint		\N	3.4.2	\N	\N
20190303142030	fonkwill	classpath:config/liquibase/changelog/20190303142030_added_load_data_Customization.xml	2019-06-13 18:55:41.643828	112	EXECUTED	7:733790bc2810d49d30d5e8ea6eeb7101	loadData		\N	3.4.2	\N	\N
20190303151030	jhipster	classpath:config/liquibase/changelog/20190303151030_added_fields_entity_CashbookAccount.xml	2019-06-13 18:55:41.653978	113	EXECUTED	7:f94f8528fa41eee6140d5a30765e1c21	addColumn (x2)		\N	3.4.2	\N	\N
20190304192340	jhipster	classpath:config/liquibase/changelog/20190304192340_added_fields_entity_BankImportData.xml	2019-06-13 18:55:41.662126	114	EXECUTED	7:5436101099fede421eda4fcc1ee6e1b9	addColumn (x3)		\N	3.4.2	\N	\N
20190324182355	jhipster	classpath:config/liquibase/changelog/20190324182355_added_fields_entity_CashbookAccount.xml	2019-06-13 18:55:41.664203	115	EXECUTED	7:5db27af61b710dc4a6d73cd059fdd01a	addColumn		\N	3.4.2	\N	\N
20190524152845	jhipster	classpath:config/liquibase/changelog/20190524152845_added_entity_FileStoreMetadata.xml	2019-06-13 18:55:41.684731	116	EXECUTED	7:8712de6884bede115e2e22ce51f8745d	createTable		\N	3.4.2	\N	\N
20190524152846	jhipster	classpath:config/liquibase/changelog/20190524152846_added_entity_constraints_FileStoreMetadata.xml	2019-06-13 18:55:41.694885	117	EXECUTED	7:83c31765433cf5d2d72364c94ea0d725	addForeignKeyConstraint		\N	3.4.2	\N	\N
20190614115844	jhipster	classpath:config/liquibase/changelog/20190614115844_alter_column_entry_reference_BankImportData.xml	2019-06-14 12:04:48.295605	118	EXECUTED	7:32524fd1ad7794afbf9683daa45fdafc	modifyDataType		\N	3.4.2	\N	\N
201906140300-1	fonkwill	classpath:config/liquibase/changelog/20190616140300_added_load_data_User.xml	2019-06-16 13:26:34.611943	119	EXECUTED	7:baedd612dea17de66b9e14b69d97653c	sql		\N	3.4.2	\N	\N
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: email_verification_link; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.email_verification_link (id, secret_key, creation_date, validation_date, invalid, person_id) FROM stdin;
1	pk49l36S	2019-06-13 19:43:12.824	2019-06-13 19:43:24.459	f	1
2	QXBg3iu9	2019-06-13 19:46:51.895	\N	t	1
\.


--
-- Data for Name: file_store_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.file_store_metadata (id, extension, original_file, type, storage, parent_id, created_at, size) FROM stdin;
b4891ee2-a9d9-49aa-8581-0de1a0a14fff	\N	Test	DIRECTORY	DOCUMENT	\N	2019-06-13 19:14:53.395	\N
40474902-5ebe-4065-8068-3d2249cea21a	csv	easybank_csv_test.csv	FILE	DOCUMENT	\N	2019-06-13 19:15:02.198	1127
df1ef870-9260-48a3-88a5-777041cd917b	pdf	Allgemeine Informationen.pdf	FILE	DOCUMENT	\N	2019-06-13 19:15:12.91	494526
0831d0da-bdd8-46b8-81a7-90bd060bb6f0	pdf	Briefpapier.pdf	FILE	TEMPLATE	\N	2019-06-13 19:17:09.543	55404
\.


--
-- Data for Name: filter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filter (id, list_name, name, filter) FROM stdin;
\.


--
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.genre (id, name) FROM stdin;
\.


--
-- Data for Name: honor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.honor (id, name, description) FROM stdin;
\.


--
-- Data for Name: instrument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument (id, name, purchase_date, private_instrument, price, type_id, cashbook_entry_id) FROM stdin;
\.


--
-- Data for Name: instrument_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument_service (id, date, note, cashbook_entry_id, instrument_id) FROM stdin;
\.


--
-- Data for Name: instrument_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instrument_type (id, name) FROM stdin;
\.


--
-- Data for Name: jhi_authority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_authority (name, role_id, id, write) FROM stdin;
BASE	1	1	t
MANAGEMENT	1	2	t
USER	1	3	t
ROLES	1	4	t
TENANT_CONFIGURATION	1	5	t
BASE	2	6	t
AWARD	3	7	t
LETTER	3	8	t
MUSIC_BOOK	3	9	t
DEMAND_LETTER	3	10	t
CASHBOOK	3	11	t
CLOTHING_TYPE	3	12	t
PERSON_AWARD	3	13	t
PERSON_INSTRUMENT	3	14	t
INSTRUMENT_SERVICE	3	15	t
COMPOSER	3	16	t
DOCUMENTS	3	17	t
LETTER_TEMPLATE	3	18	t
CLOTHING	3	19	t
CUSTOMIZING	3	20	t
HONOR	3	21	t
MEMBERSHIP	3	22	t
APPOINTMENT_TYPE	3	23	t
ARRANGER	3	24	t
APPOINTMENT	3	25	t
COMPOSITION	3	26	t
PERSON_HONOR	3	27	t
APPOINTMENT_PEOPLE	3	28	t
PERSON_GROUP	3	29	t
MEMBERSHIP_CATEGORY	3	30	t
NOTIFICATION_RULE	3	31	t
INSTRUMENT_TYPE	3	32	t
PERSON	3	33	t
AKM	3	34	t
PERSON_FULL_INFORMATION	3	35	t
ICAL	3	36	t
CASHBOOK-IMPORT	3	37	t
PERSON_CLOTHING	3	38	t
GENRE	3	39	t
EMAIL_VERIFICATION_LETTER	3	40	t
USER	3	41	t
COLOR_CODE	3	42	t
COMPANY	3	43	t
DATA_IMPORT	3	44	t
INSTRUMENT	3	45	t
NOTIFICATION	3	46	t
APPOINTMENT_COMPOSITION	3	47	t
ROLES	3	48	t
\.


--
-- Data for Name: jhi_persistent_audit_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_persistent_audit_event (event_id, principal, event_date, event_type) FROM stdin;
\.


--
-- Data for Name: jhi_persistent_audit_evt_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_persistent_audit_evt_data (event_id, name, value) FROM stdin;
\.


--
-- Data for Name: jhi_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_user (id, login, password_hash, first_name, last_name, email, activated, lang_key, activation_key, reset_key, created_by, created_date, reset_date, last_modified_by, last_modified_date) FROM stdin;
1	system	$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG	System	System	system@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
2	anonymoususer	$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO	Anonymous	User	anonymous@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
3	admin	$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC	Administrator	Administrator	admin@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
4	user	$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K	User	User	user@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
5	vorstand	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Vorstand	Verein	vorstand@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
6	mitglieder-admin	$2a$06$hcjDJ6ZbLmt0LUVvGLmbNOXPMXD.YYYrsO1xx7fGXk3TEsN2seC4q	Mitglieder-Verwalter	Verein	vorstand1@localhost	t	de	\N	\N	system	2019-06-13 18:55:39.143483	\N	system	\N
10	patrick.fleck	$2a$10$EN06sOhlriMKNtTK.arJEORLF8YePs4E8DcZZ6SDqb9ABRXbEVeKe	Patrick	Fleck	patrickflck0@gmail.com	t	de	\N	23114841884700049340	admin	2019-06-16 13:31:36.625	2019-06-16 13:31:36.448	admin	2019-06-16 13:31:36.625
\.


--
-- Data for Name: jhi_user_authority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jhi_user_authority (user_id, role_id) FROM stdin;
1	1
3	1
1	2
3	2
4	2
6	2
5	2
3	3
10	1
10	2
10	3
10	4
\.


--
-- Data for Name: letter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.letter (id, title, text, payment, current_period, previous_period, template_id, letter_type, subject, mail_text, attachment_file_name) FROM stdin;
1	Verifizierungs-Brief	24298	\N	\N	\N	1	EMAIL_VERIFICATION_LETTER	\N	\N	\N
2	Erinnerung	24299	\N	\N	\N	1	SERIAL_LETTER	\N	\N	\N
\.


--
-- Data for Name: letter_person_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.letter_person_group (person_groups_id, letters_id) FROM stdin;
2	1
2	2
\.


--
-- Data for Name: mail_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mail_log (id, date, email, subject, mail_log_status, error_message) FROM stdin;
1	2019-06-13 19:44:43.74	patrickflck0@gmail.com	Test Versand	SUCCESSFUL	\N
\.


--
-- Data for Name: membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership (id, begin_date, end_date, person_id, membership_fee_id) FROM stdin;
1	2019-01-01	\N	2	1
\.


--
-- Data for Name: membership_fee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee (id, description, period, period_time_fraction) FROM stdin;
1	Beitrag Unterstützer	ANNUALLY	11
\.


--
-- Data for Name: membership_fee_amount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee_amount (id, begin_date, end_date, amount, membership_fee_id) FROM stdin;
\.


--
-- Data for Name: membership_fee_for_period; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.membership_fee_for_period (id, nr, jhi_year, reference_code, paid, note, cashbook_entry_id, membership_id) FROM stdin;
1	1	2019	00001012019	f	\N	\N	1
\.


--
-- Data for Name: music_book; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.music_book (id, name, description) FROM stdin;
\.


--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification (id, entity_id, created_at, read_by_user_id, notification_rule_id) FROM stdin;
\.


--
-- Data for Name: notification_rule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification_rule (id, title, entity, conditions, active, message_group, message_single) FROM stdin;
1	Geburtstage im nächsten Monat	Person	SELECT p FROM Person p WHERE MONTH(p.birthDate) = ( MONTH(CURRENT_DATE) + 1 )	t	Im nächsten Monat haben {{count}} Personen Geburtstag.	Im nächsten Monat hat {{entity.firstName}} {{entity.lastName}} Geburtstag (geboren am {{entity.birthDate}})
\.


--
-- Data for Name: notification_rule_target_audiences; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notification_rule_target_audiences (notification_rules_id, target_role_id) FROM stdin;
\.


--
-- Data for Name: official_appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.official_appointment (id, organizer_name, organizer_address, is_head_quota, report_id) FROM stdin;
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (id, first_name, last_name, birth_date, email, telephone_number, street_address, postal_code, city, country, gender, email_type, has_direct_debit, date_lost_relevance) FROM stdin;
2	Theres	Gamsjäger	1989-03-13	theres.gamsjaeger@gmx.at	\N	Windmühlstraße 34	2405	Bad Deutsch Altenburg	Österreich	FEMALE	MANUALLY	f	\N
3	Ingrid	Fleck	1988-03-13	ingrid.fleck@bnet.at	\N	Windmühlstraße 34	2405	Bad Deutsch Altenburg	Österreich	FEMALE	MANUALLY	f	2019-06-13
1	Patrick	Fleck	1991-01-27	patrickflck0@gmail.com	\N	Windmühlstraße 34	2405	Bad Deutsch Altenburg	Österreich	MALE	VERIFIED	f	\N
\.


--
-- Data for Name: person_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_address (id, begin_date, end_date, address_id, person_id) FROM stdin;
\.


--
-- Data for Name: person_award; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_award (id, date, award_id, person_id) FROM stdin;
\.


--
-- Data for Name: person_clothing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_clothing (id, begin_date, end_date, clothing_id, person_id) FROM stdin;
\.


--
-- Data for Name: person_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_group (id, name, parent_id, has_person_relevance) FROM stdin;
1	Mitglieder	\N	t
2	Musiker	\N	t
3	Helfer	\N	f
\.


--
-- Data for Name: person_honor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_honor (id, date, honor_id, person_id) FROM stdin;
\.


--
-- Data for Name: person_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_info (id, name) FROM stdin;
\.


--
-- Data for Name: person_instrument; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_instrument (id, begin_date, end_date, instrument_id, person_id) FROM stdin;
\.


--
-- Data for Name: person_person_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person_person_groups (person_groups_id, people_id) FROM stdin;
2	1
1	2
3	3
\.


--
-- Data for Name: receipt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.receipt (id, date, note, title, file, identifier, company_id, overwrite_identifier, initial_cashbook_entry_id) FROM stdin;
\.


--
-- Data for Name: report; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.report (id, generation_date, association_id, association_name, street_address, postal_code, city) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, name) FROM stdin;
1	ROLE_ADMIN
2	ROLE_USER
3	ROLE_BOARD_MEMBER
4	ROLE_MEMBER_ADMIN
5	ROLE_ACCOUNTING_ADMIN
6	ROLE_INVENTORY_ADMIN
7	ROLE_BASE_ADMIN
\.


--
-- Data for Name: template; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.template (id, title, file, margin_left, margin_right, margin_top, margin_bottom) FROM stdin;
1	Briefvorlage	0831d0da-bdd8-46b8-81a7-90bd060bb6f0	2.5999999	2.5999999	2.64583325	2.64583325
\.


--
-- Data for Name: user_column_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_column_config (id, visible, "position", column_config_id, user_id) FROM stdin;
1	t	1	1	1
2	t	2	2	1
3	t	3	3	1
4	t	4	4	1
5	t	5	5	1
6	f	1	6	1
7	f	2	7	1
8	t	1	8	1
9	t	2	9	1
10	t	3	10	1
11	f	1	11	1
12	f	2	12	1
13	t	1	13	1
14	t	2	14	1
15	t	3	15	1
16	t	4	16	1
17	t	5	17	1
18	t	1	18	1
19	t	2	19	1
20	t	3	20	1
21	t	4	21	1
22	t	5	22	1
23	t	6	23	1
24	f	1	24	1
25	t	1	25	1
26	t	2	26	1
27	t	3	27	1
28	t	4	28	1
29	f	1	29	1
30	f	2	30	1
31	f	3	31	1
32	f	4	32	1
33	f	5	33	1
34	t	1	1	2
35	t	2	2	2
36	t	3	3	2
37	t	4	4	2
38	t	5	5	2
39	f	1	6	2
40	f	2	7	2
41	t	1	8	2
42	t	2	9	2
43	t	3	10	2
44	f	1	11	2
45	f	2	12	2
46	t	1	13	2
47	t	2	14	2
48	t	3	15	2
49	t	4	16	2
50	t	5	17	2
51	t	1	18	2
52	t	2	19	2
53	t	3	20	2
54	t	4	21	2
55	t	5	22	2
56	t	6	23	2
57	f	1	24	2
58	t	1	25	2
59	t	2	26	2
60	t	3	27	2
61	t	4	28	2
62	f	1	29	2
63	f	2	30	2
64	f	3	31	2
65	f	4	32	2
66	f	5	33	2
67	t	1	1	3
68	t	2	2	3
69	t	3	3	3
70	t	4	4	3
71	t	5	5	3
72	f	1	6	3
73	f	2	7	3
74	t	1	8	3
75	t	2	9	3
76	t	3	10	3
77	f	1	11	3
78	f	2	12	3
79	t	1	13	3
80	t	2	14	3
81	t	3	15	3
82	t	4	16	3
83	t	5	17	3
84	t	1	18	3
85	t	2	19	3
86	t	3	20	3
87	t	4	21	3
88	t	5	22	3
89	t	6	23	3
90	f	1	24	3
91	t	1	25	3
92	t	2	26	3
93	t	3	27	3
94	t	4	28	3
95	f	1	29	3
96	f	2	30	3
97	f	3	31	3
98	f	4	32	3
99	f	5	33	3
100	t	1	1	4
101	t	2	2	4
102	t	3	3	4
103	t	4	4	4
104	t	5	5	4
105	f	1	6	4
106	f	2	7	4
107	t	1	8	4
108	t	2	9	4
109	t	3	10	4
110	f	1	11	4
111	f	2	12	4
112	t	1	13	4
113	t	2	14	4
114	t	3	15	4
115	t	4	16	4
116	t	5	17	4
117	t	1	18	4
118	t	2	19	4
119	t	3	20	4
120	t	4	21	4
121	t	5	22	4
122	t	6	23	4
123	f	1	24	4
124	t	1	25	4
125	t	2	26	4
126	t	3	27	4
127	t	4	28	4
128	f	1	29	4
129	f	2	30	4
130	f	3	31	4
131	f	4	32	4
132	f	5	33	4
133	t	1	1	5
134	t	2	2	5
135	t	3	3	5
136	t	4	4	5
137	t	5	5	5
138	f	1	6	5
139	f	2	7	5
140	t	1	8	5
141	t	2	9	5
142	t	3	10	5
143	f	1	11	5
144	f	2	12	5
145	t	1	13	5
146	t	2	14	5
147	t	3	15	5
148	t	4	16	5
149	t	5	17	5
150	t	1	18	5
151	t	2	19	5
152	t	3	20	5
153	t	4	21	5
154	t	5	22	5
155	t	6	23	5
156	f	1	24	5
157	t	1	25	5
158	t	2	26	5
159	t	3	27	5
160	t	4	28	5
161	f	1	29	5
162	f	2	30	5
163	f	3	31	5
164	f	4	32	5
165	f	5	33	5
166	t	1	1	6
167	t	2	2	6
168	t	3	3	6
169	t	4	4	6
170	t	5	5	6
171	f	1	6	6
172	f	2	7	6
173	t	1	8	6
174	t	2	9	6
175	t	3	10	6
176	f	1	11	6
177	f	2	12	6
178	t	1	13	6
179	t	2	14	6
180	t	3	15	6
181	t	4	16	6
182	t	5	17	6
183	t	1	18	6
184	t	2	19	6
185	t	3	20	6
186	t	4	21	6
187	t	5	22	6
188	t	6	23	6
189	f	1	24	6
190	t	1	25	6
191	t	2	26	6
192	t	3	27	6
193	t	4	28	6
194	f	1	29	6
195	f	2	30	6
196	f	3	31	6
197	f	4	32	6
198	f	5	33	6
199	t	1	1	10
200	t	2	2	10
201	t	3	3	10
202	t	4	4	10
203	t	5	5	10
204	f	1	6	10
205	f	2	7	10
206	t	1	8	10
207	t	2	9	10
208	t	3	10	10
209	f	1	11	10
210	f	2	12	10
211	t	1	13	10
212	t	2	14	10
213	t	3	15	10
214	t	4	16	10
215	t	5	17	10
216	t	1	18	10
217	t	2	19	10
218	t	3	20	10
219	t	4	21	10
220	t	5	22	10
221	t	6	23	10
222	f	1	24	10
223	t	1	25	10
224	t	2	26	10
225	t	3	27	10
226	t	4	28	10
227	f	1	29	10
228	f	2	30	10
229	f	3	31	10
230	f	4	32	10
231	f	5	33	10
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.address_id_seq', 1, false);


--
-- Name: appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_id_seq', 3, true);


--
-- Name: appointment_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.appointment_type_id_seq', 2, true);


--
-- Name: arranger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arranger_id_seq', 1, false);


--
-- Name: award_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.award_id_seq', 1, false);


--
-- Name: bank_import_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_import_data_id_seq', 71, true);


--
-- Name: cashbook_account_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_account_id_seq', 3, true);


--
-- Name: cashbook_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_category_id_seq', 4, true);


--
-- Name: cashbook_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_entry_id_seq', 12, true);


--
-- Name: cashbook_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cashbook_id_seq', 1, false);


--
-- Name: clothing_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_group_id_seq', 1, false);


--
-- Name: clothing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_id_seq', 1, false);


--
-- Name: clothing_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clothing_type_id_seq', 1, false);


--
-- Name: color_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.color_code_id_seq', 1, false);


--
-- Name: column_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.column_config_id_seq', 33, true);


--
-- Name: company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.company_id_seq', 1, false);


--
-- Name: composer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.composer_id_seq', 1, false);


--
-- Name: composition_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.composition_id_seq', 1, false);


--
-- Name: customization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customization_id_seq', 1, false);


--
-- Name: email_verification_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.email_verification_link_id_seq', 3, true);


--
-- Name: filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filter_id_seq', 1, false);


--
-- Name: genre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.genre_id_seq', 1, false);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1000, false);


--
-- Name: honor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.honor_id_seq', 1, false);


--
-- Name: instrument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_id_seq', 1, false);


--
-- Name: instrument_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_service_id_seq', 1, false);


--
-- Name: instrument_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instrument_type_id_seq', 1, false);


--
-- Name: jhi_authority_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_authority_id_seq', 48, true);


--
-- Name: jhi_persistent_audit_event_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_persistent_audit_event_event_id_seq', 1, false);


--
-- Name: jhi_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jhi_user_id_seq', 10, true);


--
-- Name: letter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.letter_id_seq', 2, true);


--
-- Name: mail_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mail_log_id_seq', 1, true);


--
-- Name: membership_fee_amount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_amount_id_seq', 1, false);


--
-- Name: membership_fee_for_period_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_for_period_id_seq', 1, true);


--
-- Name: membership_fee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_fee_id_seq', 1, true);


--
-- Name: membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.membership_id_seq', 2, true);


--
-- Name: music_book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.music_book_id_seq', 1, false);


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_id_seq', 1, false);


--
-- Name: notification_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notification_rule_id_seq', 1, false);


--
-- Name: official_appointment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.official_appointment_id_seq', 1, false);


--
-- Name: person_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_address_id_seq', 1, false);


--
-- Name: person_award_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_award_id_seq', 1, false);


--
-- Name: person_clothing_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_clothing_id_seq', 1, false);


--
-- Name: person_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_group_id_seq', 3, true);


--
-- Name: person_honor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_honor_id_seq', 1, false);


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_id_seq', 5, true);


--
-- Name: person_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_info_id_seq', 1, false);


--
-- Name: person_instrument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_instrument_id_seq', 1, false);


--
-- Name: receipt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.receipt_id_seq', 1, false);


--
-- Name: report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.report_id_seq', 1, false);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 7, true);


--
-- Name: template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.template_id_seq', 1, true);


--
-- Name: user_column_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_column_config_id_seq', 231, true);


--
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner: 
--

BEGIN;

SELECT pg_catalog.lo_open('24298', 131072);
SELECT pg_catalog.lowrite(0, '\x3c703e486965722069737420696872205665726966697a696572756e67732d4c696e6b213c2f703e0a3c703e266e6273703b3c2f703e0a3c703e254c494e4b253c2f703e0a3c703e2547454845494d434f4445253c2f703e');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('24299', 131072);
SELECT pg_catalog.lowrite(0, '\x3c703e426974746520626561636874656e213c2f703e0a3c703e4162204672656974616720697374207769656465722050726f6265213c2f703e');
SELECT pg_catalog.lo_close(0);

COMMIT;

--
-- Name: appointment appointment_official_appointment_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT appointment_official_appointment_id_key UNIQUE (official_appointment_id);


--
-- Name: appointment_persons appointment_persons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT appointment_persons_pkey PRIMARY KEY (appointments_id, persons_id);


--
-- Name: arranger_compositions arranger_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT arranger_compositions_pkey PRIMARY KEY (arrangers_id, compositions_id);


--
-- Name: bank_import_data bank_import_data_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT bank_import_data_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- Name: cashbook_cashbook_entries cashbook_cashbook_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT cashbook_cashbook_entries_pkey PRIMARY KEY (cashbooks_id, cashbook_entries_id);


--
-- Name: company_person_groups company_person_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT company_person_groups_pkey PRIMARY KEY (companies_id, person_groups_id);


--
-- Name: composer_compositions composer_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT composer_compositions_pkey PRIMARY KEY (composers_id, compositions_id);


--
-- Name: composition_appointments composition_appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT composition_appointments_pkey PRIMARY KEY (compositions_id, appointments_id);


--
-- Name: customization customization_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization
    ADD CONSTRAINT customization_name_key UNIQUE (name);


--
-- Name: file_store_metadata file_store_metadata_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_store_metadata
    ADD CONSTRAINT file_store_metadata_id_key UNIQUE (id);


--
-- Name: instrument instrument_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT instrument_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- Name: instrument_service instrument_service_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT instrument_service_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- Name: jhi_authority jhi_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_authority
    ADD CONSTRAINT jhi_authority_pkey PRIMARY KEY (id);


--
-- Name: jhi_persistent_audit_evt_data jhi_persistent_audit_evt_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT jhi_persistent_audit_evt_data_pkey PRIMARY KEY (event_id, name);


--
-- Name: jhi_user_authority jhi_user_authority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT jhi_user_authority_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: jhi_user jhi_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT jhi_user_email_key UNIQUE (email);


--
-- Name: jhi_user jhi_user_login_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT jhi_user_login_key UNIQUE (login);


--
-- Name: letter_person_group letter_person_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT letter_person_group_pkey PRIMARY KEY (letters_id, person_groups_id);


--
-- Name: membership_fee_for_period membership_fee_for_period_cashbook_entry_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT membership_fee_for_period_cashbook_entry_id_key UNIQUE (cashbook_entry_id);


--
-- Name: notification_rule_target_audiences notification_rule_target_audiences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT notification_rule_target_audiences_pkey PRIMARY KEY (notification_rules_id, target_role_id);


--
-- Name: person_person_groups person_person_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT person_person_groups_pkey PRIMARY KEY (people_id, person_groups_id);


--
-- Name: address pk_address; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT pk_address PRIMARY KEY (id);


--
-- Name: appointment pk_appointment; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT pk_appointment PRIMARY KEY (id);


--
-- Name: appointment_type pk_appointment_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type
    ADD CONSTRAINT pk_appointment_type PRIMARY KEY (id);


--
-- Name: arranger pk_arranger; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger
    ADD CONSTRAINT pk_arranger PRIMARY KEY (id);


--
-- Name: award pk_award; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.award
    ADD CONSTRAINT pk_award PRIMARY KEY (id);


--
-- Name: bank_import_data pk_bank_import_data; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT pk_bank_import_data PRIMARY KEY (id);


--
-- Name: cashbook pk_cashbook; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook
    ADD CONSTRAINT pk_cashbook PRIMARY KEY (id);


--
-- Name: cashbook_account pk_cashbook_account; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account
    ADD CONSTRAINT pk_cashbook_account PRIMARY KEY (id);


--
-- Name: cashbook_category pk_cashbook_category; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category
    ADD CONSTRAINT pk_cashbook_category PRIMARY KEY (id);


--
-- Name: cashbook_entry pk_cashbook_entry; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT pk_cashbook_entry PRIMARY KEY (id);


--
-- Name: clothing pk_clothing; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing
    ADD CONSTRAINT pk_clothing PRIMARY KEY (id);


--
-- Name: clothing_group pk_clothing_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group
    ADD CONSTRAINT pk_clothing_group PRIMARY KEY (id);


--
-- Name: clothing_type pk_clothing_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_type
    ADD CONSTRAINT pk_clothing_type PRIMARY KEY (id);


--
-- Name: color_code pk_color_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code
    ADD CONSTRAINT pk_color_code PRIMARY KEY (id);


--
-- Name: column_config pk_column_config; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.column_config
    ADD CONSTRAINT pk_column_config PRIMARY KEY (id);


--
-- Name: company pk_company; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT pk_company PRIMARY KEY (id);


--
-- Name: composer pk_composer; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer
    ADD CONSTRAINT pk_composer PRIMARY KEY (id);


--
-- Name: composition pk_composition; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT pk_composition PRIMARY KEY (id);


--
-- Name: customization pk_customization; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customization
    ADD CONSTRAINT pk_customization PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: email_verification_link pk_email_verification_link; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link
    ADD CONSTRAINT pk_email_verification_link PRIMARY KEY (id);


--
-- Name: filter pk_filter; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter
    ADD CONSTRAINT pk_filter PRIMARY KEY (id);


--
-- Name: genre pk_genre; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT pk_genre PRIMARY KEY (id);


--
-- Name: honor pk_honor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.honor
    ADD CONSTRAINT pk_honor PRIMARY KEY (id);


--
-- Name: instrument pk_instrument; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT pk_instrument PRIMARY KEY (id);


--
-- Name: instrument_service pk_instrument_service; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT pk_instrument_service PRIMARY KEY (id);


--
-- Name: instrument_type pk_instrument_type; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_type
    ADD CONSTRAINT pk_instrument_type PRIMARY KEY (id);


--
-- Name: jhi_persistent_audit_event pk_jhi_persistent_audit_event; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_event
    ADD CONSTRAINT pk_jhi_persistent_audit_event PRIMARY KEY (event_id);


--
-- Name: jhi_user pk_jhi_user; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user
    ADD CONSTRAINT pk_jhi_user PRIMARY KEY (id);


--
-- Name: letter pk_letter; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter
    ADD CONSTRAINT pk_letter PRIMARY KEY (id);


--
-- Name: mail_log pk_mail_log; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_log
    ADD CONSTRAINT pk_mail_log PRIMARY KEY (id);


--
-- Name: membership pk_membership; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT pk_membership PRIMARY KEY (id);


--
-- Name: membership_fee pk_membership_fee; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee
    ADD CONSTRAINT pk_membership_fee PRIMARY KEY (id);


--
-- Name: membership_fee_amount pk_membership_fee_amount; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount
    ADD CONSTRAINT pk_membership_fee_amount PRIMARY KEY (id);


--
-- Name: membership_fee_for_period pk_membership_fee_for_period; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT pk_membership_fee_for_period PRIMARY KEY (id);


--
-- Name: music_book pk_music_book; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.music_book
    ADD CONSTRAINT pk_music_book PRIMARY KEY (id);


--
-- Name: notification pk_notification; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT pk_notification PRIMARY KEY (id);


--
-- Name: notification_rule pk_notification_rule; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule
    ADD CONSTRAINT pk_notification_rule PRIMARY KEY (id);


--
-- Name: official_appointment pk_official_appointment; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment
    ADD CONSTRAINT pk_official_appointment PRIMARY KEY (id);


--
-- Name: person pk_person; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT pk_person PRIMARY KEY (id);


--
-- Name: person_address pk_person_address; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT pk_person_address PRIMARY KEY (id);


--
-- Name: person_award pk_person_award; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT pk_person_award PRIMARY KEY (id);


--
-- Name: person_clothing pk_person_clothing; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT pk_person_clothing PRIMARY KEY (id);


--
-- Name: person_group pk_person_group; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group
    ADD CONSTRAINT pk_person_group PRIMARY KEY (id);


--
-- Name: person_honor pk_person_honor; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT pk_person_honor PRIMARY KEY (id);


--
-- Name: person_info pk_person_info; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_info
    ADD CONSTRAINT pk_person_info PRIMARY KEY (id);


--
-- Name: person_instrument pk_person_instrument; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT pk_person_instrument PRIMARY KEY (id);


--
-- Name: receipt pk_receipt; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT pk_receipt PRIMARY KEY (id);


--
-- Name: report pk_report; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.report
    ADD CONSTRAINT pk_report PRIMARY KEY (id);


--
-- Name: role pk_role; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT pk_role PRIMARY KEY (id);


--
-- Name: template pk_template; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.template
    ADD CONSTRAINT pk_template PRIMARY KEY (id);


--
-- Name: user_column_config pk_user_column_config; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT pk_user_column_config PRIMARY KEY (id);


--
-- Name: role role_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- Name: cashbook_account ux_cashbook_account_receipt_code; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_account
    ADD CONSTRAINT ux_cashbook_account_receipt_code UNIQUE (receipt_code);


--
-- Name: idx_persistent_audit_event; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_persistent_audit_event ON public.jhi_persistent_audit_event USING btree (principal, event_date);


--
-- Name: idx_persistent_audit_evt_data; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_persistent_audit_evt_data ON public.jhi_persistent_audit_evt_data USING btree (event_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_email ON public.jhi_user USING btree (email);


--
-- Name: idx_user_login; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_user_login ON public.jhi_user USING btree (login);


--
-- Name: appointment fk_appointment_official_appointment_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT fk_appointment_official_appointment_id FOREIGN KEY (official_appointment_id) REFERENCES public.official_appointment(id);


--
-- Name: appointment_persons fk_appointment_persons_appointments_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_appointments_id FOREIGN KEY (appointments_id) REFERENCES public.appointment(id);


--
-- Name: appointment_persons fk_appointment_persons_persons_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_persons
    ADD CONSTRAINT fk_appointment_persons_persons_id FOREIGN KEY (persons_id) REFERENCES public.person(id);


--
-- Name: appointment fk_appointment_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment
    ADD CONSTRAINT fk_appointment_type_id FOREIGN KEY (type_id) REFERENCES public.appointment_type(id);


--
-- Name: appointment_type fk_appointment_type_person_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.appointment_type
    ADD CONSTRAINT fk_appointment_type_person_group_id FOREIGN KEY (person_group_id) REFERENCES public.person_group(id);


--
-- Name: arranger_compositions fk_arranger_compositions_arrangers_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_arrangers_id FOREIGN KEY (arrangers_id) REFERENCES public.arranger(id);


--
-- Name: arranger_compositions fk_arranger_compositions_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arranger_compositions
    ADD CONSTRAINT fk_arranger_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- Name: bank_import_data fk_bank_import_data_cashbook_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES public.cashbook_category(id);


--
-- Name: bank_import_data fk_bank_import_data_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_import_data
    ADD CONSTRAINT fk_bank_import_data_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbook_entries_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbook_entries_id FOREIGN KEY (cashbook_entries_id) REFERENCES public.cashbook_entry(id);


--
-- Name: cashbook_cashbook_entries fk_cashbook_cashbook_entries_cashbooks_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_cashbook_entries
    ADD CONSTRAINT fk_cashbook_cashbook_entries_cashbooks_id FOREIGN KEY (cashbooks_id) REFERENCES public.cashbook(id);


--
-- Name: cashbook_category fk_cashbook_category_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_category
    ADD CONSTRAINT fk_cashbook_category_parent_id FOREIGN KEY (parent_id) REFERENCES public.cashbook_category(id);


--
-- Name: cashbook_entry fk_cashbook_entry_appointment_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_appointment_id FOREIGN KEY (appointment_id) REFERENCES public.appointment(id);


--
-- Name: cashbook_entry fk_cashbook_entry_cashbook_account_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_account_id FOREIGN KEY (cashbook_account_id) REFERENCES public.cashbook_account(id);


--
-- Name: cashbook_entry fk_cashbook_entry_cashbook_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_cashbook_category_id FOREIGN KEY (cashbook_category_id) REFERENCES public.cashbook_category(id);


--
-- Name: cashbook_entry fk_cashbook_entry_receipt_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cashbook_entry
    ADD CONSTRAINT fk_cashbook_entry_receipt_id FOREIGN KEY (receipt_id) REFERENCES public.receipt(id);


--
-- Name: clothing_group fk_clothing_group_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing_group
    ADD CONSTRAINT fk_clothing_group_type_id FOREIGN KEY (type_id) REFERENCES public.clothing_type(id);


--
-- Name: clothing fk_clothing_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clothing
    ADD CONSTRAINT fk_clothing_type_id FOREIGN KEY (type_id) REFERENCES public.clothing_type(id);


--
-- Name: company_person_groups fk_company_person_groups_companies_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_companies_id FOREIGN KEY (companies_id) REFERENCES public.company(id);


--
-- Name: company_person_groups fk_company_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company_person_groups
    ADD CONSTRAINT fk_company_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- Name: composer_compositions fk_composer_compositions_composers_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_composers_id FOREIGN KEY (composers_id) REFERENCES public.composer(id);


--
-- Name: composer_compositions fk_composer_compositions_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composer_compositions
    ADD CONSTRAINT fk_composer_compositions_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- Name: composition_appointments fk_composition_appointments_appointments_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_appointments_id FOREIGN KEY (appointments_id) REFERENCES public.official_appointment(id);


--
-- Name: composition_appointments fk_composition_appointments_compositions_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition_appointments
    ADD CONSTRAINT fk_composition_appointments_compositions_id FOREIGN KEY (compositions_id) REFERENCES public.composition(id);


--
-- Name: composition fk_composition_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: composition fk_composition_color_code_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_color_code_id FOREIGN KEY (color_code_id) REFERENCES public.color_code(id);


--
-- Name: composition fk_composition_genre_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_genre_id FOREIGN KEY (genre_id) REFERENCES public.genre(id);


--
-- Name: composition fk_composition_music_book_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_music_book_id FOREIGN KEY (music_book_id) REFERENCES public.music_book(id);


--
-- Name: composition fk_composition_ordering_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_ordering_company_id FOREIGN KEY (ordering_company_id) REFERENCES public.company(id);


--
-- Name: composition fk_composition_publisher_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.composition
    ADD CONSTRAINT fk_composition_publisher_id FOREIGN KEY (publisher_id) REFERENCES public.company(id);


--
-- Name: email_verification_link fk_email_verification_link_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.email_verification_link
    ADD CONSTRAINT fk_email_verification_link_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: jhi_persistent_audit_evt_data fk_evt_pers_audit_evt_data; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_persistent_audit_evt_data
    ADD CONSTRAINT fk_evt_pers_audit_evt_data FOREIGN KEY (event_id) REFERENCES public.jhi_persistent_audit_event(event_id);


--
-- Name: file_store_metadata fk_file_store_metadata_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.file_store_metadata
    ADD CONSTRAINT fk_file_store_metadata_parent_id FOREIGN KEY (parent_id) REFERENCES public.file_store_metadata(id);


--
-- Name: instrument fk_instrument_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT fk_instrument_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: instrument_service fk_instrument_service_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT fk_instrument_service_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: instrument_service fk_instrument_service_instrument_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument_service
    ADD CONSTRAINT fk_instrument_service_instrument_id FOREIGN KEY (instrument_id) REFERENCES public.instrument(id);


--
-- Name: instrument fk_instrument_type_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instrument
    ADD CONSTRAINT fk_instrument_type_id FOREIGN KEY (type_id) REFERENCES public.instrument_type(id);


--
-- Name: letter_person_group fk_letter_person_group_letters_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_letters_id FOREIGN KEY (letters_id) REFERENCES public.letter(id);


--
-- Name: letter_person_group fk_letter_person_group_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter_person_group
    ADD CONSTRAINT fk_letter_person_group_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- Name: letter fk_letter_template_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.letter
    ADD CONSTRAINT fk_letter_template_id FOREIGN KEY (template_id) REFERENCES public.template(id);


--
-- Name: membership_fee_amount fk_membership_fee_amount_membership_fee_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_amount
    ADD CONSTRAINT fk_membership_fee_amount_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES public.membership_fee(id);


--
-- Name: membership_fee_for_period fk_membership_fee_for_period_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_cashbook_entry_id FOREIGN KEY (cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: membership_fee_for_period fk_membership_fee_for_period_membership_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership_fee_for_period
    ADD CONSTRAINT fk_membership_fee_for_period_membership_id FOREIGN KEY (membership_id) REFERENCES public.membership(id);


--
-- Name: membership fk_membership_membership_fee_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT fk_membership_membership_fee_id FOREIGN KEY (membership_fee_id) REFERENCES public.membership_fee(id);


--
-- Name: membership fk_membership_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.membership
    ADD CONSTRAINT fk_membership_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: notification fk_notification_notification_rule_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_notification_rule_id FOREIGN KEY (notification_rule_id) REFERENCES public.notification_rule(id);


--
-- Name: notification fk_notification_read_by_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notification_read_by_user_id FOREIGN KEY (read_by_user_id) REFERENCES public.jhi_user(id);


--
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_notification_rules_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_notification_rules_id FOREIGN KEY (notification_rules_id) REFERENCES public.notification_rule(id);


--
-- Name: notification_rule_target_audiences fk_notification_rule_target_audiences_target_audiences_name; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification_rule_target_audiences
    ADD CONSTRAINT fk_notification_rule_target_audiences_target_audiences_name FOREIGN KEY (target_role_id) REFERENCES public.role(id);


--
-- Name: official_appointment fk_official_appointment_report_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.official_appointment
    ADD CONSTRAINT fk_official_appointment_report_id FOREIGN KEY (report_id) REFERENCES public.report(id);


--
-- Name: person_address fk_person_address_address_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT fk_person_address_address_id FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- Name: person_address fk_person_address_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_address
    ADD CONSTRAINT fk_person_address_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person_award fk_person_award_award_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT fk_person_award_award_id FOREIGN KEY (award_id) REFERENCES public.award(id);


--
-- Name: person_award fk_person_award_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_award
    ADD CONSTRAINT fk_person_award_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person_clothing fk_person_clothing_clothing_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT fk_person_clothing_clothing_id FOREIGN KEY (clothing_id) REFERENCES public.clothing(id);


--
-- Name: person_clothing fk_person_clothing_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_clothing
    ADD CONSTRAINT fk_person_clothing_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person_group fk_person_group_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_group
    ADD CONSTRAINT fk_person_group_parent_id FOREIGN KEY (parent_id) REFERENCES public.person_group(id);


--
-- Name: person_honor fk_person_honor_honor_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT fk_person_honor_honor_id FOREIGN KEY (honor_id) REFERENCES public.honor(id);


--
-- Name: person_honor fk_person_honor_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_honor
    ADD CONSTRAINT fk_person_honor_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person_instrument fk_person_instrument_instrument_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT fk_person_instrument_instrument_id FOREIGN KEY (instrument_id) REFERENCES public.instrument(id);


--
-- Name: person_instrument fk_person_instrument_person_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_instrument
    ADD CONSTRAINT fk_person_instrument_person_id FOREIGN KEY (person_id) REFERENCES public.person(id);


--
-- Name: person_person_groups fk_person_person_groups_people_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_people_id FOREIGN KEY (people_id) REFERENCES public.person(id);


--
-- Name: person_person_groups fk_person_person_groups_person_groups_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person_person_groups
    ADD CONSTRAINT fk_person_person_groups_person_groups_id FOREIGN KEY (person_groups_id) REFERENCES public.person_group(id);


--
-- Name: receipt fk_receipt_company_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT fk_receipt_company_id FOREIGN KEY (company_id) REFERENCES public.company(id);


--
-- Name: receipt fk_receipt_initial_cashbook_entry_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.receipt
    ADD CONSTRAINT fk_receipt_initial_cashbook_entry_id FOREIGN KEY (initial_cashbook_entry_id) REFERENCES public.cashbook_entry(id);


--
-- Name: jhi_user_authority fk_role_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_role_id FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: user_column_config fk_user_column_config_column_config_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT fk_user_column_config_column_config_id FOREIGN KEY (column_config_id) REFERENCES public.column_config(id);


--
-- Name: user_column_config fk_user_column_config_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_column_config
    ADD CONSTRAINT fk_user_column_config_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user(id);


--
-- Name: jhi_user_authority fk_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jhi_user_authority
    ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.jhi_user(id);


--
-- PostgreSQL database dump complete
--

